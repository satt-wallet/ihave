import 'package:flutter/cupertino.dart';
import 'package:ihave/model/approvedContracts.dart';
import 'package:ihave/model/crypto.dart';
import 'package:ihave/model/nft.dart';
import 'package:ihave/model/searchCrypto.dart';
import 'package:ihave/service/apiService.dart';
import 'package:provider/provider.dart';
import 'package:web3dart/src/credentials/credentials.dart';

import '../model/notifications.dart';
class WalletController with ChangeNotifier {
  bool checkTron = false;
  String? transactionHash;
  String? amount;
  String? selectedCrypto;
  String? linkTransaction;
  APIService apiService = new APIService();
  bool isSideMenuVisible = false;
  List<Crypto> cryptos = [];
  List<SearchCrypto> cryptosToAdd = [];
  double progress = 0;
  bool isProfileVisible = false;
  bool isWalletVisible = false;
  bool isNotificationVisible = false;
  String walletAddress = "";
  String walletAddressBTC = "";
  int selectedIndex = 2;
  bool buyCryptoCurrency = false;
  bool walletConnectScreen = false;
  bool walletConnectConnected = false;
  bool isSelectCryptoMode = false;


  bool isWelcomeScreen = true;

  List<NFT> nfts = [];

  NFT? nft;

  EthPrivateKey? privateKey;


  toggleSideMenuVisibility() {
    if (isSideMenuVisible) {
      isSideMenuVisible = false;
    } else {
      isSideMenuVisible = true;
      isProfileVisible = false;
      isWalletVisible = false;
      isNotificationVisible = false;
    }
    notifyListeners();
  }

  toggleProfileVisibility() {
      isSideMenuVisible = false;
      isWalletVisible = false;
      isNotificationVisible = false;
    notifyListeners();
  }
  toggleWalletVisibility() {
    if (isWalletVisible) {
      isWalletVisible = false;
    } else {
      isWalletVisible = true;
      isSideMenuVisible = false;
      isProfileVisible = false;
      isNotificationVisible = false;
    }
    notifyListeners();
  }
  toggleNotificationVisibility() {
    if (isNotificationVisible) {
      isNotificationVisible = false;
    } else {
      isNotificationVisible = true;
      isWalletVisible = false;
      isSideMenuVisible = false;
      isProfileVisible = false;

    }
    notifyListeners();
  }

  void toggleSelectedCrypto(bool cryptochecked) {
    isSelectCryptoMode = cryptochecked;
    notifyListeners();
  }
  void toggleWelcomeScreen(bool welcomeScreen) {
    isWelcomeScreen = welcomeScreen;
    notifyListeners();
  }
  updateApprovedTokensProgress(double progress){
    this.progress = progress;
    notifyListeners();
  }
}
