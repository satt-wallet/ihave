import 'dart:convert';
import 'dart:typed_data';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:ihave/controllers/walletController.dart';
import 'package:ihave/model/userDetails.dart';
import 'package:ihave/service/apiService.dart';
import 'package:intl/intl.dart';
import 'package:one_context/one_context.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sign_in_with_apple/sign_in_with_apple.dart';

import '../model/notifications.dart';
import '../service/walletConnectBackGroundService.dart';
import '../util/utils.dart';

class LoginController with ChangeNotifier {
  var _googleSignIn = GoogleSignIn();
  GoogleSignInAccount? _googleAccount;
  User? user;
  UserDetails? userDetails;
  Uint8List? userPicture;
  String recoveryEmail = "";
  String tokenSymbol = "";
  String tokenNetwork = "";
  String code = "";
  int tokenIndex = -1;
  String password = "";
  bool emailApple = false;
  String Balance = "";
  bool wrongCaptcha = false;
  List<dynamic> kyc = [];
  List<Notifications> notifs = [];
  String selectedCryptoName = "";
  String _selectedCryptoSymbol = "";
  String _selectedCryptoNetwork = "";
  bool? _selectedCryptoAddedToken;
  String _selectedCryptoPicUrl = "";

  static APIService apiService = new APIService();
  static late WCBGService walletConnectService;
  bool errorSendCrypto = false;
  bool errorRequestCrypto = false;
  bool errorTransaction = false;
  String marketCupCrypto = "";
  bool resetMailError = false;
  String deactivationReason = "";

  updatePicture() {}

  setResetError(bool state) {
    resetMailError = state;
    notifyListeners();
  }

  resetUserProvider() {
    recoveryEmail = "";
    tokenSymbol = "";
    tokenNetwork = "";
    code = "";
    tokenIndex = -1;
    password = "";
    emailApple = false;
    Balance = "";
    wrongCaptcha = false;
    marketCupCrypto = "";
    notifyListeners();
  }

  googleSignUp() async {
    var google;

    this._googleAccount = await _googleSignIn.signIn();
    if (_googleAccount != null) {
      this.userDetails = new UserDetails(
        username: this._googleAccount?.displayName,
        email: this._googleAccount!.email,
        picLink: this._googleAccount!.photoUrl,
        userToken: "",
        idSn: "2",
        id: this._googleAccount!.id,
        firstName: this._googleAccount?.displayName?.split(' ')[0],
        name: this._googleAccount?.displayName?.split(' ')[1],
        lastName: this._googleAccount?.displayName?.split(' ')[1],
      );
      final googleAuth = await _googleAccount!.authentication;
      final OAuthCredential credential = GoogleAuthProvider.credential(
        idToken: googleAuth.idToken,
        accessToken: googleAuth.accessToken,
      );
      var response = await apiService.sattSocialSignup(this.userDetails);
      userDetails?.userToken = response;
      FirebaseAuth.instance.signInWithCredential(credential);
      if (response != null && response != "" && response != "null" && response != "error") {
        google = "success";
        notifyListeners();
      }
    }
    return google;
  }

  addGoogle() async {
    var google = "";
    this._googleAccount = await _googleSignIn.signIn();
    if (_googleAccount != null) {
      var idSn = "2";
      var id = this._googleAccount!.id;
      google = await apiService.addGoogle(this.userDetails?.userToken ?? "", id);
    }
    try {
      this._googleAccount = await _googleSignIn.signOut();
    } catch (googleException) {}
    return google;
  }

  googleSignin() async {
    var google;
    this._googleAccount = await _googleSignIn.signIn();
    if (_googleAccount != null) {
      var idSn = "2";
      var id = this._googleAccount!.id;
      final googleAuth = await _googleAccount!.authentication;
      final OAuthCredential credential = GoogleAuthProvider.credential(
        idToken: googleAuth.idToken,
        accessToken: googleAuth.accessToken,
      );
      var response = await apiService.sattSocialSignIn(id, idSn);
      FirebaseAuth.instance.signInWithCredential(credential);
      if (response != null && response != "" && response != "null" && response != "error") {
        this.userDetails = new UserDetails(
          userToken: response,
          username: this._googleAccount!.displayName,
          email: this._googleAccount!.email,
          picLink: this._googleAccount!.photoUrl,
          idSn: "2",
          id: this._googleAccount!.id,
          firstName: this._googleAccount?.displayName?.split(' ')[0],
          name: this._googleAccount?.displayName?.split(' ')[1],
          lastName: this._googleAccount?.displayName?.split(' ')[1],
        );
        google = response;
        notifyListeners();
        return google;
      }
    } else {
      google = "canceled";
    }
    return google;
  }

  facebookSignUp() async {
    var facebook = "";
    var resultlogout = await FacebookAuth.i.logOut();
    var result = await FacebookAuth.i.login(
      permissions: ["public_profile", "email"],
    );
    if (result.status == LoginStatus.success) {
      final requestData = await FacebookAuth.i.getUserData(
        fields: "email, name, picture,first_name, last_name, id, token_for_business",
      );

      this.userDetails = new UserDetails(
        username: requestData["name"],
        email: requestData["email"],
        picLink: requestData["picture"]["data"]["url"] ?? "",
        userToken: "",
        idSn: "1",
        id: requestData["token_for_business"].toString(),
        firstName: requestData["first_name"],
        name: requestData["last_name"],
        lastName: requestData["last_name"],
      );
      var response = await apiService.sattSocialSignup(this.userDetails);
      userDetails?.userToken = response;
      if (response != null && response != "" && response != "null" && response != "error" && response != "account_exists") {
        facebook = "success";
        notifyListeners();
      } else if (response == "account_exists") {
        facebook = "already exists";
      }
      return facebook;
    }
    return facebook;
  }

  addFacebookAccount() async {
    var addResult = "";
    var resultlogout = await FacebookAuth.i.logOut();
    var result = await FacebookAuth.i.login(
      permissions: ["public_profile", "email"],
    );
    if (result.status == LoginStatus.success) {
      final requestData = await FacebookAuth.i.getUserData(
        fields: "email, name, picture,first_name, last_name, id, token_for_business",
      );
      var idSn = "1";
      var id = requestData["token_for_business"].toString();
      addResult = await apiService.addFacebook(this.userDetails?.userToken ?? "", id);
    }
    return addResult;
  }

  facebookSignin() async {
    var facebook = "";
    if (FacebookAuth.i.accessToken != null)
      try {
        var resultlogout = await FacebookAuth.i.logOut();
      } catch (e) {}
    var result = await FacebookAuth.instance.login(
      permissions: ['email', 'public_profile', 'user_birthday', 'user_friends', 'user_gender', 'user_link'],
      loginBehavior: LoginBehavior.dialogOnly, // (only android) show an authentication dialog instead of redirecting to facebook app
    );

    if (result.status == LoginStatus.success) {
      final requestData = await FacebookAuth.i.getUserData(
        fields: "email, name, picture,first_name, last_name, id, token_for_business",
      );
      var idSn = "1";
      var id = requestData["token_for_business"].toString();
      var response = await apiService.sattSocialSignIn(id, idSn);
      if (response != null && response != "" && response != "null" && response != "error") {
        userDetails = new UserDetails(
          userToken: response,
          email: requestData["email"],
          picLink: requestData["picture"]["data"]["url"] ?? "",
          idSn: "1",
          id: requestData["token_for_business"].toString(),
          firstName: requestData["first_name"],
          name: requestData["last_name"],
          lastName: requestData["last_name"],
        );
        facebook = response;
      } else if (response == "account_doesnt_exist") {
        facebook = "account does not exist";
      }
    } else {
      facebook = "canceled";
    }
    notifyListeners();
    return facebook;
  }

  mailLogin(String email, String password) async {
    String token = "";
    this.userDetails = new UserDetails(username: "", email: email, picLink: "", userToken: "");
    var result = await apiService.sattLoginMail(email, password);
    if (result.runtimeType == int) {
      DateTime dateNow = DateTime.now();
      final DateTime dateBlock = DateTime.fromMillisecondsSinceEpoch(result * 1000);
      return dateBlock;
    } else {
      userDetails!.userToken = result.toString();
      notifyListeners();
      if (userDetails!.userToken == "error") {
        return "error";
      } else if (userDetails!.userToken == "user not found") {
        return "user not found";
      } else {
        token = userDetails!.userToken.toString();
        return token;
      }
    }
  }

  mailsignUp(String email, String password, bool newsLetter) async {
    this.userDetails = new UserDetails(
      username: "",
      email: email,
      picLink: "",
      userToken: "",
    );
    var response = await apiService.sattSignUpMail(email, password, newsLetter);
    var result = "";
    if (response.runtimeType != String) {
      var responseJson = json.decode(response.body);
      if (response.statusCode != 200) {
        if (responseJson["error"]["error"] == true) {
          if (responseJson["error"]["message"] == "account_already_used") {
            result = "user exists";
          }
        }
      } else {
        if (responseJson["data"]["loggedIn"]) {
          return "user exists";
        }
      }
    } else {
      result = "welcome";
      userDetails!.userToken = response.toString();
    }
    notifyListeners();
    return result;
  }

  logout() async {
    try {
      this._googleAccount = await _googleSignIn.signOut();
    } catch (googleException) {
      try {
        await FacebookAuth.i.logOut();
      } catch (facebookException) {}
    } finally {
      userDetails = null;
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.remove('access_token');
      prefs.remove('session');
      reinitUserDetails();
      resetUserProvider();
      notifyListeners();
    }
  }

  confirmCode(email, code) async {
    {
      await apiService.confirmCode(email, code);
      notifyListeners();
    }
  }

  resendcode(String email, String token) async {
    this.userDetails = new UserDetails(
      email: email,
      userToken: "",
    );
    userDetails!.userToken = await apiService.resendCode(email, token);
    notifyListeners();
  }

  getUserDetails(String token) async {
    var user = await apiService.getUserDetails(token);
    if (user["expiredAt"] == null) {
      this.userDetails = UserDetails.fromJson(user);
      this.userDetails?.userToken = token;
      this.userDetails?.email = user["email"];
      return this.userDetails;
    } else {
      return "expired";
    }
  }

  putUserDetails(String token, UserDetails user) async {
    var userDetail = await apiService.putUserDetails(token, user);
    this.userDetails?.userToken = token;
    return userDetail;
  }

  loginFromSavedSession(String token) async {
    var user = await apiService.getUserDetails(token);
    this.userDetails = UserDetails.fromJson(user);
    this.userDetails?.userToken = token;
    notifyListeners();
  }

  loginWithApple() async {
    bool _isLogin = false;
    var apiResult = "";
    final credential = await SignInWithApple.getAppleIDCredential(
      scopes: [
        AppleIDAuthorizationScopes.email,
        AppleIDAuthorizationScopes.fullName,
      ],
    );
    if (credential.userIdentifier != null) {
      this.userDetails = new UserDetails(
        username: credential.givenName.toString() != "null" ? credential.givenName.toString() : "",
        email: RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(credential.email.toString()) ? credential.email.toString() : "",
        picLink: "",
        userToken: "",
      );
      var result = await apiService.sattLoginApple(credential.userIdentifier.toString(), RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(credential.email.toString()) ? credential.email.toString() : "", AppleIDAuthorizationScopes.fullName.toString());
      if (result == "fail") {
        _isLogin = false;
        Fluttertoast.showToast(msg: "Something went wrong, please try again");
      } else if (result == "Account Exist with another courrier") {
        _isLogin = false;
        Fluttertoast.showToast(msg: "Account Exist with another courrier");
      } else {
        _isLogin = true;
        Fluttertoast.showToast(msg: "Welcome");
        this.userDetails?.picLink = "";
        this.userDetails?.userToken = result;
      }
    } else {
      _isLogin = false;
      apiResult = "";
    }
    notifyListeners();
    return _isLogin;
  }
}
