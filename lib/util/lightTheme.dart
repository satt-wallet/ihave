import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

const PrimaryColor = const Color(0xFF4048FF);
const SecondaryColor = const Color(0xFF00041B);
const errorColor = const Color(0xFFF52079);
const successColor = const Color(0xFF00CC9E);


const Background = Colors.white;
const TextColor = const Color(0xFF00041B);
const LightTextColor = const Color(0xFFFFFFFF);
const hintColor = const Color(0xFF75758F);
const lightGrey = const Color(0xFFF6F6FF);
const textFieldBorder = const Color(0xFFD6D6E8);
class MyTheme {
  static final ThemeData iHaveTheme = _buildTheme();

  static ThemeData _buildTheme() {
    final ThemeData base = ThemeData.light();

    return base.copyWith(
      primaryColor: PrimaryColor,

      // scaffoldBackgroundColor: Background,
      // cardColor: Background,
      // backgroundColor: Background,

      textTheme: base.textTheme.copyWith(

        headline1: GoogleFonts.poppins(
          fontSize: 32,
          fontWeight: FontWeight.w700,
        ),
        headline2: GoogleFonts.poppins(
          fontSize: 32,
          fontWeight: FontWeight.w700,
          color: TextColor
        ),
        headline3: GoogleFonts.poppins(
          fontSize: 14,
          fontWeight: FontWeight.w600,
          color: hintColor,
        ),
        headline4: GoogleFonts.poppins(
          fontSize: 14,
          fontWeight: FontWeight.w600,
          color: TextColor,
        ),

        bodyText1: GoogleFonts.poppins(
          fontSize: 14,
          fontWeight: FontWeight.w500,
          color: hintColor,
        ),
        bodyText2: GoogleFonts.poppins(
          fontSize: 14,
          fontWeight: FontWeight.w500,
          color: LightTextColor,
        ),
          subtitle1: GoogleFonts.poppins(
            fontSize: 14,
            fontWeight: FontWeight.w500,
          color: TextColor,
        ),
        headline5: GoogleFonts.poppins(
          fontSize: 12,
          fontWeight: FontWeight.w600,
          fontStyle: FontStyle.normal,
          color: TextColor,
        ),
        headline6:
        GoogleFonts.poppins(
          fontWeight: FontWeight.w400,
          fontStyle: FontStyle.normal,
          fontSize: 11,
          color: TextColor,
      )
      ),
    );
  }
}