import 'package:hive/hive.dart';

Future<Box> createOpenBox() async {
  var box = await Hive.openBox('logindata');
  getdata(box);
  return box;
}

Map<dynamic, dynamic> getdata(Box box) {
  var loginPair = box.toMap();
  return loginPair;
}

void addLoginData(String email, String password, Box box) {
  Map<dynamic, dynamic> keyPair = {
    "email": email,
    "password": password,
  };
  box.add(keyPair);
}

void putLoginData(int index, String email, String password, Box box) {
  Map<dynamic, dynamic> keyPair = {
    "email": email,
    "password": password,
  };
  box.put(index, keyPair);
}

void removeLoginData(Box box, int key) {
  box.deleteAt(key);
}
