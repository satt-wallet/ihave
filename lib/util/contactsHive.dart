import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hive/hive.dart';
import 'package:ihave/util/Sqflite.dart';
import 'package:majascan/majascan.dart';
import 'package:one_context/one_context.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:permission_handler_platform_interface/permission_handler_platform_interface.dart';
import '../main.dart';

TextEditingController _nameaddbook1 = new TextEditingController();
TextEditingController _walletAddress1 = new TextEditingController();

Future<Box> createContactBox() async {
  var box = await Hive.openBox('contactData');
  getContactData(box);
  return box;
}

Map<dynamic, dynamic> getContactData(Box box) {
  var contact = box.toMap();
  return contact;
}

void addContactData(String name, String address) async {
  await SQLHelper.createItem(name, address);
}

void removecontactData(int id) async {
  await SQLHelper.deleteItem(id);
}

void addContactDataLogin(String email, String password) async {
  await SQLLOGIN.createItemlogin(email, password);
}

void removecontactDataLogin(int id) async {
  await SQLLOGIN.deleteItemlogin(id);
}

displayAddContact(String val) {
  _walletAddress1.text = val ?? "";
  showDialog(
      barrierDismissible: false,
      context: MyApp.materialKey.currentContext ?? OneContext() as BuildContext,
      builder: (context) {
        return StatefulBuilder(builder: (BuildContext context, setState) {
          return Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: Dialog(
                backgroundColor: Colors.transparent,
                insetPadding: EdgeInsets.zero,
                child: Container(
                  height: MediaQuery.of(context).size.height,
                  decoration: BoxDecoration(
                    color: Colors.white,
                  ),
                  child: Stack(
                    children: [
                      SingleChildScrollView(
                        child: Column(
                          children: [
                            Row(
                              children: [
                                Padding(
                                  padding: EdgeInsets.only(top: 30, left: 15),
                                  child: Text(
                                    "Add a Contact",
                                    style: GoogleFonts.poppins(
                                      color: Color(0xFF1F2337),
                                      fontSize: 28,
                                      fontWeight: FontWeight.w700,
                                      fontStyle: FontStyle.normal,
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  width: 110,
                                ),
                                Align(
                                  alignment: Alignment.topRight,
                                  child: Padding(
                                    padding: EdgeInsets.only(
                                      top: 20,
                                    ),
                                    child: IconButton(
                                        onPressed: () async {
                                          Navigator.of(context).pop();
                                          _walletAddress1.text = "";
                                        },
                                        icon: Icon(
                                          Icons.close,
                                          color: Color(0xFF75758F),
                                          size: MediaQuery.of(context).size.height * 0.045,
                                        )),
                                  ),
                                ),
                              ],
                            ),
                            Column(
                              children: [
                                SizedBox(
                                  height: 30,
                                ),
                                Image.asset(
                                  "images/Addbook.png",
                                  width: 220,
                                  height: 220,
                                ),
                                Center(
                                  child: Column(
                                    children: [
                                      Text(
                                        "Lorem ipsum dolor sit amet, consectetur",
                                        style: GoogleFonts.poppins(
                                          color: Color(0xFF000000),
                                          fontSize: 15,
                                          fontWeight: FontWeight.w500,
                                          fontStyle: FontStyle.normal,
                                        ),
                                      ),
                                      Text(
                                        'adipiscing elit Donec semper.',
                                        style: GoogleFonts.poppins(
                                          color: Color(0xFF000000),
                                          fontSize: 15,
                                          fontWeight: FontWeight.w500,
                                          fontStyle: FontStyle.normal,
                                        ),
                                      ),
                                      SizedBox(
                                        height: 15,
                                      ),
                                      Center(
                                        child: Container(
                                          width: MediaQuery.of(context).size.width * 0.85,
                                          child: TextFormField(
                                            controller: _nameaddbook1,
                                            decoration: new InputDecoration(
                                              hintText: 'Name',
                                              fillColor: Colors.grey,
                                              focusColor: Colors.grey,
                                              hintStyle: GoogleFonts.poppins(fontWeight: FontWeight.normal, color: Colors.grey, fontSize: MediaQuery.of(context).size.width * 0.036),
                                              contentPadding: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width * 0.04, vertical: MediaQuery.of(context).size.height * 0.017),
                                              enabledBorder: OutlineInputBorder(
                                                  borderRadius: new BorderRadius.only(
                                                      topLeft: Radius.circular(MediaQuery.of(context).size.height * 0.05),
                                                      bottomLeft: Radius.circular(MediaQuery.of(context).size.height * 0.05),
                                                      topRight: Radius.circular(MediaQuery.of(context).size.height * 0.05),
                                                      bottomRight: Radius.circular(MediaQuery.of(context).size.height * 0.05)),
                                                  borderSide: BorderSide(
                                                    color: Color(0xFFD6D6E8),
                                                  )),
                                              focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Color(0xff4048FF))),
                                            ),
                                          ),
                                        ),
                                      ),
                                      SizedBox(
                                        height: 15,
                                      ),
                                      Center(
                                        child: Container(
                                          width: MediaQuery.of(context).size.width * 0.85,
                                          child: TextFormField(
                                            style: GoogleFonts.poppins(
                                              fontSize: MediaQuery.of(context).size.height * 0.018,
                                            ),
                                            controller: _walletAddress1,
                                            onChanged: (value) {},
                                            decoration: InputDecoration(
                                              hintText: "0x0eD7e5...859b14fD0",
                                              fillColor: Colors.grey,
                                              focusColor: Colors.grey,
                                              hintStyle: GoogleFonts.poppins(fontWeight: FontWeight.normal, color: Colors.grey, fontSize: MediaQuery.of(context).size.width * 0.036),
                                              suffixIcon: SizedBox(
                                                width: MediaQuery.of(context).size.width * 0.2,
                                                height: MediaQuery.of(context).size.height * 0.065,
                                                child: ElevatedButton(
                                                  style: ButtonStyle(
                                                    backgroundColor: MaterialStateProperty.all(
                                                      Color(0xFFF6F6FF),
                                                    ),
                                                    shape: MaterialStateProperty.all(
                                                      RoundedRectangleBorder(
                                                          borderRadius: BorderRadius.only(topRight: Radius.circular(MediaQuery.of(context).size.height * 0.05), bottomRight: Radius.circular(MediaQuery.of(context).size.height * 0.05)),
                                                          side: BorderSide(
                                                            color: Color(0xFFD6D6E8),
                                                          )),
                                                    ),
                                                    elevation: MaterialStateProperty.resolveWith<double>(
                                                      (Set<MaterialState> states) {
                                                        if (states.contains(MaterialState.disabled)) {
                                                          return 0;
                                                        }
                                                        return 0; // Defer to the widget's default.
                                                      },
                                                    ),
                                                  ),
                                                  onPressed: () async {
                                                    String? qrResult = "";
                                                    if (Platform.isAndroid) {
                                                      var permission = Platform.isAndroid ? Permission.storage : Permission.photos;
                                                      var status_storage = await Permission.storage.status;

                                                      var status = await permission.request();
                                                      if (status == PermissionStatus.granted) {
                                                        final result = await Permission.camera.request();
                                                        if (result == PermissionStatus.granted) {
                                                          FocusScope.of(context).requestFocus(new FocusNode());
                                                          Future.delayed(Duration(milliseconds: 200), () async {
                                                            try {
                                                              String? qrResult = await MajaScan.startScan(title: "QRcode scanner", titleColor: Colors.amberAccent[700], qRCornerColor: Colors.orange, qRScannerColor: Colors.orange);
                                                              setState(() {
                                                                _walletAddress1.text = qrResult ?? 'null string';
                                                              });
                                                            } on PlatformException catch (ex) {
                                                              if (ex.code == MajaScan.CameraAccessDenied) {
                                                                setState(() {
                                                                  _walletAddress1.text = "Camera permission was denied";
                                                                });
                                                              } else {
                                                                setState(() {
                                                                  _walletAddress1.text = "Unknown Error $ex";
                                                                });
                                                              }
                                                            } on FormatException {
                                                              setState(() {
                                                                _walletAddress1.text = "You pressed the back button before scanning anything";
                                                              });
                                                            } catch (ex) {
                                                              setState(() {
                                                                _walletAddress1.text = "Unknown Error $ex";
                                                              });
                                                            }
                                                          });
                                                        } else {
                                                          showDialog(
                                                              context: context,
                                                              builder: (BuildContext context) => CupertinoAlertDialog(
                                                                    title: Text('Camera Permission'),
                                                                    content: Text('This app needs camera access to  scan QR code '),
                                                                    actions: <Widget>[
                                                                      CupertinoDialogAction(
                                                                        child: Text('Deny'),
                                                                        onPressed: () => Navigator.of(context).pop(),
                                                                      ),
                                                                      CupertinoDialogAction(
                                                                        child: Text('Settings'),
                                                                        onPressed: () => openAppSettings(),
                                                                      ),
                                                                    ],
                                                                  ));
                                                        }
                                                      }
                                                    } else {
                                                      FocusScope.of(context).requestFocus(new FocusNode());
                                                      Future.delayed(Duration(milliseconds: 200), () async {
                                                        try {
                                                          String? qrResult = await MajaScan.startScan(title: "QRcode scanner", titleColor: Colors.amberAccent[700], qRCornerColor: Colors.orange, qRScannerColor: Colors.orange);
                                                          setState(() {
                                                            _walletAddress1.text = qrResult ?? 'null string';
                                                          });
                                                        } on PlatformException catch (ex) {
                                                          if (ex.code == MajaScan.CameraAccessDenied) {
                                                            setState(() {
                                                              _walletAddress1.text = "Camera permission was denied";
                                                            });
                                                          } else {
                                                            setState(() {
                                                              _walletAddress1.text = "Unknown Error $ex";
                                                            });
                                                          }
                                                        } on FormatException {
                                                          setState(() {
                                                            _walletAddress1.text = "You pressed the back button before scanning anything";
                                                          });
                                                        } catch (ex) {
                                                          setState(() {
                                                            _walletAddress1.text = "Unknown Error $ex";
                                                          });
                                                        }
                                                      });
                                                    }
                                                  },
                                                  child: Image.asset(
                                                    "images/bi_qr-code-scan.png",
                                                    height: MediaQuery.of(context).size.height * 0.035,
                                                  ),
                                                ),
                                              ),
                                              errorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red)),
                                              contentPadding: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width * 0.04, vertical: MediaQuery.of(context).size.height * 0.017),
                                              enabledBorder: OutlineInputBorder(
                                                  borderRadius: new BorderRadius.only(
                                                      topLeft: Radius.circular(MediaQuery.of(context).size.height * 0.05),
                                                      bottomLeft: Radius.circular(MediaQuery.of(context).size.height * 0.05),
                                                      topRight: Radius.circular(MediaQuery.of(context).size.height * 0.05),
                                                      bottomRight: Radius.circular(MediaQuery.of(context).size.height * 0.05)),
                                                  borderSide: BorderSide(
                                                    color: Color(0xFFD6D6E8),
                                                  )),
                                              focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Color(0xff4048FF))),
                                            ),
                                          ),
                                        ),
                                      ),
                                      SizedBox(
                                        height: 30,
                                      ),
                                      ElevatedButton(
                                        child: Padding(
                                          padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.19, right: MediaQuery.of(context).size.width * 0.19, top: MediaQuery.of(context).size.height * 0.02, bottom: MediaQuery.of(context).size.height * 0.02),
                                          child: Text(
                                            "Add a new contact",
                                            style: GoogleFonts.poppins(fontWeight: FontWeight.w700, fontSize: MediaQuery.of(context).size.height * 0.02, color: Colors.white),
                                          ),
                                        ),
                                        onPressed: () {
                                          setState(() {
                                            if (_nameaddbook1.text != "" || _walletAddress1.text != "")
                                              addContactData(
                                                _nameaddbook1.text,
                                                _walletAddress1.text,
                                              );
                                            Navigator.of(context).pop();
                                            _walletAddress1.text = '';
                                            _nameaddbook1.text = '';
                                            Fluttertoast.showToast(msg: "The contact was successfully added", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.TOP, timeInSecForIosWeb: 1, backgroundColor: Colors.green, textColor: Colors.white, fontSize: 16.0);
                                          });
                                        },
                                        style: ButtonStyle(
                                          backgroundColor: MaterialStateProperty.all(
                                            const Color(0xFF4048FF),
                                          ),
                                          shape: MaterialStateProperty.all(
                                            RoundedRectangleBorder(
                                              borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.08),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                              ],
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                )),
          );
        });
      });
}
