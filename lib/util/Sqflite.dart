import 'package:flutter/foundation.dart';
import 'package:sqflite/sqflite.dart' as sql;

class SQLHelper {
  static Future<void> createTables(sql.Database database) async {
    await database.execute("""CREATE TABLE contact(
        id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
        name TEXT,
        address TEXT,
        createdAt TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
      )
      """);
  }

  static Future<sql.Database> db() async {
    return sql.openDatabase(
      'Contact.db',
      version: 1,
      onCreate: (sql.Database database, int version) async {
        await createTables(database);
      },
    );
  }

  static Future<int> createItem(String? name, String? address) async {
    final db = await SQLHelper.db();

    final data = {'name': name, 'address': address};
    final id = await db.insert('contact', data, conflictAlgorithm: sql.ConflictAlgorithm.replace);
    return id;
  }

  static Future<List<Map<dynamic, dynamic>>> getItems() async {
    final db = await SQLHelper.db();
    return db.query('contact', orderBy: "id");
  }

  static Future<List<Map<String, dynamic>>> getItem(int id) async {
    final db = await SQLHelper.db();
    return db.query('contact', where: "id = ?", whereArgs: [id], limit: 1);
  }

  static Future<int> updateItem(int id, String name, String? address) async {
    final db = await SQLHelper.db();

    final data = {'name': name, 'address': address, 'createdAt': DateTime.now().toString()};

    final result = await db.update('contact', data, where: "id = ?", whereArgs: [id]);
    return result;
  }

  static Future<void> deleteItem(int id) async {
    final db = await SQLHelper.db();
    try {
      await db.delete("contact", where: "id = ?", whereArgs: [id]);
    } catch (err) {}
  }
}

class SQLLOGIN {
  static Future<void> createTableslogin(sql.Database database) async {
    await database.execute("""CREATE TABLE login(
        id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
        email TEXT,
        password TEXT,
        createdAt TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
      )
      """);
  }

  static Future<sql.Database> dblogin() async {
    return sql.openDatabase(
      'Login.db',
      version: 1,
      onCreate: (sql.Database database, int version) async {
        await createTableslogin(database);
      },
    );
  }

  static Future<int> createItemlogin(String? email, String? password) async {
    final db = await SQLLOGIN.dblogin();

    final data = {'email': email, 'password': password};
    final id = await db.insert('login', data, conflictAlgorithm: sql.ConflictAlgorithm.replace);
    return id;
  }

  static Future<List<Map<dynamic, dynamic>>> getItemslogin() async {
    final db = await SQLLOGIN.dblogin();
    return db.query('login', orderBy: "id");
  }

  static Future<List<Map<String, dynamic>>> getItemlogin(int id) async {
    final db = await SQLLOGIN.dblogin();
    return db.query('login', where: "id = ?", whereArgs: [id], limit: 1);
  }

  static Future<int> updateItemlogin(int id, String email, String? password) async {
    final db = await SQLLOGIN.dblogin();

    final data = {'email': email, 'password': password, 'createdAt': DateTime.now().toString()};

    final result = await db.update('login', data, where: "id = ?", whereArgs: [id]);
    return result;
  }

  static Future<void> deleteItemlogin(int id) async {
    final db = await SQLLOGIN.dblogin();
    try {
      await db.delete("login", where: "id = ?", whereArgs: [id]);
    } catch (err) {}
  }
}
