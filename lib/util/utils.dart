import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ihave/controllers/LoginController.dart';
import 'package:ihave/controllers/walletController.dart';
import 'package:ihave/ui/WelcomeScreen.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:decimal/decimal.dart';

gotoPageGlobal(BuildContext context, Widget page) {
  Provider.of<WalletController>(context, listen: false).toggleSelectedCrypto(false);
  Navigator.pushReplacement(context, MaterialPageRoute(builder: (_) => page));
}

formatCryptoBalance(double balance, String name) {
  var stringBalance = balance.toStringAsFixed(12);
  balance = Decimal.parse(stringBalance).toDouble();
  if (balance != 0) {
    var nondecimals = balance.toInt().toString().length;
    var decimalNumber = 2;
    var decimals = 0;
    var formatted = "";
    if (balance - balance.toInt() <= 0) {
      formatted = balance.toInt().toString();
      decimalNumber = 0;
    }
    var charNumber = getTotalcharsNumber(balance);
    if (charNumber <= 9) {
      if (formatted == "") {
        if (balance.toString().indexOf(".") != -1) {
          decimalNumber = (balance.toString().split('.')[1]).length;
        }
        if ((decimalNumber + nondecimals) > charNumber) {
          decimals = charNumber - nondecimals;
        } else {
          decimals = decimalNumber;
        }
        if (decimalNumber > decimals) {
          formatted = prettify(balance, decimals);
        } else {
          formatted = prettify(balance, decimals);
        }
      }
      return NumberFormat.simpleCurrency(name: name, decimalDigits: decimals).format(double.parse(formatted));
    } else if (charNumber == 11) {
      if (formatted == "") {
        if ((balance.toString().split('.')[1]).length > 1) {
          decimals = 2;
        } else {
          decimals = (balance.toString().split('.')[1]).length;
        }
      } else {
        decimals = 0;
      }
      formatted = prettify(balance, decimals);
      return NumberFormat.simpleCurrency(name: name, decimalDigits: 2).format(double.parse(formatted));
    } else {
      var formatted = balance.toInt();
      return NumberFormat.simpleCurrency(name: name, decimalDigits: 0).format(formatted);
    }
  }
  return NumberFormat.simpleCurrency(name: "").format(0.00).toString();
}

formatDollarBalance(double balance) {
  var stringBalance = balance.toStringAsFixed(12);
  balance = Decimal.parse(stringBalance).toDouble();
  if (balance != 0) {
    var nondecimals = balance.toInt().toString().length;
    var decimalNumber = 2;
    var decimals = 0;
    var formatted = "";
    if (balance - balance.toInt() <= 0) {
      formatted = balance.toInt().toString();
      decimalNumber = 0;
    }
    var charNumber = getTotalcharsNumber(balance);
    if (charNumber <= 9) {
      if (formatted == "") {
        if (balance.toString().indexOf(".") != -1) {
          decimalNumber = (balance.toString().split('.')[1]).length;
        }
        if ((decimalNumber + nondecimals) > charNumber) {
          decimals = charNumber - nondecimals;
        } else {
          decimals = decimalNumber;
        }
        if (decimalNumber > decimals) {
          formatted = prettify(balance, decimals);
        } else {
          formatted = prettify(balance, decimals);
        }
      }
      return NumberFormat.simpleCurrency(locale: 'en_US', decimalDigits: decimals).format(double.tryParse(formatted) ?? balance);
    } else if (charNumber == 11) {
      if (formatted == "") {
        if ((balance.toString().split('.')[1]).length > 1) {
          decimals = 2;
        } else {
          decimals = (balance.toString().split('.')[1]).length;
        }
      } else {
        decimals = 0;
      }
      formatted = prettify(balance, decimals);
      return NumberFormat.simpleCurrency(locale: 'en_US', decimalDigits: 2).format(double.parse(formatted));
    } else {
      formatted = prettify(balance, 0);
      return NumberFormat.simpleCurrency(locale: 'en_US', decimalDigits: 0).format(double.tryParse(formatted) ?? 0.00);
    }
  }
  return NumberFormat.simpleCurrency(locale: 'en_US').format(0.00).toString();
}

reinitUserDetails() {
  LoginController.apiService.isEnabled = false;
  LoginController.apiService.isComplete = true;
  LoginController.apiService.is2FA = false;
  LoginController.apiService.isCompletedPassPhrase = false;
  LoginController.apiService.emailApple = false;
  LoginController.apiService.userID = "";
  LoginController.apiService.walletAddress = "";
  LoginController.apiService.btcWalletAddress = null;
  LoginController.apiService.balance = "";
  LoginController.apiService.idOnSn = "";
  LoginController.apiService.idOnSn2 = "";
}

String prettify(double d, int decimal) =>
    d.toString().substring(0, d.toString().indexOf('.') + decimal + 1).replaceFirst(RegExp(r'\.?0*$'), '');

int getTotalcharsNumber(double balance) {
  if (balance < 0.1) {
    return 9;
  } else if (balance < 1.9) {
    return 7;
  } else if (balance < 9.9999) {
    return 5;
  } else if (balance < 9999.99) {
    return 11;
  } else {
    return 12;
  }
}

class EthConversions {
  static double weiToEth(BigInt amount, int? decimal) {
    if (decimal == null) {
      double db = amount / BigInt.from(10).pow(18);
      return double.parse(db.toStringAsFixed(2));
    } else {
      double db = amount / BigInt.from(10).pow(decimal);
      return double.parse(db.toStringAsFixed(2));
    }
  }

  static double weiToEthUnTrimmed(BigInt amount, int? decimal) {
    if (decimal == null) {
      double db = amount / BigInt.from(10).pow(18);
      return double.parse(db.toStringAsFixed(6));
    } else {
      double db = amount / BigInt.from(10).pow(decimal);
      return double.parse(db.toStringAsFixed(6));
    }
  }

  static String weiToGwei(BigInt amount) {
    var db = amount / BigInt.from(10).pow(9);
    return db.toStringAsPrecision(2);
  }

  static BigInt ethToWei(String amount, int? decimal) {
    double db = double.parse(amount) * pow(10, 4);
    int it = db.toInt();
    BigInt bi = BigInt.from(it) * BigInt.from(10).pow(decimal == null ? 14 : decimal - 4);
    return bi;
  }
}
