import 'dart:math';

import 'package:english_words/english_words.dart';
import 'package:flutter/material.dart';
import 'package:scan/scan.dart';

class QrCodeScan extends StatefulWidget {
  const QrCodeScan({Key? key}) : super(key: key);

  @override
  _QrCodeScanState createState() => _QrCodeScanState();
}

class _QrCodeScanState extends State<QrCodeScan> {
  final ScanController controller = ScanController();
  var id;
  String generateUniqueId() {
    var random = Random();
    var now = DateTime.now();
    var id = '${now.second}-${random.nextInt(10000)}';
    return id;
  }
  String generateRandomText(int wordCount) {
    final randomWords = generateWordPairs().take(wordCount);
    final text = randomWords.map((pair) => pair.asString).join(' ');
    return text;
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child:  ScanView(
          controller: controller,
          scanAreaScale: 1,
          scanLineColor: Colors.green.shade400,
          onCapture: (data) {
            Future.delayed(Duration(seconds: 1), () {
              Navigator.pop(context, data ??"");
            }) ;
          },
        ),
      ),
    );
  }
}
