import 'dart:developer';
import 'dart:io';
import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:launch_review/launch_review.dart';
import 'package:neoversion/neoversion.dart';
import 'package:open_store/open_store.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:peekanapp/peekanapp.dart';
import 'package:url_launcher/url_launcher_string.dart';

class NeoVersionOverriden extends NeoVersion {
  @override
  late PeekanappClient _peekanapp;

  @override
  String? androidAppId;

  @override
  String? iOSAppId;
  NeoVersionOverriden({this.androidAppId, this.iOSAppId}) {
    this._peekanapp = PeekanappClient();
  }

  /// Factory method that creates a new [NeoVersion] instance with a different
  /// Peek-An-App API URL.
  factory NeoVersionOverriden.withPeekanappApiUrl(String url) {
    return NeoVersionOverriden(androidAppId: null, iOSAppId: null)
      .._peekanapp = PeekanappClient.withApiUrl(url);
  }
  @override
  Future<VersionStatus> getVersionStatus() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    if (Platform.isIOS) {
      return _getIOSVersionStatus(packageInfo);
    } else if (Platform.isAndroid) {
      return _getAndroidVersionStatus(packageInfo);
    } else {
      throw Exception('Unsupported platform ${Platform.operatingSystem}');
    }
  }


  @override
  Future<void> showAlertIfNecessary(
      {required BuildContext context,
        String title = 'Update available',
        UpdateDialogText? dialogText,
        String updateButtonText = 'Update',
        bool dismissable = true,
        String dismissButtonText = 'Dismiss',
        VoidCallback? onDismissed}) async {
    final VersionStatus? status = await getVersionStatus();
    if (status?.needsUpdate == true) {
      await showUpdateDialog(
          context: context,
          status: status!,
          title: title,
          dialogText: dialogText,
          updateButtonText: updateButtonText,
          dismissable: dismissable,
          dismissButtonText: dismissButtonText,
          onDismissed: onDismissed);
    }
  }

  @override
  Future<void> showUpdateDialog({
    required BuildContext context,
    required VersionStatus status,
    String title = 'Update available',
    UpdateDialogText? dialogText,
    String updateButtonText = 'Update',
    bool dismissable = true,
    String dismissButtonText = 'Dismiss',
    VoidCallback? onDismissed,
  }) async {
    final titleWidget = Text(title);
    final textWidget = Text((dialogText ?? _defaultUpdateDialogText)(
        status.localVersion, status.appStoreVersion, dismissable));

    List<Widget> actions = [];

    if (Platform.isAndroid) {
      actions.add(
        TextButton(
            child: Text(updateButtonText),
            onPressed: () => _onUpdateButtonPressed(
                context, status.appStoreUrl, dismissable)),
      );
    }

    if (Platform.isIOS) {
      actions.add(
        CupertinoDialogAction(
            child: Text(updateButtonText),
            onPressed: () => _onUpdateButtonPressed(
                context, status.appStoreUrl, dismissable)),
      );
    }

    if (dismissable) {
      final dismissAction = onDismissed != null
          ? onDismissed
          : () => _onDismissButtonPressed(context);

      if (Platform.isAndroid) {
        actions.add(
          TextButton(child: Text(dismissButtonText), onPressed: dismissAction),
        );
      }

      if (Platform.isIOS) {
        actions.add(
          CupertinoDialogAction(
              child: Text(dismissButtonText), onPressed: dismissAction),
        );
      }
    }

    await showDialog(
        context: context,
        barrierDismissible: dismissable,
        builder: (BuildContext context) {
          return WillPopScope(
            child: Platform.isAndroid
                ? AlertDialog(
              title: titleWidget,
              content: textWidget,
              actions: actions,
            )
                : CupertinoAlertDialog(
              title: titleWidget,
              content: textWidget,
              actions: actions,
            ),
            onWillPop: () => Future.value(dismissable),
          );
        });
  }
  @override
  String _defaultUpdateDialogText(
      String localVersion, String appStoreVersion, bool dismissable) {
    String base =
        'A new version of the app is available for download ($appStoreVersion). Your version is $localVersion.';

    if (dismissable) {
      return base + ' Would you like to update?';
    } else {
      return base + ' Please update to the latest version.';
    }
  }
  @override
  void _onUpdateButtonPressed(
      BuildContext context, String appStoreUrl, bool dismissable) {
    this._launchAppStore(appStoreUrl);

    if (dismissable) {
      Navigator.of(context, rootNavigator: true).pop();
    }
  }
  @override
  void _onDismissButtonPressed(BuildContext context) {
    Navigator.of(context, rootNavigator: true).pop();
  }
  @override
  Future<void> _launchAppStore(String url) async {
    OpenStore.instance.open(
      appStoreId: iOSAppId,
      androidAppBundleId: androidAppId,
    );
    }
  @override
  Future<VersionStatus> _getIOSVersionStatus(PackageInfo packageInfo) async {
    final response = await this._peekanapp
        .getAppVersion(iOSAppId: this.iOSAppId ?? packageInfo.packageName);
    return VersionStatus(
        localVersion: this._normalizeVersion(packageInfo.version),
        appStoreVersion: this._normalizeVersion(response.appstore),
        appStoreUrl: response.meta.appstoreUrl ?? 'unknown');
  }

  @override
  Future<VersionStatus> _getAndroidVersionStatus(
      PackageInfo packageInfo) async {
    final response = await this._peekanapp.getAppVersion(
        androidAppId: this.androidAppId ?? packageInfo.packageName);
    return VersionStatus(
        localVersion: this._normalizeVersion(packageInfo.version),
        appStoreVersion: this._normalizeVersion(response.playstore),
        appStoreUrl: response.meta.playstoreUrl ?? 'unknown');
  }
  @override
  String _normalizeVersion(String? raw) {
    if (raw == null) {
      log('Warning: failed to normalize version string: raw version is null',
          level: 900);
      return '0.0.0';
    }

    final match = RegExp(r'\d+(\.\d+)*').firstMatch(raw)?.group(0);

    if (match == null) {
      log('Warning: failed to normalize version string: raw version $raw is invalid',
          level: 900);
      return '0.0.0';
    }

    return match;
  }

}