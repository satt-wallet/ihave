import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:ui';
import 'package:dio/dio.dart';
import 'package:app_version/app_version.dart';
import 'package:collection/collection.dart';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:http/http.dart';
import 'package:ihave/controllers/LoginController.dart';
import 'package:ihave/controllers/walletController.dart';
import 'package:ihave/model/crypto.dart';
import 'package:ihave/service/LocalNotification.dart';
import 'package:ihave/service/apiService.dart';
import 'package:ihave/service/walletConnectBackGroundService.dart';
import 'package:ihave/ui/Profilepage.dart';
import 'package:ihave/ui/WelcomeScreen.dart';
import 'package:ihave/ui/activationMail.dart';
import 'package:ihave/ui/addToken.dart';
import 'package:ihave/ui/biometrics.dart';
import 'package:ihave/ui/blockchainKey.dart';
import 'package:ihave/ui/buy.dart';
import 'package:ihave/ui/change_password.dart';
import 'package:ihave/ui/confirmAccountDesactivation.dart';
import 'package:ihave/ui/creatwallet.dart';
import 'package:ihave/ui/cryptoDetails.dart';
import 'package:ihave/ui/dashboard.dart';
import 'package:ihave/ui/desactivate_account.dart';
import 'package:ihave/ui/exportKeyBtcEth.dart';
import 'package:ihave/ui/kyc.dart';
import 'package:ihave/ui/mywallet.dart';
import 'package:ihave/ui/notificationPage.dart';
import 'package:ihave/ui/push_notification.dart';
import 'package:ihave/ui/passPhrase.dart';
import 'package:ihave/ui/resetPassword.dart';
import 'package:ihave/ui/security.dart';
import 'package:ihave/ui/settings.dart';
import 'package:ihave/ui/signup.dart';
import 'package:ihave/ui/socialMail.dart';
import 'package:ihave/ui/splachscreen.dart';
import 'package:ihave/ui/successWallet.dart';
import 'package:ihave/ui/towFactorPage.dart';
import 'package:ihave/ui/transactionpassword.dart';
import 'package:ihave/ui/walletConnect.dart';
import 'package:ihave/util/qrCodeScan.dart';
import 'package:ihave/util/utils.dart';
import 'package:localstorage/localstorage.dart';
import 'package:neoversion/neoversion.dart';
import 'package:new_version/new_version.dart';
import 'package:one_context/one_context.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:smart_timer/smart_timer.dart';
import 'package:stack_trace/stack_trace.dart';
import '../ui/signin.dart';
import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:responsive_framework/responsive_framework.dart';
import 'package:cron/cron.dart';
import 'model/searchCrypto.dart';
import 'util/lightTheme.dart';
import 'package:in_app_update/in_app_update.dart';
import 'dart:async';
import 'package:package_info_plus/package_info_plus.dart';

const AndroidNotificationChannel channel = AndroidNotificationChannel(
    'high_importance_channel', // id
    'High Importance Notifications', // title

    importance: Importance.high,
    playSound: true);
const notificationChannelId = 'my_foreground';

// this will be used for notification id, So you can update your custom notification with this id.
const notificationId = 888;
final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
FirebaseMessaging messaging = FirebaseMessaging.instance;

Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  await Firebase.initializeApp();
}

String? token;
final LocalStorage storage = new LocalStorage('localstorage_app');
final LocalNotification servicenotif = LocalNotification();
const simplePeriodicTask = "simplePeriodicTask";
/**
 * Notification Methods
 */
/*Future initNotifs() async{
  await launchServiceNotif();
  return "done";
}*/
/*Future<String> showNotif(int i, String cryptoName, double price) async{
  await servicenotif.shownotification(id: i, title: 'Watchlist', body: '${cryptoName ?? ""} have more than 5% variation in the last hour, new price : ${formatDollarBalance(price)}');
  return "done";
}*/
/*launchServiceNotif(){
  Timer.periodic(
      Duration(
        minutes: 30,
      ), (timer) {


    SharedPreferences.getInstance().then((sharedPrefValue) async {

      token = sharedPrefValue.getString('access_token');
      final SharedPreferences prefs = await SharedPreferences.getInstance();
      if(token!= null && token != "") {
        var cryptos = (json.decode(sharedPrefValue.getString("user-list") ??"") as List ).map((i) => Crypto.fromJson(i))
            .toList() ?? [];
        LoginController.apiService.cryptosToAdd("").then((value){
          List<SearchCrypto> data = value;
          String symbols = "";
          var i = 0;
          cryptos.forEach((element) async {
            if (element.id != null && element.id != "") {
              var variationUser = element.variation ?? 0.0;
              var variationLive = data.firstWhereOrNull((e) => e.name?.toLowerCase() == element.name?.toLowerCase() || e.symbol?.toLowerCase() == element.symbol?.toLowerCase()  )?.changePourcent ?? 0.0;
              if (variationUser > 5 || variationUser < -5){
                print('${element.name ?? ""} have more than 5% variation in the last hour, new price : (\$${element.price})');
                var cryptoName = element.symbol ?? "";
                var price = element.price ?? 0.00;
                if (!cryptoName.toLowerCase().contains("sattbep20") && !cryptoName.toLowerCase().contains("wsatt")){
                  showNotif(i, cryptoName, price).then((value){
                    print (value);
                  });
                  i++;
                }
                /* flutterLocalNotificationsPlugin.show(
                    notificationId,
                    'iHave',
                    '${cryptoName ?? ""} have more than 5% variation in the last hour',
                    const NotificationDetails(
                      android: AndroidNotificationDetails(
                        notificationChannelId,
                        'iHave',
                        icon: '@mipmap/ic_launcher',
                        ongoing: true,
                        playSound: true,
                        sound: UriAndroidNotificationSound("assets/tunes/pop.mp3"),
                        //sound: RawResourceAndroidNotificationSound("assets/tunes/pop.mp3"),
                        //sound: RawResourceAndroidNotificationSound("@raw/alarm"),
                      ),

                    ),
                  );*/
              }
            }
          });
          /* if (symbols != ""){


              }*/
        });
      }else{
        print("Please connect to your iHave app" );

        /*await servicenotif.shownotification(id: 0, title: 'IHave', body: 'Please connect to your iHave app');*/

      }

    });

  });
}*/
/*Future<void> initializeService() async {
  final service = FlutterBackgroundService();
  const AndroidNotificationChannel channel = AndroidNotificationChannel(
    notificationChannelId, // id
    'MY FOREGROUND SERVICE', // title
    description:
    'This channel is used for important notifications.', // description
    importance: Importance.high, // importance must be at low or higher level
  );

  await flutterLocalNotificationsPlugin
      .resolvePlatformSpecificImplementation<
      AndroidFlutterLocalNotificationsPlugin>()
      ?.createNotificationChannel(channel);
  await service.configure(
    androidConfiguration: AndroidConfiguration(
      // this will be executed when app is in foreground or background in separated isolate
      onStart: onStart,
      notificationChannelId: notificationChannelId, // this must match with notification channel you created above.
      foregroundServiceNotificationId: notificationId,
      initialNotificationContent: "Listening to Crypto variations",
      initialNotificationTitle: 'Watchlist',
      // auto start service
      autoStart: true,
      isForegroundMode: true,
    ),
    iosConfiguration: IosConfiguration(
      // auto start service
      autoStart: true,

      // this will be executed when app is in foreground in separated isolate
      onForeground: onStart,

      // you have to enable background fetch capability on xcode project
      onBackground: onIosBackground,
    ),
  );
  service.startService();
}*/
/**
 * Beckground Serivce
 */
/*void showNotification(v, flp) async {
  var android = AndroidNotificationDetails(
      'channel id', 'channel NAME');
  var iOS = IOSNotificationDetails();
  var platform = NotificationDetails(android: android, iOS: iOS);
  await flp.show(0, 'Virtual intelligent solution', '$v', platform,
      payload: 'VIS \n $v');
}*/

/*@pragma('vm:entry-point')
Future<bool> onIosBackground(ServiceInstance service) async {
  WidgetsFlutterBinding.ensureInitialized();
  DartPluginRegistrant.ensureInitialized();

  SharedPreferences preferences = await SharedPreferences.getInstance();
  await preferences.reload();
  final log = preferences.getStringList('log') ?? <String>[];
  log.add(DateTime.now().toIso8601String());
  await preferences.setStringList('log', log);
  return true;
}*/
/*Future<void> onStart(ServiceInstance service) async {
  DartPluginRegistrant.ensureInitialized();
  DartPluginRegistrant.ensureInitialized();if (service is AndroidServiceInstance) {
    service.on('setAsForeground').listen((event) {
      service.setAsForegroundService();
    });service.on('setAsBackground').listen((event) {
      service.setAsBackgroundService();
    });
  }service.on('stopService').listen((event) {
    service.stopSelf();
  });
    initNotifs().then((value){
    print ("seviceNotif Works");
  });
}*/

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  //await initializeService();
  await Firebase.initializeApp();
/*  try {
    await FirebaseMessaging.instance.getToken().then((firebaseToken) async {
      print(" testtttttttt  firebse token ${firebaseToken}");
      if (firebaseToken != "") {
        try {
          var response = await Dio().post(
              'http://192.168.0.2:4000/api/save-token?token=' +
                  firebaseToken!);
          print(response);
        } catch (e) {
          print("Error dans le Dio token firebase ${e}");
        }
      }
      if (firebaseToken != "") {
        final uri = Uri.parse(
            'http://192.168.0.2:4000/api/save-token?token=' + firebaseToken!);
        //final headers = {'Content-Type': 'application/json'};//192.168.31.238
        var response = await post(
          uri,
          // headers: headers,
        );
        int statusCode = response.statusCode;
        String responseBody = response.body;
        print(
            'Token firebse send to express js ${statusCode} +++ ${responseBody}');
      }
    });
  } catch (e) {
    print("exception here $e");
  }*/

  runApp(MyApp());
  if (Platform.isAndroid || Platform.isIOS) {
    await Firebase.initializeApp();
    FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);

    await FirebaseMessaging.instance.setForegroundNotificationPresentationOptions(
      alert: true,
      badge: true,
      sound: true,
    );
  }
  await Hive.initFlutter();
  await SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);
  FlutterError.onError = FirebaseCrashlytics.instance.recordFlutterFatalError;
}
/*Future<void> fetchCryptoNotif(List<Crypto> cryptos,  List<SearchCrypto> data) async {
  String symbols = "";
  cryptos.forEach((element) async {
    if (element.id != null && element.id != "") {
      var variationUser = element.variation ?? 0.0;
      var variationLive = data.firstWhereOrNull((e) => e.name?.toLowerCase() == element.name?.toLowerCase() || e.symbol?.toLowerCase() == element.symbol?.toLowerCase()  )?.changePourcent ?? 0.0;
      print("dataaaaaaaa"+ (variationLive.abs() - variationUser.abs()).toString());
      if ((variationLive.abs() - variationUser.abs()) > 5 || (variationLive.abs() - variationUser.abs()) < -5){
        var symbolToAdd = element.name ?? "";
        symbols= symbols + symbolToAdd+ " ";
      }
    }
  });
  if (symbols != ""){
    servicenotif.shownotification(id: 3, title: 'IHave ', body: '$symbols have more than 5% variation');
  }
}*/

class MyApp extends StatefulWidget {
  bool isLoggedIn = true;

  static GlobalKey<NavigatorState> materialKey = GlobalKey<NavigatorState>();

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String _firebaseToken = "";
  APIService apiService = LoginController.apiService;
  final cron = Cron();

  pushnotif() async {
    await FirebaseMessaging.instance.getToken().then((firebaseToken) async {
      setState(() {
        _firebaseToken = firebaseToken ?? "";
      });
      if (_firebaseToken != "") {
        String token = "";
        SharedPreferences prefs = await SharedPreferences.getInstance();

        token = prefs.get("access_token").toString();
        if (token != "") {
          await apiService.pushNotif(token, _firebaseToken, "mobile");
        }
      }
    });
  }

  getDeviceTocken() async {
    String? tokdev = await FirebaseMessaging.instance.getToken();
  }

  @override
  initState() {
    super.initState();

    /*servicenotif = LocalNotification();
    servicenotif.intialize();*/
/*    initNotifs().then((value){
      print ("seviceNotif Works");
    });*/
    FirebaseMessaging.onMessage.listen((RemoteMessage message) async {
      RemoteNotification? notification = message.notification;
      AndroidNotification? android = message.notification?.android;
      if (notification != null && android != null) {
        flutterLocalNotificationsPlugin.show(
            notification.hashCode,
            notification.title,
            notification.body,
            NotificationDetails(
              android: AndroidNotificationDetails(
                channel.id,
                channel.name,
                color: Colors.blue,
                playSound: true,
                icon: '@mipmap/ic_launcher',
              ),
            ));
      }
      FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);
      FirebaseMessaging.instance.getInitialMessage().then((RemoteMessage? message) {
        if (notification != null && android != null) {
          flutterLocalNotificationsPlugin.show(
              notification.hashCode,
              notification.title,
              notification.body,
              NotificationDetails(
                android: AndroidNotificationDetails(
                  channel.id,
                  channel.name,
                  color: Colors.blue,
                  playSound: true,
                  icon: '@mipmap/ic_launcher',
                ),
              ));
        }
        if (message != null) {}
      });

      //lorsque l'application ouvert
      FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
        RemoteNotification? notification = message.notification;
        AndroidNotification? android = message.notification?.android;
        if (notification != null && android != null) {}
      });
      //lorsque l'application fermer
      FirebaseMessaging.instance.getInitialMessage().then((RemoteMessage? message) {
        print('A new onMessageOpenedApp event was published!');
        if (message != null) {
          print("Notif App closed");
          //json.encode(message.data);

        }
      });
    });
  }

// Future initNotifs() async{
//     await launchServiceNotif();
//     return "done";
// }
// Future<String> showNotif(int i, String cryptoName, double price) async{
//   await servicenotif.shownotification(id: i, title: 'Watchlist', body: '${cryptoName ?? ""} have more than 5% variation in the last hour, new price : ${formatDollarBalance(price)}');
//   return "done";
// }
// launchServiceNotif(){
//   Timer.periodic(
//       Duration(
//         minutes: 10,
//       ), (timer) {
//
//
//     SharedPreferences.getInstance().then((sharedPrefValue) async {
//
//       token = sharedPrefValue.getString('access_token');
//       final SharedPreferences prefs = await SharedPreferences.getInstance();
//       if(token!= null && token != "") {
//         var cryptos = (json.decode(sharedPrefValue.getString("user-list") ??"") as List ).map((i) => Crypto.fromJson(i))
//             .toList() ?? [];
//         LoginController.apiService.cryptosToAdd("").then((value){
//           List<SearchCrypto> data = value;
//           String symbols = "";
//           var i = 0;
//           cryptos.forEach((element) async {
//             if (element.id != null && element.id != "") {
//               var variationUser = element.variation ?? 0.0;
//               var variationLive = data.firstWhereOrNull((e) => e.name?.toLowerCase() == element.name?.toLowerCase() || e.symbol?.toLowerCase() == element.symbol?.toLowerCase()  )?.changePourcent ?? 0.0;
//               if (variationUser > 5 || variationUser < -5){
//                 print('${element.name ?? ""} have more than 5% variation in the last hour, new price : (\$${element.price})');
//                 var cryptoName = element.symbol ?? "";
//                 var price = element.price ?? 0.00;
//                 if (!cryptoName.toLowerCase().contains("sattbep20") && !cryptoName.toLowerCase().contains("wsatt")){
//                   showNotif(i, cryptoName, price).then((value){
//                     print (value);
//                   });
//                   i++;
//                 }
//                 /* flutterLocalNotificationsPlugin.show(
//                     notificationId,
//                     'iHave',
//                     '${cryptoName ?? ""} have more than 5% variation in the last hour',
//                     const NotificationDetails(
//                       android: AndroidNotificationDetails(
//                         notificationChannelId,
//                         'iHave',
//                         icon: '@mipmap/ic_launcher',
//                         ongoing: true,
//                         playSound: true,
//                         sound: UriAndroidNotificationSound("assets/tunes/pop.mp3"),
//                         //sound: RawResourceAndroidNotificationSound("assets/tunes/pop.mp3"),
//                         //sound: RawResourceAndroidNotificationSound("@raw/alarm"),
//                       ),
//
//                     ),
//                   );*/
//               }
//             }
//           });
//           /* if (symbols != ""){
//
//
//               }*/
//         });
//       }else{
//         print("Please connect to your iHave app" );
//
//         /*await servicenotif.shownotification(id: 0, title: 'IHave', body: 'Please connect to your iHave app');*/
//
//       }
//
//     });
//
//   });
// }
  AppUpdateInfo? _updateInfo;

  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey();

  bool _flexibleUpdateAvailable = false;

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> checkForUpdate() async {
    InAppUpdate.checkForUpdate().then((info) {
      setState(() {
        _updateInfo = info;
      });
    }).catchError((e) {
      showSnack(e.toString());
    });
  }

  void showSnack(String text) {
    if (_scaffoldKey.currentContext != null) {
      ScaffoldMessenger.of(_scaffoldKey.currentContext!).showSnackBar(SnackBar(content: Text(text)));
    }
  }

  @override
  Widget build(BuildContext context) {
    setState(() {
      LoginController.walletConnectService = new WCBGService(context, "");
    });
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return MultiProvider(
      //key: _scaffoldKey,
      providers: [
        ChangeNotifierProvider(
          create: (context) => LoginController(),
          child: Signin(),
        ),
        ChangeNotifierProvider(
          create: (context) => WalletController(),
          child: Dashboard(),
        )
      ],
      child: MaterialApp(
        navigatorKey: MyApp.materialKey,
        theme: MyTheme.iHaveTheme,
        builder: (context, widget) => ResponsiveWrapper.builder(
          OneContext().builder(context, widget),
          maxWidth: 2460,
          minWidth: 360,
          defaultScale: true,
          breakpoints: [
            const ResponsiveBreakpoint.resize(360, name: MOBILE),
            const ResponsiveBreakpoint.autoScale(1000, name: TABLET),
            const ResponsiveBreakpoint.resize(1900, name: DESKTOP),
            const ResponsiveBreakpoint.autoScale(1900, name: DESKTOP),
            const ResponsiveBreakpoint.autoScale(2460, name: "4K"),
          ],
        ),
        debugShowCheckedModeBanner: false,
        initialRoute: '/',
        routes: {
          '/': (context) => SplachScreen(),
          '/signin': (context) => Signin(),
          '/signup': (context) => Signup(),
          '/dashboard': (context) => Dashboard(),
          '/addToken': (context) => AddToken(),
          '/resetpassword': (context) => ResetPassword(),
          '/createwallet': (context) => CreateWallet(),
          '/welcomescreen': (context) => WelcomeScreen(),
          '/socialmail': (context) => SocialMail(),
          '/transactionpassword': (context) => TransactionPassword(),
          '/buy': (context) => BuyPage(),
          '/walletconnect': (context) => WalletConnect(),
          '/passphrase': (context) => PassPhrase(),
          '/towfactorPage': (context) => TowFactorPage(),
          '/activationmail': (context) => ActivationMail(),
          '/profilepage': (context) => ProfilePage(),
          '/blockchainkey': (context) => BlockchainKey(),
          '/successwallet': (context) => SuccessWallet(),
          '/settings': (context) => Setting(),
          '/qrcode': (context) => QrCodeScan(),
          '/kyc': (context) => KycPage(),
          '/confirmDeactivation': (context) => ConfirmAccountDesactivation(),
          '/exportkey': (context) => ExportKey(),
          '/desactivation': (context) => DesactivateAccount(),
          '/biometrics': (context) => Biometrics(),
          '/security': (context) => SecurityPage(),
          '/cryptodetails': (context) => CryptoDetails(),
          '/changePassword': (context) => ChangePassword(),
          '/notificationPage': (context) => NotificationPage(),
          '/myWallet': (context) => MyWallet(),
        },
      ),
    );
  }
}
