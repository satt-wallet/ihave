class UserDetails {
  String? username;
  String? email;
  String? picLink;
  String? userToken;
  String? idSn;
  String? firstName;
  String? name;
  String? id;
  String? address;
  String? birthday;
  String? city;
  String? zipCode;
  String? country;
  String? gender;
  String? phone;
  String? lastName;
  String? locale;
  bool? hasbiometrics;

  UserDetails(
      {this.username,
      this.email,
      this.picLink,
      this.userToken,
      this.idSn,
      this.firstName,
      this.lastName,
      this.name,
      this.id,
      this.address,
      this.birthday,
      this.city,
      this.country,
      this.gender,
      this.phone,
      this.zipCode,this.locale, this.hasbiometrics});

  UserDetails.fromJson(Map<String, dynamic> json) {
    username = json["username"] ?? "";
    email = json["email"] ?? "";
    picLink = json["picLink"] ?? "";
    userToken = json["userToken"] ?? "";
    idSn = json["idSn"].toString() ;
    firstName = json["firstName"] ?? "";
    name = json["name"] ?? "";
    id = json["id"] ?? "";
    address = json["address"] ?? "";
    birthday = json["birthday"] ?? "";
    city = json["city"] ?? "";
    zipCode = json["zipCode"] ?? "";

    country = json["country"] ?? "";
    print(json["phone"]);
    gender = formatGender(json["gender"]) ?? "" ;
    phone = json["phone"] != null && json["phone"] != "" ? json["phone"].runtimeType != String ? json["phone"]['internationalNumber']?.toString() : json["phone"] ?? "" : json["phone"];
    lastName = json["lastName"] ?? "";
    locale = json["locale"] ?? "";
    hasbiometrics = json["hasBiometrics"];
  }

  Map<String, dynamic> toJson() {
    var phoneObject = {
      'internationalNumber': this.phone ?? ""
    };
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data["username"] = this.username ?? "";
    data["email"] = this.email ?? "";
    data["photoUrl"] = this.picLink ?? "";
    data["userToken"] = this.userToken ?? "";
    data["idSn"] = this.idSn ?? "";
    data["firstName"] = this.firstName ?? "";
    data["name"] = this.name ?? "";
    data["id"] = this.id ?? "";


    data["address"] = this.address ?? "";
    data["birthday"] = this.birthday ?? "";
    data["city"] = this.city ?? "";
    data["zipCode"] = this.zipCode ?? "";
    data["country"] = this.country ?? "";
    data["gender"] = this.gender ?? "";
    data["phone"] = phoneObject;
    data["lastName"] = this.lastName ?? "";
    data["locale"] = this.locale ?? "";
    data["hasBiometrics"] = this.hasbiometrics;
    return data;
  }

  String? formatGender(json) {
    if(json == "Profil.campaign_list.genre_masculin")
      {
return "Man";
      }
    else if(json == "Profil.campaign_list.genre_feminin"){
 return "Woman";
    }
    return "Not Specified";
  }
}
