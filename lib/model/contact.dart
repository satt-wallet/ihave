class Contact {

   int? id;
  String? name;
  String? address;

  Contact({
    this.id,
    this.name,
    this.address,
  });
  Contact.fromJson(Map<dynamic, dynamic> json) {
    id = json["id"];
    name = json["name"] != null && json["name"] != "" ? json["name"] :" ";
    address = json["address"] ?? " ";
  }
}
class Login {

  int? id;
  String? email;
  String? password;

  Login({
    this.id,
    this.email,
    this.password,
  });
  Login.fromJson(Map<dynamic, dynamic> json) {
    id = json["id"];
    email = json["email"];
    password = json["password"];
  }
}