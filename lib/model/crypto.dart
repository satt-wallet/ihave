import 'package:decimal/decimal.dart';

class Crypto {
  String? picUrl;
  String? symbol;
  String? name;
  bool? addedToken;
  String? network;
  String? undername;
  String? undername2;
  double? price;
  int? decimal;
  double? total_balance;
  double? variation;
  String? quantity;
  String? contract;
  int? listOrder;
  String? id;

  Crypto(
      {this.picUrl,
      this.symbol,
      this.name,
      this.addedToken,
      this.network,
      this.undername,
      this.undername2,
      this.price,
      this.total_balance,
      this.variation,
      this.quantity,
      this.contract,
      this.decimal,
      this.id
      });

  Crypto.fromJson(Map<String, dynamic> json) {
      picUrl = json['picUrl'].toString()?? "";
      symbol = json['symbol'] ?? "";
      name = json['name']?? "";
      addedToken =  json['AddedToken'] != null && json['AddedToken'].toString() != 'false';
      network = json['network']?? "";
      undername = json['undername']?? "";
      undername2 = json['undername2']?? "";
      price = json['price'].runtimeType == String ? Decimal.parse(json['price']).toDouble() : json['price'].toDouble();
      total_balance = json['total_balance'].toDouble();
      variation =json['variation']?? 0.0;
      quantity = json['quantity'] != "-" ?  json['quantity'].toString() : "0";
      contract = json['contract']?? "";
      decimal =json['decimal'] != null ? (json['decimal'].runtimeType == int ? json["decimal"] : int.parse(json['decimal'])) : null;
      listOrder = attributeOrder(json['symbol'], json["network"]);
      id = json["id"] ?? "";
  }
  Map<String, dynamic> toJson(Crypto crypto) =>
    {
    'picUrl': crypto.picUrl,
    'symbol' :  crypto.symbol,
    'name':crypto.name,
    'addedToken':crypto.addedToken,
    'network': crypto.network,
    'undername':crypto.undername,
    'undername2':crypto.undername2,
     'price':crypto.price,
    'total_balance':crypto.total_balance,
    'variation':crypto.variation,
     'quantity':crypto.quantity,
    'contract':crypto.contract,
      'decimal' :crypto.decimal ,
      'id': crypto.id
    };
  int? attributeOrder(json,json2) {
    if (json == 'SATT') {
      this.listOrder = 1;
    } else if (json == 'SATTBEP20') {
      this.listOrder = 2;
    } else if (json == 'WSATT') {
      this.listOrder = 3;
    } else if (json == 'BNB') {
      this.listOrder = 4;
    } else if (json == 'ETH') {
      this.listOrder = 5;
    } else if (json == 'BTC') {
      this.listOrder = 6;
    } else if (json == 'TRON') {
      this.listOrder = 7;
    }else if (json == 'BTT') {
      this.listOrder = 8;
    }else if (json == 'MATIC') {
      this.listOrder = 9;
    }else if (json2 == 'BEP20') {
      this.listOrder = 10;
    }else if (json2 == 'ERC20') {
      this.listOrder = 11;
    }
    else {
      this.listOrder = 12;
    }
    return listOrder;
  }
  String toStringName(String name){
    return this.name.toString()??"";
  }
  String toStringid(String id){
    return this.id.toString() ??"";
  }
  double? toStringvariation(double variation){
    return this.variation;
  }
}
