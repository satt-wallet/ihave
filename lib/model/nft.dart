import 'dart:convert';

class NFT {
  String? id;
  String? image;
  String? animation;
  String? name;
  String? permalink;
  String? collection;

  NFT(
      {this.id,
        this.image,
        this.animation,
        this.name,
        this.permalink,
        this.collection,
      });

  NFT.fromJson(Map<String, dynamic> jsonObject) {
    id = jsonObject['token_id'].toString() ?? "";
    image = jsonObject['metadata']!= null ? json.decode(jsonObject['metadata'])['image'] ?? "" : "";
    animation = jsonObject['metadata']!= null ?json.decode(jsonObject['metadata'])['animation_url'] ?? "":"";
    name =  jsonObject['name']?? "";
    permalink = jsonObject['token_uri']?? "";
    collection =  jsonObject['metadata']!= null ? json.decode(jsonObject['metadata'])['description']?? "" :"";
  }
}
