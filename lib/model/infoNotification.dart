class InfoNotification {
  String? name;
  String? amount;
  String? currency;
  String? to;
  String? from;

  InfoNotification({
    this.name,
    this.amount,
    this.currency,
    this.to,
    this.from,
  });

  InfoNotification.fromJson(Map<String, dynamic> json) {
    name = json["name"] ?? "";
    amount = json["amount"] ?? json["price"].toString() ?? "";
    currency = json["cryptoCurrency"] != null ? json["cryptoCurrency"] : json["currency"] ?? "";
    if (currency!.toLowerCase().contains("satt")) {
      currency = "SATT";
    }
    to = json["to"] ?? "";
    from = json["from"] ?? "";
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data["name"] = this.name ?? "";
    data["amount"] = this.amount ?? "";
    data["cryptoCurrency"] = this.currency ?? "";
    data["to"] = this.to ?? "";
    data["from"] = this.from ?? "";

    return data;
  }
}
