class Operations {
  String? blockHash;
  double? blockNumber;
  double? confirmations;
  String? contractAddress;
  double? cumulativeGasUsed;
  String? from;
  double? gas;
  double? gasPrice;
  double? gasUsed;
  String? hash;
  String? input;
  String? network;
  double? nonce;
  DateTime? timeStamp;
  String? to;
  double? tokenDecimal;
  String? tokenName;
  String? tokenSymbol;
  double? transactionIndex;
  double? value;

  Operations(
      {this.blockHash,
      this.blockNumber,
      this.confirmations,
      this.contractAddress,
      this.cumulativeGasUsed,
      this.from,
      this.gas,
      this.gasPrice,
      this.gasUsed,
      this.hash,
      this.input,
      this.network,
      this.nonce,
      this.timeStamp,
      this.to,
      this.tokenDecimal,
      this.tokenName,
      this.tokenSymbol,
      this.transactionIndex,
      this.value});

  Operations.fromJson(Map<String, dynamic> json) {
    blockHash = json['blockHash'].toString();
    blockNumber = json['blockNumber'];
    confirmations = json['confirmations'];
    contractAddress = json['contractAddress'];
    cumulativeGasUsed = json['cumulativeGasUsed'];
    from = json['from'];
    gas = json['gas'];
    gasPrice = json['gasPrice'];
    gasUsed = json['gasUsed'];
    hash = json['hash'];
    input = json['input'];
    network = json['network'];

    nonce = json['nonce'];
    timeStamp = new DateTime.fromMillisecondsSinceEpoch(json['timeStamp'] * 1000);
    to = json['to'];
    tokenDecimal = json['tokenDecimal'];
    tokenName = json['tokenName'];
    tokenSymbol = json['tokenSymbol'];
    transactionIndex = json['transactionIndex'];
    value = json['value'];
  }
}
