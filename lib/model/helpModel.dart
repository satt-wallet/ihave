class Helps {
  String? question;
  String? answer;

  Helps({
    this.question,
    this.answer,
  });

  static List<Helps> helpWallet = [
    Helps(
        question: 'What exactly is a SaTT ? ',
        answer: 'SaTT is a digital advertising token '
            'that facilitates smoother transactions between content creators and advertisers. Ultimately, SaTT facilitates faster, cheaper, and more secure digital advertising purchases. Of note, SaTT is available on both the Ethereum blockchain ERC20 and Binance Smart Chain BEP20 '),
    Helps(
        question: 'How to buy SaTT?',
        answer:
            'Buy SaTT ERC20 and BEP20 with your credit card or Apple Pay on SaTT.io and Simplex \nBuy SaTT ERC20 on Bittrex, Probit, HitBTC, Bitcoin.com, Digifinex\nBuy Wrapped SaTT ERC20 (WSATT) on Uniswap - Contract: 0x70A6395650b47D94A77dE4cFEDF9629f6922e645. Uniswap tutorial\nBuy SaTT BEP20 on Pancakeswap - Contract: 0x448bee2d93be708b54ee6353a7cc35c4933f1156. Pancakeswap tutorial\nBridge SATT ERC20 → SATT BEP20 : 0x655371C0622cACc22732E872a68034f38E04d6e5. Bridge tutorial'),
    Helps(
        question: 'How do I configure SaTT to the crypto\nwallets MetaMask or Trust Wallet?',
        answer: 'Whether you’re trying to configure MetaMask or Trust Wallet, we recommend reviewing the following resources:MetaMask Registration   MetaMask ConfigurationTrust Wallet Registration   Trust Wallet ConfigurationLikewise, you can learn how to use the ERC-20 to BEP-20 bridge Here'),
    Helps(question: 'What can I purchase using SaTT?', answer: 'For advertisers seeking accelerated growth on Facebook, Iframe Apps offers a series of eight SaTT-eligible apps, enabling access to advertising products and services rooted in a network of vetted partners. '),
    Helps(question: 'Where can I store my SaTT after\nmy purchase?', answer: 'SaTT can be stored in a variety of cryptocurrency wallets, including our native SaTT wallet, Ledger wallet, MetaMask wallet, Ethereum wallet, or Binance Smart Chain wallet. '),
  ];

  static List<Helps> helpAccount = [
    Helps(question: 'How can I log in without Facebook\nConnect if I deleted my Facebook account?', answer: 'For assistance after a Facebook account deletion, please email contact@satt-token.com and have your account information ready, including the email you use to access Facebook. '),
  ];
  static List<Helps> helpTransaction = [
    Helps(
        question: 'How can I retrieve my SaTT password? ',
        answer:
            'Your wallet’s password is retrievable by simply selecting the ‘forgot password’ link. From there, follow the instructions to select a new password. If you lose your blockchain password, however, there is no method of retrieval. This password enables you to send and receive SaTT. As a result, the password is inaccessible, even to our SaTT technical support team. We highly recommend writing down your blockchain password in a secure location where you won’t lose it. In turn, you can avoid a potentially costly situation.'),
  ];
}

