class ApprovedContract{
  String? hash;
  String? contract;
  String? spender;
  String? amount;
  String? network;
  String? symbol;
  String? name;
  ApprovedContract({
    this.hash,
    this.contract,
    this.spender,
    this.amount,
    this.network,
    this.symbol,
    this.name,
});
  ApprovedContract.fromJson(Map<dynamic, dynamic> json) {
    hash = json["hash"] ?? "";
    contract = json["token"] ?? "";
    spender = json["spender"] ?? "";
    amount = json["amount"] ?? "";
    network = json["network"] ?? "";
    symbol = json["symbol"] ?? "";
    name = json["name"] ?? "";
  }
}