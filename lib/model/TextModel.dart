class TextModel {
  int id;
  String text;

  TextModel({required this.id, required this.text});

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'text': text,
    };
  }

  factory TextModel.fromMap(Map<String, dynamic> map) {
    return TextModel(
      id: map['id'],
      text: map['text'],
    );
  }
}