import 'package:ihave/model/attached.dart';
import 'package:ihave/model/infoNotification.dart';

class Notifications {
  String? id;
  String? idNode;
  String? type;
  String? status;
  InfoNotification? notification;
  Attached? attached;
  bool? isSeen;
  bool? isSend;
  String? created;
  Notifications({
    this.id,
    this.idNode,
    this.type,
    this.attached,
    this.notification,
    this.status,
    this.isSeen,
    this.isSend,
  });

  Notifications.fromJson(Map<String, dynamic> json) {
    if (json["label"].runtimeType != String) {
      id = json["_id"] ?? "";
      idNode = json["idNode"] ?? "";
      type = json["type"] ?? "";
      attached = (Attached.fromJson(json["attachedEls"]));
      print("label ${json["label"]}");
      notification = (InfoNotification.fromJson(json["label"]));
      print("notif: $notification");
      status = json["status"] ?? "";
      isSeen = json["isSeen"] ?? "";
      isSend = json["isSend"] ?? "";
      created = json["createdAt"] != null ? json["createdAt"]  : json["created"] ;
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data["_id"] = this.id ?? "";
    data["idNode"] = this.idNode ?? "";
    data["type"] = this.type ?? "";
    data["status"] = this.status ?? "";
    data["attachedEls"] = this.attached;
    data["label"] = this.notification;
    data["isSeen"] = this.isSeen;
    data["isSend"] = this.isSend;
    data["created"] = this.created ?? "";
    return data;
  }

}
