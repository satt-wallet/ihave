
class SearchCrypto {
  String? logo;
  String? name;
  String? network;
  double? price;
  int? decimal;
  String? contract;
  String? symbol;
  double? changePourcent;

  SearchCrypto(
      {this.logo,
        this.name,
        this.symbol,
        this.network,
        this.price,
        this.contract,
        this.decimal,
        this.changePourcent
      });

  SearchCrypto.fromJson(Map<String, dynamic> json) {
    logo = json['logo'].toString();
    symbol = "";
    name = json['name'];
    network = json['network'] ?? "";
    price = json['price'].toDouble();
    contract = json['tokenAddress'];
    decimal = json['decimals'];
    changePourcent = json["percent_change_24h"];
  }
}
