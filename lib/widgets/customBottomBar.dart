import 'package:convex_bottom_bar/convex_bottom_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:ihave/controllers/walletController.dart';
import 'package:ihave/ui/WelcomeScreen.dart';
import 'package:ihave/util/utils.dart';
import 'package:one_context/one_context.dart';
import 'package:provider/provider.dart';

class CustomBottomBar extends StatefulWidget {
  const CustomBottomBar({Key? key}) : super(key: key);

  @override
  _CustomBottomBarState createState() => _CustomBottomBarState();
}

class _CustomBottomBarState extends State<CustomBottomBar> {
  @override
  Widget build(BuildContext context) {
    return StyleProvider(
      style: Style(),
      child: ConvexAppBar(
        top: -(MediaQuery.of(context).size.width * 0.045),
        height: MediaQuery.of(context).size.width * 0.158,
        curveSize: MediaQuery.of(context).size.width * 0.14,
        disableDefaultTabController: false,
        elevation: 8,
        backgroundColor: Colors.white,
        style: TabStyle.fixed,
        items: [
          TabItem(
            icon: Padding(
              padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
              child: Column(
                children: [
                  Image.asset(
                    'images/navigateur.png',
                    height: MediaQuery.of(context).size.width * 0.055,
                    width: MediaQuery.of(context).size.width * 0.055,
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0, 1, 0, 0),
                    child: Text(
                      "Browser",
                      style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.027, color: Color(0xFF4048FF)),
                    ),
                  ),
                ],
              ),
            ),
          ),
          TabItem(
            icon: Padding(
              padding: const EdgeInsets.fromLTRB(0, 1, 0, 0),
              child: Column(
                children: [
                  SvgPicture.asset(
                    'images/send-icon.svg',
                    height: MediaQuery.of(context).size.width * 0.055,
                    width: MediaQuery.of(context).size.width * 0.055,
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0, 1, 0, 0),
                    child: Text(
                      "Send",
                      style: GoogleFonts.poppins(
                        fontSize: MediaQuery.of(context).size.width * 0.027,
                        color: Color(0xFF4048FF),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          TabItem(
            icon: Padding(
              padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
              child: Column(
                children: [
                  SizedBox(
                    height: MediaQuery.of(context).size.width * 0.13,
                    width: MediaQuery.of(context).size.width * 0.13,
                    child: CircleAvatar(
                      backgroundColor: Color(0xFF4048FF),
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(6, 0, 0, 3),
                        child: SvgPicture.asset(
                          'images/walleticone.svg',
                          height: MediaQuery.of(context).size.width * 0.1,
                          width: MediaQuery.of(context).size.width * 0.1,
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0, 3, 0, 0),
                    child: Text(
                      "Wallet",
                      style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.027, color: Color(0xFF4048FF)),
                    ),
                  ),
                ],
              ),
            ),
          ),
          TabItem(
            icon: Padding(
              padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
              child: Column(
                children: [
                  SvgPicture.asset(
                    'images/request-icon.svg',
                    height: MediaQuery.of(context).size.width * 0.055,
                    width: MediaQuery.of(context).size.width * 0.055,
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0, 1, 0, 0),
                    child: Text(
                      "Request",
                      style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.025, color: Color(0xFF4048FF)),
                    ),
                  ),
                ],
              ),
            ),
          ),
          TabItem(
            icon: Padding(
              padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
              child: Column(
                children: [
                  SvgPicture.asset(
                    'images/nft.svg',
                    height: MediaQuery.of(context).size.width * 0.055,
                    width: MediaQuery.of(context).size.width * 0.055,
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0, 1, 0, 0),
                    child: Text(
                      "NFT",
                      style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.026, fontWeight: FontWeight.w600, color: Color(0xFF4048FF)),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
        onTap: (int i) {
          Provider.of<WalletController>(context, listen: false).selectedIndex = i;
          Provider.of<WalletController>(context, listen: false).walletConnectScreen = false;
          Provider.of<WalletController>(context, listen: false).toggleWelcomeScreen(true);
          gotoPageGlobal(context, WelcomeScreen());
        },
      ),
    );
  }
}

class Style extends StyleHook {
  @override
  double get activeIconSize => OneContext().mediaQuery.size.width * 0.19;

  @override
  double get activeIconMargin => OneContext().mediaQuery.size.width * 0.02;

  @override
  double get iconSize => OneContext().mediaQuery.size.width * 0.12;

  @override
  TextStyle textStyle(Color color, String? s) {
    return TextStyle(fontSize: 0, color: color);
  }
}
