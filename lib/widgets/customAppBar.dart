import 'dart:io';
import 'dart:math';
import 'dart:typed_data';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart';
import 'package:ihave/controllers/LoginController.dart';
import 'package:ihave/controllers/walletController.dart';
import 'package:ihave/ui/Profilepage.dart';
import 'package:ihave/ui/myprofile.dart';
import 'package:ihave/ui/settings.dart';
import 'package:ihave/util/utils.dart';
import 'package:lottie/lottie.dart';
import 'package:majascan/majascan.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:permission_handler_platform_interface/permission_handler_platform_interface.dart';
import 'package:provider/provider.dart';
import 'package:socket_io_client/socket_io_client.dart' as IO;
import 'package:socket_io_client/socket_io_client.dart';

class CustomAppBar extends StatefulWidget implements PreferredSizeWidget {
  const CustomAppBar({Key? key}) : super(key: key);

  @override
  _CustomAppBarState createState() => _CustomAppBarState();

  @override
  Size get preferredSize => new Size.fromHeight(65.0);
}

class _CustomAppBarState extends State<CustomAppBar> with SingleTickerProviderStateMixin {
  late AnimationController lottieconroller;
  late IO.Socket socket;
  Uint8List? userPicture;
  Uint8List? userPictureTake;
  Uint8List? picture;
  bool _userPictureExist = false;
  bool _userPictureSocialExist = false;
  String userPictureSocial = "";
  bool _isLoadingUserPicture = true;
  ImageProvider? profileImage;
  bool isSocialPicAvailable = false;

  getUserPicture() async {
    setState(() {
      _isLoadingUserPicture = true;
    });
    if (Provider.of<LoginController>(context, listen: false).userPicture == null) {
      var pic = await LoginController.apiService.getUserPicture(Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "");
      Provider.of<LoginController>(context, listen: false).userPicture = pic != "error" ? pic : null;
      var picture = Provider.of<LoginController>(context, listen: false).userPicture;
      if (picture.toString().length > 112 && picture != null) {
        setState(() {
          userPicture = picture;
          userPictureSocial = "";
          _userPictureSocialExist = false;
          _userPictureExist = true;
        });
      } else {
        getUser().then((value) {
          if (value.picLink != null && value.picLink != "" && value.picLink != "AppleIDAuthorizationScopes.fullName") {
            setState(() {
              userPictureSocial = value.picLink;
              _userPictureExist = false;
              _userPictureSocialExist = true;
            });
            LoginController.apiService.getSocialPicture(userPictureSocial).then((value) {
              setState(() {
                isSocialPicAvailable = value == "success";
              });
            });
          } else {
            setState(() {
              _userPictureExist = false;
              _userPictureSocialExist = false;
            });
          }
        });
        setState(() {
          _isLoadingUserPicture = false;
        });
      }
    } else {
      var picture = Provider.of<LoginController>(context, listen: false).userPicture;
      if (picture.toString().length > 112 && picture != null) {
        setState(() {
          userPicture = picture;
          userPictureSocial = "";
          _userPictureSocialExist = false;
          _userPictureExist = true;
        });
      } else {
        getUser().then((value) {
          if (value.picLink != null && value.picLink != "") {
            setState(() {
              userPictureSocial = value.picLink;
              _userPictureExist = false;
              _userPictureSocialExist = true;
            });
            LoginController.apiService.getSocialPicture(userPictureSocial).then((value) {
              setState(() {
                isSocialPicAvailable = value == "success";
              });
            });
          } else {
            setState(() {
              _userPictureExist = false;
              _userPictureSocialExist = false;
            });
          }
        });
      }
    }
    getProfilePic().then((value) {
      setState(() {
        profileImage = value;
      });
    });
    setState(() {
      _isLoadingUserPicture = false;
    });
  }

  Future getUser() async {
    var user = await Provider.of<LoginController>(context, listen: false).getUserDetails(Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "");
    return user;
  }

  int sourceTarget = Random().nextInt(16000);

  void connect() {
    Socket socket = io('http://10.0.2.2:5000', OptionBuilder().setTransports(['websocket']).build());
    socket.connect();
    socket.emit("signin", sourceTarget);
    socket.onConnect((data) {
      print('socket connect');
      socket.on("message", (msg) async {
        print("message Token from Socket io" + msg["message"]);
        msg["message"] != "" && msg["message"] != null ? showLottie() : print("Token not existe");
      });
    });
    socket.onConnectError((error) => print('Socket Error = ' + error.toString()));
    socket.onDisconnect((data) {
      print("Socket.IO server disconnected");
    });
    print(socket.connected);
  }

  void sendMessage(String message, String sourceEmail, String targetEmail) {
    Socket socket = io('http://10.0.2.2:5000', OptionBuilder().setTransports(['websocket']).build());
    socket.emit("message", {
      "message": message,
      "sourceEmail": sourceEmail,
      "targetEmail": targetEmail,
    });
  }

  void initState() {
    lottieconroller = AnimationController(
      vsync: this,
    );
    lottieconroller.addStatusListener((status) async {
      if (status == AnimationStatus.completed) {
        Navigator.pop(context);
        lottieconroller.reset();
      }
    });

    connect();
    getUserPicture();
    super.initState();
  }

  @override
  void dispose() {
    userPicture = null;
    super.dispose();
    lottieconroller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AppBar(
        elevation: 0,
        leadingWidth: 70,
        backgroundColor: Colors.transparent,
        flexibleSpace: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  colors: [
                    Color(0xFF707070).withOpacity(0.6),
                    Colors.transparent,
                  ],
                  begin: const FractionalOffset(0.0, 0.0),
                  end: const FractionalOffset(0.0, 1.0),
                  stops: [0.0, 1.0],
                  tileMode: TileMode.clamp)),
        ),
        leading: Padding(
          padding: EdgeInsets.fromLTRB(14.4, 0, 0, 0),
          child: Padding(
            padding: EdgeInsets.only(right: 5.4, left: 5.4),
            child: InkWell(
              focusColor: Colors.transparent,
              hoverColor: Colors.transparent,
              splashColor: Colors.transparent,
              highlightColor: Colors.transparent,
              splashFactory: NoSplash.splashFactory,
              onTap: () {
                Provider.of<WalletController>(context, listen: false).toggleProfileVisibility();
                Navigator.of(context).push(new MaterialPageRoute(builder: (context) => Setting()));
              },
              child: (_isLoadingUserPicture)
                  ? Container(
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          gradient: LinearGradient(
                              colors: [
                                Color.fromRGBO(50, 55, 84, 1),
                                Color.fromRGBO(50, 55, 84, 0),
                              ],
                              begin: const FractionalOffset(0.0, 0.0),
                              end: const FractionalOffset(0.0, 1.0),
                              stops: [0.0, 1.0],
                              tileMode: TileMode.clamp)),
                      child: CircleAvatar(
                        radius: 27,
                        backgroundColor: Colors.transparent,
                        child: SizedBox(
                          height: 20,
                          width: 18.96,
                          child: CircularProgressIndicator(
                            color: Colors.white,
                          ),
                        ),
                      ),
                    )
                  : Container(
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          gradient: LinearGradient(
                              colors: [
                                Color.fromRGBO(50, 55, 84, 1),
                                Color.fromRGBO(50, 55, 84, 0),
                              ],
                              begin: const FractionalOffset(0.0, 0.0),
                              end: const FractionalOffset(0.0, 1.0),
                              stops: [0.0, 1.0],
                              tileMode: TileMode.clamp)),
                      child:
                          CircleAvatar(backgroundColor: Colors.transparent, radius: 27, backgroundImage: _userPictureExist ? MemoryImage(userPicture!) : (_userPictureSocialExist && isSocialPicAvailable ? (NetworkImage(userPictureSocial)) : AssetImage("images/avatar-welcome.png") as ImageProvider)),
                    ),
            ),
          ),
        ),
        actions: <Widget>[
          InkWell(
            focusColor: Colors.transparent,
            hoverColor: Colors.transparent,
            splashColor: Colors.transparent,
            highlightColor: Colors.transparent,
            splashFactory: NoSplash.splashFactory,
            onTap: () async {
              if (Platform.isAndroid) {
                var permission = Platform.isAndroid ? Permission.storage : Permission.photos;
                var status_storage = await Permission.storage.status;

                var status = await permission.request();
                if (status == PermissionStatus.granted) {
                  final result = await Permission.camera.request();
                  if (result == PermissionStatus.granted) {
                    FocusScope.of(context).requestFocus(new FocusNode());
                    Future.delayed(Duration(milliseconds: 200), () async {
                      try {
                        String? qrResult = await MajaScan.startScan(title: "QRcode scanner", titleColor: Colors.blue[700], qRCornerColor: Colors.blue, qRScannerColor: Colors.blue);
                        setState(() {
                          String token = Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "";
                          String ihConnectToken = qrResult ?? '';

                          ihConnectToken == "" ? print("Socket didn't have the Source") : sendMessage(token, "${sourceTarget}", ihConnectToken);
                        });
                      } on PlatformException catch (ex) {
                        if (ex.code == MajaScan.CameraAccessDenied) {
                          setState(() {
                            //ihConnectToken = "permission";
                          });
                        } else {
                          setState(() {
                            //ihConnectToken = "error";
                          });
                        }
                      }
                    });
                  } else {
                    showDialog(
                        context: context,
                        builder: (BuildContext context) => CupertinoAlertDialog(
                              title: Text('Camera Permission'),
                              content: Text('This app needs camera access to  scan QR code '),
                              actions: <Widget>[
                                CupertinoDialogAction(
                                  child: Text('Deny'),
                                  onPressed: () => Navigator.of(context).pop(),
                                ),
                                CupertinoDialogAction(
                                  child: Text('Settings'),
                                  onPressed: () => openAppSettings(),
                                ),
                              ],
                            ));
                  }
                }
              }
            },
            child: Container(
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  gradient: LinearGradient(
                      colors: [
                        Color.fromRGBO(50, 55, 84, 1),
                        Color.fromRGBO(50, 55, 84, 0),
                      ],
                      begin: const FractionalOffset(0.0, 0.0),
                      end: const FractionalOffset(0.0, 1.0),
                      stops: [0.0, 1.0],
                      tileMode: TileMode.clamp)),
              child: CircleAvatar(
                radius: 27,
                backgroundColor: Colors.transparent,
                child: Image.asset(
                  "images/Scan.png",
                ),
              ),
            ),
          ),
          SizedBox(
            width: MediaQuery.of(context).size.width * 0.025,
          ),
          InkWell(
            focusColor: Colors.transparent,
            hoverColor: Colors.transparent,
            splashColor: Colors.transparent,
            highlightColor: Colors.transparent,
            splashFactory: NoSplash.splashFactory,
            onTap: () {
              Navigator.pushReplacementNamed(context, '/myWallet');
            },
            child: Container(
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  gradient: LinearGradient(
                      colors: [
                        Color.fromRGBO(50, 55, 84, 1),
                        Color.fromRGBO(50, 55, 84, 0),
                      ],
                      begin: const FractionalOffset(0.0, 0.0),
                      end: const FractionalOffset(0.0, 1.0),
                      stops: [0.0, 1.0],
                      tileMode: TileMode.clamp)),
              child: CircleAvatar(
                radius: 27,
                backgroundColor: Colors.transparent,
                child: SvgPicture.asset(
                  "images/wallet-icon.svg",
                  width: 26,
                  height: 26,
                  color: Color(0xFFF6F6FF),
                ),
              ),
            ),
          ),
          SizedBox(
            width: MediaQuery.of(context).size.width * 0.025,
          ),
          Padding(
            padding: EdgeInsets.only(right: 5.4),
            child: InkWell(
              focusColor: Colors.transparent,
              hoverColor: Colors.transparent,
              splashColor: Colors.transparent,
              highlightColor: Colors.transparent,
              splashFactory: NoSplash.splashFactory,
              onTap: () {
                Provider.of<WalletController>(context, listen: false).selectedIndex = 0;
                Navigator.pushReplacementNamed(context, '/welcomescreen');
              },
              child: Container(
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    gradient: LinearGradient(
                        colors: [
                          Color.fromRGBO(50, 55, 84, 1),
                          Color.fromRGBO(50, 55, 84, 0),
                        ],
                        begin: const FractionalOffset(0.0, 0.0),
                        end: const FractionalOffset(0.0, 1.0),
                        stops: [0.0, 1.0],
                        tileMode: TileMode.clamp)),
                child: CircleAvatar(
                  radius: 27,
                  backgroundColor: Colors.transparent,
                  child: SvgPicture.asset(
                    'images/notif-icon.svg',
                    height: 20,
                    width: 18.96,
                  ),
                ),
              ),
            ),
          )
        ]);
  }

  Future getProfilePic() async {
    if (_userPictureExist) {
      return MemoryImage(userPicture!) as ImageProvider;
    } else if (_userPictureSocialExist) {
      if (await LoginController.apiService.getSocialPicture(userPictureSocial) == "success") {
        return NetworkImage(userPictureSocial) as ImageProvider;
      } else {
        return AssetImage("images/avatar-welcome.png") as ImageProvider;
      }
    }
    return AssetImage("images/avatar-welcome.png") as ImageProvider;
  }

  void showLottie() => showDialog(
      barrierDismissible: false,
      context: context,
      builder: (context) {
        Future.delayed(Duration(seconds: 7), () {
          Navigator.of(context).pop(true);
        });
        return Center(child: Image(image: new AssetImage('images/scan-qr.gif')));
      });
}
