import 'package:flutter/material.dart';

class CustomSliderThumbRect extends SliderComponentShape {
  final double thumbRadius;
  final double thumbHeight;
  final int min;
  final int max;

  const CustomSliderThumbRect({
    required this.thumbRadius,
    required this.thumbHeight,
    required this.min,
    required this.max,
  });

  @override
  Size getPreferredSize(bool isEnabled, bool isDiscrete) {
    return Size.fromRadius(thumbRadius);
  }

  @override
  void paint(
    PaintingContext context,
    Offset center, {
    Animation<double>? activationAnimation,
    Animation<double>? enableAnimation,
    bool? isDiscrete,
    TextPainter? labelPainter,
    RenderBox? parentBox,
    SliderThemeData? sliderTheme,
    TextDirection? textDirection,
    double? value,
    double? textScaleFactor,
    Size? sizeWithOverflow,
  }) {
    final Canvas canvas = context.canvas;

    final rRect = RRect.fromRectAndRadius(
      Rect.fromCenter(center: center, width: thumbHeight * .9, height: thumbHeight * .9),
      Radius.circular(thumbRadius * .01),
    );

    final paint = Paint()..color = sliderTheme!.thumbColor!; //Thumb Background Color
    TextPainter textPainter = new TextPainter(textDirection: TextDirection.rtl);
    final icon = Icons.arrow_forward;
    textPainter.text = TextSpan(text: String.fromCharCode(icon.codePoint), style: TextStyle(fontSize: 30.0, fontFamily: icon.fontFamily, color: Colors.black, fontWeight: FontWeight.bold));

    textPainter.layout();
    Offset textCenter = Offset(center.dx - (textPainter.width / 2), center.dy - (textPainter.height / 2));

    canvas.drawRRect(rRect, paint);
    textPainter.paint(canvas, textCenter);
  }
}

class CustomSliderThumbRectWrong extends SliderComponentShape {
  final double thumbRadius;
  final double thumbHeight;
  final int min;
  final int max;

  const CustomSliderThumbRectWrong({
    required this.thumbRadius,
    required this.thumbHeight,
    required this.min,
    required this.max,
  });

  @override
  Size getPreferredSize(bool isEnabled, bool isDiscrete) {
    return Size.fromRadius(thumbRadius);
  }

  @override
  void paint(
    PaintingContext context,
    Offset center, {
    Animation<double>? activationAnimation,
    Animation<double>? enableAnimation,
    bool? isDiscrete,
    TextPainter? labelPainter,
    RenderBox? parentBox,
    SliderThemeData? sliderTheme,
    TextDirection? textDirection,
    double? value,
    double? textScaleFactor,
    Size? sizeWithOverflow,
  }) {
    final Canvas canvas = context.canvas;

    final rRect = RRect.fromRectAndRadius(
      Rect.fromCenter(center: center, width: thumbHeight * .9, height: thumbHeight * .9),
      Radius.circular(thumbRadius * .01),
    );

    final paint = Paint()..color = sliderTheme!.thumbColor!; //Thumb Background Color

    TextPainter textPainter = new TextPainter(textDirection: TextDirection.rtl);
    final icon = Icons.arrow_forward;
    textPainter.text = TextSpan(text: String.fromCharCode(icon.codePoint), style: TextStyle(fontSize: 30.0, fontFamily: icon.fontFamily, color: Colors.white, fontWeight: FontWeight.bold));

    textPainter.layout();
    Offset textCenter = Offset(center.dx - (textPainter.width / 2), center.dy - (textPainter.height / 2));

    canvas.drawRRect(rRect, paint);
    textPainter.paint(canvas, textCenter);
  }
}
