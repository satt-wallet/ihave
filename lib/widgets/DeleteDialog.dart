import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:ihave/controllers/walletController.dart';
import 'package:ihave/ui/WelcomeScreen.dart';
import 'package:provider/provider.dart';

import '../controllers/LoginController.dart';

void deleteDialog(context, crypto) => showDialog(
    context: context,
    builder: (context) {
      return StatefulBuilder(builder: (context, setState) {
        return AlertDialog(
            insetPadding: EdgeInsets.zero,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(30)),
            ),
            content: Padding(
              padding: const EdgeInsets.all(0),
              child: Container(
                width: MediaQuery.of(context).size.width * 0.6,
                height: MediaQuery.of(context).size.height * 0.5,
                child: Column(
                  children: [
                    Padding(
                      padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.5, top: MediaQuery.of(context).size.width * 0.02),
                      child: IconButton(
                          alignment: Alignment.topLeft,
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                          icon: Icon(
                            Icons.close,
                            color: Colors.grey,
                            size: 35,
                          )),
                    ),
                    Image.asset(
                      "images/other.png",
                    ),
                    Text(
                      "Are you sure ? \n You are about to delete this \n token ",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.035, fontWeight: FontWeight.w700, color: Colors.black),
                    ),
                    ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            minimumSize: Size(260.0, 40.0),
                            side: BorderSide(color: Colors.grey),
                            primary: Color(0xFF4048FF),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(50.0),
                            )),
                        onPressed: () async {
                          await LoginController.apiService.removeToken(Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? '', crypto.contract!);
                          setState(() {
                            Provider.of<WalletController>(context, listen: false).cryptos.remove(crypto);
                          });

                          Navigator.pushReplacement(context, MaterialPageRoute(builder: (_) => WelcomeScreen()));
                        },
                        child: Text(
                          "Delete",
                          style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.047, fontWeight: FontWeight.w700, color: Colors.white),
                        )),
                    ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            minimumSize: Size(260.0, 40.0),
                            side: BorderSide(color: Colors.white),
                            primary: Colors.white,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(50.0),
                            )),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text(
                          "cancel",
                          style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.047, fontWeight: FontWeight.w700, color: Color(0xFF4048FF)),
                        )),
                  ],
                ),
              ),
            ));
      });
    });
