import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:ihave/controllers/walletController.dart';
import 'package:ihave/model/nft.dart';
import 'package:ihave/ui/nftSlider.dart';
import 'package:ihave/util/utils.dart';
import 'package:provider/provider.dart';

class NFTGrid extends StatelessWidget {
  final List<NFT>? nfts;

  NFTGrid({Key? key, this.nfts}) : super(key: key);

  Widget getStructuredGridCell(NFT nft, int index, BuildContext context) {
    return InkWell(
      onTap: () {
        Provider.of<WalletController>(context, listen: false).nft = nft;
        gotoPageGlobal(context, NftSlider());
      },
      child: new Card(
          semanticContainer: true,
          clipBehavior: Clip.antiAliasWithSaveLayer,
          borderOnForeground: true,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(18.0),
          ),
          elevation: 2,
          margin: EdgeInsets.all(10),
          child: new Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(30)),
            ),
            child: nft.image?.toLowerCase().contains("svg") == true
                ? new SvgPicture.network(
                    formatIPFSImages(nft.image ?? "").replaceAll("https", "http"),
                    fit: BoxFit.fill,
                  )
                : new Image.network(
                    formatIPFSImages(nft.image ?? ""),
                    fit: BoxFit.fill,
                  ),
          )),
    );
  }

  String formatIPFSImages(String image) {
    var finalURL = "";
    if (image.toLowerCase().contains("ipfs")) {
      finalURL = image.replaceAll("ipfs://", "https://ipfs.io/ipfs/");
      return finalURL;
    }
    return image;
  }

  @override
  Widget build(BuildContext context) {
    return new GridView.count(
      primary: true,
      crossAxisCount: 2,
      children: List.generate(nfts?.length ?? 0, (index) {
        return getStructuredGridCell(nfts![index], index, context);
      }),
    );
  }
}
