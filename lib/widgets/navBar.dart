import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:ihave/controllers/LoginController.dart';
import 'package:ihave/controllers/walletController.dart';
import 'package:ihave/service/apiService.dart';
import 'package:ihave/ui/dashboard.dart';
import 'package:ihave/ui/help.dart';
import 'package:ihave/ui/mywallet.dart';
import 'package:ihave/ui/notificationPage.dart';
import 'package:ihave/util/utils.dart';
import 'package:provider/provider.dart';

class NavBar extends StatefulWidget {
  NavBar(bool isSideMenuVisible, {Key? key}) : super(key: key);

  @override
  _NavBarState createState() => _NavBarState();
}

class _NavBarState extends State<NavBar> {
  APIService apiService = LoginController.apiService;
  double gasBnb = 0;

  double gasEth = 0;
  double GazConsumedByCampaign = 60000;
  bool isVisible = false;
  bool isGreater = false;

  NavBar(bool isVisible) {
    this.isVisible = isVisible;
  }

  Future getGasPriceEth() async {
    await apiService.getGasEth(Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "");
  }

  Future getGasPriceBnb() async {
    await apiService.getGasBnb(Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "");
  }

  @override
  void initState() {
    getGasPriceEth().then((value) {
      setState(() {
        gasEth = apiService.ethGasPrice ?? 0;
      });
    });
    getGasPriceBnb().then((value) {
      setState(() {
        gasBnb = apiService.bnbGasPrice ?? 0;
      });
    });
    isGreater = (gasEth > gasBnb);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      color: Color(0xFF1f2337),
      child: ListView(
        padding: EdgeInsets.zero,
        children: [
          ListTile(
            leading: SvgPicture.asset('images/walleticone.svg', color: Colors.white),
            title: Text(
              'Wallet',
              style: GoogleFonts.poppins(color: Colors.white),
            ),
            onTap: () => {gotoPage(context, Dashboard())},
          ),
          Divider(
            thickness: 0.5,
            color: Colors.white,
          ),
          ListTile(
            leading: SvgPicture.asset('images/historicone.svg', color: Colors.white),
            title: Text(
              'My history',
              style: GoogleFonts.poppins(color: Colors.white),
            ),
            onTap: () => gotoPage(context, NotificationPage()),
          ),
          Divider(
            thickness: 0.5,
            color: Colors.white,
          ),
          ListTile(
            leading: SvgPicture.asset('images/Help.svg', color: Colors.white),
            title: Text(
              'Help',
              style: GoogleFonts.poppins(color: Colors.white),
            ),
            onTap: () => gotoPage(context, Help()),
          ),
          Divider(
            thickness: 0.5,
            color: Colors.white,
            height: 10,
            indent: 20,
            endIndent: 20,
          ),
          ListTile(
            leading: Padding(
              padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
              child: Padding(
                padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
                child: Icon(
                  Icons.local_gas_station_outlined,
                  size: 30,
                  color: Colors.white,
                ),
              ),
            ),
            title: Padding(
              padding: EdgeInsets.fromLTRB(10, 10, 0, 5),
              child: Row(
                children: [
                  Text(
                    "ETH:",
                    style: GoogleFonts.poppins(color: Colors.white),
                  ),
                  Text(
                    gasEth.toStringAsFixed(2),
                    style: GoogleFonts.poppins(color: Colors.white),
                  ),
                  (isGreater) ? SvgPicture.asset("images/Shape1.svg") : SvgPicture.asset("images/Shape2.svg"),
                  Text(
                    "BNB:",
                    style: GoogleFonts.poppins(color: Colors.white),
                  ),
                  Text(
                    gasBnb.toStringAsFixed(2),
                    style: GoogleFonts.poppins(color: Colors.white),
                  ),
                  (isGreater) ? SvgPicture.asset("images/Shape2.svg") : SvgPicture.asset("images/Shape1.svg"),
                ],
              ),
            ),
          ),
          Divider(
            thickness: 0.5,
            color: Colors.white,
            height: 10,
            indent: 20,
            endIndent: 20,
          ),
        ],
      ),
    );
  }

  gotoPage(BuildContext context, Widget page) {
    Navigator.of(context).push(MaterialPageRoute(builder: (context) => page));
    Provider.of<WalletController>(context, listen: false).toggleSideMenuVisibility();
  }
}
