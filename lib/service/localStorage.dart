import 'package:shared_preferences/shared_preferences.dart';

class LocalStorageService {
  static LocalStorageService? _instance;
  static SharedPreferences? _preferences;

  static const String TokenKey = 'accessToken';
  String tokenValue = "";

  static Future<LocalStorageService> getInstance() async {
    if (_instance == null) {
      _instance = LocalStorageService();
    }

    if (_preferences == null) {
      _preferences = await SharedPreferences.getInstance();
    }

    return _instance!;
  }

  Future<void> saveToDisk<T>(String key, T content) async {
    _preferences = await SharedPreferences.getInstance();
    if (content is String) {
      _preferences?.setString(key, content);
    }
    if (content is bool) {
      _preferences?.setBool(key, content);
    }
    if (content is int) {
      _preferences?.setInt(key, content);
    }
    if (content is double) {
      _preferences?.setDouble(key, content);
    }
    if (content is List<String>) {
      _preferences?.setStringList(key, content);
    }
  }

  dynamic getFromDisk(String key) {
    var value = _preferences?.get(key);
    return value;
  }

  String get token => getFromDisk(TokenKey) ?? "";

  set token(String value) => saveToDisk(TokenKey, value);
}
