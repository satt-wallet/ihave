import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:hex/hex.dart';
import 'package:http/http.dart' as http;
import 'package:ihave/model/approvedContracts.dart';
import 'package:provider/provider.dart';
import 'package:web3dart/web3dart.dart';

import '../controllers/LoginController.dart';
import '../controllers/walletController.dart';
import '../model/crypto.dart';
import 'apiService.dart';

/**
 * RPC MainNET
 */
const WEB3_URL_BEP20 = 'https://bsc-dataseed.binance.org/';
const WEB3_URL = 'https://eth.llamarpc.com';
const WEB3_URL_POLYGON = 'https://polygon.llamarpc.com';
const WEB3_URL_BTT = 'https://rpc.bittorrentchain.io';
const WEB3_URL_TRON = '';
/**
 * RPC Testnet
 */
const WEB3_URL_TESTNET = 'https://goerli.infura.io/v3/f1d98cca429e48d29087a4a2deb587c1';
const WEB3_URL_BEP20_TESTNET = 'https://data-seed-prebsc-1-s1.binance.org:8545';
const WEB3_URL_TRON_TESTNET = 'https://api.shasta.trongrid.io/jsonrpc';
const WEB3_URL_BTT_TESTNET = 'https://pre-rpc.bt.io/';
const WEB3_URL_POLYGON_TESTNET = 'https://polygon-mumbai.infura.io/v3/557c0cefcd1b4ba5b1418d56a699f705';

class Web3Service {
  late String privateKey;
  late Wallet wallet;
  var context;
  String walletAddress = "0xf91f6ba28b75dfe358b30759613e9014a355eac2";
  late String userToken;
  final _web3client = Web3Client(
    ENV == "PROD" ? WEB3_URL_BEP20 : WEB3_URL_BEP20_TESTNET,
    http.Client(),
  );
  final _web3clientEth = Web3Client(
    ENV == "PROD" ? WEB3_URL : WEB3_URL_TESTNET,
    http.Client(),
  );
  final _web3clientPoly = Web3Client(
    ENV == "PROD" ? WEB3_URL_POLYGON : WEB3_URL_POLYGON_TESTNET,
    http.Client(),
  );
  final _web3clientBtt = Web3Client(
    ENV == "PROD" ? WEB3_URL_BTT : WEB3_URL_BTT_TESTNET,
    http.Client(),
  );
  final _web3clientTrx = Web3Client(
    ENV == "PROD" ? WEB3_URL_TRON : WEB3_URL_TRON_TESTNET,
    http.Client(),
  );

  Web3Service(String userToken, String walletAddress, BuildContext context) {
    this.userToken = userToken;
    this.walletAddress = walletAddress;
    this.context = context;
  }

  checkTransaction(Map<String, dynamic> trans, String network) async {
    var isDismissable = await checkIsDismissable(trans, network);
    return isDismissable;
  }

  calculateProgress(int total, int actual) {
    var ratio = actual / total;
    var percent = ratio * 100;
    return percent;
  }

  Future<List<ApprovedContract>> getApprovedTokens() async {
    List<ApprovedContract> approvedTokens = [];
    List<Map<String, dynamic>> transactions = await getApprovedTokenBalances(walletAddress, "BEP20");
    List<Map<String, dynamic>> transactionsETH = await getApprovedTokenBalances(walletAddress, "ERC20");
    var actual = 0;
    await Future.forEach(transactions, (trans) async {
      await Future.delayed(Duration(milliseconds: 600), () async {
        actual++;
        var test = await checkTransaction(trans as Map<String, dynamic>, "BEP20");
        if (test) {
          approvedTokens.add(ApprovedContract.fromJson(trans));
        }
        var prog = calculateProgress(transactions.length + transactionsETH.length, actual);
        Provider.of<WalletController>(context, listen: false).updateApprovedTokensProgress(prog);
        print("the result is $test");
      });
    });
    await Future.forEach(transactionsETH, (trans) async {
      await Future.delayed(Duration(milliseconds: 600), () async {
        actual++;
        print("called from foreach erc in ${DateTime.now()}");
        var test = await checkTransaction(trans as Map<String, dynamic>, "ERC20");
        if (test) {
          approvedTokens.add(ApprovedContract.fromJson(trans));
        }
        var prog = calculateProgress(transactions.length + transactionsETH.length, actual);
        Provider.of<WalletController>(context, listen: false).updateApprovedTokensProgress(prog);
        print("the result is $test");
      });
    });
    return approvedTokens;
  }

  Future getWeb3TotalBalance(List<Crypto> cryptoList) async {
    // create an Ethereum address object from the given address string
    var balanceInWei = await getBalanceByNetwork(walletAddress, _web3clientEth);
    var balanceInWeiBep20 = await getBalanceByNetwork(walletAddress, _web3client);
    var balanceInWeiPoly = await getBalanceByNetwork(walletAddress, _web3clientPoly);
    var balanceInWeiBtt = await getBalanceByNetwork(walletAddress, _web3clientBtt);
    // return the balance in Wei as a BigInt object
    double totalBalance = balanceInWei + balanceInWeiBep20 + balanceInWeiPoly + balanceInWeiBtt;
    cryptoList.forEach((element) async {
      var price = element.price ?? 0;
      var quantity = double.parse(element.quantity ?? "0");
      final tokenBalance = price * quantity;
      totalBalance += tokenBalance;
    });
    return totalBalance;
  }

  double convertToDollar(balanceInWei, double ethPrice) {
    final etherBalance = BigInt.from(balanceInWei) / BigInt.from(10).pow(18);
    final usdBalance = etherBalance * ethPrice;
    return usdBalance;
  }

  getBalanceByNetwork(String walletAddress, Web3Client client) async {
    final ethAddress = EthereumAddress.fromHex(walletAddress);

    final balanceInWei = await client.getBalance(ethAddress);
    return balanceInWei.getInWei.toInt();
  }

  /**
   * Web2 Api calls
   */
  Future generateKey(String txPassword) async {
    var result = await LoginController.apiService.getBlockchainKey(userToken, txPassword);
    return result;
  }

  getApprovedTokenBalances(String address, String network) async {
    var apiKey = network.toLowerCase().contains("bep") ? 'NU1SMRICKYV1CCW4VDT4H3ERP268X9Z6VY' : '8S1PRHAZAMHPU4YTC4HU5YT2MT21B1BQ3J';
    Map<String, dynamic> params = {
      "module": "account",
      "action": "txlist",
      "address": address,
      "sort": "asc",
      "apikey": apiKey,
    };
    var baseUrl = network.toLowerCase().contains("bep") ? "api.bscscan.com" : "api.etherscan.com";
    var url = new Uri.https(baseUrl, "/api", params);
    // Make the API call to retrieve the approved token balances for the given address
    final response = await http.get(url);

    // Check the response status code
    if (response.statusCode == 200) {
      List<Map<String, dynamic>> filteredTransactions = [];
      // Parse the response JSON
      final jsonResponse = json.decode(response.body);
      var resp = jsonResponse["result"];
      resp.forEach((transaction) {
        if (transaction["input"].startsWith("0x095ea7b3")) {
          Map<String, dynamic> checkingTuple = {
            "token": transaction["to"],
            "spender": transaction["input"].substring(34, 74),
            "hash": transaction["hash"],
            "network": network,
            "amount": "",
          };
          filteredTransactions.add(checkingTuple);
        }
      });
      return filteredTransactions;
    } else {
      // Handle any errors that occurred during the API call
      throw Exception("Failed to retrieve approved token balances");
    }
  }

  getABI(String abiURL) async {
    final res = await http.get(Uri.parse(abiURL));
    return res;
  }

  checkIsDismissable(Map<String, dynamic> trans, String network) async {
    try {
      var abiUrl = handleAbiUrl(network, trans["token"]);
      var address = EthereumAddress.fromHex(walletAddress);
      var address2 = EthereumAddress.fromHex(trans["spender"]);
      final res = await getABI(abiUrl);
      final contract = DeployedContract(ContractAbi.fromJson(json.decode(res.body)['result'], ''), EthereumAddress.fromHex(trans["token"]));
      var web3client = network.toLowerCase().contains("bep") ? _web3client : _web3clientEth;
      var result = await web3client.call(
        contract: contract,
        function: contract.function('allowance'),
        params: [address, address2],
      );
      var symbolResult = await web3client.call(
        contract: contract,
        function: contract.function('symbol'),
        params: [],
      );
      var nameResult = await web3client.call(
        contract: contract,
        function: contract.function('name'),
        params: [],
      );
      print("Token symbol is ${symbolResult[0]}");
      var approvedAmount = BigInt.tryParse(result[0].toString(), radix: 16);
      if (approvedAmount != null) {
        if (approvedAmount! > BigInt.zero) {
          trans["amount"] = approvedAmount.toString();
          trans["symbol"] = symbolResult[0].toString();
          trans["name"] = nameResult[0].toString();
          return true;
        }
      }
      return false;
    } catch (Exception) {
      print("exception in contract here " + Exception.toString());
      return false;
    }
  }

  handleAbiUrl(String network, address) {
    if (network.toLowerCase().contains("bep")) {
      return 'https://api.bscscan.com/api?module=contract&action=getabi&address=${address}&apikey=NU1SMRICKYV1CCW4VDT4H3ERP268X9Z6VY';
    } else {
      return 'https://api.etherscan.com/api?module=contract&action=getabi&address=${address}&apikey=8S1PRHAZAMHPU4YTC4HU5YT2MT21B1BQ3J';
    }
  }

  revokeAllowance(String contractAdress, String network, String txPassword, String spender) async {
    var abiUrl = handleAbiUrl(network, contractAdress);
    final res = await getABI(abiUrl);
    var web3client = network.toLowerCase().contains("bep") ? _web3client : _web3clientEth;
    final contract = DeployedContract(ContractAbi.fromJson(json.decode(res.body)['result'], ''), EthereumAddress.fromHex(contractAdress));
    var resultWallet = await generateKey(txPassword);
    if (resultWallet != "wrong password") {
      Wallet wallet = Wallet.fromJson(resultWallet, txPassword);
      Credentials creds = wallet.privateKey;
      var result = await web3client.sendTransaction(
        creds,
        Transaction.callContract(
          contract: contract,
          function: contract.function("approve"),
          parameters: [EthereumAddress.fromHex(spender), BigInt.from(0)],
        ),
        chainId: network.toLowerCase().contains("bep") ? 56 : 1,
      );
      print(result.toString());
      return result;
    } else {
      return "Wrong Pass";
    }
  }

  getTronBalance(String tronAddress) async {
    final apiUrl = ENV == "PROD" ? 'https://api.trongrid.io/v1/accounts/$tronAddress' : 'https://api.shasta.trongrid.io/v1/accounts/$tronAddress';

    final response = await http.get(Uri.parse(apiUrl));

    if (response.statusCode == 200) {
      final jsonData = json.decode(response.body);
      var balanceList = jsonData['data'];
      var balanceInSUN = balanceList[0]['balance'];
      final balanceInTRX = balanceInSUN / 1000000;
      return balanceInTRX;
    } else {
      return 0;
    }
  }
}
