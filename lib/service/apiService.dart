import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'package:http/http.dart';
import 'package:ihave/controllers/LoginController.dart';
import 'package:ihave/model/crypto.dart';
import 'package:ihave/model/nft.dart';
import 'package:ihave/model/notifications.dart';
import 'package:ihave/model/operations.dart';
import 'package:ihave/model/searchCrypto.dart';
import 'package:ihave/model/userDetails.dart';
import 'package:ihave/widgets/alertDialogWidget.dart';
import 'package:one_context/one_context.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'localStorage.dart';
import 'package:collection/collection.dart';
import 'package:local_auth/local_auth.dart';

String ENV = "PREPROD";
String version = "v1";

class APIService {
  static var client = http.Client();
  static const String Key = 'Crypto';
  var storageService = LocalStorageService();
  String balance = "";
  String? walletAddress;
  String? walletAddressV2;
  String? btcWalletAddress;
  String? btcWalletAddressV2;
  String? tronAddress;
  String? tronAddressV2;
  double? bnbGasPrice;
  double GazConsumedByCampaign = 60000;
  double? ethGasPrice;

  final String baseURL = ENV == "PROD" ? "api2.satt-token.com" : "api-preprod2.satt-token.com";

  final String coinGeckoURL = "api.coingecko.com";
  final String openSeaBaseUrl = "deep-index.moralis.io";
  bool isEnabled = false;
  bool isComplete = true;
  bool hasWallet = false;
  bool is2FA = false;
  bool hasBiometric = false;
  bool isCompletedPassPhrase = false;
  bool emailApple = false;
  String userID = "";
  static final _auth = LocalAuthentication();
  String towFAImage = "";

  bool isSwitched = false;
  bool completedParallelRequest = false;

  String userMail = "";
  String firstName = "";
  String lastName = "";

  String idSn = "";

  String idOnSn = "";
  String idOnSn2 = "";

  sattSocialSignIn(String id, String idSn) async {
    Map<String, String> requestHeaders = {'Context-Type': 'application/json;charSet=UTF-8', "content-type": "application/json", 'Origin': "https://app.ihave.io", 'Access-Control-Allow-Origin': '*'};
    var result = "";
    var url;
    url = new Uri.https(
      baseURL,
      "/auth/socialSignin",
    );
    final Map<String, dynamic> dataToSend = {
      "idSn": int.parse(idSn),
      "id": id,
    };
    var body = jsonEncode(dataToSend);
    var response = await client.post(
      url,
      headers: requestHeaders,
      body: body,
    );
    if (response.statusCode == 200) {
      var jsonResponse = json.decode(response.body);
      var responseMessage = jsonResponse['data']["message"];
      if (responseMessage != "account_doesnt_exist" || responseMessage == null) {
        if (jsonResponse['data']['access_token'] != null && jsonResponse['data']['access_token'] != "") {
          var token = jsonResponse['data']['access_token'];
          return token;
        }
      }
      // return token;
    }
    return "error";
  }

  getCryptoPrice(String token, String symbol) async {
    Map<String, String> requestHeaders = {'Context-Type': 'application/json;charSet=UTF-8', 'Authorization': 'bearer ' + token, 'Origin': "https://app.ihave.io", 'Access-Control-Allow-Origin': '*'};
    var url;
    url = new Uri.https(baseURL, "/wallet/cryptoDetails");

    var response = await http.get(url, headers: requestHeaders);
    if (response.statusCode == 200) {
      var jsonResponse = json.decode(response.body);

      return jsonResponse["data"][symbol]["price"];
    }
    return "error";
  }

  getGasErc20(String token) async {
    Map<String, String> requestHeaders = {'Context-Type': 'application/json;charSet=UTF-8', 'Authorization': 'bearer ' + token, 'Origin': "https://app.ihave.io", 'Access-Control-Allow-Origin': '*'};
    var url;
    url = new Uri.https(baseURL, "/wallet/Erc20GasPrice");
    var response = await http.get(url, headers: requestHeaders);
    var jsonResponse = json.decode(response.body);
    if (jsonResponse["code"] == 200) {
      return jsonResponse["data"]["gasPrice"];
    } else {
      return "error";
    }
  }

  getGasBep20(String token) async {
    Map<String, String> requestHeaders = {'Context-Type': 'application/json;charSet=UTF-8', 'Authorization': 'bearer ' + token, 'Origin': "https://app.ihave.io", 'Access-Control-Allow-Origin': '*'};
    var url;
    url = new Uri.https(baseURL, "/wallet/Bep20GasPrice");
    var response = await http.get(url, headers: requestHeaders);
    var jsonResponse = json.decode(response.body);
    if (jsonResponse["code"] == 200) {
      return jsonResponse["data"]["gasPrice"];
    } else {
      return "error";
    }
  }

  Future getTrxGasFees() async {
    var url;
    url = new Uri.https(baseURL, '/wallet/TrxGasPrice');
    var response = await http.get(url);
    if (response.statusCode == 200) {
      var jsonResponse = json.decode(response.body);
      return jsonResponse["data"]["gasPrice"];
    } else {
      return "error";
    }
  }

  Future<String> sattSocialSignup(UserDetails? userDetails) async {
    Map<String, String> requestHeaders = {'Context-Type': 'application/json;charSet=UTF-8', "content-type": "application/json", 'Origin': "https://app.ihave.io", 'Access-Control-Allow-Origin': '*'};
    var result = "";
    var url;
    url = new Uri.https(
      baseURL,
      "/auth/socialSignup",
    );
    var name = userDetails?.username;
    var idSn = userDetails?.idSn;
    var email = userDetails?.email;
    var photo = userDetails?.picLink;
    var givenName = userDetails?.firstName;
    var familyName = userDetails?.name;
    var id = userDetails?.id;
    final Map<String, dynamic> dataToSend = {"name": name ?? "", "idSn": int.parse(idSn ?? "0"), "email": email ?? "", "photo": photo ?? "", "givenName": givenName ?? "", "familyName": familyName ?? "", "id": id ?? "", "newsLetter": true, "lang": "en"};
    var body = jsonEncode(dataToSend);
    var response = await client.post(url, headers: requestHeaders, body: body);
    if (response.statusCode == 200) {
      var jsonResponse = json.decode(response.body);
      var signUptoken = await this.getUserToken(response);
      if (signUptoken != null && signUptoken != "" && signUptoken != "null" && signUptoken != "error") {
        var token = await sattSocialSignIn(id ?? "", idSn ?? "");
        return token;
      } else {
        return "";
      }
    } else if (response.statusCode == 401) {
      return "account_exists";
    } else
      return "";
  }

  sattLoginMail(String email, String password) async {
    balance = "";
    Map<String, String> requestHeaders = {'Context-Type': 'application/json;charSet=UTF-8', 'Origin': "https://app.ihave.io", 'Access-Control-Allow-Origin': '*'};
    var url;
    url = new Uri.https(
      baseURL,
      "/auth/signin/mail",
    );

    var response = await client.post(
      url,
      headers: requestHeaders,
      body: {"username": email, "password": password},
    );
    var jsonResponse = json.decode(response.body);
    if (response.statusCode != 200) {
      if (jsonResponse['error']['message'] == "user not found") {
        return "user not found";
      } else {
        var isBlocked = jsonResponse['error']['message'];
        var jsonMessageAccountLocked = jsonResponse['error']["blockedDate"];
        if (jsonMessageAccountLocked != null && jsonMessageAccountLocked != "" && isBlocked == "account_locked") {
          return jsonMessageAccountLocked;
        }
      }
    } else {
      if (jsonResponse['data']['access_token'] != null && jsonResponse['data']['access_token'] != "") {
        var token = jsonResponse['data']['access_token'];
        isComplete = true;
        return token;
      }
    }
    return "error";
  }

  sattLoginApple(String idApple, String email, String name) async {
    Map<String, String> requestHeaders = {'Context-Type': 'application/json;charSet=UTF-8', "content-type": "application/json", 'Origin': "https://app.ihave.io", 'Access-Control-Allow-Origin': '*'};
    var url;
    url = new Uri.https(baseURL, '/auth/apple');
    final Map<String, dynamic> dataToSend = {"id_apple": idApple, "mail": email, "idSN": 6, "name": name};
    var body = jsonEncode(dataToSend);
    var response = await client.post(url, headers: requestHeaders, body: body);
    if (response.statusCode == 200) {
      var jsonResponse = json.decode(response.body);
      String token = jsonResponse['data']['access_token'];
      await checkValidation(token);
      return token;
    } else if (response.statusCode == 401) {
      return "Account Exist with another courrier";
    } else {
      return "fail";
    }
  }

  getUserToken(Response response) async {
    Map<String, dynamic> responseUncoded = jsonDecode(response.body);
    String? token = responseUncoded['data']['access_token'].toString();
    if (token.isNotEmpty) {
      return token.toString();
    } else {
      return "error";
    }
  }

  sattSignUpMail(String email, String password, bool newsLetter) async {
    Map<String, String> requestHeaders = {'Context-Type': 'application/json;charSet=UTF-8', "content-type": "application/json", 'Origin': "https://app.ihave.io", 'Access-Control-Allow-Origin': '*'};
    var url;
    url = new Uri.https(
      baseURL,
      "/auth/signup/mail",
    );
    final Map<String, dynamic> dataToSend = {"username": email, "password": password, "lang": "en", "newsLetter": newsLetter};
    var body = jsonEncode(dataToSend);
    var response = await client.post(
      url,
      headers: requestHeaders,
      body: body,
    );
    if (response.statusCode == 200) {
      var jsonResponse = json.decode(response.body);
      if (jsonResponse['data']['loggedIn'] == true) {
        return response;
      }
      var token = await sattLoginMail(email, password);
      if (token != "error") {
        activateBiometricAuth(token, isSwitched);
        return token;
      }
    }
    return (response);
  }

  Future getUserDetails(String token) async {
    Map<String, String> requestHeaders = {'Context-Type': 'application/json;charSet=UTF-8', 'Authorization': 'bearer ' + token, 'Origin': "https://app.ihave.io", 'Access-Control-Allow-Origin': '*'};
    var url;
    url = new Uri.https(
      baseURL,
      "/profile/account",
    );

    var response = await client.get(
      url,
      headers: requestHeaders,
    );
    if (response.statusCode == 200) {
      if (response.body != "Invalid Access Token") {
        var jsonResponse = json.decode(response.body);
        if (jsonResponse["data"]["expiredAt"] == null) {
          userID = jsonResponse['data']["_id"].toString();
        }
        return jsonResponse['data'];
      }
    }
    return null;
  }

  Future putUserDetails(String token, UserDetails user) async {
    Map<String, String> requestHeaders = {'Context-Type': 'application/json;charSet=UTF-8', "content-type": "application/json", 'Authorization': 'bearer ' + token, 'Origin': "https://app.ihave.io", 'Access-Control-Allow-Origin': '*'};
    var url;
    url = new Uri.https(
      baseURL,
      "/profile/UpdateProfile",
    );
    var bodyObject = user.toJson();
    var body = json.encode(bodyObject);
    var response = await client.put(
      url,
      headers: requestHeaders,
      body: body,
    );
    if (response.statusCode == 201) {
      this.lastName = user.lastName.toString();
      this.firstName = user.firstName.toString();
      return jsonDecode(response.body);
    }
    return null;
  }

  Future getTotalBalance(List<Crypto> cryptoList) async {
    double totalBalance = 0;
    cryptoList.forEach((element) {
      var price = element.price ?? 0;
      var quantity = double.parse(element.quantity ?? "0");
      final tokenBalance = price * quantity;
      totalBalance += tokenBalance;
    });
    return totalBalance;
  }

  Future sendExportCode(String token, String network) async {
    Map<String, String> requestHeaders = {'Context-Type': 'application/json;charSet=UTF-8', 'Authorization': 'bearer ' + token, 'Origin': "https://app.ihave.io", 'Access-Control-Allow-Origin': '*'};
    var url;
    url = new Uri.https(
      baseURL,
      "/wallet/code-export-keystore",
    );
    var ver = version == "v1" ? "1" : "2";
    var response = await client.post(url, headers: requestHeaders, body: {"network": network, "version": ver});

    if (response.statusCode == 200) {
      return true;
    }
    return false;
  }

  getUserLegal(String token) async {
    Map<String, String> requestHeaders = {"Context-type": "application/json;charSet=UTF-8", "Authorization": "bearer " + token, 'Origin': "https://app.ihave.io", 'Access-Control-Allow-Origin': '*'};
    var url = new Uri.https(baseURL, "/profile/UserLegal");
    var response = await client.get(url, headers: requestHeaders);
    switch (response.statusCode) {
      case 200:
        return json.decode(response.body)["data"]["legal"];
      case 401:
        return "Invalid Access Token";
      case 500:
        return "error";
    }
  }

  addUserLegal(String token, String type, File filename) async {
    Map<String, String> requestHeaders = {"Context-type": "application/json;charSet=UTF-8", "Content-Type": "multipart/form-data", "Authorization": "bearer " + token, 'Origin': "https://app.ihave.io", 'Access-Control-Allow-Origin': '*'};
    var url = new Uri.https(baseURL, "/profile/add/Legalprofile");
    var request = http.MultipartRequest('POST', url);

    request.headers.addAll(requestHeaders);
    request.fields["type"] = type;
    //request.fields["file"] = base64Image;
    request.files.add(await http.MultipartFile.fromPath(
      'file',
      filename.path,
    ));
    http.StreamedResponse response = await request.send();
    if (response.statusCode == 201) {
      return "success";
    }
    return "error";
  }

  getUserLegalPicture(String token, String id) async {
    Map<String, String> requestHeaders = {"Context-type": "application/json;charSet=UTF-8", "Authorization": "bearer " + token, 'Origin': "https://app.ihave.io", 'Access-Control-Allow-Origin': '*'};
    var url = new Uri.https(baseURL, "/profile/legalUserUpload/$id");
    try {
      var response = await client.get(url, headers: requestHeaders);
      switch (response.statusCode) {
        case 200:
          return response.bodyBytes;
        case 401:
          return "Invalid access token";
        case 204:
          return "no file exist";
        case 500:
          return "error";
      }
    } catch (Exception) {
      return "error";
    }
  }

  getBlockchainKey(String token, String code) async {
    Map<String, String> requestHeaders = {'Authorization': 'bearer ' + token, 'Origin': "https://app.ihave.io", "content-type": "application/json", 'Access-Control-Allow-Origin': '*'};
    var url = new Uri.https(baseURL, 'wallet/export-keystore');
    var ver = version == 'v1' ? '1' : '2';
    final Map<String, dynamic> dataToSend = {"code": num.parse(code), "network": "eth", "version": ver};
    var body = json.encode(dataToSend);
    var response = await client.post(url, headers: requestHeaders, body: body);
    if (response.statusCode == 200) {
      var jsonResponse = json.decode(response.body);
      if (jsonResponse["message"] != "code wrong") {
        return json.encode(jsonResponse);
      } else {
        return "wrong password";
      }
    } else if (response.statusCode == 500) {
      return "wrong password";
    } else if (response.statusCode == 204) {
      return "wallet not found";
    } else {
      return "error";
    }
  }

  getPolygonGasPrice(String token) async {
    Map<String, String> requestHeaders = {'Authorization': 'bearer ' + token, 'Origin': "https://app.ihave.io", 'Access-Control-Allow-Origin': '*'};
    var url = new Uri.https(baseURL, '/wallet/polygonGasPrice');
    var response = await client.get(url, headers: requestHeaders);
    if (response.statusCode == 200) {
      return json.decode(response.body)["data"]["gasPrice"];
    } else {
      return "error";
    }
  }

  getBitTorentGasPrice(String token) async {
    Map<String, String> requestHeaders = {'Authorization': 'bearer ' + token, 'Origin': "https://app.ihave.io", 'Access-Control-Allow-Origin': '*'};
    var url = new Uri.https(baseURL, '/wallet/BttGasPrice');
    var response = await client.get(url, headers: requestHeaders);
    if (response.statusCode == 200) {
      return json.decode(response.body)["data"]["gasPrice"];
    } else {
      return "error";
    }
  }

  getBlockchainKeyBTC(String token, String code) async {
    Map<String, String> requestHeaders = {
      'Authorization': 'bearer ' + token,
      'Origin': "https://app.ihave.io",
      'Access-Control-Allow-Origin': '*',
      "content-type": "application/json",
    };
    var url = new Uri.https(baseURL, '/wallet/export-keystore');
    var ver = version == "v1" ? "1" : "2";
    final Map<String, dynamic> dataToSend = {"code": num.parse(code), "network": "btc", "version": ver};
    var body = jsonEncode(dataToSend);
    var response = await client.post(url, headers: requestHeaders, body: body);
    if (response.statusCode == 200) {
      var jsonResponse = json.decode(response.body);
      if (jsonResponse["message"] != "code wrong") {
        return json.encode(jsonResponse);
      } else {
        return "wrong password";
      }
    } else if (response.statusCode == 500) {
      return "wrong password";
    } else if (response.statusCode == 204) {
      return "wallet not found";
    } else {
      return "error";
    }
  }

  Future walletTronCheck(String token) async {
    Map<String, String> requestHeaders = {'Context-Type': 'application/json;charSet=UTF-8', 'Authorization': 'bearer ' + token, 'Origin': "https://app.ihave.io", 'Access-Control-Allow-Origin': '*'};
    var url;
    url = new Uri.https(baseURL, "/wallet/mywallet");
    var response = await http.get(url, headers: requestHeaders);
    if (response.statusCode == 200) {
      var jsonResponse = json.decode(response.body);
      var trxBalance = jsonResponse["data"]["trx_balance"];
      return trxBalance;
    } else {
      return "error";
    }
  }

  Future createTronWallet(String token, String pass) async {
    Map<String, String> requestHeaders = {'Context-Type': 'application/json;charSet=UTF-8', 'Authorization': 'bearer ' + token, 'Origin': "https://app.ihave.io", 'Access-Control-Allow-Origin': '*'};
    var url;
    url = new Uri.https(baseURL, "/wallet/add-tron-wallet");
    var response = await http.post(url, headers: requestHeaders, body: {"pass": pass});
    if (response.statusCode == 200) {
      return "success";
    } else if (response.statusCode == 401) {
      return "exist";
    } else {
      var jsonResponse = json.decode(response.body);
      if (jsonResponse["error"] != null && jsonResponse["error"].toString().contains("wrong password")) {
        return "wrong password";
      } else {
        return "error";
      }
    }
  }

  Future exportTronWallet(String token, String pass) async {
    Map<String, String> requestHeaders = {'Context-Type': 'application/json;charSet=UTF-8', 'Authorization': 'bearer ' + token, 'Origin': "https://app.ihave.io", 'Access-Control-Allow-Origin': '*'};
    var url;
    url = new Uri.https(baseURL, "/wallet/exportTron");
    var response = await http.post(url, headers: requestHeaders, body: {"pass": pass});
    return json.decode(response.body);
  }

  Future getWalletAddress(String token, String version) async {
    Map<String, String> requestHeaders = {'Context-Type': 'application/json;charSet=UTF-8', 'Authorization': 'bearer ' + token, 'Origin': "https://app.ihave.io", 'Access-Control-Allow-Origin': '*'};
    var result;
    var url;
    url = new Uri.https(
      baseURL,
      "/wallet/mywallet",
    );
    var response = await client.post(url, headers: requestHeaders, body: {"version": version});
    if (response.statusCode == 200) {
      var jsonResponse = jsonDecode(response.body);
      var error = jsonResponse["err"] ?? "";
      var address = jsonResponse["data"]["address"] ?? "";
      var addressBTC = jsonResponse["data"]["btc"] ?? "";
      var addressTron = jsonResponse["data"]["tronAddress"] ?? "";
      if (error != "") {
        result = "empty";
      } else {
        if (version == "v2") {
          if (walletAddress == null || walletAddress == "") {
            walletAddress = address;
            btcWalletAddress = addressBTC;
            tronAddress = addressTron;
            version = "v2";
          }
          walletAddressV2 = address;
          btcWalletAddressV2 = addressBTC;
          tronAddressV2 = addressTron;
          result = " wallet exist";
        } else {
          walletAddress = address;
          btcWalletAddress = addressBTC;
          tronAddress = addressTron;
          result = " wallet exist";
        }
      }
    }
    return result;
  }

  getPassPhrase(String token) async {
    Map<String, String> requestHeaders = {'Context-Type': 'application/json;charSet=UTF-8', 'Authorization': 'Bearer ' + token, 'Origin': "https://app.ihave.io", 'Access-Control-Allow-Origin': '*'};
    var url;
    url = new Uri.https(
      baseURL,
      "/wallet/getMnemo",
    );
    var response = await client.get(url, headers: requestHeaders);
    if (response.statusCode == 200) {
      var jsonResponse = json.decode(response.body);
      var passPhrase = jsonResponse["data"]["mnemo"];
      return passPhrase;
    }
    return "error";
  }

  verifyPassPhrase(String token, String passPhrase) async {
    Map<String, String> requestHeaders = {'Context-Type': 'application/json;charSet=UTF-8', 'Authorization': 'Bearer ' + token, 'Origin': "https://app.ihave.io", 'Access-Control-Allow-Origin': '*'};
    var url;
    url = new Uri.https(
      baseURL,
      "/wallet/verifyMnemo",
    );
    var response = await client.post(url, headers: requestHeaders, body: {"mnemo": passPhrase});
    if (response.statusCode == 200) {
      var jsonResponse = json.decode(response.body);
      var verify = jsonResponse["data"]["verify"];
      return verify;
    } else {
      return false;
    }
  }

  Future cryptos1(String token, String version) async {
    Map<String, String> requestHeaders = {'Context-Type': 'application/json;charSet=UTF-8', 'Authorization': 'bearer ' + token, 'Origin': "https://app.ihave.io", 'Access-Control-Allow-Origin': '*'};
    var url;
    url = new Uri.https(
      baseURL,
      "/wallet/userBalance",
    );

    var response = await client.post(
      url,
      headers: requestHeaders,
      body: {"version": version},
    );
    if (response.statusCode == 200) {
      List<Crypto> cryptoList;
      var jsonResponse = json.decode(response.body);
      SharedPreferences prefs = await SharedPreferences.getInstance();
      cryptoList = (jsonResponse["data"] as List).map((i) => Crypto.fromJson(i)).toList();
      cryptoList.sort((e, e1) => e.listOrder!.compareTo(e1.listOrder!));

      List<dynamic> gekoList = prefs.getString("coin-gecko") != null ? jsonDecode(prefs.getString("coin-gecko").toString()) : [];
      if (gekoList.length <= 0) {
        var gekoResult = await getCoinGekoList();
        gekoList = gekoResult != "error" ? gekoResult : [];
      }
      cryptoList.forEach((element) {
        var symbol = element.symbol?.toLowerCase() == "sattbep20" || element.symbol?.toLowerCase() == "wsatt" ? "satt" : element.symbol?.toLowerCase();
        var gekoelement = gekoList.firstWhereOrNull((e) => e["symbol"] == symbol);
        if (gekoelement != null) {
          element.id = gekoelement["id"];
        }
      });
      var dataToStore = cryptoList.map((i) => Crypto().toJson(i)).toList();
      prefs.setString("user-list", jsonEncode(dataToStore));
      return cryptoList;
    } else {
      return "error";
    }
  }

  getQuotes(String token, String tokenSymbol, String dollarBalance, String fiatCurrency, String requestCurrency) async {
    Map<String, String> requestHeaders = {'Context-Type': 'application/json;charSet=UTF-8', "content-type": "application/json", 'Authorization': 'bearer ' + token, 'Origin': "https://app.ihave.io", 'Access-Control-Allow-Origin': '*'};
    final Map<String, dynamic> dataToSend = {"digital_currency": tokenSymbol, "requested_amount": double.parse(dollarBalance), "fiat_currency": fiatCurrency, "requested_currency": requestCurrency};
    var body = jsonEncode(dataToSend);
    var url;
    url = new Uri.https(baseURL, "/wallet/getQuote");
    var response = await client.post(url, headers: requestHeaders, body: body);
    if (response.statusCode == 200) {
      var jsonResponse = json.decode(response.body);
      return jsonResponse;
    } else if (response.statusCode == 500) {
      Map<String, dynamic> dataToreturn = {
        "error": "Internal Server Error",
      };
      var res = jsonEncode(dataToreturn);
      var jsonResponse = json.decode(res);
      return jsonResponse;
    } else {
      var jsonResponse = json.decode(response.body);
      return jsonResponse;
    }
  }

  getPaymentId(String token, String walletId, String currency, String quoteId) async {
    Map<String, String> requestHeaders = {'Context-Type': 'application/json;charSet=UTF-8', 'Authorization': 'bearer ' + token, 'Origin': "https://app.ihave.io", 'Access-Control-Allow-Origin': '*'};
    var url;
    url = new Uri.https(baseURL, "/wallet/payementRequest");
    var response = await client.post(
      url,
      headers: requestHeaders,
      body: {"currency": currency, "quote_id": quoteId, "idWallet": walletId},
    );
    if (response.statusCode == 200) {
      var jsonResponse = json.decode(response.body);
      return jsonResponse;
    }
    return "error";
  }

  payment(String paymentId) async {
    var url;
    url = new Uri.https("sandbox.test-simplexcc.com", "/payments/new");
    var response = await client.post(url, body: {
      "version": "1",
      "partner": "satt",
      "payment_flow_type": "wallet",
      "return_url_success": "https://testnet.satt.atayen.us/",
      "return_url_fail": "https://dev.satt.atayen.us/",
      "payment_id": paymentId,
    });
    if (response.statusCode == 200) {
      return response.body;
    }
    return "error";
  }

  Future cryptosToAdd(String token) async {
    Map<String, String> requestHeaders = {'Context-Type': 'application/json;charSet=UTF-8', 'Origin': "https://app.ihave.io", 'Access-Control-Allow-Origin': '*'};
    var url;
    url = new Uri.https(
      baseURL,
      "/wallet/cryptoDetails",
    );

    var response = await client.get(
      url,
      headers: requestHeaders,
    );
    if (response.statusCode == 200) {
      List<SearchCrypto> cryptoList = <SearchCrypto>[];
      ;
      Map<String, dynamic> data = json.decode(response.body)["data"];
      var jsonResponse = json.decode(response.body);
      data.forEach((k, v) {
        var crypto = SearchCrypto.fromJson(v);
        crypto.symbol = k;
        cryptoList.add(crypto);
      });
      SharedPreferences prefs = await SharedPreferences.getInstance();
      return cryptoList;
    } else {
      throw Exception('Failed to load cryptos from API');
    }
  }

  Future sendReceiveRequest(
    String token,
    String currency,
    String message,
    String amountCrypto,
    String mail,
    String wallet,
    String from,
  ) async {
    Map<String, String> requestHeaders = {'Context-Type': 'application/json;charSet=UTF-8', 'Authorization': 'bearer ' + token, 'Origin': "https://app.ihave.io", 'Access-Control-Allow-Origin': '*'};
    var url;
    url = new Uri.https(
      baseURL,
      "/profile/receiveMoney",
    );
    var response = await client.post(
      url,
      headers: requestHeaders,
      body: {"cryptoCurrency": currency, "message": message, "name": from, "price": amountCrypto, "to": mail, "wallet": wallet, "lang": "en"},
    );
    if (response.statusCode == 202) {
      return true;
    } else if (response.statusCode == 204) {
      return false;
    }
  }

  Future sendCrypto(
    String token,
    String amount,
    String network,
    String pass,
    String decimal,
    String symbole,
    String to,
    String smartContract,
  ) async {
    Map<String, String> requestHeaders = {'Context-Type': 'application/json;charSet=UTF-8', 'Authorization': 'bearer ' + token, 'Origin': "https://app.ihave.io", 'Access-Control-Allow-Origin': '*'};
    var apiSuffix = "";
    var isSpecial = true;
    if (network.toLowerCase() == "btc") {
      apiSuffix = "/wallet/transfertBtc";
    } else if (symbole.toLowerCase() == "eth") {
      apiSuffix = "/wallet/transfertEther";
    } else if (symbole.toLowerCase() == "bnb") {
      apiSuffix = "/wallet/transfertBNB";
    } else if (network.toLowerCase() == "bep20") {
      apiSuffix = "/wallet/transferBep20";
      isSpecial = false;
    } else {
      apiSuffix = "/wallet/transferErc20";
      isSpecial = false;
    }
    var url;
    url = new Uri.https(baseURL, apiSuffix);

    http.Response response;
    if (isSpecial) {
      response = await transferSpecialCrypto(url, requestHeaders, amount, pass, to);
    } else {
      response = await transferNormalCrypto(url, requestHeaders, token, amount, network, pass, decimal, symbole, to, smartContract);
    }
    var jsonResponse = jsonDecode(response.body);
    if (response.statusCode == 200) {
      return jsonResponse;
    } else {
      return jsonDecode(response.body);
    }
  }

  Future<http.Response> transferNormalCrypto(url, Map<String, String> requestHeaders, String token, String amount, String network, String pass, String decimal, String symbole, String to, String smartContract) async {
    final Map<String, dynamic> dataToSend = {
      "amount": amount,
      "pass": pass,
      "symbole": symbole,
      "to": to,
      "decimal": int.parse(decimal),
      "token": smartContract,
    };
    var body = jsonEncode(dataToSend);
    var response = await client.post(
      url,
      headers: requestHeaders,
      body: body,
    );
    return response;
  }

  Future<http.Response> transferSpecialCrypto(url, Map<String, String> requestHeaders, String amount, String pass, String to) async {
    final Map<String, dynamic> dataToSend = {
      "val": amount,
      "pass": pass,
      "to": to,
    };
    var body = jsonEncode(dataToSend);
    var response = await client.post(
      url,
      headers: requestHeaders,
      body: body,
    );
    return response;
  }

  Future addCustomCrypto(
    String token,
    String decimals,
    String cryptoContract,
    String tokenSymbol,
    String network,
    String name,
  ) async {
    Map<String, String> requestHeaders = {'Context-Type': 'application/json;charSet=UTF-8', "content-type": "application/json", 'Authorization': 'bearer ' + token, 'Origin': "https://app.ihave.io", 'Access-Control-Allow-Origin': '*'};
    var url;
    url = new Uri.https(
      baseURL,
      "/wallet/addNewToken",
    );
    final Map<String, dynamic> dataToSend = {
      "decimal": decimals != null && decimals != "" ? int.parse(decimals) : null,
      "network": network,
      "symbol": tokenSymbol,
      "tokenAdress": cryptoContract,
      "tokenName": name,
    };
    var body = jsonEncode(dataToSend);
    var response = await client.post(
      url,
      headers: requestHeaders,
      body: body,
    );
    if (response.statusCode == 200) {
      return json.decode(response.body);
    } else {
      return jsonDecode(response.body);
    }
  }

  static Future<bool> hasBiometrics() async {
    try {
      return await _auth.canCheckBiometrics;
    } on PlatformException catch (e) {
      return false;
    }
  }

  static Future<List<BiometricType>> getBiometrics() async {
    try {
      return await _auth.getAvailableBiometrics();
    } on PlatformException catch (e) {
      return <BiometricType>[];
    }
  }

  authenticate() async {
    var isAvailable = await hasBiometrics();
    if (!isAvailable) return true;
    try {
      var test = await getBiometrics();
      return await _auth.authenticate(
        localizedReason: 'Scan Fingerprint to Authenticate',
        options: const AuthenticationOptions(
          useErrorDialogs: true,
          stickyAuth: true,
        ),
      );
    } on PlatformException catch (e) {
      return false;
    }
  }

  Future getCryptoInfo(
    String token,
    String cryptoContract,
    String network,
  ) async {
    Map<String, String> requestHeaders = {'Context-Type': 'application/json;charSet=UTF-8', 'Authorization': 'bearer ' + token, 'Origin': "https://app.ihave.io", 'Access-Control-Allow-Origin': '*'};
    var url;
    url = new Uri.https(
      baseURL,
      "/wallet/checkWalletToken",
    );
    var response = await client.post(
      url,
      headers: requestHeaders,
      body: {
        "network": network.toLowerCase(),
        "tokenAdress": cryptoContract,
      },
    );
    if (response.statusCode == 200) {
      return json.decode(response.body);
    } else {
      return "error";
    }
  }

  Future createWallet(String token, String password, String version) async {
    Map<String, String> requestHeaders = {'Context-Type': 'application/json;charSet=UTF-8', 'Authorization': 'bearer ' + token, 'Origin': "https://app.ihave.io", 'Access-Control-Allow-Origin': '*'};
    var url;
    url = new Uri.https(
      baseURL,
      "/wallet/create/v2",
    );

    var response = await client.post(
      url,
      headers: requestHeaders,
      body: {"pass": password},
    );
    if (response.statusCode == 200) {
      var jsonResoponse = json.decode(response.body);
      var responseMessage = jsonResoponse["data"]["address"];
      getWalletAddress(token, "v1");
      getWalletAddress(token, "v2");
      return responseMessage;
    } else {
      var jsonResoponse = json.decode(response.body);
      var responseMessage = jsonResoponse["error"];
      if (responseMessage == "same password") {
        return "Same Password";
      }
    }
    return "error";
  }

  changePassword(String email, String newPass, String code) async {
    Map<String, String> requestHeaders = {'Context-type': 'application/json;charSet=UTF-8', "content-type": "application/json", 'Origin': "https://app.ihave.io", 'Access-Control-Allow-Origin': '*'};
    final Map<String, dynamic> dataToSend = {
      "email": email,
      "newpass": newPass,
      "code": int.parse(code),
    };
    var body = jsonEncode(dataToSend);
    var result = "";
    var url;
    url = new Uri.https(
      baseURL,
      "/auth/passrecover",
    );
    var response = await client.post(url, headers: requestHeaders, body: body);
    if (response.statusCode == 200) {
      var jsonResponse = json.decode(response.body);
      if (jsonResponse['message'] == "successfully") {
        result = "success";
      } else {
        result = "error";
      }
    }
    return result;
  }

  confirmResetPassword(String email, String code) async {
    Map<String, String> requestHeaders = {'Context-type': 'application/json;charSet=UTF-8', 'Origin': "https://app.ihave.io", 'Access-Control-Allow-Origin': '*'};
    var result = "";
    var url;
    url = new Uri.https(
      baseURL,
      "/auth/confirmCode",
    );
    var response = await client.post(url, headers: requestHeaders, body: {"code": code, "email": email.trim(), "type": "reset"});
    if (response.statusCode == 200) {
      var jsonResponse = json.decode(response.body);
      var responseMessage = jsonResponse['data'];
      if (responseMessage != null && responseMessage) {
        result = "code match";
      } else {
        result = "code incorrect";
      }
    }
    return result;
  }

  confirmCode(String email, String code) async {
    // true when responsemessage = 	"code match"
    Map<String, String> requestHeaders = {'Context-Type': 'application/json;charSet=UTF-8', 'Origin': "https://app.ihave.io", 'Access-Control-Allow-Origin': '*'};
    var result = "";
    var url;
    url = new Uri.https(
      baseURL,
      "/auth/confirmCode",
    );

    var response = await client.post(
      url,
      headers: requestHeaders,
      body: {"code": code, "email": email, "type": "validation"},
    );
    if (response.statusCode == 200) {
      var jsonResponse = json.decode(response.body);
      var responseMessage = jsonResponse['data'] ?? false;
      return responseMessage;
    }
    if (response.statusCode == 401) {
      var jsonResponse = json.decode(response.body);
      var responseMessage = jsonResponse['error'];
      return (responseMessage);
    }
    return false;
  }

  Future resendCode(String email, String token) async {
    Map<String, String> requestHeaders = {'Context-Type': 'application/json;charSet=UTF-8', 'Origin': "https://app.ihave.io", 'Access-Control-Allow-Origin': '*'};
    var result = "";
    var url;
    url = new Uri.https(
      baseURL,
      "/auth/resend/confirmationToken",
    );

    var response = await client.post(
      url,
      headers: requestHeaders,
      body: {"email": email},
    );
    if (response.statusCode == 200) {
      var jsonResoponse = json.decode(response.body);
      var responseMessage = jsonResoponse["message"];
      return responseMessage;
    }
    return result;
  }

  Future changePass(String token, String oldpass, String newpass) async {
    Map<String, String> requestHeaders = {'Context-Type': 'application/json;charSet=UTF-8', 'Authorization': 'bearer ' + token, 'Origin': "https://app.ihave.io", 'Access-Control-Allow-Origin': '*'};

    var url;
    url = new Uri.https(
      baseURL,
      "/auth/changePassword",
    );

    var response = await client.post(
      url,
      headers: requestHeaders,
      body: {
        "oldpass": oldpass,
        "newpass": newpass,
      },
    );
    switch (response.statusCode) {
      case (200):
        var jsonResoponse = json.decode(response.body);
        var responseMessage = jsonResoponse["message"];
        return responseMessage;
      case (401):
        return "wrong password";
      case (204):
        return "no account ";
      case (500):
        return "error";
    }
  }

  Future pushNotif(String token, String firebaseToken, String mobile) async {
    Map<String, String> requestHeaders = {'Context-Type': 'application/json;charSet=UTF-8', 'Authorization': 'bearer ' + token, 'Origin': "https://app.ihave.io", 'Access-Control-Allow-Origin': '*'};
    Map<String, String> queryParameters = {"fireBase": mobile};
    var url;
    url = new Uri.https(baseURL, "/auth/save/firebaseAccessToken", queryParameters);

    var response = await client.post(
      url,
      headers: requestHeaders,
      body: {"fb_accesstoken": firebaseToken},
    );
    switch (response.statusCode) {
      case (200):
        var jsonResoponse = json.decode(response.body);
        var responseMessage = jsonResoponse["message"];
        return responseMessage;

      case (500):
        return "error";
    }
  }

  Future RemoveAccount(String token, String reason, String password) async {
    Map<String, String> requestHeaders = {'Context-Type': 'application/json;charSet=UTF-8', 'Authorization': 'bearer ' + token, 'Origin': "https://app.ihave.io", 'Access-Control-Allow-Origin': '*'};

    var url;
    url = new Uri.https(
      baseURL,
      "/auth/purge",
    );

    var response = await client.post(
      url,
      headers: requestHeaders,
      body: {
        "reason": reason,
        "password": password,
      },
    );
    switch (response.statusCode) {
      case (200):
        var jsonResoponse = json.decode(response.body);
        var responseMessage = jsonResoponse["message"];
        return responseMessage;
      case (401):
        return "wrong password";

      case (500):
        return "error";
    }
  }

  forgetPassword(String email) async {
    Map<String, String> requestHeaders = {'Context-Type': 'application/json;charSet=UTF-8', 'Origin': "https://app.ihave.io", 'Access-Control-Allow-Origin': '*'};
    var result = "";
    var url;
    url = new Uri.https(
      baseURL,
      "/auth/passlost",
    );

    var response = await client.post(
      url,
      headers: requestHeaders,
      body: {"mail": email.trim()},
    );
    if (response.statusCode == 200) {
      try {
        var jsonResponse = json.decode(response.body);
        var responseMessage = jsonResponse["message"];
        if (responseMessage != "") {
          result = 'Email was sent';
        } else {}
      } catch (FormatException) {
        if (response.body == "{error:\"account not exists\"}") {
          result = "account does not exist";
        }
      }
    }
    return result;
  }

  TransfertCrypto(String token, String network, String smartContract, String symbol, String to, String amount, String pass) async {
    Map<String, String> requestHeaders = {'Context-Type': 'application/json;charSet=UTF-8', 'Authorization': 'bearer ' + token, "content-type": "application/json", 'Origin': "https://app.ihave.io", 'Access-Control-Allow-Origin': '*'};
    var url;
    url = new Uri.https(baseURL, "/wallet/transferTokens");
    network = network.toUpperCase();
    final Map<String, dynamic> dataToSend = {
      "from": this.walletAddress,
      "to": to,
      "amount": amount,
      "tokenSymbol": symbol,
      "tokenAddress": smartContract == "null" || symbol == "MATIC" || symbol == "BTT" ? (network == "TRON" ? "TRpHXiD9PRoorNh9Lx4NeJUAP7NcG5zFwi" : null) : smartContract,
      "network": network == "TRON" ? network.toLowerCase() : network,
      "pass": pass,
    };
    var body = jsonEncode(dataToSend);
    var response = await http.post(url, headers: requestHeaders, body: body);

    return json.decode(response.body);
  }

  Future checkValidation(String token) async {
    var userAccount = await getUserDetails(token);
    if (userAccount != null) {
      this.userMail = userAccount["email"] ?? "";
      this.firstName = userAccount["firstName"] ?? "";
      this.lastName = userAccount["lastName"] ?? "";
      this.idSn = userAccount["idSn"].toString();
      this.idOnSn = userAccount["idOnSn"].toString();
      this.idOnSn2 = userAccount["idOnSn2"].toString();
      this.hasWallet = userAccount["hasWallet"];
      this.isEnabled = userAccount['enabled'] != 0;
      this.isComplete = userAccount['completed'] != null ? userAccount['completed'] : false;
      if (userAccount["idSn"] != 1 && userAccount["idSn"] != 2) {
        this.isComplete = true;
      }
      this.is2FA = userAccount['is2FA'] != null ? userAccount['is2FA'] == "true" || userAccount['is2FA'] : false;
      this.hasBiometric = userAccount['hasBiometrics'] != null ? userAccount['hasBiometrics'] : false;
      this.isCompletedPassPhrase = userAccount['passphrase'] != null ? userAccount['passphrase'] : false;
      emailApple = (RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(userAccount['email']));
      return "success";
    } else {
      return "error";
    }
  }

  updateProfilePassPhrase(String token) async {
    Map<String, String> requestHeaders = {'Context-Type': 'application/json;charSet=UTF-8', "content-type": "application/json", 'Authorization': 'bearer ' + token, 'Origin': "https://app.ihave.io", 'Access-Control-Allow-Origin': '*'};
    var url;
    url = new Uri.https(baseURL, "/profile/UpdateProfile");
    final Map<String, dynamic> dataToSend = {
      "passphrase": true,
      "new": true,
      "visitPassphrase": true,
    };
    var body = jsonEncode(dataToSend);
    var response = await client.put(url, headers: requestHeaders, body: body);
    if (response.statusCode == 201) {}
  }

  Future getGasEth(String token) async {
    Map<String, String> requestHeaders = {'Context-Type': 'application/json;charSet=UTF-8', 'Authorization': 'bearer ' + token, 'Origin': "https://app.ihave.io", 'Access-Control-Allow-Origin': '*'};
    var url;
    url = new Uri.https(
      baseURL,
      "/wallet/Erc20GasPrice",
    );

    var response = await client.get(
      url,
      headers: requestHeaders,
    );

    if (response.statusCode == 200) {
      var jsonResponse = json.decode(response.body);
      var eth = jsonResponse["data"]["gasPrice"];
      List<Crypto> ethPrice = await cryptos1(token, version);
      var ethgas = ethPrice.firstWhere((e) => e.symbol == "ETH").price ?? 0;
      ethGasPrice = ((eth * 60000) / 1000000000) * ethgas;
      return ethGasPrice;
    }
    return ethGasPrice;
  }

  Future getGasBnb(String token) async {
    Map<String, String> requestHeaders = {'Context-Type': 'application/json;charSet=UTF-8', 'Authorization': 'bearer ' + token, 'Origin': "https://app.ihave.io", 'Access-Control-Allow-Origin': '*', 'Origin': "https://app.ihave.io", 'Access-Control-Allow-Origin': '*'};
    var url;
    url = new Uri.https(
      baseURL,
      "/wallet/Bep20GasPrice",
    );

    var response = await client.get(
      url,
      headers: requestHeaders,
    );
    if (response.statusCode == 200) {
      var jsonResponse = json.decode(response.body);
      var bnb = jsonResponse["data"]["gasPrice"];
      List<Crypto> bnbPrice = await cryptos1(token, version);
      var bnbgas = bnbPrice.firstWhere((e) => e.symbol == "BNB").price ?? 0;
      bnbGasPrice = ((bnb * GazConsumedByCampaign) / 1000000000) * bnbgas;

      return bnbGasPrice;
    }
    return bnbGasPrice;
  }

  getUserSavedSession(token, version) async {
    await getWalletAddress(token, "v1");
    await getWalletAddress(token, "v2");
    await checkValidation(token);
    return this.walletAddress;
  }

  Future captcha() async {
    Map<String, String> requestHeaders = {'Context-Type': 'application/json;charSet=UTF-8', 'Origin': "https://app.ihave.io", 'Access-Control-Allow-Origin': '*'};
    var url;
    url = new Uri.https(baseURL, '/auth/captcha');

    var response = await client.get(url, headers: requestHeaders);
    if (response.statusCode == 200) {
      var jsonResponse = json.decode(response.body);
      var captcha = jsonResponse["data"];
      var originalImage = captcha["originalImage"];
      var puzzleImage = captcha["puzzle"];
      var id = captcha["_id"];
      return [originalImage, puzzleImage, id];
    }

    return "error";
  }

  Future verifyCaptcha(String id, String position) async {
    Map<String, String> requestHeaders = {'Context-Type': 'application/json;charSet=UTF-8', 'Origin': "https://app.ihave.io", 'Access-Control-Allow-Origin': '*'};
    var result = "";
    var url;
    url = new Uri.https(baseURL, '/auth/verifyCaptcha');
    var response = await client.post(url, headers: requestHeaders, body: {"_id": id, "position": position});
    if (response.statusCode == 200) {
      var jsonResponse = json.decode(response.body);
      var jsonMessage = jsonResponse["message"];
      if (jsonMessage != null) {
        result = "success";
      } else {
        result = "error";
      }
    } else {
      result = "error";
    }
    return result;
  }

  changeEmail(String newEmail, String password, String token) async {
    Map<String, String> requestHeaders = {'Context-Type': 'application/json;charSet=UTF-8', 'Authorization': 'bearer ' + token, 'Origin': "https://app.ihave.io", 'Access-Control-Allow-Origin': '*'};
    var result = "";
    var url;
    url = new Uri.https(baseURL, "/profile/changeEmail");
    var response = await client.post(url, headers: requestHeaders, body: {"email": newEmail, "pass": password});
    var jsonResponse = json.decode(response.body);
    if (response.statusCode == 200) {
      return "success";
    }
    return jsonResponse["error"];
  }

  confirmChangeEmail(String code, String token) async {
    Map<String, String> requestHeaders = {'Context-Type': 'application/json;charSet=UTF-8', 'content-type': 'application/json', 'Authorization': 'bearer ' + token, 'Origin': "https://app.ihave.io", 'Access-Control-Allow-Origin': '*'};
    var result = "";
    final Map<String, dynamic> dataToSend = {
      "code": int.parse(code),
    };
    var body = jsonEncode(dataToSend);
    var url;
    url = new Uri.https(baseURL, "/profile/confirmChangeEmail");
    var response = await client.post(url, headers: requestHeaders, body: body);
    if (response.statusCode == 200) {
      result = "email changed";
      return result;
    } else {
      var jsonResponse = jsonDecode(response.body);
      return jsonResponse['error'];
    }
  }

  Future getUserNotifications(String token) async {
    Map<String, String> requestHeaders = {'Context-Type': 'application/json;charSet=UTF-8', 'Authorization': 'bearer ' + token, 'Origin': "https://app.ihave.io", 'Access-Control-Allow-Origin': '*'};
    var map = Map();
    var url;
    url = new Uri.https(
      baseURL,
      "/profile/notifications",
    );

    var response = await client.get(
      url,
      headers: requestHeaders,
    );
    List<Notifications>? NotificationList;
    if (response.statusCode == 200) {
      var jsonResponse = json.decode(response.body);
      NotificationList = (jsonResponse['data']["notifications"] as List).map((i) => Notifications.fromJson(i)).toList();
      return NotificationList;
    } else {
      List<Notifications> notifs = [];
      return notifs;
    }
  }

  getUserPicture(String token) async {
    Map<String, String> requestHeaders = {'Content-Type': 'application/json;charSet=UTF-8', 'Authorization': 'Bearer ' + token, 'Origin': "https://app.ihave.io", 'Access-Control-Allow-Origin': '*'};
    Uint8List? picture;
    var url;
    url = new Uri.https(baseURL, '/profile/picture');
    var response = await client.get(
      url,
      headers: requestHeaders,
    );
    if (response.statusCode == 200) {
      picture = response.bodyBytes;
      return picture;
    }
    return "error";
  }

  updatePicture(String filename, String token) async {
    Map<String, String> requestHeaders = {'Authorization': 'Bearer ' + token, 'Origin': "https://app.ihave.io", 'Access-Control-Allow-Origin': '*'};
    var result = "";
    var url;
    url = new Uri.https(baseURL, '/profile/picture');
    var request = http.MultipartRequest('POST', url);
    request.headers.addAll(requestHeaders);
    request.files.add(await http.MultipartFile.fromBytes('file', File(filename).readAsBytesSync(), filename: filename.split("/").last));
    var response = await request.send();
    if (response.statusCode == 201) {
      result = "success";
    } else if (response.statusCode == 413) {
      result = "over-size";
    } else {
      result = "error";
    }
    return result;
  }

  Future sendHelp(
    String token,
    String email,
    String message,
    String name,
    String subject,
  ) async {
    Map<String, String> requestHeaders = {'Context-Type': 'application/json;charSet=UTF-8', 'Authorization': 'bearer ' + token, 'Origin': "https://app.ihave.io", 'Access-Control-Allow-Origin': '*'};
    var url;
    url = new Uri.https(
      baseURL,
      "/profile/SattSupport",
    );
    var response = await client.post(
      url,
      headers: requestHeaders,
      body: {
        "email": email,
        "message": message,
        "name": name,
        "subject": subject,
      },
    );
    if (response.statusCode == 200) {
      return "success";
    } else {
      return "error";
    }
  }

  Future removeToken(
    String token,
    String tokenAddress,
  ) async {
    Map<String, String> requestHeaders = {'Context-Type': 'application/json;charSet=UTF-8', 'Authorization': 'bearer ' + token, 'Origin': "https://app.ihave.io", 'Access-Control-Allow-Origin': '*'};
    var url;
    url = new Uri.https(
      baseURL,
      "/wallet/removeToken/" + tokenAddress,
    );
    var response = await client.delete(
      url,
      headers: requestHeaders,
    );
    if (response.statusCode == 200) {
      return "token removed";
    } else {
      return "error";
    }
  }

  updateLastStep(String firstName, String lastName, String email, String token) async {
    Map<String, String> requestHeaders = {'Context-Type': 'application/json;charSet=UTF-8', "content-type": "application/json", 'Authorization': 'bearer ' + token, 'Origin': "https://app.ihave.io", 'Access-Control-Allow-Origin': '*'};
    var url;
    url = new Uri.https(
      baseURL,
      "/auth/updateLastStep",
    );
    final Map<String, dynamic> dataToSend = {"email": email, "firstName": firstName, "lastName": lastName, "completed": true};
    var body = jsonEncode(dataToSend);
    var response = await client.put(url, headers: requestHeaders, body: body);
    if (response.statusCode == 200) {
      return jsonDecode(response.body);
    }
    return null;
  }

  Future get2FaSecret(String token) async {
    Map<String, String> requestHeaders = {'Context-Type': 'application/json;charSet=UTF-8', 'Authorization': 'bearer ' + token, 'Origin': "https://app.ihave.io", 'Access-Control-Allow-Origin': '*'};
    var url;
    url = new Uri.https(
      baseURL,
      "/auth/qrCode",
    );

    var response = await client.get(
      url,
      headers: requestHeaders,
    );
    if (response.statusCode == 200) {
      if (response.body != "Invalid Access Token") {
        var jsonResponse = json.decode(response.body);
        var returnedValue = jsonResponse['data'];
        towFAImage = jsonResponse['data']["qrCode"].toString().substring(jsonResponse['data']["qrCode"].toString().indexOf(",") + 1, jsonResponse['data']["qrCode"].toString().length);
        return returnedValue;
      }
    }
    return null;
  }

  Future activate2FA(String token, bool isSwitched) async {
    Map<String, String> requestHeaders = {'Context-Type': 'application/json;charSet=UTF-8', "content-type": "application/json", 'Authorization': 'bearer ' + token, 'Origin': "https://app.ihave.io", 'Access-Control-Allow-Origin': '*'};
    final Map<String, dynamic> dataToSend = {"is2FA": isSwitched};
    var body = jsonEncode(dataToSend);
    var url;
    url = new Uri.https(
      baseURL,
      "/profile/UpdateProfile",
    );

    var response = await client.put(url, headers: requestHeaders, body: body);
    if (response.statusCode == 201) {
      return jsonDecode(response.body);
    }
    return null;
  }

  Future activateAccount(String token, int accountEnabled) async {
    Map<String, String> requestHeaders = {'Context-Type': 'application/json;charSet=UTF-8', "content-type": "application/json", 'Authorization': 'bearer ' + token, 'Origin': "https://app.ihave.io", 'Access-Control-Allow-Origin': '*'};
    final Map<String, dynamic> dataToSend = {"enabled": accountEnabled};
    var body = jsonEncode(dataToSend);
    var url;
    url = new Uri.https(
      baseURL,
      "/profile/UpdateProfile",
    );

    var response = await client.put(url, headers: requestHeaders, body: body);
    if (response.statusCode == 201) {
      return jsonDecode(response.body);
    }
    return null;
  }

  Future activateBiometricAuth(String token, bool isSwitchedBiometrics) async {
    Map<String, String> requestHeaders = {'Context-Type': 'application/json;charSet=UTF-8', "content-type": "application/json", 'Authorization': 'bearer ' + token, 'Origin': "https://app.ihave.io", 'Access-Control-Allow-Origin': '*'};
    final Map<String, dynamic> dataToSend = {"hasBiometrics": isSwitchedBiometrics};
    var body = jsonEncode(dataToSend);
    var url;
    url = new Uri.https(
      baseURL,
      "/profile/UpdateProfile",
    );

    var response = await client.put(url, headers: requestHeaders, body: body);
    if (response.statusCode == 201) {
      return jsonDecode(response.body);
    }
    return null;
  }

  verifyQrCode(String code, String token) async {
    Map<String, String> requestHeaders = {'Context-Type': 'application/json;charSet=UTF-8', "content-type": "application/json", 'Authorization': 'bearer ' + token, 'Origin': "https://app.ihave.io", 'Access-Control-Allow-Origin': '*'};
    var result = false;
    var url;
    url = new Uri.https(
      baseURL,
      "/auth/verifyQrCode",
    );
    final Map<String, dynamic> dataToSend = {
      "code": code,
    };
    var body = jsonEncode(dataToSend);
    var response = await client.post(
      url,
      headers: requestHeaders,
      body: body,
    );
    if (response.statusCode == 200) {
      var jsonResponse = json.decode(response.body);
      var responseMessage = jsonResponse['data']["verifiedCode"];
      if (responseMessage != null) {
        result = responseMessage;
      }
      return result;
    }
  }

  Future getCryptoData(String id) async {
    Map<String, String> requestHeaders = {"content-type": "application/json; charset=utf-8", 'Origin': "https://app.ihave.io", 'Access-Control-Allow-Origin': '*'};
    var url = new Uri.https(coinGeckoURL, "/api/v3/coins/${id.toLowerCase()}");
    var response = await client.get(url, headers: requestHeaders);
    if (response.statusCode == 200) {
      return json.decode(response.body);
    }
    throw Exception("Unable to fetch data from server");
  }

  Future getIdCoin() async {
    Map<String, String> requestHeaders = {"content-type": "application/json; charset=utf-8", 'Origin': "https://app.ihave.io", 'Access-Control-Allow-Origin': '*'};
    var url = new Uri.https(coinGeckoURL, "/api/v3/coins/list");
    var response = await client.get(url, headers: requestHeaders);

    if (response.statusCode == 200) {
      return response.body;
    } else {
      return "error";
    }
  }

  Future getCoinGekoList() async {
    Map<String, String> requestHeaders = {"content-type": "application/json; charset=utf-8", 'Origin': "https://app.ihave.io", 'Access-Control-Allow-Origin': '*'};
    var url = new Uri.https(coinGeckoURL, "/api/v3/coins/list");
    var response = await client.get(url, headers: requestHeaders);
    if (response.statusCode == 200) {
      List<dynamic> data = json.decode(response.body);
      return data;
    } else {
      return "error";
    }
  }

  Future getCryptoHistory(String id, String days) async {
    Map<String, String> requestHeaders = {"content-type": "application/json; charset=utf-8", 'Origin': "https://app.ihave.io", 'Access-Control-Allow-Origin': '*'};
    Map<String, String> queryParameters = {"vs_currency": "usd", "days": days};
    var url = new Uri.https(coinGeckoURL, "/api/v3/coins/${id.toLowerCase()}/market_chart", queryParameters);
    var response = await client.get(url, headers: requestHeaders);
    if (response.statusCode == 200) {
      return json.decode(response.body);
    }
    throw Exception("Unable to fetch data from server");
  }

  addGoogle(String token, String idOnSnGoogle) async {
    Map<String, String> requestHeaders = {'Context-Type': 'application/json;charSet=UTF-8', "content-type": "application/json", 'Authorization': 'bearer ' + token, 'Origin': "https://app.ihave.io", 'Access-Control-Allow-Origin': '*'};
    var url;
    url = new Uri.https(baseURL, "/profile/UpdateProfile");
    final Map<String, dynamic> dataToSend = {"idOnSn2": idOnSnGoogle};
    var checkAvailability = await sattSocialSignIn(idOnSnGoogle, "2");
    if (checkAvailability != "" && checkAvailability != null && checkAvailability != "null" && checkAvailability != "error") {
      return "error account exists";
    } else {
      var body = jsonEncode(dataToSend);
      var response = await client.put(url, headers: requestHeaders, body: body);
      if (response.statusCode == 201) {
        idOnSn2 = idOnSnGoogle;
        return "success";
      } else {
        return "error";
      }
    }
  }

  addFacebook(String token, String idOnSnfacebook) async {
    Map<String, String> requestHeaders = {'Context-Type': 'application/json;charSet=UTF-8', "content-type": "application/json", 'Authorization': 'bearer ' + token, 'Origin': "https://app.ihave.io", 'Access-Control-Allow-Origin': '*'};
    var url;
    url = new Uri.https(baseURL, "/profile/UpdateProfile");
    final Map<String, dynamic> dataToSend = {"idOnSn": idOnSnfacebook};
    var checkAvailability = await sattSocialSignIn(idOnSnfacebook, "1");
    if (checkAvailability != "" && checkAvailability != null && checkAvailability != "null" && checkAvailability != "error") {
      return "error account exists";
    } else {
      var body = jsonEncode(dataToSend);
      var response = await client.put(url, headers: requestHeaders, body: body);
      if (response.statusCode == 201) {
        idOnSn = idOnSnfacebook;
        return "success";
      }
      return "error";
    }
  }

  removeSocial(String token, String social) async {
    Map<String, String> requestHeaders = {'Context-Type': 'application/json;charSet=UTF-8', 'Authorization': 'Bearer ' + token, 'Origin': "https://app.ihave.io", 'Access-Control-Allow-Origin': '*'};
    var url;
    url = new Uri.https(
      baseURL,
      "/auth/disconnect/$social",
    );
    var response = await client.put(url, headers: requestHeaders);
    if (response.statusCode == 200) {
      var jsonResponse = json.decode(response.body);
      if (social == "facebook") {
        idOnSn = "";
      } else {
        idOnSn2 = "";
      }
      return "success";
    }
    return "error";
  }

  Future<List<NFT>> getOpenSeaAssets(String walletAdress) async {
    Map<String, String> requestHeaders = {
      'Context-Type': 'application/json;charSet=UTF-8',
      'x-api-key': 'XTlQw08gzfJghqmqlNj04eJjKdp0HXrN9f94itrX2BmzyggKHcTzFfuGuu8J7w0j',
    };
    var url;
    walletAdress = walletAdress != "" ? walletAdress : this.walletAddress ?? "";
    url = new Uri.https(
      openSeaBaseUrl,
      '/api/v2/$walletAdress/nft',
    );

    var response = await client.get(
      url,
      headers: requestHeaders,
    );
    if (response.statusCode == 200) {
      var jsonResponse = json.decode(response.body);
      List<NFT> nfts = <NFT>[];
      var data = jsonResponse["result"];
      data.forEach((element) {
        var nft = NFT.fromJson(element);
        nfts.add(nft);
      });
      return nfts;
    }
    return <NFT>[];
  }

  getSocialPicture(String userPictureSocial) async {
    Map<String, String> requestHeaders = {'Content-Type': 'application/json;charSet=UTF-8', 'Origin': "https://app.ihave.io", 'Access-Control-Allow-Origin': '*'};
    var url;
    url = Uri.parse(userPictureSocial);
    var response = await client.get(
      url,
      headers: requestHeaders,
    );
    if (response.statusCode == 200) {
      return "success";
    }
    return "error";
  }
}
