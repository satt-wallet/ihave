import 'dart:async';
import 'dart:convert';
import 'package:eth_sig_util/eth_sig_util.dart';
import 'package:eth_sig_util/util/utils.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_svg/svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:ihave/main.dart';
import 'package:one_context/one_context.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:wallet_connect/wallet_connect.dart';
import 'package:web3dart/web3dart.dart';

import '../controllers/LoginController.dart';
import '../controllers/walletController.dart';
import '../util/utils.dart';

const maticRpcUri = 'https://bsc-dataseed.binance.org/';
const maticRpcUriEth = 'https://eth.llamarpc.com';

class WCBGService extends Material {
  late SharedPreferences _prefs;
  String walletAddress = "";
  EthPrivateKey? privateKey;
  late double balance;
  late Wallet wallet;
  var context;
  String userToken = "";
  bool connected = false;
  WCSessionStore? _sessionStore;
  bool _isVisible = false;
  bool _isProfileVisible = false;
  bool _isWalletVisible = false;
  bool _isNotificationVisible = false;
  int localChainId = 56;
  late StreamSubscription subscription;
  bool _hasNetwork = true;
  int _selectedIndex = 0;
  bool _obscureText = true;
  bool _obscureText2 = true;
  bool _approveClicked = false;
  bool _dialogTransactionPasswordOpened = false;
  final TextEditingController _transactionPassword = new TextEditingController();
  bool passwrong = false;
  FocusNode _transactionPasswordFocus = new FocusNode();
  final _web3client = Web3Client(
    maticRpcUri,
    http.Client(),
  );
  final _web3clientEth = Web3Client(
    maticRpcUriEth,
    http.Client(),
  );

  WCBGService(BuildContext context, String userToken) {
    this.userToken = userToken;
    this.context = MyApp.materialKey.currentContext ?? context;
    this.localChainId = localChainId;
  }

  onSignTransaction(
    int id,
    WCEthereumTransaction ethereumTransaction,
    WCClient wcClient,
    double cryptoPrice,
  ) {
    onTransaction(
      id: id,
      ethereumTransaction: ethereumTransaction,
      title: 'Sign Transaction',
      onConfirm: () async {
        final creds = privateKey;
        var clientRPC = localChainId == 56 ? _web3client : _web3clientEth;
        final tx = await clientRPC.signTransaction(
          creds!,
          _wcEthTxToWeb3Tx(ethereumTransaction),
          chainId: wcClient.chainId!,
        );
        final txhash = await clientRPC.sendRawTransaction(tx);
        debugPrint('txhash $txhash');
        wcClient.approveRequest<String>(
          id: id,
          result: bytesToHex(tx),
        );
        Navigator.of(MyApp.materialKey.currentContext ?? context, rootNavigator: true).pop();
      },
      onReject: () {
        wcClient.rejectRequest(id: id);
      },
      wcClient: wcClient,
      cryptoPrice: cryptoPrice,
    );
  }

  onSendTransaction(
    int id,
    WCEthereumTransaction ethereumTransaction,
    WCClient wcClient,
    double cryptoPrice,
  ) {
    onTransaction(
        id: id,
        ethereumTransaction: ethereumTransaction,
        title: 'Send Transaction',
        onConfirm: () async {
          var clientRPC = localChainId == 56 ? _web3client : _web3clientEth;
          final creds = privateKey ?? (await generateKey()).privateKey;
          final txhash = await clientRPC.sendTransaction(
            creds,
            _wcEthTxToWeb3Tx(ethereumTransaction),
            chainId: wcClient.chainId!,
          );
          debugPrint('txhash $txhash');
          wcClient.approveRequest<String>(
            id: id,
            result: txhash,
          );
          Navigator.of(MyApp.materialKey.currentContext ?? context, rootNavigator: true).pop();
        },
        onReject: () {
          wcClient.rejectRequest(id: id);
          Navigator.of(MyApp.materialKey.currentContext ?? context, rootNavigator: true).pop();
        },
        wcClient: wcClient,
        cryptoPrice: cryptoPrice);
  }

  onTransaction({required int id, required WCEthereumTransaction ethereumTransaction, required String title, required VoidCallback onConfirm, required VoidCallback onReject, required WCClient wcClient, required double cryptoPrice}) async {
    ContractFunction? contractFunction;
    BigInt gasInGwei = BigInt.parse(ethereumTransaction.gasPrice ?? '0');
    double gasPrice = calculateGas(gasInGwei, cryptoPrice);

    try {
      final abiUrl = handleAbiUrl(ethereumTransaction);
      final res = await http.get(Uri.parse(abiUrl));
      final Map<String, dynamic> resMap = jsonDecode(res.body);
      final abi = ContractAbi.fromJson(resMap['result'], '');
      final contract = DeployedContract(abi, EthereumAddress.fromHex(ethereumTransaction.to ?? ""));
      final dataBytes = hexToBytes(ethereumTransaction.data ?? "");
      final funcBytes = dataBytes.take(4).toList();
      debugPrint("funcBytes $funcBytes");
      final maibiFunctions = contract.functions.where((element) => listEquals<int>(element.selector, funcBytes));
      if (maibiFunctions.isNotEmpty) {
        debugPrint("isNotEmpty");
        contractFunction = maibiFunctions.first;
        debugPrint("function ${contractFunction.name}");
      }
    } catch (e, trace) {
      debugPrint("failed to decode\n$e\n$trace");
    }
    if (gasPrice == BigInt.zero || gasPrice <= 0) {
      var clientRPC = localChainId == 56 ? _web3client : _web3clientEth;
      gasInGwei = await clientRPC.estimateGas();
      gasPrice = calculateGas(gasInGwei, cryptoPrice);
    }
    showDialog(
        context: MyApp.materialKey.currentContext ?? context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return WillPopScope(
            onWillPop: () => Future.value(false),
            child: StatefulBuilder(builder: (context, setState) {
              return AlertDialog(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(30)),
                ),
                content: SingleChildScrollView(
                  child: Column(
                    children: [
                      Container(
                        child: wcClient.remotePeerMeta!.icons.isNotEmpty
                            ? Container(
                                height: 100.0,
                                width: 100.0,
                                padding: EdgeInsets.only(bottom: 8.0, top: 8.0),
                                child: Image.network(
                                  wcClient.remotePeerMeta!.icons.first,
                                  width: 80,
                                  height: 80,
                                ),
                              )
                            : null,
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 8.0, left: 15.0, right: 15.0, bottom: 8.0),
                        child: Text(
                          wcClient.remotePeerMeta!.name,
                          style: GoogleFonts.poppins(
                            fontSize: 17,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                      Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.only(bottom: 8.0, top: 8.0),
                        child: Text(
                          title,
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 18.0,
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 8.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Receipient',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 16.0,
                              ),
                            ),
                            const SizedBox(height: 8.0),
                            Text(
                              '${ethereumTransaction.to}',
                              style: TextStyle(fontSize: 16.0),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 8.0),
                        child: Column(
                          children: [
                            Row(
                              children: [
                                Expanded(
                                  flex: 2,
                                  child: Text(
                                    'Transaction Fee',
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16.0,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            Row(
                              children: [
                                Expanded(
                                  child: Text(
                                    localChainId == 56 ? '${EthConversions.weiToEthUnTrimmed(gasInGwei * BigInt.parse(ethereumTransaction.gas ?? '0'), 18)} BNB' : '${EthConversions.weiToEthUnTrimmed(gasInGwei * BigInt.parse(ethereumTransaction.gas ?? '0'), 18)} ETH',
                                    style: TextStyle(fontSize: 16.0),
                                  ),
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 0.0),
                        child: Column(
                          children: [],
                        ),
                      ),
                      if (contractFunction != null)
                        Padding(
                          padding: EdgeInsets.only(bottom: 4.0, right: MediaQuery.of(MyApp.materialKey.currentContext ?? context).size.width * 0.2),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                'Function',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 16.0,
                                ),
                              ),
                              const SizedBox(height: 2.0),
                              Text(
                                '${contractFunction.name}',
                                style: TextStyle(fontSize: 16.0),
                              ),
                            ],
                          ),
                        ),
                      Theme(
                        data: Theme.of(MyApp.materialKey.currentContext ?? context).copyWith(dividerColor: Colors.transparent),
                        child: Padding(
                          padding: EdgeInsets.only(bottom: 4.0),
                          child: ExpansionTile(
                            tilePadding: EdgeInsets.zero,
                            title: Text(
                              'Data',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 16.0,
                              ),
                            ),
                            children: [
                              Text(
                                '${ethereumTransaction.data}',
                                style: TextStyle(fontSize: 16.0),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: MediaQuery.of(MyApp.materialKey.currentContext ?? context).size.width * 0.02),
                        child: Row(
                          children: [
                            Expanded(
                              child: TextButton(
                                style: ButtonStyle(
                                  backgroundColor: MaterialStateProperty.all(
                                    Color(0XFFF52079),
                                  ),
                                  shape: MaterialStateProperty.all(
                                    RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(MediaQuery.of(MyApp.materialKey.currentContext ?? context).size.width * 0.08),
                                    ),
                                  ),
                                ),
                                onPressed: onReject,
                                child: Text(
                                  'REJECT',
                                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16.0, color: Colors.white),
                                ),
                              ),
                            ),
                            const SizedBox(width: 5.0),
                            Expanded(
                              child: TextButton(
                                style: ButtonStyle(
                                  backgroundColor: MaterialStateProperty.all(
                                    Color(0XFF4048FF),
                                  ),
                                  shape: MaterialStateProperty.all(
                                    RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(MediaQuery.of(MyApp.materialKey.currentContext ?? context).size.width * 0.08),
                                    ),
                                  ),
                                ),
                                onPressed: onConfirm,
                                child: Text(
                                  'CONFIRM',
                                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16.0, color: Colors.white),
                                ),
                              ),
                            ),
                            const SizedBox(width: 5.0),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              );
            }),
          );
        });
  }

  _onSign(int id, WCEthereumSignMessage ethereumSignMessage, WCClient wcClient) {
    final decoded = (ethereumSignMessage.type == WCSignType.TYPED_MESSAGE) ? ethereumSignMessage.data! : ascii.decode(hexToBytes(ethereumSignMessage.data!));
    showDialog(
      context: MyApp.materialKey.currentContext ?? context,
      barrierDismissible: false,
      builder: (_) {
        return WillPopScope(
          onWillPop: () => Future.value(false),
          child: SimpleDialog(
            title: Column(
              children: [
                if (wcClient.remotePeerMeta!.icons.isNotEmpty)
                  Container(
                    height: 100.0,
                    width: 100.0,
                    padding: const EdgeInsets.only(bottom: 8.0),
                    child: Image.network(wcClient.remotePeerMeta!.icons.first),
                  ),
                Text(
                  wcClient.remotePeerMeta!.name,
                  style: TextStyle(
                    fontWeight: FontWeight.normal,
                    fontSize: 20.0,
                  ),
                ),
              ],
            ),
            contentPadding: const EdgeInsets.fromLTRB(16.0, 12.0, 16.0, 16.0),
            children: [
              Container(
                alignment: Alignment.center,
                padding: const EdgeInsets.only(bottom: 8.0),
                child: Text(
                  'Sign Message',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 18.0,
                  ),
                ),
              ),
              Theme(
                data: Theme.of(MyApp.materialKey.currentContext ?? context).copyWith(dividerColor: Colors.transparent),
                child: Padding(
                  padding: const EdgeInsets.only(bottom: 8.0),
                  child: ExpansionTile(
                    tilePadding: EdgeInsets.zero,
                    title: Text(
                      'Message',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 16.0,
                      ),
                    ),
                    children: [
                      Text(
                        decoded,
                        style: TextStyle(fontSize: 16.0),
                      ),
                    ],
                  ),
                ),
              ),
              Row(
                children: [
                  Expanded(
                    child: TextButton(
                      style: TextButton.styleFrom(
                        primary: Colors.white,
                        backgroundColor: Theme.of(MyApp.materialKey.currentContext ?? context).colorScheme.secondary,
                      ),
                      onPressed: () async {
                        String signedDataHex;
                        if (ethereumSignMessage.type == WCSignType.TYPED_MESSAGE) {
                          signedDataHex = EthSigUtil.signTypedData(
                            privateKey: privateKey?.privateKeyInt?.toString() ?? "",
                            jsonData: ethereumSignMessage.data!,
                            version: TypedDataVersion.V4,
                          );
                        } else {
                          final creds = privateKey ?? (await generateKey()).privateKey;
                          final encodedMessage = hexToBytes(ethereumSignMessage.data!);
                          final signedData = await creds.signPersonalMessage(encodedMessage);
                          signedDataHex = bytesToHex(signedData, include0x: true);
                        }
                        debugPrint('SIGNED $signedDataHex');
                        if (signedDataHex != "") {
                          wcClient.approveRequest<String>(
                            id: id,
                            result: signedDataHex,
                          );
                          Navigator.of(MyApp.materialKey.currentContext ?? context, rootNavigator: true).pop();
                        }
                      },
                      child: Text('SIGN'),
                    ),
                  ),
                  const SizedBox(width: 16.0),
                  Expanded(
                    child: TextButton(
                      style: TextButton.styleFrom(
                        primary: Colors.white,
                        backgroundColor: Theme.of(MyApp.materialKey.currentContext ?? context).colorScheme.secondary,
                      ),
                      onPressed: () {
                        wcClient.rejectRequest(id: id);
                        Navigator.pop(MyApp.materialKey.currentContext ?? context);
                      },
                      child: Text('REJECT'),
                    ),
                  ),
                ],
              ),
            ],
          ),
        );
      },
    );
  }

  Transaction _wcEthTxToWeb3Tx(WCEthereumTransaction ethereumTransaction) {
    return Transaction(
      from: EthereumAddress.fromHex(ethereumTransaction.from),
      to: EthereumAddress.fromHex(ethereumTransaction.to ?? ""),
      maxGas: ethereumTransaction.gasLimit != null ? int.tryParse(ethereumTransaction.gasLimit!) : null,
      gasPrice: ethereumTransaction.gasPrice != null ? EtherAmount.inWei(BigInt.parse(ethereumTransaction.gasPrice!)) : null,
      value: EtherAmount.inWei(BigInt.parse(ethereumTransaction.value ?? '0')),
      data: hexToBytes(ethereumTransaction.data ?? ""),
      nonce: ethereumTransaction.nonce != null ? int.tryParse(ethereumTransaction.nonce!) : null,
    );
  }

  handleAbiUrl(WCEthereumTransaction ethereumTransaction) {
    if (localChainId == 56) {
      return 'https://api.bscscan.com/api?module=contract&action=getabi&address=${ethereumTransaction.to}&apikey=1KK5VK8HGMSJ86WVDBXH2MGIT8CXKQN6QP';
    } else {
      return 'https://api.etherscan.com/api?module=contract&action=getabi&address=${ethereumTransaction.to}&apikey=8S1PRHAZAMHPU4YTC4HU5YT2MT21B1BQ3J';
    }
  }

  double calculateGas(BigInt gasInGwei, double cryptoPrice) {
    var gas = 0.0;
    gas = ((gasInGwei * BigInt.parse("60000")) / BigInt.parse("1000000000000000000")) * cryptoPrice;
    return gas;
  }

  Future generateKey() async {
    var result = await LoginController.apiService.getBlockchainKey(this.userToken, _transactionPassword.text);
    return result;
  }
}
