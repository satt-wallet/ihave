import 'dart:async';
import 'dart:convert';

import 'package:collection/src/iterable_extensions.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:ihave/controllers/LoginController.dart';
import 'package:ihave/controllers/walletController.dart';
import 'package:ihave/model/crypto.dart';
import 'package:ihave/model/searchCrypto.dart';
import 'package:ihave/service/apiService.dart';
import 'package:ihave/ui/WelcomeScreen.dart';
import 'package:ihave/ui/staticElements/headerNavigation.dart';
import 'package:ihave/util/utils.dart';
import 'package:ihave/widgets/customAppBar.dart';
import 'package:ihave/widgets/customBottomBar.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:provider/provider.dart';
import 'package:contained_tab_bar_view/contained_tab_bar_view.dart';

class AddToken extends StatefulWidget {
  AddToken({Key? key}) : super(key: key);

  @override
  _AddTokenState createState() => _AddTokenState();
}

class _AddTokenState extends State<AddToken> {
  APIService apiService = LoginController.apiService;
  final _formKeyAdd = GlobalObjectKey<FormState>(1);
  String _selectedNetwork = "ERC20";
  bool isCryptoAddressValid = true;
  String decimals = "";
  String cryptoContract = "";
  String tokenSymbol = "";
  bool _isVisible = false;
  bool _isProfileVisible = false;
  bool _isWalletVisible = false;
  bool _isNotificationVisible = false;
  bool _smartContractValid = true;
  bool _smartContractRequiredField = false;
  bool _tokenSymbolRequiredField = false;
  bool _decimalRequiredField = false;
  bool _tokenSymbolReadOnly = false;
  bool _tokenDecimalReadOnly = false;
  bool _searchTokenClicked = true;
  bool _addTokenClicked = false;
  bool _tokenFound = false;
  bool _tokenErrorFound = false;
  bool _cryptoSelected = false;
  int _initialIndex = 0;
  bool _tokenfield = false;
  GlobalKey<ContainedTabBarViewState> tabKey = GlobalKey<ContainedTabBarViewState>();
  late TabController controller;
  final TextEditingController _decimalscontroller = TextEditingController();
  final TextEditingController _cryptoContractcontroller = TextEditingController();
  final TextEditingController _tokenSymbolcontroller = TextEditingController();
  late StreamSubscription subscription;
  String tokenName = "";
  bool _hasNetwork = true;
  List<SearchCrypto> _searchCryptoList = <SearchCrypto>[];

  SearchCrypto? _selectedCrypto;

  TextEditingController _search = new TextEditingController();

  bool _tokenAlreadyExists = false;

  @override
  void initState() {
    subscription = InternetConnectionChecker().onStatusChange.listen((status) {
      final hasInternet = status == InternetConnectionStatus.connected;
      setState(() {
        _hasNetwork = hasInternet;
      });
    });
    fetchCryptosToSearch().then((value) {
      setState(() {
        _searchCryptoList.clear();
        _searchCryptoList.addAll(value);
        filterList();
      });
    });
    Provider.of<LoginController>(context, listen: false).tokenSymbol == "";
    Provider.of<LoginController>(context, listen: false).tokenNetwork == "";
    super.initState();
  }

  @override
  void dispose() {
    subscription.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    setState(() {
      _isVisible = Provider.of<WalletController>(context, listen: true).isSideMenuVisible;
      _isProfileVisible = Provider.of<WalletController>(context, listen: true).isProfileVisible;
      _isWalletVisible = Provider.of<WalletController>(context, listen: true).isWalletVisible;
      _isNotificationVisible = Provider.of<WalletController>(context, listen: true).isNotificationVisible;
    });
    if (_hasNetwork) {
      return WillPopScope(
          onWillPop: () {
            gotoPageGlobal(context, WelcomeScreen());
            return Future.value(false);
          },
          child: GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
              Provider.of<WalletController>(context, listen: false).isSideMenuVisible = false;
              Provider.of<WalletController>(context, listen: false).isProfileVisible = false;
              Provider.of<WalletController>(context, listen: false).isWalletVisible = false;
              Provider.of<WalletController>(context, listen: false).isNotificationVisible = false;
              setState(() {
                _isVisible = false;
                _isProfileVisible = false;
                _isWalletVisible = false;
                _isNotificationVisible = false;
              });
            },
            child: Scaffold(
              backgroundColor: Colors.white,
              bottomNavigationBar: CustomBottomBar(),
              appBar: CustomAppBar(),
              body: Stack(
                children: [
                  Center(
                    child: SingleChildScrollView(
                      child: Padding(
                        padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.028, bottom: MediaQuery.of(context).size.height * 0.03),
                        child: Container(
                          width: MediaQuery.of(context).size.width * 0.95,
                          child: Column(
                            children: [
                              Stack(
                                children: [
                                  Padding(padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.03, top: MediaQuery.of(context).size.width * 0.02)),
                                  Align(
                                    alignment: Alignment.topLeft,
                                    child: Text(
                                      "Add Token ",
                                      style: GoogleFonts.poppins(color: Colors.black, fontSize: MediaQuery.of(context).size.height * 0.03, fontWeight: FontWeight.w700, letterSpacing: 0.7),
                                    ),
                                  ),
                                ],
                              ),
                              Padding(
                                padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0),
                                child: Stack(
                                  children: [
                                    Container(
                                      height: MediaQuery.of(context).size.height,
                                      width: MediaQuery.of(context).size.width * 0.95,
                                      child: ContainedTabBarView(
                                        key: tabKey,
                                        tabBarProperties: TabBarProperties(
                                          alignment: TabBarAlignment.center,
                                          indicatorColor: Color(0xFF4048FF),
                                          indicatorWeight: 1,
                                        ),
                                        tabs: [
                                          Align(
                                            alignment: Alignment.center,
                                            child: Text(
                                              'Custom Token',
                                              textAlign: TextAlign.center,
                                              style: !_addTokenClicked
                                                  ? GoogleFonts.poppins(color: Color(0xFF87879c), fontWeight: FontWeight.w600, letterSpacing: 0.7, fontSize: MediaQuery.of(context).size.width * 0.042)
                                                  : GoogleFonts.poppins(color: Color(0xFF4048FF), fontWeight: FontWeight.w600, fontSize: MediaQuery.of(context).size.width * 0.045),
                                            ),
                                          ),
                                        ],
                                        views: [
                                          Form(
                                            key: _formKeyAdd,
                                            child: Column(
                                              children: [
                                                Expanded(
                                                  child: Container(
                                                      alignment: Alignment.topCenter,
                                                      margin: EdgeInsets.only(
                                                        left: MediaQuery.of(context).size.width * 0.05,
                                                        right: MediaQuery.of(context).size.width * 0.05,
                                                        top: MediaQuery.of(context).size.width * 0.07,
                                                      ),
                                                      child: Column(children: [
                                                        Padding(
                                                          padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.01, bottom: MediaQuery.of(context).size.height * 0.013),
                                                          child: Row(
                                                            mainAxisAlignment: MainAxisAlignment.center,
                                                            children: [
                                                              Spacer(),
                                                              Padding(
                                                                padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.03),
                                                                child: SizedBox(
                                                                  height: MediaQuery.of(context).size.height * 0.05,
                                                                  width: MediaQuery.of(context).size.width * 0.35,
                                                                  child: ElevatedButton(
                                                                    style: ButtonStyle(
                                                                      backgroundColor: MaterialStateProperty.all(
                                                                        _selectedNetwork == "ERC20" ? Color(0xFFf6f6ff) : Color(0xFFFFFFFF),
                                                                      ),
                                                                      shape: MaterialStateProperty.all(
                                                                        RoundedRectangleBorder(borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.075), side: BorderSide(width: 1, color: Color(0xFFd6d6e8))),
                                                                      ),
                                                                      elevation: MaterialStateProperty.resolveWith<double>(
                                                                        (Set<MaterialState> states) {
                                                                          if (states.contains(MaterialState.disabled)) {
                                                                            return 0;
                                                                          }
                                                                          return 0; // Defer to the widget's default.
                                                                        },
                                                                      ),
                                                                    ),
                                                                    onPressed: () {
                                                                      setState(() {
                                                                        _formKeyAdd.currentState?.reset();
                                                                        _selectedNetwork = 'ERC20';
                                                                        _tokenSymbolcontroller.text = "";
                                                                        _decimalscontroller.text = "";
                                                                        _cryptoContractcontroller.text = "";
                                                                        _tokenFound = false;
                                                                        _tokenErrorFound = false;
                                                                        _smartContractValid = true;
                                                                        _smartContractRequiredField = false;
                                                                        _tokenSymbolRequiredField = false;
                                                                        _decimalRequiredField = false;
                                                                        isCryptoAddressValid = true;
                                                                      });
                                                                    },
                                                                    child: Text(
                                                                      "ERC 20",
                                                                      style: GoogleFonts.poppins(color: Colors.black54, fontSize: MediaQuery.of(context).size.width * 0.045, fontWeight: FontWeight.w600, letterSpacing: 1.2),
                                                                    ),
                                                                  ),
                                                                ),
                                                              ),
                                                              Padding(
                                                                padding: const EdgeInsets.all(0),
                                                                child: SizedBox(
                                                                  height: MediaQuery.of(context).size.height * 0.05,
                                                                  width: MediaQuery.of(context).size.width * 0.35,
                                                                  child: ElevatedButton(
                                                                    style: ButtonStyle(
                                                                      backgroundColor: MaterialStateProperty.all(
                                                                        _selectedNetwork == "BEP20" ? Color(0xFFf6f6ff) : Color(0xFFFFFFFF),
                                                                      ),
                                                                      shape: MaterialStateProperty.all(
                                                                        RoundedRectangleBorder(borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.06), side: BorderSide(width: 1, color: Color(0xFFd6d6e8))),
                                                                      ),
                                                                      elevation: MaterialStateProperty.resolveWith<double>(
                                                                        (Set<MaterialState> states) {
                                                                          if (states.contains(MaterialState.disabled)) {
                                                                            return 0;
                                                                          }
                                                                          return 0; // Defer to the widget's default.
                                                                        },
                                                                      ),
                                                                    ),
                                                                    onPressed: () {
                                                                      setState(() {
                                                                        _formKeyAdd.currentState?.reset();
                                                                        _selectedNetwork = 'BEP20';
                                                                        _tokenSymbolcontroller.text = "";
                                                                        _decimalscontroller.text = "";
                                                                        _cryptoContractcontroller.text = "";
                                                                        _tokenFound = false;
                                                                        _tokenErrorFound = false;
                                                                        _smartContractValid = true;
                                                                        _smartContractRequiredField = false;
                                                                        _tokenSymbolRequiredField = false;
                                                                        _decimalRequiredField = false;
                                                                        isCryptoAddressValid = true;
                                                                      });
                                                                    },
                                                                    child: Text(
                                                                      "BEP 20",
                                                                      style: GoogleFonts.poppins(color: Colors.black54, fontSize: MediaQuery.of(context).size.width * 0.045, fontWeight: FontWeight.w600, letterSpacing: 1.2),
                                                                    ),
                                                                  ),
                                                                ),
                                                              ),
                                                              Spacer(),
                                                            ],
                                                          ),
                                                        ),
                                                        Row(
                                                          mainAxisAlignment: MainAxisAlignment.start,
                                                          children: [
                                                            Padding(
                                                              padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.043, top: MediaQuery.of(context).size.height * 0.015),
                                                              child: Text(
                                                                "TOKEN'S CONTRACT ADDRESS",
                                                                textAlign: TextAlign.start,
                                                                style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.height * 0.018, fontWeight: FontWeight.w600, color: Color(0xFF75758f)),
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                        Padding(
                                                          padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.01, left: MediaQuery.of(context).size.width * 0.02, right: MediaQuery.of(context).size.width * 0.02),
                                                          child: TextFormField(
                                                            controller: _cryptoContractcontroller,
                                                            onChanged: (value) async {
                                                              if (value.length > 0) {
                                                                setState(() {
                                                                  _smartContractRequiredField = false;
                                                                  _tokenFound = false;
                                                                  _tokenAlreadyExists = false;
                                                                });
                                                                cryptoContract = value;
                                                                _cryptoContractcontroller.text = value;
                                                                _cryptoContractcontroller.selection = TextSelection.fromPosition(TextPosition(offset: _cryptoContractcontroller.text.length));
                                                                if (isContractValid(_cryptoContractcontroller.text)) {
                                                                  await parseCryptoInfo(context);
                                                                } else {
                                                                  setState(() {
                                                                    _smartContractValid = false;
                                                                    _tokenDecimalReadOnly = false;
                                                                    _tokenSymbolReadOnly = false;
                                                                    _tokenFound = false;
                                                                    _tokenErrorFound = false;
                                                                    isCryptoAddressValid = false;
                                                                    _tokenSymbolcontroller.text = "";
                                                                    _decimalscontroller.text = "";
                                                                  });
                                                                }
                                                              } else {
                                                                setState(() {
                                                                  _smartContractRequiredField = true;
                                                                  _smartContractValid = true;
                                                                  isCryptoAddressValid = false;
                                                                  _decimalscontroller.text = "";
                                                                  _tokenSymbolcontroller.text = "";
                                                                  _tokenFound = false;
                                                                });
                                                              }
                                                            },
                                                            decoration: InputDecoration(
                                                                fillColor: _tokenFound ? Color(0xFFf6f6ff) : Colors.white,
                                                                focusColor: Colors.white,
                                                                hoverColor: Colors.white,
                                                                filled: true,
                                                                isDense: true,
                                                                suffixIcon: _tokenFound
                                                                    ? Padding(
                                                                        padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.012),
                                                                        child: Container(
                                                                          margin: EdgeInsets.only(top: MediaQuery.of(context).size.width * 0.03, bottom: MediaQuery.of(context).size.width * 0.03),
                                                                          child: SvgPicture.asset(
                                                                            "images/true.svg",
                                                                            height: MediaQuery.of(context).size.width * 0.01,
                                                                            width: MediaQuery.of(context).size.width * 0.01,
                                                                          ),
                                                                        ),
                                                                      )
                                                                    : (_tokenErrorFound || _tokenAlreadyExists
                                                                        ? InkWell(
                                                                            onTap: () {
                                                                              _cryptoContractcontroller.text = "";
                                                                            },
                                                                            child: Padding(
                                                                              padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.011),
                                                                              child: Container(
                                                                                margin: EdgeInsets.only(top: MediaQuery.of(context).size.width * 0.03, bottom: MediaQuery.of(context).size.width * 0.03),
                                                                                child: SvgPicture.asset(
                                                                                  "images/false.svg",
                                                                                  height: MediaQuery.of(context).size.width * 0.01,
                                                                                  width: MediaQuery.of(context).size.width * 0.01,
                                                                                ),
                                                                              ),
                                                                            ),
                                                                          )
                                                                        : null),
                                                                contentPadding: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width * 0.03, vertical: MediaQuery.of(context).size.height * 0.02),
                                                                enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: !isCryptoAddressValid ? Color(0xFFf52079) : Colors.grey.withOpacity(0.5))),
                                                                focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: !isCryptoAddressValid ? Color(0xFFf52079) : Colors.grey.withOpacity(0.5))),
                                                                errorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red)),
                                                                focusedErrorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red))),
                                                          ),
                                                        ),
                                                        Padding(
                                                          padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.06),
                                                          child: _tokenFound
                                                              ? Align(
                                                                  alignment: Alignment.bottomLeft,
                                                                  child: Text(
                                                                    "Token found",
                                                                    style: GoogleFonts.poppins(fontWeight: FontWeight.w400, color: Color(0xFF00CC9E)),
                                                                  ),
                                                                )
                                                              : null,
                                                        ),
                                                        SizedBox(
                                                            child: !_smartContractValid
                                                                ? Align(
                                                                    alignment: Alignment.bottomLeft,
                                                                    child: Padding(
                                                                      padding: const EdgeInsets.only(left: 30, bottom: 10),
                                                                      child: Text(
                                                                        "Incorrect address format",
                                                                        style: GoogleFonts.poppins(color: Color(0xFFf52079), fontSize: MediaQuery.of(context).size.width * 0.028, fontWeight: FontWeight.bold),
                                                                      ),
                                                                    ),
                                                                  )
                                                                : (_smartContractRequiredField
                                                                    ? Align(
                                                                        alignment: Alignment.bottomLeft,
                                                                        child: Padding(
                                                                          padding: const EdgeInsets.only(left: 30, bottom: 10),
                                                                          child: Text(
                                                                            "Required field",
                                                                            style: GoogleFonts.poppins(color: Color(0xFFf52079), fontSize: MediaQuery.of(context).size.width * 0.028, fontWeight: FontWeight.bold),
                                                                          ),
                                                                        ),
                                                                      )
                                                                    : (_tokenfield
                                                                        ? Align(
                                                                            alignment: Alignment.bottomLeft,
                                                                            child: !isCryptoAddressValid
                                                                                ? Padding(
                                                                                    padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.06, top: MediaQuery.of(context).size.height * 0.01),
                                                                                    child: Text(
                                                                                      "Incorrect address and / or network",
                                                                                      style: GoogleFonts.poppins(color: Color(0xFFf52079), fontSize: MediaQuery.of(context).size.width * 0.03, fontWeight: FontWeight.bold),
                                                                                    ),
                                                                                  )
                                                                                : null,
                                                                          )
                                                                        : _tokenAlreadyExists
                                                                            ? Align(
                                                                                alignment: Alignment.bottomLeft,
                                                                                child: Padding(
                                                                                  padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.06, top: MediaQuery.of(context).size.height * 0.01),
                                                                                  child: Text(
                                                                                    "This token already exists in your list.",
                                                                                    style: GoogleFonts.poppins(color: Color(0xFFf52079), fontSize: MediaQuery.of(context).size.width * 0.03, fontWeight: FontWeight.bold),
                                                                                  ),
                                                                                ),
                                                                              )
                                                                            : null))),
                                                        Row(
                                                          mainAxisAlignment: MainAxisAlignment.start,
                                                          children: [
                                                            Padding(
                                                              padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.043, top: MediaQuery.of(context).size.height * 0.02),
                                                              child: Text(
                                                                "TOKEN'S SYMBOL",
                                                                textAlign: TextAlign.start,
                                                                style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.height * 0.018, fontWeight: FontWeight.w600, color: Color(0xFF75758f)),
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                        Padding(
                                                          padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.01, left: MediaQuery.of(context).size.width * 0.02, right: MediaQuery.of(context).size.width * 0.02),
                                                          child: TextFormField(
                                                            controller: _tokenSymbolcontroller,
                                                            enableInteractiveSelection: true,
                                                            onChanged: (value) {
                                                              if (value.length == 0) {
                                                                setState(() {
                                                                  _tokenSymbolRequiredField = true;
                                                                  _tokenfield = false;
                                                                });
                                                              } else {
                                                                setState(() {
                                                                  _tokenSymbolRequiredField = false;
                                                                  _tokenfield = true;
                                                                });
                                                                tokenSymbol = value;
                                                                _tokenSymbolcontroller.selection = TextSelection.fromPosition(TextPosition(offset: _tokenSymbolcontroller.text.length));
                                                              }
                                                            },
                                                            toolbarOptions: ToolbarOptions(paste: true, cut: true, selectAll: true, copy: true),
                                                            readOnly: _tokenSymbolReadOnly ? true : false,
                                                            decoration: InputDecoration(
                                                                fillColor: _tokenFound ? Color(0xFFf6f6ff) : Colors.white,
                                                                focusColor: Colors.white,
                                                                hoverColor: Colors.white,
                                                                filled: true,
                                                                isDense: true,
                                                                contentPadding: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width * 0.05, vertical: MediaQuery.of(context).size.height * 0.02),
                                                                enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.grey.withOpacity(0.5))),
                                                                focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.grey.withOpacity(0.5))),
                                                                errorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red)),
                                                                focusedErrorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red))),
                                                          ),
                                                        ),
                                                        SizedBox(
                                                            child: _tokenSymbolRequiredField
                                                                ? Align(
                                                                    alignment: Alignment.bottomLeft,
                                                                    child: Padding(
                                                                      padding: const EdgeInsets.only(left: 30, bottom: 10),
                                                                      child: Text(
                                                                        "Required field",
                                                                        style: GoogleFonts.poppins(color: Color(0xFFf52079), fontSize: MediaQuery.of(context).size.width * 0.028, fontWeight: FontWeight.bold),
                                                                      ),
                                                                    ),
                                                                  )
                                                                : null),
                                                        Row(
                                                          mainAxisAlignment: MainAxisAlignment.start,
                                                          children: [
                                                            Padding(
                                                              padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.043, top: MediaQuery.of(context).size.height * 0.02),
                                                              child: Text(
                                                                "DECIMALS",
                                                                textAlign: TextAlign.start,
                                                                style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.height * 0.018, fontWeight: FontWeight.w600, color: Color(0xFF75758f)),
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                        Padding(
                                                          padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.01, left: MediaQuery.of(context).size.width * 0.02, right: MediaQuery.of(context).size.width * 0.02),
                                                          child: TextFormField(
                                                            keyboardType: TextInputType.number,
                                                            controller: _decimalscontroller,
                                                            inputFormatters: <TextInputFormatter>[
                                                              FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
                                                            ],
                                                            onChanged: (value) {
                                                              if (value.length == 0) {
                                                                setState(() {
                                                                  _decimalRequiredField = true;
                                                                });
                                                              } else {
                                                                setState(() {
                                                                  _decimalRequiredField = false;
                                                                });
                                                                _decimalscontroller.text = value;
                                                                decimals = _decimalscontroller.text;
                                                                _decimalscontroller.selection = TextSelection.fromPosition(TextPosition(offset: _decimalscontroller.text.length));
                                                              }
                                                            },
                                                            readOnly: _tokenDecimalReadOnly ? true : false,
                                                            decoration: InputDecoration(
                                                                fillColor: _tokenFound ? Color(0xFFf6f6ff) : Colors.white,
                                                                focusColor: Colors.white,
                                                                hoverColor: Colors.white,
                                                                filled: true,
                                                                isDense: true,
                                                                contentPadding: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width * 0.05, vertical: MediaQuery.of(context).size.height * 0.02),
                                                                enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.grey.withOpacity(0.5))),
                                                                focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.grey.withOpacity(0.5))),
                                                                errorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red)),
                                                                focusedErrorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red))),
                                                          ),
                                                        ),
                                                        SizedBox(
                                                          child: _decimalRequiredField
                                                              ? Align(
                                                                  alignment: Alignment.bottomLeft,
                                                                  child: Padding(
                                                                    padding: const EdgeInsets.only(left: 30, bottom: 10),
                                                                    child: Text(
                                                                      "Required field",
                                                                      style: GoogleFonts.poppins(color: Color(0xFFf52079), fontSize: MediaQuery.of(context).size.width * 0.028, fontWeight: FontWeight.bold),
                                                                    ),
                                                                  ),
                                                                )
                                                              : null,
                                                        ),
                                                        Padding(
                                                          padding: EdgeInsets.only(
                                                            top: MediaQuery.of(context).size.height * 0.035,
                                                          ),
                                                          child: Row(
                                                            children: [
                                                              Spacer(),
                                                              SizedBox(
                                                                width: MediaQuery.of(context).size.width * 0.4,
                                                                height: MediaQuery.of(context).size.height * 0.075,
                                                                child: ElevatedButton(
                                                                    style: ButtonStyle(
                                                                      backgroundColor: MaterialStateProperty.all(
                                                                        Color(0xFFFFFFFF),
                                                                      ),
                                                                      shape: MaterialStateProperty.all(
                                                                        RoundedRectangleBorder(
                                                                          borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.08),
                                                                        ),
                                                                      ),
                                                                    ),
                                                                    onPressed: () => {
                                                                          setState(() {
                                                                            _decimalscontroller.text = "";
                                                                            _cryptoContractcontroller.text = "";
                                                                            _tokenSymbolcontroller.text = "";
                                                                            _smartContractRequiredField = false;
                                                                            _smartContractValid = true;
                                                                            isCryptoAddressValid = true;
                                                                            _tokenSymbolRequiredField = false;
                                                                            _decimalRequiredField = false;
                                                                            _tokenFound = false;
                                                                          })
                                                                        },
                                                                    child: Text(
                                                                      "Cancel",
                                                                      style: GoogleFonts.poppins(color: Color(0XFF4048FF), fontWeight: FontWeight.w600, fontSize: MediaQuery.of(context).size.height * 0.02),
                                                                    )),
                                                              ),
                                                              Spacer(),
                                                              Padding(
                                                                padding: const EdgeInsets.all(0),
                                                                child: (_tokenFound && (_tokenSymbolcontroller.text != "") && (_decimalscontroller.text != ""))
                                                                    ? SizedBox(
                                                                        width: MediaQuery.of(context).size.width * 0.4,
                                                                        height: MediaQuery.of(context).size.height * 0.075,
                                                                        child: ElevatedButton(
                                                                          style: ButtonStyle(
                                                                            backgroundColor: MaterialStateProperty.all(
                                                                              Color(0XFF4048FF),
                                                                            ),
                                                                            shape: MaterialStateProperty.all(
                                                                              RoundedRectangleBorder(
                                                                                borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.08),
                                                                              ),
                                                                            ),
                                                                          ),
                                                                          onPressed: () async => {
                                                                            if (_formKeyAdd.currentState!.validate())
                                                                              {
                                                                                await save(_decimalscontroller.text, _cryptoContractcontroller.text, _tokenSymbolcontroller.text, _selectedNetwork, tokenName),
                                                                                fetchCrypto(),
                                                                                gotoPageGlobal(context, WelcomeScreen()),
                                                                              }
                                                                          },
                                                                          child: Center(
                                                                            child: Text(
                                                                              "Add Token",
                                                                              textAlign: TextAlign.center,
                                                                              style: GoogleFonts.poppins(color: Colors.white, fontWeight: FontWeight.w600, fontSize: MediaQuery.of(context).size.height * 0.02),
                                                                            ),
                                                                          ),
                                                                        ),
                                                                      )
                                                                    : Container(
                                                                        width: MediaQuery.of(context).size.width * 0.4,
                                                                        height: MediaQuery.of(context).size.height * 0.075,
                                                                        decoration: BoxDecoration(
                                                                          borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.08),
                                                                          color: Color(0xFFd6d6e8),
                                                                        ),
                                                                        child: Center(
                                                                          child: Text(
                                                                            "Add Token",
                                                                            textAlign: TextAlign.center,
                                                                            style: GoogleFonts.poppins(color: Color(0xFFADADC8), fontWeight: FontWeight.w600, fontSize: MediaQuery.of(context).size.height * 0.02),
                                                                          ),
                                                                        )),
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                      ])),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                        onChange: (index) {
                                          if (index == 0) {
                                            setState(() {
                                              _searchTokenClicked = true;
                                              _addTokenClicked = false;
                                            });
                                          } else if (index == 1) {
                                            setState(() {
                                              _searchTokenClicked = false;
                                              _addTokenClicked = true;
                                            });
                                          }
                                        },
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                  HeaderNavigation(_isVisible, _isWalletVisible, _isNotificationVisible)
                ],
              ),
            ),
          ));
    } else {
      return Stack(
        children: [
          Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            color: Colors.white,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Center(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      "No Internet Connection , please check your network",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(color: Colors.black, fontSize: 14),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: ElevatedButton(
                    onPressed: () {},
                    child: Text("Retry"),
                  ),
                ),
              ],
            ),
          )
        ],
      );
    }
  }

  Future<void> parseCryptoInfo(BuildContext context) async {
    setState(() {
      _smartContractValid = true;
      _tokenFound = false;
      _tokenErrorFound = false;
    });
    var cryptoCheckResponse;
    if (Provider.of<WalletController>(context, listen: false).cryptos.any((element) => element.contract?.toLowerCase() == _cryptoContractcontroller.text.toLowerCase())) {
      cryptoCheckResponse = "Already Exists";
    } else {
      cryptoCheckResponse = await apiService.getCryptoInfo(Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "", _cryptoContractcontroller.text, _selectedNetwork);
    }
    if (cryptoCheckResponse == "error") {
      setState(() {
        _tokenAlreadyExists = false;
        _tokenfield = true;
        _tokenFound = false;
        _tokenErrorFound = true;
        isCryptoAddressValid = false;
        _tokenDecimalReadOnly = false;
        _tokenSymbolReadOnly = false;
      });
    } else if (cryptoCheckResponse == "Already Exists") {
      setState(() {
        _tokenAlreadyExists = true;
        _tokenfield = false;
        _tokenFound = false;
        _tokenErrorFound = false;
        isCryptoAddressValid = true;
        _tokenDecimalReadOnly = false;
        _tokenSymbolReadOnly = false;
      });
    } else {
      setState(() {
        _tokenFound = true;
        _tokenErrorFound = false;
        _tokenAlreadyExists = false;
      });
      _tokenSymbolcontroller.text = cryptoCheckResponse["data"]["symbol"];
      if (_tokenSymbolcontroller.text != null && _tokenSymbolcontroller.text != "") {
        setState(() {
          _tokenSymbolReadOnly = true;
        });
      }
      _decimalscontroller.text = cryptoCheckResponse["data"]["decimal"];
      if (_decimalscontroller.text != null && _decimalscontroller.text != "") {
        setState(() {
          _tokenDecimalReadOnly = true;
        });
      }
      tokenName = cryptoCheckResponse["data"]["tokenName"];
      setState(() {
        isCryptoAddressValid = true;
        _decimalRequiredField = false;
        _tokenSymbolRequiredField = false;
      });
    }
  }

  bool isContractValid(String value) {
    RegExp exp = new RegExp(r"^0x[a-fA-F0-9]{40}$|^(bc1|[13])[a-zA-HJ-NP-Z0-9]{25,39}$");
    var test = exp.pattern;
    var test2 = exp.allMatches(value);
    if (exp.hasMatch(value)) {
      return true;
    }
    return false;
  }

  Future save(String decimal, String cryptoContract, String symbol, String network, String name) async {
    var token = Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "";
    if (isCryptoAddressValid) {
      var result = await apiService.addCustomCrypto(token, decimal, cryptoContract, symbol, network, name);
      setState(() {
        _decimalscontroller.text = "";
        _cryptoContractcontroller.text = "";
        _tokenSymbolcontroller.text = "";
        _smartContractRequiredField = false;
        _smartContractValid = true;
        isCryptoAddressValid = true;
        _tokenSymbolRequiredField = false;
        _decimalRequiredField = false;
        _tokenFound = false;
        _tokenErrorFound = false;
      });
      if (result["error"] != null && result["error"] != "") {
        Fluttertoast.showToast(
          msg: result["error"],
          backgroundColor: Color(0xFFF52079),
        );
      } else {
        Fluttertoast.showToast(
          msg: "Token added successfully",
          backgroundColor: Color.fromRGBO(0, 151, 117, 1),
        );
      }
    }
  }

  Future<List<SearchCrypto>> fetchCryptosToSearch() async {
    var cryptoList = await apiService.cryptosToAdd(Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "");
    List<SearchCrypto> cryptos = <SearchCrypto>[];
    cryptos.addAll(cryptoList);
    cryptos.forEach((e) {
      if (e.contract == null || e.contract == "") {
        cryptoList.remove(e);
      }
    });
    Provider.of<WalletController>(context, listen: false).cryptosToAdd = cryptoList;
    return cryptoList;
  }

  filterList() {
    var cryptolist = Provider.of<WalletController>(context, listen: false).cryptos;
    cryptolist.forEach((e) {
      var symbol = e.symbol;
      SearchCrypto? toRemove = _searchCryptoList.firstWhereOrNull((s) => s.symbol == symbol);

      setState(() {
        if (toRemove != null) {
          _searchCryptoList.remove(toRemove);
        }
      });
    });
  }

  fetchCrypto() {
    setState(() {
      Provider.of<WalletController>(context, listen: false).cryptos.clear();
      Provider.of<WalletController>(context, listen: false).cryptos = [];
    });
  }

  searchList(String text) {
    setState(() {
      if (text != null && text != "") {
        _searchCryptoList = Provider.of<WalletController>(context, listen: false).cryptosToAdd;
        var searchListName = _searchCryptoList.where((e) => e.name!.toLowerCase().contains(text.toLowerCase())).toList();
        var searchListSymbol = _searchCryptoList.where((e) => e.symbol?.toLowerCase() == text.toLowerCase()).toList();
        var searchResult = searchListName;
        searchResult.addAll(searchListSymbol);
        _searchCryptoList = searchResult.toSet().toList();
      } else {
        _searchCryptoList = Provider.of<WalletController>(context, listen: false).cryptosToAdd;
      }
      filterList();
    });
  }
}
