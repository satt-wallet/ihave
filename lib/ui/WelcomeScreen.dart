import 'dart:async';
import 'package:convex_bottom_bar/convex_bottom_bar.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:ihave/controllers/LoginController.dart';
import 'package:ihave/controllers/walletController.dart';
import 'package:ihave/model/crypto.dart';
import 'package:ihave/ui/WebViewWidget.dart';
import 'package:ihave/ui/biometrics.dart';
import 'package:ihave/ui/buy.dart';
import 'package:ihave/ui/cryptoDetails.dart';
import 'package:ihave/ui/dashboard.dart';
import 'package:ihave/ui/homePage.dart';
import 'package:ihave/ui/mywallet.dart';
import 'package:ihave/ui/nftPage.dart';
import 'package:ihave/ui/notificationPage.dart';
import 'package:ihave/ui/request.dart';
import 'package:ihave/ui/send.dart';
import 'package:ihave/ui/staticElements/headerNavigation.dart';
import 'package:ihave/util/hiveUtils.dart';
import 'package:ihave/widgets/DeleteDialog.dart';
import 'package:ihave/widgets/customAppBar.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:neoversion/neoversion.dart';
import 'package:provider/provider.dart';
import 'package:one_context/one_context.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:web3dart/credentials.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:collection/collection.dart';

import '../main.dart';
import '../service/apiService.dart';
import '../util/neoVersionOverriden.dart';
import '../util/utils.dart';

class WelcomeScreen extends StatefulWidget {
  const WelcomeScreen({Key? key}) : super(key: key);

  @override
  _WelcomeScreenState createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> {
  late StreamSubscription subscription;
  bool _isVisible = false;
  bool _isProfileVisible = false;
  bool _isWalletVisible = false;
  bool _isNotificationVisible = false;
  int _selectedIndex = 2;
  bool _sendPage = false;
  double iconsSize = 50;
  bool _hasNetwork = true;
  bool _buyCryptoClicked = false;
  bool _isLoading = false;
  Timer? _timer;
  double fabHeight = 0;
  double fabwidth = 0;
  double itemheight = 0;
  double itemwidth = 0;
  double itemTextSize = 0;
  String? firebaseToken = " ";
  APIService apiService = LoginController.apiService;
  bool _isCryptoSelected = false;
  Crypto? selectedCrypto;
  bool errorRequest = false;
  bool errorSend = false;
  bool _obscureText = true;
  TextEditingController _WalletPassword = TextEditingController();
  bool _checkValue = false;
  bool _loading = false;

  tronWallet(BuildContext context) async {
    setState(() {
      _loading = false;
    });
    var prefs = await SharedPreferences.getInstance();
    if (prefs.getString("tron-check") != "null" || prefs.getString("tron-check") == "false") {
      if (!Provider.of<WalletController>(context, listen: false).checkTron) {
        var result = await LoginController.apiService.walletTronCheck(Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "");
        if (result == null) {
          Provider.of<WalletController>(context, listen: false).checkTron = true;
          prefs.setString("tron-check", "false");
          showDialog(
              context: context,
              barrierDismissible: false,
              builder: (BuildContext context) {
                return StatefulBuilder(builder: (context, setState) {
                  return Scaffold(
                    resizeToAvoidBottomInset: true,
                    body: Dialog(
                      backgroundColor: Colors.transparent,
                      insetPadding: EdgeInsets.zero,
                      child: SingleChildScrollView(
                        child: Container(
                          height: MediaQuery.of(context).size.height,
                          width: MediaQuery.of(context).size.width,
                          decoration: BoxDecoration(
                            color: Colors.white.withOpacity(0.9),
                          ),
                          child: Column(
                            children: <Widget>[
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Spacer(),
                                  Spacer(),
                                  Padding(
                                    padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.01, top: MediaQuery.of(context).size.width * 0.06),
                                    child: IconButton(
                                      alignment: Alignment.topLeft,
                                      onPressed: () {
                                        if (_checkValue) {
                                          prefs.setString("tron-check", "true");
                                        } else {
                                          prefs.setString("tron-check", "false");
                                        }
                                        Navigator.of(context).pop();
                                      },
                                      splashColor: Colors.transparent,
                                      highlightColor: Colors.transparent,
                                      icon: Icon(
                                        Icons.close,
                                        size: MediaQuery.of(context).size.width * 0.05,
                                        color: Colors.black.withOpacity(0.5),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: MediaQuery.of(context).size.height * 0.035,
                              ),
                              Image.asset(
                                "images/tron-wallet.png",
                              ),
                              Text(
                                "Create your Tron wallet",
                                style: GoogleFonts.poppins(fontWeight: FontWeight.w700, fontSize: MediaQuery.of(context).size.height * 0.025, color: Colors.black),
                              ),
                              Padding(
                                padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.075, right: MediaQuery.of(context).size.width * 0.075, top: MediaQuery.of(context).size.height * 0.01),
                                child: Text(
                                  "You are about to delete this token. You now have the possibility to create your Tron ​​wallet.You Need to just enter your transaction password to create it ",
                                  textAlign: TextAlign.center,
                                  style: GoogleFonts.poppins(fontWeight: FontWeight.w400, fontSize: MediaQuery.of(context).size.height * 0.0175, color: const Color(0XFF162746), letterSpacing: 1.1),
                                ),
                              ),
                              Align(
                                alignment: Alignment.topLeft,
                                child: Padding(
                                  padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.1, top: MediaQuery.of(context).size.height * 0.035),
                                  child: Text(
                                    "TRANSACTION PASSWORD",
                                    style: GoogleFonts.poppins(fontWeight: FontWeight.w600, color: const Color(0XFF75758F)),
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: MediaQuery.of(context).size.height * 0.01,
                              ),
                              Container(
                                width: MediaQuery.of(context).size.width * 0.8,
                                child: TextFormField(
                                  style: TextStyle(fontSize: MediaQuery.of(context).size.width * 0.045),
                                  textAlignVertical: TextAlignVertical.center,
                                  textAlign: TextAlign.left,
                                  controller: _WalletPassword,
                                  keyboardType: TextInputType.visiblePassword,
                                  obscureText: _obscureText,
                                  onChanged: (value) {
                                    TextSelection previousSelection = _WalletPassword.selection;
                                    _WalletPassword.text = value;
                                    _WalletPassword.selection = previousSelection;

                                    TextSelection.fromPosition(TextPosition(offset: _WalletPassword.text.length));
                                  },
                                  validator: (value) {
                                    if (value!.isEmpty) {
                                      return 'Field required';
                                    }
                                    return null;
                                  },
                                  decoration: InputDecoration(
                                      fillColor: Colors.white,
                                      focusColor: Colors.white,
                                      hoverColor: Colors.white,
                                      filled: true,
                                      isDense: true,
                                      prefixIcon: Padding(
                                        padding: EdgeInsets.all(MediaQuery.of(context).size.width * 0.017),
                                        child: SvgPicture.asset(
                                          "images/keyIcon.svg",
                                          color: Colors.orange,
                                          height: MediaQuery.of(context).size.width * 0.06,
                                          width: MediaQuery.of(context).size.width * 0.06,
                                        ),
                                      ),
                                      suffixIcon: Padding(
                                        padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.025),
                                        child: IconButton(
                                            onPressed: () {
                                              setState(() {
                                                _obscureText = !_obscureText;
                                              });
                                            },
                                            icon: _obscureText
                                                ? SvgPicture.asset(
                                                    "images/visibility-icon-off.svg",
                                                    height: MediaQuery.of(context).size.height * 0.02,
                                                  )
                                                : SvgPicture.asset(
                                                    "images/visibility-icon-on.svg",
                                                    height: MediaQuery.of(context).size.height * 0.02,
                                                  )),
                                      ),
                                      contentPadding: EdgeInsets.symmetric(vertical: 5, horizontal: 5),
                                      enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.white)),
                                      focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.white)),
                                      errorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red)),
                                      focusedErrorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red))),
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.02),
                                child: SizedBox(
                                  width: MediaQuery.of(context).size.width * 0.6,
                                  child: ElevatedButton(
                                    child: Padding(
                                      padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.015, bottom: MediaQuery.of(context).size.height * 0.015),
                                      child: _loading
                                          ? CircularProgressIndicator(
                                              color: Colors.white,
                                            )
                                          : Text(
                                              "Create Wallet",
                                              style: GoogleFonts.poppins(fontWeight: FontWeight.w700, fontSize: MediaQuery.of(context).size.height * 0.017),
                                            ),
                                    ),
                                    onPressed: !_loading
                                        ? () async {
                                            setState(() {
                                              _loading = true;
                                            });
                                            var result = await LoginController.apiService.createTronWallet(Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "", _WalletPassword.text);
                                            switch (result) {
                                              case "success":
                                                Navigator.of(context).pop();
                                                prefs.setString("tron-check", "true");
                                                Fluttertoast.showToast(msg: "Wallet tron created successfully", backgroundColor: Color.fromRGBO(0, 151, 117, 1), toastLength: Toast.LENGTH_LONG);
                                                break;
                                              case "exist":
                                                Navigator.of(context).pop();
                                                prefs.setString("tron-check", "false");
                                                Fluttertoast.showToast(msg: "You have a tron wallet", backgroundColor: Color(0xFFF52079), toastLength: Toast.LENGTH_LONG);
                                                break;
                                              case "wrong password":
                                                setState(() {
                                                  _WalletPassword.text = "";
                                                  _loading = false;
                                                });
                                                Fluttertoast.showToast(msg: "Wrong password", backgroundColor: Color(0xFFF52079), toastLength: Toast.LENGTH_LONG);
                                                break;
                                              case "error":
                                                Navigator.of(context).pop();
                                                prefs.setString("tron-check", "false");
                                                Fluttertoast.showToast(msg: "Something went wrong please try again", backgroundColor: Color(0xFFF52079), toastLength: Toast.LENGTH_LONG);
                                                break;
                                            }
                                          }
                                        : null,
                                    style: ButtonStyle(
                                      backgroundColor: MaterialStateProperty.all(
                                        Color(0xFF4048FF),
                                      ),
                                      shape: MaterialStateProperty.all(
                                        RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.08),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.012),
                                child: SizedBox(
                                  width: MediaQuery.of(context).size.width * 0.6,
                                  child: ElevatedButton(
                                    child: Padding(
                                      padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.015, bottom: MediaQuery.of(context).size.height * 0.015),
                                      child: Text(
                                        "Skip for now",
                                        style: GoogleFonts.poppins(fontWeight: FontWeight.w700, fontSize: MediaQuery.of(context).size.height * 0.017, color: const Color(0xFF4048FF)),
                                      ),
                                    ),
                                    onPressed: () {
                                      if (_checkValue) {
                                        prefs.setString("tron-check", "true");
                                      } else {
                                        prefs.setString("tron-check", "false");
                                      }
                                      Navigator.of(context).pop();
                                    },
                                    style: ButtonStyle(
                                      backgroundColor: MaterialStateProperty.all(
                                        Colors.white,
                                      ),
                                      shape: MaterialStateProperty.all(
                                        RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.08),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Align(
                                alignment: Alignment.topCenter,
                                child: Padding(
                                  padding: EdgeInsets.fromLTRB(0, MediaQuery.of(context).size.height * 0.025, 0, 0),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Container(
                                        decoration: BoxDecoration(color: _checkValue ? Colors.white : Colors.transparent, border: Border.all(color: _checkValue ? Color(0xFF00CC9E) : Color(0xFFADADC8), width: 2.5), borderRadius: BorderRadius.all(Radius.circular(5))),
                                        width: 18,
                                        height: 18,
                                        child: Theme(
                                          data: ThemeData(
                                              checkboxTheme: CheckboxThemeData(
                                                shape: RoundedRectangleBorder(
                                                  borderRadius: BorderRadius.circular(5),
                                                ),
                                              ),
                                              unselectedWidgetColor: Colors.transparent),
                                          child: Checkbox(
                                            checkColor: Color(0xFF00CC9E),
                                            activeColor: Colors.transparent,
                                            value: _checkValue,
                                            onChanged: (value) {
                                              setState(() {
                                                _checkValue = value!;
                                              });
                                            },
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.03),
                                        child: Text("Mute future reminders", style: GoogleFonts.poppins(fontWeight: FontWeight.w400, color: const Color(0XFF323754))),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  );
                });
              });
        } else {
          prefs.setString("tron-check", "true");
        }
      }
    }
  }

  static List<BoxDecoration> _decoration = <BoxDecoration>[
    BoxDecoration(
      gradient: LinearGradient(colors: [Color(0xFF1F2337).withOpacity(0.5), Colors.transparent, Colors.transparent, Colors.transparent, Colors.transparent], begin: const FractionalOffset(0.0, 0.0), end: const FractionalOffset(0.0, 1.0), stops: [0.0, 0.0, 0.0, 0.0, 1.0], tileMode: TileMode.clamp),
    ),
    BoxDecoration(
      gradient: LinearGradient(
        colors: [const Color(0XFF4048FF), const Color(0XFF1F2337)],
        begin: const FractionalOffset(0.0, -0.05),
        end: const FractionalOffset(0.0, 0.35),
      ),
    ),
    BoxDecoration(
        gradient: LinearGradient(
      colors: [Color(0xFF4048FF), Color(0xFF1F2337), Color(0xFF1F2337), Color(0xFF1F2337), Color(0xFF1F2337)],
      begin: const FractionalOffset(0.0, 0.0),
      end: const FractionalOffset(0.0, 1.0),
    )),
    BoxDecoration(
      gradient: LinearGradient(
        colors: [const Color(0XFF03C7A3), const Color(0XFF00147A)],
        begin: const FractionalOffset(0.0, -0.05),
        end: const FractionalOffset(0.0, 0.35),
      ),
    ),
    BoxDecoration(
      color: Colors.white,
    ),
    BoxDecoration(
        gradient: LinearGradient(
      colors: [
        Color.fromRGBO(64, 72, 255, 1),
        Color.fromRGBO(0, 0, 1, 1),
      ],
      begin: const FractionalOffset(0.0, 0.0),
      end: const FractionalOffset(0.0, 1.0),
      stops: [0.0, 1.0],
      tileMode: TileMode.clamp,
    )),
    BoxDecoration(
      gradient: LinearGradient(
        colors: [const Color(0XFF4048FF), const Color(0XFF1F2337)],
        begin: const FractionalOffset(0.0, -0.05),
        end: const FractionalOffset(0.0, 0.35),
      ),
    ),
    BoxDecoration(
        gradient: LinearGradient(
      colors: [
        Color(0xFFFFA318),
        Color(0xFF34281E),
        Color(0xFF34281E),
      ],
      begin: const FractionalOffset(0.0, -0.2),
      end: const FractionalOffset(0.0, 1.0),
    )),
    BoxDecoration(
      gradient: LinearGradient(
        colors: [const Color(0XFF03C7A3), const Color(0XFF00147A)],
        begin: const FractionalOffset(0.0, -0.05),
        end: const FractionalOffset(0.0, 0.35),
      ),
    ),
    BoxDecoration(
        gradient: LinearGradient(
      colors: [
        Color(0xFF4048FF),
        Color(0xFF1F2337),
      ],
      begin: const FractionalOffset(0.0, 0.0),
      end: const FractionalOffset(0.0, 1.0),
    )),
    BoxDecoration(
        gradient: LinearGradient(
      colors: [
        Color(0xFF4048FF),
        Color(0xFF1F2337),
      ],
      begin: const FractionalOffset(0.0, 0.0),
      end: const FractionalOffset(0.0, 1.0),
    )),
  ];

  List<Widget> _widgetOptions = <Widget>[
    Center(
      child: WebViewNavigator(),
    ),
    Center(
      child: Send(),
    ),
    Center(
      child: Dashboard(),
    ),
    Center(
      child: Request(),
    ),
    Center(
      child: NftPage(),
    ),
    Center(
      child: Dashboard(),
    ),
    Center(
      child: Send(),
    ),
    Center(
      child: BuyPage(),
    ),
    Center(
      child: Request(),
    ),
    Center(
      child: CryptoDetails(),
    ),
    Center(
      child: MyWallet(),
    ),
  ];

  Widget appBar() {
    return ConvexAppBar(
      top: -(MediaQuery.of(context).size.width * 0.045),
      height: MediaQuery.of(context).size.width * 0.158,
      curveSize: MediaQuery.of(context).size.width * 0.14,
      disableDefaultTabController: false,
      elevation: 8,
      backgroundColor: Colors.white,
      style: TabStyle.fixed,
      items: [
        TabItem(
          icon: Padding(
            padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
            child: Column(
              children: [
                Image.asset(
                  'images/navigateur.png',
                  height: MediaQuery.of(context).size.width * 0.055,
                  width: MediaQuery.of(context).size.width * 0.055,
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 1, 0, 0),
                  child: Text(
                    "Browser",
                    style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.026, fontWeight: FontWeight.w600, color: Color(0xFF4048FF)),
                  ),
                ),
              ],
            ),
          ),
        ),
        TabItem(
          icon: Padding(
            padding: const EdgeInsets.fromLTRB(0, 1, 0, 0),
            child: Column(
              children: [
                SvgPicture.asset(
                  'images/send-icon.svg',
                  height: MediaQuery.of(context).size.width * 0.055,
                  width: MediaQuery.of(context).size.width * 0.055,
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 1, 0, 0),
                  child: Text(
                    "Send",
                    style: GoogleFonts.poppins(
                      fontSize: MediaQuery.of(context).size.width * 0.026,
                      fontWeight: FontWeight.w600,
                      color: Color(0xFF4048FF),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        TabItem(
          icon: Padding(
            padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
            child: Column(
              children: [
                SizedBox(
                  height: MediaQuery.of(context).size.width * 0.13,
                  width: MediaQuery.of(context).size.width * 0.13,
                  child: CircleAvatar(
                    backgroundColor: Color(0xFF4048FF),
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(6, 0, 0, 3),
                      child: SvgPicture.asset(
                        'images/walleticone.svg',
                        height: MediaQuery.of(context).size.width * 0.1,
                        width: MediaQuery.of(context).size.width * 0.1,
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 3, 0, 0),
                  child: Text(
                    "Wallet",
                    style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.026, fontWeight: FontWeight.w600, color: Color(0xFF4048FF)),
                  ),
                ),
              ],
            ),
          ),
        ),
        TabItem(
          icon: Padding(
            padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
            child: Column(
              children: [
                SvgPicture.asset(
                  'images/request-icon.svg',
                  height: MediaQuery.of(context).size.width * 0.055,
                  width: MediaQuery.of(context).size.width * 0.055,
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 1, 0, 0),
                  child: Text(
                    "Request",
                    style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.025, fontWeight: FontWeight.w600, color: Color(0xFF4048FF)),
                  ),
                ),
              ],
            ),
          ),
        ),
        TabItem(
          icon: Padding(
            padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
            child: Column(
              children: [
                SvgPicture.asset(
                  'images/nft.svg',
                  height: MediaQuery.of(context).size.width * 0.055,
                  width: MediaQuery.of(context).size.width * 0.055,
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 1, 0, 0),
                  child: Text(
                    "NFT",
                    style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.026, fontWeight: FontWeight.w600, color: Color(0xFF4048FF)),
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
      onTap: !_isLoading
          ? (int i) {
              _onItemTapped(i);
            }
          : null,
    );
  }

  Widget appBarSelected() {
    return ConvexAppBar(
      top: -(MediaQuery.of(context).size.width * 0.045),
      height: MediaQuery.of(context).size.width * 0.158,
      curveSize: MediaQuery.of(context).size.width * 0.14,
      disableDefaultTabController: false,
      elevation: 8,
      backgroundColor: Color(0xFF7A7FFF),
      style: TabStyle.fixed,
      items: [
        TabItem(
          icon: Padding(
            padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
            child: Column(
              children: [
                SvgPicture.asset(
                  'images/delete.svg',
                  height: MediaQuery.of(context).size.width * 0.055,
                  width: MediaQuery.of(context).size.width * 0.055,
                  color: selectedCrypto?.addedToken == true ? Colors.white : Colors.white.withOpacity(0.2),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 1, 0, 0),
                  child: Text(
                    "Delete",
                    style: GoogleFonts.poppins(
                      fontSize: MediaQuery.of(context).size.width * 0.026,
                      fontWeight: FontWeight.w600,
                      color: selectedCrypto?.addedToken == true ? Colors.white : Colors.white.withOpacity(0.2),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        TabItem(
          icon: Padding(
            padding: const EdgeInsets.fromLTRB(0, 1, 0, 0),
            child: Column(
              children: [
                SvgPicture.asset('images/send-icon.svg', height: MediaQuery.of(context).size.width * 0.055, width: MediaQuery.of(context).size.width * 0.055, color: Colors.white),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 1, 0, 0),
                  child: Text(
                    "Send",
                    style: GoogleFonts.poppins(
                      fontSize: MediaQuery.of(context).size.width * 0.026,
                      fontWeight: FontWeight.w600,
                      color: Colors.white,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        TabItem(
          icon: Padding(
            padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
            child: Column(
              children: [
                SizedBox(
                  height: MediaQuery.of(context).size.width * 0.13,
                  width: MediaQuery.of(context).size.width * 0.13,
                  child: CircleAvatar(
                    backgroundColor: Colors.white,
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                      child: SvgPicture.asset(
                        'images/new-buy-token.svg',
                        height: MediaQuery.of(context).size.width * 0.055,
                        width: MediaQuery.of(context).size.width * 0.055,
                        color: Color(0xFF7A7FFF),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 3, 0, 0),
                  child: Text(
                    "Buy",
                    style: GoogleFonts.poppins(
                      fontSize: MediaQuery.of(context).size.width * 0.026,
                      fontWeight: FontWeight.w600,
                      color: Colors.white,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        TabItem(
          icon: Padding(
            padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
            child: Column(
              children: [
                SvgPicture.asset(
                  'images/request-icon.svg',
                  height: MediaQuery.of(context).size.width * 0.055,
                  width: MediaQuery.of(context).size.width * 0.055,
                  color: Colors.white,
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 1, 0, 0),
                  child: Text(
                    "Request",
                    style: GoogleFonts.poppins(
                      fontSize: MediaQuery.of(context).size.width * 0.025,
                      fontWeight: FontWeight.w600,
                      color: Colors.white,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        TabItem(
          icon: Padding(
            padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
            child: Column(
              children: [
                SvgPicture.asset(
                  'images/search.svg',
                  height: MediaQuery.of(context).size.width * 0.055,
                  width: MediaQuery.of(context).size.width * 0.055,
                  color: Colors.white,
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 1, 0, 0),
                  child: Text(
                    "Details",
                    style: GoogleFonts.poppins(
                      fontSize: MediaQuery.of(context).size.width * 0.026,
                      fontWeight: FontWeight.w600,
                      color: Colors.white,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
      onTap: !_isLoading
          ? (int i) {
              _onItemTapped(i + 5);
            }
          : null,
    );
  }

  void _onItemTapped(int index) async {
    setState(() {
      Provider.of<LoginController>(context, listen: false).errorSendCrypto = false;
      Provider.of<LoginController>(context, listen: false).errorRequestCrypto = false;
    });
    Provider.of<WalletController>(context, listen: false).toggleWelcomeScreen(true);
    if (index == _selectedIndex) {
      setState(() {
        _isLoading = true;
      });
      await Future.delayed(const Duration(seconds: 2), () {
        setState(() {
          _isLoading = false;
        });
      });
      gotoPageGlobal(context, WelcomeScreen());
    }
    setState(() {
      _selectedIndex = Provider.of<WalletController>(context, listen: false).selectedIndex;
      Provider.of<WalletController>(context, listen: false).buyCryptoCurrency = false;
      if (index == 9) {
        _selectedIndex = handleMarketCap(Provider.of<LoginController>(context, listen: false).marketCupCrypto);
        Provider.of<WalletController>(context, listen: false).selectedIndex = _selectedIndex;
      } else if (index == 5) {
        if (selectedCrypto?.addedToken == true) {
          deleteCrypto(Provider.of<LoginController>(context, listen: false).tokenSymbol);
          Provider.of<WalletController>(context, listen: false).toggleSelectedCrypto(false);
        } else {
          Provider.of<WalletController>(context, listen: false).toggleSelectedCrypto(true);
        }
        Provider.of<WalletController>(context, listen: false).selectedIndex = _selectedIndex;
        _selectedIndex = _selectedIndex;
      } else {
        Provider.of<WalletController>(context, listen: false).selectedIndex = index;
        _selectedIndex = index;
        Provider.of<WalletController>(context, listen: false).toggleSelectedCrypto(false);
      }
      Provider.of<WalletController>(context, listen: false).isSideMenuVisible = false;
      Provider.of<WalletController>(context, listen: false).isProfileVisible = false;
      Provider.of<WalletController>(context, listen: false).isWalletVisible = false;
      Provider.of<WalletController>(context, listen: false).isNotificationVisible = false;
    });
  }

  Future getUserPicture() async {
    String userToken = Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "";
    var picture = await LoginController.apiService.getUserPicture(userToken);
    Provider.of<LoginController>(context, listen: false).userPicture = picture != "error" ? picture : null;
  }

  Future getUserToken() async {
    if (!LoginController.apiService.hasBiometric || !LoginController.apiService.is2FA) {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString('access_token', Provider.of<LoginController>(context, listen: false).userDetails!.userToken ?? "");
    }
  }

  Future getAddress() async {
    if (apiService.walletAddress == null || apiService.walletAddress == "" || apiService.walletAddress == "") {
      await apiService.getWalletAddress(Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "", "v1");
      await apiService.getWalletAddress(Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "", "v2");
    }
    return apiService.walletAddress;
  }

  @override
  void initState() {
    tronWallet(context);
    _isCryptoSelected = false;
    Future.wait([
      getUserPicture(),
      getAddress(),
      getUserToken(),
    ]).then((value) {}).catchError((error) {
      print(error);
    });

    _buyCryptoClicked = false;
    subscription = InternetConnectionChecker().onStatusChange.listen((status) {
      final hasInternet = status == InternetConnectionStatus.connected;
      setState(() {
        _hasNetwork = hasInternet;
        _buyCryptoClicked = true;
      });
    });
    super.initState();
    final neoVersion = NeoVersionOverriden(
      iOSAppId: '1610750323',
      androidAppId: 'us.atayen.ihave',
    );
    const simpleBehavior = true;

    if (simpleBehavior) {
      basicStatusCheck(neoVersion);
    } else {
      advancedStatusCheck(neoVersion);
    }
  }

  basicStatusCheck(NeoVersionOverriden neoVersion) {
    neoVersion.showAlertIfNecessary(context: context);
  }

  advancedStatusCheck(NeoVersionOverriden neoVersion) async {
    final status = await neoVersion.getVersionStatus();
    neoVersion.showUpdateDialog(
      context: context,
      status: status,
      title: 'Update',
      dialogText: (String local, String appstore, bool dismissable) => 'Custom Text - local: $local, appstore: $appstore, dismissable: $dismissable',
    );
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    setState(() {
      _isVisible = Provider.of<WalletController>(context, listen: true).isSideMenuVisible;
      errorRequest = context.watch<LoginController>().errorRequestCrypto;
      errorSend = Provider.of<LoginController>(context).errorSendCrypto;
      _isProfileVisible = Provider.of<WalletController>(context, listen: true).isProfileVisible;
      _isWalletVisible = Provider.of<WalletController>(context, listen: true).isWalletVisible;
      _isNotificationVisible = Provider.of<WalletController>(context, listen: true).isNotificationVisible;
      _selectedIndex = Provider.of<WalletController>(context, listen: true).selectedIndex;
      _isCryptoSelected = Provider.of<WalletController>(context, listen: true).isSelectCryptoMode;
      selectedCrypto = Provider.of<WalletController>(context, listen: true).isSelectCryptoMode ? Provider.of<WalletController>(context, listen: true).cryptos.firstWhereOrNull((e) => e.symbol == Provider.of<LoginController>(context, listen: true).tokenSymbol) : null;
    });
    if (_hasNetwork) {
      return WillPopScope(
        onWillPop: () async {
          return false;
        },
        child: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
            Provider.of<WalletController>(context, listen: false).isSideMenuVisible = false;
            Provider.of<WalletController>(context, listen: false).isProfileVisible = false;
            Provider.of<WalletController>(context, listen: false).isWalletVisible = false;
            Provider.of<WalletController>(context, listen: false).isNotificationVisible = false;
            setState(() {
              _isVisible = false;
              _isProfileVisible = false;
              _isWalletVisible = false;
              _isNotificationVisible = false;
            });
          },
          child: Container(
            decoration: _decoration.elementAt(_selectedIndex),
            child: Scaffold(
              resizeToAvoidBottomInset: false,
              backgroundColor: Colors.transparent,
              appBar: CustomAppBar(),
              bottomNavigationBar: StyleProvider(
                style: Style(),
                child: _isCryptoSelected ? appBarSelected() : appBar(),
              ),
              body: Stack(children: [
                Container(child: _widgetOptions.elementAt(_selectedIndex)),
                Positioned(
                  right: 180,
                  top: 40,
                  child: Padding(
                    padding: const EdgeInsets.all(0),
                    child: Provider.of<WalletController>(context, listen: false).cryptos.length == 0
                        ? null
                        : _isLoading
                            ? Container(
                                height: MediaQuery.of(context).size.width * 0.1,
                                width: MediaQuery.of(context).size.width * 0.1,
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  shape: BoxShape.circle,
                                ),
                                child: Padding(
                                  padding: EdgeInsets.all(MediaQuery.of(context).size.width * 0.025),
                                  child: CircularProgressIndicator(
                                    color: Colors.blueAccent,
                                  ),
                                ))
                            : null,
                  ),
                ),
                HeaderNavigation(_isVisible, _isWalletVisible, _isNotificationVisible)
              ]),
            ),
          ),
        ),
      );
    } else {
      return Stack(
        children: [
          Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            color: Colors.white,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Center(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      "No Internet Connection , please check your network",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(color: Colors.black, fontSize: 14),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: ElevatedButton(
                    onPressed: () {},
                    child: Text("Retry"),
                  ),
                ),
              ],
            ),
          )
        ],
      );
    }
  }

  int handleMarketCap(String marketCupCrypto) {
    if (marketCupCrypto != null && marketCupCrypto != "") {
      return 9;
    } else {
      showDialog(
          barrierDismissible: false,
          context: context,
          builder: (context) {
            return StatefulBuilder(builder: (context, setState) {
              return AlertDialog(
                  insetPadding: EdgeInsets.zero,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(30)),
                  ),
                  content: Padding(
                    padding: const EdgeInsets.all(0),
                    child: Container(
                      width: MediaQuery.of(context).size.width * 0.6,
                      height: MediaQuery.of(context).size.height * 0.4,
                      child: Column(
                        children: [
                          Padding(
                            padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.5, top: MediaQuery.of(context).size.width * 0.02),
                            child: IconButton(
                                alignment: Alignment.topLeft,
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                                icon: Icon(
                                  Icons.close,
                                  color: Colors.grey,
                                  size: 35,
                                )),
                          ),
                          Image.asset(
                            "images/other.png",
                          ),
                          Text(
                            "Sorry this token doesn't seem to be listed \non our parteners plateforms.",
                            textAlign: TextAlign.center,
                            style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.035, fontWeight: FontWeight.w700, color: Colors.black),
                          ),
                          Spacer(),
                          ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                  minimumSize: Size(200.0, 40.0),
                                  side: BorderSide(color: Colors.grey),
                                  primary: Color(0xFF4048FF),
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(50.0),
                                  )),
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                              child: Text(
                                "ok",
                                style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.047, fontWeight: FontWeight.w700, color: Colors.white),
                              )),
                        ],
                      ),
                    ),
                  ));
            });
          });
      return 2;
    }
  }

  Future cryptoList() async {
    var cryptoList = await apiService.cryptos1(Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "", version);
    if (cryptoList != "error") {
      setState(() {
        Provider.of<WalletController>(context, listen: false).cryptos = cryptoList;
      });
    }

    return cryptoList;
  }

  void deleteCrypto(String? selectedCryptoSymbol) {
    var Selectedcrypto = Provider.of<WalletController>(context, listen: false).cryptos.firstWhere((e) => e.symbol == selectedCryptoSymbol);
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (context) {
          return StatefulBuilder(builder: (context, setState) {
            return AlertDialog(
                insetPadding: EdgeInsets.zero,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(30)),
                ),
                content: Padding(
                  padding: const EdgeInsets.all(0),
                  child: Container(
                    width: MediaQuery.of(context).size.width * 0.6,
                    height: MediaQuery.of(context).size.height * 0.5,
                    child: Column(
                      children: [
                        Padding(
                          padding: EdgeInsets.only(bottom: 8.0),
                          child: Row(
                            children: [
                              SizedBox(
                                width: 60,
                              ),
                              Image.asset(
                                "images/quest.png",
                              ),
                              Padding(
                                padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.04, bottom: MediaQuery.of(context).size.width * 0.25),
                                child: IconButton(
                                    alignment: Alignment.topLeft,
                                    onPressed: () {
                                      Navigator.of(context).pop();
                                    },
                                    icon: Icon(
                                      Icons.close,
                                      color: Colors.grey,
                                      size: 35,
                                    )),
                              ),
                            ],
                          ),
                        ),
                        Text(
                          "Are you sure ?",
                          textAlign: TextAlign.center,
                          style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.038, fontWeight: FontWeight.w700, color: Colors.black),
                        ),
                        Text(
                          "You are about to delete \n this token",
                          textAlign: TextAlign.center,
                          style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.035, fontWeight: FontWeight.normal, color: Colors.black),
                        ),
                        Padding(
                          padding: EdgeInsets.all(8.0),
                          child: Container(
                            width: MediaQuery.of(context).size.width * 0.6,
                            height: MediaQuery.of(context).size.height * 0.065,
                            child: ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                    minimumSize: Size(260.0, 40.0),
                                    side: BorderSide(color: Colors.grey),
                                    primary: Color(0xFF4048FF),
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(50.0),
                                    )),
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                                child: Text(
                                  "cancel",
                                  style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.047, fontWeight: FontWeight.w700, color: Colors.white),
                                )),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                            width: MediaQuery.of(context).size.width * 0.6,
                            height: MediaQuery.of(context).size.height * 0.065,
                            child: ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                    minimumSize: Size(260.0, 40.0),
                                    side: BorderSide(color: Colors.white),
                                    primary: Colors.white,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(50.0),
                                    )),
                                onPressed: () async {
                                  var result = await LoginController.apiService.removeToken(Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? '', Selectedcrypto.contract.toString());
                                  if (result == "token removed") {
                                    setState(() {
                                      Provider.of<WalletController>(context, listen: false).cryptos = [];
                                    });
                                    Provider.of<WalletController>(context, listen: false).selectedIndex = 2;
                                    Fluttertoast.showToast(
                                      msg: "Token removed successfully",
                                      backgroundColor: Color.fromRGBO(0, 151, 117, 1),
                                    );
                                    Navigator.pushReplacement(context, MaterialPageRoute(builder: (_) => WelcomeScreen()));
                                  } else {
                                    Navigator.of(context).pop();
                                    Fluttertoast.showToast(
                                      msg: "Network error, please try again !",
                                      backgroundColor: Color(0xFFF52079),
                                    );
                                  }
                                },
                                child: Text(
                                  "Delete",
                                  style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.047, fontWeight: FontWeight.w700, color: Color(0xFF4048FF)),
                                )),
                          ),
                        ),
                      ],
                    ),
                  ),
                ));
          });
        });
  }
}

class Style extends StyleHook {
  final size = OneContext().mediaQuery.size;

  @override
  double get activeIconSize => size.width * 0.19;

  @override
  double get activeIconMargin => size.width * 0.02;

  @override
  double get iconSize => size.width * 0.12;

  @override
  TextStyle textStyle(Color color, String? s) {
    return TextStyle(fontSize: 0, color: color);
  }
}
