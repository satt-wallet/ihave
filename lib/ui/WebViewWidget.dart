import 'dart:async';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http; // For making HTTP requests
import 'package:flutter_svg/flutter_svg.dart'; // For displaying SVG images
import 'package:html/parser.dart' as parser; // For parsing HTML
import 'package:google_fonts/google_fonts.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

List<String> _historyListglob = [];

class WebViewNavigator extends StatefulWidget {
  const WebViewNavigator({Key? key}) : super(key: key);

  @override
  State<WebViewNavigator> createState() => _WebViewNavigatorState();
}

class _WebViewNavigatorState extends State<WebViewNavigator> {
  String nameurl = "";
  late int itemIndex = 0;
  TextEditingController _searchaddbook = new TextEditingController();
  List<Website> websites = [];
  List<String?> items = ['Satt', 'Multichain', 'Pancakeswap', 'Justmoney'];
  List<String?> itemsurl = ['https://dapp.satt.com/welcome', 'https://www.multichain.com/', 'https://pancakeswap.finance/', 'https://www.justmoney.co.za/'];
  List<String?> itemsurlimage = [
    'https://s2.coinmarketcap.com/static/img/coins/200x200/7244.png',
    'https://altcoinsbox.com/wp-content/uploads/2023/04/multichain-logo.png',
    'https://pancakeswap.finance/logo.png',
    'https://www.justmoney.co.za/uploads/c477a272-dd69-4620-9e75-2a910eec3130__21e5a6b668f33cba42e47a481fdc0dcccfa2bdb0.webp'
  ];
  List<String> _savedTexts = [];

  @override
  void initState() {
    super.initState();
    fetchWebsites();
    _loadSavedTexts();
  }

  Future<void> _loadSavedTexts() async {
    final savedTexts = await DatabaseHelper.instance.getSavedTexts();
    setState(() {
      _savedTexts = savedTexts.map((e) => e['text'] as String).toList();
    });
  }

  Future<void> _saveText(String text) async {
    if (text.isNotEmpty) {
      await DatabaseHelper.instance.insertText(text);
      _searchaddbook.clear();
      await _loadSavedTexts();
    }
  }

  fetchWebsites() async {
    List<String> urls = [
      'https://www.google.com/',
    ];

    for (String url in urls) {
      // Make an HTTP GET request to fetch the website content
      final response = await http.get(Uri.parse(url));

      if (response.statusCode == 200) {
        // Parse the HTML response
        final document = parser.parse(response.body);

        // Find the <link> tag with the website's logo
        final logoLink = document.head!.querySelector("link[rel='icon']") ?? document.head!.querySelector("link[rel='shortcut icon']");

        if (logoLink != null) {
          String logoUrl = logoLink.attributes['href']!;

          // Create a Website object with the URL and logo URL
          Website website = Website(url: url, logoUrl: logoUrl);
          setState(() {
            websites.add(website);
          });
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(children: [
        Center(
          child: Image.asset(
            "images/headtitlenavigator.png",
          ),
        ),
        SizedBox(height: MediaQuery.of(context).size.height * 0.035),
        Padding(
          padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.height * 0.023, MediaQuery.of(context).size.width * 0.013, MediaQuery.of(context).size.height * 0.023, MediaQuery.of(context).size.width * 0.013),
          child: TextFormField(
            textInputAction: TextInputAction.go,
            onFieldSubmitted: (value) {
              RegExp regex = RegExp(r'^(?!https?://)');
              bool hasNoPrefix = regex.hasMatch(value!);

              final List<String> excludedExtensions = ['.com', '.tn', '.co', '.fr', '.org', '.us'];
              final List<String> escapedExtensions = excludedExtensions.map(RegExp.escape).toList();

              final RegExp regexcom = RegExp('^(?!.*(?:${escapedExtensions.join('|')})).+\$');
              final bool hasNoExcludedExtension = regexcom.hasMatch(value!);

              if (hasNoPrefix == false || hasNoExcludedExtension == false) {
                _saveText(value!);
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => _WebViewNavigator2State(
                              itemsurl: value!,
                            )));
              } else {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => _WebViewNavigator2State(
                              itemsurl: "https://www.google.com/search?q=" + value!,
                            )));
                _saveText("https://www.google.com/search?q=" + value!);
              }
            },
            onChanged: (value) {
              TextSelection previousSelection = _searchaddbook.selection;
              _searchaddbook.text = value;
              setState(() {
                _searchaddbook.text = value;
              });
              _searchaddbook.selection = previousSelection;
            },
            controller: _searchaddbook,
            decoration: new InputDecoration(
              hintText: 'URL or site name',
              prefixIcon: Padding(
                padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.028, right: MediaQuery.of(context).size.width * 0.028),
                child: SvgPicture.asset(
                  "images/search.svg",
                  width: MediaQuery.of(context).size.width * 0.02,
                ),
              ),
              fillColor: Colors.grey,
              focusColor: Colors.grey,
              hintStyle: GoogleFonts.poppins(fontWeight: FontWeight.normal, color: Colors.grey, fontSize: MediaQuery.of(context).size.width * 0.036),
              contentPadding: new EdgeInsets.symmetric(vertical: 0.0, horizontal: MediaQuery.of(context).size.width * 0.03),
              enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.grey.withOpacity(0.3))),
              focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Color(0xff4048FF))),
            ),
          ),
        ),
        SizedBox(height: MediaQuery.of(context).size.height * 0.05),
        Padding(
          padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * .55),
          child: Text(
            "PROPOSED APP",
            style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.04, fontWeight: FontWeight.w600, color: Color(0xFF1F2337)),
          ),
        ),
        SizedBox(height: MediaQuery.of(context).size.height * 0.005),

        ListView.builder(
          shrinkWrap: true,
          itemCount: items.length,
          itemBuilder: (context, index) {
            return Container(
              margin: EdgeInsets.only(
                bottom: 10.0,
              ),
              width: MediaQuery.of(context).size.width * .035,
              height: MediaQuery.of(context).size.height * .13,
              decoration: BoxDecoration(
                color: Color(0xFFF6F6FF),
                borderRadius: BorderRadius.circular(70),
                border: Border.all(
                  color: Color(0xFFF6F6FF),
                ),
              ),
              child: Column(
                children: [
                  ListTile(
                    leading: Container(
                      width: 50,
                      height: 50,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        image: DecorationImage(
                          fit: BoxFit.cover,
                          image: NetworkImage(itemsurlimage[index]!),
                        ),
                      ),
                    ),
                    title: Text(
                      items[index]!,
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    subtitle: Padding(
                      padding: EdgeInsets.only(top: 2.0),
                      child: Text(
                        itemsurl[index]!,
                        style: TextStyle(
                          decoration: TextDecoration.underline,
                          fontSize: 18,
                          color: Colors.grey[600],
                        ),
                      ),
                    ),
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => _WebViewNavigator2State(
                              itemsurl: itemsurl[index]!,
                            ),
                          ));
                    },
                  ),
                ],
              ),
            );
          },
        ),

        SizedBox(height: MediaQuery.of(context).size.height * 0.035),
        Padding(
          padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * .55),
          child: Text(
            "SEARCH HISTORY",
            style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.04, fontWeight: FontWeight.w600, color: Color(0xFF1F2337)),
          ),
        ),
        SizedBox(height: MediaQuery.of(context).size.height * 0.035),
        ListView.builder(
          shrinkWrap: true,
          itemCount: _savedTexts.length >= 3 ? 3 : _savedTexts.length,
          itemBuilder: (context, index) {
            if (_savedTexts.length > 3) {
              itemIndex = _savedTexts.length - 3 + index;
            } else {
              itemIndex = index;
            }

            RegExp regex = RegExp(r'^(?!https?://)');
            bool hasNoPrefix = regex.hasMatch(_savedTexts[itemIndex]!);
            if (hasNoPrefix == false) {
              final text = Uri.parse(_savedTexts[itemIndex]!);
              final String name = text.host;
              if (name != null) //{
                nameurl = name;
            } else {
              final text = Uri.parse("https://" + _savedTexts[itemIndex]!);
              final String name = text.host;
              if (name != null)
                nameurl = name;
            }
            return Container(
              margin: EdgeInsets.only(
                bottom: 10.0,
              ),
              width: MediaQuery.of(context).size.width * .035,
              height: MediaQuery.of(context).size.height * .13,
              decoration: BoxDecoration(
                color: Color(0xFFF6F6FF),
                borderRadius: BorderRadius.circular(70),
                border: Border.all(
                  color: Color(0xFFF6F6FF),
                ),
              ),
              child: Column(
                children: [
                  ListTile(
                    leading: Container(
                      width: 50,
                      height: 50,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        image: DecorationImage(fit: BoxFit.cover, image: AssetImage("images/googleicon.png")),
                      ),
                    ),
                    title: Text(
                      nameurl,
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    subtitle: Padding(
                      padding: EdgeInsets.only(top: 2.0),
                      child: Text(
                        _savedTexts[itemIndex]!,
                        style: TextStyle(
                          decoration: TextDecoration.underline,
                          fontSize: 18,
                          color: Colors.grey[600],
                        ),
                      ),
                    ),
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => _WebViewNavigator2State(
                              itemsurl: _savedTexts[itemIndex]!,
                            ),
                          ));
                    },
                  ),
                ],
              ),
            );
          },
        ),
      ]),
    );
  }
}

class DatabaseHelper {
  static final DatabaseHelper instance = DatabaseHelper._();
  static Database? _database;

  DatabaseHelper._();

  Future<Database> get database async {
    if (_database != null) return _database!;

    _database = await _initDatabase();
    return _database!;
  }

  Future<Database> _initDatabase() async {
    final String path = join(await getDatabasesPath(), 'my_database.db');
    return await openDatabase(
      path,
      version: 1,
      onCreate: (db, version) {
        return db.execute('''
          CREATE TABLE my_table (
            id INTEGER PRIMARY KEY,
            text TEXT
          )
        ''');
      },
    );
  }

  Future<int> insertText(String text) async {
    final db = await database;
    final Map<String, dynamic> row = {'text': text};
    return await db.insert('my_table', row);
  }

  Future<List<Map<String, dynamic>>> getSavedTexts() async {
    final db = await database;
    return await db.query('my_table');
  }
}

class Website {
  final String url;
  final String logoUrl;

  Website({required this.url, required this.logoUrl});
}

class _WebViewNavigator2State extends StatefulWidget {
  final String itemsurl;

  _WebViewNavigator2State({required this.itemsurl});

  @override
  State<_WebViewNavigator2State> createState() => _WebViewNavigator2StateState();
}

bool icon1 = true;
bool icon2 = false;
bool icon3 = false;
bool icon4 = false;

class _WebViewNavigator2StateState extends State<_WebViewNavigator2State> {
  final Completer<WebViewController> _controller = Completer<WebViewController>();
  List<String> _historyList = [];
  int _selectedTab = 0;
  int _currentIndex = 0;

  Future<void> _handleBackButton() async {
    final controller = await _controller.future;
    if (await controller.canGoBack()) {
      await controller.goBack();
      setState(() {
        _currentIndex--;
      });
    }
  }

  Future<void> _handleForwardButton() async {
    final controller = await _controller.future;
    if (await controller.canGoForward()) {
      await controller.goForward();
      setState(() {
        _currentIndex++;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Stack(children: [
          SizedBox(height: MediaQuery.of(context).size.height * .02),
          WebView(
            initialUrl: widget.itemsurl,
            javascriptMode: JavascriptMode.unrestricted,
            debuggingEnabled: false,
            gestureNavigationEnabled: false,
            onWebViewCreated: (WebViewController controller) {
              _controller.complete(controller);
            },
            onPageFinished: (String url) {
              setState(() {
                _historyList.add(url);
                _historyListglob.add(url);
                _currentIndex = _historyList.length - 1;
              });
            },
          ),
        ]),
      ),
      bottomNavigationBar: new Container(
        height: 60,
        decoration: BoxDecoration(
          color: Colors.black,
          borderRadius: const BorderRadius.only(
              ),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            IconButton(
              enableFeedback: false,
              onPressed: () {
                setState(() {
                  icon1 = true;
                  icon2 = false;
                  icon3 = false;
                  icon4 = false;
                  _handleBackButton();
                });
              },
              icon: Icon(
                Icons.arrow_back,
                color: icon1 ? Color(0xff4048FF) : Colors.white,
                size: 35,
              ),
            ),
            IconButton(
              enableFeedback: false,
              onPressed: () {
                setState(() {
                  icon1 = false;
                  icon2 = true;
                  icon3 = false;
                  icon4 = false;
                  _handleForwardButton();
                });
              },
              icon: Icon(
                Icons.arrow_forward,
                color: icon2 ? Color(0xff4048FF) : Colors.white,
                size: 35,
              ),
            ),
            Wrap(crossAxisAlignment: WrapCrossAlignment.center, children: [
              IconButton(
                enableFeedback: false,
                onPressed: () {
                  setState(() {
                    icon1 = false;
                    icon2 = false;
                    icon3 = true;
                    icon4 = false;
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => HistoryPage(
                            url: _historyList!,
                          ),
                        ));
                  });
                },
                icon: Icon(
                  Icons.history,
                  color: icon3 ? Color(0xff4048FF) : Colors.white,
                  size: 35,
                ),
              ),
              Text(
                "${_historyList.length}",
                style: TextStyle(
                  fontSize: 22,
                  color: icon3 ? Color(0xff4048FF) : Colors.white,
                ),
              ),
            ]),
            IconButton(
              enableFeedback: false,
              onPressed: () {
                setState(() {
                  icon1 = false;
                  icon2 = false;
                  icon3 = false;
                  icon4 = true;
                });
              },
              icon: Icon(
                Icons.more_horiz_outlined,
                color: icon4 ? Color(0xff4048FF) : Colors.white,
                size: 35,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class ForwardButton extends StatelessWidget {
  final Function onPressed;

  ForwardButton({required this.onPressed});

  @override
  Widget build(BuildContext context) {
    return IconButton(
      icon: Icon(Icons.arrow_forward),
      onPressed: onPressed as void Function()?,
    );
  }
}

class HistoryPage extends StatefulWidget {
  final List<String> url;

  HistoryPage({required this.url});

  @override
  State<HistoryPage> createState() => _HistoryPageState();
}

final Completer<WebViewController> _controller = Completer<WebViewController>();

class _HistoryPageState extends State<HistoryPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white70,
        title: Text(
          'History',
          style: TextStyle(color: Colors.black, fontSize: 18),
        ),
      ),
      body: GridView.builder(
        itemCount: widget.url.length,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 1,
          mainAxisSpacing: 30.0,
          crossAxisSpacing: 30.0,
          childAspectRatio: (1 / .4),
        ),
        itemBuilder: (BuildContext context, int index) {
          return GestureDetector(
            onTap: () {
              setState(() {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => _WebViewNavigator2State(
                        itemsurl: widget.url[index]!,
                      ),
                    ));
              });
            },
            child: Container(
              height: MediaQuery.of(context).size.height * .03,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10.0),
                border: Border.all(
                  color: Colors.blueAccent,
                  width: 2.0,
                ),
              ),
              child: Card(
                elevation: 5,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      color: Colors.red,
                      child: Text(Uri.parse(widget.url[index]!).host),
                    ),
                    SingleChildScrollView(
                      scrollDirection: Axis.vertical,
                      child: Container(
                        height: MediaQuery.of(context).size.height * .15,
                        child: Stack(children: [
                          SizedBox(height: MediaQuery.of(context).size.height * .02),
                          WebView(
                            initialUrl: widget.url[index]!,
                            javascriptMode: JavascriptMode.unrestricted,
                            debuggingEnabled: false,
                            gestureNavigationEnabled: false,
                            onWebViewCreated: (WebViewController controller) {
                              _controller.complete(controller);
                            },
                          ),
                        ]),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
