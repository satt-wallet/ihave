import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:ihave/ui/signin.dart';
import 'package:ihave/ui/staticElements/headerNavigation.dart';
import 'package:provider/provider.dart';

import '../controllers/LoginController.dart';
import '../controllers/walletController.dart';
import '../service/apiService.dart';
import '../util/utils.dart';
import '../widgets/customAppBar.dart';
import '../widgets/customBottomBar.dart';
import 'WelcomeScreen.dart';

class ChangePassword extends StatefulWidget {
  const ChangePassword({Key? key}) : super(key: key);

  @override
  _ChangePasswordState createState() => _ChangePasswordState();
}

class _ChangePasswordState extends State<ChangePassword> {
  bool _isVisible = false;
  bool _isProfileVisible = false;
  bool _isWalletVisible = false;
  bool _isNotificationVisible = false;
  bool isNotExist = false;
  bool passwordexist = false;
  TextEditingController _passwordController = new TextEditingController();
  TextEditingController _newPasswordController = new TextEditingController();
  TextEditingController _confirmPasswordController =
      new TextEditingController();
  RegExp numReg = RegExp(r".*[0-9].*");
  RegExp RegUpperCase = RegExp(r'[A-Z]');
  RegExp? BlacklistingTextInputFormatter;
  RegExp RegLowerCase = RegExp(r'[a-z]');
  RegExp RegSpecialChar = RegExp('[A-Za-z0-9]');
  bool hasSpecialCharacteres = false;
  bool _passwordFieldForm = false;
  bool _newPasswordFieldForm = false;
  bool _confirmPasswordFieldForm = false;
  bool _obscureText = true;
  bool _obscureText2 = true;
  bool _obscureText3 = true;
  bool _isConfirmed = false;
  bool _isActivatedButton = false;
  bool _isValidForm = false;
  bool isChanged = false;
  bool wrongpassword = false;
  String? firebaseToken = " ";
  APIService apiService = LoginController.apiService;

  bool _sameLogin = false;
  bool checkSpecialChar(String pass){
    var specialPass = pass.replaceAll(RegSpecialChar, "");
    return specialPass.length > 0 ;
  }
  @override
  Widget build(BuildContext context) {
    setState(() {
      _isVisible = Provider.of<WalletController>(context, listen: true)
          .isSideMenuVisible;
      _isProfileVisible =
          Provider.of<WalletController>(context, listen: true).isProfileVisible;
      _isWalletVisible =
          Provider.of<WalletController>(context, listen: true).isWalletVisible;
      _isNotificationVisible =
          Provider.of<WalletController>(context, listen: true)
              .isNotificationVisible;
    });
    return Scaffold(
      appBar: CustomAppBar(),
      bottomNavigationBar: CustomBottomBar(),
      body: WillPopScope(
        onWillPop: () {
          return Future.value(true);
        },
        child: Stack(
          children: [
            SingleChildScrollView(
              child: Column(
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: EdgeInsets.only(
                            top: MediaQuery.of(context).size.height * 0.025,
                          left: MediaQuery.of(context).size.width * 0.06,
                        ),
                        child: Text(
                          "Change login\nPassword",
                          textAlign: TextAlign.left,
                          style: GoogleFonts.poppins(
                            color: Color(0xFF1F2337),
                            fontSize:
                                MediaQuery.of(context).size.height * 0.033,
                            fontWeight: FontWeight.w700,
                            height: 1.2,
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                          left: MediaQuery.of(context).size.width * 0.06,
                          top: MediaQuery.of(context).size.height * 0.02
                        ),
                        child: Text(
                          "Need to change your login password ?",
                          textAlign: TextAlign.left,
                          style: GoogleFonts.poppins(
                            fontSize:
                                MediaQuery.of(context).size.height *
                                    0.017,
                            color: Color(0xFF75758F),
                            fontWeight: FontWeight.w500,
                            letterSpacing: 0.6
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                          left: MediaQuery.of(context).size.width * 0.06,
                          top: MediaQuery.of(context).size.height * 0.022
                        ),
                        child: Text(
                          "For security reasons, we HIGHLY\nrecommand that you choose something",
                          textAlign: TextAlign.left,
                          style: GoogleFonts.poppins(
                            fontSize:
                                MediaQuery.of(context).size.height * 0.018,
                            color: Color(0xFF1F2337),
                            fontWeight: FontWeight.w500,
                            height: 1.1
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                          left: MediaQuery.of(context).size.width * 0.06
                        ),
                        child: Text(
                          "completely different from your\ntransaction password.",
                          style: GoogleFonts.poppins(
                              fontSize:
                              MediaQuery.of(context).size.height * 0.018,
                              color: Color(0xFF1F2337),
                              fontWeight: FontWeight.w700,
                              height: 1.1
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                          left: MediaQuery.of(context).size.width * 0.06,
                          top: MediaQuery.of(context).size.height * 0.045
                        ),
                        child: Text(
                          "YOUR CURRENT PASSWORD",
                          style: GoogleFonts.poppins(
                            fontSize:
                                MediaQuery.of(context).size.height *
                                    0.018,
                            color: const Color(0XFF75758F),
                            fontWeight: FontWeight.w600,
                            letterSpacing: 0.5
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                          left: MediaQuery.of(context).size.width * 0.06,
                          top: MediaQuery.of(context).size.height * 0.01
                        ),
                        child: SizedBox(
                          width: MediaQuery.of(context).size.width * 0.88,
                          child: TextFormField(
                            controller: _passwordController,
                            keyboardType: TextInputType.visiblePassword,
                            obscureText: _obscureText,
                            style: GoogleFonts.poppins(
                              fontWeight: FontWeight.w600,
                              fontStyle: FontStyle.normal,
                              fontSize: MediaQuery.of(context).size.height * 0.0182,

                            ),
                            onChanged: (value) {
                              setState(() {
                                wrongpassword = false;
                              });
                              _passwordFieldForm = true;
                              TextSelection previousSelection =
                                  _passwordController.selection;
                              _passwordController.text = value;
                              _passwordController.selection =
                                  previousSelection;
                              if (_passwordController.text.isEmpty) {
                                setState(() {
                                  _passwordFieldForm = false;
                                  _newPasswordController.text = "";
                                  _confirmPasswordController.text = "";
                                  _newPasswordFieldForm = false;
                                  _confirmPasswordFieldForm = false;
                                  _isActivatedButton = false;
                                });
                              } else {
                                setState(() {
                                  _passwordFieldForm = true;
                                });
                              }
                              if (_passwordController.text.length > 1) {
                                setState(() {
                                  passwordexist = true;
                                });
                              } else {
                                setState(() {
                                  passwordexist = false;
                                });
                              }
                              TextSelection.fromPosition(
                                  TextPosition(offset: value.length));
                            },
                            validator: (value) {
                              if (value!.isEmpty) {
                                return 'Field required';
                              }
                              return null;
                            },
                            decoration: InputDecoration(
                                fillColor: Colors.white,
                                focusColor: Colors.white,
                                hoverColor: Colors.white,

                                filled: true,
                                isDense: true,
                                border: InputBorder.none,
                                suffixIcon: Padding(
                                  padding: EdgeInsets.only(
                                    right: MediaQuery.of(context).size.width * 0.02,
                                    top: MediaQuery.of(context).size.height * 0.005,
                                    bottom: MediaQuery.of(context).size.height * 0.005
                                  ),
                                  child: IconButton(
                                      onPressed: () {
                                        setState(() {
                                          _obscureText = !_obscureText;
                                        });
                                      },
                                      icon: _obscureText
                                          ? SvgPicture.asset(
                                        "images/visibility-icon-off.svg",
                                        height: MediaQuery.of(
                                            context)
                                            .size
                                            .height *
                                            0.018,
                                      )
                                          : SvgPicture.asset(
                                        "images/visibility-icon-on.svg",
                                        height: MediaQuery.of(
                                            context)
                                            .size
                                            .height *
                                            0.02,
                                      )
                                            ),
                                ),
                                contentPadding:
                                    EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width * 0.05),
                                enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(60),
                                    borderSide:
                                        BorderSide(color: const Color(0XFFD6D6E8))),
                                focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(60),
                                  borderSide:
                                      BorderSide(color: const Color(0XFFD6D6E8)),
                                ),
                                errorBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(60),
                                    borderSide: BorderSide(
                                        color: Color(0xffF52079))),
                                focusedErrorBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(60),
                                    borderSide: BorderSide(
                                        color: Color(0xffF52079)))),
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                          left: MediaQuery.of(context).size.width * 0.06,
                          top: MediaQuery.of(context).size.height * 0.025
                        ),
                        child: Text(
                          "YOUR NEW PASSWORD ",
                          style: GoogleFonts.poppins(
                            fontSize:
                            MediaQuery.of(context).size.height *
                                0.018,
                            color: _newPasswordFieldForm
                                ? (_isValidForm
                                    ? const Color(0XFF75758F)
                                    : Color(0xffF52079))
                                : const Color(0XFF75758F),
                              fontWeight: FontWeight.w600,
                              letterSpacing: 0.5
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                          top: MediaQuery.of(context).size.height * 0.01,
                          left: MediaQuery.of(context).size.width * 0.06
                        ),
                        child: SizedBox(
                          width: MediaQuery.of(context).size.width * 0.88,
                          child: TextFormField(
                            controller: _newPasswordController,
                            keyboardType: TextInputType.visiblePassword,
                            obscureText: _obscureText2,
                            style: GoogleFonts.poppins(
                              fontWeight: FontWeight.w600,
                              fontStyle: FontStyle.normal,
                              fontSize: MediaQuery.of(context).size.height * 0.0182,

                            ),
                            readOnly: _passwordFieldForm ? false : true,
                            onChanged: (value) {
                              setState(() {
                                _sameLogin = false;
                              });
                              if (_confirmPasswordController.text !=
                                      _newPasswordController.text ||
                                  _passwordController.text.isEmpty) {
                                setState(() {
                                  _isConfirmed = false;
                                  _isActivatedButton = false;
                                });
                              } else {
                                setState(() {
                                  _isConfirmed = true;
                                  _isActivatedButton = true;
                                });
                              }
                              _sameLogin =
                                  _passwordController.text == value;
                              _newPasswordFieldForm = true;
                              TextSelection previousSelection =
                                  _newPasswordController.selection;
                              _newPasswordController.text = value;
                              _newPasswordController.selection =
                                  previousSelection;
                              if (_newPasswordController.text.length < 8 ||
                                  (!RegUpperCase.hasMatch(
                                          _newPasswordController.text) ||
                                      !RegLowerCase.hasMatch(
                                          _newPasswordController.text) ||
                                      !numReg.hasMatch(
                                          _newPasswordController.text)) ||
                                  !checkSpecialChar(_newPasswordController.text)) {
                                setState(() {
                                  _isValidForm = false;
                                  _isActivatedButton = false;
                                });
                              } else {
                                setState(() {
                                  _isValidForm = true;
                                });
                              }
                              if (_newPasswordController.text.length > 0) {
                                setState(() {
                                  _newPasswordFieldForm = true;
                                });
                              } else {
                                setState(() {
                                  _newPasswordFieldForm = false;
                                  _isActivatedButton = false;
                                });
                              }

                              TextSelection.fromPosition(
                                  TextPosition(offset: value.length));
                            },
                            validator: (value) {
                              if (value!.isEmpty) {
                                return 'Field required';
                              }
                              return null;
                            },
                            decoration: InputDecoration(
                              border: InputBorder.none,

                              fillColor: _newPasswordFieldForm
                                  ? (_isValidForm
                                      ? Colors.white
                                      : Colors.white.withGreen(990))
                                  : (Colors.white),
                              focusColor: Colors.white,
                              hoverColor: _newPasswordFieldForm
                                  ? (_isValidForm
                                      ? Colors.green
                                      : Colors.red)
                                  : (Colors.white),
                              filled: true,
                              isDense: true,
                              contentPadding: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width * 0.05),
                              enabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(60),
                                  borderSide: _newPasswordFieldForm
                                      ? (_isValidForm
                                          ? BorderSide(color: Colors.green)
                                          : BorderSide(color: Colors.red))
                                      : (BorderSide(color: const Color(0XFFD6D6E8)))),
                              focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(60),
                                  borderSide: _newPasswordFieldForm
                                      ? (_isValidForm
                                          ? BorderSide(color: Colors.green)
                                          : BorderSide(color: Colors.red))
                                      : BorderSide(color: const Color(0XFFD6D6E8))),
                              errorBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(60),
                                  borderSide:
                                      BorderSide(color: Color(0xffF52079))),
                              focusedErrorBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(60),
                                  borderSide:
                                      BorderSide(color: Color(0xffF52079))),
                              suffixIcon: Padding(
                                padding: EdgeInsets.only(
                                    right: MediaQuery.of(context).size.width * 0.02,
                                    top: MediaQuery.of(context).size.height * 0.005,
                                    bottom: MediaQuery.of(context).size.height * 0.005
                                ),
                                child: IconButton(
                                    onPressed: () {
                                      setState(() {
                                        _obscureText2 = !_obscureText2;
                                      });
                                    },
                                    icon: _obscureText2
                                        ? SvgPicture.asset(
                                      "images/visibility-icon-off.svg",
                                      height: MediaQuery.of(
                                          context)
                                          .size
                                          .height *
                                          0.018,
                                    )
                                        : SvgPicture.asset(
                                      "images/visibility-icon-on.svg",
                                      height: MediaQuery.of(
                                          context)
                                          .size
                                          .height *
                                          0.02,
                                    )
                                         ),
                              ),
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                          left: MediaQuery.of(context).size.width * 0.06,
                            top: MediaQuery.of(context).size.height * 0.025
                        ),
                        child: Text(
                          "CONFIRM NEW PASSWORD",
                          style: GoogleFonts.poppins(
                            fontSize:
                            MediaQuery.of(context).size.height *
                                0.018,
                            color: _confirmPasswordFieldForm
                                ? (_isConfirmed
                                    ? const Color(0XFF75758F)
                                    : Color(0xffF52079))
                                : const Color(0XFF75758F),
                              fontWeight: FontWeight.w600,
                              letterSpacing: 0.5
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                          left: MediaQuery.of(context).size.width * 0.06,
                          top: MediaQuery.of(context).size.height * 0.01
                        ),
                        child: SizedBox(
                          width: MediaQuery.of(context).size.width * 0.88,
                          child: TextFormField(
                            controller: _confirmPasswordController,
                            keyboardType: TextInputType.visiblePassword,
                            readOnly: _isValidForm ? false : true,
                            obscureText: _obscureText3,
                            style: GoogleFonts.poppins(
                              fontWeight: FontWeight.w600,
                              fontStyle: FontStyle.normal,
                              fontSize: MediaQuery.of(context).size.height * 0.0182,

                            ),
                            onChanged: (value) {
                              _confirmPasswordFieldForm = true;
                              TextSelection previousSelection =
                                  _confirmPasswordController.selection;
                              _confirmPasswordController.text = value;
                              _confirmPasswordController.selection =
                                  previousSelection;
                              if (_confirmPasswordController
                                  .text.isNotEmpty) {
                                setState(() {
                                  _confirmPasswordFieldForm = true;
                                });
                              } else {
                                setState(() {
                                  _confirmPasswordFieldForm = false;
                                  _isActivatedButton = false;
                                });
                              }
                              if (_confirmPasswordController.text !=
                                      _newPasswordController.text ||
                                  _passwordController.text.isEmpty) {
                                setState(() {
                                  _isConfirmed = false;
                                  _isActivatedButton = false;
                                });
                              } else {
                                setState(() {
                                  _isConfirmed = true;
                                  _isActivatedButton = true;
                                });
                              }

                              TextSelection.fromPosition(
                                  TextPosition(offset: value.length));
                            },
                            validator: (value) {
                              if (value!.isEmpty) {
                                return 'Field required';
                              }
                              return null;
                            },
                            decoration: InputDecoration(
                                filled: true,
                                isDense: true,
                                border: InputBorder.none,

                                suffixIcon: Padding(
                                  padding:  EdgeInsets.only(
                                      right: MediaQuery.of(context).size.width * 0.02,
                                      top: MediaQuery.of(context).size.height * 0.005,
                                      bottom: MediaQuery.of(context).size.height * 0.005
                                  ),
                                  child: IconButton(
                                      onPressed: () {
                                        setState(() {
                                          _obscureText3 = !_obscureText3;
                                        });
                                      },
                                      icon: _obscureText3
                                          ? SvgPicture.asset(
                                        "images/visibility-icon-off.svg",
                                        height: MediaQuery.of(
                                            context)
                                            .size
                                            .height *
                                            0.018,
                                      )
                                          : SvgPicture.asset(
                                        "images/visibility-icon-on.svg",
                                        height: MediaQuery.of(
                                            context)
                                            .size
                                            .height *
                                            0.02,
                                      )
                                         ),
                                ),
                                contentPadding: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width * 0.05),
                                fillColor: _confirmPasswordFieldForm
                                    ? (_isValidForm
                                        ? Colors.white
                                        : Colors.white.withGreen(990))
                                    : (Colors.white),
                                focusColor: Colors.white,
                                hoverColor: _confirmPasswordFieldForm
                                    ? (_isValidForm
                                        ? Colors.green
                                        : Colors.red)
                                    : (Colors.white),
                                enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(60),
                                    borderSide: _confirmPasswordFieldForm
                                        ? (_isConfirmed && _isValidForm
                                            ? BorderSide(
                                                color: Colors.green)
                                            : BorderSide(color: Colors.red))
                                        : (BorderSide(color: const Color(0XFFD6D6E8)))),
                                focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(60),
                                    borderSide: _confirmPasswordFieldForm &&
                                            _isValidForm
                                        ? (_isConfirmed
                                            ? BorderSide(
                                                color: Colors.green)
                                            : BorderSide(color: Colors.red))
                                        : BorderSide(color: const Color(0XFFD6D6E8))),
                                errorBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(60),
                                    borderSide: BorderSide(
                                        color: Color(0xffF52079))),
                                focusedErrorBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(60),
                                    borderSide: BorderSide(
                                        color: Color(0xffF52079)))),
                          ),
                        ),
                      ),
                      _newPasswordFieldForm
                          ? Visibility(
                              visible:
                                  _newPasswordController.text.length > 0,
                              child: Padding(
                                padding: EdgeInsets.only(
                                    left: MediaQuery.of(context).size.width * 0.06
                                ),
                                child: Row(
                                  children: [
                                    !_isValidForm
                                        ? Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          children: [
                                            SizedBox(height: MediaQuery.of(context).size.height * 0.012,),
                                            Text(
                                                'Password must contain a minimum\nof 8 characters : ',

                                                style: GoogleFonts.poppins(
                                                  color: Color(0xffF52079),
                                                  fontSize: MediaQuery.of(context).size.height * 0.015,
                                                  fontWeight: FontWeight.w600,
                                                )),
                                            SizedBox(height: MediaQuery.of(context).size.height * 0.0075,),
                                            Text(
                                                "- One LowerCase letter.",
                                                style: GoogleFonts.poppins(
                                                  color: Color(0xffF52079),
                                                  fontSize: MediaQuery.of(context).size.height * 0.015,
                                                  fontWeight: FontWeight.w600,
                                                )),
                                            Text(
                                                "- One UpperCase letter.",
                                                style: GoogleFonts.poppins(
                                                  color: Color(0xffF52079),
                                                  fontSize: MediaQuery.of(context).size.height * 0.015,
                                                  fontWeight: FontWeight.w600,
                                                )),
                                            Text(
                                                "- One digit.",
                                                style: GoogleFonts.poppins(
                                                  color: Color(0xffF52079),
                                                  fontSize: MediaQuery.of(context).size.height * 0.015,
                                                  fontWeight: FontWeight.w600,
                                                )),
                                            Text(
                                                "- One special character: @\$ \!%*?&#./-+",
                                                style: GoogleFonts.poppins(
                                                  color: Color(0xffF52079),
                                                  fontSize: MediaQuery.of(context).size.height * 0.015,
                                                  fontWeight: FontWeight.w600,
                                                )),
                                            SizedBox(height: MediaQuery.of(context).size.height * 0.0075,),
                                          ],
                                        )
                                        : SizedBox()
                                  ],
                                ),
                              ),
                            )
                          : SizedBox(),
                      _confirmPasswordFieldForm
                          ? Padding(
                        padding: EdgeInsets.only(
                            left: MediaQuery.of(context).size.width * 0.06,
                            top: MediaQuery.of(context).size.height * 0.012,
                          bottom: MediaQuery.of(context).size.height * 0.012
                        ),
                              child: Visibility(
                                  child: !_isConfirmed
                                      ? Text(
                                        " Password must match !!",
                                        style: GoogleFonts.poppins(
                                          color: Color(0xffF52079),
                                          fontSize: MediaQuery.of(context).size.height * 0.015,
                                          fontWeight:
                                              FontWeight.w600,
                                        ),
                                      )
                                      : SizedBox()),
                            )
                          : SizedBox(),
                      Visibility(
                        visible: true,
                        child: wrongpassword
                            ? Padding(
                          padding: EdgeInsets.only(
                              left: MediaQuery.of(context).size.width * 0.06
                          ),
                                child: Align(
                                  alignment: Alignment.topLeft,
                                  child: Text(
                                    "Old password is wrong",
                                    style: GoogleFonts.poppins(
                                      color: Color(0xffF52079),
                                      fontSize: MediaQuery.of(context).size.height * 0.015,
                                      fontWeight: FontWeight.w600,
                                    ),
                                  ),
                                ),
                              )
                            : _sameLogin
                                ? Padding(
                          padding: EdgeInsets.only(
                              top: MediaQuery.of(context).size.height * 0.012,
                            left: MediaQuery.of(context).size.width * 0.06,
                            right: MediaQuery.of(context).size.width * 0.06
                          ),
                                    child: Text(
                                      "Please set a new password different from the old one",
                                      style: GoogleFonts.poppins(
                                        color: Color(0xffF52079),
                                        fontSize: MediaQuery.of(context).size.height * 0.015,
                                        fontWeight: FontWeight.w600,
                                      ),
                                    ),
                                  )
                                : SizedBox(),
                      ),
                      Visibility(
                        child: isNotExist
                            ? Padding(
                          padding: EdgeInsets.all(MediaQuery.of(context).size.height * 0.01),
                                child: Text(
                                  "Account doesn't exist",
                                  style: GoogleFonts.poppins(
                                    color: Color(0xffF52079),
                                    fontSize: 14.0,
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                              )
                            : SizedBox(),
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                          left: MediaQuery.of(context).size.width * 0.06,
                          top: MediaQuery.of(context).size.height * 0.03,
                          bottom: MediaQuery.of(context).size.height * 0.1
                        ),
                        child: SizedBox(
                          width: MediaQuery.of(context).size.width * 0.88,
                          height: MediaQuery.of(context).size.height * 0.06,
                          child: ElevatedButton(
                            style: ButtonStyle(
                              backgroundColor: MaterialStateProperty.all(
                                _isActivatedButton
                                    ? Color(0xff4048FF)
                                    : Color(0xFFF6F6FF),
                              ),
                              shape: MaterialStateProperty.all(
                                RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(
                                      MediaQuery.of(context).size.width *
                                          0.08),
                                ),
                              ),
                              side: _isActivatedButton ? MaterialStateProperty.all(BorderSide(color: Colors.transparent)): MaterialStateProperty.all(BorderSide(color: Color(0xFFD6D6E8)))
                            ),
                            onPressed: _isActivatedButton
                                ? () async {
                                    if (!_sameLogin) {
                                      var passwordChange =
                                          await LoginController
                                              .apiService
                                              .changePass(
                                                  Provider.of<LoginController>(
                                                              context,
                                                              listen: false)
                                                          .userDetails
                                                          ?.userToken ??
                                                      "",
                                                  _passwordController.text,
                                                  _newPasswordController
                                                      .text);
                                      setState(() {
                                        isChanged =
                                            (passwordChange == "changed");

                                        wrongpassword = (passwordChange ==
                                            "wrong password");
                                        isNotExist = (passwordChange ==
                                            "no account");
                                        _passwordController.text = "";
                                        _newPasswordController.text = "";
                                        _confirmPasswordController.text =
                                            "";
                                      });
                                      isChanged
                                          ? Fluttertoast.showToast(
                                              msg:
                                                  " Your password has been successfully changed!")
                                          : Fluttertoast.showToast(
                                              msg: "error");
                                    }
                                    setState(() {
                                      _isActivatedButton = false;
                                    });
                                  }
                                : null,
                            child: Text(
                              "Save",
                              style: GoogleFonts.poppins(
                                color: _isActivatedButton
                                    ? Colors.white
                                    : Color(0xffADADC8),
                                fontWeight: FontWeight.w600,
                                fontSize: MediaQuery.of(context).size.height * 0.02,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            HeaderNavigation(_isVisible, _isWalletVisible,
                _isNotificationVisible)
          ],
        ),
      ),
    );
  }
}
