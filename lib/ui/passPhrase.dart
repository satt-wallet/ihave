import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:ihave/ui/successWallet.dart';

class PassPhrase extends StatefulWidget {
  const PassPhrase({Key? key}) : super(key: key);

  @override
  _PassPhraseState createState() => _PassPhraseState();
}

class _PassPhraseState extends State<PassPhrase> {
  @override
  Widget build(BuildContext context) {
    return SuccessWallet();
  }
}
