import 'dart:convert';
import 'dart:io' show Platform;
import 'dart:typed_data';

import 'package:eosdart/eosdart.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/material.dart';
import 'package:flutter/material.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_api_availability/google_api_availability.dart';
import 'package:ihave/controllers/LoginController.dart';
import 'package:ihave/controllers/walletController.dart';
import 'package:ihave/model/userDetails.dart';
import 'package:ihave/ui/WelcomeScreen.dart';
import 'package:ihave/ui/passPhrase.dart';
import 'package:ihave/ui/signin.dart';
import 'package:ihave/model/user.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;
import 'package:google_sign_in/google_sign_in.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:ihave/ui/socialMail.dart';
import 'package:ihave/ui/towFactorPage.dart';
import 'package:ihave/ui/transactionpassword.dart';
import 'package:ihave/util/utils.dart';
import 'package:ihave/widgets/CustomSlider.dart';
import 'package:ihave/widgets/alertDialogWidget.dart';
import 'package:neoversion/neoversion.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sign_in_with_apple/sign_in_with_apple.dart';
import '../util/neoVersionOverriden.dart';
import 'creatwallet.dart';
import 'activationMail.dart';
import 'creatwallet.dart';
import 'package:url_launcher/url_launcher.dart';

class Signup extends StatefulWidget {
  Signup({Key? key}) : super(key: key);

  @override
  _SignupState createState() => _SignupState();
}

class _SignupState extends State<Signup> {
  TextEditingController _emailController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  String userEmail = "";
  bool isLoadingGoogle = false;
  bool isLoadingFacebook = false;
  String userPassword = "";
  bool isWalletExist = false;
  bool _obscureText = true;
  bool isNew = false;
  bool _mailFieldForm = false;
  bool _passwordFieldForm = false;
  bool hasUpperCase = false;
  bool hasLowerCase = false;
  bool hasSpecialCharacteres = false;
  bool _isvalidFormEmail = false;
  bool hasLength = false;
  bool hasNumber = false;
  RegExp RegUpperCase = RegExp(r'[A-Z]');
  RegExp? BlacklistingTextInputFormatter;
  RegExp RegLowerCase = RegExp(r'[a-z]');
  RegExp RegNumber = RegExp(r'[0-9]');
  RegExp RegSpecialChar = RegExp('[A-Za-z0-9]');
  double _strength = 0;
  bool _isActivatedButton = false;
  List<dynamic> captchaPuzzle = [];
  bool _isLoadingPuzzle = false;
  bool _textDisplay = true;
  bool _textDisplayError = false;
  double _value = 0;
  bool _isSuccessCaptcha = false;
  bool _isWrongCaptcha = false;
  String _displayText = '';
  bool _isLoading = false;
  bool _isButtonActive = true;
  bool _isLoginApple = false;
  bool _isLoadingApple = false;
  bool _isloginGoogle = false;
  bool _isloginFacebook = false;
  bool _checkedValue1 = false;
  bool _checkedValue2 = false;
  bool errorAccountExist = false;
  bool errorMailOrPass = false;

  bool _passwordFieldEdited = false;
  bool _emailFieldEdited = false;

  bool isFacebook = false;

  bool ischecked = false;

  bool _loadingApple = false;

  Future getCoinGeckoList() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (prefs.getString("coin-gecko") == null) {
      var result = await LoginController.apiService.getIdCoin();
      if (result != "error") {
        prefs.setString("coin-gecko", result);
      }
    }
  }

  Future save() async {
    var newUser = await Provider.of<LoginController>(context, listen: false).mailsignUp(user.email, user.password, _checkedValue2);
    isNew = (newUser == "welcome");
    var token = Provider.of<LoginController>(context, listen: false).userDetails!.userToken ?? "";
  }

  void initState() {
    reinitUserDetails();
    getCoinGeckoList();
    setState(() {
      _value = 0;
      _textDisplay = true;
      _textDisplayError = false;
      _isSuccessCaptcha = false;
      _isWrongCaptcha = false;
      _isLoading = false;
      _isButtonActive = true;
      ischecked = LoginController.apiService.isSwitched;
    });
    final neoVersion = NeoVersionOverriden(
      iOSAppId: '1610750323',
      androidAppId: 'us.atayen.ihave',
    );
    const simpleBehavior = true;

    if (simpleBehavior) {
      basicStatusCheck(neoVersion);
    } else {
      advancedStatusCheck(neoVersion);
    }
  }

  basicStatusCheck(NeoVersionOverriden neoVersion) {
    neoVersion.showAlertIfNecessary(context: context);
  }

  advancedStatusCheck(NeoVersionOverriden neoVersion) async {
    final status = await neoVersion.getVersionStatus();
    neoVersion.showUpdateDialog(
      context: context,
      status: status,
      title: 'Update',
      dialogText: (String local, String appstore, bool dismissable) => 'Custom Text - local: $local, appstore: $appstore, dismissable: $dismissable',
    );
  }

  void _checkPassword(String value) {
    if (_passwordController.text.length < 8) {
      setState(() {
        _strength = 1 / 4;
        _displayText = 'Low security level';
        _isActivatedButton = true;
        _passwordFieldForm = false;
      });
    } else {
      if (_passwordController.text.length < 10) {
        setState(() {
          _strength = 2 / 4;
          _displayText = 'Medium security level';
          _isActivatedButton = true;
          _passwordFieldForm = false;
        });
      } else {
        if (_passwordController.text.length >= 10 && RegLowerCase.hasMatch(_passwordController.text) && RegUpperCase.hasMatch(_passwordController.text) && checkSpecialChar(_passwordController.text)) {
          setState(() {
            _strength = 1;
            _displayText = 'High security level';
            _isActivatedButton = true;
            _passwordFieldForm = true;
          });
        }
      }
    }
  }

  bool checkSpecialChar(String pass) {
    var specialPass = pass.replaceAll(RegSpecialChar, "");
    return specialPass.length > 0;
  }

  User user = User('', '');

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () {
          Navigator.pushReplacement(context, MaterialPageRoute(builder: (_) => Signin()));
          return Future.value(false);
        },
        child: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
                colors: [
                  const Color(0xFF4048FF),
                  const Color(0xFF1F2337),
                ],
                begin: const FractionalOffset(0.0, -0.1),
                end: const FractionalOffset(0.0, 0.35),
                stops: [0.0, 1.0],
                tileMode: TileMode.clamp),
          ),
          child: Scaffold(
            backgroundColor: Colors.transparent,
            resizeToAvoidBottomInset: false,
            body: GestureDetector(
              onTap: () {
                FocusScope.of(context).requestFocus(new FocusNode());
              },
              child: SafeArea(
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      SizedBox(
                        height: MediaQuery.of(context).size.height * 0.025,
                      ),
                      SvgPicture.asset(
                        'images/logo-auth.svg',
                        alignment: Alignment.center,
                        width: MediaQuery.of(context).size.height * 0.04,
                        height: MediaQuery.of(context).size.height * 0.04,
                      ),
                      SizedBox(
                        height: MediaQuery.of(context).size.height * 0.03,
                      ),
                      Text(
                        "Sign Up",
                        style: GoogleFonts.poppins(fontWeight: FontWeight.w700, fontSize: MediaQuery.of(context).size.height * 0.035, color: Colors.white),
                      ),
                      SizedBox(
                        height: MediaQuery.of(context).size.height * 0.015,
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.15, right: MediaQuery.of(context).size.width * 0.15),
                        child: Text(
                          "Welcome on iHave !\nSubscribe with social networks to create your wallet.",
                          textAlign: TextAlign.center,
                          style: GoogleFonts.poppins(fontStyle: FontStyle.normal, fontWeight: FontWeight.w500, color: Colors.white, height: 1.2, fontSize: MediaQuery.of(context).size.width * 0.034, letterSpacing: 1.5),
                        ),
                      ),
                      SizedBox(
                        height: MediaQuery.of(context).size.height * 0.03,
                      ),
                      SizedBox(
                        child: SignUpControls(context),
                      ),
                      SizedBox(
                        height: MediaQuery.of(context).size.height * 0.015,
                      ),
                      Row(children: <Widget>[
                        Expanded(
                          child: new Container(
                              margin: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.06, right: MediaQuery.of(context).size.width * 0.015),
                              child: Divider(
                                color: Colors.white,
                                thickness: 1,
                                height: 36,
                              )),
                        ),
                        Text("or", style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.037, fontWeight: FontWeight.w400, color: Colors.white, fontStyle: FontStyle.normal)),
                        Expanded(
                          child: new Container(
                              margin: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.06, left: MediaQuery.of(context).size.width * 0.015),
                              child: Divider(
                                color: Colors.white,
                                thickness: 1,
                                height: 36,
                              )),
                        ),
                      ]),
                      SizedBox(
                        height: MediaQuery.of(context).size.height * 0.025,
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width * 0.75,
                        child: TextFormField(
                          scrollPadding: EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom.toString() == "0.0" ? MediaQuery.of(context).size.height * 0.2 : MediaQuery.of(context).size.height * 0.7),
                          style: GoogleFonts.poppins(color: Color(0xFF75758F), fontWeight: FontWeight.w600, fontStyle: FontStyle.normal, fontSize: MediaQuery.of(context).size.height * 0.021),
                          cursorHeight: MediaQuery.of(context).size.height * 0.03,
                          controller: _emailController,
                          onChanged: (value) {
                            setState(() {
                              _emailFieldEdited = true;
                              errorAccountExist = false;
                            });
                            TextSelection previousSelection = _emailController.selection;
                            _emailController.text = value;
                            user.email = _emailController.text;
                            _emailController.selection = previousSelection;
                            if (_emailController.text.isNotEmpty && RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(_emailController.text)) {
                              setState(() {
                                _mailFieldForm = true;
                              });
                            } else {
                              setState(() {
                                _mailFieldForm = false;
                              });
                            }
                            TextSelection.fromPosition(TextPosition(offset: _emailController.text.length));
                            if (_emailController.text.isNotEmpty) {
                              if (RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(value)) {
                                setState(() {
                                  _isvalidFormEmail = true;
                                });
                              } else {
                                setState(() {
                                  _isvalidFormEmail = false;
                                });
                              }
                            }
                          },
                          validator: (value) {
                            if (value!.isEmpty) {
                              return 'Field required';
                            } else if (RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(value)) {
                              return null;
                            } else {
                              return 'Enter valid email';
                            }
                          },
                          inputFormatters: [
                            FilteringTextInputFormatter.deny(RegExp(r'\s')),
                          ],
                          decoration: InputDecoration(
                              labelStyle: TextStyle(backgroundColor: !_isvalidFormEmail ? Colors.red : Colors.green),
                              contentPadding: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.height * 0.025, vertical: MediaQuery.of(context).size.height * 0.015),
                              hintText: 'EMAIL',
                              hintStyle: GoogleFonts.poppins(color: Color(0xFF75758F), fontWeight: FontWeight.w600, fontStyle: FontStyle.normal, fontSize: MediaQuery.of(context).size.height * 0.017),
                              suffixIcon: _emailFieldEdited
                                  ? Padding(
                                      padding: EdgeInsets.only(
                                        right: MediaQuery.of(context).size.width * 0.045,
                                        top: _isvalidFormEmail ? MediaQuery.of(context).size.height * 0.0135 : MediaQuery.of(context).size.height * 0.018,
                                        bottom: _isvalidFormEmail ? MediaQuery.of(context).size.height * 0.0135 : MediaQuery.of(context).size.height * 0.018,
                                      ),
                                      child: Visibility(
                                        child: !_isvalidFormEmail
                                            ? InkWell(
                                                onTap: () {
                                                  _emailController.text = "";
                                                },
                                                child: SvgPicture.asset(
                                                  "images/false-figma.svg",
                                                ),
                                              )
                                            : SvgPicture.asset(
                                                "images/check-figma.svg",
                                              ),
                                      ),
                                    )
                                  : Padding(
                                      padding: const EdgeInsets.all(0.0),
                                      child: Text(''),
                                    ),
                              fillColor: _emailFieldEdited ? (_isvalidFormEmail ? Colors.white : Colors.white.withGreen(990)) : (Colors.white),
                              focusColor: Colors.white,
                              hoverColor: _emailFieldEdited ? (_isvalidFormEmail ? Colors.green : Colors.red) : (Colors.white),
                              filled: true,
                              isDense: false,
                              enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: _emailFieldEdited ? (_isvalidFormEmail ? BorderSide(color: Colors.green) : BorderSide(color: Colors.red)) : (BorderSide(color: Colors.white))),
                              focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: _emailFieldEdited ? (_isvalidFormEmail ? BorderSide(color: Colors.green) : BorderSide(color: Colors.red)) : BorderSide(color: Colors.white)),
                              errorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red)),
                              focusedErrorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red))),
                        ),
                      ),
                      Visibility(
                        visible: (_emailController.text.length > 0),
                        child: Padding(
                          padding: !_isvalidFormEmail ? EdgeInsets.only(top: MediaQuery.of(context).size.width * 0.02, right: MediaQuery.of(context).size.width * 0.045) : EdgeInsets.all(0.0),
                          child: !_isvalidFormEmail
                              ? Align(
                                  alignment: Alignment.topLeft,
                                  child: Padding(
                                    padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.13),
                                    child: Text(
                                      "Field Email Address must have a valid form ",
                                      style: GoogleFonts.poppins(
                                        color: Color(0xFFd63384),
                                        fontWeight: FontWeight.bold,
                                        fontSize: MediaQuery.of(context).size.height * 0.015,
                                      ),
                                    ),
                                  ),
                                )
                              : null,
                        ),
                      ),
                      SizedBox(height: MediaQuery.of(context).size.height * 0.03),
                      Container(
                        width: MediaQuery.of(context).size.width * 0.75,
                        child: TextFormField(
                          scrollPadding: EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom.toString() == "0.0" ? MediaQuery.of(context).size.height * 0.2 : MediaQuery.of(context).size.height * 0.9),
                          style: GoogleFonts.poppins(color: const Color(0XFF323754), fontWeight: FontWeight.w400, fontStyle: FontStyle.normal, fontSize: MediaQuery.of(context).size.height * 0.021),
                          cursorHeight: MediaQuery.of(context).size.height * 0.03,
                          obscureText: _obscureText,
                          controller: _passwordController,
                          onChanged: (value) {
                            setState(() {
                              _passwordFieldEdited = true;
                              errorAccountExist = false;
                            });
                            TextSelection previousSelection = _passwordController.selection;
                            _passwordController.text = value;
                            user.password = _passwordController.text;
                            _passwordController.selection = previousSelection;
                            if (_passwordController.text.length != 0) {
                              _checkPassword(value);
                              if (RegUpperCase.hasMatch(_passwordController.text)) {
                                setState(() {
                                  hasUpperCase = true;
                                });
                              } else {
                                setState(() {
                                  hasUpperCase = false;
                                });
                              }
                              if (RegLowerCase.hasMatch(_passwordController.text)) {
                                setState(() {
                                  hasLowerCase = true;
                                });
                              } else {
                                setState(() {
                                  hasLowerCase = false;
                                });
                              }
                              if (RegNumber.hasMatch(_passwordController.text)) {
                                setState(() {
                                  hasNumber = true;
                                });
                              } else {
                                setState(() {
                                  hasNumber = false;
                                });
                              }
                              if (checkSpecialChar(_passwordController.text)) {
                                setState(() {
                                  hasSpecialCharacteres = true;
                                });
                              } else {
                                setState(() {
                                  hasSpecialCharacteres = false;
                                });
                              }
                              if (_passwordController.text.length >= 8) {
                                setState(() {
                                  hasLength = true;
                                });
                              } else {
                                setState(() {
                                  hasLength = false;
                                });
                              }
                              setState(() {
                                _passwordFieldForm = (hasUpperCase && hasLength && hasSpecialCharacteres && hasNumber && hasLowerCase);
                              });
                            } else {
                              setState(() {
                                _passwordFieldForm = false;
                              });
                            }
                          },
                          validator: (value) {
                            if (value!.isEmpty) {
                              return 'Field required';
                            }
                            return null;
                          },
                          decoration: InputDecoration(
                              contentPadding: EdgeInsets.only(
                                top: MediaQuery.of(context).size.height * 0.031,
                                left: MediaQuery.of(context).size.height * 0.025,
                              ),
                              hintText: 'PASSWORD',
                              hintStyle: GoogleFonts.poppins(color: Color(0xFF75758F), fontWeight: FontWeight.w600, fontStyle: FontStyle.normal, fontSize: MediaQuery.of(context).size.height * 0.017),
                              suffixStyle: TextStyle(color: Colors.grey),
                              suffixIcon: Padding(
                                padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.06),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    IconButton(
                                        onPressed: () {
                                          setState(() {
                                            _obscureText = !_obscureText;
                                          });
                                        },
                                        icon: _obscureText
                                            ? SvgPicture.asset(
                                                "images/visibility-icon-off.svg",
                                                height: MediaQuery.of(context).size.height * 0.02,
                                              )
                                            : SvgPicture.asset(
                                                "images/visibility-icon-on.svg",
                                                height: MediaQuery.of(context).size.height * 0.02,
                                              )),
                                    _passwordFieldEdited
                                        ? Visibility(
                                            child: !_passwordFieldForm
                                                ? InkWell(
                                                    onTap: () {
                                                      _passwordController.text = "";
                                                    },
                                                    child: SvgPicture.asset("images/false-figma.svg"))
                                                : SvgPicture.asset("images/check-figma.svg"))
                                        : Padding(
                                            padding: const EdgeInsets.all(0.0),
                                            child: Text(''),
                                          ),
                                  ],
                                ),
                              ),
                              fillColor: _passwordFieldEdited ? (_passwordFieldForm ? Colors.white : Colors.white.withGreen(990)) : (Colors.white),
                              focusColor: Colors.white,
                              hoverColor: _passwordFieldEdited ? (_passwordFieldForm ? Colors.green : Colors.red) : (Colors.white),
                              filled: true,
                              isDense: true,
                              enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: _passwordFieldEdited ? (_passwordFieldForm ? BorderSide(color: Colors.green) : BorderSide(color: Colors.red)) : (BorderSide(color: Colors.white))),
                              focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: _passwordFieldEdited ? (_passwordFieldForm ? BorderSide(color: Colors.green) : BorderSide(color: Colors.red)) : BorderSide(color: Colors.white)),
                              errorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red)),
                              focusedErrorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red))),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                          top: MediaQuery.of(context).size.height * 0.015,
                        ),
                        child: errorAccountExist
                            ? Container(
                                width: MediaQuery.of(context).size.width * 0.75,
                                height: MediaQuery.of(context).size.height * 0.068,
                                decoration: BoxDecoration(
                                  color: Colors.pink.withOpacity(0.2),
                                  borderRadius: BorderRadius.all(Radius.circular(20)),
                                ),
                                child: Row(
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.all(6.0),
                                      child: SvgPicture.asset("images/cross-pink.svg"),
                                    ),
                                    Text(isFacebook ? "This Email/Account already \n  exists, Please use Facebook Login" : "This Email/Account already \n  exists, Please use Google Login",
                                        style: GoogleFonts.poppins(
                                          fontWeight: FontWeight.bold,
                                          color: Colors.pink,
                                          fontSize: 12,
                                        )),
                                  ],
                                ))
                            : null,
                      ),
                      Padding(
                        padding: EdgeInsets.fromLTRB(0, 0, 0, 3),
                        child: errorMailOrPass
                            ? Container(
                                width: MediaQuery.of(context).size.width * 0.78,
                                height: MediaQuery.of(context).size.height * 0.06,
                                decoration: BoxDecoration(
                                  color: Colors.pink.withOpacity(0.2),
                                  borderRadius: BorderRadius.all(Radius.circular(20)),
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.all(1.0),
                                  child: Row(
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.all(3.0),
                                        child: SvgPicture.asset("images/cross-pink.svg"),
                                      ),
                                      Text("This Email already exists,  please \n use your email and password to login",
                                          style: GoogleFonts.poppins(
                                            fontWeight: FontWeight.bold,
                                            color: Colors.pink,
                                            fontSize: 12,
                                          )),
                                    ],
                                  ),
                                ))
                            : null,
                      ),
                      SizedBox(
                        height: MediaQuery.of(context).size.height * 0.01,
                      ),
                      Visibility(
                          visible: _passwordController.text.length > 0,
                          child: Column(children: [
                            Padding(
                                padding: _isButtonActive
                                    ? EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.035, 0, MediaQuery.of(context).size.width * 0.14, MediaQuery.of(context).size.height * 0.02)
                                    : EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.035, 0, MediaQuery.of(context).size.width * 0.14, MediaQuery.of(context).size.height * 0.02),
                                child: Text("Your password must contain at least :",
                                    style: GoogleFonts.poppins(
                                      fontWeight: FontWeight.w500,
                                      color: Colors.white,
                                      fontSize: MediaQuery.of(context).size.height * 0.015,
                                    ))),
                            Padding(
                              padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.165, 0, 0, 1),
                              child: !hasUpperCase
                                  ? Row(
                                      children: [
                                        SvgPicture.asset(
                                          "images/icon-wrong.svg",
                                          height: MediaQuery.of(context).size.height * 0.0155,
                                          width: MediaQuery.of(context).size.width * 0.027,
                                        ),
                                        SizedBox(
                                          width: MediaQuery.of(context).size.width * 0.045,
                                        ),
                                        Text(" UpperCase letter",
                                            style: GoogleFonts.poppins(
                                              fontWeight: FontWeight.normal,
                                              color: Colors.white,
                                              fontSize: MediaQuery.of(context).size.height * 0.015,
                                            )),
                                      ],
                                    )
                                  : Row(
                                      children: [
                                        SvgPicture.asset(
                                          "images/icon-checked.svg",
                                          height: MediaQuery.of(context).size.height * 0.0155,
                                          width: MediaQuery.of(context).size.width * 0.03,
                                        ),
                                        SizedBox(
                                          width: MediaQuery.of(context).size.width * 0.04,
                                        ),
                                        Text("UpperCase letter",
                                            style: GoogleFonts.poppins(
                                              fontWeight: FontWeight.normal,
                                              color: Colors.white,
                                              fontSize: MediaQuery.of(context).size.height * 0.015,
                                            )),
                                      ],
                                    ),
                            ),
                            Padding(
                              padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.165, MediaQuery.of(context).size.height * 0.005, 0, 1),
                              child: !hasLowerCase
                                  ? Row(
                                      children: [
                                        SvgPicture.asset(
                                          "images/icon-wrong.svg",
                                          height: MediaQuery.of(context).size.height * 0.0155,
                                          width: MediaQuery.of(context).size.width * 0.027,
                                        ),
                                        SizedBox(
                                          width: MediaQuery.of(context).size.width * 0.045,
                                        ),
                                        Text(" LowerCase letter",
                                            style: GoogleFonts.poppins(
                                              fontWeight: FontWeight.normal,
                                              color: Colors.white,
                                              fontSize: MediaQuery.of(context).size.height * 0.015,
                                            )),
                                      ],
                                    )
                                  : Row(
                                      children: [
                                        SvgPicture.asset(
                                          "images/icon-checked.svg",
                                          height: MediaQuery.of(context).size.height * 0.0155,
                                          width: MediaQuery.of(context).size.width * 0.03,
                                        ),
                                        SizedBox(
                                          width: MediaQuery.of(context).size.width * 0.04,
                                        ),
                                        Text("LowerCase letter",
                                            style: GoogleFonts.poppins(
                                              fontWeight: FontWeight.normal,
                                              color: Colors.white,
                                              fontSize: MediaQuery.of(context).size.height * 0.015,
                                            )),
                                      ],
                                    ),
                            ),
                            Padding(
                              padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.165, MediaQuery.of(context).size.height * 0.005, 0, 1),
                              child: !hasNumber
                                  ? Row(
                                      children: [
                                        SvgPicture.asset(
                                          "images/icon-wrong.svg",
                                          height: MediaQuery.of(context).size.height * 0.0155,
                                          width: MediaQuery.of(context).size.width * 0.027,
                                        ),
                                        SizedBox(
                                          width: MediaQuery.of(context).size.width * 0.045,
                                        ),
                                        Text(" Number",
                                            style: GoogleFonts.poppins(
                                              fontWeight: FontWeight.normal,
                                              color: Colors.white,
                                              fontSize: MediaQuery.of(context).size.height * 0.015,
                                            )),
                                      ],
                                    )
                                  : Row(
                                      children: [
                                        SvgPicture.asset(
                                          "images/icon-checked.svg",
                                          height: MediaQuery.of(context).size.height * 0.0155,
                                          width: MediaQuery.of(context).size.width * 0.03,
                                        ),
                                        SizedBox(
                                          width: MediaQuery.of(context).size.width * 0.04,
                                        ),
                                        Text("Number",
                                            style: GoogleFonts.poppins(
                                              fontWeight: FontWeight.normal,
                                              color: Colors.white,
                                              fontSize: MediaQuery.of(context).size.height * 0.015,
                                            )),
                                      ],
                                    ),
                            ),
                            Padding(
                              padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.165, MediaQuery.of(context).size.height * 0.005, 0, 1),
                              child: !hasSpecialCharacteres
                                  ? Row(
                                      children: [
                                        SvgPicture.asset(
                                          "images/icon-wrong.svg",
                                          height: MediaQuery.of(context).size.height * 0.0155,
                                          width: MediaQuery.of(context).size.width * 0.027,
                                        ),
                                        SizedBox(
                                          width: MediaQuery.of(context).size.width * 0.045,
                                        ),
                                        Text(" Special character",
                                            style: GoogleFonts.poppins(
                                              fontWeight: FontWeight.normal,
                                              color: Colors.white,
                                              fontSize: MediaQuery.of(context).size.height * 0.015,
                                            )),
                                      ],
                                    )
                                  : Row(
                                      children: [
                                        SvgPicture.asset(
                                          "images/icon-checked.svg",
                                          height: MediaQuery.of(context).size.height * 0.0155,
                                          width: MediaQuery.of(context).size.width * 0.03,
                                        ),
                                        SizedBox(
                                          width: MediaQuery.of(context).size.width * 0.04,
                                        ),
                                        Text("Special character",
                                            style: GoogleFonts.poppins(
                                              fontWeight: FontWeight.normal,
                                              color: Colors.white,
                                              fontSize: MediaQuery.of(context).size.height * 0.015,
                                            )),
                                      ],
                                    ),
                            ),
                            Padding(
                              padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.165, MediaQuery.of(context).size.height * 0.005, 0, 1),
                              child: !hasLength
                                  ? Row(
                                      children: [
                                        SvgPicture.asset(
                                          "images/icon-wrong.svg",
                                          height: MediaQuery.of(context).size.height * 0.0155,
                                          width: MediaQuery.of(context).size.width * 0.027,
                                        ),
                                        SizedBox(
                                          width: MediaQuery.of(context).size.width * 0.045,
                                        ),
                                        Text(" Password  is too short (8 chars minimum)",
                                            style: GoogleFonts.poppins(
                                              fontWeight: FontWeight.normal,
                                              color: Colors.white,
                                              fontSize: MediaQuery.of(context).size.height * 0.015,
                                            )),
                                      ],
                                    )
                                  : Row(
                                      children: [
                                        SvgPicture.asset(
                                          "images/icon-checked.svg",
                                          height: MediaQuery.of(context).size.height * 0.0155,
                                          width: MediaQuery.of(context).size.width * 0.03,
                                        ),
                                        SizedBox(
                                          width: MediaQuery.of(context).size.width * 0.04,
                                        ),
                                        Text("Password is too short",
                                            style: GoogleFonts.poppins(
                                              fontWeight: FontWeight.normal,
                                              color: Colors.white,
                                              fontSize: MediaQuery.of(context).size.height * 0.015,
                                            )),
                                      ],
                                    ),
                            ),
                          ])),
                      Visibility(
                        visible: _passwordController.text.length > 0,
                        child: Padding(
                          padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SizedBox(
                                height: MediaQuery.of(context).size.height * 0.035,
                              ),
                              Padding(
                                padding: EdgeInsets.only(
                                  left: MediaQuery.of(context).size.width * 0.1,
                                  top: MediaQuery.of(context).size.height * 0.02,
                                ),
                                child: Align(
                                  alignment: Alignment.topCenter,
                                  child: SizedBox(
                                    width: MediaQuery.of(context).size.width * 0.8,
                                    child: _isActivatedButton
                                        ? Row(
                                            children: [
                                              SizedBox(
                                                width: _strength == 1 ? MediaQuery.of(context).size.width * 0.7 : (_strength <= 1 / 4 ? MediaQuery.of(context).size.width * 0.2 : MediaQuery.of(context).size.width * 0.35),
                                                child: LinearProgressIndicator(
                                                  value: 2,
                                                  backgroundColor: Colors.transparent,
                                                  color: _strength <= 1 / 4
                                                      ? Color(0xFFF3247C)
                                                      : _strength == 2 / 4
                                                          ? Color(0XFFF9E756)
                                                          : _strength == 1
                                                              ? Color(0XFF00CC9E)
                                                              : Color(0xFF00CC9E),
                                                  minHeight: 3,
                                                ),
                                              ),
                                              if (_strength != 1)
                                                SizedBox(
                                                  width: _strength <= 1 / 4 ? MediaQuery.of(context).size.width * 0.5 : MediaQuery.of(context).size.width * 0.35,
                                                  child: LinearProgressIndicator(
                                                    value: 2,
                                                    color: Color(0xFFADADC8),
                                                    backgroundColor: Colors.transparent,
                                                    minHeight: 3,
                                                  ),
                                                )
                                            ],
                                          )
                                        : null,
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: MediaQuery.of(context).size.height * 0.03,
                              ),
                              Align(
                                alignment: Alignment.topCenter,
                                child: Container(
                                  height: MediaQuery.of(context).size.height * 0.043,
                                  width: MediaQuery.of(context).size.width * 0.6,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(30),
                                    color: _strength <= 1 / 4
                                        ? Color(0xFFF3247C)
                                        : _strength == 2 / 4
                                            ? Color(0XFFF9E756)
                                            : _strength == 1
                                                ? Color(0XFF00CC9E)
                                                : Color(0xFF00CC9E),
                                  ),
                                  child: Center(
                                    child: Text(
                                      _displayText,
                                      style: GoogleFonts.poppins(
                                        color: Colors.white,
                                        fontWeight: FontWeight.w600,
                                        fontSize: MediaQuery.of(context).size.height * 0.018,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: MediaQuery.of(context).size.height * 0.035,
                              )
                            ],
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.09, 0, MediaQuery.of(context).size.width * 0.08, 0),
                        child: Row(
                          children: [
                            Container(
                              decoration: BoxDecoration(color: _checkedValue1 ? Colors.white : Colors.transparent, border: Border.all(color: _checkedValue1 ? Color(0xFF00CC9E) : Color(0xFFADADC8), width: 2), borderRadius: BorderRadius.all(Radius.circular(5))),
                              width: 18,
                              height: 18,
                              child: Theme(
                                data: ThemeData(
                                    checkboxTheme: CheckboxThemeData(
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(5),
                                      ),
                                    ),
                                    unselectedWidgetColor: Colors.transparent),
                                child: Checkbox(
                                  checkColor: Color(0xFF00CC9E),
                                  activeColor: Colors.transparent,
                                  value: _checkedValue1,
                                  onChanged: (value) {
                                    setState(() {
                                      _checkedValue1 = value!;
                                    });
                                  },
                                ),
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.06),
                              child: Text("I have read, understood, and agree\nto the TCU and privacy policy", style: Theme.of(context).textTheme.bodyText2),
                            )
                          ],
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.09, MediaQuery.of(context).size.height * 0.01, MediaQuery.of(context).size.width * 0.08, 0),
                        child: Row(
                          children: [
                            Container(
                              decoration: BoxDecoration(color: _checkedValue2 ? Colors.white : Colors.transparent, border: Border.all(color: _checkedValue2 ? Color(0xFF00CC9E) : Color(0xFFADADC8), width: 2.5), borderRadius: BorderRadius.all(Radius.circular(5))),
                              width: 18,
                              height: 18,
                              child: Theme(
                                data: ThemeData(
                                    checkboxTheme: CheckboxThemeData(
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(5),
                                      ),
                                    ),
                                    unselectedWidgetColor: Colors.transparent),
                                child: Checkbox(
                                  checkColor: Color(0xFF00CC9E),
                                  activeColor: Colors.transparent,
                                  value: _checkedValue2,
                                  onChanged: (value) {
                                    setState(() {
                                      _checkedValue2 = value!;
                                    });
                                  },
                                ),
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.06),
                              child: Text("I agree to my e-mail being stored\nand used to receive the newsletter ", style: Theme.of(context).textTheme.bodyText2),
                            )
                          ],
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.fromLTRB(50, 16, 50, 0),
                        child: (_mailFieldForm && _passwordFieldForm && _checkedValue1)
                            ? Container(
                                height: MediaQuery.of(context).size.height * 0.055,
                                width: MediaQuery.of(context).size.width * 0.65,
                                child: ElevatedButton(
                                  style: ButtonStyle(
                                    backgroundColor: MaterialStateProperty.all(
                                      Color(0xFF00cc9e),
                                    ),
                                    shape: MaterialStateProperty.all(
                                      RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(30),
                                      ),
                                    ),
                                  ),
                                  onPressed: _isButtonActive
                                      ? () async {
                                          Provider.of<LoginController>(context, listen: false).kyc = [];
                                          Provider.of<LoginController>(context, listen: false).userPicture = null;
                                          setState(() {
                                            _isButtonActive = false;
                                          });

                                          setState(() {
                                            _value = 0;
                                            _textDisplay = true;
                                            _textDisplayError = false;
                                            _isSuccessCaptcha = false;
                                            _isWrongCaptcha = false;
                                            _isLoading = true;
                                          });
                                          captchaPuzzle = await LoginController.apiService.captcha();
                                          if (captchaPuzzle.isNotEmpty) {
                                            var originalPuzzle = captchaPuzzle[0].toString().substring(22);
                                            var puzzleImage = captchaPuzzle[1].toString().substring(22);
                                            var idPuzzleImage = captchaPuzzle[2].toString();
                                            Uint8List _bytesOriginalImage = base64Decode(originalPuzzle);
                                            Uint8List _bytesPuzzleImage = base64Decode(puzzleImage);
                                            showGeneralDialog(
                                                barrierDismissible: false,
                                                transitionBuilder: (context, animation, secondaryAnimation, child) {
                                                  return FadeTransition(
                                                    opacity: animation,
                                                    child: ScaleTransition(
                                                      scale: animation,
                                                      child: child,
                                                    ),
                                                  );
                                                },
                                                transitionDuration: Duration(milliseconds: 100),
                                                context: context,
                                                pageBuilder: (context, animation, secondaryAnimation) {
                                                  return StatefulBuilder(builder: (context, setState) {
                                                    return Scaffold(
                                                      body: SafeArea(
                                                        child: Container(
                                                          height: MediaQuery.of(context).size.height,
                                                          child: Column(
                                                            children: [
                                                              Row(
                                                                children: <Widget>[
                                                                  Padding(
                                                                    padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.025),
                                                                    child: Text(
                                                                      "Security Verification",
                                                                      style: GoogleFonts.poppins(color: Colors.black, fontWeight: FontWeight.w700, fontSize: MediaQuery.of(context).size.height * 0.02),
                                                                    ),
                                                                  ),
                                                                  Spacer(),
                                                                  Padding(
                                                                    padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.012),
                                                                    child: IconButton(
                                                                        onPressed: () async {
                                                                          Navigator.of(context).pop();

                                                                          captchaPuzzle = await LoginController.apiService.captcha();
                                                                          return stateInitalisation(context);
                                                                        },
                                                                        icon: Icon(
                                                                          Icons.close,
                                                                          color: Colors.black,
                                                                          size: MediaQuery.of(context).size.height * 0.03,
                                                                        )),
                                                                  ),
                                                                ],
                                                              ),
                                                              const Spacer(),
                                                              Stack(
                                                                children: [
                                                                  Image.memory(
                                                                    _bytesOriginalImage,
                                                                    height: 155,
                                                                    width: 310,
                                                                  ),
                                                                  Positioned(
                                                                    right: -_value,
                                                                    child: Image.memory(
                                                                      _bytesPuzzleImage,
                                                                      height: 155,
                                                                      width: 310,
                                                                      color: _isSuccessCaptcha ? Colors.green : (_isWrongCaptcha ? Colors.redAccent : null),
                                                                    ),
                                                                  ),
                                                                  Positioned(
                                                                    right: -MediaQuery.of(context).size.width * 0.02,
                                                                    child: Padding(
                                                                      padding: const EdgeInsets.all(8.0),
                                                                      child: IconButton(
                                                                        onPressed: () async {
                                                                          captchaPuzzle = await LoginController.apiService.captcha();
                                                                          originalPuzzle = captchaPuzzle[0].toString().substring(22);
                                                                          puzzleImage = captchaPuzzle[1].toString().substring(22);
                                                                          idPuzzleImage = captchaPuzzle[2].toString();
                                                                          setState(() {
                                                                            _bytesPuzzleImage = base64Decode(puzzleImage);
                                                                            _bytesOriginalImage = base64Decode(originalPuzzle);

                                                                            _textDisplay = true;
                                                                            _value = 0;
                                                                            _textDisplayError = false;
                                                                            _isSuccessCaptcha = false;
                                                                            _isWrongCaptcha = false;
                                                                          });
                                                                        },
                                                                        icon: Icon(
                                                                          Icons.refresh,
                                                                          color: Colors.white,
                                                                          size: 30,
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ),
                                                                ],
                                                              ),
                                                              const Spacer(),
                                                              Padding(
                                                                padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.01),
                                                                child: !_isSuccessCaptcha
                                                                    ? Stack(
                                                                        children: [
                                                                          SliderTheme(
                                                                            data: SliderTheme.of(context).copyWith(
                                                                              trackHeight: 40,
                                                                              inactiveTrackColor: const Color(0XFFF6F6FF),
                                                                              inactiveTickMarkColor: Color(0xFFF5F5F5),
                                                                              activeTrackColor: Color(0xFFF5F5F5),
                                                                              activeTickMarkColor: Color(0xFFF5F5F5),
                                                                              valueIndicatorColor: Colors.white,
                                                                              thumbColor: _isSuccessCaptcha ? Colors.green : (_isWrongCaptcha ? const Color(0XFFF52079) : const Color(0XFFD6D6E8)),
                                                                              thumbShape: !_isWrongCaptcha ? CustomSliderThumbRect(min: 0, max: 247, thumbRadius: 0, thumbHeight: 55) : CustomSliderThumbRectWrong(min: 0, max: 247, thumbRadius: 0, thumbHeight: 55),
                                                                            ),
                                                                            child: SizedBox(
                                                                              width: 350,
                                                                              child: Slider(
                                                                                value: _value,
                                                                                onChanged: (value) {
                                                                                  if (value >= 247) {
                                                                                  } else {
                                                                                    setState(() {
                                                                                      _value = value.roundToDouble();
                                                                                      _textDisplay = false;
                                                                                      _textDisplayError = false;
                                                                                      _isSuccessCaptcha = false;
                                                                                      _isWrongCaptcha = false;
                                                                                    });
                                                                                  }
                                                                                },
                                                                                max: 247,
                                                                                min: 0,
                                                                                onChangeStart: (value) {
                                                                                  setState(() {
                                                                                    _value = value;
                                                                                    _textDisplay = false;
                                                                                    _textDisplayError = false;
                                                                                    _isSuccessCaptcha = false;
                                                                                    _isWrongCaptcha = false;
                                                                                  });
                                                                                },
                                                                                onChangeEnd: (value) async {
                                                                                  _value = value;
                                                                                  int position = _value.toInt();
                                                                                  var result = await LoginController.apiService.verifyCaptcha(idPuzzleImage, position.toString());
                                                                                  if (result == "error") {
                                                                                    captchaPuzzle = await LoginController.apiService.captcha();
                                                                                    originalPuzzle = captchaPuzzle[0].toString().substring(22);
                                                                                    puzzleImage = captchaPuzzle[1].toString().substring(22);
                                                                                    idPuzzleImage = captchaPuzzle[2].toString();
                                                                                    setState(() {
                                                                                      _bytesPuzzleImage = base64Decode(puzzleImage);
                                                                                      _bytesOriginalImage = base64Decode(originalPuzzle);
                                                                                      _value = 0;
                                                                                      _textDisplay = false;
                                                                                      _textDisplayError = true;
                                                                                      _isWrongCaptcha = true;
                                                                                    });
                                                                                    Future.delayed(const Duration(milliseconds: 300), () {
                                                                                      setState(() {
                                                                                        _isWrongCaptcha = false;
                                                                                        _textDisplay = true;
                                                                                        _textDisplayError = false;
                                                                                      });
                                                                                    });
                                                                                  } else {
                                                                                    setState(() {
                                                                                      _value = value;
                                                                                      _textDisplay = false;
                                                                                      _textDisplayError = false;
                                                                                      _isSuccessCaptcha = true;
                                                                                      _isWrongCaptcha = false;
                                                                                    });
                                                                                    await save();
                                                                                    if (!isNew) {
                                                                                      Navigator.of(context).pop();
                                                                                      setState(() {
                                                                                        errorMailOrPass = true;
                                                                                        errorAccountExist = false;
                                                                                      });
                                                                                      return stateInitalisation(context);
                                                                                    } else {
                                                                                      if (Provider.of<LoginController>(context, listen: false).userDetails?.idSn != null) {
                                                                                        Navigator.pushReplacement(context, MaterialPageRoute(builder: (_) => SocialMail()));
                                                                                      } else {
                                                                                        final user = User(_emailController.text, _passwordController.text);
                                                                                        setState(() {
                                                                                          _emailController.text = "";
                                                                                          _passwordController.text = "";
                                                                                          _isLoading = false;
                                                                                        });
                                                                                        Navigator.pushReplacement(context, MaterialPageRoute(builder: (_) => ActivationMail()));
                                                                                      }
                                                                                    }
                                                                                  }
                                                                                },
                                                                              ),
                                                                            ),
                                                                          ),
                                                                          Padding(
                                                                              padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.15, top: MediaQuery.of(context).size.height * 0.012),
                                                                              child: SizedBox(
                                                                                child: _textDisplay
                                                                                    ? Text(
                                                                                        "Slide to complete the puzzle",
                                                                                        style: GoogleFonts.poppins(color: Colors.black, fontWeight: FontWeight.w400, fontSize: MediaQuery.of(context).size.height * 0.016, fontStyle: FontStyle.normal),
                                                                                      )
                                                                                    : (_textDisplayError
                                                                                        ? Text(
                                                                                            "Wrong captcha : Try again",
                                                                                            style: GoogleFonts.poppins(
                                                                                              fontWeight: FontWeight.w600,
                                                                                              fontSize: MediaQuery.of(context).size.height * 0.016,
                                                                                              fontStyle: FontStyle.normal,
                                                                                              color: const Color(0XFFF52079),
                                                                                            ),
                                                                                          )
                                                                                        : null),
                                                                              )),
                                                                        ],
                                                                      )
                                                                    : SizedBox(
                                                                        width: 280,
                                                                        child: LinearProgressIndicator(),
                                                                      ),
                                                              ),
                                                              const Spacer(),
                                                            ],
                                                          ),
                                                        ),
                                                      ),
                                                    );
                                                  });
                                                });
                                          } else {
                                            displayDialog(context, "Error", "Something went wrong, Please try again");
                                            setState(() {
                                              _isLoading = false;
                                              _isButtonActive = true;
                                            });
                                          }
                                        }
                                      : null,
                                  child: !_isLoading
                                      ? Text("Sign Up",
                                          style: GoogleFonts.poppins(
                                            color: Colors.white,
                                            fontWeight: FontWeight.w600,
                                            fontSize: MediaQuery.of(context).size.width * 0.04,
                                          ))
                                      : SizedBox(
                                          height: MediaQuery.of(context).size.height * 0.018,
                                          width: MediaQuery.of(context).size.height * 0.018,
                                          child: CircularProgressIndicator(
                                            color: Colors.white,
                                          )),
                                ),
                              )
                            : Container(
                                height: MediaQuery.of(context).size.height * 0.055,
                                width: MediaQuery.of(context).size.width * 0.65,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(30),
                                  color: Color(0xFFF6F6FF),
                                ),
                                child: Center(
                                  child: Text(
                                    "Sign Up",
                                    style: GoogleFonts.poppins(
                                      color: Color(0xFFADADC8),
                                      fontWeight: FontWeight.w600,
                                      fontSize: MediaQuery.of(context).size.width * 0.04,
                                    ),
                                  ),
                                ),
                              ),
                      ),
                      Padding(
                        padding: EdgeInsets.fromLTRB(
                          MediaQuery.of(context).size.width * 0.12,
                          MediaQuery.of(context).size.width * 0.025,
                          MediaQuery.of(context).size.width * 0.12,
                          MediaQuery.of(context).size.width * 0.12,
                        ),
                        child: Container(
                          height: MediaQuery.of(context).size.height * 0.055,
                          width: MediaQuery.of(context).size.width * 0.65,
                          child: ElevatedButton(
                              style: ButtonStyle(
                                backgroundColor: MaterialStateProperty.all(
                                  Color(0xFFFFFFFF),
                                ),
                                shape: MaterialStateProperty.all(
                                  RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(30),
                                  ),
                                ),
                              ),
                              onPressed: () {
                                gotoPageGlobal(context, Signin());
                              },
                              child: Text("I have an account",
                                  style: GoogleFonts.poppins(
                                    color: Color(0xFF4048FF),
                                    fontWeight: FontWeight.w600,
                                    fontSize: MediaQuery.of(context).size.width * 0.04,
                                  ))),
                        ),
                      ),
                      !_passwordFieldForm
                          ? SizedBox(
                              height: MediaQuery.of(context).viewInsets.bottom.toString() == "0.0" ? MediaQuery.of(context).size.height * 0.02 : MediaQuery.of(context).size.height * 0.4,
                            )
                          : Padding(padding: EdgeInsets.all(0.0)),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ));
  }

  SignUI(BuildContext context) {
    return Consumer<LoginController>(
      builder: (context, model, child) {
        if ((model.userDetails?.userToken != null) && (model.userDetails?.userToken != "")) {
          if (LoginController.apiService.isEnabled) {
            if (LoginController.apiService.walletAddress != "") {
              return Center(
                child: WalletCreatedUI(model),
              );
            } else {
              return Center(
                child: ActivatedUI(model),
              );
            }
          } else {
            return Center(
              child: SignupUI(model),
            );
          }
        } else {
          return SignUpControls(context);
        }
      },
    );
  }

  SignupUI(LoginController model) {
    gotoPageGlobal(context, SocialMail());
  }

  ActivatedUI(LoginController model) {
    gotoPageGlobal(context, TransactionPassword());
  }

  WalletCreatedUI(LoginController model) {
    gotoPageGlobal(context, WelcomeScreen());
  }

  SignUpControls(BuildContext context) {
    return Column(children: [
      Container(
          height: MediaQuery.of(context).size.height * 0.058,
          width: MediaQuery.of(context).size.width * 0.65,
          child: ElevatedButton(
            style: ButtonStyle(
              backgroundColor: MaterialStateProperty.all(
                Color(0xFF1967FF),
              ),
              shape: MaterialStateProperty.all(
                RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(MediaQuery.of(context).size.height * 0.07),
                ),
              ),
            ),
            onPressed: () {
              SignUpToSattWithFacebook();
            },
            child: isLoadingFacebook
                ? CircularProgressIndicator(
                    color: Colors.blueAccent,
                  )
                : Row(children: [
                    Padding(
                      padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.007, 0, 0, 0),
                      child: SvgPicture.asset(
                        'images/facebook-icon.svg',
                        alignment: Alignment.center,
                        color: Colors.white,
                        width: MediaQuery.of(context).size.width * 0.03,
                        height: MediaQuery.of(context).size.width * 0.047,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.06, 0, 0, 0),
                      child: Text(
                        "Sign up with Facebook",
                        style: GoogleFonts.roboto(color: Colors.white, fontWeight: FontWeight.w500, fontSize: MediaQuery.of(context).size.width * 0.038, fontStyle: FontStyle.normal),
                      ),
                    ),
                  ]),
          )),
      Padding(
        padding: EdgeInsets.only(top: MediaQuery.of(context).size.width * 0.025),
        child: Container(
            height: MediaQuery.of(context).size.height * 0.058,
            width: MediaQuery.of(context).size.width * 0.65,
            child: ElevatedButton(
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all(
                  Colors.white,
                ),
                shape: MaterialStateProperty.all(
                  RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(MediaQuery.of(context).size.height * 0.07),
                  ),
                ),
              ),
              onPressed: () async {
                if (await GoogleApiAvailability.instance.checkGooglePlayServicesAvailability() != GooglePlayServicesAvailability.serviceInvalid) {
                  SignUpToSattWithGoogle();
                }
              },
              child: isLoadingGoogle
                  ? CircularProgressIndicator(
                      color: Colors.blueAccent,
                    )
                  : Row(children: [
                      SvgPicture.asset(
                        'images/google_image.svg',
                        alignment: Alignment.center,
                        width: MediaQuery.of(context).size.width * 0.023,
                        height: MediaQuery.of(context).size.width * 0.043,
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.05),
                        child: Text(
                          "Sign up with Google",
                          style: GoogleFonts.roboto(color: Color(0xFF757575), fontWeight: FontWeight.w500, fontSize: MediaQuery.of(context).size.width * 0.038, fontStyle: FontStyle.normal),
                        ),
                      ),
                    ]),
            )),
      ),
      Padding(
        padding: EdgeInsets.only(top: MediaQuery.of(context).size.width * 0.025),
        child: Platform.isIOS
            ? (!_loadingApple
                ? Container(
                    height: MediaQuery.of(context).size.height * 0.058,
                    width: MediaQuery.of(context).size.width * 0.65,
                    child: SignInWithAppleButton(
                        height: MediaQuery.of(context).size.width * 0.095,
                        text: 'Sign up with Apple',
                        iconAlignment: IconAlignment.left,
                        borderRadius: BorderRadius.circular(60.0),
                        onPressed: () async {
                          try {
                            setState(() {
                              _loadingApple = true;
                            });
                            _isLoginApple = await Provider.of<LoginController>(context, listen: false).loginWithApple();
                            if (_isLoginApple) {
                              Provider.of<LoginController>(context, listen: false).userDetails?.picLink =
                                  Provider.of<LoginController>(context, listen: false).userDetails?.picLink == "AppleIDAuthorizationScopes.fullName" ? "" : Provider.of<LoginController>(context, listen: false).userDetails?.picLink;
                              Provider.of<LoginController>(context, listen: false).Balance = "";

                              if (LoginController.apiService.userMail == "") {
                                gotoPageGlobal(context, SocialMail());
                                setState(() {
                                  _loadingApple = false;
                                });
                              } else {
                                if (!LoginController.apiService.isEnabled) {
                                  Provider.of<LoginController>(context, listen: false).userDetails?.email = LoginController.apiService.userMail;
                                  gotoPageGlobal(context, ActivationMail());
                                  setState(() {
                                    _loadingApple = false;
                                  });
                                } else {
                                  if (!LoginController.apiService.hasWallet) {
                                    gotoPageGlobal(context, TransactionPassword());
                                    setState(() {
                                      _loadingApple = false;
                                    });
                                  } else {
                                    if (!LoginController.apiService.isCompletedPassPhrase) {
                                      gotoPageGlobal(context, PassPhrase());
                                      setState(() {
                                        _loadingApple = false;
                                      });
                                    } else {
                                      if (LoginController.apiService.is2FA) {
                                        gotoPageGlobal(context, TowFactorPage());
                                        setState(() {
                                          _loadingApple = false;
                                        });
                                      } else {
                                        Provider.of<WalletController>(context, listen: false).selectedIndex = 2;
                                        gotoPageGlobal(context, WelcomeScreen());
                                        setState(() {
                                          _loadingApple = false;
                                        });
                                      }
                                    }
                                  }
                                }
                              }
                            } else {
                              setState(() {
                                _loadingApple = false;
                              });
                            }
                          } catch (e) {
                            if (!e.toString().contains('AuthorizationErrorCode.canceled')) {
                              displayDialog(context, "Error", "Something went wrong, Please try again");
                              setState(() {
                                _loadingApple = false;
                              });
                            } else {
                              setState(() {
                                _loadingApple = false;
                              });
                            }
                          }
                        }),
                  )
                : SizedBox(
                    height: MediaQuery.of(context).size.height * 0.018,
                    width: MediaQuery.of(context).size.height * 0.018,
                    child: CircularProgressIndicator(
                      color: Colors.white,
                    )))
            : null,
      ),
    ]);
  }

  stateInitalisation(BuildContext context) {
    setState(() {
      _isLoading = false;
      _isButtonActive = true;
    });
  }

  _launchURLGCU() async {
    const url = 'https://satt.atayen.us/cgu';
    if (await canLaunch(url)) {
      await launch(url, forceSafariVC: true, forceWebView: false, enableJavaScript: true);
    } else {
      throw 'Could not launch $url';
    }
  }

  _launchURLPrivacy() async {
    const url = 'https://satt.atayen.us/privacy-policy';
    if (await canLaunch(url)) {
      await launch(url, forceSafariVC: true, forceWebView: false, enableJavaScript: true);
    } else {
      throw 'Could not launch $url';
    }
  }

  void SignUpToSattWithGoogle() async {
    setState(() {
      isFacebook = false;
      isLoadingGoogle = true;
    });
    GooglePlayServicesAvailability availability = await GoogleApiAvailability.instance.checkGooglePlayServicesAvailability(true);
    var google = await Provider.of<LoginController>(context, listen: false).googleSignUp();

    var token = await Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "";

    if (token != "" && token != "null" && token != "error" && token != "account_exists") {
      Navigator.pushReplacement(context, MaterialPageRoute(builder: (_) => SocialMail()));
      Fluttertoast.showToast(msg: "Create a wallet");
    } else if (token == "account_exists") {
      setState(() {
        errorAccountExist = true;
        errorMailOrPass = false;
      });
    }
    setState(() {
      isLoadingGoogle = false;
    });
  }

  neededUI(Widget page) {
    gotoPageGlobal(context, page);
  }

  loadNeededUi() {
    if (mounted) {
      var model = Provider.of<LoginController>(context, listen: false);
      if ((model.userDetails?.userToken != null) && (model.userDetails?.userToken != "null") && (model.userDetails?.userToken != "error") && (model.userDetails?.userToken != "")) {
        if (!LoginController.apiService.isComplete && model.userDetails?.idSn != null) {
          neededUI(SocialMail());
        } else {
          if (LoginController.apiService.isEnabled) {
            if (LoginController.apiService.walletAddress != null && LoginController.apiService.walletAddress != "") {
              neededUI(WelcomeScreen());
            } else {
              neededUI(TransactionPassword());
            }
          } else {
            neededUI(ActivationMail());
          }
        }
      }
    }
  }

  void SignUpToSattWithFacebook() async {
    setState(() {
      isFacebook = true;
      isLoadingFacebook = true;
    });
    await Provider.of<LoginController>(context, listen: false).facebookSignUp();

    var token = await Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "";

    if (token != "" && token != "null" && token != "error" && token != "account_exists") {
      gotoPageGlobal(context, SocialMail());
    } else if (token == "account_exists") {
      setState(() {
        errorAccountExist = true;
        errorMailOrPass = false;
      });
    }
    setState(() {
      isLoadingFacebook = false;
    });
  }
}
