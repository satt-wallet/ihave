import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:ihave/controllers/LoginController.dart';
import 'package:ihave/ui/signin.dart';
import 'package:provider/provider.dart';

class ResetPassword extends StatefulWidget {
  const ResetPassword({Key? key}) : super(key: key);

  @override
  _ResetPasswordState createState() => _ResetPasswordState();
}

class _ResetPasswordState extends State<ResetPassword> {
  final KeyGlobal = GlobalKey<FormState>();
  String mail = "";
  String code = "";
  TextEditingController _newPassword = TextEditingController();
  TextEditingController _confirmPassword = TextEditingController();

  bool isConfirmedPassword = false;
  bool _obscureText = true;
  bool _obscureText2 = true;
  bool isChanged = false;
  bool newpasswordEdited = false;
  bool isconfirmed = false;
  bool confirmedpasswordEdited = false;
  bool validForm = false;
  double _strength = 0;
  bool hasUpperCase = false;
  bool hasLowerCase = false;
  bool hasSpecialCharacteres = false;

  bool hasLength = false;
  bool hasNumber = false;
  RegExp numReg = RegExp(r".*[0-9].*");
  RegExp letterReg = RegExp(r".*[A-Za-z].*");
  RegExp RegSpecialChar = RegExp('[A-Za-z0-9]');
  RegExp RegUpperCase = RegExp(r'[A-Z]');
  RegExp? BlacklistingTextInputFormatter;
  RegExp RegLowerCase = RegExp(r'[a-z]');
  RegExp RegNumber = RegExp(r'[0-9]');

  bool _isActivatedButton = false;
  String _displayText = '';

  bool checkSpecialChar(String pass) {
    var specialPass = pass.replaceAll(RegSpecialChar, "");
    return specialPass.length > 0;
  }

  void _checkPassword(String value) {
    if (_newPassword.text.length < 8) {
      setState(() {
        _strength = 1 / 4;
        _displayText = 'Low security level';
        _isActivatedButton = true;
      });
    } else {
      if (_newPassword.text.length < 10) {
        setState(() {
          _strength = 2 / 4;
          _displayText = 'Medium security level';
          _isActivatedButton = true;
        });
      } else {
        if (_newPassword.text.length >= 10 && RegLowerCase.hasMatch(_newPassword.text) && RegUpperCase.hasMatch(_newPassword.text) && checkSpecialChar(_newPassword.text)) {
          setState(() {
            _strength = 1;
            _displayText = 'High security level';
            _isActivatedButton = true;
          });
        }
      }
    }
  }

  @override
  void initState() {
    mail = Provider.of<LoginController>(context, listen: false).recoveryEmail;
    setState(() {
      _isActivatedButton = false;
    });
    Provider.of<LoginController>(context, listen: false).tokenSymbol == "";
    Provider.of<LoginController>(context, listen: false).tokenNetwork == "";
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
            colors: [
              const Color(0xFFF52079),
              const Color(0xFF1F2337),
            ],
            begin: const FractionalOffset(0.0, -0.1),
            end: const FractionalOffset(0.0, 0.35),
            stops: [0.0, 1.0],
            tileMode: TileMode.clamp),
      ),
      child: Scaffold(
          backgroundColor: Colors.transparent,
          appBar: null,
          body: SafeArea(
            child: SingleChildScrollView(
              child: Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(
                      height: MediaQuery.of(context).size.height * 0.025,
                    ),
                    SvgPicture.asset(
                      'images/logo-auth.svg',
                      alignment: Alignment.center,
                      width: MediaQuery.of(context).size.height * 0.04,
                      height: MediaQuery.of(context).size.height * 0.04,
                    ),
                    SizedBox(height: MediaQuery.of(context).size.height * 0.05),
                    Text(
                      "Reset your\nPassword",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(color: Colors.white, fontSize: MediaQuery.of(context).size.height * 0.04, fontWeight: FontWeight.w700, height: 1),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.04, right: MediaQuery.of(context).size.width * 0.035, left: MediaQuery.of(context).size.width * 0.035),
                      child: Text(
                        "This new password must be different from the previous one, as well as your transaction password",
                        textAlign: TextAlign.center,
                        style: GoogleFonts.poppins(color: Colors.white, fontSize: MediaQuery.of(context).size.height * 0.019, fontWeight: FontWeight.w500, letterSpacing: 0.7, height: 1.15),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.1, top: MediaQuery.of(context).size.height * 0.045),
                      child: Align(
                        alignment: Alignment.topLeft,
                        child: Text(
                          "NEW PASSWORD",
                          style: GoogleFonts.poppins(
                            color: Colors.white,
                            fontSize: MediaQuery.of(context).size.height * 0.018,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.0075),
                      child: SizedBox(
                        width: MediaQuery.of(context).size.width * 0.8,
                        child: TextFormField(
                          onTap: () {
                            setState(() {
                              newpasswordEdited = true;
                            });
                          },
                          controller: _newPassword,
                          scrollPadding: EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom.toString() == "0.0" ? MediaQuery.of(context).size.height * 0.2 : MediaQuery.of(context).size.height * 0.5),
                          keyboardType: TextInputType.visiblePassword,
                          obscureText: _obscureText,
                          onChanged: (value) {
                            if (_newPassword.text.length > 0) {
                              setState(() {
                                newpasswordEdited = true;
                              });
                            }
                            if (_newPassword.text == _confirmPassword.text) {
                              setState(() {
                                isconfirmed = true;
                              });
                            } else {
                              setState(() {
                                isconfirmed = false;
                              });
                            }

                            _newPassword.text = value;
                            _newPassword.selection = TextSelection.fromPosition(TextPosition(offset: _newPassword.text.length));
                            if (_newPassword.text.length != 0) {
                              _checkPassword(value);
                              if (RegUpperCase.hasMatch(_newPassword.text)) {
                                setState(() {
                                  hasUpperCase = true;
                                });
                              } else {
                                setState(() {
                                  hasUpperCase = false;
                                });
                              }
                              if (RegLowerCase.hasMatch(_newPassword.text)) {
                                setState(() {
                                  hasLowerCase = true;
                                });
                              } else {
                                setState(() {
                                  hasLowerCase = false;
                                });
                              }
                              if (RegNumber.hasMatch(_newPassword.text)) {
                                setState(() {
                                  hasNumber = true;
                                });
                              } else {
                                setState(() {
                                  hasNumber = false;
                                });
                              }
                              if (checkSpecialChar(_newPassword.text)) {
                                setState(() {
                                  hasSpecialCharacteres = true;
                                });
                              } else {
                                setState(() {
                                  hasSpecialCharacteres = false;
                                });
                              }
                              if (_newPassword.text.length >= 8) {
                                setState(() {
                                  hasLength = true;
                                });
                              } else {
                                setState(() {
                                  hasLength = false;
                                });
                              }
                              setState(() {
                                validForm = (hasLength && hasSpecialCharacteres && hasNumber && hasLowerCase && hasUpperCase);
                              });
                            } else {
                              setState(() {
                                validForm = false;
                              });
                            }
                            TextSelection.fromPosition(TextPosition(offset: value.length));
                          },
                          validator: (value) {
                            if (value!.isEmpty) {
                              return 'Field required';
                            }
                            return null;
                          },
                          decoration: InputDecoration(
                              contentPadding: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.height * 0.025, vertical: MediaQuery.of(context).size.height * 0.015),
                              fillColor: Colors.white,
                              focusColor: Colors.white,
                              hoverColor: Colors.white,
                              filled: true,
                              isDense: true,
                              border: InputBorder.none,
                              suffixStyle: TextStyle(
                                color: Colors.grey,
                              ),
                              suffixIcon: Padding(
                                padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.025),
                                child: IconButton(
                                    onPressed: () {
                                      setState(() {
                                        _obscureText = !_obscureText;
                                      });
                                    },
                                    icon: _obscureText
                                        ? SvgPicture.asset(
                                            "images/visibility-icon-off.svg",
                                            height: MediaQuery.of(context).size.height * 0.018,
                                          )
                                        : SvgPicture.asset(
                                            "images/visibility-icon-on.svg",
                                            height: MediaQuery.of(context).size.height * 0.02,
                                          )),
                              ),
                              enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.white)),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(60),
                                borderSide: BorderSide(color: Colors.white),
                              ),
                              errorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red)),
                              focusedErrorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red))),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.1, top: MediaQuery.of(context).size.height * 0.025),
                      child: Align(
                        alignment: Alignment.topLeft,
                        child: Text(
                          "CONFIRM NEW PASSWORD",
                          style: GoogleFonts.poppins(
                            color: Colors.white,
                            fontSize: MediaQuery.of(context).size.height * 0.018,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.0075),
                      child: SizedBox(
                        width: MediaQuery.of(context).size.width * 0.8,
                        child: TextFormField(
                          controller: _confirmPassword,
                          scrollPadding: EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom.toString() == "0.0" ? MediaQuery.of(context).size.height * 0.2 : MediaQuery.of(context).size.height * 0.38),
                          keyboardType: TextInputType.visiblePassword,
                          obscureText: _obscureText2,
                          onChanged: (value) {
                            if (_confirmPassword.text.length > 0) {
                              setState(() {
                                confirmedpasswordEdited = true;
                              });
                              if (_newPassword.text == _confirmPassword.text) {
                                setState(() {
                                  isconfirmed = true;
                                });
                              } else {
                                setState(() {
                                  isconfirmed = false;
                                });
                              }
                            } else {
                              setState(() {
                                confirmedpasswordEdited = false;
                              });
                            }

                            _confirmPassword.text = value;
                            _confirmPassword.selection = TextSelection.fromPosition(TextPosition(offset: _confirmPassword.text.length));

                            if (_confirmPassword.text != _newPassword.text) {
                              setState(() {
                                isconfirmed = false;
                                _isActivatedButton == false;
                              });
                            } else {
                              setState(() {
                                isconfirmed = true;
                                _isActivatedButton = true;
                              });
                            }
                          },
                          validator: (value) {
                            if (value!.isEmpty) {
                              return 'Field required';
                            }

                            return null;
                          },
                          decoration: InputDecoration(
                              contentPadding: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.height * 0.025, vertical: MediaQuery.of(context).size.height * 0.015),
                              fillColor: Colors.white,
                              focusColor: Colors.white,
                              hoverColor: Colors.white,
                              filled: true,
                              isDense: true,
                              border: InputBorder.none,
                              suffixStyle: TextStyle(
                                color: Colors.grey,
                              ),
                              suffixIcon: Padding(
                                padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.025),
                                child: IconButton(
                                    onPressed: () {
                                      setState(() {
                                        _obscureText2 = !_obscureText2;
                                      });
                                    },
                                    icon: _obscureText2
                                        ? SvgPicture.asset(
                                            "images/visibility-icon-off.svg",
                                            height: MediaQuery.of(context).size.height * 0.018,
                                          )
                                        : SvgPicture.asset(
                                            "images/visibility-icon-on.svg",
                                            height: MediaQuery.of(context).size.height * 0.02,
                                          )),
                              ),
                              enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.white)),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(60),
                                borderSide: BorderSide(color: Colors.white),
                              ),
                              errorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red)),
                              focusedErrorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red))),
                        ),
                      ),
                    ),
                    if (confirmedpasswordEdited)
                      Visibility(
                        child: isconfirmed
                            ? Padding(
                                padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.1, top: MediaQuery.of(context).size.height * 0.025),
                                child: Row(
                                  children: [
                                    SvgPicture.asset(
                                      "images/input-checked-icon.svg",
                                      height: 20,
                                      width: 20,
                                      color: Color(0xff00CC9E),
                                    ),
                                    SizedBox(
                                      width: MediaQuery.of(context).size.width * 0.02,
                                    ),
                                    Text(
                                      "The passwords match.",
                                      style: GoogleFonts.poppins(color: Color(0xff00CC9E), fontWeight: FontWeight.w600),
                                    )
                                  ],
                                ),
                              )
                            : Padding(
                                padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.1, top: MediaQuery.of(context).size.height * 0.02),
                                child: Align(
                                  alignment: Alignment.topLeft,
                                  child: Text(
                                    "Password must match!!",
                                    textAlign: TextAlign.center,
                                    style: GoogleFonts.poppins(color: Color(0xffF52079), fontWeight: FontWeight.w600),
                                  ),
                                ),
                              ),
                      ),
                    if (newpasswordEdited)
                      Column(
                        children: [
                          Visibility(
                              visible: _newPassword.text.length > 0,
                              child: Column(children: [
                                Padding(
                                    padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.05, MediaQuery.of(context).size.height * 0.025, MediaQuery.of(context).size.width * 0.14, MediaQuery.of(context).size.height * 0.025),
                                    child: Text("Your password must contain at least :",
                                        style: GoogleFonts.poppins(
                                          fontWeight: FontWeight.w500,
                                          color: Colors.white,
                                          fontSize: MediaQuery.of(context).size.height * 0.016,
                                        ))),
                                Padding(
                                  padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.165, 0, 0, 1),
                                  child: !hasUpperCase
                                      ? Row(
                                          children: [
                                            SvgPicture.asset(
                                              "images/icon-wrong.svg",
                                              height: MediaQuery.of(context).size.height * 0.0155,
                                              width: MediaQuery.of(context).size.width * 0.027,
                                            ),
                                            SizedBox(
                                              width: MediaQuery.of(context).size.width * 0.046,
                                            ),
                                            Text("UpperCase letter",
                                                style: GoogleFonts.poppins(
                                                  fontWeight: FontWeight.normal,
                                                  color: Colors.white,
                                                  fontSize: MediaQuery.of(context).size.height * 0.016,
                                                )),
                                          ],
                                        )
                                      : Row(
                                          children: [
                                            SvgPicture.asset(
                                              "images/icon-checked.svg",
                                              height: MediaQuery.of(context).size.height * 0.0155,
                                              width: MediaQuery.of(context).size.width * 0.03,
                                            ),
                                            SizedBox(
                                              width: MediaQuery.of(context).size.width * 0.04,
                                            ),
                                            Text("UpperCase letter",
                                                style: GoogleFonts.poppins(
                                                  fontWeight: FontWeight.normal,
                                                  color: Colors.white,
                                                  fontSize: MediaQuery.of(context).size.height * 0.016,
                                                )),
                                          ],
                                        ),
                                ),
                                Padding(
                                  padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.165, MediaQuery.of(context).size.height * 0.005, 0, 1),
                                  child: !hasLowerCase
                                      ? Row(
                                          children: [
                                            SvgPicture.asset(
                                              "images/icon-wrong.svg",
                                              height: MediaQuery.of(context).size.height * 0.0155,
                                              width: MediaQuery.of(context).size.width * 0.027,
                                            ),
                                            SizedBox(
                                              width: MediaQuery.of(context).size.width * 0.046,
                                            ),
                                            Text("LowerCase letter",
                                                style: GoogleFonts.poppins(
                                                  fontWeight: FontWeight.normal,
                                                  color: Colors.white,
                                                  fontSize: MediaQuery.of(context).size.height * 0.016,
                                                )),
                                          ],
                                        )
                                      : Row(
                                          children: [
                                            SvgPicture.asset(
                                              "images/icon-checked.svg",
                                              height: MediaQuery.of(context).size.height * 0.0155,
                                              width: MediaQuery.of(context).size.width * 0.02,
                                            ),
                                            SizedBox(
                                              width: MediaQuery.of(context).size.width * 0.04,
                                            ),
                                            Text("LowerCase letter",
                                                style: GoogleFonts.poppins(
                                                  fontWeight: FontWeight.normal,
                                                  color: Colors.white,
                                                  fontSize: MediaQuery.of(context).size.height * 0.016,
                                                )),
                                          ],
                                        ),
                                ),
                                Padding(
                                  padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.165, MediaQuery.of(context).size.height * 0.005, 0, 1),
                                  child: !hasNumber
                                      ? Row(
                                          children: [
                                            SvgPicture.asset(
                                              "images/icon-wrong.svg",
                                              height: MediaQuery.of(context).size.height * 0.0155,
                                              width: MediaQuery.of(context).size.width * 0.027,
                                            ),
                                            SizedBox(
                                              width: MediaQuery.of(context).size.width * 0.046,
                                            ),
                                            Text("Number",
                                                style: GoogleFonts.poppins(
                                                  fontWeight: FontWeight.normal,
                                                  color: Colors.white,
                                                  fontSize: MediaQuery.of(context).size.height * 0.016,
                                                )),
                                          ],
                                        )
                                      : Row(
                                          children: [
                                            SvgPicture.asset(
                                              "images/icon-checked.svg",
                                              height: MediaQuery.of(context).size.height * 0.0155,
                                              width: MediaQuery.of(context).size.width * 0.03,
                                            ),
                                            SizedBox(
                                              width: MediaQuery.of(context).size.width * 0.04,
                                            ),
                                            Text("Number",
                                                style: GoogleFonts.poppins(
                                                  fontWeight: FontWeight.normal,
                                                  color: Colors.white,
                                                  fontSize: MediaQuery.of(context).size.height * 0.016,
                                                )),
                                          ],
                                        ),
                                ),
                                Padding(
                                  padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.165, MediaQuery.of(context).size.height * 0.005, 0, 1),
                                  child: !hasSpecialCharacteres
                                      ? Row(
                                          children: [
                                            SvgPicture.asset(
                                              "images/icon-wrong.svg",
                                              height: MediaQuery.of(context).size.height * 0.0155,
                                              width: MediaQuery.of(context).size.width * 0.027,
                                            ),
                                            SizedBox(
                                              width: MediaQuery.of(context).size.width * 0.045,
                                            ),
                                            Text("Special character",
                                                style: GoogleFonts.poppins(
                                                  fontWeight: FontWeight.normal,
                                                  color: Colors.white,
                                                  fontSize: MediaQuery.of(context).size.height * 0.016,
                                                )),
                                          ],
                                        )
                                      : Row(
                                          children: [
                                            SvgPicture.asset(
                                              "images/icon-checked.svg",
                                              height: MediaQuery.of(context).size.height * 0.0155,
                                              width: MediaQuery.of(context).size.width * 0.03,
                                            ),
                                            SizedBox(
                                              width: MediaQuery.of(context).size.width * 0.04,
                                            ),
                                            Text("Special character",
                                                style: GoogleFonts.poppins(
                                                  fontWeight: FontWeight.normal,
                                                  color: Colors.white,
                                                  fontSize: MediaQuery.of(context).size.height * 0.016,
                                                )),
                                          ],
                                        ),
                                ),
                                Padding(
                                  padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.165, MediaQuery.of(context).size.height * 0.005, 0, 1),
                                  child: !hasLength
                                      ? Row(
                                          children: [
                                            SvgPicture.asset(
                                              "images/icon-wrong.svg",
                                              height: MediaQuery.of(context).size.height * 0.0155,
                                              width: MediaQuery.of(context).size.width * 0.027,
                                            ),
                                            SizedBox(
                                              width: MediaQuery.of(context).size.width * 0.047,
                                            ),
                                            Text("Password  is too short",
                                                style: GoogleFonts.poppins(
                                                  fontWeight: FontWeight.normal,
                                                  color: Colors.white,
                                                  fontSize: MediaQuery.of(context).size.height * 0.016,
                                                )),
                                          ],
                                        )
                                      : Row(
                                          children: [
                                            SvgPicture.asset(
                                              "images/icon-checked.svg",
                                              height: MediaQuery.of(context).size.height * 0.0155,
                                              width: MediaQuery.of(context).size.width * 0.03,
                                            ),
                                            SizedBox(
                                              width: MediaQuery.of(context).size.width * 0.04,
                                            ),
                                            Text("Password is too short",
                                                style: GoogleFonts.poppins(
                                                  fontWeight: FontWeight.normal,
                                                  color: Colors.white,
                                                  fontSize: MediaQuery.of(context).size.height * 0.016,
                                                )),
                                          ],
                                        ),
                                ),
                              ])),
                          Visibility(
                            visible: _newPassword.text.length > 0,
                            child: Padding(
                              padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  SizedBox(
                                    height: MediaQuery.of(context).size.height * 0.01,
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(
                                      left: MediaQuery.of(context).size.width * 0.1,
                                      top: MediaQuery.of(context).size.height * 0.02,
                                    ),
                                    child: Align(
                                      alignment: Alignment.topCenter,
                                      child: SizedBox(
                                        width: MediaQuery.of(context).size.width * 0.8,
                                        child: _isActivatedButton
                                            ? Row(
                                                children: [
                                                  SizedBox(
                                                    width: _strength == 1 ? MediaQuery.of(context).size.width * 0.7 : (_strength <= 1 / 4 ? MediaQuery.of(context).size.width * 0.2 : MediaQuery.of(context).size.width * 0.35),
                                                    child: LinearProgressIndicator(
                                                      value: 2,
                                                      backgroundColor: Colors.transparent,
                                                      color: _strength <= 1 / 4
                                                          ? Color(0xFFF3247C)
                                                          : _strength == 2 / 4
                                                              ? Color(0XFFF9E756)
                                                              : _strength == 1
                                                                  ? Color(0XFF00CC9E)
                                                                  : Color(0xFF00CC9E),
                                                      minHeight: 3,
                                                    ),
                                                  ),
                                                  if (_strength != 1)
                                                    SizedBox(
                                                      width: _strength <= 1 / 4 ? MediaQuery.of(context).size.width * 0.5 : MediaQuery.of(context).size.width * 0.35,
                                                      child: LinearProgressIndicator(
                                                        value: 2,
                                                        color: Color(0xFFADADC8),
                                                        backgroundColor: Colors.transparent,
                                                        minHeight: 3,
                                                      ),
                                                    )
                                                ],
                                              )
                                            : null,
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    height: MediaQuery.of(context).size.height * 0.03,
                                  ),
                                  Align(
                                    alignment: Alignment.topCenter,
                                    child: Container(
                                      height: MediaQuery.of(context).size.height * 0.043,
                                      width: MediaQuery.of(context).size.width * 0.6,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(30),
                                        color: _strength <= 1 / 4
                                            ? Color(0xFFF3247C)
                                            : _strength == 2 / 4
                                                ? Color(0XFFF9E756)
                                                : _strength == 1
                                                    ? Color(0XFF00CC9E)
                                                    : Color(0xFF00CC9E),
                                      ),
                                      child: Center(
                                        child: Text(
                                          _displayText,
                                          style: GoogleFonts.poppins(
                                            color: Colors.white,
                                            fontWeight: FontWeight.w600,
                                            fontSize: MediaQuery.of(context).size.height * 0.018,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    height: MediaQuery.of(context).size.height * 0.035,
                                  )
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    Padding(
                      padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.035, bottom: MediaQuery.of(context).size.height * 0.05),
                      child: SizedBox(
                        width: MediaQuery.of(context).size.width * 0.8,
                        height: MediaQuery.of(context).size.height * 0.06,
                        child: ElevatedButton(
                          style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.all(
                              isconfirmed && validForm ? const Color(0xff4048FF) : const Color(0xFFffffff),
                            ),
                            shape: MaterialStateProperty.all(
                              RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.08),
                              ),
                            ),
                          ),
                          onPressed: isconfirmed && validForm
                              ? () async {
                                  code = Provider.of<LoginController>(context, listen: false).code;
                                  var passwordChange = await LoginController.apiService.changePassword(mail, _newPassword.text, code);
                                  isChanged = (passwordChange == "success");
                                  if (isChanged) {
                                    showDialog(
                                        barrierDismissible: false,
                                        context: context,
                                        builder: (BuildContext context) {
                                          return AlertDialog(
                                              insetPadding: EdgeInsets.zero,
                                              shape: RoundedRectangleBorder(
                                                borderRadius: BorderRadius.all(Radius.circular(MediaQuery.of(context).size.width * 0.05)),
                                              ),
                                              content: Container(
                                                height: MediaQuery.of(context).size.height * 0.32,
                                                width: MediaQuery.of(context).size.width * 0.6,
                                                child: Column(
                                                  children: [
                                                    Row(
                                                      mainAxisAlignment: MainAxisAlignment.start,
                                                      crossAxisAlignment: CrossAxisAlignment.start,
                                                      children: <Widget>[
                                                        SizedBox(
                                                          width: MediaQuery.of(context).size.width * 0.15,
                                                        ),
                                                        Padding(
                                                          padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.012),
                                                          child: Image.asset(
                                                            "images/new-success.png",
                                                            width: MediaQuery.of(context).size.width * 0.3,
                                                            height: MediaQuery.of(context).size.height * 0.12,
                                                          ),
                                                        ),
                                                        SizedBox(
                                                          width: MediaQuery.of(context).size.width * 0.05,
                                                        ),
                                                        InkWell(
                                                          onTap: () {
                                                            Navigator.pushReplacement(context, MaterialPageRoute(builder: (_) => Signin()));
                                                          },
                                                          child: Icon(
                                                            Icons.close,
                                                            color: const Color(0XFF5F5F5F),
                                                            size: MediaQuery.of(context).size.width * 0.1,
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                    Text(
                                                      "Success !",
                                                      style: GoogleFonts.poppins(
                                                        fontWeight: FontWeight.w700,
                                                        fontSize: MediaQuery.of(context).size.height * 0.022,
                                                        color: Colors.black,
                                                      ),
                                                    ),
                                                    SizedBox(
                                                      height: MediaQuery.of(context).size.height * 0.02,
                                                    ),
                                                    Text(
                                                      "You have successfully changed your password.",
                                                      textAlign: TextAlign.center,
                                                      style: GoogleFonts.poppins(fontWeight: FontWeight.w400, color: const Color(0XFF162746), height: 1.1, fontSize: MediaQuery.of(context).size.height * 0.019),
                                                    ),
                                                    const Spacer(),
                                                    SizedBox(
                                                      width: MediaQuery.of(context).size.width * 0.5,
                                                      height: MediaQuery.of(context).size.height * 0.057,
                                                      child: ElevatedButton(
                                                        child: Text("Great !",
                                                            style: GoogleFonts.poppins(
                                                              color: Colors.white,
                                                              fontSize: MediaQuery.of(context).size.height * 0.0185,
                                                              fontWeight: FontWeight.w600,
                                                            )),
                                                        style: ButtonStyle(
                                                          backgroundColor: MaterialStateProperty.all(
                                                            const Color(0XFF4048FF),
                                                          ),
                                                          shape: MaterialStateProperty.all(
                                                            RoundedRectangleBorder(
                                                              borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.08),
                                                            ),
                                                          ),
                                                        ),
                                                        onPressed: () {
                                                          Navigator.pushReplacement(context, MaterialPageRoute(builder: (_) => Signin()));
                                                        },
                                                      ),
                                                    )
                                                  ],
                                                ),
                                              ));
                                        });
                                  } else {
                                    Fluttertoast.showToast(msg: "Something went wrong, Try again !");
                                  }
                                }
                              : null,
                          child: Text(
                            "Reset my Password",
                            style: GoogleFonts.poppins(
                              color: isconfirmed && validForm ? Colors.white : const Color(0xffADADC8),
                              fontWeight: FontWeight.w600,
                              fontSize: MediaQuery.of(context).size.height * 0.018,
                            ),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          )),
    );
  }
}
