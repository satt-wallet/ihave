import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_file_dialog/flutter_file_dialog.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:ihave/ui/confirmAccountDesactivation.dart';
import 'package:ihave/ui/signin.dart';
import 'package:ihave/ui/successWallet.dart';
import 'package:ihave/widgets/alertDialogWidget.dart';
import 'package:pinput/pin_put/pin_put.dart';
import 'package:provider/provider.dart';
import 'package:path_provider/path_provider.dart';
import '../controllers/LoginController.dart';
import '../util/utils.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';

class BlockchainKey extends StatefulWidget {
  const BlockchainKey({Key? key}) : super(key: key);

  @override
  _BlockchainKeyState createState() => _BlockchainKeyState();
}

class _BlockchainKeyState extends State<BlockchainKey> {
  TextEditingController _transactionPassword = TextEditingController();
  final FocusNode _pinPutFocusNode = FocusNode();
  final TextEditingController _pinPutController = TextEditingController();
  bool isPinPutEdited = false;
  bool isPinPutfocused = false;
  bool _isCorrectCode = false;
  bool isConfirmPasswordEdited = false;
  bool isActivatedButton = false;
  bool isConfirmedPassword = false;
  bool _obscureText = true;
  bool _emptyPasswordField = true;
  File? _blockchainKey;
  bool _isLoading = false;
  String errorMessage = "";
  bool successCode = false;
  bool errorCode = false;

  BoxDecoration get _pinPutDecoration {
    return BoxDecoration(
      color: Colors.white,
      borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.033),
      border: Border.all(
        color: Colors.white,
      ),
    );
  }

  final _formKeyGlobal = GlobalKey<FormState>();

  Future<String> get directoryPath async {
    Directory? directory = await getTemporaryDirectory();
    return directory.path;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
            colors: [
              const Color(0xFFF52079),
              const Color(0xFF1F2337),
            ],
            begin: const FractionalOffset(0.0, -0.1),
            end: const FractionalOffset(0.0, 0.3),
            stops: [0.0, 1.0],
            tileMode: TileMode.clamp),
      ),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: SingleChildScrollView(
          child: SafeArea(
            child: Column(
              children: <Widget>[
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.02,
                ),
                Stack(
                  children: [
                    Align(
                      alignment: Alignment.topCenter,
                      child: SvgPicture.asset(
                        'images/logo-auth.svg',
                        alignment: Alignment.center,
                        width: MediaQuery.of(context).size.height * 0.035,
                        height: MediaQuery.of(context).size.height * 0.035,
                      ),
                    ),
                    Align(
                      alignment: Alignment.topRight,
                      child: Padding(
                        padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.02, top: MediaQuery.of(context).size.height * 0.0075),
                        child: InkWell(
                          child: SvgPicture.asset(
                            'images/new-logout.svg',
                          ),
                          onTap: () {
                            Provider.of<LoginController>(context, listen: false).logout();
                            gotoPageGlobal(context, Signin());
                          },
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.038,
                ),
                Padding(
                  padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.1, right: MediaQuery.of(context).size.width * 0.1),
                  child: Text(
                    "Let's download the JSON File",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(fontWeight: FontWeight.w700, color: Colors.white, fontSize: MediaQuery.of(context).size.height * 0.032, fontStyle: FontStyle.normal, letterSpacing: 0.3, height: 1.2),
                  ),
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.075,
                ),
                Padding(
                  padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.085, right: MediaQuery.of(context).size.width * 0.085),
                  child: Text(
                    "Enter your transaction password below then download your backup file. Just remember, you cannot reset your password.",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(color: Colors.white, fontWeight: FontWeight.w500, letterSpacing: 0.4, fontStyle: FontStyle.normal, fontSize: MediaQuery.of(context).size.height * 0.018, height: 1.2),
                  ),
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.2,
                ),
                Container(
                    width: MediaQuery.of(context).size.width * 0.8,
                    child: Padding(
                      padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.015),
                      child: Container(
                        decoration: BoxDecoration(
                          color: Color(0xFFF6F6FE),
                          border: Border.all(
                            color: Colors.transparent,
                          ),
                          borderRadius: BorderRadius.all(Radius.circular(20)),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Center(
                                child: Padding(
                                  padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height * 0.007),
                                  child: Text(
                                    "Authentication code",
                                    style: GoogleFonts.poppins(
                                      fontSize: 18,
                                      fontWeight: FontWeight.w600,
                                      color: (isPinPutEdited) ? ((_isCorrectCode) ? Color(0xFF00CC9E) : Color(0xFFF52079)) : Color(0xFF4048FF),
                                    ),
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.fromLTRB(15, 5, 15, 5),
                                child: Form(
                                  key: _formKeyGlobal,
                                  child: PinPut(
                                      controller: _pinPutController,
                                      fieldsCount: 6,
                                      focusNode: _pinPutFocusNode,
                                      onChanged: (content) async {
                                        isPinPutfocused = true;
                                        if (_pinPutController.text.length == 6) {
                                          isPinPutEdited = true;
                                          var codeConfirmed = await LoginController.apiService.verifyQrCode(_pinPutController.text, Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "");
                                          isConfirmPasswordEdited = true;
                                          isConfirmedPassword = codeConfirmed;
                                          if (isConfirmedPassword) {
                                            setState(() {
                                              _isCorrectCode = true;
                                              isActivatedButton = true;
                                              errorCode == false;
                                            });
                                          } else {
                                            setState(() {
                                              _isCorrectCode = false;
                                              isActivatedButton = false;
                                              errorCode = true;
                                            });
                                          }
                                        }
                                      },
                                      textStyle: TextStyle(
                                        fontSize: MediaQuery.of(context).size.width * 0.05,
                                        fontWeight: FontWeight.bold,
                                        color: (isPinPutEdited) ? ((_isCorrectCode) ? Color(0xFF00CC9E) : Color(0xFFF52079)) : Color(0xFF4048FF),
                                      ),
                                      eachFieldHeight: MediaQuery.of(context).size.width * 0.1,
                                      eachFieldWidth: MediaQuery.of(context).size.width * 0.1,
                                      onSubmit: (String pin) => _showSnackBar(pin, context),
                                      submittedFieldDecoration: _pinPutDecoration.copyWith(
                                          border: Border.all(
                                            color: isPinPutfocused ? ((isPinPutEdited) ? ((_isCorrectCode) ? Color(0xFF00CC9E) : Color(0xFFF52079)) : Color(0xFF4048FF)) : Color(0xFF4048FF),
                                          ),
                                          borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.042)),
                                      selectedFieldDecoration: _pinPutDecoration.copyWith(
                                          color: Colors.white,
                                          borderRadius: BorderRadius.circular(15.0),
                                          border: Border.all(
                                            color: isPinPutfocused ? ((isPinPutEdited) ? ((_isCorrectCode) ? Color(0xFF00CC9E) : Color(0xFFF52079)) : Color(0xFF4048FF)) : Color(0xFF4048FF),
                                          )),
                                      followingFieldDecoration: _pinPutDecoration.copyWith(
                                        borderRadius: BorderRadius.circular(5.0),
                                        border: Border.all(
                                          color: isPinPutfocused ? ((isPinPutEdited) ? ((_isCorrectCode) ? Color(0xFF00CC9E) : Color(0xFFF52079)) : Color(0xFF4048FF)) : Colors.grey,
                                        ),
                                      )),
                                ),
                              ),
                              Center(
                                child: Padding(
                                  padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height * 0.007),
                                  child: Text(
                                    isPinPutEdited ? (_isCorrectCode ? "Code Match" : "Incorrect Code") : "This code will only be valid for 5 min",
                                    style: GoogleFonts.poppins(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w700,
                                      color: isPinPutEdited ? (_isCorrectCode ? Color(0xFF00CC9E) : Color(0xFFF52079)) : Color(0xFF75758F),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    )),
                Padding(
                  padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.01),
                  child: errorMessage.length > 0
                      ? Text(
                          errorMessage,
                          style: GoogleFonts.poppins(
                            color: Colors.red,
                            fontWeight: FontWeight.w600,
                            fontSize: MediaQuery.of(context).size.height * 0.017,
                            fontStyle: FontStyle.normal,
                          ),
                        )
                      : null,
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.02,
                ),
                SizedBox(
                  width: MediaQuery.of(context).size.width * 0.8,
                  height: MediaQuery.of(context).size.height * 0.06,
                  child: ElevatedButton(
                    onPressed: !_emptyPasswordField && !_isLoading
                        ? () async {
                            setState(() {
                              errorMessage = "";
                              _isLoading = true;
                            });
                            var result = await LoginController.apiService.getBlockchainKey(Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "", _transactionPassword.text);
                            if (result == "wallet not found") {
                              Fluttertoast.showToast(msg: "Wallet not found");
                              setState(() {
                                errorMessage = "";
                                _isLoading = false;
                                _transactionPassword.text = "";
                                _emptyPasswordField = true;
                              });
                            } else if (result == "wrong password") {
                              setState(() {
                                errorMessage = "Wrong password";
                                _isLoading = false;
                                _transactionPassword.text = "";
                                _emptyPasswordField = true;
                              });
                            } else if (result == "error") {
                              setState(() {
                                errorMessage = "";
                                _isLoading = false;
                                _transactionPassword.text = "";
                                _emptyPasswordField = true;
                              });
                              displayDialog(context, "Error", "Something went wrong, please try again!");
                            } else {
                              final path = await directoryPath;
                              _blockchainKey = File('$path/blockchain-key.json');
                              await _blockchainKey?.writeAsString(result);
                              final params = SaveFileDialogParams(sourceFilePath: _blockchainKey!.path);
                              final filePath = await FlutterFileDialog.saveFile(params: params);
                              if (filePath != null) {
                                setState(() {
                                  errorMessage = "";
                                  _isLoading = false;
                                });
                                Fluttertoast.showToast(msg: "you have successfully downloaded your blockchain key");
                                if (Provider.of<LoginController>(context, listen: false).deactivationReason != "") {
                                  gotoPageGlobal(context, ConfirmAccountDesactivation());
                                } else {
                                  gotoPageGlobal(context, SuccessWallet());
                                }
                              } else {
                                Fluttertoast.showToast(msg: "you have aborted the download");
                                setState(() {
                                  errorMessage = "";
                                  _isLoading = false;
                                  _transactionPassword.text = "";
                                  _emptyPasswordField = true;
                                });
                              }
                            }
                          }
                        : null,
                    style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all(
                        _emptyPasswordField ? Color(0XFFF6F6FF) : (_isLoading ? Color(0XFFF6F6FF) : Color(0xFF00CC9E)),
                      ),
                      shape: MaterialStateProperty.all(
                        RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.07),
                        ),
                      ),
                    ),
                    child: !_isLoading
                        ? Text(
                            "Confirm to download",
                            style: GoogleFonts.poppins(color: _transactionPassword.text.length == 0 ? const Color(0XFFADADC8) : Colors.white, fontWeight: FontWeight.w700, fontSize: MediaQuery.of(context).size.height * 0.02),
                          )
                        : SizedBox(
                            height: MediaQuery.of(context).size.height * 0.022,
                            width: MediaQuery.of(context).size.height * 0.022,
                            child: CircularProgressIndicator(
                              color: Colors.grey,
                            ),
                          ),
                  ),
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.15,
                ),
                Padding(
                  padding: EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom.toString() != "0.0" ? MediaQuery.of(context).size.height * 0.3 : 0),
                  child: InkWell(
                    splashColor: Colors.transparent,
                    hoverColor: Colors.transparent,
                    highlightColor: Colors.transparent,
                    onTap: () {
                      if (Provider.of<LoginController>(context, listen: false).deactivationReason != "") {
                        gotoPageGlobal(context, ConfirmAccountDesactivation());
                      } else {
                        gotoPageGlobal(context, SuccessWallet());
                      }
                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          "Download later   ",
                          style: GoogleFonts.poppins(fontWeight: FontWeight.w500, fontStyle: FontStyle.normal, color: Colors.white, letterSpacing: 0.7, fontSize: MediaQuery.of(context).size.height * 0.018),
                        ),
                        Text(
                          ">",
                          style: GoogleFonts.poppins(fontWeight: FontWeight.w200, fontStyle: FontStyle.normal, color: Colors.white, letterSpacing: 0.7, fontSize: MediaQuery.of(context).size.height * 0.03),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

void _showSnackBar(String pin, BuildContext context) {
  final snackBar = SnackBar(
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(18.0),
    ),
    duration: Duration(seconds: 5),
    content: Container(
        height: 80.0,
        color: Colors.grey,
        child: Center(
          child: Text(
            'Pin Submitted. Value: $pin',
            style: TextStyle(fontSize: 25.0, backgroundColor: Colors.grey),
          ),
        )),
    backgroundColor: Colors.grey,
  );
}