import 'dart:async';
import 'dart:math';
import 'dart:io' show Platform;
import 'package:big_decimal/big_decimal.dart';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_svg/svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hive/hive.dart';
import 'package:ihave/controllers/LoginController.dart';
import 'package:ihave/controllers/walletController.dart';
import 'package:ihave/main.dart';
import 'package:ihave/model/crypto.dart';
import 'package:ihave/service/apiService.dart';
import 'package:ihave/ui/WelcomeScreen.dart';
import 'package:ihave/ui/kyc.dart';
import 'package:ihave/ui/staticElements/headerNavigation.dart';
import 'package:ihave/util/Sqflite.dart';
import 'package:ihave/util/utils.dart';
import 'package:ihave/widgets/alertDialogWidget.dart';
import 'package:one_context/one_context.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:permission_handler_platform_interface/permission_handler_platform_interface.dart';
import 'package:provider/provider.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:animated_text_kit/animated_text_kit.dart';
import 'dart:math' as math;

import 'package:url_launcher/url_launcher.dart';

import '../model/contact.dart';
import '../util/contactsHive.dart';
import '../util/qrCodeScan.dart';

late Box box;
late List<Map<dynamic, dynamic>> boxMap;
List<Contact> contactsList = [];
List<Contact> surFix = [];
TextEditingController _cryptoBalance = TextEditingController();
TextEditingController _dollarBalance = TextEditingController();
TextEditingController _walletAddress = TextEditingController();

TextEditingController _transactionPwd = TextEditingController();

class Send extends StatefulWidget {
  Send({Key? key}) : super(key: key);

  @override
  _SendState createState() => _SendState();
}

class _SendState extends State<Send> {
  TextEditingController _WalletPassword = TextEditingController();

  bool _loading = false;
  FocusNode cryptoFocusNode = FocusNode();
  FocusNode dollarFocusNode = FocusNode();
  var balance;
  String cryptoBalance = "";
  String dollarBalance = "";
  String walletAddress = "";
  String transactionPwd = "";
  String _selectedCrypto = "";
  bool _balanceExist = true;
  String _smartContract = "";
  String _selectedNetwork = "ERC20";
  List<Crypto> _cryptoList = <Crypto>[];
  List<String> networkList = ["ERC20", "BEP20", "BTC"];
  APIService apiService = LoginController.apiService;
  final _formKey = GlobalKey<FormState>();
  final _formKeyPassword = GlobalKey<FormState>();
  Crypto? cryptoSelectedBalance;
  String cryptoBalanceSelect = "";
  String? transactionHash;
  String errorMessage = "";
  bool _firstStepSendFlow = true;
  bool _secondStepSendFlow = false;
  bool _successSend = false;
  bool _isLoadingSend = false;
  bool _isActivatedButtonSend = true;
  var dropdownValue = 1;
  bool _isLoadingNetwork = false;
  double gasEth = 0;
  double gasBnb = 0;
  String gasEthApprox = "";
  String gasCryptoBallance = "";
  bool _cryptoField = false;
  bool _dollarField = false;
  bool _walletField = false;
  bool _cryptoPattern = false;
  bool _dollarPattern = false;
  bool _walletPattern = false;
  bool _walletFieldRequired = false;
  bool _transactionPasswordField = false;
  String? _messageTransaction;
  bool _messageTransactionExist = false;
  bool _errorMessageTransaction = false;
  String selectedToken = "";
  final GlobalKey _qrKey = GlobalKey();
  bool _isSelected = false;
  String firebaseToken = "";
  bool _obscureText = true;
  String cryptoBalanceUserInput = "";
  bool _clickedContainerPaymentMethod = false;
  String _selectedPaymentMethod = "erc20";
  bool _clickedCryptoContainer = false;
  bool _selectedCryptoAddedToken = false;
  String _selectedCryptoPicUrl = "";
  String _selectedCryptoName = "";
  String _selectedCryptoSymbol = "";
  bool _errorSend = false;
  double priceETH = 0;
  double feesERC20 = 0;
  double feesETHQuantity = 0;
  double priceBNB = 0;
  double priceMATIC = 0;
  double priceBTT = 0;
  double feesBTT = 0;
  double feesBTTQuantity = 0;
  double priceTRX = 0;
  double feesTRX = 0;
  double feesTRXQuantity = 0;
  double feesMATIC = 0;
  double feesMATICQuantity = 0;
  double feesBEP20 = 0;
  double feesBNBQuantity = 0;
  bool _enoughFees = true;
  bool _feesLoading = false;
  bool _errorSendProcess = false;
  bool _feesAwait = true;
  ThemeData scrolltheme = ThemeData(
      scrollbarTheme: ScrollbarThemeData(
    isAlwaysShown: true,
    thickness: MaterialStateProperty.all(3),
    thumbColor: MaterialStateProperty.all(Color(0xFF75758F)),
    radius: const Radius.circular(30),
  ));

  String? qrresult;
  bool _isQrCodeVide = false;

  bool listElementClicked = false;

  Future<List<Crypto>> fetchCrypto() async {
    if (Provider.of<WalletController>(context, listen: false).cryptos.length < 1) {
      var cryptoList = await apiService.cryptos1(Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "", version);
      Provider.of<WalletController>(context, listen: false).cryptos = cryptoList;
    }
    return Provider.of<WalletController>(context, listen: false).cryptos;
  }

  fillContactList() {
    setState(() {
      contactsList.clear();
      boxMap.forEach((element) {
        contactsList.add(Contact.fromJson(element));
      });
      surFix.clear();
      surFix.addAll(contactsList);
    });
  }

  Future feesETH() async {
    var price = await LoginController.apiService.getCryptoPrice(Provider.of<LoginController>(context, listen: false).userDetails!.userToken ?? "", "ETH");
    if (price != null && price != "error") {
      setState(() {
        priceETH = price;
      });
      var gasPrice = await LoginController.apiService.getGasErc20(Provider.of<LoginController>(context, listen: false).userDetails!.userToken ?? "");
      if (gasPrice != "error" && gasPrice != null) {
        setState(() {
          feesERC20 = ((gasPrice * 60000) / 1000000000) * priceETH;
          feesETHQuantity = feesERC20 / priceETH;
        });
      }
    }
  }

  Future feesPolyGon() async {
    var price = await LoginController.apiService.getCryptoPrice(Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "", "MATIC");
    if (price != null && price != "error") {
      setState(() {
        priceMATIC = price;
      });
      var gasPrice = await LoginController.apiService.getPolygonGasPrice(Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "");
      if (gasPrice != "error" && gasPrice != null) {
        setState(() {
          feesMATIC = ((gasPrice * 60000) / 1000000000) * priceMATIC;
          feesMATICQuantity = feesMATIC / priceMATIC;
        });
      } else {}
    }
  }

  Future feesBitTorrent() async {
    var price = await LoginController.apiService.getCryptoPrice(Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "", "BTT");
    if (price != null && price != "error") {
      String tokenPrice = price.toStringAsFixed(8);
      setState(() {
        priceBTT = double.parse(tokenPrice);
      });
      var gasPrice = await LoginController.apiService.getBitTorentGasPrice(Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "");
      if (gasPrice != "error" && gasPrice != null) {
        setState(() {
          feesBTT = ((gasPrice * 60000) / 1000000000) * priceBTT;
          feesBTTQuantity = feesBTT / priceBTT;
        });
      } else {}
    }
  }

  Future feesTrx() async {
    var price = await LoginController.apiService.getCryptoPrice(Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "", "TRX");

    if (price != null && price != "error") {
      String tokenPrice = price.toStringAsFixed(8);
      setState(() {
        priceTRX = double.parse(tokenPrice);
      });
      var gasPrice = await LoginController.apiService.getTrxGasFees();
      if (gasPrice != "error" && gasPrice != null) {
        setState(() {
          feesTRXQuantity = 15.7004932 / gasPrice;
          feesTRX = feesTRXQuantity * priceTRX;
        });
      }
    }
  }

  Future feesBNB() async {
    var price = await LoginController.apiService.getCryptoPrice(Provider.of<LoginController>(context, listen: false).userDetails!.userToken ?? "", "BNB");
    if (price != null && price != "error") {
      setState(() {
        priceBNB = price;
      });
      var gasPrice = await LoginController.apiService.getGasBep20(Provider.of<LoginController>(context, listen: false).userDetails!.userToken ?? "");
      if (gasPrice != "error" && gasPrice != null) {
        setState(() {
          feesBEP20 = ((gasPrice * 60000) / 1000000000) * priceBNB;
          feesBNBQuantity = feesBEP20 / priceBNB;
        });
      }
    }
  }

  Future getGasPriceEth() async {
    if (apiService.ethGasPrice == null || apiService.ethGasPrice == 0) {
      await apiService.getGasEth(Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "");
    } else {
      return;
    }
  }

  Future getGasPriceBnb() async {
    if (apiService.bnbGasPrice == null || apiService.bnbGasPrice == 0) {
      await apiService.getGasBnb(Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "");
    } else {
      return;
    }
  }

  Future<void> scanQr() async {
    Navigator.pushNamed(context, '/qrcode').then((value) {
      setState(() {
        value = value ?? "";
        qrresult = value.toString();
        _walletAddress.text = qrresult ?? "";
        if (_selectedPaymentMethod == "tron") {
          if (RegExp("T[A-Za-z1-9]{33}\$").hasMatch(_walletAddress.text)) {
            setState(() {
              _walletField = true;
              _walletPattern = false;
              _walletFieldRequired = false;
            });
          } else {
            setState(() {
              _walletField = false;
              _walletPattern = true;
              _walletFieldRequired = false;
            });
          }
        } else {
          if (RegExp(r"^0x[a-fA-F0-9]{40}$|^(bc1|[13])[a-zA-HJ-NP-Z0-9]{25,39}$").hasMatch(_walletAddress.text)) {
            setState(() {
              _walletField = true;
              _walletPattern = false;
              _walletFieldRequired = false;
            });
          } else {
            setState(() {
              _walletField = false;
              _walletPattern = true;
              _walletFieldRequired = false;
            });
          }
        }
        if (qrresult.toString() == "-1") {
          setState(() {
            _walletAddress.text = "";
          });
        } else if (qrresult.toString() == "") {
          setState(() {
            _walletAddress.text = "";
            _walletPattern = false;
          });
        }
      });
      if (_walletAddress.text != "") ;
      searchbook(_walletAddress.text);
    }).catchError((error) => print(error));
  }

  createTronWalletUI(BuildContext context) {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return StatefulBuilder(builder: (context, setState) {
            return Scaffold(
              resizeToAvoidBottomInset: true,
              body: Dialog(
                backgroundColor: Colors.transparent,
                insetPadding: EdgeInsets.zero,
                child: SingleChildScrollView(
                  child: Container(
                    height: MediaQuery.of(context).size.height,
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      color: Colors.white.withOpacity(0.9),
                    ),
                    child: Column(
                      children: <Widget>[
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Spacer(),
                            Spacer(),
                            Padding(
                              padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.01, top: MediaQuery.of(context).size.width * 0.06),
                              child: IconButton(
                                alignment: Alignment.topLeft,
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                                splashColor: Colors.transparent,
                                highlightColor: Colors.transparent,
                                icon: Icon(
                                  Icons.close,
                                  size: MediaQuery.of(context).size.width * 0.05,
                                  color: Colors.black.withOpacity(0.5),
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: MediaQuery.of(context).size.height * 0.035,
                        ),
                        Image.asset(
                          "images/tron-wallet.png",
                        ),
                        Text(
                          "Create your Tron wallet",
                          style: GoogleFonts.poppins(fontWeight: FontWeight.w700, fontSize: MediaQuery.of(context).size.height * 0.025, color: Colors.black),
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.075, right: MediaQuery.of(context).size.width * 0.075, top: MediaQuery.of(context).size.height * 0.01),
                          child: Text(
                            "You are about to delete this token. You now have the possibility to create your Tron ​​wallet.You Need to just enter your transactional password to create it ",
                            textAlign: TextAlign.center,
                            style: GoogleFonts.poppins(fontWeight: FontWeight.w400, fontSize: MediaQuery.of(context).size.height * 0.0175, color: const Color(0XFF162746), letterSpacing: 1.1),
                          ),
                        ),
                        Align(
                          alignment: Alignment.topLeft,
                          child: Padding(
                            padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.1, top: MediaQuery.of(context).size.height * 0.035),
                            child: Text(
                              "TRANSACTION PASSWORD",
                              style: GoogleFonts.poppins(fontWeight: FontWeight.w600, color: const Color(0XFF75758F)),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: MediaQuery.of(context).size.height * 0.01,
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width * 0.8,
                          child: TextFormField(
                            style: TextStyle(fontSize: MediaQuery.of(context).size.width * 0.045),
                            textAlignVertical: TextAlignVertical.center,
                            textAlign: TextAlign.left,
                            controller: _WalletPassword,
                            keyboardType: TextInputType.visiblePassword,
                            obscureText: _obscureText,
                            onChanged: (value) {
                              TextSelection previousSelection = _WalletPassword.selection;
                              _WalletPassword.text = value;
                              _WalletPassword.selection = previousSelection;

                              TextSelection.fromPosition(TextPosition(offset: _WalletPassword.text.length));
                            },
                            validator: (value) {
                              if (value!.isEmpty) {
                                return 'Field required';
                              }
                              return null;
                            },
                            decoration: InputDecoration(
                                fillColor: Colors.white,
                                focusColor: Colors.white,
                                hoverColor: Colors.white,
                                filled: true,
                                isDense: true,
                                prefixIcon: Padding(
                                  padding: EdgeInsets.all(MediaQuery.of(context).size.width * 0.017),
                                  child: SvgPicture.asset(
                                    "images/keyIcon.svg",
                                    color: Colors.orange,
                                    height: MediaQuery.of(context).size.width * 0.06,
                                    width: MediaQuery.of(context).size.width * 0.06,
                                  ),
                                ),
                                suffixIcon: Padding(
                                  padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.025),
                                  child: IconButton(
                                      onPressed: () {
                                        setState(() {
                                          _obscureText = !_obscureText;
                                        });
                                      },
                                      icon: _obscureText
                                          ? SvgPicture.asset(
                                              "images/visibility-icon-off.svg",
                                              height: MediaQuery.of(context).size.height * 0.02,
                                            )
                                          : SvgPicture.asset(
                                              "images/visibility-icon-on.svg",
                                              height: MediaQuery.of(context).size.height * 0.02,
                                            )),
                                ),
                                contentPadding: EdgeInsets.symmetric(vertical: 5, horizontal: 5),
                                enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.white)),
                                focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.white)),
                                errorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red)),
                                focusedErrorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red))),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.02),
                          child: SizedBox(
                            width: MediaQuery.of(context).size.width * 0.6,
                            child: ElevatedButton(
                              child: Padding(
                                padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.015, bottom: MediaQuery.of(context).size.height * 0.015),
                                child: _loading
                                    ? CircularProgressIndicator(
                                        color: Colors.white,
                                      )
                                    : Text(
                                        "Create Wallet",
                                        style: GoogleFonts.poppins(fontWeight: FontWeight.w700, fontSize: MediaQuery.of(context).size.height * 0.017),
                                      ),
                              ),
                              onPressed: !_loading
                                  ? () async {
                                      setState(() {
                                        _loading = true;
                                      });
                                      var result = await LoginController.apiService.createTronWallet(Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "", _WalletPassword.text);
                                      switch (result) {
                                        case "success":
                                          Navigator.of(context).pop();

                                          Fluttertoast.showToast(msg: "Wallet tron created successfully", backgroundColor: Color.fromRGBO(0, 151, 117, 1), toastLength: Toast.LENGTH_LONG);
                                          break;
                                        case "exist":
                                          Navigator.of(context).pop();

                                          Fluttertoast.showToast(msg: "You have a tron wallet", backgroundColor: Color(0xFFF52079), toastLength: Toast.LENGTH_LONG);
                                          break;
                                        case "wrong password":
                                          setState(() {
                                            _WalletPassword.text = "";
                                            _loading = false;
                                          });
                                          Fluttertoast.showToast(msg: "Wrong password", backgroundColor: Color(0xFFF52079), toastLength: Toast.LENGTH_LONG);
                                          break;
                                        case "error":
                                          Navigator.of(context).pop();

                                          Fluttertoast.showToast(msg: "Something went wrong please try again", backgroundColor: Color(0xFFF52079), toastLength: Toast.LENGTH_LONG);
                                          break;
                                      }
                                    }
                                  : null,
                              style: ButtonStyle(
                                backgroundColor: MaterialStateProperty.all(
                                  Color(0xFF4048FF),
                                ),
                                shape: MaterialStateProperty.all(
                                  RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.08),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.012),
                          child: SizedBox(
                            width: MediaQuery.of(context).size.width * 0.6,
                            child: ElevatedButton(
                              child: Padding(
                                padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.015, bottom: MediaQuery.of(context).size.height * 0.015),
                                child: Text(
                                  "Skip for now",
                                  style: GoogleFonts.poppins(fontWeight: FontWeight.w700, fontSize: MediaQuery.of(context).size.height * 0.017, color: const Color(0xFF4048FF)),
                                ),
                              ),
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                              style: ButtonStyle(
                                backgroundColor: MaterialStateProperty.all(
                                  Colors.white,
                                ),
                                shape: MaterialStateProperty.all(
                                  RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.08),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            );
          });
        });
  }

  bool _isLoading = true;

  void _refreshContacts() async {
    final data = await SQLHelper.getItems();
    setState(() {
      boxMap = data;
      _isLoading = false;
    });
    fillContactList();
  }

  @override
  void initState() {
    _refreshContacts();
    Provider.of<LoginController>(context, listen: false).errorSendCrypto = false;
    Future.wait([feesETH(), feesBNB(), feesPolyGon(), feesBitTorrent(), feesTrx()]).then((value) => setState(() {
          _feesAwait = false;
        }));

    setState(() {
      _errorMessageTransaction = false;
      _messageTransactionExist = false;
      _cryptoField = false;
      _dollarField = false;
      _walletField = false;
      _cryptoPattern = false;
      _dollarPattern = false;
      _walletPattern = false;
      _walletFieldRequired = false;
      _transactionPasswordField = false;
      _firstStepSendFlow = true;
      listElementClicked = false;
      _secondStepSendFlow = false;
      _successSend = false;
      _cryptoBalance.text = "";
      _dollarBalance.text = "";
      _transactionPwd.text = "";
      _walletAddress.text = "";
      dropdownValue = 1;
    });
    getGasPriceEth().then((value) {
      setState(() {
        gasEth = apiService.ethGasPrice ?? 0;
      });
    });
    getGasPriceBnb().then((value) {
      setState(() {
        gasBnb = apiService.bnbGasPrice ?? 0;
      });
    });
    cryptoFocusNode.addListener(() {
      _cryptoBalance.text = formatCryptoBalance(double.parse(_cryptoBalance.text.replaceAll(new RegExp("[^\\d.]"), "")), "");
      _dollarBalance.text = formatDollarBalance(double.parse(_dollarBalance.text.replaceAll(new RegExp("[^\\d.]"), "")));
    });
    dollarFocusNode.addListener(() {
      _cryptoBalance.text = formatCryptoBalance(double.parse(_cryptoBalance.text.replaceAll(new RegExp("[^\\d.]"), "")), "");
      _dollarBalance.text = formatDollarBalance(double.parse(_dollarBalance.text.replaceAll(new RegExp("[^\\d.]"), "")));
    });
    balance = apiService.balance;
    if (Provider.of<LoginController>(context, listen: false).tokenNetwork == "") {
      fetchCrypto().then((value) {
        setState(() {
          _cryptoList.clear();
          _cryptoList.addAll(value.where((c) => c.network == _selectedNetwork).toList());
          _selectedCrypto = _cryptoList[0].symbol.toString();
          _clickedCryptoContainer = false;
          setCryptoSelection(_cryptoList[0]);
          ;
        });
      });
    } else {
      _selectedNetwork = Provider.of<LoginController>(context, listen: false).tokenNetwork;
      _selectedPaymentMethod = _selectedNetwork == "BTC" ? "fiat" : (_selectedNetwork == "BTTC" ? "btt" : _selectedNetwork.toLowerCase());
      fetchCrypto().then((value) {
        setState(() {
          _cryptoList.clear();
          _cryptoList.addAll(value.where((c) => c.network == _selectedNetwork).toList());
          Crypto crypto = _cryptoList.firstWhere((element) => element.symbol == Provider.of<LoginController>(context, listen: false).tokenSymbol);

          _selectedCrypto = Provider.of<LoginController>(context, listen: false).tokenSymbol;
          setCryptoSelection(crypto);
        });
      });
    }
  }

  Future<void> _refresh() {
    return Future.delayed(Duration(seconds: 1));
  }

  @override
  Widget build(BuildContext context) {
    if (_cryptoList.isNotEmpty) {
      return KeyboardVisibilityBuilder(builder: (context, visible) {
        return WillPopScope(
          onWillPop: () {
            gotoPageGlobal(context, WelcomeScreen());
            return Future.value(false);
          },
          child: Container(
            child: GestureDetector(
              onTap: () {
                FocusScope.of(context).requestFocus(new FocusNode());
              },
              child: _firstStepSendFlow
                  ? (MediaQuery.of(context).viewInsets.bottom.toString() == "0.0"
                      ? Stack(
                          children: [firstStepFlowWidget()],
                        )
                      : Stack(
                          children: [SingleChildScrollView(child: Container(height: MediaQuery.of(context).size.height, child: firstStepFlowWidget()))],
                        ))
                  : (_secondStepSendFlow
                      ? (MediaQuery.of(context).viewInsets.bottom.toString() == "0.0"
                          ? Stack(
                              children: [
                                secondStepFlowWidget(),
                              ],
                            )
                          : Stack(
                              children: [
                                SingleChildScrollView(
                                  child: Container(
                                    height: MediaQuery.of(context).size.height,
                                    child: secondStepFlowWidget(),
                                  ),
                                ),
                              ],
                            ))
                      : (_successSend
                          ? Stack(
                              children: <Widget>[
                                Column(
                                  children: <Widget>[
                                    Container(
                                      height: MediaQuery.of(context).size.height * 0.14,
                                      child: Row(
                                        crossAxisAlignment: CrossAxisAlignment.end,
                                        children: <Widget>[
                                          Padding(
                                            padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.05, 0, 0, MediaQuery.of(context).size.height * 0.032),
                                            child: Text(
                                              "Send",
                                              style: GoogleFonts.poppins(
                                                fontSize: MediaQuery.of(context).size.height * 0.035,
                                                fontWeight: FontWeight.w700,
                                                color: Colors.white,
                                              ),
                                            ),
                                          ),
                                          Spacer(),
                                          Padding(
                                            padding: EdgeInsets.fromLTRB(0, 0, MediaQuery.of(context).size.width * 0.037, MediaQuery.of(context).size.height * 0.021),
                                            child: InkWell(
                                              onTap: () {
                                                Provider.of<WalletController>(context, listen: false).selectedIndex = 2;
                                                Provider.of<WalletController>(context, listen: false).cryptos = [];
                                                Provider.of<LoginController>(context, listen: false).Balance = "";
                                                Navigator.pushReplacementNamed(context, '/welcomescreen');
                                              },
                                              child: Text(
                                                "< Back",
                                                style: GoogleFonts.poppins(color: Colors.white, letterSpacing: 1.2, fontWeight: FontWeight.w600, fontSize: MediaQuery.of(context).size.height * 0.022),
                                              ),
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                    Expanded(
                                      child: Container(
                                        width: MediaQuery.of(context).size.width,
                                        decoration: BoxDecoration(
                                            color: Colors.white,
                                            borderRadius: BorderRadius.only(
                                              topLeft: Radius.circular(MediaQuery.of(context).size.width * 0.08),
                                              topRight: Radius.circular(MediaQuery.of(context).size.width * 0.08),
                                            )),
                                        child: Column(
                                          children: <Widget>[
                                            Padding(
                                              padding: EdgeInsets.fromLTRB(0, MediaQuery.of(context).size.height * 0.02, 0, 0),
                                              child: SvgPicture.asset(
                                                "images/valid.svg",
                                                height: MediaQuery.of(context).size.height * 0.11,
                                                width: MediaQuery.of(context).size.height * 0.11,
                                              ),
                                            ),
                                            Padding(
                                              padding: EdgeInsets.fromLTRB(0, MediaQuery.of(context).size.height * 0.015, 0, 0),
                                              child: Text(
                                                "Transaction completed\nsuccessfully!",
                                                textAlign: TextAlign.center,
                                                style: GoogleFonts.poppins(
                                                  color: Color(0xFF4048FF),
                                                  fontSize: MediaQuery.of(context).size.height * 0.022,
                                                  fontWeight: FontWeight.w700,
                                                  height: 1.15,
                                                  letterSpacing: 1.7,
                                                ),
                                              ),
                                            ),
                                            Padding(
                                              padding: EdgeInsets.fromLTRB(0, MediaQuery.of(context).size.height * 0.03, 0, 0),
                                              child: Text(
                                                "\$${_dollarBalance.text.replaceAll("\$", "")} USD\nhas been sent",
                                                textAlign: TextAlign.center,
                                                style: GoogleFonts.poppins(
                                                  color: Color(0xFF1F2337),
                                                  fontSize: MediaQuery.of(context).size.height * 0.022,
                                                  fontWeight: FontWeight.w700,
                                                  height: 1.25,
                                                  letterSpacing: 1.5,
                                                ),
                                              ),
                                            ),
                                            SizedBox(height: MediaQuery.of(context).size.height * 0.025),
                                            Container(
                                                width: MediaQuery.of(context).size.width * 0.8,
                                                decoration: BoxDecoration(
                                                  borderRadius: BorderRadius.circular(30),
                                                  color: Color(0xFFF6F6FF),
                                                ),
                                                child: Center(
                                                    child: Column(
                                                  children: [
                                                    SizedBox(height: MediaQuery.of(context).size.height * 0.02),
                                                    Text(
                                                      "TRANSACTION HASH",
                                                      style: GoogleFonts.poppins(
                                                        color: const Color(0xFF323754),
                                                        fontWeight: FontWeight.w600,
                                                        fontSize: MediaQuery.of(context).size.height * 0.0145,
                                                        letterSpacing: 1.2,
                                                      ),
                                                    ),
                                                    SizedBox(height: MediaQuery.of(context).size.height * 0.01),
                                                    Row(
                                                      crossAxisAlignment: CrossAxisAlignment.start,
                                                      mainAxisAlignment: MainAxisAlignment.start,
                                                      children: <Widget>[
                                                        const Spacer(),
                                                        InkWell(
                                                          onTap: () {
                                                            _launchTxHashUrl(_selectedNetwork, transactionHash ?? "");
                                                          },
                                                          child: Text(
                                                            "${transactionHash!.substring(0, 11)}...${transactionHash!.substring(54)}",
                                                            style: GoogleFonts.poppins(
                                                              fontSize: MediaQuery.of(context).size.width * 0.04,
                                                              fontWeight: FontWeight.w400,
                                                              color: Color(0xFF4048FF),
                                                            ),
                                                          ),
                                                        ),
                                                        const Spacer(),
                                                        InkWell(
                                                          onTap: () {
                                                            Clipboard.setData(ClipboardData(text: transactionHash));
                                                            Fluttertoast.showToast(msg: "Copied to clipboard", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.BOTTOM, timeInSecForIosWeb: 1);
                                                          },
                                                          child: SvgPicture.asset("images/clipboard-icon.svg"),
                                                        ),
                                                        const Spacer(),
                                                      ],
                                                    ),
                                                    SizedBox(height: MediaQuery.of(context).size.height * 0.02),
                                                  ],
                                                ))),
                                            SizedBox(height: MediaQuery.of(context).size.height * 0.03),
                                            SizedBox(
                                              height: MediaQuery.of(context).size.height * 0.052,
                                              width: MediaQuery.of(context).size.width * 0.8,
                                              child: ElevatedButton(
                                                style: ButtonStyle(
                                                  backgroundColor: MaterialStateProperty.all(
                                                    const Color(0xFF4048FF),
                                                  ),
                                                  shape: MaterialStateProperty.all(
                                                    RoundedRectangleBorder(
                                                      borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.07),
                                                    ),
                                                  ),
                                                ),
                                                onPressed: () {
                                                  Provider.of<LoginController>(context, listen: false).Balance = "";
                                                  setState(() {
                                                    transactionHash = "";
                                                    _errorMessageTransaction = false;
                                                    _messageTransactionExist = false;
                                                    _cryptoField = false;
                                                    _dollarField = false;
                                                    _walletField = false;
                                                    _cryptoPattern = false;
                                                    _dollarPattern = false;
                                                    _walletPattern = false;
                                                    _walletFieldRequired = false;
                                                    _transactionPasswordField = false;
                                                    _firstStepSendFlow = true;
                                                    listElementClicked = false;
                                                    _secondStepSendFlow = false;
                                                    _successSend = false;
                                                    _messageTransaction = "";
                                                    _isLoadingSend = false;
                                                    _feesLoading = false;
                                                    _cryptoBalance.text = "";
                                                    _dollarBalance.text = "";
                                                    _transactionPwd.text = "";
                                                    _walletAddress.text = "";
                                                    _selectedNetwork = "BEP20";
                                                    dropdownValue = 1;
                                                    _isLoadingSend = false;
                                                    _feesLoading = false;
                                                  });

                                                  Navigator.pushReplacementNamed(context, '/welcomescreen');
                                                },
                                                child: Text(
                                                  "Send again",
                                                  style: GoogleFonts.poppins(
                                                    fontSize: MediaQuery.of(context).size.width * 0.04,
                                                    color: Colors.white,
                                                    fontWeight: FontWeight.w600,
                                                  ),
                                                ),
                                              ),
                                            ),
                                            SizedBox(height: MediaQuery.of(context).size.height * 0.015),
                                            SizedBox(
                                              height: MediaQuery.of(context).size.height * 0.052,
                                              width: MediaQuery.of(context).size.width * 0.8,
                                              child: ElevatedButton(
                                                style: ButtonStyle(
                                                  backgroundColor: MaterialStateProperty.all(
                                                    Color(0xFFFFFFFF),
                                                  ),
                                                  shape: MaterialStateProperty.all(
                                                    RoundedRectangleBorder(
                                                      borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.07),
                                                    ),
                                                  ),
                                                ),
                                                onPressed: () {
                                                  Provider.of<WalletController>(context, listen: false).cryptos = [];
                                                  Provider.of<LoginController>(context, listen: false).Balance = "";
                                                  Provider.of<WalletController>(context, listen: false).selectedIndex = 2;
                                                  Navigator.pushReplacementNamed(context, "/welcomescreen");
                                                },
                                                child: Text(
                                                  "Back to Wallet",
                                                  style: GoogleFonts.poppins(
                                                    fontSize: MediaQuery.of(context).size.width * 0.04,
                                                    color: Color(0xFF4048FF),
                                                    fontWeight: FontWeight.w600,
                                                  ),
                                                ),
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            )
                          : (_errorSendProcess
                              ? Stack(
                                  children: [
                                    Column(
                                      children: <Widget>[
                                        Container(
                                          height: MediaQuery.of(context).size.height * 0.14,
                                          child: Row(
                                            crossAxisAlignment: CrossAxisAlignment.end,
                                            children: <Widget>[
                                              Padding(
                                                padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.05, 0, 0, MediaQuery.of(context).size.height * 0.032),
                                                child: Text(
                                                  "Send",
                                                  style: GoogleFonts.poppins(
                                                    fontSize: MediaQuery.of(context).size.height * 0.035,
                                                    fontWeight: FontWeight.w700,
                                                    color: Colors.white,
                                                  ),
                                                ),
                                              ),
                                              Spacer(),
                                              Padding(
                                                padding: EdgeInsets.fromLTRB(0, 0, MediaQuery.of(context).size.width * 0.037, MediaQuery.of(context).size.height * 0.021),
                                                child: InkWell(
                                                  onTap: () {
                                                    Provider.of<WalletController>(context, listen: false).selectedIndex = 2;
                                                    Navigator.pushReplacementNamed(context, '/welcomescreen');
                                                  },
                                                  child: Text(
                                                    "< Back",
                                                    style: GoogleFonts.poppins(color: Colors.white, letterSpacing: 1.2, fontWeight: FontWeight.w600, fontSize: MediaQuery.of(context).size.height * 0.022),
                                                  ),
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                        Expanded(
                                          child: Container(
                                            height: MediaQuery.of(context).size.height,
                                            width: MediaQuery.of(context).size.width,
                                            decoration: BoxDecoration(
                                              color: Colors.white,
                                              borderRadius: BorderRadius.only(
                                                topRight: Radius.circular(MediaQuery.of(context).size.width * 0.08),
                                                topLeft: Radius.circular(MediaQuery.of(context).size.width * 0.08),
                                              ),
                                            ),
                                            child: Column(
                                              children: <Widget>[
                                                Padding(
                                                  padding: EdgeInsets.fromLTRB(0, MediaQuery.of(context).size.width * 0.07, 0, 0),
                                                  child: SvgPicture.asset(
                                                    "images/oups.svg",
                                                    height: MediaQuery.of(context).size.height * 0.11,
                                                    width: MediaQuery.of(context).size.height * 0.11,
                                                  ),
                                                ),
                                                SizedBox(height: MediaQuery.of(context).size.height * 0.03),
                                                Text(
                                                  "Oops !\nSomething went wrong.",
                                                  textAlign: TextAlign.center,
                                                  style: GoogleFonts.poppins(
                                                    fontSize: MediaQuery.of(context).size.height * 0.022,
                                                    fontWeight: FontWeight.w700,
                                                    height: 1.1,
                                                    color: Color(0xFFF52079),
                                                  ),
                                                ),
                                                SizedBox(height: MediaQuery.of(context).size.height * 0.02),
                                                Text(
                                                  "An error appeared during transaction.\nPlease verify your informations and then\ntry again or contact support.",
                                                  textAlign: TextAlign.center,
                                                  style: GoogleFonts.poppins(color: Color(0xFF323754), fontSize: MediaQuery.of(context).size.height * 0.018, letterSpacing: 1, height: 1.23, fontWeight: FontWeight.w400),
                                                ),
                                                SizedBox(height: MediaQuery.of(context).size.height * 0.03),
                                                SizedBox(
                                                  height: MediaQuery.of(context).size.height * 0.05,
                                                  width: MediaQuery.of(context).size.width * 0.5,
                                                  child: ElevatedButton(
                                                    style: ButtonStyle(
                                                      backgroundColor: MaterialStateProperty.all(
                                                        Color(0xFFFFFFFF),
                                                      ),
                                                      shape: MaterialStateProperty.all(
                                                        RoundedRectangleBorder(
                                                          borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.07),
                                                        ),
                                                      ),
                                                    ),
                                                    onPressed: () {
                                                      setState(() {
                                                        transactionHash = "";
                                                        _errorMessageTransaction = false;
                                                        _messageTransactionExist = false;

                                                        _cryptoField = false;
                                                        _dollarField = false;
                                                        _walletField = false;
                                                        _cryptoPattern = false;
                                                        _dollarPattern = false;
                                                        _walletPattern = false;
                                                        _walletFieldRequired = false;
                                                        _transactionPasswordField = false;
                                                        _firstStepSendFlow = true;
                                                        listElementClicked = false;
                                                        _secondStepSendFlow = false;
                                                        _successSend = false;
                                                        _errorSendProcess = false;
                                                        _cryptoBalance.text = "";
                                                        _dollarBalance.text = "";
                                                        _transactionPwd.text = "";
                                                        _walletAddress.text = "";

                                                        dropdownValue = 1;
                                                        _isLoadingSend = false;
                                                        _feesLoading = false;
                                                      });
                                                    },
                                                    child: Row(
                                                      children: <Widget>[
                                                        Spacer(),
                                                        Text(
                                                          "Try again",
                                                          style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.height * 0.017, color: Color(0xFF4048FF), fontWeight: FontWeight.w600),
                                                        ),
                                                        Spacer(),
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                                SizedBox(height: MediaQuery.of(context).size.height * 0.02),
                                                SizedBox(
                                                  height: MediaQuery.of(context).size.height * 0.05,
                                                  width: MediaQuery.of(context).size.width * 0.5,
                                                  child: ElevatedButton(
                                                    style: ButtonStyle(
                                                      backgroundColor: MaterialStateProperty.all(
                                                        Color(0xFFFFFFFF),
                                                      ),
                                                      shape: MaterialStateProperty.all(
                                                        RoundedRectangleBorder(
                                                          borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.07),
                                                        ),
                                                      ),
                                                    ),
                                                    onPressed: () {},
                                                    child: Row(
                                                      children: <Widget>[
                                                        Spacer(),
                                                        Text(
                                                          "Contact support",
                                                          style: GoogleFonts.poppins(
                                                            color: Color(0xFF4048FF),
                                                            fontSize: MediaQuery.of(context).size.height * 0.017,
                                                            fontWeight: FontWeight.w600,
                                                          ),
                                                        ),
                                                        Spacer(),
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  ],
                                )
                              : null))),
            ),
          ),
        );
      });
    } else {
      return Center(
          child: CircularProgressIndicator(
        color: Colors.white,
      ));
    }
  }

  Widget firstStepFlowWidget() {
    return Column(
      children: <Widget>[
        Container(
          height: MediaQuery.of(context).size.height * 0.14,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.05, 0, 0, MediaQuery.of(context).size.height * 0.032),
                child: Text(
                  "Send",
                  style: GoogleFonts.poppins(
                    fontSize: MediaQuery.of(context).size.height * 0.035,
                    fontWeight: FontWeight.w700,
                    color: Colors.white,
                  ),
                ),
              ),
              Spacer(),
              Padding(
                padding: EdgeInsets.fromLTRB(0, 0, MediaQuery.of(context).size.width * 0.037, MediaQuery.of(context).size.height * 0.021),
                child: InkWell(
                  onTap: () {
                    Provider.of<WalletController>(context, listen: false).selectedIndex = 2;
                    Navigator.pushReplacementNamed(context, '/welcomescreen');
                  },
                  child: Text(
                    "< Back",
                    style: GoogleFonts.poppins(color: Colors.white, letterSpacing: 1.2, fontWeight: FontWeight.w600, fontSize: MediaQuery.of(context).size.height * 0.022),
                  ),
                ),
              )
            ],
          ),
        ),
        Expanded(
          child: ListView(
            children: [
              Container(
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(MediaQuery.of(context).size.width * 0.08),
                      topRight: Radius.circular(MediaQuery.of(context).size.width * 0.08),
                    )),
                child: Form(
                  key: _formKey,
                  child: Column(
                    children: <Widget>[
                      Stack(
                        children: [
                          /**
                           *      USER INPUT CRYPTO BALANCE
                           * */
                          Padding(
                            padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.075),
                            child: Center(
                              child: Padding(
                                padding: EdgeInsets.fromLTRB(0, MediaQuery.of(context).size.height * 0.03, 0, 0),
                                child: Align(
                                  alignment: Alignment.center,
                                  child: TextFormField(
                                    readOnly: !_balanceExist,
                                    onTap: () {
                                      setState(() {
                                        _clickedContainerPaymentMethod = false;
                                        _clickedCryptoContainer = false;
                                      });
                                    },
                                    inputFormatters: <TextInputFormatter>[
                                      DecimalTextInputFormatter(decimalRange: 8),
                                      FilteringTextInputFormatter.allow(
                                        new RegExp(r'^[0-9]+\.?\d{0,8}'),
                                      ),
                                      CustomMaxValueInputFormatter(maxInputValue: 999999999.99999999),
                                    ],
                                    controller: _cryptoBalance,
                                    focusNode: cryptoFocusNode,
                                    decoration: InputDecoration(
                                      border: InputBorder.none,
                                      hintText: '0',
                                      hintStyle: GoogleFonts.poppins(color: Colors.black, fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.045),
                                      alignLabelWithHint: true,
                                    ),
                                    style: GoogleFonts.poppins(fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.045, color: _balanceExist ? Colors.black : const Color(0XFFF52079)),
                                    textAlign: TextAlign.center,
                                    keyboardType: Platform.isIOS ? TextInputType.datetime : TextInputType.number,
                                    onChanged: (value) {
                                      setState(() {
                                        cryptoBalanceUserInput = value;
                                      });
                                      value = value.replaceAll(new RegExp("[^\\d.]"), "");
                                      try {
                                        if (value.length == 0 || double.parse(value) <= 0) {
                                          _dollarBalance.text = "";
                                          setState(() {
                                            _cryptoField = false;
                                            _dollarField = false;
                                            _cryptoPattern = false;
                                            _dollarPattern = false;
                                          });
                                        } else {
                                          value = value.replaceAll(new RegExp("[^\\d.]"), "");
                                          _cryptoBalance.text = value;
                                          cryptoBalance = _cryptoBalance.text.replaceAll(',', '');
                                          cryptoBalance = _cryptoBalance.text.replaceAll('\$', '');
                                          setState(() {
                                            _cryptoField = true;
                                            _dollarField = true;
                                            _cryptoPattern = true;
                                            _dollarPattern = true;
                                          });
                                          _dollarBalance.text = setDollarBalance(_selectedCrypto, value);
                                          dollarBalance = _dollarBalance.text.replaceAll(new RegExp(r'[^\w\s]+'), '');
                                          _cryptoBalance.selection = TextSelection.fromPosition(TextPosition(offset: _cryptoBalance.text.length));
                                        }
                                      } catch (e) {
                                        setState(() {
                                          _cryptoPattern = false;
                                          _dollarPattern = false;
                                          _cryptoField = false;
                                          _dollarField = false;
                                        });
                                      }
                                    },
                                  ),
                                ),
                              ),
                            ),
                          ),

                          /**
                           *     USER BALANCE for crypto selected
                           * */
                          Padding(
                            padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.19),
                            child: Center(
                              child: Column(
                                children: [
                                  Text(
                                    "Balance : \$${formatDollarBalance(_cryptoList.firstWhere((e) => e.symbol == _selectedCrypto).total_balance ?? 0).toString().replaceAll("\$", "")}",
                                    style: GoogleFonts.poppins(color: Color(0XFFADADC8), fontSize: MediaQuery.of(context).size.height * 0.02, fontStyle: FontStyle.normal, fontWeight: FontWeight.w400, letterSpacing: 0.9),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.fromLTRB(0, MediaQuery.of(context).size.height * 0.002, 0, 0),
                                    child: InkWell(
                                        onTap: !_feesAwait
                                            ? () async {
                                                /**                Get user balance of crypto selected                   */
                                                String balance = formatDollarBalance(_cryptoList.firstWhere((e) => e.symbol == _selectedCrypto).total_balance ?? 0).toString().replaceAll("\$", "").replaceAll(',', '');

                                                /**                  Check if the user balance is greater than 0           */
                                                if (double.parse(balance) > 0) {
                                                  /**               Check the selected crypto to remove the fees           */

                                                  if (_selectedCrypto == "ETH") {
                                                    _dollarBalance.text = (double.parse(balance) - feesERC20).toString().replaceAll(new RegExp("[^\\d.]"), "");

                                                    dollarBalance = _dollarBalance.text.replaceAll(',', '');
                                                    dollarBalance = _dollarBalance.text.replaceAll('\$', '');
                                                    setState(() {
                                                      _dollarField = true;
                                                      _cryptoField = true;
                                                      _dollarPattern = true;
                                                      _cryptoPattern = true;
                                                    });
                                                    _cryptoBalance.text = setCryptoBalance(_selectedCrypto, _dollarBalance.text);
                                                    cryptoBalance = _cryptoBalance.text.replaceAll(',', '');
                                                    cryptoBalance = _cryptoBalance.text.replaceAll('\$', '');

                                                    _cryptoBalance.text = formatCryptoBalance(double.parse(_cryptoBalance.text.replaceAll(new RegExp("[^\\d.]"), "")), "");
                                                    _dollarBalance.text = formatDollarBalance(double.parse(_dollarBalance.text.replaceAll(new RegExp("[^\\d.]"), "")));
                                                  } else if (_selectedCrypto == "BNB") {
                                                    _dollarBalance.text = (double.parse(balance) - feesBEP20).toString().replaceAll(new RegExp("[^\\d.]"), "");

                                                    dollarBalance = _dollarBalance.text.replaceAll(',', '');
                                                    dollarBalance = _dollarBalance.text.replaceAll('\$', '');
                                                    setState(() {
                                                      _dollarField = true;
                                                      _cryptoField = true;
                                                      _dollarPattern = true;
                                                      _cryptoPattern = true;
                                                    });
                                                    _cryptoBalance.text = setCryptoBalance(_selectedCrypto, _dollarBalance.text);
                                                    cryptoBalance = _cryptoBalance.text.replaceAll(',', '');
                                                    cryptoBalance = _cryptoBalance.text.replaceAll('\$', '');

                                                    _cryptoBalance.text = formatCryptoBalance(double.parse(_cryptoBalance.text.replaceAll(new RegExp("[^\\d.]"), "")), "");
                                                    _dollarBalance.text = formatDollarBalance(double.parse(_dollarBalance.text.replaceAll(new RegExp("[^\\d.]"), "")));
                                                  } else if (_selectedCrypto == "MATIC") {
                                                    _dollarBalance.text = (double.parse(balance) - feesMATIC).toString().replaceAll(new RegExp("[^\\d.]"), "");

                                                    dollarBalance = _dollarBalance.text.replaceAll(',', '');
                                                    dollarBalance = _dollarBalance.text.replaceAll('\$', '');
                                                    setState(() {
                                                      _dollarField = true;
                                                      _cryptoField = true;
                                                      _dollarPattern = true;
                                                      _cryptoPattern = true;
                                                    });
                                                    _cryptoBalance.text = setCryptoBalance(_selectedCrypto, _dollarBalance.text);
                                                    cryptoBalance = _cryptoBalance.text.replaceAll(',', '');
                                                    cryptoBalance = _cryptoBalance.text.replaceAll('\$', '');

                                                    _cryptoBalance.text = formatCryptoBalance(double.parse(_cryptoBalance.text.replaceAll(new RegExp("[^\\d.]"), "")), "");
                                                    _dollarBalance.text = formatDollarBalance(double.parse(_dollarBalance.text.replaceAll(new RegExp("[^\\d.]"), "")));
                                                  } else if (_selectedCrypto == "BTT") {
                                                    _dollarBalance.text = (double.parse(balance) - feesBTT).toString().replaceAll(new RegExp("[^\\d.]"), "");

                                                    dollarBalance = _dollarBalance.text.replaceAll(',', '');
                                                    dollarBalance = _dollarBalance.text.replaceAll('\$', '');
                                                    setState(() {
                                                      _dollarField = true;
                                                      _cryptoField = true;
                                                      _dollarPattern = true;
                                                      _cryptoPattern = true;
                                                    });
                                                    _cryptoBalance.text = setCryptoBalance(_selectedCrypto, _dollarBalance.text);
                                                    cryptoBalance = _cryptoBalance.text.replaceAll(',', '');
                                                    cryptoBalance = _cryptoBalance.text.replaceAll('\$', '');

                                                    _cryptoBalance.text = formatCryptoBalance(double.parse(_cryptoBalance.text.replaceAll(new RegExp("[^\\d.]"), "")), "");
                                                    _dollarBalance.text = formatDollarBalance(double.parse(_dollarBalance.text.replaceAll(new RegExp("[^\\d.]"), "")));
                                                  } else if (_selectedCrypto == "TRX") {
                                                    _dollarBalance.text = (double.parse(balance) - feesTRX).toString().replaceAll(new RegExp("[^\\d.]"), "");

                                                    dollarBalance = _dollarBalance.text.replaceAll(',', '');
                                                    dollarBalance = _dollarBalance.text.replaceAll('\$', '');
                                                    setState(() {
                                                      _dollarField = true;
                                                      _cryptoField = true;
                                                      _dollarPattern = true;
                                                      _cryptoPattern = true;
                                                    });
                                                    _cryptoBalance.text = setCryptoBalance(_selectedCrypto, _dollarBalance.text);
                                                    cryptoBalance = _cryptoBalance.text.replaceAll(',', '');
                                                    cryptoBalance = _cryptoBalance.text.replaceAll('\$', '');

                                                    _cryptoBalance.text = formatCryptoBalance(double.parse(_cryptoBalance.text.replaceAll(new RegExp("[^\\d.]"), "")), "");
                                                    _dollarBalance.text = formatDollarBalance(double.parse(_dollarBalance.text.replaceAll(new RegExp("[^\\d.]"), "")));
                                                  } else {
                                                    _dollarBalance.text = balance.replaceAll(new RegExp("[^\\d.]"), "");

                                                    dollarBalance = _dollarBalance.text.replaceAll(',', '');
                                                    dollarBalance = _dollarBalance.text.replaceAll('\$', '');
                                                    setState(() {
                                                      _dollarField = true;
                                                      _cryptoField = true;
                                                      _dollarPattern = true;
                                                      _cryptoPattern = true;
                                                    });
                                                    _cryptoBalance.text = setCryptoBalance(_selectedCrypto, _dollarBalance.text);
                                                    cryptoBalance = _cryptoBalance.text.replaceAll(',', '');
                                                    cryptoBalance = _cryptoBalance.text.replaceAll('\$', '');

                                                    _cryptoBalance.text = formatCryptoBalance(double.parse(_cryptoBalance.text.replaceAll(new RegExp("[^\\d.]"), "")), "");
                                                    _dollarBalance.text = formatDollarBalance(double.parse(_dollarBalance.text.replaceAll(new RegExp("[^\\d.]"), "")));
                                                  }
                                                } else {
                                                  Fluttertoast.showToast(msg: "Not enough budget");
                                                }
                                              }
                                            : null,
                                        child: !_feesAwait
                                            ? Text(
                                                "USE MAX",
                                                style: GoogleFonts.poppins(
                                                  color: Color(0xFF4048FF),
                                                  fontStyle: FontStyle.normal,
                                                  letterSpacing: 0.4,
                                                  fontSize: MediaQuery.of(context).size.height * 0.02,
                                                  fontWeight: FontWeight.w500,
                                                ),
                                              )
                                            : DefaultTextStyle(
                                                style: GoogleFonts.poppins(
                                                  color: Color(0xFF4048FF),
                                                  fontStyle: FontStyle.normal,
                                                  letterSpacing: 0.4,
                                                  fontSize: MediaQuery.of(context).size.height * 0.02,
                                                  fontWeight: FontWeight.w500,
                                                ),
                                                child: AnimatedTextKit(
                                                  animatedTexts: [ScaleAnimatedText('- -')],
                                                ))),
                                  )
                                ],
                              ),
                            ),
                          ),

                          /**
                           *     USER INPUT dollar balance
                           * */
                          Padding(
                            padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.22),
                            child: Center(
                              child: Padding(
                                padding: EdgeInsets.fromLTRB(0, MediaQuery.of(context).size.height * 0.03, 0, 0),
                                child: TextFormField(
                                  readOnly: !_balanceExist,
                                  onTap: () {
                                    setState(() {
                                      _clickedContainerPaymentMethod = false;
                                      _clickedCryptoContainer = false;
                                    });
                                  },
                                  inputFormatters: <TextInputFormatter>[
                                    DecimalTextInputFormatter(decimalRange: 8),
                                    FilteringTextInputFormatter.allow(
                                      new RegExp(r'^[0-9]+\.?\d{0,8}'),
                                    ),
                                    CustomMaxValueInputFormatter(maxInputValue: 999999999.99999999),
                                  ],
                                  controller: _dollarBalance,
                                  focusNode: dollarFocusNode,
                                  decoration: InputDecoration(
                                      border: InputBorder.none,
                                      hintText: '\$0',
                                      hintStyle: GoogleFonts.poppins(
                                        color: Color(0xFF75758F),
                                        fontSize: MediaQuery.of(context).size.height * 0.0375,
                                        fontWeight: FontWeight.bold,
                                      )),
                                  style: GoogleFonts.poppins(
                                    color: Color(0xFF75758F),
                                    fontSize: MediaQuery.of(context).size.height * 0.0375,
                                    fontWeight: FontWeight.bold,
                                  ),
                                  keyboardType: Platform.isIOS ? TextInputType.datetime : TextInputType.number,
                                  textAlign: TextAlign.center,
                                  onChanged: (value) {
                                    value = value.replaceAll(new RegExp("[^\\d.]"), "");

                                    try {
                                      if (value.length == 0 || double.parse(value) <= 0) {
                                        _cryptoBalance.text = "";
                                        setState(() {
                                          _dollarField = false;
                                          _cryptoField = false;
                                          _dollarPattern = false;
                                          _cryptoPattern = false;
                                        });
                                      } else {
                                        value = value.replaceAll(new RegExp("[^\\d.]"), "");
                                        _dollarBalance.text = value;
                                        dollarBalance = _dollarBalance.text.replaceAll(',', '');
                                        dollarBalance = _dollarBalance.text.replaceAll('\$', '');
                                        setState(() {
                                          _dollarField = true;
                                          _cryptoField = true;
                                          _dollarPattern = true;
                                          _cryptoPattern = true;
                                        });
                                        _cryptoBalance.text = setCryptoBalance(_selectedCrypto, value);
                                        cryptoBalance = _cryptoBalance.text.replaceAll(',', '');
                                        cryptoBalance = _cryptoBalance.text.replaceAll('\$', '');
                                        _dollarBalance.selection = TextSelection.fromPosition(TextPosition(offset: _dollarBalance.text.length));
                                      }
                                    } catch (e) {
                                      setState(() {
                                        _dollarPattern = false;
                                        _cryptoPattern = false;
                                        _cryptoField = false;
                                        _dollarField = false;
                                      });
                                    }
                                  },
                                ),
                              ),
                            ),
                          ),

                          /**
                           * User wallet ID
                           * */
                          Padding(
                            padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.37),
                            child: Center(
                              child: Container(
                                width: MediaQuery.of(context).size.width * 0.85,
                                child: TextFormField(
                                  readOnly: !_balanceExist,
                                  onTap: () {
                                    setState(() {
                                      _clickedContainerPaymentMethod = false;
                                      _clickedCryptoContainer = false;
                                    });
                                  },
                                  style: GoogleFonts.poppins(
                                    fontSize: MediaQuery.of(context).size.height * 0.018,
                                  ),
                                  controller: _walletAddress,
                                  onChanged: (value) {
                                    TextSelection previousSelection = _walletAddress.selection;
                                    _walletAddress.text = value;
                                    setState(() {
                                      listElementClicked = false;
                                      _walletAddress.text = value;
                                      searchbook(_walletAddress.text);
                                    });
                                    _walletAddress.selection = previousSelection;

                                    if (value.length == 0) {
                                      setState(() {
                                        _walletPattern = false;
                                      });
                                    }
                                    if (_walletAddress.text.length == 0 && _walletAddress.text == "") {
                                      setState(() {
                                        _walletAddress = qrresult.toString() as TextEditingController;
                                        _walletFieldRequired = true;
                                        _walletField = false;
                                        _walletPattern = false;
                                      });
                                    } else {
                                      if (_selectedPaymentMethod == "tron") {
                                        if (RegExp("T[A-Za-z1-9]{33}\$").hasMatch(_walletAddress.text)) {
                                          setState(() {
                                            _walletField = true;
                                            _walletPattern = false;
                                            _walletFieldRequired = false;
                                          });
                                          handleAdressBookEntry(_walletAddress.text);
                                        } else {
                                          setState(() {
                                            _walletField = false;
                                            _walletPattern = true;
                                            _walletFieldRequired = false;
                                          });
                                        }
                                      } else {
                                        if (RegExp(r"^0x[a-fA-F0-9]{40}$|^(bc1|[13])[a-zA-HJ-NP-Z0-9]{25,39}$").hasMatch(_walletAddress.text)) {
                                          setState(() {
                                            _walletField = true;
                                            _walletPattern = false;
                                            _walletFieldRequired = false;
                                          });
                                          handleAdressBookEntry(_walletAddress.text);
                                        } else {
                                          setState(() {
                                            _walletField = false;
                                            _walletPattern = true;
                                            _walletFieldRequired = false;
                                          });
                                        }
                                      }
                                    }
                                  },
                                  decoration: InputDecoration(
                                    hintText: "WALLET ID",
                                    hintStyle: GoogleFonts.poppins(
                                      fontSize: MediaQuery.of(context).size.height * 0.018,
                                      letterSpacing: 0.2,
                                      fontWeight: FontWeight.w600,
                                      color: const Color(0XFF75758F),
                                    ),
                                    suffixIcon: SizedBox(
                                      width: MediaQuery.of(context).size.width * 0.2,
                                      height: MediaQuery.of(context).size.height * 0.065,
                                      child: ElevatedButton(
                                        style: ButtonStyle(
                                          backgroundColor: MaterialStateProperty.all(
                                            Color(0xFFF6F6FF),
                                          ),
                                          shape: MaterialStateProperty.all(
                                            RoundedRectangleBorder(
                                                borderRadius: BorderRadius.only(topRight: Radius.circular(MediaQuery.of(context).size.height * 0.05), bottomRight: Radius.circular(MediaQuery.of(context).size.height * 0.05)),
                                                side: BorderSide(
                                                  color: !_walletPattern ? Color(0xFFD6D6E8) : Colors.red,
                                                )),
                                          ),
                                          elevation: MaterialStateProperty.resolveWith<double>(
                                            (Set<MaterialState> states) {
                                              if (states.contains(MaterialState.disabled)) {
                                                return 0;
                                              }
                                              return 0; // Defer to the widget's default.
                                            },
                                          ),
                                        ),
                                        onPressed: () async {
                                          if (Platform.isAndroid) {
                                            var permission = Platform.isAndroid ? Permission.storage : Permission.photos;
                                            var status_storage = await Permission.storage.status;

                                            var status = await permission.request();
                                            if (status == PermissionStatus.granted) {
                                              final result = await Permission.camera.request();
                                              if (result == PermissionStatus.granted) {
                                                FocusScope.of(context).requestFocus(new FocusNode());
                                                Future.delayed(Duration(milliseconds: 200), () {
                                                  scanQr();
                                                });
                                              } else {
                                                showDialog(
                                                    context: context,
                                                    builder: (BuildContext context) => CupertinoAlertDialog(
                                                          title: Text('Camera Permission'),
                                                          content: Text('This app needs camera access to  scan QR code '),
                                                          actions: <Widget>[
                                                            CupertinoDialogAction(
                                                              child: Text('Deny'),
                                                              onPressed: () => Navigator.of(context).pop(),
                                                            ),
                                                            CupertinoDialogAction(
                                                              child: Text('Settings'),
                                                              onPressed: () => openAppSettings(),
                                                            ),
                                                          ],
                                                        ));
                                              }
                                            }
                                          } else {
                                            FocusScope.of(context).requestFocus(new FocusNode());
                                            Future.delayed(Duration(milliseconds: 200), () {
                                              scanQr();
                                            });
                                          }
                                        },
                                        child: SvgPicture.asset(
                                          "images/qr-code-scan.svg",
                                          height: MediaQuery.of(context).size.height * 0.035,
                                        ),
                                      ),
                                    ),
                                    errorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red)),
                                    contentPadding: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width * 0.04, vertical: MediaQuery.of(context).size.height * 0.017),
                                    enabledBorder: OutlineInputBorder(
                                        borderRadius: new BorderRadius.only(
                                            topLeft: Radius.circular(MediaQuery.of(context).size.height * 0.05),
                                            bottomLeft: Radius.circular(MediaQuery.of(context).size.height * 0.05),
                                            topRight: Radius.circular(MediaQuery.of(context).size.height * 0.05),
                                            bottomRight: Radius.circular(MediaQuery.of(context).size.height * 0.05)),
                                        borderSide: BorderSide(
                                          color: !_walletPattern ? Color(0xFFD6D6E8) : Colors.red,
                                        )),
                                    focusedBorder: new OutlineInputBorder(
                                      borderRadius: new BorderRadius.only(
                                          topLeft: Radius.circular(MediaQuery.of(context).size.height * 0.05),
                                          bottomLeft: Radius.circular(MediaQuery.of(context).size.height * 0.05),
                                          topRight: Radius.circular(MediaQuery.of(context).size.height * 0.05),
                                          bottomRight: Radius.circular(MediaQuery.of(context).size.height * 0.05)),
                                      borderSide: BorderSide(
                                        color: !_walletPattern ? Color(0xFFD6D6E8) : Colors.red,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          /**
                           * See More
                           * */
                          SizedBox(
                            child: _walletPattern && !listElementClicked
                                ? Padding(
                                    padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.77),
                                    child: Center(
                                      child: InkWell(
                                        onTap: () async {
                                          displaySearchContactDialog();
                                          _refreshContacts();
                                        },
                                        child: Text(
                                          "see more",
                                          textAlign: TextAlign.center,
                                          style: GoogleFonts.poppins(
                                            fontWeight: FontWeight.w300,
                                            fontSize: MediaQuery.of(context).size.height * 0.024,
                                            color: const Color(0xFF4048FF),
                                          ),
                                        ),
                                      ),
                                    ))
                                : SizedBox(),
                          ),
                          /*****
                           *    Wallet id error MESSAGE
                           * */
                          SizedBox(
                            child: _walletPattern
                                ? Padding(
                                    padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.435),
                                    child: Center(
                                      child: Text(
                                        "The wallet address you entered does not exist.",
                                        style: GoogleFonts.poppins(color: Color(0xFFf52079), fontSize: MediaQuery.of(context).size.width * 0.032, fontWeight: FontWeight.normal),
                                      ),
                                    ),
                                  )
                                : (_walletFieldRequired
                                    ? Padding(
                                        padding: EdgeInsets.only(top: MediaQuery.of(context).size.width * 0.003),
                                        child: Text(
                                          "Field required",
                                          style: GoogleFonts.poppins(color: Color(0xFFf52079), fontSize: MediaQuery.of(context).size.width * 0.032, fontWeight: FontWeight.bold),
                                        ),
                                      )
                                    : (_walletAddress.text.length > 0 && surFix.length == 0 && _balanceExist)
                                        ? Padding(
                                            padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.54),
                                            child: Center(
                                              child: Container(
                                                  height: MediaQuery.of(context).size.height * .07,
                                                  width: MediaQuery.of(context).size.width,
                                                  decoration: BoxDecoration(color: Color(0xFF4048FF).withOpacity(0.2)),
                                                  child: InkWell(
                                                    onTap: () {
                                                      displayAddContact(_walletAddress.text);
                                                    },
                                                    child: Row(
                                                      children: [
                                                        SizedBox(width: MediaQuery.of(context).size.width * .012),
                                                        SvgPicture.asset("images/icon_alert_info.svg"),
                                                        SizedBox(width: MediaQuery.of(context).size.width * .08),
                                                        Text.rich(
                                                          TextSpan(
                                                            children: <InlineSpan>[
                                                              TextSpan(
                                                                text: 'New adress detected! ',
                                                                style: GoogleFonts.poppins(color: Color(0xFF4048FF), fontSize: MediaQuery.of(context).size.width * 0.032, fontWeight: FontWeight.normal),
                                                              ),
                                                              TextSpan(
                                                                text: ' click here ',
                                                                style: GoogleFonts.poppins(color: Color(0xFF4048FF), decoration: TextDecoration.underline, fontSize: MediaQuery.of(context).size.width * 0.032, fontWeight: FontWeight.normal),
                                                              ),
                                                              TextSpan(
                                                                text: ' to add to\nyour adress book.!',
                                                                style: GoogleFonts.poppins(color: Color(0xFF4048FF), fontSize: MediaQuery.of(context).size.width * 0.032, fontWeight: FontWeight.normal),
                                                              ),
                                                            ],
                                                          ),
                                                          textAlign: TextAlign.center,
                                                        ),
                                                      ],
                                                    ),
                                                  )),
                                            ),
                                          )
                                        : SizedBox()),
                          ),

                          /*****
                           *    List contact
                           * */
                          _walletAddress.text != "" && surFix.isNotEmpty && _balanceExist && !listElementClicked ? list() : SizedBox.shrink(),

                          /**
                           * BUTTON CONFIRM
                           * */
                          if (_balanceExist)
                            Center(
                              child: Padding(
                                padding: EdgeInsets.fromLTRB(
                                  0,
                                  _walletPattern && surFix.isNotEmpty && !listElementClicked
                                      ? MediaQuery.of(context).size.height * 0.82
                                      : surFix.isNotEmpty && _walletAddress.text != "" && !listElementClicked
                                          ? MediaQuery.of(context).size.height * 0.80
                                          : _walletPattern
                                              ? MediaQuery.of(context).size.height * 0.472
                                              : MediaQuery.of(context).size.height * 0.45,
                                  0,
                                  MediaQuery.of(context).size.height * 0.45,
                                ),
                                child: (_cryptoField && _dollarField && _walletField && _dollarPattern && _cryptoPattern)
                                    ? SizedBox(
                                        height: MediaQuery.of(context).size.height * 0.058,
                                        width: MediaQuery.of(context).size.width * 0.85,
                                        child: ElevatedButton(
                                          style: ButtonStyle(
                                            backgroundColor: MaterialStateProperty.all(
                                              Color(0xFF4048FF),
                                            ),
                                            shape: MaterialStateProperty.all(
                                              RoundedRectangleBorder(
                                                borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.08),
                                              ),
                                            ),
                                          ),
                                          onPressed: !_feesLoading
                                              ? () async {
                                                  setState(() {
                                                    _feesLoading = true;
                                                    _messageTransaction = "";
                                                    _isLoadingSend = false;
                                                    _transactionPasswordField = false;
                                                    _transactionPwd.text = "";
                                                  });

                                                  if (double.parse(Provider.of<WalletController>(context, listen: false).cryptos.firstWhere((element) => element.symbol == _selectedCrypto && element.network == _selectedNetwork).quantity.toString()) >=
                                                      double.parse(_cryptoBalance.text.replaceAll(',', ''))) {
                                                    switch (_selectedNetwork) {
                                                      case "ERC20":
                                                        await Future.wait([
                                                          feesETH(),
                                                        ]).then((value) => print(value)).catchError((e) {
                                                          print(e);
                                                        });
                                                        if (_selectedCrypto == "ETH") {
                                                          if (double.parse(Provider.of<WalletController>(context, listen: false).cryptos.firstWhere((element) => element.symbol == "ETH" && element.network == "ERC20").quantity.toString()) - double.parse(_cryptoBalance.text) > feesETHQuantity) {
                                                            setState(() {
                                                              _firstStepSendFlow = false;
                                                              _enoughFees = true;
                                                              _secondStepSendFlow = true;
                                                              _balanceExist = true;
                                                              _feesLoading = false;
                                                            });
                                                          } else {
                                                            setState(() {
                                                              _enoughFees = false;
                                                              _firstStepSendFlow = false;
                                                              _secondStepSendFlow = true;
                                                              _balanceExist = true;
                                                              _feesLoading = false;
                                                            });
                                                          }
                                                        } else {
                                                          if (double.parse(Provider.of<WalletController>(context, listen: false).cryptos.firstWhere((element) => element.symbol == "ETH" && element.network == "ERC20").quantity.toString()) < feesETHQuantity) {
                                                            setState(() {
                                                              _enoughFees = false;
                                                              _firstStepSendFlow = false;
                                                              _secondStepSendFlow = true;
                                                              _balanceExist = true;
                                                              _feesLoading = false;
                                                            });
                                                          } else {
                                                            setState(() {
                                                              _firstStepSendFlow = false;
                                                              _enoughFees = true;
                                                              _secondStepSendFlow = true;
                                                              _balanceExist = true;
                                                              _feesLoading = false;
                                                            });
                                                          }
                                                        }

                                                        break;
                                                      case "BEP20":
                                                        await Future.wait([
                                                          feesBNB(),
                                                        ]).then((value) => print("test value 50 $value")).catchError((e) {
                                                          print(e);
                                                        });
                                                        if (_selectedCrypto == "BNB") {
                                                          if (double.parse(Provider.of<WalletController>(context, listen: false).cryptos.firstWhere((element) => element.symbol == "BNB" && element.network == "BEP20").quantity.toString()) - double.parse(_cryptoBalance.text) > feesBNBQuantity) {
                                                            setState(() {
                                                              _firstStepSendFlow = false;
                                                              _enoughFees = true;
                                                              _secondStepSendFlow = true;
                                                              _balanceExist = true;
                                                              _feesLoading = false;
                                                            });
                                                          } else {
                                                            setState(() {
                                                              _enoughFees = false;
                                                              _firstStepSendFlow = false;
                                                              _secondStepSendFlow = true;
                                                              _balanceExist = true;
                                                              _feesLoading = false;
                                                            });
                                                          }
                                                        } else {
                                                          if (double.parse(Provider.of<WalletController>(context, listen: false).cryptos.firstWhere((element) => element.symbol == "BNB").quantity.toString()) < feesBNBQuantity) {
                                                            setState(() {
                                                              _firstStepSendFlow = false;
                                                              _enoughFees = false;
                                                              _secondStepSendFlow = true;
                                                              _balanceExist = true;
                                                              _feesLoading = false;
                                                            });
                                                          } else {
                                                            setState(() {
                                                              _firstStepSendFlow = false;
                                                              _enoughFees = true;
                                                              _secondStepSendFlow = true;
                                                              _balanceExist = true;
                                                              _feesLoading = false;
                                                            });
                                                          }
                                                        }

                                                        break;
                                                      case "BTC":
                                                        setState(() {
                                                          _firstStepSendFlow = false;

                                                          _secondStepSendFlow = true;
                                                          _balanceExist = true;
                                                          _feesLoading = false;
                                                        });
                                                        break;

                                                      case "POLYGON":
                                                        await Future.wait([
                                                          feesPolyGon(),
                                                        ]).then((value) => print("test value 50 poly $value")).catchError((e) {
                                                          print(e);
                                                        });
                                                        if (_selectedCrypto == "MATIC") {
                                                          if (double.parse(Provider.of<WalletController>(context, listen: false).cryptos.firstWhere((element) => element.symbol == "MATIC" && element.network == "POLYGON").quantity.toString()) - double.parse(_cryptoBalance.text) > feesMATICQuantity) {
                                                            setState(() {
                                                              _firstStepSendFlow = false;
                                                              _enoughFees = true;
                                                              _secondStepSendFlow = true;
                                                              _balanceExist = true;
                                                              _feesLoading = false;
                                                            });
                                                          } else {
                                                            setState(() {
                                                              _enoughFees = false;
                                                              _firstStepSendFlow = false;
                                                              _secondStepSendFlow = true;
                                                              _balanceExist = true;
                                                              _feesLoading = false;
                                                            });
                                                          }
                                                        } else {
                                                          if (double.parse(Provider.of<WalletController>(context, listen: false).cryptos.firstWhere((element) => element.symbol == "MATIC").quantity.toString()) < feesMATICQuantity) {
                                                            setState(() {
                                                              _firstStepSendFlow = false;
                                                              _enoughFees = false;
                                                              _secondStepSendFlow = true;
                                                              _balanceExist = true;
                                                              _feesLoading = false;
                                                            });
                                                          } else {
                                                            setState(() {
                                                              _firstStepSendFlow = false;
                                                              _enoughFees = true;
                                                              _secondStepSendFlow = true;
                                                              _balanceExist = true;
                                                              _feesLoading = false;
                                                            });
                                                          }
                                                        }

                                                        break;

                                                      case "BTTC":
                                                        await Future.wait([
                                                          feesBitTorrent(),
                                                        ]).then((value) => print("test value 50 btt $value")).catchError((e) {
                                                          print(e);
                                                        });
                                                        if (_selectedCrypto == "BTT") {
                                                          if (double.parse(Provider.of<WalletController>(context, listen: false).cryptos.firstWhere((element) => element.symbol == "BTT" && element.network == "BTTC").quantity.toString()) - double.parse(_cryptoBalance.text.replaceAll(',', '')) >
                                                              feesBTTQuantity) {
                                                            setState(() {
                                                              _firstStepSendFlow = false;
                                                              _enoughFees = true;
                                                              _secondStepSendFlow = true;
                                                              _balanceExist = true;
                                                              _feesLoading = false;
                                                            });
                                                          } else {
                                                            setState(() {
                                                              _enoughFees = false;
                                                              _firstStepSendFlow = false;
                                                              _secondStepSendFlow = true;
                                                              _balanceExist = true;
                                                              _feesLoading = false;
                                                            });
                                                          }
                                                        } else {
                                                          if (double.parse(Provider.of<WalletController>(context, listen: false).cryptos.firstWhere((element) => element.symbol == "BTT").quantity.toString()) < feesBTTQuantity) {
                                                            setState(() {
                                                              _firstStepSendFlow = false;
                                                              _enoughFees = false;
                                                              _secondStepSendFlow = true;
                                                              _balanceExist = true;
                                                              _feesLoading = false;
                                                            });
                                                          } else {
                                                            setState(() {
                                                              _firstStepSendFlow = false;
                                                              _enoughFees = true;
                                                              _secondStepSendFlow = true;
                                                              _balanceExist = true;
                                                              _feesLoading = false;
                                                            });
                                                          }
                                                        }
                                                        break;

                                                      case "TRON":
                                                        await Future.wait([
                                                          feesTrx(),
                                                        ]).then((value) => print("test value 50 tron $value")).catchError((e) {
                                                          print(e);
                                                        });
                                                        if (_selectedCrypto == "TRX") {
                                                          if (double.parse(Provider.of<WalletController>(context, listen: false).cryptos.firstWhere((element) => element.symbol == "TRX" && element.network == "TRON").quantity.toString()) - double.parse(_cryptoBalance.text.replaceAll(',', '')) >
                                                              feesTRXQuantity) {
                                                            setState(() {
                                                              _firstStepSendFlow = false;
                                                              _enoughFees = true;
                                                              _secondStepSendFlow = true;
                                                              _balanceExist = true;
                                                              _feesLoading = false;
                                                            });
                                                          } else {
                                                            setState(() {
                                                              _enoughFees = false;
                                                              _firstStepSendFlow = false;
                                                              _secondStepSendFlow = true;
                                                              _balanceExist = true;
                                                              _feesLoading = false;
                                                            });
                                                          }
                                                        } else {
                                                          if (double.parse(Provider.of<WalletController>(context, listen: false).cryptos.firstWhere((element) => element.symbol == "TRX").quantity.toString()) < feesTRXQuantity) {
                                                            setState(() {
                                                              _firstStepSendFlow = false;
                                                              _enoughFees = false;
                                                              _secondStepSendFlow = true;
                                                              _balanceExist = true;
                                                              _feesLoading = false;
                                                            });
                                                          } else {
                                                            setState(() {
                                                              _firstStepSendFlow = false;
                                                              _enoughFees = true;
                                                              _secondStepSendFlow = true;
                                                              _balanceExist = true;
                                                              _feesLoading = false;
                                                            });
                                                          }
                                                        }

                                                        break;
                                                    }
                                                  } else {
                                                    setState(() {
                                                      _balanceExist = false;
                                                      _feesLoading = false;
                                                    });
                                                  }
                                                }
                                              : null,
                                          child: !_feesLoading
                                              ? Text(
                                                  "Confirm",
                                                  style: GoogleFonts.poppins(
                                                    fontSize: MediaQuery.of(context).size.height * 0.02,
                                                    fontWeight: FontWeight.w600,
                                                  ),
                                                )
                                              : SizedBox(
                                                  height: MediaQuery.of(context).size.height * 0.0135,
                                                  width: MediaQuery.of(context).size.height * 0.0135,
                                                  child: CircularProgressIndicator(
                                                    color: Colors.white,
                                                  ),
                                                ),
                                        ))
                                    : Container(
                                        height: MediaQuery.of(context).size.height * 0.058,
                                        width: MediaQuery.of(context).size.width * 0.85,
                                        decoration: BoxDecoration(borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.08), color: Color(0xFFF6F6FF), border: Border.all(color: Color(0xFFD6D6E8), width: 1)),
                                        child: Center(
                                          child: Text(
                                            "Confirm",
                                            style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.height * 0.02, fontWeight: FontWeight.w600, color: Color(0xFFADADC8)),
                                          ),
                                        ),
                                      ),
                              ),
                            ),
                          if (!_balanceExist)
                            Center(
                                child: Padding(
                              padding: EdgeInsets.only(
                                top: MediaQuery.of(context).size.height * 0.45,
                              ),
                              child: Column(
                                children: <Widget>[
                                  Text("Insufficient funds.\nBuy tokens or change crypto.", textAlign: TextAlign.center, style: GoogleFonts.poppins(color: const Color(0XFFF52079), fontWeight: FontWeight.w500, fontStyle: FontStyle.normal, fontSize: MediaQuery.of(context).size.height * 0.0165)),
                                  SizedBox(height: MediaQuery.of(context).size.height * 0.01),
                                  SizedBox(
                                      height: MediaQuery.of(context).size.width * 0.13,
                                      width: MediaQuery.of(context).size.width * 0.8,
                                      child: ElevatedButton(
                                        style: ButtonStyle(
                                          backgroundColor: MaterialStateProperty.all(
                                            Colors.white,
                                          ),
                                          shape: MaterialStateProperty.all(
                                            RoundedRectangleBorder(
                                              borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.08),
                                            ),
                                          ),
                                        ),
                                        onPressed: () {
                                          if (_selectedNetwork != "BTTC") {
                                            Provider.of<LoginController>(context, listen: false).tokenSymbol = Provider.of<WalletController>(context, listen: false).cryptos.firstWhere((e) => e.symbol == _selectedCrypto).symbol.toString();
                                            Provider.of<LoginController>(context, listen: false).tokenNetwork = Provider.of<WalletController>(context, listen: false).cryptos.firstWhere((e) => e.symbol == _selectedCrypto).network.toString();
                                            Provider.of<WalletController>(context, listen: false).selectedIndex = 7;
                                            Navigator.pushReplacementNamed(context, "/welcomescreen");
                                          } else {
                                            _launchURLBuyBTT();
                                          }
                                        },
                                        child: Text(
                                          "Buy more ${Provider.of<WalletController>(context, listen: false).cryptos.firstWhere((e) => e.symbol == _selectedCrypto && e.network == _selectedNetwork).name}",
                                          style: GoogleFonts.poppins(
                                            fontSize: MediaQuery.of(context).size.height * 0.018,
                                            fontWeight: FontWeight.w600,
                                            color: const Color(0XFF4048FF),
                                          ),
                                        ),
                                      ))
                                ],
                              ),
                            )),
                          /**
                           *  NETWORK LIST && CRYPTO LIST  ----- SECTION
                           * */
                          Padding(
                            padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.025, left: MediaQuery.of(context).size.width * 0.0375),
                            child: Stack(
                              children: <Widget>[
                                /**
                                 *
                                 * NETWORK SELECT SECTION      */

                                Padding(
                                  padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.475),
                                  child: Align(
                                    alignment: Alignment.bottomLeft,
                                    child: InkWell(
                                      onTap: () {
                                        if (_selectedNetwork != "BTC" || _selectedPaymentMethod != "polygon") {
                                          setState(() {
                                            _clickedCryptoContainer = !_clickedCryptoContainer;
                                          });
                                        }
                                      },
                                      child: Container(
                                        height: _clickedCryptoContainer ? MediaQuery.of(context).size.height * 0.15 : MediaQuery.of(context).size.height * 0.06,
                                        width: MediaQuery.of(context).size.width * 0.45,
                                        decoration: BoxDecoration(
                                          border: Border.all(color: Color(0xFFD6D6E8)),
                                          borderRadius: BorderRadius.circular(30),
                                          color: Colors.white,
                                        ),
                                        child: _clickedCryptoContainer
                                            ? Padding(
                                                padding: EdgeInsets.only(
                                                  bottom: MediaQuery.of(context).size.height * 0.015,
                                                  right: MediaQuery.of(context).size.height * 0.015,
                                                  top: MediaQuery.of(context).size.height * 0.01,
                                                ),
                                                child: Theme(
                                                    data: scrolltheme,
                                                    child: Scrollbar(
                                                      child: ListView.builder(
                                                        scrollDirection: Axis.vertical,
                                                        shrinkWrap: true,
                                                        itemCount: _cryptoList.length,
                                                        itemBuilder: (context, index) {
                                                          String cryptoName = "";
                                                          if (_cryptoList[index].name.toString().length > 5) {
                                                            cryptoName = _cryptoList[index].name.toString().substring(0, 5).toUpperCase() + "...";
                                                          } else {
                                                            cryptoName = _cryptoList[index].name.toString().toUpperCase();
                                                          }
                                                          return Padding(
                                                            padding: EdgeInsets.only(
                                                              top: MediaQuery.of(context).size.height * 0.015,
                                                              left: MediaQuery.of(context).size.width * 0.015,
                                                            ),
                                                            child: InkWell(
                                                              onTap: () {
                                                                setState(() {
                                                                  setFieldSelection(_cryptoList[index], _selectedPaymentMethod, _selectedNetwork, _isLoadingNetwork);
                                                                });
                                                              },
                                                              child: Row(
                                                                children: [
                                                                  _cryptoList[index].addedToken == true
                                                                      ? (_cryptoList[index].picUrl != "false" && _cryptoList[index].picUrl != "null"
                                                                          ? Padding(
                                                                              padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.018),
                                                                              child: Image.network(
                                                                                _cryptoList[index].picUrl ?? "",
                                                                                width: MediaQuery.of(context).size.width * 0.055,
                                                                                height: MediaQuery.of(context).size.width * 0.055,
                                                                              ),
                                                                            )
                                                                          : Padding(
                                                                              padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.018),
                                                                              child: SvgPicture.asset(
                                                                                'images/indispo.svg',
                                                                                width: MediaQuery.of(context).size.width * 0.055,
                                                                                height: MediaQuery.of(context).size.width * 0.055,
                                                                              ),
                                                                            ))
                                                                      : Padding(
                                                                          padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.018),
                                                                          child: SvgPicture.asset(
                                                                            'images/' + _cryptoList[index].name.toString().toLowerCase().replaceAll(' ', '') + '.svg',
                                                                            alignment: Alignment.center,
                                                                            width: MediaQuery.of(context).size.width * 0.055,
                                                                            height: MediaQuery.of(context).size.width * 0.055,
                                                                          ),
                                                                        ),
                                                                  SizedBox(
                                                                    width: MediaQuery.of(context).size.width * 0.02,
                                                                  ),
                                                                  new Text(
                                                                    _cryptoList[index].symbol.toString().toUpperCase() == "SATTBEP20" ? "SATT" : _cryptoList[index].symbol.toString().toUpperCase(),
                                                                    style: GoogleFonts.poppins(color: Colors.black, fontWeight: FontWeight.w600, fontSize: MediaQuery.of(context).size.height * 0.0165),
                                                                  ),
                                                                ],
                                                              ),
                                                            ),
                                                          );
                                                        },
                                                      ),
                                                    )))
                                            : Padding(
                                                padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.015),
                                                child: Row(
                                                  children: [
                                                    _selectedCryptoAddedToken == true
                                                        ? (_selectedCryptoPicUrl != "false" && _selectedCryptoPicUrl != "null"
                                                            ? Padding(
                                                                padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.018),
                                                                child: Image.network(
                                                                  _selectedCryptoPicUrl,
                                                                  width: MediaQuery.of(context).size.width * 0.055,
                                                                  height: MediaQuery.of(context).size.width * 0.055,
                                                                ),
                                                              )
                                                            : Padding(
                                                                padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.018),
                                                                child: SvgPicture.asset(
                                                                  'images/indispo.svg',
                                                                  width: MediaQuery.of(context).size.width * 0.055,
                                                                  height: MediaQuery.of(context).size.width * 0.055,
                                                                ),
                                                              ))
                                                        : Padding(
                                                            padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.018),
                                                            child: SvgPicture.asset(
                                                              'images/' + _selectedCryptoName.toLowerCase().replaceAll(' ', '') + '.svg',
                                                              alignment: Alignment.center,
                                                              width: MediaQuery.of(context).size.width * 0.055,
                                                              height: MediaQuery.of(context).size.width * 0.055,
                                                            ),
                                                          ),
                                                    SizedBox(
                                                      width: MediaQuery.of(context).size.width * 0.02,
                                                    ),
                                                    new Text(
                                                      _selectedCryptoSymbol == "SATTBEP20" ? "SATT" : _selectedCryptoSymbol,
                                                      style: GoogleFonts.poppins(color: Colors.black, fontWeight: FontWeight.w600, fontSize: MediaQuery.of(context).size.height * 0.0165),
                                                    ),
                                                    const Spacer(),
                                                    Padding(
                                                      padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.05),
                                                      child: SizedBox(
                                                        width: 10,
                                                        child: Transform.rotate(
                                                            angle: -pi / 2,
                                                            child: Icon(
                                                              Icons.arrow_back_ios,
                                                              color: Colors.grey,
                                                              size: MediaQuery.of(context).size.width * 0.06,
                                                            )),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                      ),
                                    ),
                                  ),
                                ),

                                /**
                                 *  Crypto List SECTION
                                 * */

                                InkWell(
                                  onTap: () {
                                    setState(() {
                                      _clickedContainerPaymentMethod = !_clickedContainerPaymentMethod;
                                    });
                                  },
                                  child: Container(
                                    width: MediaQuery.of(context).size.width * 0.45,
                                    height: _clickedContainerPaymentMethod ? MediaQuery.of(context).size.height * 0.3 : MediaQuery.of(context).size.height * 0.06,
                                    decoration: BoxDecoration(
                                      border: Border.all(color: Color(0xFFD6D6E8)),
                                      borderRadius: BorderRadius.circular(30),
                                      color: Colors.white,
                                    ),
                                    child: Column(
                                      children: <Widget>[
                                        Padding(
                                          padding: EdgeInsets.only(top: MediaQuery.of(context).size.height < 1000 ? MediaQuery.of(context).size.height * 0.01 : MediaQuery.of(context).size.height * 0.005),
                                          child: _selectedPaymentMethod == "fiat"
                                              ? Row(
                                                  children: <Widget>[
                                                    Padding(
                                                      padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025),
                                                      child: SvgPicture.asset(
                                                        "images/btc-icon.svg",
                                                        height: MediaQuery.of(context).size.height * 0.025,
                                                        width: MediaQuery.of(context).size.width * 0.025,
                                                      ),
                                                    ),
                                                    Padding(
                                                      padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.035, left: MediaQuery.of(context).size.width * 0.012),
                                                      child: Text(
                                                        "BTC",
                                                        style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.018),
                                                      ),
                                                    ),
                                                    Spacer(),
                                                    Padding(
                                                      padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.055, top: MediaQuery.of(context).size.height * 0.0035),
                                                      child: SizedBox(
                                                        width: 10,
                                                        child: Transform.rotate(
                                                            angle: -pi / 2,
                                                            child: Icon(
                                                              Icons.arrow_back_ios,
                                                              color: Colors.grey,
                                                              size: MediaQuery.of(context).size.width * 0.06,
                                                            )),
                                                      ),
                                                    ),
                                                  ],
                                                )
                                              : (_selectedPaymentMethod == "erc20"
                                                  ? Row(
                                                      children: <Widget>[
                                                        Padding(
                                                          padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025),
                                                          child: SvgPicture.asset(
                                                            'images/testtest.svg',
                                                            height: MediaQuery.of(context).size.width * 0.08,
                                                            width: MediaQuery.of(context).size.width * 0.08,
                                                            color: Colors.black,
                                                          ),
                                                        ),
                                                        Padding(
                                                          padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.035, left: MediaQuery.of(context).size.width * 0.0075),
                                                          child: Text(
                                                            "ERC20",
                                                            style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.018),
                                                          ),
                                                        ),
                                                        Spacer(),
                                                        Padding(
                                                          padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.05, top: MediaQuery.of(context).size.height * 0.0035),
                                                          child: SizedBox(
                                                            width: 10,
                                                            child: Transform.rotate(
                                                                angle: -pi / 2,
                                                                child: Icon(
                                                                  Icons.arrow_back_ios,
                                                                  color: Colors.grey,
                                                                  size: MediaQuery.of(context).size.width * 0.06,
                                                                )),
                                                          ),
                                                        ),
                                                      ],
                                                    )
                                                  : (_selectedPaymentMethod == "polygon"
                                                      ? Row(
                                                          children: <Widget>[
                                                            Padding(
                                                              padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025),
                                                              child: SvgPicture.asset(
                                                                'images/polygon-network.svg',
                                                                height: MediaQuery.of(context).size.width * 0.05,
                                                                width: MediaQuery.of(context).size.width * 0.05,
                                                                color: Colors.black,
                                                              ),
                                                            ),
                                                            Padding(
                                                              padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.035),
                                                              child: Text(
                                                                "POLYGON",
                                                                style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.015),
                                                              ),
                                                            ),
                                                            Spacer(),
                                                            Padding(
                                                              padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.05, top: MediaQuery.of(context).size.height * 0.0035),
                                                              child: SizedBox(
                                                                width: 10,
                                                                child: Transform.rotate(
                                                                    angle: -pi / 2,
                                                                    child: Icon(
                                                                      Icons.arrow_back_ios,
                                                                      color: Colors.grey,
                                                                      size: MediaQuery.of(context).size.width * 0.06,
                                                                    )),
                                                              ),
                                                            ),
                                                          ],
                                                        )
                                                      : (_selectedPaymentMethod == "btt"
                                                          ? Row(
                                                              children: <Widget>[
                                                                Padding(
                                                                  padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025),
                                                                  child: SvgPicture.asset(
                                                                    "images/btt-network.svg",
                                                                    height: MediaQuery.of(context).size.height * 0.025,
                                                                    width: MediaQuery.of(context).size.width * 0.025,
                                                                  ),
                                                                ),
                                                                Padding(
                                                                  padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.035, left: MediaQuery.of(context).size.width * 0.012),
                                                                  child: Text(
                                                                    "BTTC",
                                                                    style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.018),
                                                                  ),
                                                                ),
                                                                Spacer(),
                                                                Padding(
                                                                  padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.055, top: MediaQuery.of(context).size.height * 0.0035),
                                                                  child: SizedBox(
                                                                    width: 10,
                                                                    child: Transform.rotate(
                                                                        angle: -pi / 2,
                                                                        child: Icon(
                                                                          Icons.arrow_back_ios,
                                                                          color: Colors.grey,
                                                                          size: MediaQuery.of(context).size.width * 0.06,
                                                                        )),
                                                                  ),
                                                                ),
                                                              ],
                                                            )
                                                          : (_selectedPaymentMethod == "tron"
                                                              ? Row(
                                                                  children: <Widget>[
                                                                    Padding(
                                                                      padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025),
                                                                      child: SvgPicture.asset(
                                                                        'images/wallet-tron.svg',
                                                                        height: MediaQuery.of(context).size.width * 0.05,
                                                                        width: MediaQuery.of(context).size.width * 0.05,
                                                                        color: Colors.black,
                                                                      ),
                                                                    ),
                                                                    Padding(
                                                                      padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.035, left: MediaQuery.of(context).size.width * 0.005),
                                                                      child: Text(
                                                                        "TRON",
                                                                        style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.018),
                                                                      ),
                                                                    ),
                                                                    Spacer(),
                                                                    Padding(
                                                                      padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.05, top: MediaQuery.of(context).size.height * 0.0035),
                                                                      child: SizedBox(
                                                                        width: 10,
                                                                        child: Transform.rotate(
                                                                            angle: -pi / 2,
                                                                            child: Icon(
                                                                              Icons.arrow_back_ios,
                                                                              color: Colors.grey,
                                                                              size: MediaQuery.of(context).size.width * 0.06,
                                                                            )),
                                                                      ),
                                                                    ),
                                                                  ],
                                                                )
                                                              : Row(
                                                                  children: <Widget>[
                                                                    Padding(
                                                                      padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025),
                                                                      child: SvgPicture.asset(
                                                                        'images/bep.svg',
                                                                        height: MediaQuery.of(context).size.width * 0.05,
                                                                        width: MediaQuery.of(context).size.width * 0.05,
                                                                        color: Colors.black,
                                                                      ),
                                                                    ),
                                                                    Padding(
                                                                      padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.035, left: MediaQuery.of(context).size.width * 0.005),
                                                                      child: Text(
                                                                        "BEP20",
                                                                        style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.018),
                                                                      ),
                                                                    ),
                                                                    Spacer(),
                                                                    Padding(
                                                                      padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.05, top: MediaQuery.of(context).size.height * 0.0035),
                                                                      child: SizedBox(
                                                                        width: 10,
                                                                        child: Transform.rotate(
                                                                            angle: -pi / 2,
                                                                            child: Icon(
                                                                              Icons.arrow_back_ios,
                                                                              color: Colors.grey,
                                                                              size: MediaQuery.of(context).size.width * 0.06,
                                                                            )),
                                                                      ),
                                                                    ),
                                                                  ],
                                                                ))))),
                                        ),
                                        Visibility(
                                            visible: _clickedContainerPaymentMethod,
                                            child: _selectedPaymentMethod == "fiat"
                                                ? Padding(
                                                    padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.012),
                                                    child: Column(
                                                      children: <Widget>[
                                                        Padding(
                                                          padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.0085),
                                                          child: InkWell(
                                                            onTap: () {
                                                              fetchCrypto().then((value) {
                                                                setState(() {
                                                                  _cryptoList.clear();
                                                                  _cryptoList.addAll(value.where((c) => c.network == _selectedNetwork).toList());
                                                                  setFieldSelection(_cryptoList[0], "bep20", "BEP20", false);
                                                                });
                                                              });
                                                            },
                                                            child: Row(
                                                              children: <Widget>[
                                                                Padding(
                                                                  padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025),
                                                                  child: SvgPicture.asset(
                                                                    'images/bep.svg',
                                                                    height: MediaQuery.of(context).size.width * 0.05,
                                                                    width: MediaQuery.of(context).size.width * 0.05,
                                                                    color: Colors.black,
                                                                  ),
                                                                ),
                                                                SizedBox(width: MediaQuery.of(context).size.width * 0.005),
                                                                Text(
                                                                  "BEP20",
                                                                  style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.017),
                                                                ),
                                                              ],
                                                            ),
                                                          ),
                                                        ),
                                                        InkWell(
                                                          onTap: () {
                                                            fetchCrypto().then((value) {
                                                              setState(() {
                                                                _cryptoList.clear();
                                                                _cryptoList.addAll(value.where((c) => c.network == _selectedNetwork).toList());
                                                                setFieldSelection(_cryptoList[0], "erc20", "ERC20", false);
                                                              });
                                                            });
                                                          },
                                                          child: Row(
                                                            children: <Widget>[
                                                              Padding(
                                                                padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.037, right: MediaQuery.of(context).size.width * 0.025),
                                                                child: SvgPicture.asset(
                                                                  'images/testtest.svg',
                                                                  height: MediaQuery.of(context).size.width * 0.08,
                                                                  width: MediaQuery.of(context).size.width * 0.08,
                                                                  color: Colors.black,
                                                                ),
                                                              ),
                                                              Padding(
                                                                padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.0075),
                                                                child: Text(
                                                                  "ERC20",
                                                                  style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.017),
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                        Padding(
                                                          padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.0085, top: MediaQuery.of(context).size.height * 0.005),
                                                          child: InkWell(
                                                            onTap: () {
                                                              fetchCrypto().then((value) {
                                                                setState(() {
                                                                  _cryptoList.clear();
                                                                  _cryptoList.addAll(value.where((c) => c.network == _selectedNetwork).toList());
                                                                  setFieldSelection(_cryptoList[0], "polygon", "POLYGON", false);
                                                                });
                                                              });
                                                            },
                                                            child: Row(
                                                              children: <Widget>[
                                                                Padding(
                                                                  padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025),
                                                                  child: SvgPicture.asset(
                                                                    'images/polygon-network.svg',
                                                                    height: MediaQuery.of(context).size.width * 0.05,
                                                                    width: MediaQuery.of(context).size.width * 0.05,
                                                                    color: Colors.black,
                                                                  ),
                                                                ),
                                                                Text(
                                                                  "POLYGON",
                                                                  style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.017),
                                                                ),
                                                              ],
                                                            ),
                                                          ),
                                                        ),
                                                        Padding(
                                                          padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.0085, top: MediaQuery.of(context).size.height * 0.005),
                                                          child: InkWell(
                                                            onTap: () {
                                                              fetchCrypto().then((value) {
                                                                setState(() {
                                                                  _cryptoList.clear();
                                                                  _cryptoList.addAll(value.where((c) => c.network == _selectedNetwork).toList());
                                                                  setFieldSelection(_cryptoList[0], "btt", "BTTC", false);
                                                                });
                                                              });
                                                            },
                                                            child: Row(
                                                              children: <Widget>[
                                                                Padding(
                                                                  padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025),
                                                                  child: SvgPicture.asset(
                                                                    'images/btt-network.svg',
                                                                    height: MediaQuery.of(context).size.width * 0.05,
                                                                    width: MediaQuery.of(context).size.width * 0.05,
                                                                  ),
                                                                ),
                                                                Text(
                                                                  "BTTC",
                                                                  style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.017),
                                                                ),
                                                              ],
                                                            ),
                                                          ),
                                                        ),
                                                        Padding(
                                                          padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.0085, top: MediaQuery.of(context).size.height * 0.005),
                                                          child: InkWell(
                                                            onTap: () {
                                                              if (LoginController.apiService.tronAddress != null && LoginController.apiService.tronAddress != "") {
                                                                fetchCrypto().then((value) {
                                                                  setState(() {
                                                                    _cryptoList.clear();
                                                                    _cryptoList.addAll(value.where((c) => c.network == _selectedNetwork).toList());
                                                                    setFieldSelection(_cryptoList[0], "tron", "TRON", false);
                                                                  });
                                                                });
                                                              } else {
                                                                setState(() {
                                                                  _walletAddress.text = "";
                                                                  _walletPattern = false;
                                                                  _walletFieldRequired = false;
                                                                });
                                                                createTronWalletUI(context);
                                                              }
                                                            },
                                                            child: Row(
                                                              children: <Widget>[
                                                                Padding(
                                                                  padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025),
                                                                  child: SvgPicture.asset(
                                                                    'images/wallet-tron.svg',
                                                                    height: MediaQuery.of(context).size.width * 0.05,
                                                                    width: MediaQuery.of(context).size.width * 0.05,
                                                                    color: Colors.black,
                                                                  ),
                                                                ),
                                                                Text(
                                                                  "TRON",
                                                                  style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.017),
                                                                ),
                                                              ],
                                                            ),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  )
                                                : (_selectedPaymentMethod == "bep20"
                                                    ? Padding(
                                                        padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.012),
                                                        child: Column(
                                                          children: <Widget>[
                                                            Padding(
                                                              padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.0085),
                                                              child: InkWell(
                                                                onTap: () async {
                                                                  fetchCrypto().then((value) {
                                                                    setState(() {
                                                                      _cryptoList.clear();
                                                                      _cryptoList.addAll(value.where((c) => c.network == _selectedNetwork).toList());
                                                                      setFieldSelection(_cryptoList[0], "fiat", "BTC", false);
                                                                    });
                                                                  });
                                                                },
                                                                child: Row(
                                                                  children: <Widget>[
                                                                    Padding(
                                                                      padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025, bottom: MediaQuery.of(context).size.height * 0.002),
                                                                      child: SvgPicture.asset(
                                                                        "images/btc-icon.svg",
                                                                        height: MediaQuery.of(context).size.height * 0.025,
                                                                        width: MediaQuery.of(context).size.width * 0.025,
                                                                      ),
                                                                    ),
                                                                    Padding(
                                                                      padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.035, left: MediaQuery.of(context).size.width * 0.012),
                                                                      child: Text(
                                                                        "BTC",
                                                                        style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.018),
                                                                      ),
                                                                    ),
                                                                  ],
                                                                ),
                                                              ),
                                                            ),
                                                            InkWell(
                                                              onTap: () {
                                                                fetchCrypto().then((value) {
                                                                  setState(() {
                                                                    _cryptoList.clear();
                                                                    _cryptoList.addAll(value.where((c) => c.network == _selectedNetwork).toList());
                                                                    setFieldSelection(_cryptoList[0], "erc20", "ERC20", false);
                                                                  });
                                                                });
                                                              },
                                                              child: Row(
                                                                children: <Widget>[
                                                                  Padding(
                                                                    padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.037, right: MediaQuery.of(context).size.width * 0.025),
                                                                    child: SvgPicture.asset(
                                                                      'images/testtest.svg',
                                                                      height: MediaQuery.of(context).size.width * 0.08,
                                                                      width: MediaQuery.of(context).size.width * 0.08,
                                                                      color: Colors.black,
                                                                    ),
                                                                  ),
                                                                  Padding(
                                                                    padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.0075),
                                                                    child: Text(
                                                                      "ERC20",
                                                                      style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.017),
                                                                    ),
                                                                  ),
                                                                ],
                                                              ),
                                                            ),
                                                            Padding(
                                                              padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.0085, top: MediaQuery.of(context).size.height * 0.0085),
                                                              child: InkWell(
                                                                onTap: () {
                                                                  fetchCrypto().then((value) {
                                                                    setState(() {
                                                                      _cryptoList.clear();
                                                                      _cryptoList.addAll(value.where((c) => c.network == _selectedNetwork).toList());
                                                                      setFieldSelection(_cryptoList[0], "polygon", "POLYGON", false);
                                                                    });
                                                                  });
                                                                },
                                                                child: Row(
                                                                  children: <Widget>[
                                                                    Padding(
                                                                      padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025),
                                                                      child: SvgPicture.asset(
                                                                        'images/polygon-network.svg',
                                                                        height: MediaQuery.of(context).size.width * 0.05,
                                                                        width: MediaQuery.of(context).size.width * 0.05,
                                                                        color: Colors.black,
                                                                      ),
                                                                    ),
                                                                    Text(
                                                                      "POLYGON",
                                                                      style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.017),
                                                                    ),
                                                                  ],
                                                                ),
                                                              ),
                                                            ),
                                                            Padding(
                                                              padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.0085, top: MediaQuery.of(context).size.height * 0.005),
                                                              child: InkWell(
                                                                onTap: () {
                                                                  fetchCrypto().then((value) {
                                                                    setState(() {
                                                                      _cryptoList.clear();
                                                                      _cryptoList.addAll(value.where((c) => c.network == _selectedNetwork).toList());
                                                                      setFieldSelection(_cryptoList[0], "btt", "BTTC", false);
                                                                    });
                                                                  });
                                                                },
                                                                child: Row(
                                                                  children: <Widget>[
                                                                    Padding(
                                                                      padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025),
                                                                      child: SvgPicture.asset(
                                                                        'images/btt-network.svg',
                                                                        height: MediaQuery.of(context).size.width * 0.05,
                                                                        width: MediaQuery.of(context).size.width * 0.05,
                                                                      ),
                                                                    ),
                                                                    Text(
                                                                      "BTTC",
                                                                      style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.017),
                                                                    ),
                                                                  ],
                                                                ),
                                                              ),
                                                            ),
                                                            Padding(
                                                              padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.0085, top: MediaQuery.of(context).size.height * 0.005),
                                                              child: InkWell(
                                                                onTap: () {
                                                                  if (LoginController.apiService.tronAddress != null && LoginController.apiService.tronAddress != "") {
                                                                    fetchCrypto().then((value) {
                                                                      setState(() {
                                                                        _cryptoList.clear();
                                                                        _cryptoList.addAll(value.where((c) => c.network == _selectedNetwork).toList());
                                                                        setFieldSelection(_cryptoList[0], "tron", "TRON", false);
                                                                      });
                                                                    });
                                                                  } else {
                                                                    setState(() {
                                                                      _walletPattern = false;
                                                                      _walletFieldRequired = false;
                                                                      _walletAddress.text = "";
                                                                    });
                                                                    createTronWalletUI(context);
                                                                  }
                                                                },
                                                                child: Row(
                                                                  children: <Widget>[
                                                                    Padding(
                                                                      padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025),
                                                                      child: SvgPicture.asset(
                                                                        'images/wallet-tron.svg',
                                                                        height: MediaQuery.of(context).size.width * 0.05,
                                                                        width: MediaQuery.of(context).size.width * 0.05,
                                                                        color: Colors.black,
                                                                      ),
                                                                    ),
                                                                    Text(
                                                                      "TRON",
                                                                      style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.017),
                                                                    ),
                                                                  ],
                                                                ),
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      )
                                                    : (_selectedPaymentMethod == "polygon"
                                                        ? Padding(
                                                            padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.012, left: MediaQuery.of(context).size.width * 0.0085),
                                                            child: Column(
                                                              children: <Widget>[
                                                                Padding(
                                                                  padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.0085),
                                                                  child: InkWell(
                                                                    onTap: () async {
                                                                      fetchCrypto().then((value) {
                                                                        setState(() {
                                                                          _cryptoList.clear();
                                                                          _cryptoList.addAll(value.where((c) => c.network == _selectedNetwork).toList());
                                                                          setFieldSelection(_cryptoList[0], "fiat", "BTC", false);
                                                                        });
                                                                      });
                                                                    },
                                                                    child: Row(
                                                                      children: <Widget>[
                                                                        Padding(
                                                                          padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025, bottom: MediaQuery.of(context).size.height * 0.002),
                                                                          child: SvgPicture.asset(
                                                                            "images/btc-icon.svg",
                                                                            height: MediaQuery.of(context).size.height * 0.025,
                                                                            width: MediaQuery.of(context).size.width * 0.025,
                                                                          ),
                                                                        ),
                                                                        Padding(
                                                                          padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.035, left: MediaQuery.of(context).size.width * 0.0075),
                                                                          child: Text(
                                                                            "BTC",
                                                                            style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.018),
                                                                          ),
                                                                        ),
                                                                      ],
                                                                    ),
                                                                  ),
                                                                ),
                                                                InkWell(
                                                                  onTap: () {
                                                                    fetchCrypto().then((value) {
                                                                      setState(() {
                                                                        _cryptoList.clear();
                                                                        _cryptoList.addAll(value.where((c) => c.network == _selectedNetwork).toList());
                                                                        setFieldSelection(_cryptoList[0], "bep20", "BEP20", false);
                                                                      });
                                                                    });
                                                                  },
                                                                  child: Row(
                                                                    children: <Widget>[
                                                                      Padding(
                                                                        padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025),
                                                                        child: SvgPicture.asset(
                                                                          'images/bep.svg',
                                                                          height: MediaQuery.of(context).size.width * 0.05,
                                                                          width: MediaQuery.of(context).size.width * 0.05,
                                                                          color: Colors.black,
                                                                        ),
                                                                      ),
                                                                      Text(
                                                                        "BEP20",
                                                                        style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.017),
                                                                      ),
                                                                    ],
                                                                  ),
                                                                ),
                                                                InkWell(
                                                                  onTap: () {
                                                                    fetchCrypto().then((value) {
                                                                      setState(() {
                                                                        _cryptoList.clear();
                                                                        _cryptoList.addAll(value.where((c) => c.network == _selectedNetwork).toList());
                                                                        setFieldSelection(_cryptoList[0], "erc20", "ERC20", false);
                                                                      });
                                                                    });
                                                                  },
                                                                  child: Row(
                                                                    children: <Widget>[
                                                                      Padding(
                                                                        padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.037, right: MediaQuery.of(context).size.width * 0.025),
                                                                        child: SvgPicture.asset(
                                                                          'images/testtest.svg',
                                                                          height: MediaQuery.of(context).size.width * 0.08,
                                                                          width: MediaQuery.of(context).size.width * 0.08,
                                                                          color: Colors.black,
                                                                        ),
                                                                      ),
                                                                      Padding(
                                                                        padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.0035),
                                                                        child: Text(
                                                                          "ERC20",
                                                                          style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.017),
                                                                        ),
                                                                      ),
                                                                    ],
                                                                  ),
                                                                ),
                                                                Padding(
                                                                  padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.0085, top: MediaQuery.of(context).size.height * 0.005),
                                                                  child: InkWell(
                                                                    onTap: () {
                                                                      fetchCrypto().then((value) {
                                                                        setState(() {
                                                                          _cryptoList.clear();
                                                                          _cryptoList.addAll(value.where((c) => c.network == _selectedNetwork).toList());
                                                                          setFieldSelection(_cryptoList[0], "btt", "BTTC", false);
                                                                        });
                                                                      });
                                                                    },
                                                                    child: Row(
                                                                      children: <Widget>[
                                                                        Padding(
                                                                          padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025),
                                                                          child: SvgPicture.asset(
                                                                            'images/btt-network.svg',
                                                                            height: MediaQuery.of(context).size.width * 0.05,
                                                                            width: MediaQuery.of(context).size.width * 0.05,
                                                                          ),
                                                                        ),
                                                                        Text(
                                                                          "BTTC",
                                                                          style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.017),
                                                                        ),
                                                                      ],
                                                                    ),
                                                                  ),
                                                                ),
                                                                Padding(
                                                                  padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.0085, top: MediaQuery.of(context).size.height * 0.005),
                                                                  child: InkWell(
                                                                    onTap: () {
                                                                      if (LoginController.apiService.tronAddress != null && LoginController.apiService.tronAddress != "") {
                                                                        fetchCrypto().then((value) {
                                                                          setState(() {
                                                                            _cryptoList.clear();
                                                                            _cryptoList.addAll(value.where((c) => c.network == _selectedNetwork).toList());
                                                                            setFieldSelection(_cryptoList[0], "tron", "TRON", false);
                                                                          });
                                                                        });
                                                                      } else {
                                                                        setState(() {
                                                                          _walletPattern = false;
                                                                          _walletFieldRequired = false;
                                                                          _walletAddress.text = "";
                                                                        });
                                                                        createTronWalletUI(context);
                                                                      }
                                                                    },
                                                                    child: Row(
                                                                      children: <Widget>[
                                                                        Padding(
                                                                          padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025),
                                                                          child: SvgPicture.asset(
                                                                            'images/wallet-tron.svg',
                                                                            height: MediaQuery.of(context).size.width * 0.05,
                                                                            width: MediaQuery.of(context).size.width * 0.05,
                                                                            color: Colors.black,
                                                                          ),
                                                                        ),
                                                                        Text(
                                                                          "TRON",
                                                                          style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.017),
                                                                        ),
                                                                      ],
                                                                    ),
                                                                  ),
                                                                ),
                                                              ],
                                                            ),
                                                          )
                                                        : (_selectedPaymentMethod == "btt"
                                                            ? Padding(
                                                                padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.012),
                                                                child: Column(
                                                                  children: <Widget>[
                                                                    Padding(
                                                                      padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.0085),
                                                                      child: InkWell(
                                                                        onTap: () async {
                                                                          fetchCrypto().then((value) {
                                                                            setState(() {
                                                                              _cryptoList.clear();
                                                                              _cryptoList.addAll(value.where((c) => c.network == _selectedNetwork).toList());
                                                                              setFieldSelection(_cryptoList[0], "fiat", "BTC", false);
                                                                            });
                                                                          });
                                                                        },
                                                                        child: Row(
                                                                          children: <Widget>[
                                                                            Padding(
                                                                              padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025, bottom: MediaQuery.of(context).size.height * 0.002),
                                                                              child: SvgPicture.asset(
                                                                                "images/btc-icon.svg",
                                                                                height: MediaQuery.of(context).size.height * 0.025,
                                                                                width: MediaQuery.of(context).size.width * 0.025,
                                                                              ),
                                                                            ),
                                                                            Padding(
                                                                              padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.035, left: MediaQuery.of(context).size.width * 0.01),
                                                                              child: Text(
                                                                                "BTC",
                                                                                style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.018),
                                                                              ),
                                                                            ),
                                                                          ],
                                                                        ),
                                                                      ),
                                                                    ),
                                                                    InkWell(
                                                                      onTap: () {
                                                                        fetchCrypto().then((value) {
                                                                          setState(() {
                                                                            _cryptoList.clear();
                                                                            _cryptoList.addAll(value.where((c) => c.network == _selectedNetwork).toList());
                                                                            setFieldSelection(_cryptoList[0], "bep20", "BEP20", false);
                                                                          });
                                                                        });
                                                                      },
                                                                      child: Row(
                                                                        children: <Widget>[
                                                                          Padding(
                                                                            padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025),
                                                                            child: SvgPicture.asset(
                                                                              'images/bep.svg',
                                                                              height: MediaQuery.of(context).size.width * 0.05,
                                                                              width: MediaQuery.of(context).size.width * 0.05,
                                                                              color: Colors.black,
                                                                            ),
                                                                          ),
                                                                          SizedBox(width: MediaQuery.of(context).size.width * 0.0035),
                                                                          Text(
                                                                            "BEP20",
                                                                            style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.017),
                                                                          ),
                                                                        ],
                                                                      ),
                                                                    ),
                                                                    InkWell(
                                                                      onTap: () {
                                                                        fetchCrypto().then((value) {
                                                                          setState(() {
                                                                            _cryptoList.clear();
                                                                            _cryptoList.addAll(value.where((c) => c.network == _selectedNetwork).toList());
                                                                            setFieldSelection(_cryptoList[0], "erc20", "ERC20", false);
                                                                          });
                                                                        });
                                                                      },
                                                                      child: Row(
                                                                        children: <Widget>[
                                                                          Padding(
                                                                            padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.037, right: MediaQuery.of(context).size.width * 0.025),
                                                                            child: SvgPicture.asset(
                                                                              'images/testtest.svg',
                                                                              height: MediaQuery.of(context).size.width * 0.08,
                                                                              width: MediaQuery.of(context).size.width * 0.08,
                                                                              color: Colors.black,
                                                                            ),
                                                                          ),
                                                                          Padding(
                                                                            padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.0035),
                                                                            child: Text(
                                                                              "ERC20",
                                                                              style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.017),
                                                                            ),
                                                                          ),
                                                                        ],
                                                                      ),
                                                                    ),
                                                                    Padding(
                                                                      padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.0085, top: MediaQuery.of(context).size.height * 0.0075),
                                                                      child: InkWell(
                                                                        onTap: () {
                                                                          fetchCrypto().then((value) {
                                                                            setState(() {
                                                                              _cryptoList.clear();
                                                                              _cryptoList.addAll(value.where((c) => c.network == _selectedNetwork).toList());
                                                                              setFieldSelection(_cryptoList[0], "polygon", "POLYGON", false);
                                                                            });
                                                                          });
                                                                        },
                                                                        child: Row(
                                                                          children: <Widget>[
                                                                            Padding(
                                                                              padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025),
                                                                              child: SvgPicture.asset(
                                                                                'images/polygon-network.svg',
                                                                                height: MediaQuery.of(context).size.width * 0.05,
                                                                                width: MediaQuery.of(context).size.width * 0.05,
                                                                                color: Colors.black,
                                                                              ),
                                                                            ),
                                                                            Text(
                                                                              "POLYGON",
                                                                              style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.017),
                                                                            ),
                                                                          ],
                                                                        ),
                                                                      ),
                                                                    ),
                                                                    Padding(
                                                                      padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.0085, top: MediaQuery.of(context).size.height * 0.005),
                                                                      child: InkWell(
                                                                        onTap: () {
                                                                          if (LoginController.apiService.tronAddress != null && LoginController.apiService.tronAddress != "") {
                                                                            fetchCrypto().then((value) {
                                                                              setState(() {
                                                                                _cryptoList.clear();
                                                                                _cryptoList.addAll(value.where((c) => c.network == _selectedNetwork).toList());
                                                                                setFieldSelection(_cryptoList[0], "tron", "TRON", false);
                                                                              });
                                                                            });
                                                                          } else {
                                                                            setState(() {
                                                                              _walletPattern = false;
                                                                              _walletFieldRequired = false;
                                                                              _walletAddress.text = "";
                                                                            });
                                                                            createTronWalletUI(context);
                                                                          }
                                                                        },
                                                                        child: Row(
                                                                          children: <Widget>[
                                                                            Padding(
                                                                              padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025),
                                                                              child: SvgPicture.asset(
                                                                                'images/wallet-tron.svg',
                                                                                height: MediaQuery.of(context).size.width * 0.05,
                                                                                width: MediaQuery.of(context).size.width * 0.05,
                                                                                color: Colors.black,
                                                                              ),
                                                                            ),
                                                                            Text(
                                                                              "TRON",
                                                                              style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.017),
                                                                            ),
                                                                          ],
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ],
                                                                ),
                                                              )
                                                            : (_selectedPaymentMethod == "tron"
                                                                ? Padding(
                                                                    padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.012),
                                                                    child: Column(
                                                                      children: <Widget>[
                                                                        Padding(
                                                                          padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.0085),
                                                                          child: InkWell(
                                                                            onTap: () {
                                                                              fetchCrypto().then((value) {
                                                                                setState(() {
                                                                                  _cryptoList.clear();
                                                                                  _cryptoList.addAll(value.where((c) => c.network == _selectedNetwork).toList());
                                                                                  setFieldSelection(_cryptoList[0], "fiat", "BTC", false);
                                                                                });
                                                                              });
                                                                            },
                                                                            child: Row(
                                                                              children: <Widget>[
                                                                                Padding(
                                                                                  padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025, bottom: MediaQuery.of(context).size.height * 0.002),
                                                                                  child: SvgPicture.asset(
                                                                                    "images/btc-icon.svg",
                                                                                    height: MediaQuery.of(context).size.height * 0.025,
                                                                                    width: MediaQuery.of(context).size.width * 0.025,
                                                                                  ),
                                                                                ),
                                                                                Padding(
                                                                                  padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.035, left: MediaQuery.of(context).size.width * 0.01),
                                                                                  child: Text(
                                                                                    "BTC",
                                                                                    style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.018),
                                                                                  ),
                                                                                ),
                                                                              ],
                                                                            ),
                                                                          ),
                                                                        ),
                                                                        InkWell(
                                                                          onTap: () {
                                                                            fetchCrypto().then((value) {
                                                                              setState(() {
                                                                                _cryptoList.clear();
                                                                                _cryptoList.addAll(value.where((c) => c.network == _selectedNetwork).toList());
                                                                                setFieldSelection(_cryptoList[0], "bep20", "BEP20", false);
                                                                              });
                                                                            });
                                                                          },
                                                                          child: Row(
                                                                            children: <Widget>[
                                                                              Padding(
                                                                                padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025),
                                                                                child: SvgPicture.asset(
                                                                                  'images/bep.svg',
                                                                                  height: MediaQuery.of(context).size.width * 0.05,
                                                                                  width: MediaQuery.of(context).size.width * 0.05,
                                                                                  color: Colors.black,
                                                                                ),
                                                                              ),
                                                                              SizedBox(width: MediaQuery.of(context).size.width * 0.0035),
                                                                              Text(
                                                                                "BEP20",
                                                                                style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.017),
                                                                              ),
                                                                            ],
                                                                          ),
                                                                        ),
                                                                        Padding(
                                                                          padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.0085, top: MediaQuery.of(context).size.height * 0.0075),
                                                                          child: InkWell(
                                                                            onTap: () {
                                                                              fetchCrypto().then((value) {
                                                                                setState(() {
                                                                                  _cryptoList.clear();
                                                                                  _cryptoList.addAll(value.where((c) => c.network == _selectedNetwork).toList());
                                                                                  setFieldSelection(_cryptoList[0], "polygon", "POLYGON", false);
                                                                                });
                                                                              });
                                                                            },
                                                                            child: Row(
                                                                              children: <Widget>[
                                                                                Padding(
                                                                                  padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025),
                                                                                  child: SvgPicture.asset(
                                                                                    'images/polygon-network.svg',
                                                                                    height: MediaQuery.of(context).size.width * 0.05,
                                                                                    width: MediaQuery.of(context).size.width * 0.05,
                                                                                    color: Colors.black,
                                                                                  ),
                                                                                ),
                                                                                Text(
                                                                                  "POLYGON",
                                                                                  style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.017),
                                                                                ),
                                                                              ],
                                                                            ),
                                                                          ),
                                                                        ),
                                                                        Padding(
                                                                          padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.0085, top: MediaQuery.of(context).size.height * 0.005),
                                                                          child: InkWell(
                                                                            onTap: () {
                                                                              fetchCrypto().then((value) {
                                                                                setState(() {
                                                                                  _cryptoList.clear();
                                                                                  _cryptoList.addAll(value.where((c) => c.network == _selectedNetwork).toList());
                                                                                  setFieldSelection(_cryptoList[0], "btt", "BTTC", false);
                                                                                });
                                                                              });
                                                                            },
                                                                            child: Row(
                                                                              children: <Widget>[
                                                                                Padding(
                                                                                  padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025),
                                                                                  child: SvgPicture.asset(
                                                                                    'images/btt-network.svg',
                                                                                    height: MediaQuery.of(context).size.width * 0.05,
                                                                                    width: MediaQuery.of(context).size.width * 0.05,
                                                                                  ),
                                                                                ),
                                                                                Text(
                                                                                  "BTTC",
                                                                                  style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.017),
                                                                                ),
                                                                              ],
                                                                            ),
                                                                          ),
                                                                        ),
                                                                        InkWell(
                                                                          onTap: () {
                                                                            fetchCrypto().then((value) {
                                                                              setState(() {
                                                                                _cryptoList.clear();
                                                                                _cryptoList.addAll(value.where((c) => c.network == _selectedNetwork).toList());
                                                                                setFieldSelection(_cryptoList[0], "erc20", "ERC20", false);
                                                                              });
                                                                            });
                                                                          },
                                                                          child: Row(
                                                                            children: <Widget>[
                                                                              Padding(
                                                                                padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.037, right: MediaQuery.of(context).size.width * 0.025),
                                                                                child: SvgPicture.asset(
                                                                                  'images/testtest.svg',
                                                                                  height: MediaQuery.of(context).size.width * 0.08,
                                                                                  width: MediaQuery.of(context).size.width * 0.08,
                                                                                  color: Colors.black,
                                                                                ),
                                                                              ),
                                                                              Padding(
                                                                                padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.0035),
                                                                                child: Text(
                                                                                  "ERC20",
                                                                                  style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.017),
                                                                                ),
                                                                              ),
                                                                            ],
                                                                          ),
                                                                        ),
                                                                      ],
                                                                    ),
                                                                  )
                                                                : Padding(
                                                                    padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.012),
                                                                    child: Column(
                                                                      children: <Widget>[
                                                                        Padding(
                                                                          padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.0085),
                                                                          child: InkWell(
                                                                            onTap: () {
                                                                              fetchCrypto().then((value) {
                                                                                setState(() {
                                                                                  _cryptoList.clear();
                                                                                  _cryptoList.addAll(value.where((c) => c.network == _selectedNetwork).toList());
                                                                                  setFieldSelection(_cryptoList[0], "fiat", "BTC", false);
                                                                                });
                                                                              });
                                                                            },
                                                                            child: Row(
                                                                              children: <Widget>[
                                                                                Padding(
                                                                                  padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025, bottom: MediaQuery.of(context).size.height * 0.002),
                                                                                  child: SvgPicture.asset(
                                                                                    "images/btc-icon.svg",
                                                                                    height: MediaQuery.of(context).size.height * 0.025,
                                                                                    width: MediaQuery.of(context).size.width * 0.025,
                                                                                  ),
                                                                                ),
                                                                                Padding(
                                                                                  padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.035, left: MediaQuery.of(context).size.width * 0.01),
                                                                                  child: Text(
                                                                                    "BTC",
                                                                                    style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.018),
                                                                                  ),
                                                                                ),
                                                                              ],
                                                                            ),
                                                                          ),
                                                                        ),
                                                                        InkWell(
                                                                          onTap: () {
                                                                            fetchCrypto().then((value) {
                                                                              setState(() {
                                                                                _cryptoList.clear();
                                                                                _cryptoList.addAll(value.where((c) => c.network == _selectedNetwork).toList());
                                                                                setFieldSelection(_cryptoList[0], "bep20", "BEP20", false);
                                                                              });
                                                                            });
                                                                          },
                                                                          child: Row(
                                                                            children: <Widget>[
                                                                              Padding(
                                                                                padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025),
                                                                                child: SvgPicture.asset(
                                                                                  'images/bep.svg',
                                                                                  height: MediaQuery.of(context).size.width * 0.05,
                                                                                  width: MediaQuery.of(context).size.width * 0.05,
                                                                                  color: Colors.black,
                                                                                ),
                                                                              ),
                                                                              SizedBox(width: MediaQuery.of(context).size.width * 0.0035),
                                                                              Text(
                                                                                "BEP20",
                                                                                style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.017),
                                                                              ),
                                                                            ],
                                                                          ),
                                                                        ),
                                                                        Padding(
                                                                          padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.0085, top: MediaQuery.of(context).size.height * 0.0075),
                                                                          child: InkWell(
                                                                            onTap: () {
                                                                              fetchCrypto().then((value) {
                                                                                setState(() {
                                                                                  _cryptoList.clear();
                                                                                  _cryptoList.addAll(value.where((c) => c.network == _selectedNetwork).toList());
                                                                                  setFieldSelection(_cryptoList[0], "polygon", "POLYGON", false);
                                                                                });
                                                                              });
                                                                            },
                                                                            child: Row(
                                                                              children: <Widget>[
                                                                                Padding(
                                                                                  padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025),
                                                                                  child: SvgPicture.asset(
                                                                                    'images/polygon-network.svg',
                                                                                    height: MediaQuery.of(context).size.width * 0.05,
                                                                                    width: MediaQuery.of(context).size.width * 0.05,
                                                                                    color: Colors.black,
                                                                                  ),
                                                                                ),
                                                                                Text(
                                                                                  "POLYGON",
                                                                                  style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.017),
                                                                                ),
                                                                              ],
                                                                            ),
                                                                          ),
                                                                        ),
                                                                        Padding(
                                                                          padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.0085, top: MediaQuery.of(context).size.height * 0.005),
                                                                          child: InkWell(
                                                                            onTap: () {
                                                                              fetchCrypto().then((value) {
                                                                                setState(() {
                                                                                  _cryptoList.clear();
                                                                                  _cryptoList.addAll(value.where((c) => c.network == _selectedNetwork).toList());
                                                                                  setFieldSelection(_cryptoList[0], "btt", "BTTC", false);
                                                                                });
                                                                              });
                                                                            },
                                                                            child: Row(
                                                                              children: <Widget>[
                                                                                Padding(
                                                                                  padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025),
                                                                                  child: SvgPicture.asset(
                                                                                    'images/btt-network.svg',
                                                                                    height: MediaQuery.of(context).size.width * 0.05,
                                                                                    width: MediaQuery.of(context).size.width * 0.05,
                                                                                  ),
                                                                                ),
                                                                                Text(
                                                                                  "BTTC",
                                                                                  style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.017),
                                                                                ),
                                                                              ],
                                                                            ),
                                                                          ),
                                                                        ),
                                                                        Padding(
                                                                          padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.0085, top: MediaQuery.of(context).size.height * 0.005),
                                                                          child: InkWell(
                                                                            onTap: () {
                                                                              if (LoginController.apiService.tronAddress != null && LoginController.apiService.tronAddress != "") {
                                                                                fetchCrypto().then((value) {
                                                                                  setState(() {
                                                                                    _cryptoList.clear();
                                                                                    _cryptoList.addAll(value.where((c) => c.network == _selectedNetwork).toList());
                                                                                    setFieldSelection(_cryptoList[0], "tron", "TRON", false);
                                                                                  });
                                                                                });
                                                                              } else {
                                                                                setState(() {
                                                                                  _walletPattern = false;
                                                                                  _walletFieldRequired = false;
                                                                                  _walletAddress.text = "";
                                                                                });
                                                                                createTronWalletUI(context);
                                                                              }
                                                                            },
                                                                            child: Row(
                                                                              children: <Widget>[
                                                                                Padding(
                                                                                  padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025),
                                                                                  child: SvgPicture.asset(
                                                                                    'images/wallet-tron.svg',
                                                                                    height: MediaQuery.of(context).size.width * 0.05,
                                                                                    width: MediaQuery.of(context).size.width * 0.05,
                                                                                    color: Colors.black,
                                                                                  ),
                                                                                ),
                                                                                Text(
                                                                                  "TRON",
                                                                                  style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.017),
                                                                                ),
                                                                              ],
                                                                            ),
                                                                          ),
                                                                        ),
                                                                      ],
                                                                    ),
                                                                  )))))),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        )
      ],
    );
  }

  Widget secondStepFlowWidget() {
    return Column(
      children: <Widget>[
        Container(
          height: MediaQuery.of(context).size.height * 0.14,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.05, 0, 0, MediaQuery.of(context).size.height * 0.032),
                child: Text(
                  "Send",
                  style: GoogleFonts.poppins(
                    fontSize: MediaQuery.of(context).size.height * 0.035,
                    fontWeight: FontWeight.w700,
                    color: Colors.white,
                  ),
                ),
              ),
              Spacer(),
              Padding(
                padding: EdgeInsets.fromLTRB(0, 0, MediaQuery.of(context).size.width * 0.037, MediaQuery.of(context).size.height * 0.021),
                child: InkWell(
                  onTap: () {
                    if (_enoughFees) {
                      setState(() {
                        _firstStepSendFlow = true;
                        listElementClicked = false;
                        _secondStepSendFlow = false;
                        _transactionPwd.text = "";
                        _errorMessageTransaction = false;
                        _transactionPasswordField = false;
                        _messageTransactionExist = false;
                      });
                    } else {
                      Provider.of<WalletController>(context, listen: false).selectedIndex = 2;
                      Navigator.pushReplacementNamed(context, "/welcomescreen");
                    }
                  },
                  child: Text(
                    "< Back",
                    style: GoogleFonts.poppins(color: Colors.white, letterSpacing: 1.2, fontWeight: FontWeight.w600, fontSize: MediaQuery.of(context).size.height * 0.022),
                  ),
                ),
              )
            ],
          ),
        ),
        Expanded(
          child: Container(
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(MediaQuery.of(context).size.width * 0.08),
                  topRight: Radius.circular(MediaQuery.of(context).size.width * 0.08),
                )),
            child: Form(
              key: _formKeyPassword,
              child: Column(
                children: <Widget>[
                  Center(
                    child: Padding(
                      padding: EdgeInsets.fromLTRB(0, MediaQuery.of(context).size.height * 0.02, 0, 0),
                      child: new SvgPicture.asset(
                        'images/send-icon-confirmation.svg',
                        height: MediaQuery.of(context).size.height * 0.11,
                        width: MediaQuery.of(context).size.height * 0.11,
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.fromLTRB(0, MediaQuery.of(context).size.height * 0.018, 0, 0),
                    child: Text(
                      "You are about to send",
                      style: GoogleFonts.poppins(color: Color(0xFF1F2337), fontWeight: FontWeight.w700, fontSize: MediaQuery.of(context).size.height * 0.025, letterSpacing: 1),
                    ),
                  ),
                  Center(
                    child: Padding(
                      padding: EdgeInsets.fromLTRB(0, MediaQuery.of(context).size.height * 0.0075, 0, 0),
                      child: Text(
                        "\$${_dollarBalance.text.replaceAll("\$", "")}",
                        style: GoogleFonts.poppins(
                          color: Color(0xFF1F2337),
                          fontWeight: FontWeight.w700,
                          fontSize: MediaQuery.of(context).size.height * 0.04,
                        ),
                      ),
                    ),
                  ),
                  Text("≈${_cryptoBalance.text} ${_selectedCrypto.toLowerCase().contains("satt") ? "SaTT" : _selectedCrypto}",
                      style: GoogleFonts.poppins(
                        color: Color(0xFF75758F),
                        fontWeight: FontWeight.w700,
                        fontSize: MediaQuery.of(context).size.height * 0.03,
                      )),
                  Padding(
                    padding: EdgeInsets.fromLTRB(0, _enoughFees ? MediaQuery.of(context).size.height * 0.08 : MediaQuery.of(context).size.height * 0.08, 0, MediaQuery.of(context).size.width * 0.033),
                    child: (_selectedNetwork == "ERC20")
                        ? Text(
                            "Fees : ${formatCryptoBalance(feesETHQuantity, "")} ETH ( ≈ ${formatDollarBalance(feesERC20)} )",
                            style: GoogleFonts.poppins(fontWeight: FontWeight.w500, fontSize: MediaQuery.of(context).size.height * 0.016, letterSpacing: 1.2, color: Color(0xFF75758F)),
                          )
                        : ((_selectedNetwork == "BEP20")
                            ? Text(
                                "Fees : ${formatCryptoBalance(feesBNBQuantity, "")} BNB ( ≈ ${formatDollarBalance(feesBEP20)} )",
                                style: GoogleFonts.poppins(fontWeight: FontWeight.w500, fontSize: MediaQuery.of(context).size.height * 0.016, letterSpacing: 1.2, color: Color(0xFF75758F)),
                              )
                            : (_selectedNetwork == "POLYGON"
                                ? Text(
                                    "Fees : ${formatCryptoBalance(feesMATICQuantity, "")} MATIC ( ≈ ${formatDollarBalance(feesMATIC)} )",
                                    style: GoogleFonts.poppins(fontWeight: FontWeight.w500, fontSize: MediaQuery.of(context).size.height * 0.013, letterSpacing: 1.2, color: Color(0xFF75758F)),
                                  )
                                : (_selectedNetwork == "BTTC"
                                    ? Text(
                                        "Fees : ${formatCryptoBalance(feesBTTQuantity, "")} BTT ( ≈  ${formatDollarBalance(feesBTT)} )",
                                        style: GoogleFonts.poppins(fontWeight: FontWeight.w500, fontSize: MediaQuery.of(context).size.height * 0.013, letterSpacing: 1.2, color: Color(0xFF75758F)),
                                      )
                                    : (_selectedNetwork == "TRON"
                                        ? Text(
                                            "Fees : ${formatCryptoBalance(feesTRXQuantity, "")} TRX ( ≈ ${formatDollarBalance(feesTRX)} )",
                                            style: GoogleFonts.poppins(fontWeight: FontWeight.w600, fontSize: Platform.isIOS ? MediaQuery.of(context).size.width * 0.033 : MediaQuery.of(context).size.width * 0.028, color: Color(0XFF75758F)),
                                          )
                                        : Text(
                                            "BTC",
                                            style: GoogleFonts.poppins(fontWeight: FontWeight.w600, fontSize: Platform.isIOS ? MediaQuery.of(context).size.width * 0.033 : MediaQuery.of(context).size.width * 0.028, color: Color(0XFF75758F)),
                                          ))))),
                  ),
                  if (_enoughFees)
                    Column(
                      children: [
                        Container(
                          width: MediaQuery.of(context).size.width * 0.8,
                          child: TextFormField(
                            controller: _transactionPwd,
                            style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.height * 0.02),
                            onChanged: (value) {
                              if (value.length == 0) {
                                setState(() {
                                  _transactionPasswordField = false;
                                  _isActivatedButtonSend = false;
                                });
                              } else {
                                setState(() {
                                  _transactionPasswordField = true;
                                  _isActivatedButtonSend = true;
                                });
                              }
                            },
                            obscureText: _obscureText,
                            keyboardType: TextInputType.visiblePassword,
                            decoration: InputDecoration(
                              suffixIcon: Padding(
                                padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.025),
                                child: IconButton(
                                    onPressed: () {
                                      setState(() {
                                        _obscureText = !_obscureText;
                                      });
                                    },
                                    icon: _obscureText
                                        ? SvgPicture.asset(
                                            "images/visibility-icon-off.svg",
                                            height: MediaQuery.of(context).size.height * 0.02,
                                          )
                                        : SvgPicture.asset(
                                            "images/visibility-icon-on.svg",
                                            height: MediaQuery.of(context).size.height * 0.02,
                                          )),
                              ),
                              prefixIcon: Padding(
                                padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.02, MediaQuery.of(context).size.height * 0.005, MediaQuery.of(context).size.width * 0.032, MediaQuery.of(context).size.height * 0.005),
                                child: SvgPicture.asset(
                                  "images/keyIcon.svg",
                                  color: Colors.orange,
                                  height: MediaQuery.of(context).size.height * 0.035,
                                  width: MediaQuery.of(context).size.height * 0.035,
                                ),
                              ),
                              enabledBorder: OutlineInputBorder(borderRadius: new BorderRadius.circular(30.0), borderSide: BorderSide(color: Color(0xFFD6D6E8))),
                              focusedBorder: new OutlineInputBorder(
                                borderRadius: new BorderRadius.circular(30.0),
                                borderSide: BorderSide(color: Color(0xFFD6D6E8)),
                              ),
                              contentPadding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height * 0.013, horizontal: MediaQuery.of(context).size.width * 0.03),
                            ),
                          ),
                        ),
                        SizedBox(
                          child: _messageTransaction.toString().length > 0
                              ? Align(
                                  alignment: Alignment.bottomLeft,
                                  child: Padding(
                                    padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.15, top: MediaQuery.of(context).size.height * 0.005),
                                    child: Text(
                                      _messageTransaction.toString(),
                                      style: GoogleFonts.poppins(color: Color(0xFFf52079), fontSize: MediaQuery.of(context).size.width * 0.028, fontWeight: FontWeight.w500),
                                    ),
                                  ),
                                )
                              : null,
                        ),
                        Padding(
                          padding: EdgeInsets.fromLTRB(0, MediaQuery.of(context).size.height * 0.018, 0, 0),
                          child: (_transactionPasswordField)
                              ? SizedBox(
                                  height: MediaQuery.of(context).size.height * 0.052,
                                  width: MediaQuery.of(context).size.width * 0.8,
                                  child: ElevatedButton(
                                    style: ButtonStyle(
                                      backgroundColor: MaterialStateProperty.all(
                                        _isActivatedButtonSend ? Color(0xFF4048FF) : Color(0xFFF6F6FF),
                                      ),
                                      shape: MaterialStateProperty.all(
                                        RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.08),
                                        ),
                                      ),
                                    ),
                                    onPressed: _isActivatedButtonSend
                                        ? () async {
                                            try {
                                              setState(() {
                                                _isLoadingSend = true;
                                                _isActivatedButtonSend = false;
                                                _messageTransactionExist = false;
                                                _errorMessageTransaction = false;
                                              });

                                              var userLegalResult = await LoginController.apiService.getUserLegal(Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "");
                                              if (userLegalResult != "error" || userLegalResult != "Invalid Access Token") {
                                                List legal = userLegalResult;
                                                if (legal.length == 2 && legal[0]["validate"] == true && legal[0]["validate"] == true) {
                                                  if (LoginController.apiService.hasBiometric) {
                                                    await LoginController.apiService.authenticate();
                                                  }
                                                  Crypto cryptoSelected = Provider.of<WalletController>(context, listen: false).cryptos.firstWhere((element) => element.symbol == _selectedCrypto && element.network == _selectedNetwork);
                                                  var result = await LoginController.apiService.TransfertCrypto(
                                                    Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "",
                                                    cryptoSelected.network.toString().toLowerCase(),
                                                    cryptoSelected.contract.toString(),
                                                    cryptoSelected.symbol.toString(),
                                                    _walletAddress.text,
                                                    formatAmount(cryptoSelected.decimal ?? 18, _cryptoBalance.text),
                                                    _transactionPwd.text,
                                                  );

                                                  if (result["code"] == 200) {
                                                    setState(() {
                                                      _successSend = true;
                                                      _firstStepSendFlow = false;
                                                      _secondStepSendFlow = false;
                                                      _isLoadingSend = false;
                                                      _isActivatedButtonSend = true;
                                                      transactionHash = result["data"]["transactionHash"] ?? "";
                                                      _transactionPasswordField = false;
                                                    });
                                                  } else {
                                                    if (result["error"].contains("wrong password") || result["error"].contains("Invalid private key")) {
                                                      setState(() {
                                                        _messageTransaction = "Your password is wrong, please try again.";
                                                        _messageTransactionExist = true;
                                                        _isActivatedButtonSend = true;
                                                        _isLoadingSend = false;
                                                        _transactionPwd.text = "";
                                                        _transactionPasswordField = false;
                                                      });
                                                    } else if (result["error"].contains("insufficient funds for gas")) {
                                                      setState(() {
                                                        _messageTransaction = "";
                                                        _messageTransactionExist = true;
                                                        _isActivatedButtonSend = true;
                                                        _isLoadingSend = false;
                                                        _transactionPwd.text = "";
                                                        _transactionPasswordField = false;
                                                        _enoughFees = false;
                                                      });
                                                    } else {
                                                      setState(() {
                                                        _messageTransaction = "Network error, please try again later";
                                                        _messageTransactionExist = true;
                                                        _isActivatedButtonSend = true;
                                                        _isLoadingSend = false;
                                                        _transactionPwd.text = "";
                                                        _transactionPasswordField = false;
                                                      });
                                                    }
                                                  }
                                                } else {
                                                  showDialog(
                                                      context: context,
                                                      barrierDismissible: false,
                                                      builder: (BuildContext context) {
                                                        return StatefulBuilder(builder: (context, setState) {
                                                          return Dialog(
                                                            backgroundColor: Colors.transparent,
                                                            insetPadding: EdgeInsets.zero,
                                                            child: Container(
                                                              height: MediaQuery.of(context).size.height,
                                                              width: MediaQuery.of(context).size.width,
                                                              decoration: BoxDecoration(
                                                                color: Colors.white.withOpacity(0.8),
                                                              ),
                                                              child: Column(
                                                                children: <Widget>[
                                                                  Row(
                                                                    crossAxisAlignment: CrossAxisAlignment.center,
                                                                    mainAxisAlignment: MainAxisAlignment.start,
                                                                    children: [
                                                                      Spacer(),
                                                                      Spacer(),
                                                                      Padding(
                                                                        padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.01, top: MediaQuery.of(context).size.width * 0.06),
                                                                        child: IconButton(
                                                                          alignment: Alignment.topLeft,
                                                                          onPressed: () {
                                                                            setState(() {
                                                                              _messageTransaction = "";
                                                                              _messageTransactionExist = false;
                                                                              _isActivatedButtonSend = true;
                                                                              _isLoadingSend = false;
                                                                              _transactionPwd.text = "";
                                                                              _transactionPasswordField = false;
                                                                            });
                                                                            Navigator.of(context).pop();
                                                                          },
                                                                          splashColor: Colors.transparent,
                                                                          highlightColor: Colors.transparent,
                                                                          icon: Icon(
                                                                            Icons.close,
                                                                            size: MediaQuery.of(context).size.width * 0.05,
                                                                            color: Colors.black.withOpacity(0.5),
                                                                          ),
                                                                        ),
                                                                      ),
                                                                    ],
                                                                  ),
                                                                  Padding(
                                                                    padding: EdgeInsets.only(
                                                                      top: MediaQuery.of(context).size.height * 0.2,
                                                                    ),
                                                                    child: SvgPicture.asset("images/KYC.svg"),
                                                                  ),
                                                                  Padding(
                                                                    padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.03, bottom: MediaQuery.of(context).size.height * 0.023),
                                                                    child: Text(
                                                                      "Please verify your account",
                                                                      style: GoogleFonts.poppins(
                                                                        fontWeight: FontWeight.w700,
                                                                        fontStyle: FontStyle.normal,
                                                                        color: Color(0XFF1F2337),
                                                                        fontSize: MediaQuery.of(context).size.height * 0.025,
                                                                      ),
                                                                    ),
                                                                  ),
                                                                  Text(
                                                                    "To perform this transaction",
                                                                    style: GoogleFonts.poppins(color: Color(0XFF47526A), fontSize: MediaQuery.of(context).size.height * 0.017, fontWeight: FontWeight.w500),
                                                                  ),
                                                                  Text(
                                                                    "Proceed to KYC page",
                                                                    style: GoogleFonts.poppins(color: Color(0XFF47526A), fontSize: MediaQuery.of(context).size.height * 0.017, fontWeight: FontWeight.w500),
                                                                  ),
                                                                  Padding(
                                                                    padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.012),
                                                                    child: SizedBox(
                                                                      width: MediaQuery.of(context).size.width * 0.4,
                                                                      child: ElevatedButton(
                                                                        child: Text(
                                                                          "KYC",
                                                                          style: GoogleFonts.poppins(fontWeight: FontWeight.w900, fontSize: MediaQuery.of(context).size.height * 0.017),
                                                                        ),
                                                                        onPressed: () {
                                                                          gotoPageGlobal(context, KycPage());
                                                                        },
                                                                        style: ButtonStyle(
                                                                          backgroundColor: MaterialStateProperty.all(
                                                                            Color(0xFF4048FF),
                                                                          ),
                                                                          shape: MaterialStateProperty.all(
                                                                            RoundedRectangleBorder(
                                                                              borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.08),
                                                                            ),
                                                                          ),
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ),
                                                                ],
                                                              ),
                                                            ),
                                                          );
                                                        });
                                                      });
                                                }
                                              } else {
                                                displayDialog(context, "Error", "Something went wrong, please try again");
                                              }
                                            } catch (e) {
                                              setState(() {
                                                _messageTransaction = "Network error, please try again later";
                                                _messageTransactionExist = true;
                                                _isActivatedButtonSend = true;
                                                _isLoadingSend = false;
                                                _transactionPwd.text = "";
                                                _transactionPasswordField = false;
                                              });
                                            }
                                          }
                                        : null,
                                    child: !_isLoadingSend
                                        ? Row(
                                            children: [
                                              Spacer(),
                                              Text(
                                                "Send",
                                                style: GoogleFonts.poppins(
                                                  fontSize: MediaQuery.of(context).size.width * 0.045,
                                                  fontWeight: FontWeight.w600,
                                                ),
                                              ),
                                              Spacer(),
                                            ],
                                          )
                                        : SizedBox(
                                            height: MediaQuery.of(context).size.width * 0.032,
                                            width: MediaQuery.of(context).size.width * 0.032,
                                            child: CircularProgressIndicator(
                                              color: Color(0xFFADADC8),
                                            ),
                                          ),
                                  ),
                                )
                              : Container(
                                  height: MediaQuery.of(context).size.height * 0.052,
                                  width: MediaQuery.of(context).size.width * 0.8,
                                  decoration: BoxDecoration(borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.08), color: Color(0xFFF6F6FF), border: Border.all(color: Color(0xFFD6D6E8), width: 1)),
                                  child: Row(
                                    children: [
                                      Spacer(),
                                      Text(
                                        "Send",
                                        style: GoogleFonts.poppins(
                                          fontSize: MediaQuery.of(context).size.width * 0.045,
                                          fontWeight: FontWeight.w600,
                                          color: Color(0xFFADADC8),
                                        ),
                                      ),
                                      Spacer(),
                                    ],
                                  ),
                                ),
                        ),
                      ],
                    ),
                  if (!_enoughFees)
                    Column(
                      children: [
                        Text(
                            "Not enough gas.\nPlease get more ${_selectedNetwork == "ERC20" ? "ETH" : (_selectedNetwork == "POLYGON" ? "MATIC" : (_selectedNetwork == "BTTC" ? "BTT" : _selectedNetwork == "TRON" ? "TRON" : "BNB"))} before to make\nthis transaction.",
                            textAlign: TextAlign.center,
                            style: GoogleFonts.poppins(color: const Color(0XFFF52079), fontStyle: FontStyle.normal, fontWeight: FontWeight.w500, fontSize: MediaQuery.of(context).size.height * 0.016)),
                        SizedBox(height: MediaQuery.of(context).size.height * 0.02),
                        SizedBox(
                          width: MediaQuery.of(context).size.width * 0.8,
                          height: MediaQuery.of(context).size.height * 0.06,
                          child: ElevatedButton(
                              onPressed: () {
                                if (_selectedNetwork != "BTTC") {
                                  Provider.of<LoginController>(context, listen: false).tokenSymbol = _selectedNetwork == "ERC20" ? "ETH" : (_selectedNetwork == "POLYGON" ? "MATIC" : (_selectedNetwork == "BTTC" ? "BTT" : "BNB"));
                                  Provider.of<LoginController>(context, listen: false).tokenNetwork = _selectedNetwork;
                                  Provider.of<WalletController>(context, listen: false).selectedIndex = 7;
                                  Navigator.pushReplacementNamed(context, "/welcomescreen");
                                } else {
                                  _launchURLBuyBTT();
                                }
                              },
                              style: ButtonStyle(
                                backgroundColor: MaterialStateProperty.all(
                                  Colors.white,
                                ),
                                shape: MaterialStateProperty.all(
                                  RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.08),
                                  ),
                                ),
                              ),
                              child: Text(
                                  "Buy more ${_selectedNetwork == "ERC20" ? "Ethereum" : (_selectedNetwork == "POLYGON" ? "MATIC" : (_selectedNetwork == "BTTC" ? "BitTorrent" : _selectedNetwork == "TRON" ? "TRON" : "Binance Coin"))}",
                                  style: GoogleFonts.poppins(color: const Color(0XFF4048FF), fontWeight: FontWeight.w600, fontStyle: FontStyle.normal, fontSize: MediaQuery.of(context).size.height * 0.018))),
                        )
                      ],
                    )
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }

  setFieldSelection(Crypto crypto, String paymentMethod, String selectedNetwork, bool loadingNetwork) {
    _selectedCrypto = crypto.symbol.toString();
    _clickedCryptoContainer = false;
    _clickedContainerPaymentMethod = false;
    setCryptoSelection(crypto);
    _cryptoBalance.text = "";
    _dollarBalance.text = "";
    _walletAddress.text = "";
    _walletPattern = false;
    _walletFieldRequired = false;
    _selectedPaymentMethod = paymentMethod;
    _selectedNetwork = selectedNetwork;
    _isLoadingNetwork = loadingNetwork;
  }

  setCryptoSelection(Crypto crypto) {
    _selectedCrypto = crypto.symbol.toString();
    _selectedCryptoAddedToken = crypto.addedToken ?? false;
    _selectedCryptoPicUrl = crypto.picUrl.toString();
    _selectedCryptoName = crypto.name.toString();
    _selectedCryptoSymbol = crypto.symbol.toString();
  }

  filterCryptosByNetwork(String network) {
    fetchCrypto().then((value) {
      setState(() {
        _selectedNetwork = network;
        _cryptoList.clear();
        _cryptoList.addAll(value.where((c) => c.network == _selectedNetwork).toList());
      });
    });
  }

  Future save(String amount, String network, String pass, String symbole, String to, String smartContract) async {
    var userDetails = Provider.of<LoginController>(context, listen: false).userDetails;
    num decimal = _cryptoList.firstWhere((c) => c.symbol == _selectedCrypto).decimal ?? 18;
    amount = formatAmount(decimal, amount);
    var sendCryptoResult = await apiService.sendCrypto(userDetails?.userToken ?? "", amount, network, pass, decimal.toString(), symbole, to, smartContract);
    return sendCryptoResult;
  }

  formatAmount(num decimal, String amount) {
    amount = amount.replaceAll(",", '');
    var power = pow(10, decimal).toString();
    var amountNum = (BigDecimal.parse(amount) * BigDecimal.parse(power)).toString();
    var amountformatted = amountNum.indexOf('.') != -1 ? BigInt.parse(amountNum.substring(0, amountNum.indexOf('.'))) : BigInt.parse(amountNum);
    amount = amountformatted.toString();
    return amount;
  }

  String setTransactionLink(String selectedNetwork, transactionHash) {
    var network = selectedNetwork.toLowerCase();
    var result = "";
    var isPreprod = LoginController.apiService.baseURL.toString().toLowerCase().contains("preprod");
    if (network == "bep20") {
      result = isPreprod ? 'https://testnet.bscscan.com/tx/' + transactionHash : 'https://bscscan.com/tx/' + transactionHash;
    } else if (network == "erc20") {
      result = result = isPreprod ? 'https://Goerli.etherscan.io/tx/' + transactionHash : 'https://etherscan.io/tx/' + transactionHash;
    } else if (network == "tron") {
      result = result = isPreprod ? 'https://shasta.tronscan.org/#/transaction/' + transactionHash : 'https://tronscan.org/#/transaction/' + transactionHash;
    } else if (network == "bttc") {
      result = result = isPreprod ? 'https://testnet.bttcscan.com/tx/' + transactionHash : 'https://bttcscan.com/tx/' + transactionHash;
    } else if (network == "polygon") {
      result = result = isPreprod ? 'https://mumbai.polygonscan.com/tx/' + transactionHash : 'https://polygonscan.com/tx/' + transactionHash;
    } else if (network == "btc") {
      result = result = isPreprod ? 'https://www.blockchain.com/btc-testnet/tx/' + transactionHash : 'https://www.blockchain.com/btc/tx/' + transactionHash;
    }
    return result;
  }

  setDollarBalance(String selectedCrypto, String value) {
    var price = _cryptoList.firstWhere((e) => e.symbol == selectedCrypto).price ?? 0;
    value = value == "" ? "0" : value;
    var amount = (price * double.parse(value));
    return formatDollarBalance(amount);
  }

  setCryptoBalance(String selectedCrypto, String value) {
    var price = _cryptoList.firstWhere((e) => e.symbol == selectedCrypto).price ?? 0;
    value = value == "" ? "0" : value;
    var amount = 0.0;
    if (price > 0) {
      amount = double.parse(value) / price;
    }
    return formatCryptoBalance(amount, "");
  }

  getMaxCrypto(String selectedCrypto) {
    var balance = double.parse((_cryptoList.firstWhere((e) => e.symbol == selectedCrypto).quantity ?? 0).toString());
    _cryptoBalance.text = formatCryptoBalance(balance, "");
    cryptoBalance = formatCryptoBalance(balance, "");
    _dollarBalance.text = setDollarBalance(selectedCrypto, _cryptoBalance.text).toString();
    dollarBalance = setDollarBalance(selectedCrypto, _cryptoBalance.text).toString();
  }

  getMaxBalance(String selectedCrypto) {
    var balance = _cryptoList.firstWhere((e) => e.symbol == selectedCrypto).total_balance ?? 0;
    _dollarBalance.text = formatDollarBalance(balance);
    dollarBalance = _dollarBalance.text.replaceAll(new RegExp(r'[^\w\s]+'), '');
    _cryptoBalance.text = setCryptoBalance(selectedCrypto, _dollarBalance.text).toString();
    cryptoBalance = _cryptoBalance.text.replaceAll(',', '');
    cryptoBalance = _cryptoBalance.text.replaceAll('\$', '');
  }

  void _launchURLBuyBTT() async {
    const url = "https://sunswap.com/#/home";
    if (await canLaunch(url))
      await launch(url);
    else
      throw "Could not launch $url";
  }

  void _launchTxHashUrl(String network, String txHash) async {
    var link = setTransactionLink(network, txHash);
    if (await canLaunch(link))
      await launch(link);
    else
      throw "Could not launch $link";
  }

  void handleAdressBookEntry(String text) {
    if (!contactsList.any((e) => e.address?.toLowerCase() == text)) {}
  }

  Widget list() {
    return Padding(
      padding: EdgeInsets.only(
        top: _walletPattern ? MediaQuery.of(context).size.height * 0.46 : MediaQuery.of(context).size.height * 0.44,
        left: MediaQuery.of(context).size.width * .08,
        right: MediaQuery.of(context).size.width * .08,
      ),
      child: Container(
        width: MediaQuery.of(context).size.width * .85,
        decoration: BoxDecoration(
          border: Border.all(
            color: Color(0xFFE1E1E1),
            width: .75,
          ),
          borderRadius: BorderRadius.all(Radius.circular(30.0)),
        ),
        child: ListView.builder(
          shrinkWrap: true,
          itemCount: surFix.length > 3 ? 3 : surFix.length,
          itemBuilder: (BuildContext context, int index) {
            return TextButton(
              style: const ButtonStyle(
                overlayColor: MaterialStatePropertyAll(Color(0xFFD6D6E8)),
              ),
              onPressed: () {
                setState(() {
                  listElementClicked = true;
                });
                _walletAddress.text = surFix[index].address ?? "";
                searchbook(_walletAddress.text);
                if (_selectedPaymentMethod == "tron") {
                  if (RegExp("T[A-Za-z1-9]{33}\$").hasMatch(_walletAddress.text)) {
                    setState(() {
                      _walletField = true;
                      _walletPattern = false;
                      _walletFieldRequired = false;
                    });
                  } else {
                    setState(() {
                      _walletField = false;
                      _walletPattern = true;
                      _walletFieldRequired = false;
                    });
                  }
                } else {
                  if (RegExp(r"^0x[a-fA-F0-9]{40}$|^(bc1|[13])[a-zA-HJ-NP-Z0-9]{25,39}$").hasMatch(_walletAddress.text)) {
                    setState(() {
                      _walletField = true;
                      _walletPattern = false;
                      _walletFieldRequired = false;
                    });
                  } else {
                    setState(() {
                      _walletField = false;
                      _walletPattern = true;
                      _walletFieldRequired = false;
                    });
                  }
                }
              },
              child: Column(children: [
                Align(
                  alignment: Alignment.topLeft,
                  child: Text(
                    '${formatName(surFix[index].name)}',
                    style: GoogleFonts.poppins(
                      color: Color(0xFF1F2337),
                      fontSize: 20,
                      fontWeight: FontWeight.w500,
                      fontStyle: FontStyle.normal,
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.topLeft,
                  child: Text(
                    formatAdress(surFix[index].address),
                    style: GoogleFonts.poppins(
                      color: Color(0xFF1F2337),
                      fontSize: 20,
                      fontWeight: FontWeight.w500,
                      fontStyle: FontStyle.normal,
                    ),
                  ),
                ),
                index < 2 && index < surFix.length && surFix.length > 1 ? Divider(thickness: 2) : SizedBox.shrink(),
              ]),
            );
          },
        ),
      ),
    );
  }

  displaySearchContactDialog() {
    onSearchTextChanged(_walletAddress.text);
    showDialog(
        barrierDismissible: false,
        context: MyApp.materialKey.currentContext ?? OneContext() as BuildContext,
        builder: (context) {
          return StatefulBuilder(builder: (BuildContext context, setState) {
            return Dialog(
                backgroundColor: Colors.transparent,
                insetPadding: EdgeInsets.zero,
                child: Container(
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.height,
                  decoration: BoxDecoration(
                    color: Colors.white,
                  ),
                  child: Stack(
                    children: [
                      SingleChildScrollView(
                          child: Column(
                        children: <Widget>[
                          SizedBox(height: MediaQuery.of(context).size.height * .005),
                          Row(
                            children: [
                              Padding(
                                padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * .03, left: MediaQuery.of(context).size.width * 0.055),
                                child: Text(
                                  "Search a Contact",
                                  style: GoogleFonts.poppins(
                                    color: Color(0xFF1F2337),
                                    fontSize: MediaQuery.of(context).size.height * 0.027,
                                    fontWeight: FontWeight.w700,
                                    fontStyle: FontStyle.normal,
                                  ),
                                ),
                              ),
                              SizedBox(
                                width: MediaQuery.of(context).size.width * .27,
                              ),
                              Padding(
                                padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * .03),
                                child: IconButton(
                                    onPressed: () async {
                                      Navigator.of(context).pop();
                                    },
                                    icon: Icon(
                                      Icons.close,
                                      color: Color(0xFF75758F),
                                      size: MediaQuery.of(context).size.height * 0.038,
                                    )),
                              ),
                            ],
                          ),
                          Padding(
                            padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.height * 0.03, MediaQuery.of(context).size.width * 0.03, MediaQuery.of(context).size.height * 0.03, MediaQuery.of(context).size.width * 0.03),
                            child: TextFormField(
                              controller: _walletAddress,
                              decoration: new InputDecoration(
                                hintText: 'Search',
                                suffixIcon: IconButton(
                                  onPressed: () {
                                    setState(() {
                                      _walletAddress.text = "";
                                      searchbook("");
                                    });
                                  },
                                  icon: Icon(Icons.close),
                                ),
                                prefixIcon: Padding(
                                  padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.025, right: MediaQuery.of(context).size.width * 0.025),
                                  child: SvgPicture.asset(
                                    "images/search.svg",
                                    width: MediaQuery.of(context).size.width * 0.02,
                                  ),
                                ),
                                fillColor: Colors.grey,
                                focusColor: Colors.grey,
                                hintStyle: GoogleFonts.poppins(fontWeight: FontWeight.normal, color: Colors.grey, fontSize: MediaQuery.of(context).size.width * 0.036),
                                contentPadding: new EdgeInsets.symmetric(vertical: 0.0, horizontal: MediaQuery.of(context).size.width * 0.03),
                                enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.grey.withOpacity(0.3))),
                                focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Color(0xff4048FF))),
                              ),
                              onChanged: (value) {
                                TextSelection previousSelection = _walletAddress.selection;
                                _walletAddress.text = value;
                                setState(() {
                                  _walletAddress.text = value;
                                  searchbook(_walletAddress.text);
                                });
                                _walletAddress.selection = previousSelection;
                              },
                            ),
                          ),
                          SizedBox(
                            height: MediaQuery.of(context).size.height * .01,
                          ),
                          Container(
                            height: MediaQuery.of(context).size.height,
                            width: MediaQuery.of(context).size.width * .85,
                            child: ListView.builder(
                              shrinkWrap: true,
                              itemCount: surFix.length,
                              itemBuilder: (BuildContext context, int index) {
                                return TextButton(
                                  style: const ButtonStyle(
                                    overlayColor: MaterialStatePropertyAll(Color(0xFFD6D6E8)),
                                  ),
                                  onPressed: () {
                                    _walletAddress.text = surFix[index].address ?? "";
                                    Navigator.of(context).pop();
                                  },
                                  child: Column(children: [
                                    Align(
                                      alignment: Alignment.topLeft,
                                      child: Text(
                                        '${formatName(surFix[index].name)}',
                                        style: GoogleFonts.poppins(
                                          color: Color(0xFF1F2337),
                                          fontSize: 20,
                                          fontWeight: FontWeight.w500,
                                          fontStyle: FontStyle.normal,
                                        ),
                                      ),
                                    ),
                                    Align(
                                      alignment: Alignment.topLeft,
                                      child: Text(
                                        formatAdress(surFix[index].address),
                                        style: GoogleFonts.poppins(
                                          color: Color(0xFF1F2337),
                                          fontSize: 20,
                                          fontWeight: FontWeight.w500,
                                          fontStyle: FontStyle.normal,
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      height: MediaQuery.of(context).size.height * .01,
                                    ),
                                    Container(
                                      width: MediaQuery.of(context).size.width,
                                      height: MediaQuery.of(context).size.height * .002,
                                      decoration: BoxDecoration(
                                        color: Color(0xffD6D6E8),
                                      ),
                                    )
                                  ]),
                                );
                              },
                            ),
                          ),
                        ],
                      ))
                    ],
                  ),
                ));
          });
        });
  }

  void searchbook(String query) {
    final suggestions = contactsList.where((surFix) {
      final address = surFix.address!.toLowerCase();
      final name = surFix.name!.toLowerCase();
      final input = query.toLowerCase();
      return address.contains(input) || name.contains(input);
    }).toList();
    setState(() => surFix = suggestions);
  }

  onSearchTextChanged(String text) {
    final suggestions = contactsList.where((surFix) {
      final address = surFix.address!.contains(text);
      final name = surFix.name!.contains(text);
      final input = text.toLowerCase();
      return address || name;
    }).toList();
    setState(() => surFix = suggestions);
  }

  String formatAdress(String? address) {
    String result = address ?? "";
    if (result.length >= 8) {
      return result.substring(0, 4) + '...........................' + result.substring(result.length - 3, result.length);
    }
    return result;
  }

  String formatName(String? name) {
    String result = name ?? "";
    if (result.length >= 8) {
      return result.substring(0, 5) + '...........................' + result.substring(result.length - 5, result.length);
    }
    return result;
  }
}

class DecimalTextInputFormatter extends TextInputFormatter {
  DecimalTextInputFormatter({this.decimalRange}) : assert(decimalRange == null || decimalRange > 0);
  final int? decimalRange;

  @override
  TextEditingValue formatEditUpdate(
    TextEditingValue oldValue, // unused.
    TextEditingValue newValue,
  ) {
    TextSelection newSelection = newValue.selection;
    String truncated = newValue.text;

    if (decimalRange != null) {
      String value = newValue.text;

      if (value.contains(".") && value.substring(value.indexOf(".") + 1).length > decimalRange!) {
        truncated = oldValue.text;
        newSelection = oldValue.selection;
      } else if (value == ".") {
        truncated = "0.";

        newSelection = newValue.selection.copyWith(
          baseOffset: math.min(truncated.length, truncated.length + 1),
          extentOffset: math.min(truncated.length, truncated.length + 1),
        );
      }

      return TextEditingValue(
        text: truncated,
        selection: newSelection,
        composing: TextRange.empty,
      );
    }
    return newValue;
  }
}

class CustomMaxValueInputFormatter extends TextInputFormatter {
  final double? maxInputValue;

  CustomMaxValueInputFormatter({this.maxInputValue});

  @override
  TextEditingValue formatEditUpdate(TextEditingValue oldValue, TextEditingValue newValue) {
    final TextSelection newSelection = newValue.selection;
    String truncated = newValue.text;
    if (maxInputValue != null) {
      try {
        final double value = double.parse(newValue.text);
        if (value == null) {
          return TextEditingValue(
            text: truncated,
            selection: newSelection,
          );
        }
        if (value > double.parse(maxInputValue.toString())) {
          truncated = oldValue.text.toString();
        }
      } catch (e) {
        print(e);
      }
    }
    return TextEditingValue(
      text: truncated,
      selection: newSelection,
    );
  }
}
