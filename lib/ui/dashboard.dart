import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:ihave/controllers/LoginController.dart';
import 'package:ihave/controllers/walletController.dart';
import 'package:ihave/model/crypto.dart';
import 'package:ihave/model/searchCrypto.dart';
import 'package:ihave/service/apiService.dart';
import 'package:ihave/ui/WelcomeScreen.dart';
import 'package:ihave/ui/buy.dart';
import 'package:ihave/ui/cryptoDetails.dart';
import 'package:ihave/util/utils.dart';
import 'package:provider/provider.dart';
import 'package:collection/collection.dart';
import 'package:http/http.dart' as http;
import 'package:url_launcher/url_launcher.dart';
import '../service/Items.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../service/web3Service.dart';
import 'addToken.dart';

class Dashboard extends StatefulWidget {
  const Dashboard({Key? key}) : super(key: key);

  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  bool verif = true;
  var solde;
  var user;
  APIService apiService = LoginController.apiService;
  List<Crypto> _cryptoList = <Crypto>[];
  List<SearchCrypto> _searchCryptoList = <SearchCrypto>[];
  List<Crypto> addedTokenList = <Crypto>[];
  bool _isVisible = false;
  bool _isProfileVisible = false;
  bool _isWalletVisible = false;
  String? firebaseToken = " ";
  bool _changelist = false;
  ButtonStyle marketCapStyle = ButtonStyle(
    backgroundColor: MaterialStateProperty.all(
      Color(0xFFFFFFFF),
    ),
    shape: MaterialStateProperty.all(
      RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(30),
      ),
    ),
  );
  bool _isNotificationVisible = false;
  bool _obscureText = true;
  TextEditingController _WalletPassword = TextEditingController();
  bool _isLoading = true;
  bool isSendClicked = false;
  bool _isClickedSatt = false;
  bool _isremovedToken = false;
  bool _screenCreated = false;
  bool _buyCrypto = false;
  bool isclicked = false;
  bool _color = false;
  int? checkedItem;
  int? checkedItem2;
  String? checkedNetwork;
  String? text = "new";
  String? _checkedNetwork;
  int? selectedIndex;
  bool sattchecked = false;
  bool _sattchecked = false;
  bool cryptochecked = false;
  bool _checkValue = false;
  String refreshBalance = "";
  final _formkeyDash = GlobalKey<FormState>();
  GlobalKey _refreshKey = GlobalKey<RefreshIndicatorState>();
  TextEditingController _search = new TextEditingController();
  late Future userBalance;
  List<List<Crypto>> _cryptoListGrouped = <List<Crypto>>[];

  Future getTotalBalance() async {
    if (Provider.of<WalletController>(context, listen: false).cryptos.isNotEmpty) {
      var currentBalance = await apiService.getTotalBalance(Provider.of<WalletController>(context, listen: false).cryptos);
      if (currentBalance.toString() != "") {
        setState(() {
          Provider.of<LoginController>(context, listen: false).Balance = currentBalance.toStringAsFixed(2);
        });
        return "success";
      } else {
        return "error";
      }
    }
    return "error";
  }

  tronWallet(BuildContext context) async {
    var result = await LoginController.apiService.walletTronCheck(Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "");
    if (result == null) {
      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) {
            return StatefulBuilder(builder: (context, setState) {
              return Scaffold(
                resizeToAvoidBottomInset: true,
                body: Dialog(
                  backgroundColor: Colors.transparent,
                  insetPadding: EdgeInsets.zero,
                  child: SingleChildScrollView(
                    child: Container(
                      height: MediaQuery.of(context).size.height,
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                        color: Colors.white.withOpacity(0.9),
                      ),
                      child: Column(
                        children: <Widget>[
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Spacer(),
                              Spacer(),
                              Padding(
                                padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.01, top: MediaQuery.of(context).size.width * 0.06),
                                child: IconButton(
                                  alignment: Alignment.topLeft,
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                  splashColor: Colors.transparent,
                                  highlightColor: Colors.transparent,
                                  icon: Icon(
                                    Icons.close,
                                    size: MediaQuery.of(context).size.width * 0.05,
                                    color: Colors.black.withOpacity(0.5),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: MediaQuery.of(context).size.height * 0.035,
                          ),
                          Image.asset(
                            "images/tron-wallet.png",
                          ),
                          Text(
                            "Create your Tron wallet",
                            style: GoogleFonts.poppins(fontWeight: FontWeight.w700, fontSize: MediaQuery.of(context).size.height * 0.025, color: Colors.black),
                          ),
                          Padding(
                            padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.075, right: MediaQuery.of(context).size.width * 0.075, top: MediaQuery.of(context).size.height * 0.01),
                            child: Text(
                              "You are about to delete this token. You now have the possibility to create your Tron ​​wallet.You Need to just enter your transaction password to create it ",
                              textAlign: TextAlign.center,
                              style: GoogleFonts.poppins(fontWeight: FontWeight.w400, fontSize: MediaQuery.of(context).size.height * 0.0175, color: const Color(0XFF162746), letterSpacing: 1.1),
                            ),
                          ),
                          Align(
                            alignment: Alignment.topLeft,
                            child: Padding(
                              padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.1, top: MediaQuery.of(context).size.height * 0.035),
                              child: Text(
                                "TRANSACTION PASSWORD",
                                style: GoogleFonts.poppins(fontWeight: FontWeight.w600, color: const Color(0XFF75758F)),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: MediaQuery.of(context).size.height * 0.01,
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width * 0.8,
                            child: TextFormField(
                              style: TextStyle(fontSize: MediaQuery.of(context).size.width * 0.045),
                              textAlignVertical: TextAlignVertical.center,
                              textAlign: TextAlign.left,
                              controller: _WalletPassword,
                              keyboardType: TextInputType.visiblePassword,
                              obscureText: _obscureText,
                              onChanged: (value) {
                                TextSelection previousSelection = _WalletPassword.selection;
                                _WalletPassword.text = value;
                                _WalletPassword.selection = previousSelection;

                                TextSelection.fromPosition(TextPosition(offset: _WalletPassword.text.length));
                              },
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return 'Field required';
                                }
                                return null;
                              },
                              decoration: InputDecoration(
                                  fillColor: Colors.white,
                                  focusColor: Colors.white,
                                  hoverColor: Colors.white,
                                  filled: true,
                                  isDense: true,
                                  prefixIcon: Padding(
                                    padding: EdgeInsets.all(MediaQuery.of(context).size.width * 0.017),
                                    child: SvgPicture.asset(
                                      "images/keyIcon.svg",
                                      color: Colors.orange,
                                      height: MediaQuery.of(context).size.width * 0.06,
                                      width: MediaQuery.of(context).size.width * 0.06,
                                    ),
                                  ),
                                  suffixIcon: Padding(
                                    padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.025),
                                    child: IconButton(
                                        onPressed: () {
                                          setState(() {
                                            _obscureText = !_obscureText;
                                          });
                                        },
                                        icon: _obscureText
                                            ? SvgPicture.asset(
                                                "images/visibility-icon-off.svg",
                                                height: MediaQuery.of(context).size.height * 0.02,
                                              )
                                            : SvgPicture.asset(
                                                "images/visibility-icon-on.svg",
                                                height: MediaQuery.of(context).size.height * 0.02,
                                              )),
                                  ),
                                  contentPadding: EdgeInsets.symmetric(vertical: 5, horizontal: 5),
                                  enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.white)),
                                  focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.white)),
                                  errorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red)),
                                  focusedErrorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red))),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.02),
                            child: SizedBox(
                              width: MediaQuery.of(context).size.width * 0.6,
                              child: ElevatedButton(
                                child: Padding(
                                  padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.015, bottom: MediaQuery.of(context).size.height * 0.015),
                                  child: Text(
                                    "Create Wallet",
                                    style: GoogleFonts.poppins(fontWeight: FontWeight.w700, fontSize: MediaQuery.of(context).size.height * 0.017),
                                  ),
                                ),
                                onPressed: () {},
                                style: ButtonStyle(
                                  backgroundColor: MaterialStateProperty.all(
                                    Color(0xFF4048FF),
                                  ),
                                  shape: MaterialStateProperty.all(
                                    RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.08),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.012),
                            child: SizedBox(
                              width: MediaQuery.of(context).size.width * 0.6,
                              child: ElevatedButton(
                                child: Padding(
                                  padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.015, bottom: MediaQuery.of(context).size.height * 0.015),
                                  child: Text(
                                    "Skip for now",
                                    style: GoogleFonts.poppins(fontWeight: FontWeight.w700, fontSize: MediaQuery.of(context).size.height * 0.017, color: const Color(0xFF4048FF)),
                                  ),
                                ),
                                onPressed: () {},
                                style: ButtonStyle(
                                  backgroundColor: MaterialStateProperty.all(
                                    Colors.white,
                                  ),
                                  shape: MaterialStateProperty.all(
                                    RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.08),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Align(
                            alignment: Alignment.topCenter,
                            child: Padding(
                              padding: EdgeInsets.fromLTRB(0, MediaQuery.of(context).size.height * 0.025, 0, 0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Container(
                                    decoration: BoxDecoration(color: _checkValue ? Colors.white : Colors.transparent, border: Border.all(color: _checkValue ? Color(0xFF00CC9E) : Color(0xFFADADC8), width: 2.5), borderRadius: BorderRadius.all(Radius.circular(5))),
                                    width: 18,
                                    height: 18,
                                    child: Theme(
                                      data: ThemeData(
                                          checkboxTheme: CheckboxThemeData(
                                            shape: RoundedRectangleBorder(
                                              borderRadius: BorderRadius.circular(5),
                                            ),
                                          ),
                                          unselectedWidgetColor: Colors.transparent),
                                      child: Checkbox(
                                        checkColor: Color(0xFF00CC9E),
                                        activeColor: Colors.transparent,
                                        value: _checkValue,
                                        onChanged: (value) {
                                          setState(() {
                                            _checkValue = value!;
                                          });
                                        },
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.03),
                                    child: Text("Mute future reminders", style: GoogleFonts.poppins(fontWeight: FontWeight.w400, color: const Color(0XFF323754))),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              );
            });
          });
    }
  }

  List<List<Crypto>> groupCryptos(List<Crypto> cryptoList) {
    var tempSet = Set<String>();
    var tempList = cryptoList.where((c) => tempSet.add(c.name?.toLowerCase() ?? "")).toList();
    List<List<Crypto>> groupedList = <List<Crypto>>[];
    tempList.forEach((e) {
      groupedList.add(cryptoList.where((c) => c.name?.toLowerCase() == e.name?.toLowerCase()).toList());
    });
    return groupedList;
  }

  _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  Future<void> _refresh() async {
    Provider.of<WalletController>(context, listen: false).cryptos = [];
    Provider.of<LoginController>(context, listen: false).Balance = "";
    await Future.wait([
      cryptoList(),
      getTotalBalance(),
    ]).then((value) {
      setState(() {
        _cryptoList.clear();
        _cryptoList.addAll(Provider.of<WalletController>(context, listen: false).cryptos);
        _isLoading = false;
      });
    });
  }

  double quantitySum(List<Crypto> crypto) {
    var sum = 0.00;
    crypto.forEach((e) {
      sum += double.parse(e.quantity ?? "0");
    });
    return sum;
  }

  Future cryptoList() async {
    var cryptoList = await apiService.cryptos1(Provider.of<LoginController>(context, listen: false).userDetails!.userToken.toString(), version!);
    if (cryptoList != "error") {
      setState(() {
        Provider.of<WalletController>(context, listen: false).cryptos.clear();
        Provider.of<WalletController>(context, listen: false).cryptos.addAll(cryptoList);
        _cryptoList.clear();
        _cryptoList.addAll(Provider.of<WalletController>(context, listen: false).cryptos);
      });
    }
    getTotalBalance();
    return cryptoList;
  }

  Future<List<Crypto>> fetchCrypto() async {
    if (Provider.of<WalletController>(context, listen: false).cryptos.length <= 1) {
      var cryptoList = await apiService.cryptos1(Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "", version!);
      Provider.of<WalletController>(context, listen: false).cryptos = cryptoList;
      getTotalBalance();
    }
    return Provider.of<WalletController>(context, listen: false).cryptos;
  }

  List<String> listViewerBuilderString = [];
  List<double?> listViewerBuilderdouble = [];
  List<String> listViewerBuilderStringverf = [];
  List<double?> listViewerBuilderdoubleverf = [];
  Map<String, double?> map = Map<String, double?>();
  final storage = FlutterSecureStorage();

  @override
  void initState() {
    setState(() {
      _screenCreated = true;
    });
    fetchCrypto().then((value) async {
      setState(() {
        _cryptoList.clear();
        _cryptoList.addAll(value);
        _isLoading = !_isLoading;
        _cryptoListGrouped.clear();
        _cryptoListGrouped.addAll(groupCryptos(_cryptoList));
      });
      for (var pet in _cryptoList) {
        listViewerBuilderdouble.add(pet.variation);
        listViewerBuilderString.add(pet.name.toString());
      }
      map = Map.fromIterables(listViewerBuilderString, listViewerBuilderdouble);
      final SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setStringList("lst", listViewerBuilderString);
    });

    fetchCryptosToSearch().then((value) {
      setState(() {
        _searchCryptoList.clear();
        _searchCryptoList.addAll(value);
        filterList();
      });
    });
    Provider.of<LoginController>(context, listen: false).tokenSymbol = "";
    Provider.of<LoginController>(context, listen: false).tokenNetwork = "";
  }

  String calculateAffectation(num balance) {
    var totalBalance = num.parse(solde!);
    if (totalBalance != 0) {
      var affectation = (balance / totalBalance) * 100;
      return affectation.toString() + "%";
    }
    return "0 %";
  }

  String formatCryptoName(List<Crypto> crypto) {
    var name = crypto[0].symbol ?? crypto[0].undername ?? crypto[0].undername2 ?? "Network error";
    name = name.toLowerCase().contains("satt") ? "SaTT" : name;
    name = crypto[0].name?.toLowerCase().contains("wsatt") == true ? "WSaTT" : name;
    return name;
  }

  String calculSomme(List<Crypto> crypto) {
    double sum = 0;
    crypto.forEach((e) {
      sum += e.total_balance ?? 0;
    });
    return formatDollarBalance(sum);
  }

  bool isCryptoAddressValid = true;
  SearchCrypto? _selectedCrypto;

  Future save(String decimal, String cryptoContract, String symbol, String network, String name) async {
    var token = Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "";
    if (isCryptoAddressValid) {
      var result = await apiService.addCustomCrypto(token, decimal, cryptoContract, symbol, network, name);
      setState(() {});
      if (result["error"] != null && result["error"] != "") {
        Fluttertoast.showToast(
          msg: result["error"],
          backgroundColor: Color(0xFFF52079),
        );
      } else {
        Fluttertoast.showToast(
          msg: "Token added successfully",
          backgroundColor: Color.fromRGBO(0, 151, 117, 1),
        );
      }
    }
  }

  DateTime pre_backpress = DateTime.now();

  @override
  Widget build(BuildContext context) {
    setState(() {
      _isVisible = Provider.of<WalletController>(context, listen: true).isSideMenuVisible;
      _isProfileVisible = Provider.of<WalletController>(context, listen: true).isProfileVisible;
      _isWalletVisible = Provider.of<WalletController>(context, listen: true).isWalletVisible;
      _isNotificationVisible = Provider.of<WalletController>(context, listen: true).isNotificationVisible;
      cryptochecked = Provider.of<WalletController>(context, listen: true).isSelectCryptoMode;
      checkedItem = Provider.of<WalletController>(context, listen: true).isSelectCryptoMode ? checkedItem : -1;
      checkedItem2 = Provider.of<WalletController>(context, listen: true).isSelectCryptoMode ? checkedItem2 : -1;
    });
    return WillPopScope(
        onWillPop: () async {
          return false;
        },
        child: Scaffold(
            resizeToAvoidBottomInset: false,
            backgroundColor: Colors.transparent,
            body: RefreshIndicator(
              onRefresh: _refresh,
              child: Stack(children: [
                SingleChildScrollView(
                    child: Column(
                  children: [
                    Container(
                      height: MediaQuery.of(context).size.height * 0.16,
                      child: Column(children: [
                        Text(
                          "Current Balance",
                          textAlign: TextAlign.center,
                          style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.047, fontWeight: FontWeight.w700, color: Colors.white),
                        ),
                        Provider.of<LoginController>(context, listen: false).Balance != ""
                            ? Text(
                                formatDollarBalance(double.tryParse(Provider.of<LoginController>(context, listen: false).Balance) ?? 0.00),
                                textAlign: TextAlign.center,
                                style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.098, fontWeight: FontWeight.w700, color: Colors.white),
                              )
                            : FutureBuilder(
                                future: getTotalBalance(),
                                builder: (context, AsyncSnapshot snapshot) {
                                  if (snapshot.hasData && snapshot.connectionState == ConnectionState.done) {
                                    Provider.of<LoginController>(context, listen: false).Balance = snapshot.data.toString();
                                    return Text(
                                      formatDollarBalance(double.tryParse(snapshot.data) ?? 0.00),
                                      textAlign: TextAlign.center,
                                      style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.083, fontWeight: FontWeight.w700, color: Colors.white),
                                    );
                                  } else {
                                    return Text(
                                      "\$",
                                      textAlign: TextAlign.center,
                                      style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.083, fontWeight: FontWeight.w700, color: Colors.white),
                                    );
                                  }
                                },
                              ),
                        InkWell(
                          onTap: () async {
                            setState(() {
                              verif = !verif;
                              verif ? version = "v1" : version = "v2";
                              verif ? text = "new" : text = "old";
                              Provider.of<WalletController>(context, listen: false).cryptos.clear();
                              _cryptoList.clear();
                              _cryptoListGrouped.clear();
                            });
                            await Future.wait([
                              cryptoList(),
                              getTotalBalance(),
                            ]).then((value) {
                              setState(() {
                                _cryptoList.clear();
                                _cryptoList.addAll(Provider.of<WalletController>(context, listen: false).cryptos);
                                _isLoading = false;
                                _cryptoListGrouped.clear();
                                _cryptoListGrouped.addAll(groupCryptos(_cryptoList));
                              });
                            });
                          },
                          child: Visibility(
                            visible: apiService.walletAddress != apiService.walletAddressV2,
                            child: Padding(
                              padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * .34),
                              child: Row(
                                children: [
                                  Image.asset(
                                    "images/refresh.png",
                                    width: MediaQuery.of(context).size.width * .032,
                                    height: MediaQuery.of(context).size.height * .02,
                                  ),
                                  SizedBox(width: MediaQuery.of(context).size.width * .02),
                                  Text("Go to"),
                                  Text(" " + text!),
                                  Text(" Wallet"),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ]),
                    ),
                    Padding(
                        padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                        child: Stack(
                          children: [
                            Container(
                              height: MediaQuery.of(context).size.height * 0.71,
                              decoration: BoxDecoration(
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.grey.withOpacity(0.2),
                                      spreadRadius: 5,
                                      blurRadius: 7,
                                      offset: Offset(0, 3), // changes position of shadow
                                    ),
                                  ],
                                  color: Colors.white,
                                  border: Border.all(
                                    color: Colors.white,
                                  ),
                                  borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(MediaQuery.of(context).size.width * 0.08),
                                    topRight: Radius.circular(MediaQuery.of(context).size.width * 0.08),
                                  )),
                            ),
                            Container(
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    border: Border.all(
                                      color: Colors.white,
                                    ),
                                    borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(MediaQuery.of(context).size.width * 0.08),
                                      topRight: Radius.circular(MediaQuery.of(context).size.width * 0.08),
                                    )),
                                child: Column(
                                  children: [
                                    Padding(
                                        padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.height * 0.02, 0, MediaQuery.of(context).size.height * 0.02, 0),
                                        child: Form(
                                          key: _formkeyDash,
                                          child: Padding(
                                            padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.height * 0.03, MediaQuery.of(context).size.width * 0.03, MediaQuery.of(context).size.height * 0.03, MediaQuery.of(context).size.width * 0.03),
                                            child: TextFormField(
                                              controller: _search,
                                              onChanged: (text) {
                                                _search.text = text;
                                                _search.selection = TextSelection.fromPosition(TextPosition(offset: _search.text.length));
                                                setState(() {
                                                  _isLoading = true;
                                                  text != '' ? _changelist = true : _changelist = false;
                                                });
                                                searchList(text);
                                                searchList2(text);
                                                setState(() {
                                                  _isLoading = false;
                                                });
                                              },
                                              validator: (value) {
                                                return null;
                                              },
                                              autocorrect: false,
                                              readOnly: _isNotificationVisible,
                                              decoration: new InputDecoration(
                                                hintText: 'Search token or paste address',
                                                prefixIcon: Padding(
                                                  padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.025, right: MediaQuery.of(context).size.width * 0.025),
                                                  child: SvgPicture.asset(
                                                    "images/search.svg",
                                                    width: MediaQuery.of(context).size.width * 0.02,
                                                  ),
                                                ),
                                                fillColor: Colors.grey,
                                                focusColor: Colors.grey,
                                                hintStyle: GoogleFonts.poppins(fontWeight: FontWeight.normal, color: Colors.grey, fontSize: MediaQuery.of(context).size.width * 0.036),
                                                contentPadding: new EdgeInsets.symmetric(vertical: 0.0, horizontal: MediaQuery.of(context).size.width * 0.03),
                                                enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.grey.withOpacity(0.3))),
                                                focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.grey)),
                                                errorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red)),
                                              ),
                                            ),
                                          ),
                                        )),
                                    _cryptoListGrouped.length < 1 && _searchCryptoList.length < 1
                                        ? _search.text.length > 1
                                            ? Image.asset("images/not.png")
                                            : Center(
                                                child: SizedBox(
                                                  height: MediaQuery.of(context).size.width * 0.037,
                                                  width: MediaQuery.of(context).size.width * 0.037,
                                                  child: CircularProgressIndicator(
                                                    color: Color(0xFF4048FF),
                                                  ),
                                                ),
                                              )
                                        : ListView.builder(
                                            physics: NeverScrollableScrollPhysics(),
                                            shrinkWrap: true,
                                            scrollDirection: Axis.vertical,
                                            itemBuilder: (context, index) {
                                              return ExpansionTile(
                                                onExpansionChanged: (expantion) {
                                                  setState(() {
                                                    _sattchecked = false;
                                                    sattchecked = false;
                                                    cryptochecked = false;
                                                    checkedItem = -1;
                                                    checkedItem2 = -1;
                                                    Provider.of<WalletController>(context, listen: false).toggleSelectedCrypto(cryptochecked);
                                                  });
                                                },
                                                trailing: SizedBox.shrink(),
                                                title: Row(
                                                  children: [
                                                    Padding(
                                                      padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.height * 0.025, 0, MediaQuery.of(context).size.height * 0.005, 0),
                                                      child: _cryptoListGrouped[index][0].addedToken == true
                                                          ? (_cryptoListGrouped[index][0].picUrl != "false" && _cryptoListGrouped[index][0].picUrl != "null"
                                                              ? Image.network(
                                                                  _cryptoListGrouped[index][0].picUrl ?? "",
                                                                  height: MediaQuery.of(context).size.width * 0.095,
                                                                  width: MediaQuery.of(context).size.width * 0.095,
                                                                )
                                                              : SvgPicture.asset(
                                                                  'images/indispo.svg',
                                                                  width: MediaQuery.of(context).size.width * 0.095,
                                                                  height: MediaQuery.of(context).size.width * 0.095,
                                                                ))
                                                          : SvgPicture.asset(
                                                              'images/' + _cryptoListGrouped[index][0].name.toString().toLowerCase().replaceAll(' ', '') + '.svg',
                                                              width: MediaQuery.of(context).size.width * 0.095,
                                                              height: MediaQuery.of(context).size.width * 0.095,
                                                            ),
                                                    ),
                                                    Column(
                                                      crossAxisAlignment: CrossAxisAlignment.start,
                                                      children: [
                                                        Text(
                                                          formatCryptoName(_cryptoListGrouped[index]),
                                                          style: GoogleFonts.poppins(
                                                            fontWeight: FontWeight.bold,
                                                            fontSize: MediaQuery.of(context).size.width * 0.037,
                                                            color: Colors.black,
                                                          ),
                                                        ),
                                                        Text(
                                                          formatCryptoBalance(quantitySum(_cryptoListGrouped[index]), ""),
                                                          style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.035, fontWeight: FontWeight.w700, color: Color(0xFF75758F)),
                                                        ),
                                                      ],
                                                    ),
                                                    Spacer(),
                                                    Column(
                                                      crossAxisAlignment: CrossAxisAlignment.end,
                                                      mainAxisAlignment: MainAxisAlignment.end,
                                                      children: [
                                                        Text(
                                                          calculSomme(_cryptoListGrouped[index]).toString(),
                                                          style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.038, fontWeight: FontWeight.w600, color: Color(0xFF000000)),
                                                        ),
                                                        Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
                                                          Text(
                                                            _cryptoListGrouped[index][0].symbol?.toLowerCase().contains("btt") == true ? _cryptoListGrouped[index][0].price?.toStringAsFixed(8) : formatDollarBalance(_cryptoListGrouped[index][0].price ?? 0),
                                                            style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.038, fontWeight: FontWeight.w600, color: Color(0xFF75758F)),
                                                          ),
                                                          SizedBox.fromSize(
                                                            size: Size(MediaQuery.of(context).size.width * 0.027, 0),
                                                          ),
                                                          Padding(
                                                            padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.height * 0.005, 0, 0, 0),
                                                            child: Row(
                                                              children: [
                                                                Text(
                                                                  ((_cryptoListGrouped[index][0].variation ?? 0.00) >= 0) ? "+" : "",
                                                                  style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.032, fontWeight: FontWeight.w600, color: ((_cryptoListGrouped[index][0].variation ?? 0.00) >= 0) ? Color(0xFF00CC9E) : Color(0xFFF52079)),
                                                                ),
                                                                Text(
                                                                  (_cryptoListGrouped[index][0].variation?.toStringAsFixed(2) ?? "0.00") + "%",
                                                                  style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.032, fontWeight: FontWeight.w600, color: ((_cryptoListGrouped[index][0].variation ?? 0.00) >= 0) ? Color(0xFF00CC9E) : Color(0xFFF52079)),
                                                                )
                                                              ],
                                                            ),
                                                          ),
                                                        ])
                                                      ],
                                                    ),
                                                  ],
                                                ),
                                                tilePadding: EdgeInsets.all(0),
                                                children: [
                                                  ListView.builder(
                                                    shrinkWrap: true,
                                                    physics: ScrollPhysics(),
                                                    itemBuilder: (context, index2) {
                                                      return Column(
                                                        children: [
                                                          InkWell(
                                                            onTap: () {
                                                              setState(() {
                                                                selectedIndex = index;
                                                                _checkedNetwork = _cryptoListGrouped[index][index2].network.toString();
                                                                if (checkedItem != index || checkedItem2 != index2) {
                                                                  checkedItem = index;
                                                                  checkedItem2 = index2;
                                                                } else {
                                                                  checkedItem = -1;
                                                                  checkedItem2 = -1;
                                                                }
                                                                false;
                                                                cryptochecked = checkedItem == index && checkedItem2 == index2;
                                                                _color = !_color;
                                                              });
                                                              Provider.of<LoginController>(context, listen: false).tokenSymbol = _cryptoListGrouped[index][index2].symbol.toString();
                                                              Provider.of<LoginController>(context, listen: false).tokenNetwork = _checkedNetwork.toString();
                                                              Provider.of<WalletController>(context, listen: false).toggleSelectedCrypto(cryptochecked);
                                                              if (checkedItem == index && checkedItem2 == index2 && cryptochecked) ;
                                                              {
                                                                Provider.of<LoginController>(context, listen: false).marketCupCrypto = _cryptoListGrouped[index][index2].id ?? "";
                                                              }
                                                            },
                                                            child: Container(
                                                              color: checkedItem == index && checkedItem2 == index2 && cryptochecked ? Color(0xff7A7FFF) : Color(0XFFF6F6FF),
                                                              child: Column(
                                                                mainAxisAlignment: MainAxisAlignment.end,
                                                                crossAxisAlignment: CrossAxisAlignment.end,
                                                                children: [
                                                                  Row(
                                                                    children: [
                                                                      Padding(
                                                                          padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.height * 0.022, MediaQuery.of(context).size.width * 0.03, 0, 0),
                                                                          child: Text(
                                                                            "•  ${_cryptoListGrouped[index][index2].network.toString()}",
                                                                            style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.038, fontWeight: FontWeight.w600, color: checkedItem == index && checkedItem2 == index2 && cryptochecked ? Colors.white : Color(0xFF323754)),
                                                                          )),
                                                                    ],
                                                                  ),
                                                                  Row(
                                                                    children: [
                                                                      Padding(
                                                                        padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.height * 0.025, 0, 0, MediaQuery.of(context).size.width * 0.01),
                                                                        child: Text(
                                                                          formatCryptoBalance(double.parse(_cryptoListGrouped[index][index2].quantity ?? "0"), ""),
                                                                          style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.038, fontWeight: FontWeight.w600, color: checkedItem == index && checkedItem2 == index2 && cryptochecked ? Colors.white : Color(0xFF75758F)),
                                                                        ),
                                                                      ),
                                                                      Spacer(),
                                                                      Padding(
                                                                        padding: EdgeInsets.fromLTRB(0, 0, MediaQuery.of(context).size.height * 0.02, MediaQuery.of(context).size.width * 0.01),
                                                                        child: Text(
                                                                          formatDollarBalance(_cryptoListGrouped[index][index2].total_balance ?? 0),
                                                                          style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.038, fontWeight: FontWeight.w600, color: checkedItem == index && checkedItem2 == index2 && cryptochecked ? Colors.white : Color(0xFF212529)),
                                                                        ),
                                                                      ),
                                                                    ],
                                                                  ),
                                                                ],
                                                              ),
                                                            ),
                                                          ),
                                                          Divider(
                                                            height: MediaQuery.of(context).size.height * 0.001,
                                                            color: Colors.white.withOpacity(0.3),
                                                          ),
                                                        ],
                                                      );
                                                    },
                                                    itemCount: _cryptoListGrouped[index].length,
                                                  ),
                                                ],
                                              );
                                            },
                                            itemCount: _cryptoListGrouped.length,
                                          ),
                                    _changelist
                                        ? ListView.builder(
                                            physics: NeverScrollableScrollPhysics(),
                                            shrinkWrap: true,
                                            scrollDirection: Axis.vertical,
                                            itemBuilder: (context, index) {
                                              return Container(
                                                color: Color(0xFFF6F6FF),
                                                child: Padding(
                                                  padding: EdgeInsets.all(MediaQuery.of(context).size.width * 0.025),
                                                  child: Row(
                                                    mainAxisSize: MainAxisSize.max,
                                                    children: [
                                                      Padding(
                                                        padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.027),
                                                        child: Image.network(
                                                          _searchCryptoList[index].logo ?? "",
                                                          height: MediaQuery.of(context).size.width * 0.096,
                                                          width: MediaQuery.of(context).size.width * 0.096,
                                                        ),
                                                      ),
                                                      SizedBox(width: MediaQuery.of(context).size.width * 0.013),
                                                      Text(
                                                        _searchCryptoList[index].symbol ?? "",
                                                        style: GoogleFonts.poppins(fontWeight: FontWeight.w800, fontStyle: FontStyle.normal, fontSize: MediaQuery.of(context).size.width * 0.037, letterSpacing: 0.7, color: Colors.black),
                                                      ),
                                                      Spacer(),
                                                      Container(
                                                        width: MediaQuery.of(context).size.width * 0.1,
                                                        height: MediaQuery.of(context).size.height * 0.045,
                                                        child: ElevatedButton(
                                                          style: ElevatedButton.styleFrom(
                                                              minimumSize: Size(MediaQuery.of(context).size.width * 0.84, MediaQuery.of(context).size.height * 0.055),
                                                              side: BorderSide(color: Colors.white),
                                                              primary: Colors.white,
                                                              shadowColor: Colors.grey,
                                                              shape: RoundedRectangleBorder(
                                                                borderRadius: BorderRadius.circular(50.0),
                                                              )),
                                                          child: SvgPicture.asset('images/info-add-token.svg'),
                                                          onPressed: () async {
                                                            setState(() {
                                                              String network = _searchCryptoList[index].network.toString();
                                                              network == "ERC20" ? _launchURL("https://etherscan.io/address/${_searchCryptoList[index].contract}") : _launchURL("https://bscscan.com/address/${_searchCryptoList[index].contract}");
                                                            });
                                                          },
                                                        ),
                                                      ),
                                                      SizedBox(width: MediaQuery.of(context).size.width * 0.043),
                                                      Container(
                                                        decoration: BoxDecoration(
                                                          color: Colors.white,
                                                          borderRadius: BorderRadius.all(Radius.circular(30)),
                                                          boxShadow: [
                                                            BoxShadow(
                                                              color: Colors.grey.withOpacity(0.5),
                                                              spreadRadius: 1,
                                                              blurRadius: 1,
                                                              offset: Offset(0, 1), // changes position of shadow
                                                            ),
                                                          ],
                                                        ),
                                                        width: MediaQuery.of(context).size.width * 0.1,
                                                        height: MediaQuery.of(context).size.height * 0.045,
                                                        child: InkWell(
                                                          child: Align(
                                                            alignment: Alignment.center,
                                                            child: Icon(
                                                              CupertinoIcons.add,
                                                              size: 30,
                                                              color: Color(0xFF0000FF),
                                                            ),
                                                          ),
                                                          onTap: () async {
                                                            Provider.of<WalletController>(context, listen: false).selectedIndex = 2;
                                                            setState(() {
                                                              _selectedCrypto = _searchCryptoList[index];
                                                            });
                                                            await save(_selectedCrypto?.decimal?.toString() ?? "", _selectedCrypto?.contract?.toString() ?? "", _selectedCrypto?.symbol?.toString() ?? "", _selectedCrypto?.network?.toString() ?? "", _selectedCrypto?.name?.toString() ?? "");
                                                            Provider.of<WalletController>(context, listen: false).cryptos.clear();
                                                            Provider.of<WalletController>(context, listen: false).cryptos = [];
                                                            fetchCrypto();
                                                            filterList();
                                                            gotoPageGlobal(context, WelcomeScreen());
                                                          },
                                                        ),
                                                      ),
                                                      SizedBox(width: MediaQuery.of(context).size.width * 0.06),
                                                    ],
                                                  ),
                                                ),
                                              );
                                            },
                                            itemCount: _searchCryptoList.length,
                                          )
                                        : SizedBox(),
                                    if (_search.text.length > 1 && _cryptoListGrouped.length < 1 && _searchCryptoList.length < 1)
                                      Padding(
                                        padding: EdgeInsets.all(MediaQuery.of(context).size.width * 0.03),
                                        child: Column(
                                          children: [
                                            Center(
                                              child: Text(
                                                "No Token Found Please add manually",
                                                style: TextStyle(color: Colors.black, fontSize: 10),
                                              ),
                                            ),
                                            SizedBox(
                                              height: MediaQuery.of(context).size.height * .02,
                                            ),
                                            Container(
                                              width: MediaQuery.of(context).size.width * 0.85,
                                              child: ElevatedButton(
                                                style: ElevatedButton.styleFrom(
                                                    minimumSize: Size(MediaQuery.of(context).size.width * 0.84, MediaQuery.of(context).size.height * 0.055),
                                                    side: BorderSide(color: Colors.white),
                                                    primary: Colors.white,
                                                    shadowColor: Colors.grey,
                                                    shape: RoundedRectangleBorder(
                                                      borderRadius: BorderRadius.circular(50.0),
                                                    )),
                                                child: Text(
                                                  "+ Add token",
                                                  style: GoogleFonts.poppins(fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.width * 0.045, color: Color(0xFF4048FF)),
                                                ),
                                                onPressed: () {
                                                  setState(() {
                                                    _cryptoList.clear();
                                                    _cryptoList.addAll(Provider.of<WalletController>(context, listen: false).cryptos);
                                                  });
                                                  gotoPageGlobal(context, AddToken());
                                                  Provider.of<WalletController>(context, listen: false).toggleSelectedCrypto(false);
                                                },
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                  ],
                                )),
                          ],
                        ))
                  ],
                )),
              ]),
            )));
  }

  searchList(String text) {
    setState(() {
      if (text != null && text != "") {
        _cryptoList = Provider.of<WalletController>(context, listen: false).cryptos;
        var searchListName = _cryptoList.where((e) => e.name!.toLowerCase().contains(text.toLowerCase())).toList();
        var searchListSymbol = _cryptoList.where((e) => e.symbol?.toLowerCase() == text.toLowerCase()).toList();
        var searchListContract = _cryptoList.where((e) => e.contract?.toLowerCase() == text.toLowerCase()).toList();
        var searchResult = searchListName;
        searchResult.addAll(searchListSymbol);
        searchResult.addAll(searchListContract);
        _cryptoList = searchResult.toSet().toList();
        _cryptoListGrouped.clear();
        _cryptoListGrouped = groupCryptos(_cryptoList);
      } else {
        _cryptoList = Provider.of<WalletController>(context, listen: false).cryptos;
        _cryptoListGrouped.clear();
        _cryptoListGrouped = groupCryptos(_cryptoList);
      }
    });
  }

  searchList2(String text) {
    setState(() {
      if (text != null && text != "") {
        _searchCryptoList = Provider.of<WalletController>(context, listen: false).cryptosToAdd;
        var searchListName = _searchCryptoList.where((e) => e.name!.toLowerCase().contains(text.toLowerCase())).toList();
        var searchListSymbol = _searchCryptoList.where((e) => e.symbol?.toLowerCase() == text.toLowerCase()).toList();
        var searchListContract = _searchCryptoList.where((e) => e.contract?.toLowerCase() == text.toLowerCase()).toList();
        var searchResult = searchListName;
        searchResult.addAll(searchListSymbol);
        searchResult.addAll(searchListContract);
        _searchCryptoList = searchResult.toSet().toList();
      } else {
        _searchCryptoList = Provider.of<WalletController>(context, listen: false).cryptosToAdd;
      }
      filterList();
    });
  }

  filterList() {
    var cryptolist = Provider.of<WalletController>(context, listen: false).cryptos;
    cryptolist.forEach((e) {
      var symbol = e.symbol;
      SearchCrypto? toRemove = _searchCryptoList.firstWhereOrNull((s) => s.symbol == symbol);

      setState(() {
        if (toRemove != null) {
          _searchCryptoList.remove(toRemove);
        }
      });
    });
  }

  void GoToMarketCap(String? id) {
    if (id != null && id != "") {
      Provider.of<LoginController>(context, listen: false).marketCupCrypto = id;
      gotoPageGlobal(context, CryptoDetails());
    } else {
      showDialog(
          barrierDismissible: false,
          context: context,
          builder: (context) {
            return StatefulBuilder(builder: (context, setState) {
              return AlertDialog(
                  insetPadding: EdgeInsets.zero,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(30)),
                  ),
                  content: Padding(
                    padding: const EdgeInsets.all(0),
                    child: Container(
                      width: MediaQuery.of(context).size.width * 0.6,
                      height: MediaQuery.of(context).size.height * 0.4,
                      child: Column(
                        children: [
                          Padding(
                            padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.5, top: MediaQuery.of(context).size.width * 0.02),
                            child: IconButton(
                                alignment: Alignment.topLeft,
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                                icon: Icon(
                                  Icons.close,
                                  color: Colors.grey,
                                  size: 35,
                                )),
                          ),
                          Image.asset(
                            "images/other.png",
                          ),
                          Text(
                            "Sorry this token doesn't seem to be listed \non our parteners plateforms.",
                            textAlign: TextAlign.center,
                            style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.035, fontWeight: FontWeight.w700, color: Colors.black),
                          ),
                          Spacer(),
                          ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                  minimumSize: Size(200.0, 40.0),
                                  side: BorderSide(color: Colors.grey),
                                  primary: Color(0xFF4048FF),
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(50.0),
                                  )),
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                              child: Text(
                                "ok",
                                style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.047, fontWeight: FontWeight.w700, color: Colors.white),
                              )),
                        ],
                      ),
                    ),
                  ));
            });
          });
    }
  }

  Future<List<SearchCrypto>> fetchCryptosToSearch() async {
    if (Provider.of<WalletController>(context, listen: false).cryptosToAdd.length < 1) {
      var cryptoList = await apiService.cryptosToAdd(Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "");
      List<SearchCrypto> cryptos = <SearchCrypto>[];
      cryptos.addAll(cryptoList);
      cryptos.forEach((e) {
        if (e.contract == null || e.contract == "") {
          cryptoList.remove(e);
        }
      });
      Provider.of<WalletController>(context, listen: false).cryptosToAdd = cryptoList;
    }
    return Provider.of<WalletController>(context, listen: false).cryptosToAdd;
  }

  void deleteToken(Crypto crypto, context) {
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (context) {
          return StatefulBuilder(builder: (context, setState) {
            return AlertDialog(
                insetPadding: EdgeInsets.zero,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(30)),
                ),
                content: Padding(
                  padding: const EdgeInsets.all(0),
                  child: Container(
                    width: MediaQuery.of(context).size.width * 0.6,
                    height: MediaQuery.of(context).size.height * 0.5,
                    child: Column(
                      children: [
                        Padding(
                          padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.5, top: MediaQuery.of(context).size.width * 0.02),
                          child: IconButton(
                              alignment: Alignment.topLeft,
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                              icon: Icon(
                                Icons.close,
                                color: Colors.grey,
                                size: 35,
                              )),
                        ),
                        SvgPicture.asset(
                          "images/question.svg",
                        ),
                        Text(
                          "Are you sure ?",
                          textAlign: TextAlign.center,
                          style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.035, fontWeight: FontWeight.w700, color: Colors.black),
                        ),
                        Text(
                          "You are about to delete \n this token ",
                          textAlign: TextAlign.center,
                          style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.035, fontWeight: FontWeight.normal, color: Colors.black),
                        ),
                        ElevatedButton(
                            style: ElevatedButton.styleFrom(
                                minimumSize: Size(260.0, 40.0),
                                side: BorderSide(color: Colors.white),
                                primary: Colors.white,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(50.0),
                                )),
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                            child: Text(
                              "cancel",
                              style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.047, fontWeight: FontWeight.w700, color: Color(0xFF4048FF)),
                            )),
                        ElevatedButton(
                            style: ElevatedButton.styleFrom(
                                minimumSize: Size(260.0, 40.0),
                                side: BorderSide(color: Colors.grey),
                                primary: Color(0xFF4048FF),
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(50.0),
                                )),
                            onPressed: () async {
                              await LoginController.apiService.removeToken(Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? '', crypto.contract!);
                              setState(() {
                                Provider.of<WalletController>(context, listen: false).cryptos.remove(crypto);
                                _cryptoList.clear();
                                _cryptoList = Provider.of<WalletController>(context, listen: false).cryptos.toSet().toList();
                                _cryptoListGrouped.clear();
                                _cryptoListGrouped = groupCryptos(_cryptoList);
                              });

                              Navigator.pushReplacement(context, MaterialPageRoute(builder: (_) => WelcomeScreen()));
                            },
                            child: Text(
                              "Delete",
                              style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.047, fontWeight: FontWeight.w700, color: Colors.white),
                            )),
                      ],
                    ),
                  ),
                ));
          });
        });
  }
}
