import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:ihave/controllers/LoginController.dart';
import 'package:ihave/controllers/walletController.dart';
import 'package:ihave/service/apiService.dart';
import 'package:ihave/ui/WelcomeScreen.dart';
import 'package:ihave/ui/activationMail.dart';
import 'package:ihave/ui/dashboard.dart';
import 'package:ihave/ui/passPhrase.dart';
import 'package:ihave/ui/socialMail.dart';
import 'package:ihave/ui/transactionpassword.dart';
import 'package:local_auth/local_auth.dart';
import 'package:page_transition/page_transition.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:ihave/ui/signin.dart';
import 'package:provider/provider.dart';
import 'package:localstorage/localstorage.dart';
import 'package:animated_widgets/animated_widgets.dart';

import '../util/utils.dart';
import 'creatwallet.dart';

class SplachScreen extends StatefulWidget {
  @override
  _SplachScreenState createState() => _SplachScreenState();
}

class _SplachScreenState extends State<SplachScreen> with TickerProviderStateMixin {
  AnimationController? controller;

  bool get isForwardAnimation => controller!.status == AnimationStatus.forward || controller!.status == AnimationStatus.completed;
  String? token;
  String balance = "";
  bool _exceptionExist = false;
  int? _splashDuration;
  bool _tokenExist = false;
  bool _userLoggedIn = false;
  bool _userNotLoggedIn = false;
  bool _sessionSaved = false;
  bool _displayAnimation = false;
  bool isBiometricalAuthentificated = false;
  final LocalStorage storage = new LocalStorage('localstorage_app');

  Future getCoinGeckoList() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var result = await LoginController.apiService.getIdCoin();
    if (result != "error") {
      prefs.setString("coin-gecko", result);
    }
    var encodedMap = prefs.getString('my_string_key');
  }

  getSigninState() async {
    SharedPreferences.getInstance().then((sharedPrefValue) async {
      Stopwatch stopwatch = new Stopwatch()..start();
      try {
        token = sharedPrefValue.getString('access_token');
        if (token != null && token != "") {
          setState(() {
            _tokenExist = true;
          });
          await LoginController.apiService.getUserSavedSession(token, version);
          bool userLoggedIn = (LoginController.apiService.walletAddress != null && LoginController.apiService.walletAddress != "");
          if (userLoggedIn) {
            await Provider.of<LoginController>(context, listen: false).loginFromSavedSession(token.toString());
            if (LoginController.apiService.hasBiometric) {
              isBiometricalAuthentificated = await LoginController.apiService.authenticate();
            }
            Provider.of<LoginController>(context, listen: false).userDetails?.userToken = token.toString();
            if (Provider.of<LoginController>(context, listen: false).userDetails?.userToken != null && Provider.of<LoginController>(context, listen: false).userDetails?.userToken != "") {
              setState(() {
                _userLoggedIn = true;
                _splashDuration = stopwatch.elapsed.inSeconds;
              });
            }
            return true;
          } else {
            reinitUserDetails();
            gotoPageGlobal(context, Signin());
          }
          return false;
        } else {
          reinitUserDetails();
          gotoPageGlobal(context, Signin());
        }
      } catch (e) {
        print(e);
        setState(() {
          _userLoggedIn = false;
          _tokenExist = false;
          _exceptionExist = true;
          reinitUserDetails();
          gotoPageGlobal(context, Signin());
        });
      }
    });
  }

  @override
  void initState() {
    super.initState();
    Provider.of<WalletController>(context, listen: false).selectedIndex = 2;
    getCoinGeckoList();
    Timer(Duration(seconds: 1), () {
      setState(() {
        _displayAnimation = true;
      });
    });
    _userLoggedIn = false;
    _exceptionExist = false;
    getSigninState();
    Provider.of<LoginController>(context, listen: false).tokenSymbol == "";
    Provider.of<LoginController>(context, listen: false).tokenNetwork == "";
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: loginUI(),
      ),
    );
  }

  neededUI(LoginController model, Widget page) {
    WidgetsBinding.instance!.addPostFrameCallback((_) {
      Navigator.pushReplacement(context, MaterialPageRoute(builder: (_) => page));
    });
  }

  loginUI() {
    var model = Provider.of<LoginController>(context, listen: false);
    if (Platform.isAndroid) {
      if ((model.userDetails?.userToken != null) && (model.userDetails?.userToken != "null") && (model.userDetails?.userToken != "error") && (model.userDetails?.userToken != "") && (_tokenExist)) {
        if (!LoginController.apiService.isComplete && model.userDetails?.idSn != null) {
          return Center(
            child: neededUI(model, SocialMail()),
          );
        } else {
          if (LoginController.apiService.isEnabled) {
            if (LoginController.apiService.walletAddress != null && LoginController.apiService.walletAddress != "") {
              if (LoginController.apiService.isCompletedPassPhrase) {
                if (LoginController.apiService.hasBiometric) {
                  if (isBiometricalAuthentificated) {
                    return Center(
                      child: neededUI(model, WelcomeScreen()),
                    );
                  } else {
                    LoginController.apiService.authenticate().then((value) {
                      setState(() {
                        isBiometricalAuthentificated = value;
                      });
                    });
                    return Center(
                      child: neededUI(model, Signin()),
                    );
                  }
                } else {
                  return Center(
                    child: neededUI(model, WelcomeScreen()),
                  );
                }
              } else {
                return Center(
                  child: neededUI(model, PassPhrase()),
                );
              }
            } else {
              return Center(
                child: neededUI(model, TransactionPassword()),
              );
            }
          } else {
            return Center(
              child: neededUI(model, ActivationMail()),
            );
          }
        }
      } else {
        return Scaffold(
          body: Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
              colors: [
                Color(0xFF797FFF),
                Color(0xFF0F0F23),
                Color(0xFF0F0F23),
                Color(0xFF0F0F23),
              ],
              begin: const FractionalOffset(0.0, 0.0),
              end: const FractionalOffset(0.0, 1.0),
              stops: [
                0.0,
                0.0,
                0.0,
                1.0,
              ],
              tileMode: TileMode.clamp,
            )),
            child: OpacityAnimatedWidget.tween(
              opacityEnabled: 1,
              opacityDisabled: 0.4,
              enabled: _displayAnimation,
              child: Align(
                alignment: Alignment.center,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SvgPicture.asset(
                      "images/Group 151.svg",
                      width: MediaQuery.of(context).size.width * 0.2,
                      height: MediaQuery.of(context).size.width * 0.2,
                    ),
                    SizedBox(
                      height: 40,
                    ),
                    SvgPicture.asset(
                      "images/Group 153.svg",
                      width: MediaQuery.of(context).size.width * 0.2,
                      height: MediaQuery.of(context).size.width * 0.2,
                    ),
                    SizedBox(
                      height: 70,
                    ),
                    SizedBox(
                      height: 23,
                      width: 23,
                      child: _displayAnimation
                          ? CircularProgressIndicator(
                              color: Colors.white,
                            )
                          : null,
                    )
                  ],
                ),
              ),
            ),
          ),
        );
      }
    } else {
      return Center(
        child: neededUI(model, Signin()),
      );
    }
  }
}
