import 'dart:io';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_file_dialog/flutter_file_dialog.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:ihave/ui/WelcomeScreen.dart';
import 'package:ihave/ui/kyc.dart';
import 'package:ihave/ui/staticElements/headerNavigation.dart';
import 'package:ihave/widgets/customAppBar.dart';
import 'package:ihave/widgets/customBottomBar.dart';
import 'package:path_provider/path_provider.dart';
import 'package:pinput/pin_put/pin_put.dart';
import 'package:provider/provider.dart';

import '../controllers/LoginController.dart';
import '../controllers/walletController.dart';
import '../service/apiService.dart';
import '../util/utils.dart';
import '../widgets/alertDialogWidget.dart';

class ExportKey extends StatefulWidget {
  const ExportKey({Key? key}) : super(key: key);

  @override
  _ExportKeyState createState() => _ExportKeyState();
}

class _ExportKeyState extends State<ExportKey> {
  TextEditingController _transactionPassword = new TextEditingController();
  TextEditingController transactionPassword = TextEditingController();
  final FocusNode _pinPutFocusNode = FocusNode();
  final TextEditingController _pinPutController = TextEditingController();
  bool _isLoadingTRON = false;
  bool _isVisible = false;
  bool _isProfileVisible = false;
  bool _isWalletVisible = false;
  bool _isNotificationVisible = false;
  bool _obscureText = true;
  bool _obscureText2 = true;
  bool _isLoadingBTC = false;
  bool _isLoadingETH = false;
  File? _blockchainKey;
  bool _isLoadingKEYBTC = false;
  bool _isLoadingKEYETH = false;
  bool _isLoadingKEYTRON = false;
  bool _iswrong = false;
  bool passwrong = false;
  bool _iswrongETH = false;
  bool isPinPutEdited = false;
  bool isPinPutfocused = false;
  bool _isCorrectCode = false;
  bool isConfirmPasswordEdited = false;
  bool isActivatedButton = false;
  bool isConfirmedPassword = false;
  bool successCode = false;
  bool errorCode = false;
  final _formKeyGlobal = GlobalKey<FormState>();
  bool _loading = false;
  TextEditingController _WalletPassword = TextEditingController();

  Future<String> get directoryPath async {
    Directory? directory = await getTemporaryDirectory();
    return directory.path;
  }

  BoxDecoration get _pinPutDecoration {
    return BoxDecoration(
      color: Colors.white,
      borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.033),
      border: Border.all(
        color: Colors.white,
      ),
    );
  }

  String? firebaseToken = " ";
  APIService apiService = LoginController.apiService;

  tronWallet(BuildContext context) async {
    setState(() {
      _loading = false;
    });

    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return StatefulBuilder(builder: (context, setState) {
            return Scaffold(
              resizeToAvoidBottomInset: true,
              body: Dialog(
                backgroundColor: Colors.transparent,
                insetPadding: EdgeInsets.zero,
                child: SingleChildScrollView(
                  child: Container(
                    height: MediaQuery.of(context).size.height,
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      color: Colors.white.withOpacity(0.9),
                    ),
                    child: Column(
                      children: <Widget>[
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Spacer(),
                            Spacer(),
                            Padding(
                              padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.01, top: MediaQuery.of(context).size.width * 0.06),
                              child: IconButton(
                                alignment: Alignment.topLeft,
                                onPressed: () {
                                  Navigator.of(context).pop();
                                  return setState(() {
                                    _isLoadingTRON = false;
                                  });
                                },
                                splashColor: Colors.transparent,
                                highlightColor: Colors.transparent,
                                icon: Icon(
                                  Icons.close,
                                  size: MediaQuery.of(context).size.width * 0.05,
                                  color: Colors.black.withOpacity(0.5),
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: MediaQuery.of(context).size.height * 0.035,
                        ),
                        Image.asset(
                          "images/tron-wallet.png",
                        ),
                        Text(
                          "Create your Tron wallet",
                          style: GoogleFonts.poppins(fontWeight: FontWeight.w700, fontSize: MediaQuery.of(context).size.height * 0.025, color: Colors.black),
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.075, right: MediaQuery.of(context).size.width * 0.075, top: MediaQuery.of(context).size.height * 0.01),
                          child: Text(
                            "You are about to delete this token. You now have the possibility to create your Tron ​​wallet.You Need to just enter your transactional password to create it ",
                            textAlign: TextAlign.center,
                            style: GoogleFonts.poppins(fontWeight: FontWeight.w400, fontSize: MediaQuery.of(context).size.height * 0.0175, color: const Color(0XFF162746), letterSpacing: 1.1),
                          ),
                        ),
                        Align(
                          alignment: Alignment.topLeft,
                          child: Padding(
                            padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.1, top: MediaQuery.of(context).size.height * 0.035),
                            child: Text(
                              "TRANSACTION PASSWORD",
                              style: GoogleFonts.poppins(fontWeight: FontWeight.w600, color: const Color(0XFF75758F)),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: MediaQuery.of(context).size.height * 0.01,
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width * 0.8,
                          child: TextFormField(
                            style: TextStyle(fontSize: MediaQuery.of(context).size.width * 0.045),
                            textAlignVertical: TextAlignVertical.center,
                            textAlign: TextAlign.left,
                            controller: _WalletPassword,
                            keyboardType: TextInputType.visiblePassword,
                            obscureText: _obscureText,
                            onChanged: (value) {
                              TextSelection previousSelection = _WalletPassword.selection;
                              _WalletPassword.text = value;
                              _WalletPassword.selection = previousSelection;

                              TextSelection.fromPosition(TextPosition(offset: _WalletPassword.text.length));
                            },
                            validator: (value) {
                              if (value!.isEmpty) {
                                return 'Field required';
                              }
                              return null;
                            },
                            decoration: InputDecoration(
                                fillColor: Colors.white,
                                focusColor: Colors.white,
                                hoverColor: Colors.white,
                                filled: true,
                                isDense: true,
                                prefixIcon: Padding(
                                  padding: EdgeInsets.all(MediaQuery.of(context).size.width * 0.017),
                                  child: SvgPicture.asset(
                                    "images/keyIcon.svg",
                                    color: Colors.orange,
                                    height: MediaQuery.of(context).size.width * 0.06,
                                    width: MediaQuery.of(context).size.width * 0.06,
                                  ),
                                ),
                                suffixIcon: Padding(
                                  padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.025),
                                  child: IconButton(
                                      onPressed: () {
                                        setState(() {
                                          _obscureText = !_obscureText;
                                        });
                                      },
                                      icon: _obscureText
                                          ? SvgPicture.asset(
                                              "images/visibility-icon-off.svg",
                                              height: MediaQuery.of(context).size.height * 0.02,
                                            )
                                          : SvgPicture.asset(
                                              "images/visibility-icon-on.svg",
                                              height: MediaQuery.of(context).size.height * 0.02,
                                            )),
                                ),
                                contentPadding: EdgeInsets.symmetric(vertical: 5, horizontal: 5),
                                enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.white)),
                                focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.white)),
                                errorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red)),
                                focusedErrorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red))),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.02),
                          child: SizedBox(
                            width: MediaQuery.of(context).size.width * 0.6,
                            child: ElevatedButton(
                              child: Padding(
                                padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.015, bottom: MediaQuery.of(context).size.height * 0.015),
                                child: _loading
                                    ? CircularProgressIndicator(
                                        color: Colors.white,
                                      )
                                    : Text(
                                        "Create Wallet",
                                        style: GoogleFonts.poppins(fontWeight: FontWeight.w700, fontSize: MediaQuery.of(context).size.height * 0.017),
                                      ),
                              ),
                              onPressed: !_loading
                                  ? () async {
                                      setState(() {
                                        _loading = true;
                                      });
                                      var result = await LoginController.apiService.createTronWallet(Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "", _WalletPassword.text);
                                      switch (result) {
                                        case "success":
                                          Navigator.of(context).pop();
                                          Fluttertoast.showToast(msg: "Wallet tron created successfully", backgroundColor: Color.fromRGBO(0, 151, 117, 1), toastLength: Toast.LENGTH_LONG);
                                          return setState(() {
                                            _isLoadingTRON = false;
                                          });

                                        case "exist":
                                          Navigator.of(context).pop();
                                          Fluttertoast.showToast(msg: "You have a tron wallet", backgroundColor: Color(0xFFF52079), toastLength: Toast.LENGTH_LONG);
                                          return setState(() {
                                            _isLoadingTRON = false;
                                          });
                                        case "wrong password":
                                          setState(() {
                                            _WalletPassword.text = "";
                                            _loading = false;
                                          });
                                          Fluttertoast.showToast(msg: "Wrong password", backgroundColor: Color(0xFFF52079), toastLength: Toast.LENGTH_LONG);
                                          break;
                                        case "error":
                                          Navigator.of(context).pop();
                                          Fluttertoast.showToast(msg: "Something went wrong please try again", backgroundColor: Color(0xFFF52079), toastLength: Toast.LENGTH_LONG);
                                          return setState(() {
                                            _isLoadingTRON = false;
                                          });
                                      }
                                    }
                                  : null,
                              style: ButtonStyle(
                                backgroundColor: MaterialStateProperty.all(
                                  Color(0xFF4048FF),
                                ),
                                shape: MaterialStateProperty.all(
                                  RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.08),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.012),
                          child: SizedBox(
                            width: MediaQuery.of(context).size.width * 0.6,
                            child: ElevatedButton(
                              child: Padding(
                                padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.015, bottom: MediaQuery.of(context).size.height * 0.015),
                                child: Text(
                                  "Skip for now",
                                  style: GoogleFonts.poppins(fontWeight: FontWeight.w700, fontSize: MediaQuery.of(context).size.height * 0.017, color: const Color(0xFF4048FF)),
                                ),
                              ),
                              onPressed: () {
                                Navigator.of(context).pop();
                                return setState(() {
                                  _isLoadingTRON = false;
                                });
                              },
                              style: ButtonStyle(
                                backgroundColor: MaterialStateProperty.all(
                                  Colors.white,
                                ),
                                shape: MaterialStateProperty.all(
                                  RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.08),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            );
          });
        });
  }

  @override
  Widget build(BuildContext context) {
    setState(() {
      _isVisible = Provider.of<WalletController>(context, listen: true).isSideMenuVisible;
      _isProfileVisible = Provider.of<WalletController>(context, listen: true).isProfileVisible;
      _isWalletVisible = Provider.of<WalletController>(context, listen: true).isWalletVisible;
      _isNotificationVisible = Provider.of<WalletController>(context, listen: true).isNotificationVisible;
    });
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: CustomAppBar(),
      bottomNavigationBar: CustomBottomBar(),
      body: WillPopScope(
        onWillPop: () {
          return Future.value(true);
        },
        child: Stack(
          children: <Widget>[
            SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Align(
                    alignment: Alignment.topCenter,
                    child: Container(
                      width: MediaQuery.of(context).size.width * 0.9,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.02, left: MediaQuery.of(context).size.width * 0.01),
                            child: Text(
                              "Export your\nblockchain key",
                              style: Theme.of(context).textTheme.headline2,
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(
                              top: MediaQuery.of(context).size.height * 0.05,
                              left: MediaQuery.of(context).size.width * 0.01,
                            ),
                            child: Text(
                              "Your .JSON file is completely confidential. Do not share it with anyone or leave it in an unprotected location. Of note, your file is accessible via your transaction password.",
                              textAlign: TextAlign.left,
                              style: GoogleFonts.poppins(color: Color(0XFF47526a), fontStyle: FontStyle.normal, fontWeight: FontWeight.w400),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.08, bottom: MediaQuery.of(context).size.height * 0.02),
                            child: Align(
                              alignment: Alignment.topCenter,
                              child: SizedBox(
                                width: MediaQuery.of(context).size.width * 0.7,
                                height: MediaQuery.of(context).size.height * 0.06,
                                child: ElevatedButton(
                                    style: ButtonStyle(
                                      backgroundColor: MaterialStateProperty.all(
                                        Colors.white,
                                      ),
                                      shape: MaterialStateProperty.all(
                                        RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(20),
                                        ),
                                      ),
                                    ),
                                    onPressed: !_isLoadingBTC
                                        ? () async {
                                            setState(() {
                                              _isLoadingBTC = true;
                                              transactionPassword.text = "";
                                              _iswrong = false;
                                              _pinPutController.text = "";
                                              isPinPutEdited = false;
                                            });
                                            var userLegalResult = await LoginController.apiService.getUserLegal(Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "");
                                            if (userLegalResult != "error" || userLegalResult != "Invalid Access Token") {
                                              List legal = userLegalResult;
                                              if (legal.length == 2 && legal[0]["validate"] == true && legal[0]["validate"] == true) {
                                                setState(() {
                                                  _isLoadingBTC = false;
                                                });
                                                await apiService.sendExportCode(Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "", "btc");
                                                showDialog(
                                                    context: context,
                                                    barrierDismissible: false,
                                                    builder: (BuildContext ctx) {
                                                      return StatefulBuilder(builder: (context, setState) {
                                                        setState(() {
                                                          _isLoadingKEYBTC = false;
                                                        });
                                                        return Scaffold(
                                                          body: Container(
                                                            height: MediaQuery.of(context).size.height,
                                                            width: MediaQuery.of(context).size.width,
                                                            decoration: BoxDecoration(
                                                              color: Colors.white,
                                                            ),
                                                            child: SingleChildScrollView(
                                                              child: Column(
                                                                children: <Widget>[
                                                                  SizedBox(
                                                                    height: MediaQuery.of(context).size.height * 0.007,
                                                                  ),
                                                                  Row(
                                                                    children: [
                                                                      Padding(
                                                                        padding: EdgeInsets.only(
                                                                          left: MediaQuery.of(context).size.width * 0.06,
                                                                          top: MediaQuery.of(context).size.height * 0.025,
                                                                        ),
                                                                        child: Text(
                                                                          "Export my wallet",
                                                                          style: GoogleFonts.poppins(fontWeight: FontWeight.w700, fontStyle: FontStyle.normal, letterSpacing: 1.4, fontSize: MediaQuery.of(context).size.height * 0.023),
                                                                        ),
                                                                      ),
                                                                      Spacer(),
                                                                      IconButton(
                                                                        onPressed: () {
                                                                          setState(() {
                                                                            _obscureText = true;
                                                                            _isLoadingKEYBTC = false;
                                                                            _iswrong = false;
                                                                          });
                                                                          Navigator.of(context).pop();
                                                                        },
                                                                        splashColor: Colors.transparent,
                                                                        highlightColor: Colors.transparent,
                                                                        icon: Icon(Icons.close, size: MediaQuery.of(context).size.height * 0.035, color: const Color(0XFF75758F)),
                                                                      ),
                                                                      SizedBox(
                                                                        width: MediaQuery.of(context).size.width * 0.03,
                                                                      )
                                                                    ],
                                                                  ),
                                                                  Padding(
                                                                    padding: EdgeInsets.only(
                                                                      top: MediaQuery.of(context).size.height * 0.05,
                                                                    ),
                                                                    child: SvgPicture.asset("images/export-json.svg"),
                                                                  ),
                                                                  Padding(
                                                                    padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.035, left: MediaQuery.of(context).size.width * 0.075, right: MediaQuery.of(context).size.width * 0.075),
                                                                    child: Text(
                                                                      "Enter your password to export your account in JSON Keystore format",
                                                                      textAlign: TextAlign.center,
                                                                      style: GoogleFonts.poppins(fontWeight: FontWeight.w600, color: Color(0XFF000000), fontStyle: FontStyle.normal, fontSize: MediaQuery.of(context).size.height * 0.018, height: 1.2),
                                                                    ),
                                                                  ),
                                                                  SizedBox(
                                                                    height: MediaQuery.of(context).size.height * 0.065,
                                                                  ),
                                                                  SizedBox(
                                                                    width: MediaQuery.of(context).size.width * 0.8,
                                                                    child: Padding(
                                                                      padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.015),
                                                                      child: Container(
                                                                        decoration: BoxDecoration(
                                                                          color: Color(0xFFF6F6FE),
                                                                          border: Border.all(
                                                                            color: Colors.transparent,
                                                                          ),
                                                                          borderRadius: BorderRadius.all(Radius.circular(20)),
                                                                        ),
                                                                        child: Padding(
                                                                          padding: const EdgeInsets.all(8.0),
                                                                          child: Column(
                                                                            mainAxisAlignment: MainAxisAlignment.start,
                                                                            crossAxisAlignment: CrossAxisAlignment.start,
                                                                            children: [
                                                                              Center(
                                                                                child: Padding(
                                                                                  padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height * 0.007),
                                                                                  child: Text(
                                                                                    "Authentication code",
                                                                                    style: GoogleFonts.poppins(
                                                                                      fontSize: 18,
                                                                                      fontWeight: FontWeight.w600,
                                                                                      color: (isPinPutEdited) ? ((_isCorrectCode) ? Color(0xFF00CC9E) : Color(0xFFF52079)) : Color(0xFF4048FF),
                                                                                    ),
                                                                                  ),
                                                                                ),
                                                                              ),
                                                                              Padding(
                                                                                padding: const EdgeInsets.fromLTRB(15, 5, 15, 5),
                                                                                child: Form(
                                                                                  key: _formKeyGlobal,
                                                                                  child: PinPut(
                                                                                      controller: _pinPutController,
                                                                                      fieldsCount: 6,
                                                                                      focusNode: _pinPutFocusNode,
                                                                                      onChanged: (content) async {
                                                                                        isPinPutfocused = true;
                                                                                        if (_pinPutController.text.length == 6) {
                                                                                          isPinPutEdited = true;
                                                                                        }
                                                                                      },
                                                                                      textStyle: TextStyle(
                                                                                        fontSize: MediaQuery.of(context).size.width * 0.05,
                                                                                        fontWeight: FontWeight.bold,
                                                                                        color: (isPinPutEdited) ? ((_isCorrectCode) ? Color(0xFF00CC9E) : Color(0xFFF52079)) : Color(0xFF4048FF),
                                                                                      ),
                                                                                      eachFieldHeight: MediaQuery.of(context).size.width * 0.1,
                                                                                      eachFieldWidth: MediaQuery.of(context).size.width * 0.1,
                                                                                      onSubmit: (String pin) => _showSnackBar(pin, context),
                                                                                      submittedFieldDecoration: _pinPutDecoration.copyWith(
                                                                                          border: Border.all(
                                                                                            color: isPinPutfocused ? ((isPinPutEdited) ? ((_isCorrectCode) ? Color(0xFF00CC9E) : Color(0xFFF52079)) : Color(0xFF4048FF)) : Color(0xFF4048FF),
                                                                                          ),
                                                                                          borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.042)),
                                                                                      selectedFieldDecoration: _pinPutDecoration.copyWith(
                                                                                          color: Colors.white,
                                                                                          borderRadius: BorderRadius.circular(15.0),
                                                                                          border: Border.all(
                                                                                            color: isPinPutfocused ? ((isPinPutEdited) ? ((_isCorrectCode) ? Color(0xFF00CC9E) : Color(0xFFF52079)) : Color(0xFF4048FF)) : Color(0xFF4048FF),
                                                                                          )),
                                                                                      followingFieldDecoration: _pinPutDecoration.copyWith(
                                                                                        borderRadius: BorderRadius.circular(5.0),
                                                                                        border: Border.all(
                                                                                          color: isPinPutfocused ? ((isPinPutEdited) ? ((_isCorrectCode) ? Color(0xFF00CC9E) : Color(0xFFF52079)) : Color(0xFF4048FF)) : Colors.grey,
                                                                                        ),
                                                                                      )),
                                                                                ),
                                                                              ),
                                                                              Center(
                                                                                child: Padding(
                                                                                  padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height * 0.007),
                                                                                  child: Text(
                                                                                    isPinPutEdited ? (_isCorrectCode ? "Code Match" : "Incorrect Code") : "This code will only be valid for 5 min",
                                                                                    style: GoogleFonts.poppins(
                                                                                      fontSize: 14,
                                                                                      fontWeight: FontWeight.w700,
                                                                                      color: isPinPutEdited ? (_isCorrectCode ? Color(0xFF00CC9E) : Color(0xFFF52079)) : Color(0xFF75758F),
                                                                                    ),
                                                                                  ),
                                                                                ),
                                                                              ),
                                                                            ],
                                                                          ),
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ),
                                                                  _iswrong
                                                                      ? Padding(
                                                                          padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.012),
                                                                          child: Text(
                                                                            "Wrong Password",
                                                                            style: GoogleFonts.poppins(
                                                                              color: Color(0xFFd63384),
                                                                              fontWeight: FontWeight.w500,
                                                                              fontSize: MediaQuery.of(context).size.height * 0.015,
                                                                              fontStyle: FontStyle.normal,
                                                                            ),
                                                                          ),
                                                                        )
                                                                      : SizedBox.shrink(),
                                                                  Padding(
                                                                    padding: EdgeInsets.only(top: !_iswrong ? MediaQuery.of(context).size.height * 0.05 : MediaQuery.of(context).size.height * 0.02),
                                                                    child: SizedBox(
                                                                      width: MediaQuery.of(context).size.width * 0.80,
                                                                      child: ElevatedButton(
                                                                        style: ButtonStyle(
                                                                          minimumSize: MaterialStateProperty.all(Size(MediaQuery.of(context).size.width * 0.78, MediaQuery.of(context).size.height * 0.06)),
                                                                          backgroundColor: MaterialStateProperty.all(
                                                                            Color(0XFF4048ff),
                                                                          ),
                                                                          shape: MaterialStateProperty.all(
                                                                            RoundedRectangleBorder(
                                                                              borderRadius: BorderRadius.circular(30),
                                                                            ),
                                                                          ),
                                                                        ),
                                                                        onPressed: () async {
                                                                          if (_pinPutController.text.length != 0) {
                                                                            setState(() {
                                                                              _isLoadingKEYBTC = true;
                                                                            });
                                                                            var result = await LoginController.apiService.getBlockchainKeyBTC(Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "", _pinPutController.text);
                                                                            if (result == "wallet not found") {
                                                                              setState(() {
                                                                                _isLoadingKEYBTC = false;
                                                                                _iswrong = false;
                                                                              });
                                                                              Fluttertoast.showToast(msg: "Wallet not found");
                                                                            } else if (result == "wrong password") {
                                                                              setState(() {
                                                                                _isLoadingKEYBTC = false;
                                                                                _iswrong = true;
                                                                                _isCorrectCode = false;
                                                                              });
                                                                            } else if (result == "error") {
                                                                              setState(() {
                                                                                _isLoadingKEYBTC = false;
                                                                                _iswrong = false;
                                                                              });
                                                                            } else {
                                                                              setState(() {
                                                                                _isCorrectCode = true;
                                                                              });
                                                                              final path = await directoryPath;
                                                                              _blockchainKey = File('$path/blockchain-key.json');
                                                                              await _blockchainKey?.writeAsString(result);
                                                                              final params = SaveFileDialogParams(sourceFilePath: _blockchainKey!.path);
                                                                              final filePath = await FlutterFileDialog.saveFile(params: params);
                                                                              if (filePath != null) {
                                                                                setState(() {
                                                                                  _obscureText = true;
                                                                                  _isLoadingKEYBTC = false;
                                                                                });
                                                                                Navigator.of(context).pop();
                                                                                Fluttertoast.showToast(msg: "you have successfully downloaded your blockchain key");
                                                                              } else {
                                                                                setState(() {
                                                                                  _isLoadingKEYBTC = false;
                                                                                });
                                                                                Fluttertoast.showToast(msg: "you have aborted the download");
                                                                              }
                                                                            }
                                                                          }
                                                                        },
                                                                        child: !_isLoadingKEYBTC
                                                                            ? Center(
                                                                                child: Row(
                                                                                  children: [
                                                                                    const Spacer(),
                                                                                    SvgPicture.asset(
                                                                                      "images/download.svg",
                                                                                      height: MediaQuery.of(context).size.height * 0.02,
                                                                                      width: MediaQuery.of(context).size.height * 0.02,
                                                                                    ),
                                                                                    SizedBox(
                                                                                      width: MediaQuery.of(context).size.width * 0.03,
                                                                                    ),
                                                                                    Text(
                                                                                      "Download",
                                                                                      style: GoogleFonts.poppins(color: Colors.white, fontStyle: FontStyle.normal, fontWeight: FontWeight.w600, fontSize: MediaQuery.of(context).size.height * 0.0185),
                                                                                    ),
                                                                                    const Spacer()
                                                                                  ],
                                                                                ),
                                                                              )
                                                                            : SizedBox(
                                                                                height: MediaQuery.of(context).size.height * 0.02,
                                                                                width: MediaQuery.of(context).size.width * 0.02,
                                                                                child: CircularProgressIndicator(
                                                                                  color: Colors.white,
                                                                                ),
                                                                              ),
                                                                      ),
                                                                    ),
                                                                  ),
                                                                  Container(
                                                                    width: MediaQuery.of(context).viewInsets.bottom.toString() == "0.0" ? MediaQuery.of(context).size.width * 0.02 : MediaQuery.of(context).size.height * 0.2,
                                                                    height: MediaQuery.of(context).viewInsets.bottom.toString() == "0.0" ? MediaQuery.of(context).size.width * 0.02 : MediaQuery.of(context).size.height * 0.1,
                                                                  )
                                                                ],
                                                              ),
                                                            ),
                                                          ),
                                                        );
                                                      });
                                                    });
                                              } else {
                                                setState(() {
                                                  _isLoadingBTC = false;
                                                });
                                                showDialog(
                                                    context: context,
                                                    barrierDismissible: false,
                                                    builder: (BuildContext context) {
                                                      return StatefulBuilder(builder: (context, setState) {
                                                        return Dialog(
                                                          backgroundColor: Colors.transparent,
                                                          insetPadding: EdgeInsets.zero,
                                                          child: Container(
                                                            height: MediaQuery.of(context).size.height,
                                                            width: MediaQuery.of(context).size.width,
                                                            decoration: BoxDecoration(
                                                              color: Colors.white,
                                                            ),
                                                            child: Column(
                                                              children: <Widget>[
                                                                Row(
                                                                  crossAxisAlignment: CrossAxisAlignment.center,
                                                                  mainAxisAlignment: MainAxisAlignment.start,
                                                                  children: [
                                                                    Spacer(),
                                                                    Spacer(),
                                                                    Padding(
                                                                      padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.01, top: MediaQuery.of(context).size.width * 0.06),
                                                                      child: IconButton(
                                                                        alignment: Alignment.topLeft,
                                                                        onPressed: () {
                                                                          Navigator.of(context).pop();
                                                                        },
                                                                        splashColor: Colors.transparent,
                                                                        highlightColor: Colors.transparent,
                                                                        icon: Icon(
                                                                          Icons.close,
                                                                          size: MediaQuery.of(context).size.width * 0.05,
                                                                          color: Colors.black.withOpacity(0.5),
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ],
                                                                ),
                                                                Padding(
                                                                  padding: EdgeInsets.only(
                                                                    top: MediaQuery.of(context).size.height * 0.2,
                                                                  ),
                                                                  child: SvgPicture.asset("images/KYC.svg"),
                                                                ),
                                                                Padding(
                                                                  padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.03, bottom: MediaQuery.of(context).size.height * 0.023),
                                                                  child: Text(
                                                                    "Please verify your account",
                                                                    style: GoogleFonts.poppins(
                                                                      fontWeight: FontWeight.w700,
                                                                      fontStyle: FontStyle.normal,
                                                                      color: Color(0XFF1F2337),
                                                                      fontSize: MediaQuery.of(context).size.height * 0.025,
                                                                    ),
                                                                  ),
                                                                ),
                                                                Text(
                                                                  "To perform this transaction",
                                                                  style: GoogleFonts.poppins(color: Color(0XFF47526A), fontSize: MediaQuery.of(context).size.height * 0.017, fontWeight: FontWeight.w500),
                                                                ),
                                                                Text(
                                                                  "Proceed to KYC page",
                                                                  style: GoogleFonts.poppins(color: Color(0XFF47526A), fontSize: MediaQuery.of(context).size.height * 0.017, fontWeight: FontWeight.w500),
                                                                ),
                                                                Padding(
                                                                  padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.012),
                                                                  child: SizedBox(
                                                                    width: MediaQuery.of(context).size.width * 0.4,
                                                                    child: ElevatedButton(
                                                                      child: Text(
                                                                        "KYC",
                                                                        style: GoogleFonts.poppins(fontWeight: FontWeight.w900, fontSize: MediaQuery.of(context).size.height * 0.017),
                                                                      ),
                                                                      onPressed: () {
                                                                        Navigator.of(context).push(new MaterialPageRoute(builder: (context) => KycPage()));
                                                                      },
                                                                      style: ButtonStyle(
                                                                        backgroundColor: MaterialStateProperty.all(
                                                                          Color(0xFF4048FF),
                                                                        ),
                                                                        shape: MaterialStateProperty.all(
                                                                          RoundedRectangleBorder(
                                                                            borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.08),
                                                                          ),
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ),
                                                                ),
                                                              ],
                                                            ),
                                                          ),
                                                        );
                                                      });
                                                    });
                                              }
                                            } else {
                                              setState(() {
                                                _isLoadingBTC = false;
                                              });
                                              displayDialog(context, "Error", "Something went wrong, please try again");
                                            }
                                          }
                                        : null,
                                    child: !_isLoadingBTC
                                        ? Text(
                                            "Download the BTC key",
                                            style: GoogleFonts.poppins(color: Color(0XFF4048FF), fontWeight: FontWeight.w600, fontSize: MediaQuery.of(context).size.height * 0.015),
                                          )
                                        : SizedBox(
                                            height: MediaQuery.of(context).size.height * 0.017,
                                            width: MediaQuery.of(context).size.height * 0.017,
                                            child: CircularProgressIndicator(
                                              color: Color(0XFF4048FF),
                                            ),
                                          )),
                              ),
                            ),
                          ),
                          Align(
                            alignment: Alignment.topCenter,
                            child: SizedBox(
                              width: MediaQuery.of(context).size.width * 0.7,
                              height: MediaQuery.of(context).size.height * 0.06,
                              child: ElevatedButton(
                                  style: ButtonStyle(
                                    backgroundColor: MaterialStateProperty.all(
                                      Colors.white,
                                    ),
                                    shape: MaterialStateProperty.all(
                                      RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(20),
                                      ),
                                    ),
                                  ),
                                  onPressed: !_isLoadingETH
                                      ? () async {
                                          setState(() {
                                            _isLoadingETH = true;
                                            passwrong = false;
                                            _transactionPassword.text = "";
                                            _pinPutController.text = "";
                                            isPinPutEdited = false;
                                          });
                                          var userLegalResult = await LoginController.apiService.getUserLegal(Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "");
                                          if (userLegalResult != "error" || userLegalResult != "Invalid Access Token") {
                                            List legal = userLegalResult;
                                            if (legal.length == 2 && legal[0]["validate"] == true && legal[0]["validate"] == true) {
                                              setState(() {
                                                _isLoadingETH = false;
                                              });
                                              await apiService.sendExportCode(Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "", "eth");
                                              showDialog(
                                                  context: context,
                                                  barrierDismissible: false,
                                                  builder: (BuildContext context) {
                                                    return StatefulBuilder(builder: (context, setState) {
                                                      return Scaffold(
                                                        body: Container(
                                                          height: MediaQuery.of(context).size.height,
                                                          width: MediaQuery.of(context).size.width,
                                                          decoration: BoxDecoration(
                                                            color: Colors.white,
                                                          ),
                                                          child: SingleChildScrollView(
                                                            child: Column(
                                                              children: <Widget>[
                                                                SizedBox(
                                                                  height: MediaQuery.of(context).size.height * 0.007,
                                                                ),
                                                                Row(
                                                                  children: [
                                                                    Padding(
                                                                      padding: EdgeInsets.only(
                                                                        left: MediaQuery.of(context).size.width * 0.06,
                                                                        top: MediaQuery.of(context).size.height * 0.025,
                                                                      ),
                                                                      child: Text(
                                                                        "Export my wallet",
                                                                        style: GoogleFonts.poppins(fontWeight: FontWeight.w700, fontStyle: FontStyle.normal, letterSpacing: 1.4, fontSize: MediaQuery.of(context).size.height * 0.023),
                                                                      ),
                                                                    ),
                                                                    const Spacer(),
                                                                    IconButton(
                                                                      alignment: Alignment.topLeft,
                                                                      onPressed: () {
                                                                        setState(() {
                                                                          _obscureText2 = true;
                                                                          _transactionPassword.text = "";
                                                                        });
                                                                        Navigator.of(context).pop();
                                                                      },
                                                                      splashColor: Colors.transparent,
                                                                      highlightColor: Colors.transparent,
                                                                      icon: Icon(Icons.close, size: MediaQuery.of(context).size.height * 0.035, color: const Color(0XFF75758F)),
                                                                    ),
                                                                    SizedBox(
                                                                      width: MediaQuery.of(context).size.width * 0.03,
                                                                    )
                                                                  ],
                                                                ),
                                                                Padding(
                                                                  padding: EdgeInsets.only(
                                                                    top: MediaQuery.of(context).size.height * 0.05,
                                                                  ),
                                                                  child: SvgPicture.asset("images/export-json.svg"),
                                                                ),
                                                                Padding(
                                                                  padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.035, left: MediaQuery.of(context).size.width * 0.075, right: MediaQuery.of(context).size.width * 0.075),
                                                                  child: Text(
                                                                    "Enter your password to export your account in JSON Keystore format",
                                                                    textAlign: TextAlign.center,
                                                                    style: GoogleFonts.poppins(fontWeight: FontWeight.w600, color: Color(0XFF000000), fontStyle: FontStyle.normal, fontSize: MediaQuery.of(context).size.height * 0.018, height: 1.2),
                                                                  ),
                                                                ),
                                                                SizedBox(
                                                                  height: MediaQuery.of(context).size.height * 0.065,
                                                                ),
                                                                SizedBox(
                                                                    width: MediaQuery.of(context).size.width * 0.8,
                                                                    child: Padding(
                                                                      padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.015),
                                                                      child: Container(
                                                                        decoration: BoxDecoration(
                                                                          color: Color(0xFFF6F6FE),
                                                                          border: Border.all(
                                                                            color: Colors.transparent,
                                                                          ),
                                                                          borderRadius: BorderRadius.all(Radius.circular(20)),
                                                                        ),
                                                                        child: Padding(
                                                                          padding: const EdgeInsets.all(8.0),
                                                                          child: Column(
                                                                            mainAxisAlignment: MainAxisAlignment.start,
                                                                            crossAxisAlignment: CrossAxisAlignment.start,
                                                                            children: [
                                                                              Center(
                                                                                child: Padding(
                                                                                  padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height * 0.007),
                                                                                  child: Text(
                                                                                    "Authentication code",
                                                                                    style: GoogleFonts.poppins(
                                                                                      fontSize: 18,
                                                                                      fontWeight: FontWeight.w600,
                                                                                      color: (isPinPutEdited) ? ((_isCorrectCode) ? Color(0xFF00CC9E) : Color(0xFFF52079)) : Color(0xFF4048FF),
                                                                                    ),
                                                                                  ),
                                                                                ),
                                                                              ),
                                                                              Padding(
                                                                                padding: const EdgeInsets.fromLTRB(15, 5, 15, 5),
                                                                                child: Form(
                                                                                  key: _formKeyGlobal,
                                                                                  child: PinPut(
                                                                                      controller: _pinPutController,
                                                                                      fieldsCount: 6,
                                                                                      focusNode: _pinPutFocusNode,
                                                                                      onChanged: (content) async {
                                                                                        isPinPutfocused = true;
                                                                                        if (_pinPutController.text.length == 6) {
                                                                                          isPinPutEdited = true;
                                                                                        }
                                                                                      },
                                                                                      textStyle: TextStyle(
                                                                                        fontSize: MediaQuery.of(context).size.width * 0.05,
                                                                                        fontWeight: FontWeight.bold,
                                                                                        color: (isPinPutEdited) ? ((_isCorrectCode) ? Color(0xFF00CC9E) : Color(0xFFF52079)) : Color(0xFF4048FF),
                                                                                      ),
                                                                                      eachFieldHeight: MediaQuery.of(context).size.width * 0.1,
                                                                                      eachFieldWidth: MediaQuery.of(context).size.width * 0.1,
                                                                                      onSubmit: (String pin) => _showSnackBar(pin, context),
                                                                                      submittedFieldDecoration: _pinPutDecoration.copyWith(
                                                                                          border: Border.all(
                                                                                            color: isPinPutfocused ? ((isPinPutEdited) ? ((_isCorrectCode) ? Color(0xFF00CC9E) : Color(0xFFF52079)) : Color(0xFF4048FF)) : Color(0xFF4048FF),
                                                                                          ),
                                                                                          borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.042)),
                                                                                      selectedFieldDecoration: _pinPutDecoration.copyWith(
                                                                                          color: Colors.white,
                                                                                          borderRadius: BorderRadius.circular(15.0),
                                                                                          border: Border.all(
                                                                                            color: isPinPutfocused ? ((isPinPutEdited) ? ((_isCorrectCode) ? Color(0xFF00CC9E) : Color(0xFFF52079)) : Color(0xFF4048FF)) : Color(0xFF4048FF),
                                                                                          )),
                                                                                      followingFieldDecoration: _pinPutDecoration.copyWith(
                                                                                        borderRadius: BorderRadius.circular(5.0),
                                                                                        border: Border.all(
                                                                                          color: isPinPutfocused ? ((isPinPutEdited) ? ((_isCorrectCode) ? Color(0xFF00CC9E) : Color(0xFFF52079)) : Color(0xFF4048FF)) : Colors.grey,
                                                                                        ),
                                                                                      )),
                                                                                ),
                                                                              ),
                                                                              Center(
                                                                                child: Padding(
                                                                                  padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height * 0.007),
                                                                                  child: Text(
                                                                                    isPinPutEdited ? (_isCorrectCode ? "Code Match" : "Incorrect Code") : "This code will only be valid for 5 min",
                                                                                    style: GoogleFonts.poppins(
                                                                                      fontSize: 14,
                                                                                      fontWeight: FontWeight.w700,
                                                                                      color: isPinPutEdited ? (_isCorrectCode ? Color(0xFF00CC9E) : Color(0xFFF52079)) : Color(0xFF75758F),
                                                                                    ),
                                                                                  ),
                                                                                ),
                                                                              ),
                                                                            ],
                                                                          ),
                                                                        ),
                                                                      ),
                                                                    )),
                                                                Center(
                                                                  child: passwrong
                                                                      ? Padding(
                                                                          padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.012),
                                                                          child: Text(
                                                                            "Wrong Password",
                                                                            style: GoogleFonts.poppins(
                                                                              color: Color(0xFFd63384),
                                                                              fontWeight: FontWeight.w500,
                                                                              fontSize: MediaQuery.of(context).size.height * 0.015,
                                                                              fontStyle: FontStyle.normal,
                                                                            ),
                                                                          ),
                                                                        )
                                                                      : null,
                                                                ),
                                                                Padding(
                                                                  padding: EdgeInsets.only(top: !passwrong ? MediaQuery.of(context).size.height * 0.05 : MediaQuery.of(context).size.height * 0.02),
                                                                  child: SizedBox(
                                                                    width: MediaQuery.of(context).size.width * 0.8,
                                                                    child: ElevatedButton(
                                                                        style: ButtonStyle(
                                                                          minimumSize: MaterialStateProperty.all(Size(MediaQuery.of(context).size.width * 0.78, MediaQuery.of(context).size.height * 0.06)),
                                                                          backgroundColor: MaterialStateProperty.all(
                                                                            Color(0XFF4048ff),
                                                                          ),
                                                                          shape: MaterialStateProperty.all(
                                                                            RoundedRectangleBorder(
                                                                              borderRadius: BorderRadius.circular(30),
                                                                            ),
                                                                          ),
                                                                        ),
                                                                        onPressed: () async {
                                                                          if (_pinPutController.text.isNotEmpty) {
                                                                            var result = await LoginController.apiService.getBlockchainKey(Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "", _pinPutController.text);
                                                                            if (result == "wallet not found") {
                                                                              Fluttertoast.showToast(msg: "Wallet not found");
                                                                            } else if (result == "wrong password") {
                                                                              setState(() {
                                                                                passwrong = true;
                                                                                _isCorrectCode = false;
                                                                              });
                                                                            } else if (result == "error") {
                                                                              Fluttertoast.showToast(msg: "Key derivation failed - possibly wrong password");
                                                                            } else {
                                                                              setState(() {
                                                                                _isCorrectCode = true;
                                                                              });
                                                                              final path = await directoryPath;
                                                                              _blockchainKey = File('$path/blockchain-key.json');
                                                                              await _blockchainKey?.writeAsString(result);
                                                                              final params = SaveFileDialogParams(sourceFilePath: _blockchainKey!.path);
                                                                              final filePath = await FlutterFileDialog.saveFile(params: params);
                                                                              if (filePath != null) {
                                                                                setState(() {
                                                                                  _obscureText2 = true;
                                                                                });
                                                                                Navigator.of(context).pop();
                                                                                Fluttertoast.showToast(msg: "you have successfully downloaded your blockchain key");
                                                                              } else {
                                                                                Fluttertoast.showToast(msg: "you have aborted the download");
                                                                              }
                                                                            }
                                                                          }
                                                                        },
                                                                        child: Center(
                                                                          child: Row(
                                                                            mainAxisAlignment: MainAxisAlignment.center,
                                                                            crossAxisAlignment: CrossAxisAlignment.center,
                                                                            children: [
                                                                              SvgPicture.asset("images/download.svg"),
                                                                              Text(
                                                                                "  Download",
                                                                                style: GoogleFonts.poppins(
                                                                                  color: Colors.white,
                                                                                  fontStyle: FontStyle.normal,
                                                                                  fontWeight: FontWeight.w600,
                                                                                ),
                                                                              ),
                                                                            ],
                                                                          ),
                                                                        )),
                                                                  ),
                                                                ),
                                                                Container(
                                                                  width: MediaQuery.of(context).viewInsets.bottom.toString() == "0.0" ? MediaQuery.of(context).size.width * 0.02 : MediaQuery.of(context).size.height * 0.2,
                                                                  height: MediaQuery.of(context).viewInsets.bottom.toString() == "0.0" ? MediaQuery.of(context).size.width * 0.01 : MediaQuery.of(context).size.height * 0.1,
                                                                )
                                                              ],
                                                            ),
                                                          ),
                                                        ),
                                                      );
                                                    });
                                                  });
                                            } else {
                                              setState(() {
                                                _isLoadingETH = false;
                                              });
                                              showDialog(
                                                  context: context,
                                                  barrierDismissible: false,
                                                  builder: (BuildContext context) {
                                                    return StatefulBuilder(builder: (context, setState) {
                                                      return Dialog(
                                                        backgroundColor: Colors.transparent,
                                                        insetPadding: EdgeInsets.zero,
                                                        child: Container(
                                                          height: MediaQuery.of(context).size.height,
                                                          width: MediaQuery.of(context).size.width,
                                                          decoration: BoxDecoration(
                                                            color: Colors.white.withOpacity(0.8),
                                                          ),
                                                          child: Column(
                                                            children: <Widget>[
                                                              Row(
                                                                crossAxisAlignment: CrossAxisAlignment.center,
                                                                mainAxisAlignment: MainAxisAlignment.start,
                                                                children: [
                                                                  Spacer(),
                                                                  Spacer(),
                                                                  Padding(
                                                                    padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.01, top: MediaQuery.of(context).size.width * 0.06),
                                                                    child: IconButton(
                                                                      alignment: Alignment.topLeft,
                                                                      onPressed: () {
                                                                        Navigator.of(context).pop();
                                                                      },
                                                                      splashColor: Colors.transparent,
                                                                      highlightColor: Colors.transparent,
                                                                      icon: Icon(
                                                                        Icons.close,
                                                                        size: MediaQuery.of(context).size.width * 0.05,
                                                                        color: Colors.black.withOpacity(0.5),
                                                                      ),
                                                                    ),
                                                                  ),
                                                                ],
                                                              ),
                                                              Padding(
                                                                padding: EdgeInsets.only(
                                                                  top: MediaQuery.of(context).size.height * 0.2,
                                                                ),
                                                                child: SvgPicture.asset("images/KYC.svg"),
                                                              ),
                                                              Padding(
                                                                padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.03, bottom: MediaQuery.of(context).size.height * 0.023),
                                                                child: Text(
                                                                  "Please verify your account",
                                                                  style: GoogleFonts.poppins(
                                                                    fontWeight: FontWeight.w700,
                                                                    fontStyle: FontStyle.normal,
                                                                    color: Color(0XFF1F2337),
                                                                    fontSize: MediaQuery.of(context).size.height * 0.025,
                                                                  ),
                                                                ),
                                                              ),
                                                              Text(
                                                                "To perform this transaction",
                                                                style: GoogleFonts.poppins(color: Color(0XFF47526A), fontSize: MediaQuery.of(context).size.height * 0.017, fontWeight: FontWeight.w500),
                                                              ),
                                                              Text(
                                                                "Proceed to KYC page",
                                                                style: GoogleFonts.poppins(color: Color(0XFF47526A), fontSize: MediaQuery.of(context).size.height * 0.017, fontWeight: FontWeight.w500),
                                                              ),
                                                              Padding(
                                                                padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.012),
                                                                child: SizedBox(
                                                                  width: MediaQuery.of(context).size.width * 0.4,
                                                                  child: ElevatedButton(
                                                                    child: Text(
                                                                      "KYC",
                                                                      style: GoogleFonts.poppins(fontWeight: FontWeight.w900, fontSize: MediaQuery.of(context).size.height * 0.017),
                                                                    ),
                                                                    onPressed: () {
                                                                      Navigator.of(context).push(new MaterialPageRoute(builder: (context) => KycPage()));
                                                                    },
                                                                    style: ButtonStyle(
                                                                      backgroundColor: MaterialStateProperty.all(
                                                                        Color(0xFF4048FF),
                                                                      ),
                                                                      shape: MaterialStateProperty.all(
                                                                        RoundedRectangleBorder(
                                                                          borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.08),
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ),
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                      );
                                                    });
                                                  });
                                            }
                                          } else {
                                            setState(() {
                                              _isLoadingETH = false;
                                            });
                                            displayDialog(context, "Error", "Something went wrong, please try again");
                                          }
                                        }
                                      : null,
                                  child: !_isLoadingETH
                                      ? Text(
                                          "Download the ETH key",
                                          style: GoogleFonts.poppins(color: Color(0XFF4048FF), fontWeight: FontWeight.w600, fontSize: MediaQuery.of(context).size.height * 0.015),
                                        )
                                      : SizedBox(
                                          height: MediaQuery.of(context).size.height * 0.017,
                                          width: MediaQuery.of(context).size.height * 0.017,
                                          child: CircularProgressIndicator(
                                            color: Color(0XFF4048FF),
                                          ),
                                        )),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            HeaderNavigation(_isVisible, _isWalletVisible, _isNotificationVisible)
          ],
        ),
      ),
    );
  }
}

void _showSnackBar(String pin, BuildContext context) {
  final snackBar = SnackBar(
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(18.0),
    ),
    duration: Duration(seconds: 5),
    content: Container(
        height: 80.0,
        color: Colors.grey,
        child: Center(
          child: Text(
            'Pin Submitted. Value: $pin',
            style: TextStyle(fontSize: 25.0, backgroundColor: Colors.grey),
          ),
        )),
    backgroundColor: Colors.grey,
  );
}