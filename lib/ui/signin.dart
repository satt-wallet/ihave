import 'dart:async';
import 'dart:convert';
import 'dart:io' show Platform;
import 'dart:typed_data';
import 'package:collection/collection.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_api_availability/google_api_availability.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:ihave/controllers/LoginController.dart';
import 'package:ihave/controllers/walletController.dart';
import 'package:ihave/model/contact.dart';
import 'package:ihave/model/user.dart';
import 'package:ihave/ui/WelcomeScreen.dart';
import 'package:ihave/ui/passPhrase.dart';
import 'package:ihave/ui/resetPassword.dart';
import 'package:ihave/ui/signup.dart';
import 'package:ihave/ui/socialMail.dart';
import 'package:ihave/ui/towFactorPage.dart';
import 'package:ihave/ui/transactionpassword.dart';
import 'package:ihave/util/Sqflite.dart';
import 'package:ihave/util/contactsHive.dart';
import 'package:ihave/util/utils.dart';
import 'package:ihave/widgets/CustomSlider.dart';
import 'package:ihave/widgets/alertDialogWidget.dart';
import 'package:intl/intl.dart';
import 'package:local_auth/local_auth.dart';
import 'package:majascan/majascan.dart';
import 'package:neoversion/neoversion.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:pinput/pin_put/pin_put.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sign_in_with_apple/sign_in_with_apple.dart';
import '../service/apiService.dart';
import '../util/neoVersionOverriden.dart';
import 'activationMail.dart';

class Signin extends StatefulWidget {
  Signin({Key? key}) : super(key: key);

  @override
  _SigninState createState() => _SigninState();
}

class _SigninState extends State<Signin> {
  final FocusNode _pinPutFocusNode = FocusNode();
  TextEditingController _pinPutController = TextEditingController();
  bool isLoggedIn = false;

  GlobalKey<FormState> _formKeyGlobalSignIn = GlobalKey<FormState>();
  late GlobalKey<FormState> _formKeySignIn;
  List<Login> loginsList = [];
  List<Login> surFix = [];
  bool _stopCaptcha = false;
  bool _loadingResetPassword = false;
  bool _obscureText = true;
  bool isAccountExist = false;
  bool isNotExist = false;
  String userToken = "";
  bool _isvalidFormEmail = false;
  TextEditingController mail = new TextEditingController();
  TextEditingController newPassword = new TextEditingController();
  TextEditingController _emailController = new TextEditingController();
  TextEditingController _passwordController = new TextEditingController();
  String newPasswordConfirm = "";
  bool isExist = false;
  bool isConfirmedPassword = false;
  bool isConfirmPasswordEdited = false;
  bool isConfirmedPass = false;
  bool isResend = false;
  bool isChanged = false;
  bool _isLoading = false;
  bool isEditedEmail = false;
  bool _isButtonActive = true;
  bool _googleLoading = false;
  bool _facebookLoading = false;
  bool _mailFieldForm = false;
  bool _passwordFieldForm = false;
  bool _isLoadingCaptcha = false;
  bool _isButtonActiveCaptcha = true;
  bool userLoggedIn = false;
  String? token;
  bool _isConnected = false;
  bool isWalletExist = false;
  String os = Platform.operatingSystem;
  bool verifyAccount = false;
  bool _isLoadingApple = false;
  bool _isLoginApple = false;
  bool _notFound = false;
  String google = "";
  String facebook = "";
  bool successCode = false;
  bool errorCode = false;

  bool _loadingApple = false;

  List<dynamic> captchaPuzzle = [];
  bool _isLoadingPuzzle = false;
  bool _textDisplay = true;
  bool _textDisplayError = false;
  double _value = 0;
  bool _isSuccessCaptcha = false;
  bool _isWrongCaptcha = false;
  bool _successLogin = false;
  bool _errorLogin = false;
  bool _lockedLogin = false;
  Timer? _timer;
  DateTime date = DateTime.now();
  String _seconds = "";
  String _minutes = "";
  int _startSeconds = 59;
  int _startMinutes = 29;
  bool errorMailOrPass = false;
  bool verifyAccountGoogle = false;

  bool errorAccountExist = false;

  String? text;
  bool checked = true;
  bool _errorSignIn = false;
  late DateTime lockedTime;

  bool searchMail = false;
  late List<Map<dynamic, dynamic>> boxMap;
  bool isClick = false;
  List<Login> loginList = [];
  String savedMail = "";

  FocusNode _mailFocusNode = new FocusNode();
  bool hasFocus = false;

  ScrollController _scrollController = new ScrollController();

  int selectedIndex = -1;
  bool focusReset = false;
  FocusNode resetFocusNode = new FocusNode();

  bool _ihConnectLoading = false;

  Future getCaptcha() async {
    captchaPuzzle = await LoginController.apiService.captcha();
  }

  Future reGetCaptcha() async {
    var result = await LoginController.apiService.captcha();
    if (result != "error") {
      captchaPuzzle = result;
      return captchaPuzzle;
    } else {
      return "error";
    }
  }

  fillContactList() {
    setState(() {
      loginsList.clear();
      boxMap.forEach((element) {
        loginsList.add(Login.fromJson(element));
      });
      surFix.clear();
      surFix.addAll(loginsList);
    });
  }

  void _refreshLogins() async {
    final data = await SQLLOGIN.getItemslogin();
    setState(() {
      boxMap = data;
      _isLoading = false;
    });
    fillContactList();
  }

  Future save() async {
    if (_lockedLogin) {
      cancelTimer();
    }
    var result = "";
    var apiResult = await Provider.of<LoginController>(context, listen: false).mailLogin(user.email, user.password);

    if (apiResult.runtimeType == DateTime) {
      apiResult = apiResult.add(new Duration(minutes: 30));
      setState(() {
        lockedTime = apiResult;
      });
      var difference = apiResult.difference(DateTime.now());
      var minutes = difference.inMinutes;
      var seconds = difference.inSeconds % 60;
      setState(() {
        _startSeconds = seconds;
        _startMinutes = minutes;
      });
      return result = "locked";
    } else {
      userToken = await Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "";
      if (userToken != null && userToken != "" && userToken != "null" && userToken != "error" && userToken != "user not found") {
        return result = "success";
      } else if (userToken == "user not found") {
        return result = "notfound";
      } else {
        return result = "error";
      }
    }
  }

  BoxDecoration get _pinPutDecoration {
    return BoxDecoration(
      color: Colors.white,
      borderRadius: BorderRadius.circular(15.0),
      border: Border.all(
        color: Colors.white,
      ),
    );
  }

  Future getCoinGeckoList() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    var result = await LoginController.apiService.getIdCoin();

    if (result != "error") {
      prefs.setString("coin-gecko", result);
    }
  }

  User user = User('', '');

  void cancelTimer() {
    _timer!.cancel();
  }

  void startTimer() {
    const oneSec = const Duration(seconds: 1);
    _timer = new Timer.periodic(oneSec, (Timer timer) {
      if (mounted) {
        setState(() {
          if (_startSeconds == 0) {
            _startMinutes--;
            _minutes = _startMinutes < 10 ? "0${_startMinutes}" : _startMinutes.toString();
            _startSeconds = 60;
          } else {
            _startSeconds--;
            _seconds = _startSeconds < 10 ? "0${_startSeconds}" : _startSeconds.toString();
            if (_startMinutes <= 0 && _startSeconds <= 0) {
              setState(() {
                _lockedLogin = false;
                _startSeconds = 0;
                _timer!.cancel();
              });
            }
          }
          if (_startMinutes < 0 || _startSeconds < 0) {
            setState(() {
              _lockedLogin = false;
              _startSeconds = 0;
              _timer!.cancel();
            });
          }
        });
      }
    });
  }

  @override
  void initState() {
    _refreshLogins();
    Provider.of<WalletController>(context, listen: false).checkTron = false;
    Provider.of<WalletController>(context, listen: false).selectedIndex = 2;
    LoginController.apiService.walletAddress = "";
    LoginController.apiService.btcWalletAddress = "";
    getCaptcha();
    getCoinGeckoList();
    String formattedDate = DateFormat('yyyy-MM-dd – kk:mm').format(date);
    reinitUserDetails();
    setState(() {
      _value = 0;
      _textDisplay = true;
      _textDisplayError = false;
      _isSuccessCaptcha = false;
      _isWrongCaptcha = false;
      _isLoading = false;
      _isButtonActive = true;
      _successLogin = false;
      _errorLogin = false;
    });
    _mailFocusNode.addListener(() {
      hasFocus = !hasFocus;
    });
    final neoVersion = NeoVersionOverriden(
      iOSAppId: '1610750323',
      androidAppId: 'us.atayen.ihave',
    );
    const simpleBehavior = true;

    if (simpleBehavior) {
      basicStatusCheck(neoVersion);
    } else {
      advancedStatusCheck(neoVersion);
    }
  }

  basicStatusCheck(NeoVersionOverriden neoVersion) {
    neoVersion.showAlertIfNecessary(context: context);
  }

  advancedStatusCheck(NeoVersionOverriden neoVersion) async {
    final status = await neoVersion.getVersionStatus();
    neoVersion.showUpdateDialog(
      context: context,
      status: status,
      title: 'Update',
      dialogText: (String local, String appstore, bool dismissable) => 'Custom Text - local: $local, appstore: $appstore, dismissable: $dismissable',
    );
  }

  @override
  Widget build(BuildContext context) {
    setState(() {
      isNotExist = Provider.of<LoginController>(context, listen: true).resetMailError;
    });
    if (_lockedLogin) {
      var difference = lockedTime.difference(DateTime.now());
      var minutes = difference.inMinutes;
      var seconds = difference.inSeconds % 60;
      _startSeconds = seconds;
      _startMinutes = minutes;
      if (_startMinutes <= 0 && _startSeconds <= 0) {
        setState(() {
          _startSeconds = 0;
          _startMinutes = 0;
          _lockedLogin = false;
        });
      }
    }
    if (surFix.length >= 1 && MediaQuery.of(context).viewInsets.bottom != 0) {
      _scrollController.animateTo(_scrollController.position.maxScrollExtent * 0.6, duration: Duration(milliseconds: 500), curve: Curves.fastOutSlowIn);
    } else if (isEditedEmail) {
      _scrollController.animateTo(_scrollController.position.maxScrollExtent * 0, duration: Duration(milliseconds: 500), curve: Curves.fastOutSlowIn);
    }
    return WillPopScope(
        onWillPop: () async {
          return false;
        },
        child: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                  colors: [
                    const Color(0xFFF52079),
                    const Color(0xFF1F2337),
                  ],
                  begin: const FractionalOffset(0.0, -0.1),
                  end: const FractionalOffset(0.0, 0.35),
                  stops: [0.0, 1.0],
                  tileMode: TileMode.clamp),
            ),
            child: Scaffold(
                backgroundColor: Colors.transparent,
                resizeToAvoidBottomInset: false,
                appBar: null,
                body: GestureDetector(
                  onTap: () {
                    FocusScope.of(context).requestFocus(new FocusNode());
                  },
                  child: SafeArea(
                    child: SingleChildScrollView(
                      controller: _scrollController,
                      child: Column(
                        children: [
                          SizedBox(
                            height: MediaQuery.of(context).size.height * 0.025,
                          ),
                          SvgPicture.asset(
                            'images/logo-auth.svg',
                            alignment: Alignment.center,
                            width: MediaQuery.of(context).size.height * 0.04,
                            height: MediaQuery.of(context).size.height * 0.04,
                          ),
                          SizedBox(
                            height: MediaQuery.of(context).size.height * 0.03,
                          ),
                          Text(
                            "Sign In",
                            style: Theme.of(context).textTheme.headline1,
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.01),
                            child: _lockedLogin
                                ? Text(
                                    "00:${_startMinutes.toString().length == 1 ? "0$_startMinutes" : _startMinutes.toString()}:${_startSeconds.toString().length == 1 ? "0$_startSeconds" : _startSeconds.toString()}",
                                    style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.height * 0.02, fontWeight: FontWeight.w700, fontStyle: FontStyle.normal, color: Colors.red),
                                  )
                                : null,
                          ),
                          SizedBox(
                            height: MediaQuery.of(context).size.height * 0.015,
                          ),
                          SizedBox(
                            child: loginControls(context),
                          ),
                          SizedBox(
                            height: MediaQuery.of(context).size.height * 0.015,
                          ),
                          Row(children: <Widget>[
                            Expanded(
                              child: new Container(
                                  margin: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.06, right: MediaQuery.of(context).size.width * 0.015),
                                  child: Divider(
                                    color: Colors.white,
                                    thickness: 1,
                                    height: 36,
                                  )),
                            ),
                            Text("or", style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.037, fontWeight: FontWeight.w400, color: Colors.white, fontStyle: FontStyle.normal)),
                            Expanded(
                              child: new Container(
                                  margin: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.06, left: MediaQuery.of(context).size.width * 0.015),
                                  child: Divider(
                                    color: Colors.white,
                                    thickness: 1,
                                    height: 36,
                                  )),
                            ),
                          ]),
                          SizedBox(
                            height: MediaQuery.of(context).size.height * 0.025,
                          ),
                          Stack(children: [
                            Column(children: [
                              Center(
                                child: AutofillGroup(
                                  child: Column(
                                    children: [
                                      Container(
                                        width: MediaQuery.of(context).size.width * 0.75,
                                        child: Center(
                                          child: TextFormField(
                                            cursorHeight: MediaQuery.of(context).size.height * 0.03,
                                            autofillHints: [AutofillHints.email],
                                            keyboardType: TextInputType.emailAddress,
                                            focusNode: _mailFocusNode,
                                            style: GoogleFonts.poppins(color: Colors.black, fontWeight: FontWeight.w600, fontStyle: FontStyle.normal, fontSize: MediaQuery.of(context).size.height * 0.021),
                                            controller: _emailController,
                                            onChanged: (value) {
                                              searchbook;
                                              _notFound = false;
                                              errorAccountExist = false;
                                              TextSelection previousSelection = _emailController.selection;
                                              _emailController.text = value;
                                              user.email = _emailController.text;
                                              _emailController.selection = previousSelection;
                                              if (_emailController.text.isNotEmpty && RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(_emailController.text)) {
                                                setState(() {
                                                  _mailFieldForm = true;
                                                });
                                              } else {
                                                setState(() {
                                                  _mailFieldForm = false;
                                                });
                                              }
                                              TextSelection.fromPosition(TextPosition(offset: _emailController.text.length));
                                              if (_emailController.text.length > 2) {
                                                searchSavedMail(_emailController.text);
                                                setState(() {
                                                  searchMail = true;
                                                });
                                              } else {
                                                setState(() {
                                                  searchMail = false;
                                                });
                                              }
                                            },
                                            validator: (value) {
                                              if (value!.isEmpty) {
                                                return 'Field required ';
                                              } else if (RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(value)) {
                                                return null;
                                              } else {
                                                return 'Field Email Address must have a valid form';
                                              }
                                            },
                                            inputFormatters: [
                                              FilteringTextInputFormatter.deny(RegExp(r'\s')),
                                            ],
                                            decoration: InputDecoration(
                                                suffixIcon: _emailController.text != ""
                                                    ? Padding(
                                                        padding: EdgeInsets.only(
                                                          right: MediaQuery.of(context).size.width * 0.045,
                                                          top: _mailFieldForm ? MediaQuery.of(context).size.height * 0.0135 : MediaQuery.of(context).size.height * 0.018,
                                                          bottom: _mailFieldForm ? MediaQuery.of(context).size.height * 0.0135 : MediaQuery.of(context).size.height * 0.018,
                                                        ),
                                                        child: Visibility(
                                                          child: !_mailFieldForm
                                                              ? InkWell(
                                                                  onTap: () {
                                                                    _emailController.text = "";
                                                                  },
                                                                  child: SvgPicture.asset(
                                                                    "images/false-figma.svg",
                                                                  ),
                                                                )
                                                              : SvgPicture.asset(
                                                                  "images/check-figma.svg",
                                                                ),
                                                        ),
                                                      )
                                                    : Padding(
                                                        padding: const EdgeInsets.all(0.0),
                                                        child: Text(''),
                                                      ),
                                                contentPadding: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.height * 0.025, vertical: MediaQuery.of(context).size.height * 0.015),
                                                hintText: 'EMAIL',
                                                hintStyle: Theme.of(context).textTheme.headline3,
                                                fillColor: (_emailController.text.length > 0) ? (_mailFieldForm ? Colors.white : Colors.white.withGreen(990)) : (Colors.white),
                                                focusColor: Colors.white,
                                                hoverColor: Colors.white,
                                                filled: true,
                                                isDense: true,
                                                enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.white)),
                                                focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.white)),
                                                errorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red)),
                                                focusedErrorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red))),
                                          ),
                                        ),
                                      ),
                                      Visibility(
                                        visible: (_emailController.text.length > 0),
                                        child: Padding(
                                          padding: !_mailFieldForm ? EdgeInsets.only(top: MediaQuery.of(context).size.width * 0.02, right: MediaQuery.of(context).size.width * 0.045) : EdgeInsets.all(0.0),
                                          child: !_mailFieldForm
                                              ? Align(
                                                  alignment: Alignment.topLeft,
                                                  child: Padding(
                                                    padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.14),
                                                    child: Text(
                                                      'Field Email Address must have  a valid form',
                                                      style: GoogleFonts.poppins(color: Color(0xFFF52079), fontSize: MediaQuery.of(context).size.height * 0.015, fontWeight: FontWeight.bold),
                                                    ),
                                                  ),
                                                )
                                              : null,
                                        ),
                                      ),
                                      SizedBox(height: MediaQuery.of(context).size.height * 0.03),
                                      Center(
                                        child: Container(
                                          width: MediaQuery.of(context).size.width * 0.75,
                                          child: Center(
                                            child: TextFormField(
                                              autofillHints: [AutofillHints.password],
                                              onEditingComplete: () => TextInput.finishAutofillContext(),
                                              style: Theme.of(context).textTheme.headline4,
                                              controller: _passwordController,
                                              keyboardType: TextInputType.visiblePassword,
                                              obscureText: _obscureText,
                                              onChanged: (value) {
                                                errorAccountExist = false;
                                                TextSelection previousSelection = _passwordController.selection;
                                                _passwordController.text = value;
                                                user.password = _passwordController.text;
                                                _passwordController.selection = previousSelection;
                                                if (_passwordController.text.isNotEmpty) {
                                                  setState(() {
                                                    _passwordFieldForm = true;
                                                  });
                                                } else {
                                                  setState(() {
                                                    _passwordFieldForm = false;
                                                  });
                                                }
                                                TextSelection.fromPosition(TextPosition(offset: value.length));
                                              },
                                              validator: (value) {
                                                if (value!.isEmpty) {
                                                  return 'Field required';
                                                }
                                                return null;
                                              },
                                              cursorHeight: MediaQuery.of(context).size.height * 0.03,
                                              decoration: InputDecoration(
                                                  contentPadding: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.height * 0.025, vertical: MediaQuery.of(context).size.height * 0.015),
                                                  hintText: 'PASSWORD',
                                                  hintStyle: Theme.of(context).textTheme.headline3,
                                                  fillColor: Colors.white,
                                                  focusColor: Colors.white,
                                                  hoverColor: Colors.white,
                                                  filled: true,
                                                  isDense: true,
                                                  border: InputBorder.none,
                                                  suffixStyle: TextStyle(
                                                    color: Colors.grey,
                                                  ),
                                                  suffixIcon: Padding(
                                                    padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.025),
                                                    child: IconButton(
                                                        onPressed: () {
                                                          setState(() {
                                                            _obscureText = !_obscureText;
                                                          });
                                                        },
                                                        icon: _obscureText
                                                            ? SvgPicture.asset(
                                                                "images/visibility-icon-off.svg",
                                                                height: MediaQuery.of(context).size.height * 0.018,
                                                              )
                                                            : SvgPicture.asset(
                                                                "images/visibility-icon-on.svg",
                                                                height: MediaQuery.of(context).size.height * 0.02,
                                                              )),
                                                  ),
                                                  enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.white)),
                                                  focusedBorder: OutlineInputBorder(
                                                    borderRadius: BorderRadius.circular(60),
                                                    borderSide: BorderSide(color: Colors.white),
                                                  ),
                                                  errorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red)),
                                                  focusedErrorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red))),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              if (_errorSignIn)
                                Padding(
                                  padding: EdgeInsets.all(MediaQuery.of(context).size.width * 0.01),
                                  child: Container(
                                      width: MediaQuery.of(context).size.width * 0.78,
                                      height: MediaQuery.of(context).size.height * 0.06,
                                      decoration: BoxDecoration(
                                        color: Colors.pink.withOpacity(0.2),
                                        borderRadius: BorderRadius.all(Radius.circular(20)),
                                      ),
                                      child: Padding(
                                        padding: EdgeInsets.all(MediaQuery.of(context).size.width * 0.005),
                                        child: Row(
                                          children: [
                                            Padding(
                                              padding: EdgeInsets.all(MediaQuery.of(context).size.width * 0.01),
                                              child: SvgPicture.asset("images/cross-pink.svg"),
                                            ),
                                            Text("Something went wrong,\nPlease try again !",
                                                style: GoogleFonts.poppins(
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.pink,
                                                  fontSize: MediaQuery.of(context).size.width * 0.033,
                                                )),
                                          ],
                                        ),
                                      )),
                                ),
                              Padding(
                                padding: errorMailOrPass ? EdgeInsets.all(MediaQuery.of(context).size.width * 0.01) : EdgeInsets.all(0),
                                child: errorMailOrPass
                                    ? Container(
                                        width: MediaQuery.of(context).size.width * 0.75,
                                        height: MediaQuery.of(context).size.height * 0.06,
                                        decoration: BoxDecoration(
                                          color: Colors.pink.withOpacity(0.2),
                                          borderRadius: BorderRadius.all(Radius.circular(30)),
                                        ),
                                        child: Padding(
                                          padding: EdgeInsets.all(MediaQuery.of(context).size.width * 0.005),
                                          child: Row(
                                            children: [
                                              Padding(
                                                padding: EdgeInsets.all(MediaQuery.of(context).size.width * 0.01),
                                                child: SvgPicture.asset("images/cross-pink.svg"),
                                              ),
                                              Text("Incorrect password",
                                                  style: GoogleFonts.poppins(
                                                    fontWeight: FontWeight.bold,
                                                    color: Colors.pink,
                                                    fontSize: MediaQuery.of(context).size.width * 0.033,
                                                  )),
                                            ],
                                          ),
                                        ))
                                    : errorAccountExist
                                        ? Container(
                                            width: MediaQuery.of(context).size.width * 0.75,
                                            height: MediaQuery.of(context).size.height * 0.06,
                                            decoration: BoxDecoration(
                                              color: Colors.pink.withOpacity(0.2),
                                              borderRadius: BorderRadius.all(Radius.circular(30)),
                                            ),
                                            child: Padding(
                                              padding: const EdgeInsets.all(2.0),
                                              child: Row(
                                                children: [
                                                  Padding(
                                                    padding: const EdgeInsets.all(6.0),
                                                    child: SvgPicture.asset("images/cross-pink.svg"),
                                                  ),
                                                  Text("Register First",
                                                      style: GoogleFonts.poppins(
                                                        fontWeight: FontWeight.bold,
                                                        color: Colors.pink,
                                                        fontSize: 12,
                                                      )),
                                                ],
                                              ),
                                            ))
                                        : (_lockedLogin
                                            ? Padding(
                                                padding: const EdgeInsets.all(12.0),
                                                child: Container(
                                                    width: MediaQuery.of(context).size.width * 0.75,
                                                    decoration: BoxDecoration(
                                                      color: Colors.pink.withOpacity(0.2),
                                                      borderRadius: BorderRadius.all(Radius.circular(20)),
                                                    ),
                                                    child: Padding(
                                                      padding: const EdgeInsets.all(2.0),
                                                      child: Row(
                                                        children: [
                                                          Padding(
                                                            padding: const EdgeInsets.all(3.0),
                                                            child: SvgPicture.asset("images/cross-pink.svg"),
                                                          ),
                                                          Text("Your account has been locked following\n to too many failed login attempts.\n Please wait until the countdown is over.",
                                                              style: GoogleFonts.poppins(
                                                                fontWeight: FontWeight.bold,
                                                                color: Color.fromRGBO(245, 32, 121, 1),
                                                                fontSize: 12,
                                                              )),
                                                        ],
                                                      ),
                                                    )),
                                              )
                                            : _notFound
                                                ? Container(
                                                    width: MediaQuery.of(context).size.width * 0.75,
                                                    height: MediaQuery.of(context).size.height * 0.06,
                                                    decoration: BoxDecoration(
                                                      color: Colors.pink.withOpacity(0.2),
                                                      borderRadius: BorderRadius.all(Radius.circular(30)),
                                                    ),
                                                    child: Padding(
                                                      padding: EdgeInsets.all(MediaQuery.of(context).size.width * 0.005),
                                                      child: Row(
                                                        children: [
                                                          Padding(
                                                            padding: EdgeInsets.all(MediaQuery.of(context).size.width * 0.01),
                                                            child: SvgPicture.asset("images/cross-pink.svg"),
                                                          ),
                                                          Text("invalid email address",
                                                              style: GoogleFonts.poppins(
                                                                fontWeight: FontWeight.bold,
                                                                color: Colors.pink,
                                                                fontSize: MediaQuery.of(context).size.width * 0.033,
                                                              )),
                                                        ],
                                                      ),
                                                    ))
                                                : null),
                              ),
                              Padding(
                                padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.03),
                                child: (_mailFieldForm && _passwordFieldForm)
                                    ? Container(
                                        height: MediaQuery.of(context).size.height * 0.06,
                                        width: MediaQuery.of(context).size.width * 0.65,
                                        child: ElevatedButton(
                                            style: ButtonStyle(
                                              backgroundColor: MaterialStateProperty.all(
                                                Color(0xFF00cc9e),
                                              ),
                                              shape: MaterialStateProperty.all(
                                                RoundedRectangleBorder(
                                                  borderRadius: BorderRadius.circular(30),
                                                ),
                                              ),
                                            ),
                                            onPressed: _isButtonActive
                                                ? () async {
                                                    Provider.of<LoginController>(context, listen: false).kyc = [];
                                                    Provider.of<LoginController>(context, listen: false).userPicture = null;
                                                    Provider.of<LoginController>(context, listen: false).Balance = "";
                                                    setState(() {
                                                      _stopCaptcha = false;
                                                      _errorSignIn = false;
                                                      _notFound = false;
                                                      _isButtonActive = false;
                                                      _isLoading = false;
                                                      _value = 0;
                                                      _textDisplay = true;
                                                      _textDisplayError = false;
                                                      _isSuccessCaptcha = false;
                                                      _isWrongCaptcha = false;
                                                      _isLoading = true;
                                                      _lockedLogin = false;
                                                      errorMailOrPass = false;
                                                    });
                                                    if (captchaPuzzle.isNotEmpty) {
                                                      var originalPuzzle = captchaPuzzle[0].toString().substring(22);
                                                      var puzzleImage = captchaPuzzle[1].toString().substring(22);
                                                      var idPuzzleImage = captchaPuzzle[2].toString();
                                                      Uint8List _bytesOriginalImage = base64Decode(originalPuzzle);
                                                      Uint8List _bytesPuzzleImage = base64Decode(puzzleImage);
                                                      showGeneralDialog(
                                                          barrierDismissible: false,
                                                          transitionBuilder: (context, animation, secondaryAnimation, child) {
                                                            return FadeTransition(
                                                              opacity: animation,
                                                              child: ScaleTransition(
                                                                scale: animation,
                                                                child: child,
                                                              ),
                                                            );
                                                          },
                                                          transitionDuration: Duration(milliseconds: 100),
                                                          context: context,
                                                          pageBuilder: (context, animation, secondaryAnimation) {
                                                            return StatefulBuilder(builder: (context, setState) {
                                                              return Scaffold(
                                                                body: SafeArea(
                                                                  child: Container(
                                                                    height: MediaQuery.of(context).size.height,
                                                                    child: Column(
                                                                      children: <Widget>[
                                                                        Row(
                                                                          children: <Widget>[
                                                                            Padding(
                                                                              padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.025),
                                                                              child: Text(
                                                                                "Security Verification",
                                                                                style: GoogleFonts.poppins(color: Colors.black, fontWeight: FontWeight.w700, fontSize: MediaQuery.of(context).size.height * 0.02),
                                                                              ),
                                                                            ),
                                                                            Spacer(),
                                                                            Padding(
                                                                              padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.012),
                                                                              child: IconButton(
                                                                                  onPressed: () async {
                                                                                    Navigator.of(context).pop();
                                                                                    setState(() {
                                                                                      _stopCaptcha = false;
                                                                                    });
                                                                                    captchaPuzzle = await LoginController.apiService.captcha();
                                                                                    return stateInitalisation(context);
                                                                                  },
                                                                                  icon: Icon(
                                                                                    Icons.close,
                                                                                    color: Colors.black,
                                                                                    size: MediaQuery.of(context).size.height * 0.03,
                                                                                  )),
                                                                            ),
                                                                          ],
                                                                        ),
                                                                        const Spacer(),
                                                                        Stack(
                                                                          children: [
                                                                            Image.memory(
                                                                              _bytesOriginalImage,
                                                                              height: 155,
                                                                              width: 310,
                                                                            ),
                                                                            Positioned(
                                                                              right: -_value,
                                                                              child: Image.memory(
                                                                                _bytesPuzzleImage,
                                                                                height: 155,
                                                                                width: 310,
                                                                                color: _isSuccessCaptcha ? Colors.green : (_isWrongCaptcha ? Colors.redAccent : null),
                                                                              ),
                                                                            ),
                                                                            Positioned(
                                                                              right: -MediaQuery.of(context).size.width * 0.02,
                                                                              child: Padding(
                                                                                padding: const EdgeInsets.all(8.0),
                                                                                child: IconButton(
                                                                                  onPressed: () async {
                                                                                    captchaPuzzle = await LoginController.apiService.captcha();
                                                                                    originalPuzzle = captchaPuzzle[0].toString().substring(22);
                                                                                    puzzleImage = captchaPuzzle[1].toString().substring(22);
                                                                                    idPuzzleImage = captchaPuzzle[2].toString();
                                                                                    setState(() {
                                                                                      _bytesPuzzleImage = base64Decode(puzzleImage);
                                                                                      _bytesOriginalImage = base64Decode(originalPuzzle);
                                                                                      _stopCaptcha = false;
                                                                                      _textDisplay = true;
                                                                                      _value = 0;
                                                                                      _textDisplayError = false;
                                                                                      _isSuccessCaptcha = false;
                                                                                      _isWrongCaptcha = false;
                                                                                    });
                                                                                  },
                                                                                  icon: Icon(
                                                                                    Icons.refresh,
                                                                                    color: Colors.white,
                                                                                    size: 30,
                                                                                  ),
                                                                                ),
                                                                              ),
                                                                            ),
                                                                          ],
                                                                        ),
                                                                        const Spacer(),
                                                                        Padding(
                                                                          padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.01),
                                                                          child: !_isSuccessCaptcha
                                                                              ? Stack(
                                                                                  children: [
                                                                                    SliderTheme(
                                                                                      data: SliderTheme.of(context).copyWith(
                                                                                        trackHeight: 40,
                                                                                        inactiveTrackColor: const Color(0XFFF6F6FF),
                                                                                        inactiveTickMarkColor: Color(0xFFF5F5F5),
                                                                                        activeTrackColor: Color(0xFFF5F5F5),
                                                                                        activeTickMarkColor: Color(0xFFF5F5F5),
                                                                                        valueIndicatorColor: Colors.white,
                                                                                        thumbColor: _isSuccessCaptcha ? Colors.green : (_isWrongCaptcha ? const Color(0XFFF52079) : const Color(0XFFD6D6E8)),
                                                                                        thumbShape: !_isWrongCaptcha ? CustomSliderThumbRect(min: 0, max: 247, thumbRadius: 0, thumbHeight: 55) : CustomSliderThumbRectWrong(min: 0, max: 247, thumbRadius: 0, thumbHeight: 55),
                                                                                      ),
                                                                                      child: SizedBox(
                                                                                        width: 350,
                                                                                        child: Slider(
                                                                                          value: _value,
                                                                                          onChanged: (value) {
                                                                                            if (value >= 247) {
                                                                                            } else {
                                                                                              setState(() {
                                                                                                _value = value.roundToDouble();
                                                                                                _textDisplay = false;
                                                                                                _textDisplayError = false;
                                                                                                _isSuccessCaptcha = false;
                                                                                                _isWrongCaptcha = false;
                                                                                              });
                                                                                            }
                                                                                          },
                                                                                          max: 247,
                                                                                          min: 0,
                                                                                          onChangeStart: (value) {
                                                                                            setState(() {
                                                                                              _value = value;
                                                                                              _textDisplay = false;
                                                                                              _textDisplayError = false;
                                                                                              _isSuccessCaptcha = false;
                                                                                              _isWrongCaptcha = false;
                                                                                            });
                                                                                          },
                                                                                          onChangeEnd: (value) async {
                                                                                            setState(() {
                                                                                              _stopCaptcha = true;
                                                                                              _textDisplay = false;
                                                                                            });
                                                                                            LoginController.apiService.verifyCaptcha(captchaPuzzle[2].toString(), value.toInt().toString()).then((result) {
                                                                                              switch (result) {
                                                                                                case "success":
                                                                                                  {
                                                                                                    setState(() {
                                                                                                      _isSuccessCaptcha = true;
                                                                                                    });
                                                                                                    save().then((value) {
                                                                                                      switch (value) {
                                                                                                        case "success":
                                                                                                          {
                                                                                                            Future.wait([
                                                                                                              LoginController.apiService.checkValidation(Provider.of<LoginController>(context, listen: false).userDetails!.userToken.toString()),
                                                                                                            ]).then((value) {
                                                                                                              if (value[0] == "success" || value == "success") {
                                                                                                                Provider.of<LoginController>(context, listen: false).userDetails?.idSn = LoginController.apiService.idSn.toString();
                                                                                                                Navigator.of(context).pop();
                                                                                                                loginUI();
                                                                                                              } else {
                                                                                                                setState(() {
                                                                                                                  _errorSignIn = true;
                                                                                                                });
                                                                                                                stateInitalisation(context);
                                                                                                                Navigator.of(context).pop();
                                                                                                              }
                                                                                                            }).catchError((error) async {
                                                                                                              captchaPuzzle = await LoginController.apiService.captcha();
                                                                                                              setState(() {
                                                                                                                _errorSignIn = true;
                                                                                                                _notFound = false;
                                                                                                                _lockedLogin = false;
                                                                                                                errorMailOrPass = false;
                                                                                                                _stopCaptcha = false;
                                                                                                              });
                                                                                                              Navigator.of(context).pop();

                                                                                                              stateInitalisation(context);
                                                                                                            });
                                                                                                          }
                                                                                                          break;
                                                                                                        case "error":
                                                                                                          {
                                                                                                            setState(() {
                                                                                                              errorMailOrPass = true;
                                                                                                              _stopCaptcha = false;
                                                                                                              _errorSignIn = false;
                                                                                                              _notFound = false;
                                                                                                              _lockedLogin = false;
                                                                                                            });
                                                                                                            stateInitalisation(context);
                                                                                                            Navigator.of(context).pop();
                                                                                                          }
                                                                                                          break;
                                                                                                        case "locked":
                                                                                                          {
                                                                                                            setState(() {
                                                                                                              errorAccountExist = false;
                                                                                                              errorMailOrPass = false;
                                                                                                              _errorSignIn = false;
                                                                                                              _notFound = false;
                                                                                                              _stopCaptcha = false;
                                                                                                              _lockedLogin = true;
                                                                                                            });
                                                                                                            stateInitalisation(context);
                                                                                                            Navigator.of(context).pop();
                                                                                                          }
                                                                                                          break;
                                                                                                        case "notfound":
                                                                                                          {
                                                                                                            setState(() {
                                                                                                              errorAccountExist = false;
                                                                                                              errorMailOrPass = false;
                                                                                                              _errorSignIn = false;
                                                                                                              _notFound = true;
                                                                                                              _stopCaptcha = false;
                                                                                                              _lockedLogin = false;
                                                                                                            });
                                                                                                            stateInitalisation(context);
                                                                                                            Navigator.of(context).pop();
                                                                                                          }
                                                                                                          break;
                                                                                                      }
                                                                                                    }).catchError((error) {
                                                                                                      setState(() {
                                                                                                        _errorSignIn = true;
                                                                                                        _notFound = false;
                                                                                                        _lockedLogin = false;
                                                                                                        errorMailOrPass = false;
                                                                                                        _stopCaptcha = false;
                                                                                                      });
                                                                                                      stateInitalisation(context);
                                                                                                      Navigator.of(context).pop();
                                                                                                    });
                                                                                                  }
                                                                                                  break;
                                                                                                case "error":
                                                                                                  {
                                                                                                    setState(() {
                                                                                                      _value = 0;
                                                                                                      _textDisplay = false;
                                                                                                      _textDisplayError = true;
                                                                                                      _isWrongCaptcha = true;
                                                                                                    });
                                                                                                    LoginController.apiService.captcha().then((value) {
                                                                                                      if (value != "error") {
                                                                                                        captchaPuzzle = value;
                                                                                                        setState(() {
                                                                                                          _bytesPuzzleImage = base64Decode(captchaPuzzle[1].toString().substring(22));
                                                                                                          _bytesOriginalImage = base64Decode(captchaPuzzle[0].toString().substring(22));
                                                                                                          _value = 0;
                                                                                                        });
                                                                                                        Future.delayed(const Duration(milliseconds: 300), () {
                                                                                                          setState(() {
                                                                                                            _value = 0;
                                                                                                            _isWrongCaptcha = false;
                                                                                                            _textDisplay = true;
                                                                                                            _textDisplayError = false;
                                                                                                            _stopCaptcha = false;
                                                                                                          });
                                                                                                        });
                                                                                                      } else {
                                                                                                        setState(() {
                                                                                                          _stopCaptcha = false;
                                                                                                          _errorSignIn = true;
                                                                                                          _notFound = false;
                                                                                                        });
                                                                                                        stateInitalisation(context);
                                                                                                        Navigator.of(context).pop();
                                                                                                      }
                                                                                                    }).catchError((error) {
                                                                                                      setState(() {
                                                                                                        _errorSignIn = true;
                                                                                                        _notFound = false;
                                                                                                        _lockedLogin = false;
                                                                                                        errorMailOrPass = false;
                                                                                                        _stopCaptcha = false;
                                                                                                      });
                                                                                                      stateInitalisation(context);
                                                                                                      Navigator.of(context).pop();
                                                                                                    });
                                                                                                  }
                                                                                                  break;
                                                                                                case "notfound":
                                                                                                  {
                                                                                                    setState(() {
                                                                                                      _value = 0;
                                                                                                      _textDisplay = false;
                                                                                                      _textDisplayError = true;
                                                                                                      _isWrongCaptcha = true;
                                                                                                    });
                                                                                                    LoginController.apiService.captcha().then((value) {
                                                                                                      if (value != "error") {
                                                                                                        captchaPuzzle = value;
                                                                                                        setState(() {
                                                                                                          _bytesPuzzleImage = base64Decode(captchaPuzzle[1].toString().substring(22));
                                                                                                          _bytesOriginalImage = base64Decode(captchaPuzzle[0].toString().substring(22));
                                                                                                          _value = 0;
                                                                                                        });
                                                                                                        Future.delayed(const Duration(milliseconds: 300), () {
                                                                                                          setState(() {
                                                                                                            _value = 0;
                                                                                                            _isWrongCaptcha = false;
                                                                                                            _textDisplay = true;
                                                                                                            _textDisplayError = false;
                                                                                                            _stopCaptcha = false;
                                                                                                          });
                                                                                                        });
                                                                                                      } else {
                                                                                                        setState(() {
                                                                                                          _stopCaptcha = false;
                                                                                                          _errorSignIn = false;
                                                                                                          _notFound = true;
                                                                                                        });
                                                                                                        stateInitalisation(context);
                                                                                                        Navigator.of(context).pop();
                                                                                                      }
                                                                                                    }).catchError((error) {
                                                                                                      setState(() {
                                                                                                        _notFound = true;
                                                                                                        _errorSignIn = false;
                                                                                                        _lockedLogin = false;
                                                                                                        errorMailOrPass = false;
                                                                                                        _stopCaptcha = false;
                                                                                                      });
                                                                                                      stateInitalisation(context);
                                                                                                      Navigator.of(context).pop();
                                                                                                    });
                                                                                                  }
                                                                                                  break;
                                                                                              }
                                                                                            }).catchError((error) {
                                                                                              setState(() {
                                                                                                _errorSignIn = true;
                                                                                                _notFound = false;
                                                                                                _lockedLogin = false;
                                                                                                errorMailOrPass = false;
                                                                                                _stopCaptcha = false;
                                                                                              });
                                                                                              stateInitalisation(context);
                                                                                              Navigator.of(context).pop();
                                                                                            });
                                                                                          },
                                                                                        ),
                                                                                      ),
                                                                                    ),
                                                                                    Padding(
                                                                                        padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.15, top: MediaQuery.of(context).size.height * 0.012),
                                                                                        child: SizedBox(
                                                                                          child: _textDisplay
                                                                                              ? Text(
                                                                                                  "Slide to complete the puzzle",
                                                                                                  style: GoogleFonts.poppins(color: Colors.black, fontWeight: FontWeight.w400, fontSize: MediaQuery.of(context).size.height * 0.016, fontStyle: FontStyle.normal),
                                                                                                )
                                                                                              : (_textDisplayError
                                                                                                  ? Text(
                                                                                                      "Wrong captcha : Try again",
                                                                                                      style: GoogleFonts.poppins(
                                                                                                        fontWeight: FontWeight.w600,
                                                                                                        fontSize: MediaQuery.of(context).size.height * 0.016,
                                                                                                        fontStyle: FontStyle.normal,
                                                                                                        color: const Color(0XFFF52079),
                                                                                                      ),
                                                                                                    )
                                                                                                  : null),
                                                                                        )),
                                                                                  ],
                                                                                )
                                                                              : SizedBox(
                                                                                  width: 280,
                                                                                  child: LinearProgressIndicator(),
                                                                                ),
                                                                        ),
                                                                        const Spacer(),
                                                                      ],
                                                                    ),
                                                                  ),
                                                                ),
                                                              );
                                                            });
                                                          });
                                                    } else {
                                                      reGetCaptcha().then((value) {
                                                        if (value != "error") {
                                                          var originalPuzzle = captchaPuzzle[0].toString().substring(22);
                                                          var puzzleImage = captchaPuzzle[1].toString().substring(22);
                                                          var idPuzzleImage = captchaPuzzle[2].toString();
                                                          Uint8List _bytesOriginalImage = base64Decode(originalPuzzle);
                                                          Uint8List _bytesPuzzleImage = base64Decode(puzzleImage);
                                                          showGeneralDialog(
                                                              barrierDismissible: false,
                                                              transitionBuilder: (context, animation, secondaryAnimation, child) {
                                                                return FadeTransition(
                                                                  opacity: animation,
                                                                  child: ScaleTransition(
                                                                    scale: animation,
                                                                    child: child,
                                                                  ),
                                                                );
                                                              },
                                                              transitionDuration: Duration(milliseconds: 100),
                                                              context: context,
                                                              pageBuilder: (context, animation, secondaryAnimation) {
                                                                return StatefulBuilder(builder: (context, setState) {
                                                                  return Scaffold(
                                                                    body: SafeArea(
                                                                      child: Container(
                                                                        height: MediaQuery.of(context).size.height,
                                                                        child: Column(
                                                                          children: <Widget>[
                                                                            Row(
                                                                              children: <Widget>[
                                                                                Padding(
                                                                                  padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.025),
                                                                                  child: Text(
                                                                                    "Security Verification",
                                                                                    style: GoogleFonts.poppins(color: Colors.black, fontWeight: FontWeight.w700, fontSize: MediaQuery.of(context).size.height * 0.02),
                                                                                  ),
                                                                                ),
                                                                                Spacer(),
                                                                                Padding(
                                                                                  padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.012),
                                                                                  child: IconButton(
                                                                                      onPressed: () async {
                                                                                        Navigator.of(context).pop();
                                                                                        setState(() {
                                                                                          _stopCaptcha = false;
                                                                                        });
                                                                                        captchaPuzzle = await LoginController.apiService.captcha();
                                                                                        return stateInitalisation(context);
                                                                                      },
                                                                                      icon: Icon(
                                                                                        Icons.close,
                                                                                        color: Colors.black,
                                                                                        size: MediaQuery.of(context).size.height * 0.03,
                                                                                      )),
                                                                                ),
                                                                              ],
                                                                            ),
                                                                            const Spacer(),
                                                                            Stack(
                                                                              children: [
                                                                                Image.memory(
                                                                                  _bytesOriginalImage,
                                                                                  height: 155,
                                                                                  width: 310,
                                                                                ),
                                                                                Positioned(
                                                                                  right: -_value,
                                                                                  child: Image.memory(
                                                                                    _bytesPuzzleImage,
                                                                                    height: 155,
                                                                                    width: 310,
                                                                                    color: _isSuccessCaptcha ? Colors.green : (_isWrongCaptcha ? Colors.redAccent : null),
                                                                                  ),
                                                                                ),
                                                                                Positioned(
                                                                                  right: -MediaQuery.of(context).size.width * 0.02,
                                                                                  child: Padding(
                                                                                    padding: const EdgeInsets.all(8.0),
                                                                                    child: IconButton(
                                                                                      onPressed: () async {
                                                                                        captchaPuzzle = await LoginController.apiService.captcha();
                                                                                        originalPuzzle = captchaPuzzle[0].toString().substring(22);
                                                                                        puzzleImage = captchaPuzzle[1].toString().substring(22);
                                                                                        idPuzzleImage = captchaPuzzle[2].toString();
                                                                                        setState(() {
                                                                                          _bytesPuzzleImage = base64Decode(puzzleImage);
                                                                                          _bytesOriginalImage = base64Decode(originalPuzzle);
                                                                                          _stopCaptcha = false;
                                                                                          _textDisplay = true;
                                                                                          _value = 0;
                                                                                          _textDisplayError = false;
                                                                                          _isSuccessCaptcha = false;
                                                                                          _isWrongCaptcha = false;
                                                                                        });
                                                                                      },
                                                                                      icon: Icon(
                                                                                        Icons.refresh,
                                                                                        color: Colors.white,
                                                                                        size: 30,
                                                                                      ),
                                                                                    ),
                                                                                  ),
                                                                                ),
                                                                              ],
                                                                            ),
                                                                            const Spacer(),
                                                                            Padding(
                                                                              padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.01),
                                                                              child: !_isSuccessCaptcha
                                                                                  ? Stack(
                                                                                      children: [
                                                                                        SliderTheme(
                                                                                          data: SliderTheme.of(context).copyWith(
                                                                                            trackHeight: 40,
                                                                                            inactiveTrackColor: const Color(0XFFF6F6FF),
                                                                                            inactiveTickMarkColor: Color(0xFFF5F5F5),
                                                                                            activeTrackColor: Color(0xFFF5F5F5),
                                                                                            activeTickMarkColor: Color(0xFFF5F5F5),
                                                                                            thumbColor: _isSuccessCaptcha ? Colors.green : (_isWrongCaptcha ? const Color(0XFFF52079) : const Color(0XFFD6D6E8)),
                                                                                            thumbShape: CustomSliderThumbRect(min: 0, max: 247, thumbRadius: 0, thumbHeight: 55),
                                                                                          ),
                                                                                          child: SizedBox(
                                                                                            width: 350,
                                                                                            child: Slider(
                                                                                              value: _value,
                                                                                              onChanged: (value) {
                                                                                                if (value >= 247) {
                                                                                                } else {
                                                                                                  setState(() {
                                                                                                    _value = value.roundToDouble();
                                                                                                    _textDisplay = false;
                                                                                                    _textDisplayError = false;
                                                                                                    _isSuccessCaptcha = false;
                                                                                                    _isWrongCaptcha = false;
                                                                                                  });
                                                                                                }
                                                                                              },
                                                                                              max: 247,
                                                                                              min: 0,
                                                                                              onChangeStart: (value) {
                                                                                                setState(() {
                                                                                                  _value = value;
                                                                                                  _textDisplay = false;
                                                                                                  _textDisplayError = false;
                                                                                                  _isSuccessCaptcha = false;
                                                                                                  _isWrongCaptcha = false;
                                                                                                });
                                                                                              },
                                                                                              onChangeEnd: (value) async {
                                                                                                setState(() {
                                                                                                  _stopCaptcha = true;
                                                                                                  _textDisplay = false;
                                                                                                });
                                                                                                LoginController.apiService.verifyCaptcha(captchaPuzzle[2].toString(), value.toInt().toString()).then((result) {
                                                                                                  switch (result) {
                                                                                                    case "success":
                                                                                                      {
                                                                                                        setState(() {
                                                                                                          _isSuccessCaptcha = true;
                                                                                                        });
                                                                                                        save().then((value) {
                                                                                                          switch (value) {
                                                                                                            case "success":
                                                                                                              {
                                                                                                                Future.wait([
                                                                                                                  LoginController.apiService.checkValidation(Provider.of<LoginController>(context, listen: false).userDetails!.userToken.toString()),
                                                                                                                ]).then((value) {
                                                                                                                  if (value[0] == "success") {
                                                                                                                    Navigator.of(context).pop();
                                                                                                                    loginUI();
                                                                                                                  } else {
                                                                                                                    setState(() {
                                                                                                                      _errorSignIn = true;
                                                                                                                    });
                                                                                                                    stateInitalisation(context);
                                                                                                                    Navigator.of(context).pop();
                                                                                                                  }
                                                                                                                }).catchError((error) async {
                                                                                                                  captchaPuzzle = await LoginController.apiService.captcha();
                                                                                                                  setState(() {
                                                                                                                    _errorSignIn = true;
                                                                                                                    _notFound = false;
                                                                                                                    _lockedLogin = false;
                                                                                                                    errorMailOrPass = false;
                                                                                                                    _stopCaptcha = false;
                                                                                                                  });
                                                                                                                  Navigator.of(context).pop();

                                                                                                                  stateInitalisation(context);
                                                                                                                });
                                                                                                              }
                                                                                                              break;
                                                                                                            case "error":
                                                                                                              {
                                                                                                                setState(() {
                                                                                                                  errorMailOrPass = true;
                                                                                                                  _stopCaptcha = false;
                                                                                                                  _errorSignIn = false;
                                                                                                                  _notFound = false;
                                                                                                                  _lockedLogin = false;
                                                                                                                });
                                                                                                                stateInitalisation(context);
                                                                                                                Navigator.of(context).pop();
                                                                                                              }
                                                                                                              break;
                                                                                                            case "locked":
                                                                                                              {
                                                                                                                setState(() {
                                                                                                                  errorAccountExist = false;
                                                                                                                  errorMailOrPass = false;
                                                                                                                  _errorSignIn = false;
                                                                                                                  _notFound = false;
                                                                                                                  _stopCaptcha = false;
                                                                                                                  _lockedLogin = true;
                                                                                                                });
                                                                                                                Navigator.of(context).pop();
                                                                                                                stateInitalisation(context);
                                                                                                              }
                                                                                                              break;
                                                                                                            case "notfound":
                                                                                                              {
                                                                                                                setState(() {
                                                                                                                  _value = 0;
                                                                                                                  _textDisplay = false;
                                                                                                                  _textDisplayError = true;
                                                                                                                  _isWrongCaptcha = true;
                                                                                                                });
                                                                                                                LoginController.apiService.captcha().then((value) {
                                                                                                                  if (value != "error") {
                                                                                                                    captchaPuzzle = value;
                                                                                                                    setState(() {
                                                                                                                      _bytesPuzzleImage = base64Decode(captchaPuzzle[1].toString().substring(22));
                                                                                                                      _bytesOriginalImage = base64Decode(captchaPuzzle[0].toString().substring(22));
                                                                                                                      _value = 0;
                                                                                                                    });
                                                                                                                    Future.delayed(const Duration(milliseconds: 300), () {
                                                                                                                      setState(() {
                                                                                                                        _value = 0;
                                                                                                                        _isWrongCaptcha = false;
                                                                                                                        _textDisplay = true;
                                                                                                                        _textDisplayError = false;
                                                                                                                        _stopCaptcha = false;
                                                                                                                      });
                                                                                                                    });
                                                                                                                  } else {
                                                                                                                    setState(() {
                                                                                                                      _stopCaptcha = false;
                                                                                                                      _errorSignIn = false;
                                                                                                                      _notFound = true;
                                                                                                                    });
                                                                                                                    stateInitalisation(context);
                                                                                                                    Navigator.of(context).pop();
                                                                                                                  }
                                                                                                                }).catchError((error) {
                                                                                                                  setState(() {
                                                                                                                    _notFound = true;
                                                                                                                    _errorSignIn = false;
                                                                                                                    _lockedLogin = false;
                                                                                                                    errorMailOrPass = false;
                                                                                                                    _stopCaptcha = false;
                                                                                                                  });
                                                                                                                  stateInitalisation(context);
                                                                                                                  Navigator.of(context).pop();
                                                                                                                });
                                                                                                              }
                                                                                                              break;
                                                                                                          }
                                                                                                        }).catchError((error) {
                                                                                                          setState(() {
                                                                                                            _errorSignIn = true;
                                                                                                            _notFound = false;
                                                                                                            _lockedLogin = false;
                                                                                                            errorMailOrPass = false;
                                                                                                            _stopCaptcha = false;
                                                                                                          });
                                                                                                          stateInitalisation(context);
                                                                                                          Navigator.of(context).pop();
                                                                                                        });
                                                                                                      }
                                                                                                      break;
                                                                                                    case "error":
                                                                                                      {
                                                                                                        setState(() {
                                                                                                          _value = 0;
                                                                                                          _textDisplay = false;
                                                                                                          _textDisplayError = true;
                                                                                                          _isWrongCaptcha = true;
                                                                                                        });
                                                                                                        LoginController.apiService.captcha().then((value) {
                                                                                                          if (value != "error") {
                                                                                                            captchaPuzzle = value;
                                                                                                            setState(() {
                                                                                                              _bytesPuzzleImage = base64Decode(captchaPuzzle[1].toString().substring(22));
                                                                                                              _bytesOriginalImage = base64Decode(captchaPuzzle[0].toString().substring(22));
                                                                                                              _value = 0;
                                                                                                            });
                                                                                                            Future.delayed(const Duration(milliseconds: 300), () {
                                                                                                              setState(() {
                                                                                                                _value = 0;
                                                                                                                _isWrongCaptcha = false;
                                                                                                                _textDisplay = true;
                                                                                                                _textDisplayError = false;
                                                                                                                _stopCaptcha = false;
                                                                                                              });
                                                                                                            });
                                                                                                          } else {
                                                                                                            setState(() {
                                                                                                              _stopCaptcha = false;
                                                                                                              _errorSignIn = true;
                                                                                                              _notFound = false;
                                                                                                            });
                                                                                                            stateInitalisation(context);
                                                                                                            Navigator.of(context).pop();
                                                                                                          }
                                                                                                        }).catchError((error) {
                                                                                                          setState(() {
                                                                                                            _errorSignIn = true;
                                                                                                            _notFound = false;
                                                                                                            _lockedLogin = false;
                                                                                                            errorMailOrPass = false;
                                                                                                            _stopCaptcha = false;
                                                                                                          });
                                                                                                          stateInitalisation(context);
                                                                                                          Navigator.of(context).pop();
                                                                                                        });
                                                                                                      }
                                                                                                      break;
                                                                                                  }
                                                                                                }).catchError((error) {
                                                                                                  setState(() {
                                                                                                    _errorSignIn = true;
                                                                                                    _notFound = false;
                                                                                                    _lockedLogin = false;
                                                                                                    errorMailOrPass = false;
                                                                                                    _stopCaptcha = false;
                                                                                                  });
                                                                                                  stateInitalisation(context);
                                                                                                  Navigator.of(context).pop();
                                                                                                });
                                                                                              },
                                                                                            ),
                                                                                          ),
                                                                                        ),
                                                                                        Padding(
                                                                                            padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.15, top: MediaQuery.of(context).size.height * 0.012),
                                                                                            child: SizedBox(
                                                                                              child: _textDisplay
                                                                                                  ? Text(
                                                                                                      "Slide to complete the puzzle",
                                                                                                      style: GoogleFonts.poppins(color: Colors.black, fontWeight: FontWeight.w400, fontSize: MediaQuery.of(context).size.height * 0.016, fontStyle: FontStyle.normal),
                                                                                                    )
                                                                                                  : (_textDisplayError
                                                                                                      ? Text(
                                                                                                          "Wrong captcha : Try again",
                                                                                                          style: GoogleFonts.poppins(
                                                                                                            fontWeight: FontWeight.w600,
                                                                                                            fontSize: MediaQuery.of(context).size.height * 0.016,
                                                                                                            fontStyle: FontStyle.normal,
                                                                                                            color: const Color(0XFFF52079),
                                                                                                          ),
                                                                                                        )
                                                                                                      : null),
                                                                                            )),
                                                                                      ],
                                                                                    )
                                                                                  : SizedBox(
                                                                                      width: 280,
                                                                                      child: LinearProgressIndicator(),
                                                                                    ),
                                                                            ),
                                                                            const Spacer(),
                                                                          ],
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  );
                                                                });
                                                              });
                                                        } else {
                                                          setState(() {
                                                            _errorSignIn = true;
                                                            _notFound = false;
                                                            _lockedLogin = false;
                                                            errorMailOrPass = false;
                                                            _stopCaptcha = false;
                                                          });
                                                          stateInitalisation(context);
                                                          Navigator.of(context).pop();
                                                        }
                                                      }).catchError((error) {
                                                        setState(() {
                                                          _errorSignIn = true;
                                                          _notFound = false;
                                                          _lockedLogin = false;
                                                          errorMailOrPass = false;
                                                          _stopCaptcha = false;
                                                        });
                                                        stateInitalisation(context);
                                                        Navigator.of(context).pop();
                                                      });
                                                    }
                                                  }
                                                : null,
                                            child: _isLoading
                                                ? SizedBox(
                                                    height: 15,
                                                    width: 15,
                                                    child: CircularProgressIndicator(
                                                      color: Colors.white,
                                                    ),
                                                  )
                                                : Text(
                                                    "Sign In",
                                                    style: GoogleFonts.poppins(
                                                      color: Colors.white,
                                                      fontWeight: FontWeight.w600,
                                                      fontSize: MediaQuery.of(context).size.width * 0.04,
                                                    ),
                                                  )),
                                      )
                                    : Container(
                                        height: MediaQuery.of(context).size.height * 0.06,
                                        width: MediaQuery.of(context).size.width * 0.65,
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(30),
                                          color: Color(0xFFF6F6FF),
                                        ),
                                        child: Center(
                                          child: Text(
                                            "Sign In",
                                            style: GoogleFonts.poppins(
                                              color: Color(0xFFADADC8),
                                              fontWeight: FontWeight.w600,
                                              fontSize: MediaQuery.of(context).size.width * 0.04,
                                            ),
                                          ),
                                        ),
                                      ),
                              ),
                              SizedBox(
                                height: MediaQuery.of(context).size.height * 0.025,
                              ),
                              InkWell(
                                onTap: () {
                                  var originalPuzzle;
                                  var puzzleImage;
                                  var idPuzzleImage;
                                  Uint8List? _bytesPuzzleImage;
                                  Uint8List? _bytesOriginalImage;
                                  mail.text = "";
                                  isConfirmedPassword = false;
                                  isConfirmPasswordEdited = false;
                                  _pinPutController.text = "";
                                  setState(() {
                                    _isvalidFormEmail = false;
                                    isAccountExist = false;
                                    triggerAccountInexistanceError(false);
                                    isNotExist = false;
                                    _loadingResetPassword = false;
                                    _stopCaptcha = false;
                                    _errorSignIn = false;
                                    _notFound = false;
                                    _isButtonActive = false;
                                    _isLoading = false;
                                    _value = 0;
                                    _textDisplay = true;
                                    _textDisplayError = false;
                                    _isSuccessCaptcha = false;
                                    _isWrongCaptcha = false;
                                    _isLoading = true;
                                    _lockedLogin = false;
                                    errorMailOrPass = false;
                                  });
                                  if (captchaPuzzle.isNotEmpty) {
                                    originalPuzzle = captchaPuzzle[0].toString().substring(22);
                                    puzzleImage = captchaPuzzle[1].toString().substring(22);
                                    idPuzzleImage = captchaPuzzle[2].toString();
                                    _bytesOriginalImage = base64Decode(originalPuzzle);
                                    _bytesPuzzleImage = base64Decode(puzzleImage);
                                  } else {
                                    reGetCaptcha().then((value) {
                                      if (value != "error") {
                                        var originalPuzzle = captchaPuzzle[0].toString().substring(22);
                                        puzzleImage = captchaPuzzle[1].toString().substring(22);
                                        idPuzzleImage = captchaPuzzle[2].toString();
                                        _bytesOriginalImage = base64Decode(originalPuzzle);
                                        _bytesPuzzleImage = base64Decode(puzzleImage);
                                      }
                                    });
                                  }
                                  showGeneralDialog(
                                      barrierDismissible: false,
                                      transitionBuilder: (context, animation, secondaryAnimation, child) {
                                        return FadeTransition(
                                          opacity: animation,
                                          child: ScaleTransition(
                                            scale: animation,
                                            child: child,
                                          ),
                                        );
                                      },
                                      transitionDuration: Duration(milliseconds: 100),
                                      context: context,
                                      pageBuilder: (context, animation, secondaryAnimation) {
                                        return StatefulBuilder(builder: (context, setState) {
                                          return Scaffold(
                                            body: SafeArea(
                                              child: Container(
                                                height: MediaQuery.of(context).size.height,
                                                width: MediaQuery.of(context).size.width,
                                                child: Column(
                                                  children: [
                                                    Padding(
                                                      padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.065, top: MediaQuery.of(context).size.height * 0.012),
                                                      child: Align(
                                                        alignment: Alignment.topRight,
                                                        child: InkResponse(
                                                          onTap: () {
                                                            setState(() {
                                                              _isvalidFormEmail = false;
                                                              isAccountExist = false;
                                                              triggerAccountInexistanceError(false);
                                                              isNotExist = false;
                                                            });
                                                            Navigator.of(context).pop();
                                                          },
                                                          child: Icon(
                                                            Icons.close,
                                                            color: const Color(0xFF75758F),
                                                            size: MediaQuery.of(context).size.height * 0.025,
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                    SvgPicture.asset(
                                                      "images/recovery.svg",
                                                      width: MediaQuery.of(context).size.height * 0.15,
                                                      height: MediaQuery.of(context).size.height * 0.15,
                                                    ),
                                                    SizedBox(height: MediaQuery.of(context).size.height * 0.01),
                                                    Text("Forgot your password ?", style: GoogleFonts.poppins(color: const Color(0XFF1F2337), fontSize: MediaQuery.of(context).size.height * 0.03, letterSpacing: 0.3, fontWeight: FontWeight.w600)),
                                                    SizedBox(height: MediaQuery.of(context).size.height * 0.01),
                                                    Text("Simply enter your account's Email address", textAlign: TextAlign.center, style: GoogleFonts.poppins(color: const Color(0XFF75758F), fontSize: MediaQuery.of(context).size.height * 0.016, letterSpacing: 0.8, fontWeight: FontWeight.w500)),
                                                    SizedBox(height: MediaQuery.of(context).size.height * 0.02),
                                                    SizedBox(
                                                      width: MediaQuery.of(context).size.width * 0.82,
                                                      child: TextFormField(
                                                          controller: mail,
                                                          focusNode: resetFocusNode,
                                                          autofocus: true,
                                                          onChanged: (value) {
                                                            triggerAccountInexistanceError(false);
                                                            isNotExist = false;
                                                            TextSelection previousSelection = mail.selection;
                                                            mail.text = value;

                                                            mail.selection = previousSelection;
                                                            if (mail.text.isNotEmpty && RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(_emailController.text)) {
                                                              setState(() {
                                                                isEditedEmail = true;
                                                              });
                                                            } else {
                                                              setState(() {
                                                                isEditedEmail = false;
                                                              });
                                                            }
                                                            TextSelection.fromPosition(TextPosition(offset: mail.text.length));
                                                            if (mail.text.isNotEmpty) {
                                                              if (RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(value)) {
                                                                setState(() {
                                                                  _isvalidFormEmail = true;
                                                                });
                                                              } else {
                                                                setState(() {
                                                                  _isvalidFormEmail = false;
                                                                });
                                                              }
                                                            }
                                                          },
                                                          inputFormatters: [
                                                            FilteringTextInputFormatter.deny(RegExp(r'\s')),
                                                          ],
                                                          decoration: InputDecoration(
                                                              fillColor: Colors.white,
                                                              focusColor: Colors.white,
                                                              hoverColor: Colors.white,
                                                              filled: true,
                                                              isDense: true,
                                                              contentPadding: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width * 0.05, vertical: 15),
                                                              suffixIcon: _isvalidFormEmail
                                                                  ? Icon(
                                                                      Icons.check,
                                                                      color: const Color(0XFF00CC9E),
                                                                    )
                                                                  : null,
                                                              enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: const Color(0XFFD6D6E8))),
                                                              focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: const Color(0XFFD6D6E8))),
                                                              errorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red)),
                                                              focusedErrorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red)))),
                                                    ),
                                                    Padding(
                                                      padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.012),
                                                      child: Visibility(
                                                        visible: (!isEditedEmail && !_isvalidFormEmail && (mail.text.length > 0)),
                                                        child: Column(
                                                          children: [
                                                            Text(
                                                              "Field Email Address must have a valid form ",
                                                              style: GoogleFonts.poppins(
                                                                color: Color(0xFFF52079),
                                                                fontWeight: FontWeight.w500,
                                                                fontSize: MediaQuery.of(context).size.height * 0.013,
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                    ),
                                                    Padding(
                                                      padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.012),
                                                      child: Visibility(
                                                        visible: Provider.of<LoginController>(context, listen: true).resetMailError,
                                                        child: Column(
                                                          children: [
                                                            Text(
                                                              "This account doesn't exist.",
                                                              style: GoogleFonts.poppins(
                                                                color: Color(0xFFF52079),
                                                                fontWeight: FontWeight.w500,
                                                                fontSize: MediaQuery.of(context).size.height * 0.013,
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                    ),
                                                    Padding(
                                                      padding: EdgeInsets.fromLTRB(0, MediaQuery.of(context).size.height * 0.02, 0, 0),
                                                      child: _isvalidFormEmail
                                                          ? SizedBox(
                                                              height: MediaQuery.of(context).size.height * 0.06,
                                                              width: MediaQuery.of(context).size.width * 0.82,
                                                              child: ElevatedButton(
                                                                style: ButtonStyle(
                                                                  backgroundColor: MaterialStateProperty.all(
                                                                    Color(0xFF4048FF),
                                                                  ),
                                                                  shape: MaterialStateProperty.all(
                                                                    RoundedRectangleBorder(
                                                                      borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.08),
                                                                    ),
                                                                  ),
                                                                ),
                                                                onPressed: !_loadingResetPassword
                                                                    ? () async {
                                                                        setState(() {
                                                                          _loadingResetPassword = true;
                                                                        });
                                                                        FocusManager.instance.primaryFocus?.unfocus();
                                                                        showGeneralDialog(
                                                                            barrierDismissible: false,
                                                                            transitionBuilder: (context, animation, secondaryAnimation, child) {
                                                                              return FadeTransition(
                                                                                opacity: animation,
                                                                                child: ScaleTransition(
                                                                                  scale: animation,
                                                                                  child: child,
                                                                                ),
                                                                              );
                                                                            },
                                                                            transitionDuration: Duration(milliseconds: 100),
                                                                            context: context,
                                                                            pageBuilder: (context, animation, secondaryAnimation) {
                                                                              return StatefulBuilder(builder: (context, setState) {
                                                                                return Scaffold(
                                                                                  body: SafeArea(
                                                                                    child: Container(
                                                                                      height: MediaQuery.of(context).size.height,
                                                                                      child: Column(
                                                                                        children: <Widget>[
                                                                                          Row(
                                                                                            children: <Widget>[
                                                                                              Padding(
                                                                                                padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.025),
                                                                                                child: Text(
                                                                                                  "Security Verification",
                                                                                                  style: GoogleFonts.poppins(color: Colors.black, fontWeight: FontWeight.w700, fontSize: MediaQuery.of(context).size.height * 0.02),
                                                                                                ),
                                                                                              ),
                                                                                              Spacer(),
                                                                                              Padding(
                                                                                                padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.012),
                                                                                                child: IconButton(
                                                                                                    onPressed: () async {
                                                                                                      Navigator.of(context).pop();
                                                                                                      setState(() {
                                                                                                        _stopCaptcha = false;
                                                                                                      });
                                                                                                      captchaPuzzle = await LoginController.apiService.captcha();
                                                                                                      return stateInitalisation(context);
                                                                                                    },
                                                                                                    icon: Icon(
                                                                                                      Icons.close,
                                                                                                      color: Colors.black,
                                                                                                      size: MediaQuery.of(context).size.height * 0.03,
                                                                                                    )),
                                                                                              ),
                                                                                            ],
                                                                                          ),
                                                                                          const Spacer(),
                                                                                          Stack(
                                                                                            children: [
                                                                                              Image.memory(
                                                                                                _bytesOriginalImage!,
                                                                                                height: 155,
                                                                                                width: 310,
                                                                                              ),
                                                                                              Positioned(
                                                                                                right: -_value,
                                                                                                child: Image.memory(
                                                                                                  _bytesPuzzleImage!,
                                                                                                  height: 155,
                                                                                                  width: 310,
                                                                                                  color: _isSuccessCaptcha ? Colors.green : (_isWrongCaptcha ? Colors.redAccent : null),
                                                                                                ),
                                                                                              ),
                                                                                              Positioned(
                                                                                                right: -MediaQuery.of(context).size.width * 0.02,
                                                                                                child: Padding(
                                                                                                  padding: const EdgeInsets.all(8.0),
                                                                                                  child: IconButton(
                                                                                                    onPressed: () async {
                                                                                                      captchaPuzzle = await LoginController.apiService.captcha();
                                                                                                      originalPuzzle = captchaPuzzle[0].toString().substring(22);
                                                                                                      puzzleImage = captchaPuzzle[1].toString().substring(22);
                                                                                                      idPuzzleImage = captchaPuzzle[2].toString();
                                                                                                      setState(() {
                                                                                                        _bytesPuzzleImage = base64Decode(puzzleImage);
                                                                                                        _bytesOriginalImage = base64Decode(originalPuzzle);
                                                                                                        _stopCaptcha = false;
                                                                                                        _textDisplay = true;
                                                                                                        _value = 0;
                                                                                                        _textDisplayError = false;
                                                                                                        _isSuccessCaptcha = false;
                                                                                                        _isWrongCaptcha = false;
                                                                                                      });
                                                                                                    },
                                                                                                    icon: Icon(
                                                                                                      Icons.refresh,
                                                                                                      color: Colors.white,
                                                                                                      size: 30,
                                                                                                    ),
                                                                                                  ),
                                                                                                ),
                                                                                              ),
                                                                                            ],
                                                                                          ),
                                                                                          const Spacer(),
                                                                                          Padding(
                                                                                            padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.01),
                                                                                            child: !_isSuccessCaptcha
                                                                                                ? Stack(
                                                                                                    children: [
                                                                                                      SliderTheme(
                                                                                                        data: SliderTheme.of(context).copyWith(
                                                                                                          trackHeight: 40,
                                                                                                          inactiveTrackColor: const Color(0XFFF6F6FF),
                                                                                                          inactiveTickMarkColor: Color(0xFFF5F5F5),
                                                                                                          activeTrackColor: Color(0xFFF5F5F5),
                                                                                                          activeTickMarkColor: Color(0xFFF5F5F5),
                                                                                                          valueIndicatorColor: Colors.white,
                                                                                                          thumbColor: _isSuccessCaptcha ? Colors.green : (_isWrongCaptcha ? const Color(0XFFF52079) : const Color(0XFFD6D6E8)),
                                                                                                          thumbShape: !_isWrongCaptcha ? CustomSliderThumbRect(min: 0, max: 247, thumbRadius: 0, thumbHeight: 55) : CustomSliderThumbRectWrong(min: 0, max: 247, thumbRadius: 0, thumbHeight: 55),
                                                                                                        ),
                                                                                                        child: SizedBox(
                                                                                                          width: 350,
                                                                                                          child: Slider(
                                                                                                            value: _value,
                                                                                                            onChanged: (value) {
                                                                                                              if (value >= 247) {
                                                                                                              } else {
                                                                                                                setState(() {
                                                                                                                  _value = value.roundToDouble();
                                                                                                                  _textDisplay = false;
                                                                                                                  _textDisplayError = false;
                                                                                                                  _isSuccessCaptcha = false;
                                                                                                                  _isWrongCaptcha = false;
                                                                                                                });
                                                                                                              }
                                                                                                            },
                                                                                                            max: 247,
                                                                                                            min: 0,
                                                                                                            onChangeStart: (value) {
                                                                                                              setState(() {
                                                                                                                _value = value;
                                                                                                                _textDisplay = false;
                                                                                                                _textDisplayError = false;
                                                                                                                _isSuccessCaptcha = false;
                                                                                                                _isWrongCaptcha = false;
                                                                                                              });
                                                                                                            },
                                                                                                            onChangeEnd: (value) async {
                                                                                                              setState(() {
                                                                                                                _stopCaptcha = true;
                                                                                                                _textDisplay = false;
                                                                                                              });
                                                                                                              LoginController.apiService.verifyCaptcha(captchaPuzzle[2].toString(), value.toInt().toString()).then((result) async {
                                                                                                                switch (result) {
                                                                                                                  case "success":
                                                                                                                    {
                                                                                                                      setState(() {
                                                                                                                        _isSuccessCaptcha = true;
                                                                                                                      });
                                                                                                                      Future.delayed(const Duration(seconds: 2), () {});
                                                                                                                      stateInitalisation(context);
                                                                                                                      var userExist = await LoginController.apiService.forgetPassword(mail.text);
                                                                                                                      isExist = (userExist == "Email was sent");
                                                                                                                      if (isExist) {
                                                                                                                        FocusManager.instance.primaryFocus?.unfocus();
                                                                                                                        setState(() {
                                                                                                                          isAccountExist = true;
                                                                                                                          _loadingResetPassword = false;
                                                                                                                        });
                                                                                                                        if (isAccountExist) {
                                                                                                                          Navigator.of(context).pop();
                                                                                                                          showGeneralDialog(
                                                                                                                              barrierDismissible: false,
                                                                                                                              transitionBuilder: (context, animation, secondaryAnimation, child) {
                                                                                                                                return FadeTransition(
                                                                                                                                  opacity: animation,
                                                                                                                                  child: ScaleTransition(
                                                                                                                                    scale: animation,
                                                                                                                                    child: child,
                                                                                                                                  ),
                                                                                                                                );
                                                                                                                              },
                                                                                                                              transitionDuration: Duration(milliseconds: 100),
                                                                                                                              context: context,
                                                                                                                              pageBuilder: (context, animation, secondaryAnimation) {
                                                                                                                                return StatefulBuilder(builder: (context, setState) {
                                                                                                                                  return Scaffold(
                                                                                                                                    body: SafeArea(
                                                                                                                                      child: Container(
                                                                                                                                        color: Colors.white,
                                                                                                                                        height: MediaQuery.of(context).size.height,
                                                                                                                                        width: MediaQuery.of(context).size.width,
                                                                                                                                        child: Column(
                                                                                                                                          children: <Widget>[
                                                                                                                                            Padding(
                                                                                                                                              padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.065, top: MediaQuery.of(context).size.height * 0.012),
                                                                                                                                              child: Align(
                                                                                                                                                alignment: Alignment.topRight,
                                                                                                                                                child: InkResponse(
                                                                                                                                                  onTap: () {
                                                                                                                                                    Navigator.of(context).pop();
                                                                                                                                                  },
                                                                                                                                                  child: Icon(
                                                                                                                                                    Icons.close,
                                                                                                                                                    color: const Color(0xFF75758F),
                                                                                                                                                    size: MediaQuery.of(context).size.height * 0.025,
                                                                                                                                                  ),
                                                                                                                                                ),
                                                                                                                                              ),
                                                                                                                                            ),
                                                                                                                                            SvgPicture.asset(
                                                                                                                                              "images/recovery.svg",
                                                                                                                                              width: MediaQuery.of(context).size.height * 0.15,
                                                                                                                                              height: MediaQuery.of(context).size.height * 0.15,
                                                                                                                                            ),
                                                                                                                                            SizedBox(height: MediaQuery.of(context).size.height * 0.01),
                                                                                                                                            Text("Recover your password",
                                                                                                                                                style: GoogleFonts.poppins(
                                                                                                                                                    color: const Color(0XFF1F2337), fontSize: MediaQuery.of(context).size.height * 0.03, letterSpacing: 0.3, fontWeight: FontWeight.w600)),
                                                                                                                                            SizedBox(height: MediaQuery.of(context).size.height * 0.01),
                                                                                                                                            Text("We've sent a code to your email address.",
                                                                                                                                                textAlign: TextAlign.center,
                                                                                                                                                style: GoogleFonts.poppins(
                                                                                                                                                    color: const Color(0XFF75758F), fontSize: MediaQuery.of(context).size.height * 0.016, letterSpacing: 0.8, fontWeight: FontWeight.w500)),
                                                                                                                                            Text("Enter it below to reset your password.",
                                                                                                                                                textAlign: TextAlign.center,
                                                                                                                                                style: GoogleFonts.poppins(
                                                                                                                                                    color: const Color(0XFF75758F), fontSize: MediaQuery.of(context).size.height * 0.016, letterSpacing: 0.8, fontWeight: FontWeight.w500)),
                                                                                                                                            SizedBox(height: MediaQuery.of(context).size.height * 0.025),
                                                                                                                                            Container(
                                                                                                                                                margin: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.1, right: MediaQuery.of(context).size.width * 0.1),
                                                                                                                                                padding: EdgeInsets.only(
                                                                                                                                                    bottom: MediaQuery.of(context).size.height * 0.03, left: MediaQuery.of(context).size.width * 0.035, right: MediaQuery.of(context).size.width * 0.035),
                                                                                                                                                decoration: BoxDecoration(
                                                                                                                                                  color: Color(0xFFF6F6FF),
                                                                                                                                                  borderRadius: BorderRadius.all(Radius.circular(20)),
                                                                                                                                                ),
                                                                                                                                                child: Column(
                                                                                                                                                  children: [
                                                                                                                                                    Padding(
                                                                                                                                                      padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.013, bottom: MediaQuery.of(context).size.height * 0.01),
                                                                                                                                                      child: Text("Authentication code",
                                                                                                                                                          style: GoogleFonts.poppins(
                                                                                                                                                              color: (isConfirmPasswordEdited) ? (isConfirmedPassword ? Color(0xff00CC9E) : Color(0xffF52079)) : Color(0xff4048FF),
                                                                                                                                                              fontSize: MediaQuery.of(context).size.height * 0.016,
                                                                                                                                                              fontWeight: FontWeight.w500)),
                                                                                                                                                    ),
                                                                                                                                                    PinPut(
                                                                                                                                                        fieldsCount: 6,
                                                                                                                                                        onSubmit: (String pin) => _showSnackBar(pin, context),
                                                                                                                                                        focusNode: _pinPutFocusNode,
                                                                                                                                                        controller: _pinPutController,
                                                                                                                                                        textStyle: TextStyle(
                                                                                                                                                            color: isConfirmPasswordEdited ? (isConfirmedPassword ? Color(0xff00CC9E) : Color.fromRGBO(245, 32, 121, 1)) : Colors.blue,
                                                                                                                                                            fontSize: MediaQuery.of(context).size.width * 0.04),
                                                                                                                                                        onChanged: (content) async {
                                                                                                                                                          TextSelection previousSelection = _pinPutController.selection;
                                                                                                                                                          _pinPutController.selection = previousSelection;
                                                                                                                                                          TextSelection.fromPosition(TextPosition(offset: _pinPutController.text.length));
                                                                                                                                                          if (_pinPutController.text.length == 6) {
                                                                                                                                                            var codeConfirmed = await LoginController.apiService.confirmResetPassword(mail.text, _pinPutController.text);
                                                                                                                                                            isConfirmPasswordEdited = true;
                                                                                                                                                            isConfirmedPassword = (codeConfirmed == "code match");
                                                                                                                                                            if (codeConfirmed != "code incorrect") {
                                                                                                                                                              setState(() {
                                                                                                                                                                Provider.of<LoginController>(context, listen: false).code = _pinPutController.text;
                                                                                                                                                                successCode == true;
                                                                                                                                                                errorCode == false;
                                                                                                                                                                isConfirmedPassword == true;
                                                                                                                                                              });
                                                                                                                                                            } else if (codeConfirmed == "code incorrect") {
                                                                                                                                                              setState(() {
                                                                                                                                                                successCode == false;
                                                                                                                                                                errorCode == true;
                                                                                                                                                                isConfirmedPassword == false;
                                                                                                                                                              });
                                                                                                                                                            }
                                                                                                                                                          }
                                                                                                                                                        },
                                                                                                                                                        submittedFieldDecoration: _pinPutDecoration.copyWith(
                                                                                                                                                          borderRadius: BorderRadius.circular(20.0),
                                                                                                                                                        ),
                                                                                                                                                        selectedFieldDecoration: BoxDecoration(
                                                                                                                                                          color: Colors.white,
                                                                                                                                                          borderRadius: BorderRadius.circular(15.0),
                                                                                                                                                          border: Border.all(
                                                                                                                                                            color: (isConfirmPasswordEdited) ? ((isConfirmedPassword) ? Color(0xff00CC9E) : Color(0xffF52079)) : Colors.grey,
                                                                                                                                                          ),
                                                                                                                                                        ),
                                                                                                                                                        followingFieldDecoration: _pinPutDecoration.copyWith(
                                                                                                                                                          borderRadius: BorderRadius.circular(5.0),
                                                                                                                                                          border: Border.all(
                                                                                                                                                            color: (isConfirmPasswordEdited) ? ((isConfirmedPassword) ? Color(0xff00CC9E) : Color(0xffF52079)) : Colors.grey,
                                                                                                                                                          ),
                                                                                                                                                        )),
                                                                                                                                                    Padding(
                                                                                                                                                        padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.01),
                                                                                                                                                        child: (isConfirmPasswordEdited
                                                                                                                                                            ? ((isConfirmedPassword)
                                                                                                                                                                ? Text("Correct code",
                                                                                                                                                                    style: GoogleFonts.poppins(
                                                                                                                                                                      color: Color(0xff00CC9E),
                                                                                                                                                                      fontWeight: FontWeight.w500,
                                                                                                                                                                      fontSize: MediaQuery.of(context).size.height * 0.016,
                                                                                                                                                                    ))
                                                                                                                                                                : Text("Error code",
                                                                                                                                                                    style: GoogleFonts.poppins(
                                                                                                                                                                      color: const Color(0XFFF52079),
                                                                                                                                                                      fontWeight: FontWeight.w500,
                                                                                                                                                                      fontSize: MediaQuery.of(context).size.height * 0.016,
                                                                                                                                                                    )))
                                                                                                                                                            : null))
                                                                                                                                                  ],
                                                                                                                                                )),
                                                                                                                                            SizedBox(height: MediaQuery.of(context).size.height * 0.0075),
                                                                                                                                            Padding(
                                                                                                                                              padding: EdgeInsets.fromLTRB(0, MediaQuery.of(context).size.height * 0.02, 0, 0),
                                                                                                                                              child: isConfirmedPassword
                                                                                                                                                  ? SizedBox(
                                                                                                                                                      height: MediaQuery.of(context).size.height * 0.06,
                                                                                                                                                      width: MediaQuery.of(context).size.width * 0.82,
                                                                                                                                                      child: ElevatedButton(
                                                                                                                                                        style: ButtonStyle(
                                                                                                                                                          backgroundColor: MaterialStateProperty.all(
                                                                                                                                                            Color(0xFF4048FF),
                                                                                                                                                          ),
                                                                                                                                                          shape: MaterialStateProperty.all(
                                                                                                                                                            RoundedRectangleBorder(
                                                                                                                                                              borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.08),
                                                                                                                                                            ),
                                                                                                                                                          ),
                                                                                                                                                        ),
                                                                                                                                                        onPressed: () {
                                                                                                                                                          Provider.of<LoginController>(context, listen: false).recoveryEmail = mail.text;
                                                                                                                                                          Navigator.pushReplacement(context, MaterialPageRoute(builder: (_) => ResetPassword()));
                                                                                                                                                        },
                                                                                                                                                        child: Text(
                                                                                                                                                          "Reset Password",
                                                                                                                                                          style: GoogleFonts.poppins(
                                                                                                                                                            fontSize: MediaQuery.of(context).size.height * 0.019,
                                                                                                                                                            fontWeight: FontWeight.w700,
                                                                                                                                                          ),
                                                                                                                                                        ),
                                                                                                                                                      ))
                                                                                                                                                  : Container(
                                                                                                                                                      height: MediaQuery.of(context).size.height * 0.06,
                                                                                                                                                      width: MediaQuery.of(context).size.width * 0.82,
                                                                                                                                                      decoration: BoxDecoration(
                                                                                                                                                          borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.08),
                                                                                                                                                          color: Color(0xFFF6F6FF),
                                                                                                                                                          border: Border.all(color: Color(0xFFD6D6E8), width: 1)),
                                                                                                                                                      child: Center(
                                                                                                                                                        child: Text(
                                                                                                                                                          "Reset Password",
                                                                                                                                                          style: GoogleFonts.poppins(
                                                                                                                                                            fontSize: MediaQuery.of(context).size.height * 0.019,
                                                                                                                                                            fontWeight: FontWeight.w700,
                                                                                                                                                            color: Color(0xFFADADC8),
                                                                                                                                                          ),
                                                                                                                                                        ),
                                                                                                                                                      ),
                                                                                                                                                    ),
                                                                                                                                            ),
                                                                                                                                            SizedBox(height: MediaQuery.of(context).size.height * 0.008),
                                                                                                                                            Container(
                                                                                                                                              alignment: Alignment.center,
                                                                                                                                              child: InkWell(
                                                                                                                                                onTap: () async {
                                                                                                                                                  var sendEmail = await LoginController.apiService.forgetPassword(mail.text);
                                                                                                                                                  isResend = (sendEmail == "Email was sent");
                                                                                                                                                  if (isResend) {
                                                                                                                                                    Fluttertoast.showToast(msg: "Email was sent");
                                                                                                                                                  } else {
                                                                                                                                                    displayDialog(context, "Error", "Something went wrong, try again");
                                                                                                                                                  }
                                                                                                                                                },
                                                                                                                                                child: Text(
                                                                                                                                                  "Resend Link",
                                                                                                                                                  style: GoogleFonts.poppins(
                                                                                                                                                    color: Color(0xff4048FF),
                                                                                                                                                    fontSize: MediaQuery.of(context).size.height * 0.0165,
                                                                                                                                                    fontWeight: FontWeight.normal,
                                                                                                                                                  ),
                                                                                                                                                ),
                                                                                                                                              ),
                                                                                                                                            )
                                                                                                                                          ],
                                                                                                                                        ),
                                                                                                                                      ),
                                                                                                                                    ),
                                                                                                                                  );
                                                                                                                                });
                                                                                                                              });
                                                                                                                        } else {
                                                                                                                          setState(() {
                                                                                                                            isExist = false;
                                                                                                                            isEditedEmail = true;
                                                                                                                          });
                                                                                                                        }
                                                                                                                      } else {
                                                                                                                        Navigator.of(context).pop();
                                                                                                                        setState(() {
                                                                                                                          isAccountExist = false;
                                                                                                                          _loadingResetPassword = false;
                                                                                                                        });
                                                                                                                        triggerAccountInexistanceError(true);
                                                                                                                      }
                                                                                                                      setState(() {
                                                                                                                        _loadingResetPassword = false;
                                                                                                                        _isSuccessCaptcha = false;
                                                                                                                      });
                                                                                                                    }
                                                                                                                    break;
                                                                                                                  case "error":
                                                                                                                    {
                                                                                                                      setState(() {
                                                                                                                        _value = 0;
                                                                                                                        _textDisplay = false;
                                                                                                                        _textDisplayError = true;
                                                                                                                        _isWrongCaptcha = true;
                                                                                                                      });
                                                                                                                      LoginController.apiService.captcha().then((value) {
                                                                                                                        if (value != "error") {
                                                                                                                          captchaPuzzle = value;
                                                                                                                          setState(() {
                                                                                                                            _bytesPuzzleImage = base64Decode(captchaPuzzle[1].toString().substring(22));
                                                                                                                            _bytesOriginalImage = base64Decode(captchaPuzzle[0].toString().substring(22));
                                                                                                                            _value = 0;
                                                                                                                          });
                                                                                                                          Future.delayed(const Duration(milliseconds: 300), () {
                                                                                                                            setState(() {
                                                                                                                              _value = 0;
                                                                                                                              _isWrongCaptcha = false;
                                                                                                                              _textDisplay = true;
                                                                                                                              _textDisplayError = false;
                                                                                                                              _stopCaptcha = false;
                                                                                                                            });
                                                                                                                          });
                                                                                                                        } else {
                                                                                                                          setState(() {
                                                                                                                            _stopCaptcha = false;
                                                                                                                            _errorSignIn = true;
                                                                                                                            _notFound = false;
                                                                                                                          });
                                                                                                                          stateInitalisation(context);
                                                                                                                          Navigator.of(context).pop();
                                                                                                                        }
                                                                                                                      }).catchError((error) {
                                                                                                                        setState(() {
                                                                                                                          _errorSignIn = true;
                                                                                                                          _notFound = false;
                                                                                                                          _lockedLogin = false;
                                                                                                                          errorMailOrPass = false;
                                                                                                                          _stopCaptcha = false;
                                                                                                                        });
                                                                                                                        stateInitalisation(context);
                                                                                                                        Navigator.of(context).pop();
                                                                                                                      });
                                                                                                                    }
                                                                                                                    break;
                                                                                                                }
                                                                                                              }).catchError((error) {
                                                                                                                setState(() {
                                                                                                                  _errorSignIn = true;
                                                                                                                  _notFound = false;
                                                                                                                  _lockedLogin = false;
                                                                                                                  errorMailOrPass = false;
                                                                                                                  _stopCaptcha = false;
                                                                                                                });
                                                                                                                stateInitalisation(context);
                                                                                                                Navigator.of(context).pop();
                                                                                                              });
                                                                                                            },
                                                                                                          ),
                                                                                                        ),
                                                                                                      ),
                                                                                                      Padding(
                                                                                                          padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.15, top: MediaQuery.of(context).size.height * 0.012),
                                                                                                          child: SizedBox(
                                                                                                            child: _textDisplay
                                                                                                                ? Text(
                                                                                                                    "Slide to complete the puzzle",
                                                                                                                    style: GoogleFonts.poppins(color: Colors.black, fontWeight: FontWeight.w400, fontSize: MediaQuery.of(context).size.height * 0.016, fontStyle: FontStyle.normal),
                                                                                                                  )
                                                                                                                : (_textDisplayError
                                                                                                                    ? Text(
                                                                                                                        "Wrong captcha : Try again",
                                                                                                                        style: GoogleFonts.poppins(
                                                                                                                          fontWeight: FontWeight.w600,
                                                                                                                          fontSize: MediaQuery.of(context).size.height * 0.016,
                                                                                                                          fontStyle: FontStyle.normal,
                                                                                                                          color: const Color(0XFFF52079),
                                                                                                                        ),
                                                                                                                      )
                                                                                                                    : null),
                                                                                                          )),
                                                                                                    ],
                                                                                                  )
                                                                                                : SizedBox(
                                                                                                    width: 280,
                                                                                                    child: LinearProgressIndicator(),
                                                                                                  ),
                                                                                          ),
                                                                                          const Spacer(),
                                                                                        ],
                                                                                      ),
                                                                                    ),
                                                                                  ),
                                                                                );
                                                                              });
                                                                            });
                                                                      }
                                                                    : null,
                                                                child: !_loadingResetPassword
                                                                    ? Text(
                                                                        "Reset Password",
                                                                        style: GoogleFonts.poppins(
                                                                          fontSize: MediaQuery.of(context).size.height * 0.019,
                                                                          fontWeight: FontWeight.w600,
                                                                        ),
                                                                      )
                                                                    : SizedBox(
                                                                        height: MediaQuery.of(context).size.height * 0.018,
                                                                        width: MediaQuery.of(context).size.height * 0.018,
                                                                        child: CircularProgressIndicator(
                                                                          color: Colors.white,
                                                                        ),
                                                                      ),
                                                              ))
                                                          : Container(
                                                              height: MediaQuery.of(context).size.height * 0.06,
                                                              width: MediaQuery.of(context).size.width * 0.82,
                                                              decoration: BoxDecoration(borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.08), color: Color(0xFFF6F6FF), border: Border.all(color: Color(0xFFD6D6E8), width: 1)),
                                                              child: Center(
                                                                child: Text(
                                                                  "Reset Password",
                                                                  style: GoogleFonts.poppins(
                                                                    fontSize: MediaQuery.of(context).size.height * 0.019,
                                                                    fontWeight: FontWeight.w600,
                                                                    color: Color(0xFFADADC8),
                                                                  ),
                                                                ),
                                                              ),
                                                            ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                          );
                                        });
                                      });
                                  setState(() {
                                    _loadingResetPassword = false;
                                    _isSuccessCaptcha = false;
                                    _value = 0;
                                    _isWrongCaptcha = false;
                                    _textDisplay = true;
                                    _textDisplayError = false;
                                    _stopCaptcha = false;
                                  });
                                },
                                child: Padding(
                                  padding: const EdgeInsets.all(2.0),
                                  child: Text(
                                    "Forgot your password ?",
                                    style: GoogleFonts.poppins(color: Color(0xFFF6F6FF), fontSize: MediaQuery.of(context).size.width * 0.03, fontWeight: FontWeight.w500, letterSpacing: 1.2),
                                  ),
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.fromLTRB(0, MediaQuery.of(context).size.height * 0.05, 0, MediaQuery.of(context).viewInsets.bottom.toString() == "0.0" ? 0 : MediaQuery.of(context).size.height * 0.4),
                                child: Container(
                                  height: MediaQuery.of(context).size.height * 0.06,
                                  width: MediaQuery.of(context).size.width * 0.75,
                                  child: ElevatedButton(
                                      style: ButtonStyle(
                                        backgroundColor: MaterialStateProperty.all(
                                          Color(0xFFFFFFFF),
                                        ),
                                        shape: MaterialStateProperty.all(
                                          RoundedRectangleBorder(
                                            borderRadius: BorderRadius.circular(30),
                                          ),
                                        ),
                                      ),
                                      onPressed: () async {
                                        gotoPageGlobal(context, Signup());
                                      },
                                      child: Text(
                                        "Create a new account",
                                        style: GoogleFonts.poppins(
                                          color: Color(0xFF4048FF),
                                          fontWeight: FontWeight.w600,
                                          fontSize: MediaQuery.of(context).size.width * 0.04,
                                        ),
                                      )),
                                ),
                              ),
                            ]),
                            searchMail && hasFocus
                                ? Visibility(
                                    child: surFix.isNotEmpty
                                        ? Padding(
                                            padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.064),
                                            child: Center(
                                              child: Container(
                                                width: MediaQuery.of(context).size.width * 0.75,
                                                decoration: BoxDecoration(
                                                  border: Border.all(color: Colors.grey.withOpacity(0.5)),
                                                  color: Colors.white,
                                                  borderRadius: BorderRadius.circular(20.0),
                                                ),
                                                child: Column(
                                                  children: [
                                                    Row(
                                                      children: [
                                                        Expanded(
                                                          child: Container(
                                                            child: ListView.builder(
                                                              shrinkWrap: true,
                                                              itemCount: surFix.length,
                                                              itemBuilder: (BuildContext context, int index) {
                                                                return ListTile(
                                                                  title: RichText(
                                                                      text: TextSpan(
                                                                    children: [
                                                                      new TextSpan(
                                                                        text: surFix[index].email.toString().toLowerCase().substring(0, surFix[index].email.toString().indexOf(_emailController.text.toLowerCase())),
                                                                        style: GoogleFonts.poppins(
                                                                          color: Color(0xFFADADC8),
                                                                          fontSize: 16,
                                                                          fontWeight: FontWeight.w600,
                                                                        ),
                                                                      ),
                                                                      new TextSpan(
                                                                        text: _emailController.text,
                                                                        style: GoogleFonts.poppins(
                                                                          color: Color(0xFF323754),
                                                                          fontSize: 16,
                                                                          fontWeight: FontWeight.w600,
                                                                        ),
                                                                      ),
                                                                      new TextSpan(
                                                                        text: surFix[index].email.toString().substring(surFix[index].email.toString().toLowerCase().indexOf(_emailController.text.toLowerCase()) + _emailController.text.length),
                                                                        style: GoogleFonts.poppins(
                                                                          color: Color(0xFFADADC8),
                                                                          fontSize: 16,
                                                                          fontWeight: FontWeight.w600,
                                                                        ),
                                                                      ),
                                                                    ],
                                                                  )),
                                                                  onTap: () {
                                                                    setState(() {
                                                                      hasFocus = false;
                                                                      selectedIndex = index;
                                                                      savedMail = surFix[index].email.toString();
                                                                      _emailController.text = savedMail;
                                                                      _passwordController.text = surFix[index].password.toString();
                                                                      _mailFieldForm = true;
                                                                      _passwordFieldForm = true;
                                                                      user.email = _emailController.text;
                                                                      user.password = _passwordController.text;
                                                                    });
                                                                  },
                                                                );
                                                              },
                                                            ),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                          )
                                        : SizedBox.shrink(),
                                  )
                                : SizedBox.shrink(),
                          ]),
                        ],
                      ),
                    ),
                  ),
                )),
          ),
        ));
  }

  searchSavedMail(String searchedString) {
    final suggestions = loginsList.where((surFix) {
      final email = surFix.email!.contains(searchedString);
      return email;
    }).toList();
    setState(() => surFix = suggestions);
  }

  loginUI() async {
    var model = Provider.of<LoginController>(context, listen: false);
    if ((model.userDetails?.userToken != null) && (model.userDetails?.userToken != "null") && (model.userDetails?.userToken != "error") && (model.userDetails?.userToken != "")) {
      if (LoginController.apiService.is2FA) {
        if (model.userDetails?.idSn == "0") {
          handleSavedPassword();
        }
        return Center(
          child: neededUI(TowFactorPage()),
        );
      } else {
        if (model.userDetails?.idSn != null && model.userDetails?.idSn != 0 && model.userDetails?.idSn != "0" && !LoginController.apiService.isEnabled) {
          return Center(
            child: neededUI(SocialMail()),
          );
        } else {
          if (LoginController.apiService.isEnabled) {
            if (LoginController.apiService.hasWallet) {
              if (LoginController.apiService.isCompletedPassPhrase) {
                if (LoginController.apiService.hasBiometric) {
                  var isauthenticated = await LoginController.apiService.authenticate();
                  if (isauthenticated == true) {
                    if (model.userDetails?.idSn == "0") {
                      handleSavedPassword();
                    }
                    return Center(
                      child: neededUI(WelcomeScreen()),
                    );
                  } else {
                    return Center(
                      child: neededUI(Signin()),
                    );
                  }
                } else {
                  if (model.userDetails?.idSn == "0") {
                    handleSavedPassword();
                  }
                  return Center(
                    child: neededUI(WelcomeScreen()),
                  );
                }
              } else {
                if (model.userDetails?.idSn == "0") {
                  handleSavedPassword();
                }
                return Center(
                  child: neededUI(PassPhrase()),
                );
              }
            } else {
              if (model.userDetails?.idSn == "0") {
                handleSavedPassword();
              }
              return Center(
                child: neededUI(TransactionPassword()),
              );
            }
          } else {
            if (model.userDetails?.idSn == "0") {
              handleSavedPassword();
            }
            return Center(
              child: neededUI(ActivationMail()),
            );
          }
        }
      }
    } else {
      return loginControls(context);
    }
  }

  neededUI(Widget page) {
    gotoPageGlobal(context, page);
  }

  loginControls(BuildContext context) {
    return Column(children: [
      Container(
          height: MediaQuery.of(context).size.height * 0.058,
          width: MediaQuery.of(context).size.width * 0.65,
          child: ElevatedButton(
            style: ButtonStyle(
              backgroundColor: MaterialStateProperty.all(
                Color(0xFF1967FF),
              ),
              shape: MaterialStateProperty.all(
                RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(MediaQuery.of(context).size.height * 0.07),
                ),
              ),
            ),
            onPressed: () async {
              await loginToSattWithFacebook();
            },
            child: _facebookLoading
                ? CircularProgressIndicator(
                    color: Colors.blueAccent,
                  )
                : Row(children: [
                    Padding(
                      padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.007, 0, 0, 0),
                      child: SvgPicture.asset(
                        'images/facebook-icon.svg',
                        alignment: Alignment.center,
                        color: Colors.white,
                        width: MediaQuery.of(context).size.width * 0.03,
                        height: MediaQuery.of(context).size.width * 0.047,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.05, 0, 0, 0),
                      child: Text(
                        "Sign in with Facebook",
                        style: GoogleFonts.roboto(color: Colors.white, fontWeight: FontWeight.w500, fontSize: MediaQuery.of(context).size.width * 0.038, fontStyle: FontStyle.normal),
                      ),
                    ),
                  ]),
          )),
      Padding(
        padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.015),
        child: Container(
            height: MediaQuery.of(context).size.height * 0.058,
            width: MediaQuery.of(context).size.width * 0.65,
            child: ElevatedButton(
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all(
                  Colors.white,
                ),
                shape: MaterialStateProperty.all(
                  RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(MediaQuery.of(context).size.height * 0.07),
                  ),
                ),
              ),
              onPressed: () async {
                Provider.of<LoginController>(context, listen: false).Balance = "";
                if (await GoogleApiAvailability.instance.checkGooglePlayServicesAvailability() != GooglePlayServicesAvailability.serviceInvalid) {
                  Provider.of<LoginController>(context, listen: false).Balance = "";
                  await loginToSattWithGoogle();
                }
              },
              child: _googleLoading
                  ? CircularProgressIndicator(
                      color: Colors.blueAccent,
                    )
                  : Row(children: [
                      SvgPicture.asset(
                        'images/google_image.svg',
                        alignment: Alignment.center,
                        width: MediaQuery.of(context).size.width * 0.037,
                        height: MediaQuery.of(context).size.width * 0.037,
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.05),
                        child: Text(
                          "Sign in with Google",
                          style: GoogleFonts.roboto(color: Color(0xFF757575), fontWeight: FontWeight.w500, fontSize: MediaQuery.of(context).size.width * 0.038, fontStyle: FontStyle.normal),
                        ),
                      ),
                    ]),
            )),
      ),
      Padding(
        padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.015),
        child: Container(
          height: MediaQuery.of(context).size.height * 0.058,
          width: MediaQuery.of(context).size.width * 0.65,
          child: ElevatedButton(
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all(
                  Colors.white,
                ),
                shape: MaterialStateProperty.all(
                  RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(MediaQuery.of(context).size.height * 0.07),
                  ),
                ),
              ),
              child: _ihConnectLoading
                  ? CircularProgressIndicator(
                      color: Colors.blueAccent,
                    )
                  : Row(children: [
                      SvgPicture.asset(
                        'images/ihconnect.svg',
                        alignment: Alignment.center,
                        width: MediaQuery.of(context).size.width * 0.037,
                        height: MediaQuery.of(context).size.width * 0.037,
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.05),
                        child: Text(
                          "Sign in with iHave Connect",
                          style: GoogleFonts.roboto(color: Color(0xFF1F2337), fontWeight: FontWeight.w500, fontSize: MediaQuery.of(context).size.width * 0.038, fontStyle: FontStyle.normal),
                        ),
                      ),
                    ]),
              onPressed: () async {
                setState(() {
                  _errorSignIn = false;
                  _notFound = false;
                  _lockedLogin = false;
                  errorMailOrPass = false;
                  _stopCaptcha = false;
                });
                await iHaveConnect();
              }),
        ),
      ),
      Padding(
        padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.015),
        child: Platform.isIOS
            ? (!_loadingApple
                ? Container(
                    height: MediaQuery.of(context).size.height * 0.058,
                    width: MediaQuery.of(context).size.width * 0.65,
                    child: SignInWithAppleButton(
                        text: "Sign in with Apple",
                        height: MediaQuery.of(context).size.width * 0.095,
                        style: SignInWithAppleButtonStyle.black,
                        iconAlignment: IconAlignment.left,
                        borderRadius: BorderRadius.circular(60.0),
                        onPressed: () async {
                          try {
                            setState(() {
                              _loadingApple = true;
                            });
                            _isLoginApple = await Provider.of<LoginController>(context, listen: false).loginWithApple();
                            if (_isLoginApple) {
                              Provider.of<LoginController>(context, listen: false).userDetails?.picLink =
                                  Provider.of<LoginController>(context, listen: false).userDetails?.picLink == "AppleIDAuthorizationScopes.fullName" ? "" : Provider.of<LoginController>(context, listen: false).userDetails?.picLink;
                              Provider.of<LoginController>(context, listen: false).Balance = "";
                              if (!LoginController.apiService.hasWallet) {
                                gotoPageGlobal(context, TransactionPassword());
                                setState(() {
                                  _loadingApple = false;
                                });
                              } else {
                                if (!LoginController.apiService.isCompletedPassPhrase) {
                                  gotoPageGlobal(context, PassPhrase());
                                  setState(() {
                                    _loadingApple = false;
                                  });
                                } else {
                                  if (LoginController.apiService.is2FA) {
                                    gotoPageGlobal(context, TowFactorPage());
                                    setState(() {
                                      _loadingApple = false;
                                    });
                                  } else {
                                    Provider.of<WalletController>(context, listen: false).selectedIndex = 2;
                                    gotoPageGlobal(context, WelcomeScreen());
                                    setState(() {
                                      _loadingApple = false;
                                    });
                                  }
                                }
                              }
                            } else {
                              setState(() {
                                _loadingApple = false;
                              });
                            }
                          } catch (e) {
                            if (!e.toString().contains('AuthorizationErrorCode.canceled')) {
                              displayDialog(context, "Error", "Something went wrong, Please try again");
                              setState(() {
                                _loadingApple = false;
                              });
                            } else {
                              setState(() {
                                _loadingApple = false;
                              });
                            }
                          }
                        }))
                : SizedBox(
                    height: MediaQuery.of(context).size.height * 0.018,
                    width: MediaQuery.of(context).size.height * 0.018,
                    child: CircularProgressIndicator(
                      color: Colors.white,
                    )))
            : null,
      ),
    ]);
  }

  Future<void> iHaveConnect() async {
    var ihConnectToken = "";
    setState(() {
      _ihConnectLoading = true;
      Provider.of<LoginController>(context, listen: false).Balance = "";
    });
    String? qrResult = "";
    if (Platform.isAndroid) {
      var permission = Platform.isAndroid ? Permission.storage : Permission.photos;
      var status_storage = await Permission.storage.status;

      var status = await permission.request();
      if (status == PermissionStatus.granted) {
        final result = await Permission.camera.request();
        if (result == PermissionStatus.granted) {
          FocusScope.of(context).requestFocus(new FocusNode());
          Future.delayed(Duration(milliseconds: 200), () async {
            try {
              String? qrResult = await MajaScan.startScan(title: "QRcode scanner", titleColor: Colors.amberAccent[700], qRCornerColor: Colors.orange, qRScannerColor: Colors.orange);
              setState(() {
                ihConnectToken = qrResult ?? '';
              });
              await handleIhConnect(ihConnectToken);
            } on PlatformException catch (ex) {
              if (ex.code == MajaScan.CameraAccessDenied) {
                setState(() {
                  ihConnectToken = "permission";
                });
              } else {
                setState(() {
                  ihConnectToken = "error";
                });
              }
            } on FormatException {
              setState(() {
                ihConnectToken = "back";
              });
            } catch (ex) {
              setState(() {
                ihConnectToken = "error";
              });
            }
          });
        } else {
          showDialog(
              context: context,
              builder: (BuildContext context) => CupertinoAlertDialog(
                    title: Text('Camera Permission'),
                    content: Text('This app needs camera access to  scan QR code '),
                    actions: <Widget>[
                      CupertinoDialogAction(
                        child: Text('Deny'),
                        onPressed: () => Navigator.of(context).pop(),
                      ),
                      CupertinoDialogAction(
                        child: Text('Settings'),
                        onPressed: () => openAppSettings(),
                      ),
                    ],
                  ));
        }
      }
    } else {
      FocusScope.of(context).requestFocus(new FocusNode());
      Future.delayed(Duration(milliseconds: 200), () async {
        try {
          String? qrResult = await MajaScan.startScan(title: "QRcode scanner", titleColor: Colors.amberAccent[700], qRCornerColor: Colors.orange, qRScannerColor: Colors.orange);
          setState(() {
            ihConnectToken = qrResult ?? '';
          });
          await handleIhConnect(ihConnectToken);
        } on PlatformException catch (ex) {
          if (ex.code == MajaScan.CameraAccessDenied) {
            setState(() {
              ihConnectToken = "permission";
            });
          } else {
            setState(() {
              ihConnectToken = "error";
            });
          }
        } on FormatException {
          setState(() {
            ihConnectToken = "back";
          });
        } catch (ex) {
          setState(() {
            ihConnectToken = "error";
          });
        }
      });
    }
    setState(() {
      _ihConnectLoading = false;
    });
  }

  Future<void> handleIhConnect(String ihToken) async {
    if (ihToken != null && ihToken != "" && ihToken != "permission" && ihToken != "error" && ihToken != "back") {
      var user = await Provider.of<LoginController>(context, listen: false).getUserDetails(ihToken);
      if (user.runtimeType != String) {
        Future.wait([
          LoginController.apiService.checkValidation(ihToken),
        ]).then((value) {
          if (value[0] == "success" || value == "success") {
            Provider.of<LoginController>(context, listen: false).userDetails?.idSn = LoginController.apiService.idSn.toString();

            gotoPageGlobal(context, WelcomeScreen());
          } else {
            setState(() {
              _errorSignIn = true;
            });
          }
        }).catchError((error) async {
          setState(() {
            _errorSignIn = true;
            _notFound = false;
            _lockedLogin = false;
            errorMailOrPass = false;
            _stopCaptcha = false;
          });
        });
      } else {
        setState(() {
          _errorSignIn = true;
          _notFound = false;
          _lockedLogin = false;
          errorMailOrPass = false;
          _stopCaptcha = false;
        });
      }
    }
  }

  Future<void> loginToSattWithGoogle() async {
    setState(() {
      _googleLoading = true;
      Provider.of<LoginController>(context, listen: false).Balance = "";
    });
    GooglePlayServicesAvailability availability = await GoogleApiAvailability.instance.checkGooglePlayServicesAvailability(true);
    var loginGoogle = await Provider.of<LoginController>(context, listen: false).googleSignin();
    var isConnected = (loginGoogle != "" && loginGoogle != "canceled");
    if (isConnected) {
      if (loginGoogle == "account_doesnt_exist" || loginGoogle == null || loginGoogle == "") {
        setState(() {
          verifyAccountGoogle = true;
          verifyAccount = false;
        });
        setState(() {
          errorAccountExist = true;
        });
      } else if (Provider.of<LoginController>(context, listen: false).userDetails?.userToken.toString() != "" && Provider.of<LoginController>(context, listen: false).userDetails?.userToken.toString().toString() != "null") {
        Future.wait([LoginController.apiService.checkValidation(Provider.of<LoginController>(context, listen: false).userDetails!.userToken.toString())]).then((value) {
          if (value[0] == "success") {
            setState(() {
              Provider.of<LoginController>(context, listen: false).Balance = "";
            });
            Provider.of<LoginController>(context, listen: false).userDetails?.email = LoginController.apiService.userMail;
            Provider.of<LoginController>(context, listen: false).userDetails?.name = LoginController.apiService.firstName;
            Provider.of<LoginController>(context, listen: false).userDetails?.lastName = LoginController.apiService.lastName;
            loginUI();
          } else {
            setState(() {
              _errorSignIn = true;
            });
            stateInitalisation(context);
            Navigator.of(context).pop();
          }
        }).catchError((error) {
          setState(() {
            _errorSignIn = true;
          });
          Navigator.of(context).pop();

          stateInitalisation(context);
        });
      }
    } else if (loginGoogle == "canceled") {
      setState(() {
        _googleLoading = false;
      });
    } else {
      displayDialog(context, "Error", "try another time");
    }
    setState(() {
      _googleLoading = false;
    });
  }

  Future<void> loginToSattWithFacebook() async {
    setState(() {
      _facebookLoading = true;
    });
    var isLogged = await Provider.of<LoginController>(context, listen: false).facebookSignin();
    if (isLogged == "account_doesnt_exist" || isLogged == null || isLogged == "") {
      setState(() {
        verifyAccount = true;
        verifyAccountGoogle = false;
      });
      setState(() {
        errorAccountExist = true;
      });
    } else if (isLogged == "canceled") {
      setState(() {
        _facebookLoading = false;
      });
    } else {
      if (Provider.of<LoginController>(context, listen: false).userDetails?.userToken.toString() != "" && Provider.of<LoginController>(context, listen: false).userDetails?.userToken.toString().toString() != "null") {
        Future.wait([LoginController.apiService.checkValidation(Provider.of<LoginController>(context, listen: false).userDetails!.userToken.toString())]).then((value) {
          if (value[0] == "success") {
            Provider.of<LoginController>(context, listen: false).userDetails?.email = LoginController.apiService.userMail;
            Provider.of<LoginController>(context, listen: false).userDetails?.name = LoginController.apiService.firstName;
            Provider.of<LoginController>(context, listen: false).userDetails?.lastName = LoginController.apiService.lastName;
            loginUI();
            Navigator.of(context).pop;
          } else {
            setState(() {
              _errorSignIn = true;
            });
            stateInitalisation(context);
            Navigator.of(context).pop();
          }
        });
      }
    }
    setState(() {
      _facebookLoading = false;
    });
  }

  stateInitalisation(BuildContext context) async {
    setState(() {
      _isLoading = false;
      _isButtonActive = true;
    });
    if (_lockedLogin) {
      startTimer();
    }
    Provider.of<LoginController>(context, listen: false).Balance = "";
  }

  void _showSnackBar(String pin, BuildContext context) {
    final snackBar = SnackBar(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(18.0),
      ),
      duration: Duration(seconds: 5),
      content: Container(
          height: 80.0,
          color: Colors.grey,
          child: Center(
            child: Text(
              'Pin Submitted. Value: $pin',
              style: TextStyle(fontSize: 25.0, backgroundColor: Colors.grey),
            ),
          )),
      backgroundColor: Colors.grey,
    );
  }

  Widget buildAvailability(BuildContext bioContext) => buildButton(
        text: 'Check Availability',
        icon: Icons.event_available,
        onClicked: () async {
          final isAvailable = await APIService.hasBiometrics();
          final biometrics = await APIService.getBiometrics();

          final hasFingerprint = biometrics.contains(BiometricType.fingerprint);

          showDialog(
            context: bioContext,
            builder: (bioContext) => AlertDialog(
              title: Text('Availability'),
              content: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  buildText('Biometrics', isAvailable),
                  buildText('Fingerprint', hasFingerprint),
                ],
              ),
            ),
          );
        },
      );

  Widget buildText(String text, bool checked) => Container(
        margin: EdgeInsets.symmetric(vertical: 8),
        child: Row(
          children: [
            checked ? Icon(Icons.check, color: Colors.green, size: 24) : Icon(Icons.close, color: Colors.red, size: 24),
            const SizedBox(width: 12),
            Text(text, style: TextStyle(fontSize: 24)),
          ],
        ),
      );

  Widget buildButton({
    required String text,
    required IconData icon,
    required VoidCallback onClicked,
  }) =>
      ElevatedButton.icon(
        style: ElevatedButton.styleFrom(
          minimumSize: Size.fromHeight(50),
        ),
        icon: Icon(icon, size: 26),
        label: Text(
          text,
          style: TextStyle(fontSize: 20),
        ),
        onPressed: onClicked,
      );

  String setSavedPassword(String savedMail) {
    var password = "";
    setState(() {
      if (password.isNotEmpty) {
        _passwordFieldForm = true;
      }
      _mailFieldForm = true;
      searchMail = false;
    });
    return password;
  }

  void handleSavedPassword() {
    if (!surFix.any((e) => e.email == user.email.toLowerCase())) {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return StatefulBuilder(builder: (context, setState) {
              return AlertDialog(
                insetPadding: EdgeInsets.zero,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(15)),
                ),
                content: Container(
                  height: MediaQuery.of(context).size.height * 0.3,
                  width: MediaQuery.of(context).size.width * 0.75,
                  child: Column(mainAxisAlignment: MainAxisAlignment.center, crossAxisAlignment: CrossAxisAlignment.center, children: [
                    Spacer(),
                    Padding(
                      padding: const EdgeInsets.all(1.0),
                      child: Text(
                        "Do you want to save your login\ncredentials ?",
                        textAlign: TextAlign.center,
                        style: GoogleFonts.poppins(color: Colors.black, fontSize: MediaQuery.of(context).size.height * 0.022, fontWeight: FontWeight.w600),
                      ),
                    ),
                    Spacer(),
                    Padding(
                      padding: const EdgeInsets.all(1.0),
                      child: Text(
                        "You won’t have to enter your connexion\npassword to login anymore.",
                        textAlign: TextAlign.center,
                        style: GoogleFonts.poppins(color: Colors.black, fontSize: MediaQuery.of(context).size.height * 0.02, fontWeight: FontWeight.w500),
                      ),
                    ),
                    Spacer(),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(1.0),
                          child: TextButton(
                            style: ButtonStyle(
                              elevation: MaterialStateProperty.all(2.0),
                              shadowColor: MaterialStateProperty.all(Color(0xFF323754)),
                              minimumSize: MaterialStateProperty.all(Size(MediaQuery.of(context).size.width * 0.35, MediaQuery.of(context).size.height * 0.06)),
                              backgroundColor: MaterialStateProperty.all(
                                Colors.white,
                              ),
                              shape: MaterialStateProperty.all(
                                RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.08),
                                ),
                              ),
                            ),
                            child: Text(
                              "No",
                              style: GoogleFonts.poppins(color: Color(0XFF4048FF), fontWeight: FontWeight.w600),
                            ),
                            onPressed: () {
                              addContactDataLogin(
                                user.email,
                                "",
                              );
                              Navigator.of(context).pop();
                            },
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(1.0),
                          child: TextButton(
                            style: ButtonStyle(
                              minimumSize: MaterialStateProperty.all(Size(MediaQuery.of(context).size.width * 0.35, MediaQuery.of(context).size.height * 0.06)),
                              backgroundColor: MaterialStateProperty.all(
                                Color(0XFF4048FF),
                              ),
                              shape: MaterialStateProperty.all(
                                RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.08),
                                ),
                              ),
                            ),
                            child: Text(
                              "Yes",
                              style: GoogleFonts.poppins(color: Colors.white, fontWeight: FontWeight.w600),
                            ),
                            onPressed: () {
                              addContactDataLogin(user.email, user.password);
                              Navigator.of(context).pop();
                            },
                          ),
                        ),
                      ],
                    ),
                  ]),
                ),
              );
            });
          });
    } else {
      var password = surFix.firstWhereOrNull((e) => e.email == user.email)?.password ?? "";
      if (password != user.password && password != "") {
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return StatefulBuilder(builder: (context, setState) {
                return AlertDialog(
                  insetPadding: EdgeInsets.zero,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(15)),
                  ),
                  content: Container(
                    height: MediaQuery.of(context).size.height * 0.2,
                    width: MediaQuery.of(context).size.width * 0.75,
                    child: Column(mainAxisAlignment: MainAxisAlignment.center, crossAxisAlignment: CrossAxisAlignment.center, children: [
                      Spacer(),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          "Do you wish to update your password for ${user.email}?",
                          textAlign: TextAlign.center,
                          style: GoogleFonts.poppins(color: Colors.black, fontSize: MediaQuery.of(context).size.height * 0.022, fontWeight: FontWeight.w600),
                        ),
                      ),
                      Spacer(),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(1.0),
                            child: TextButton(
                              style: ButtonStyle(
                                elevation: MaterialStateProperty.all(2.0),
                                shadowColor: MaterialStateProperty.all(Color(0xFF323754)),
                                minimumSize: MaterialStateProperty.all(Size(MediaQuery.of(context).size.width * 0.35, MediaQuery.of(context).size.height * 0.06)),
                                backgroundColor: MaterialStateProperty.all(
                                  Colors.white,
                                ),
                                shape: MaterialStateProperty.all(
                                  RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.08),
                                  ),
                                ),
                              ),
                              child: Text(
                                "No",
                                style: GoogleFonts.poppins(color: Color(0XFF4048FF), fontWeight: FontWeight.w600),
                              ),
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(1.0),
                            child: TextButton(
                              style: ButtonStyle(
                                minimumSize: MaterialStateProperty.all(Size(MediaQuery.of(context).size.width * 0.35, MediaQuery.of(context).size.height * 0.06)),
                                backgroundColor: MaterialStateProperty.all(
                                  Color(0XFF4048FF),
                                ),
                                shape: MaterialStateProperty.all(
                                  RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.08),
                                  ),
                                ),
                              ),
                              child: Text(
                                "Yes",
                                style: GoogleFonts.poppins(color: Colors.white, fontWeight: FontWeight.w600),
                              ),
                              onPressed: () {
                                var index = surFix.firstWhereOrNull((e) => e.email == user.email.toLowerCase())?.id;
                                if (index != null) {
                                  removecontactDataLogin(index!);
                                }
                                addContactDataLogin(user.email.toLowerCase(), user.password);
                                Navigator.of(context).pop();
                              },
                            ),
                          ),
                        ],
                      ),
                    ]),
                  ),
                );
              });
            });
      }
    }
  }

  void rebuildAllChildren(BuildContext context) {
    void rebuild(Element el) {
      el.markNeedsBuild();
      el.visitChildren(rebuild);
    }

    (context as Element).visitChildren(rebuild);
  }

  void triggerAccountInexistanceError(bool state) {
    Provider.of<LoginController>(context, listen: false).setResetError(state);
  }

  void searchbook(String query) {
    final suggestions = loginsList.where((surFix) {
      final email = surFix.email!.toLowerCase();
      final input = query.toLowerCase();
      return email.contains(input);
    }).toList();
    setState(() => surFix = suggestions);
  }
}