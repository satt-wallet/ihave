import 'dart:async';
import 'package:custom_image_crop/custom_image_crop.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:ihave/ui/WelcomeScreen.dart';
import 'package:ihave/ui/staticElements/headerNavigation.dart';
import 'package:ihave/util/utils.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:local_auth/local_auth.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:permission_handler_platform_interface/permission_handler_platform_interface.dart';
import 'package:provider/provider.dart';

import '../controllers/LoginController.dart';
import '../controllers/walletController.dart';
import '../model/userDetails.dart';
import '../service/apiService.dart';
import '../widgets/customAppBar.dart';
import '../widgets/customBottomBar.dart';

class Biometrics extends StatefulWidget {
  const Biometrics({Key? key}) : super(key: key);

  @override
  State<Biometrics> createState() => _BiometricsState();
}

class _BiometricsState extends State<Biometrics> {
  bool _isVisible = false;
  bool _isProfileVisible = false;
  bool _isWalletVisible = false;
  bool _isNotificationVisible = false;
  bool ischecked = false;
  var biometrics = TextEditingController();
  late CustomImageCropController controller;
  late StreamSubscription subscription;
  bool _hasNetwork = true;
  final _formKey = GlobalKey<FormState>();
  bool isBiometricalAuthentificated = false;
  bool _isButtonActive = false;

  Future getUser() async {
    var user = await Provider.of<LoginController>(context, listen: false).getUserDetails(Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "");
    return user;
  }

  Future updateUser() async {
    var newuserDetails = newUser();
    var newUserUpdated = await Provider.of<LoginController>(context, listen: false).putUserDetails(Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "", newuserDetails);
    return newUserUpdated;
  }

  void initState() {
    super.initState();
    controller = CustomImageCropController();
    subscription = InternetConnectionChecker().onStatusChange.listen((status) {
      final hasInternet = status == InternetConnectionStatus.connected;
      setState(() {
        _hasNetwork = hasInternet;
      });
    });
    setState(() {
      ischecked = LoginController.apiService.isSwitched;
    });
    getUser().then((value) {
      setState(() {
        ischecked = value.hasbiometrics;
      });

      setState(() {
        biometrics.text = value.hasbiometrics.toString();
      });
    });
    Provider.of<LoginController>(context, listen: false).tokenSymbol == "";
    Provider.of<LoginController>(context, listen: false).tokenNetwork == "";
  }

  UserDetails newUser() {
    return new UserDetails(
      hasbiometrics: ischecked,
    );
  }

  @override
  Widget build(BuildContext context) {
    setState(() {
      _isVisible = Provider.of<WalletController>(context, listen: true).isSideMenuVisible;
      _isProfileVisible = Provider.of<WalletController>(context, listen: true).isProfileVisible;
      _isWalletVisible = Provider.of<WalletController>(context, listen: true).isWalletVisible;
      _isNotificationVisible = Provider.of<WalletController>(context, listen: true).isNotificationVisible;
    });
    return Scaffold(
        appBar: CustomAppBar(),
        body: WillPopScope(
          onWillPop: () {
            return Future.value(true);
          },
          child: Stack(
            children: [
              Container(
                child: Column(
                  children: [
                    Padding(
                      padding: EdgeInsets.only(
                        top: MediaQuery.of(context).size.height * 0.03,
                        left: MediaQuery.of(context).size.height * 0.02,
                      ),
                      child: Align(
                        alignment: Alignment.topLeft,
                        child: Text(
                          "Biometrics",
                          style: GoogleFonts.poppins(
                            color: Colors.black,
                            fontSize: MediaQuery.of(context).size.width * 0.08,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.02),
                      child: Center(
                        child: SvgPicture.asset(
                          'images/moonbiometrics.svg',
                          width: 150,
                          height: 150,
                        ),
                      ),
                    ),
                    Center(
                      child: Padding(
                        padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.height * 0.025, MediaQuery.of(context).size.width * 0.05, MediaQuery.of(context).size.height * 0.03, MediaQuery.of(context).size.width * 0.01),
                        child: Form(
                          child: Row(
                            children: [
                              Text("Enable Biometrics",
                                  style: GoogleFonts.poppins(
                                    color: Colors.black,
                                    fontSize: MediaQuery.of(context).size.width * 0.045,
                                    fontWeight: FontWeight.w700,
                                  )),
                              Spacer(),
                              Switch(
                                value: ischecked,
                                onChanged: (value) async {
                                  ischecked = value;
                                  LoginController.apiService.isSwitched = ischecked;

                                  var isauthenticated = await LoginController.apiService.authenticate();
                                  setState(() {
                                    _isButtonActive = isauthenticated;
                                  });
                                },
                                activeTrackColor: Color(0xFFA0A0FF),
                                activeColor: Color(0xFF4048FF),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.height * 0.018, MediaQuery.of(context).size.height * 0.01, MediaQuery.of(context).size.height * 0.02, MediaQuery.of(context).size.height * 0.02),
                      child: Text(
                        "Use the biometrics system of your mobile \nphone to open your wallet.\nFaceID or Digital prints",
                        style: GoogleFonts.poppins(
                          color: Colors.black,
                          fontSize: MediaQuery.of(context).size.width * 0.037,
                          fontWeight: FontWeight.normal,
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.height * 0.018, MediaQuery.of(context).size.height * 0.28, MediaQuery.of(context).size.height * 0.02, MediaQuery.of(context).size.height * 0.02),
                      child: Container(
                        height: MediaQuery.of(context).size.height * 0.058,
                        width: MediaQuery.of(context).size.width * 0.75,
                        child: ElevatedButton(
                          style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.all(
                              _isButtonActive ? Color(0xFF4048FF) : Color(0xFFF6F6FF),
                            ),
                            shape: MaterialStateProperty.all(
                              RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(MediaQuery.of(context).size.height * 0.07),
                              ),
                            ),
                          ),
                          onPressed: _isButtonActive
                              ? () async {
                                  await LoginController.apiService.activateBiometricAuth(Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "", ischecked);
                                  setState(() {
                                    Provider.of<WalletController>(context, listen: false).selectedIndex = 2;
                                  });
                                  gotoPageGlobal(context, WelcomeScreen());
                                }
                              : null,
                          child: Text(
                            "Continue",
                            style: GoogleFonts.poppins(
                              color: _isButtonActive ? Colors.white : Colors.grey,
                              fontSize: MediaQuery.of(context).size.width * 0.037,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
              HeaderNavigation(_isVisible, _isWalletVisible, _isNotificationVisible)
            ],
          ),
        ));
  }
}
