import 'package:azlistview/azlistview.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:ihave/main.dart';
import 'package:ihave/util/contactsHive.dart';
import 'package:ihave/widgets/customAppBar.dart';
import 'package:one_context/one_context.dart';
import '../model/contact.dart';
import '../util/Sqflite.dart';
import 'dart:io';
import 'package:flutter/services.dart';
import 'package:majascan/majascan.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:permission_handler_platform_interface/permission_handler_platform_interface.dart';

class _AZItem extends ISuspensionBean {
  late final Contact title;
  late final String tag;

  _AZItem({
    required this.title,
    required this.tag,
  });

  @override
  String getSuspensionTag() => tag;
}

List<Contact> surFix = [];

TextEditingController _searchaddbook = new TextEditingController();
TextEditingController _nameaddbook = new TextEditingController();
TextEditingController _walletAddress = new TextEditingController();
TextEditingController _nameaddbook1 = new TextEditingController();
TextEditingController _walletAddress1 = new TextEditingController();

class AdressBook extends StatefulWidget {
  final List<Contact> contacts;

  const AdressBook({
    Key? key,
    required this.contacts,
  }) : super(key: key);

  @override
  _AdressBookPageState createState() => _AdressBookPageState();
}

class _AdressBookPageState extends State<AdressBook> {
  List<_AZItem> items = [];

  late List<Map<dynamic, dynamic>> boxMap;
  List<Contact> contactsList = [];

  @override
  void initState() {
    super.initState();
    load();
  }

  bool _isLoading = true;

  load() async {
    final data = await SQLHelper.getItems();
    setState(() {
      boxMap = data;
      fillContactList();
      surFix.clear();
      items.clear();
      surFix.addAll(contactsList);
      var itemTemp = surFix.map((item) => _AZItem(title: item, tag: item?.name?.toLowerCase()[0] ?? "")).toList();
      this.items.addAll(itemTemp);
      SuspensionUtil.sortListBySuspensionTag(this.items);
      SuspensionUtil.setShowSuspensionStatus(this.items);
    });
  }

  void _refreshContacts() async {
    final data = await SQLHelper.getItems();
    setState(() {
      boxMap = data;
      _isLoading = false;
    });
    fillContactList();
    initList(widget.contacts);
  }

  void initList(List<Contact> contacts) {
    setState(() {
      surFix.clear();
      surFix.addAll(contactsList);
      var itemTemp = surFix.map((item) => _AZItem(title: item, tag: item?.name?.toLowerCase()[0] ?? "")).toList();
      this.items.addAll(itemTemp);
      SuspensionUtil.sortListBySuspensionTag(this.items);
      SuspensionUtil.setShowSuspensionStatus(this.items);
    });
  }

  displayAddContactDialog() {
    showDialog(
        barrierDismissible: false,
        context: MyApp.materialKey.currentContext ?? OneContext() as BuildContext,
        builder: (context) {
          return StatefulBuilder(builder: (BuildContext context, setState) {
            return Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              child: Dialog(
                  backgroundColor: Colors.transparent,
                  insetPadding: EdgeInsets.zero,
                  child: Container(
                    height: MediaQuery.of(context).size.height,
                    decoration: BoxDecoration(
                      color: Colors.white,
                    ),
                    child: Stack(
                      children: [
                        SingleChildScrollView(
                          child: Column(
                            children: [
                              Row(
                                children: [
                                  Padding(
                                    padding: EdgeInsets.only(top: 30, left: 15),
                                    child: Text(
                                      "Add a Contact",
                                      style: GoogleFonts.poppins(
                                        color: Color(0xFF1F2337),
                                        fontSize: 28,
                                        fontWeight: FontWeight.w700,
                                        fontStyle: FontStyle.normal,
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    width: MediaQuery.of(context).size.width * .3,
                                  ),
                                  Align(
                                    alignment: Alignment.topRight,
                                    child: Padding(
                                      padding: EdgeInsets.only(
                                        top: 20,
                                      ),
                                      child: IconButton(
                                          onPressed: () async {
                                            Navigator.of(context).pop();
                                            _walletAddress1.text = "";
                                          },
                                          icon: Icon(
                                            Icons.close,
                                            color: Color(0xFF75758F),
                                            size: MediaQuery.of(context).size.height * 0.045,
                                          )),
                                    ),
                                  ),
                                ],
                              ),
                              Column(
                                children: [
                                  SizedBox(
                                    height: 30,
                                  ),
                                  Image.asset(
                                    "images/Addbook.png",
                                    width: 220,
                                    height: 220,
                                  ),
                                  Center(
                                    child: Column(
                                      children: [
                                        Text(
                                          "Please fill in the contact name and address",
                                          style: GoogleFonts.poppins(
                                            color: Color(0xFF000000),
                                            fontSize: 15,
                                            fontWeight: FontWeight.w500,
                                            fontStyle: FontStyle.normal,
                                          ),
                                        ),
                                        Text(
                                          'to add it to your Adress Book.',
                                          style: GoogleFonts.poppins(
                                            color: Color(0xFF000000),
                                            fontSize: 15,
                                            fontWeight: FontWeight.w500,
                                            fontStyle: FontStyle.normal,
                                          ),
                                        ),
                                        SizedBox(
                                          height: 15,
                                        ),
                                        Center(
                                          child: Container(
                                            width: MediaQuery.of(context).size.width * 0.85,
                                            child: TextFormField(
                                              controller: _nameaddbook1,
                                              decoration: new InputDecoration(
                                                hintText: 'Name',
                                                fillColor: Colors.grey,
                                                focusColor: Colors.grey,
                                                hintStyle: GoogleFonts.poppins(fontWeight: FontWeight.normal, color: Colors.grey, fontSize: MediaQuery.of(context).size.width * 0.036),
                                                contentPadding: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width * 0.04, vertical: MediaQuery.of(context).size.height * 0.017),
                                                enabledBorder: OutlineInputBorder(
                                                    borderRadius: new BorderRadius.only(
                                                        topLeft: Radius.circular(MediaQuery.of(context).size.height * 0.05),
                                                        bottomLeft: Radius.circular(MediaQuery.of(context).size.height * 0.05),
                                                        topRight: Radius.circular(MediaQuery.of(context).size.height * 0.05),
                                                        bottomRight: Radius.circular(MediaQuery.of(context).size.height * 0.05)),
                                                    borderSide: BorderSide(
                                                      color: Color(0xFFD6D6E8),
                                                    )),
                                                focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Color(0xff4048FF))),
                                              ),
                                            ),
                                          ),
                                        ),
                                        SizedBox(
                                          height: 15,
                                        ),
                                        Center(
                                          child: Container(
                                            width: MediaQuery.of(context).size.width * 0.85,
                                            child: TextFormField(
                                              style: GoogleFonts.poppins(
                                                fontSize: MediaQuery.of(context).size.height * 0.018,
                                              ),
                                              controller: _walletAddress1,
                                              onChanged: (value) {},
                                              decoration: InputDecoration(
                                                hintText: "0x0eD7e5...859b14fD0",
                                                fillColor: Colors.grey,
                                                focusColor: Colors.grey,
                                                hintStyle: GoogleFonts.poppins(fontWeight: FontWeight.normal, color: Colors.grey, fontSize: MediaQuery.of(context).size.width * 0.036),
                                                suffixIcon: SizedBox(
                                                  width: MediaQuery.of(context).size.width * 0.2,
                                                  height: MediaQuery.of(context).size.height * 0.065,
                                                  child: ElevatedButton(
                                                    style: ButtonStyle(
                                                      backgroundColor: MaterialStateProperty.all(
                                                        Color(0xFFF6F6FF),
                                                      ),
                                                      shape: MaterialStateProperty.all(
                                                        RoundedRectangleBorder(
                                                            borderRadius: BorderRadius.only(topRight: Radius.circular(MediaQuery.of(context).size.height * 0.05), bottomRight: Radius.circular(MediaQuery.of(context).size.height * 0.05)),
                                                            side: BorderSide(
                                                              color: Color(0xFFD6D6E8),
                                                            )),
                                                      ),
                                                      elevation: MaterialStateProperty.resolveWith<double>(
                                                        (Set<MaterialState> states) {
                                                          if (states.contains(MaterialState.disabled)) {
                                                            return 0;
                                                          }
                                                          return 0; // Defer to the widget's default.
                                                        },
                                                      ),
                                                    ),
                                                    onPressed: () async {
                                                      String? qrResult = "";
                                                      if (Platform.isAndroid) {
                                                        var permission = Platform.isAndroid ? Permission.storage : Permission.photos;
                                                        var status_storage = await Permission.storage.status;

                                                        var status = await permission.request();
                                                        if (status == PermissionStatus.granted) {
                                                          final result = await Permission.camera.request();
                                                          if (result == PermissionStatus.granted) {
                                                            FocusScope.of(context).requestFocus(new FocusNode());
                                                            Future.delayed(Duration(milliseconds: 200), () async {
                                                              try {
                                                                String? qrResult = await MajaScan.startScan(title: "QRcode scanner", titleColor: Colors.amberAccent[700], qRCornerColor: Colors.orange, qRScannerColor: Colors.orange);
                                                                setState(() {
                                                                  _walletAddress1.text = qrResult ?? 'null string';
                                                                });
                                                              } on PlatformException catch (ex) {
                                                                if (ex.code == MajaScan.CameraAccessDenied) {
                                                                  setState(() {
                                                                    _walletAddress1.text = "Camera permission was denied";
                                                                  });
                                                                } else {
                                                                  setState(() {
                                                                    _walletAddress1.text = "Unknown Error $ex";
                                                                  });
                                                                }
                                                              } on FormatException {
                                                                setState(() {
                                                                  _walletAddress1.text = "You pressed the back button before scanning anything";
                                                                });
                                                              } catch (ex) {
                                                                setState(() {
                                                                  _walletAddress1.text = "Unknown Error $ex";
                                                                });
                                                              }
                                                            });
                                                          } else {
                                                            showDialog(
                                                                context: context,
                                                                builder: (BuildContext context) => CupertinoAlertDialog(
                                                                      title: Text('Camera Permission'),
                                                                      content: Text('This app needs camera access to  scan QR code '),
                                                                      actions: <Widget>[
                                                                        CupertinoDialogAction(
                                                                          child: Text('Deny'),
                                                                          onPressed: () => Navigator.of(context).pop(),
                                                                        ),
                                                                        CupertinoDialogAction(
                                                                          child: Text('Settings'),
                                                                          onPressed: () => openAppSettings(),
                                                                        ),
                                                                      ],
                                                                    ));
                                                          }
                                                        }
                                                      } else {
                                                        FocusScope.of(context).requestFocus(new FocusNode());
                                                        Future.delayed(Duration(milliseconds: 200), () async {
                                                          try {
                                                            String? qrResult = await MajaScan.startScan(title: "QRcode scanner", titleColor: Colors.amberAccent[700], qRCornerColor: Colors.orange, qRScannerColor: Colors.orange);
                                                            setState(() {
                                                              _walletAddress1.text = qrResult ?? 'null string';
                                                            });
                                                          } on PlatformException catch (ex) {
                                                            if (ex.code == MajaScan.CameraAccessDenied) {
                                                              setState(() {
                                                                _walletAddress1.text = "Camera permission was denied";
                                                              });
                                                            } else {
                                                              setState(() {
                                                                _walletAddress1.text = "Unknown Error $ex";
                                                              });
                                                            }
                                                          } on FormatException {
                                                            setState(() {
                                                              _walletAddress1.text = "You pressed the back button before scanning anything";
                                                            });
                                                          } catch (ex) {
                                                            setState(() {
                                                              _walletAddress1.text = "Unknown Error $ex";
                                                            });
                                                          }
                                                        });
                                                      }
                                                    },
                                                    child: Image.asset(
                                                      "images/bi_qr-code-scan.png",
                                                      height: MediaQuery.of(context).size.height * 0.035,
                                                    ),
                                                  ),
                                                ),
                                                errorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red)),
                                                contentPadding: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width * 0.04, vertical: MediaQuery.of(context).size.height * 0.017),
                                                enabledBorder: OutlineInputBorder(
                                                    borderRadius: new BorderRadius.only(
                                                        topLeft: Radius.circular(MediaQuery.of(context).size.height * 0.05),
                                                        bottomLeft: Radius.circular(MediaQuery.of(context).size.height * 0.05),
                                                        topRight: Radius.circular(MediaQuery.of(context).size.height * 0.05),
                                                        bottomRight: Radius.circular(MediaQuery.of(context).size.height * 0.05)),
                                                    borderSide: BorderSide(
                                                      color: Color(0xFFD6D6E8),
                                                    )),
                                                focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Color(0xff4048FF))),
                                              ),
                                            ),
                                          ),
                                        ),
                                        SizedBox(
                                          height: 30,
                                        ),
                                        ElevatedButton(
                                          child: Padding(
                                            padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.19, right: MediaQuery.of(context).size.width * 0.19, top: MediaQuery.of(context).size.height * 0.02, bottom: MediaQuery.of(context).size.height * 0.02),
                                            child: Text(
                                              "Add a new contact",
                                              style: GoogleFonts.poppins(fontWeight: FontWeight.w700, fontSize: MediaQuery.of(context).size.height * 0.02, color: Colors.white),
                                            ),
                                          ),
                                          onPressed: () {
                                            setState(() {
                                              if (_nameaddbook1.text != "" || _walletAddress1.text != "")
                                                addContactData(
                                                  _nameaddbook1.text,
                                                  _walletAddress1.text,
                                                );
                                              load();
                                              Navigator.of(context).pop();
                                              _walletAddress1.text = '';
                                              _nameaddbook1.text = '';
                                            });
                                          },
                                          style: ButtonStyle(
                                            backgroundColor: MaterialStateProperty.all(
                                              const Color(0xFF4048FF),
                                            ),
                                            shape: MaterialStateProperty.all(
                                              RoundedRectangleBorder(
                                                borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.08),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  SizedBox(
                                    height: 20,
                                  ),
                                ],
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  )),
            );
          });
        });
  }

  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    return Scaffold(
        appBar: CustomAppBar(),
        body: Column(
          children: <Widget>[
            Row(
              children: [
                Padding(
                  padding: EdgeInsets.only(top: height * .03, left: width * 0.055),
                  child: Text(
                    "Address Book",
                    style: GoogleFonts.poppins(
                      color: Color(0xFF1F2337),
                      fontSize: height * 0.04,
                      fontWeight: FontWeight.w700,
                      fontStyle: FontStyle.normal,
                    ),
                  ),
                ),
                SizedBox(
                  width: width * .2,
                ),
                GestureDetector(
                  onTap: () {
                    displayAddContactDialog();
                  },
                  child: Padding(
                    padding: EdgeInsets.only(top: height * .025),
                    child: Image.asset("images/Addressbookadd.png"),
                  ),
                ),
              ],
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(height * 0.03, width * 0.03, height * 0.03, width * 0.03),
              child: TextFormField(
                onChanged: (value) {
                  TextSelection previousSelection = _searchaddbook.selection;
                  _searchaddbook.text = value;
                  setState(() {
                    _searchaddbook.text = value;
                    searchbook(_searchaddbook.text);
                  });
                  _searchaddbook.selection = previousSelection;
                },
                controller: _searchaddbook,
                decoration: new InputDecoration(
                  hintText: 'Search',
                  prefixIcon: Padding(
                    padding: EdgeInsets.only(left: width * 0.025, right: width * 0.025),
                    child: SvgPicture.asset(
                      "images/search.svg",
                      width: width * 0.02,
                    ),
                  ),
                  fillColor: Colors.grey,
                  focusColor: Colors.grey,
                  hintStyle: GoogleFonts.poppins(fontWeight: FontWeight.normal, color: Colors.grey, fontSize: MediaQuery.of(context).size.width * 0.036),
                  contentPadding: new EdgeInsets.symmetric(vertical: 0.0, horizontal: MediaQuery.of(context).size.width * 0.03),
                  enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.grey.withOpacity(0.3))),
                  focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Color(0xff4048FF))),
                ),
              ),
            ),
            SizedBox(
              height: height * .02,
            ),
            Expanded(
              child: AzListView(
                padding: EdgeInsets.only(left: width * 0.02, right: width * 0.02, top: height * 0.02),
                data: items,
                itemCount: items.length,
                itemBuilder: (context, index) {
                  final item = items[index];
                  return buildListitem(
                    item,
                    index,
                    context,
                  );
                },
                indexBarOptions: IndexBarOptions(
                  needRebuild: false,
                  textStyle: TextStyle(color: Color(0xFF4048FF)),
                  selectTextStyle: TextStyle(
                    color: Color(0xFF4048FF),
                  ),
                  selectItemDecoration: BoxDecoration(
                    color: Colors.transparent,
                  ),
                ),
              ),
            ),
          ],
        ));
  }

  void searchbook(String query) {
    final suggestions = contactsList.where((items) {
      final address = items.address!.toLowerCase();
      final name = items.name!.toLowerCase();
      final input = query.toLowerCase();
      return address.contains(input) || name.contains(input);
    }).toList();
    setState(() {
      items.clear();
      items.addAll(suggestions.map((item) => _AZItem(title: item, tag: item?.name?.toLowerCase()[0] ?? "")).toList());
      SuspensionUtil.sortListBySuspensionTag(this.items);
      SuspensionUtil.setShowSuspensionStatus(this.items);
    });
  }

  fillContactList() {
    contactsList.clear();
    boxMap.forEach((element) {
      contactsList.add(Contact.fromJson(element));
    });
  }

  displayEditContactDialog(
    int index,
    String nameaddbook,
    String walletAddress,
  ) {
    _nameaddbook.text = nameaddbook;
    _walletAddress.text = walletAddress;
    showDialog(
        barrierDismissible: false,
        context: MyApp.materialKey.currentContext ?? OneContext() as BuildContext,
        builder: (context) {
          return StatefulBuilder(builder: (BuildContext context, setState) {
            return Dialog(
                backgroundColor: Colors.transparent,
                insetPadding: EdgeInsets.zero,
                child: Container(
                  height: MediaQuery.of(context).size.height * .44,
                  width: MediaQuery.of(context).size.height * .4,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(60.0),
                  ),
                  child: Stack(
                    children: [
                      SingleChildScrollView(
                        child: Column(
                          children: [
                            Padding(
                              padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.02, left: MediaQuery.of(context).size.width * 0.6),
                              child: IconButton(
                                  onPressed: () async {
                                    Navigator.of(context).pop();
                                    _nameaddbook.text = '';
                                    _walletAddress.text = '';
                                  },
                                  icon: Icon(
                                    Icons.close,
                                    color: Color(0xFF75758F),
                                    size: MediaQuery.of(context).size.height * 0.05,
                                  )),
                            ),
                            SizedBox(
                              height: MediaQuery.of(context).size.height * .0051,
                            ),
                            Text(
                              "Are you sure to edit",
                              textAlign: TextAlign.center,
                              style: GoogleFonts.poppins(
                                color: Color(0xFF1F2337),
                                fontSize: 23,
                                fontWeight: FontWeight.w600,
                                fontStyle: FontStyle.normal,
                              ),
                            ),
                            Text(
                              "this contact",
                              textAlign: TextAlign.center,
                              style: GoogleFonts.poppins(
                                color: Color(0xFF1F2337),
                                fontSize: 23,
                                fontWeight: FontWeight.w600,
                                fontStyle: FontStyle.normal,
                              ),
                            ),
                            SizedBox(
                              height: MediaQuery.of(context).size.height * .015,
                            ),
                            Center(
                              child: Container(
                                width: MediaQuery.of(context).size.width * 0.692,
                                child: TextFormField(
                                  controller: _nameaddbook,
                                  decoration: new InputDecoration(
                                    hintText: nameaddbook,
                                    fillColor: Colors.grey,
                                    focusColor: Colors.grey,
                                    hintStyle: GoogleFonts.poppins(fontWeight: FontWeight.normal, color: Colors.grey, fontSize: MediaQuery.of(context).size.width * 0.036),
                                    contentPadding: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width * 0.04, vertical: MediaQuery.of(context).size.height * 0.017),
                                    enabledBorder: OutlineInputBorder(
                                        borderRadius: new BorderRadius.only(
                                            topLeft: Radius.circular(MediaQuery.of(context).size.height * 0.05),
                                            bottomLeft: Radius.circular(MediaQuery.of(context).size.height * 0.05),
                                            topRight: Radius.circular(MediaQuery.of(context).size.height * 0.05),
                                            bottomRight: Radius.circular(MediaQuery.of(context).size.height * 0.05)),
                                        borderSide: BorderSide(
                                          color: Color(0xFFD6D6E8),
                                        )),
                                    focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Color(0xff4048FF))),
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: MediaQuery.of(context).size.height * .015,
                            ),
                            Center(
                              child: Container(
                                width: MediaQuery.of(context).size.width * 0.692,
                                child: TextFormField(
                                  style: GoogleFonts.poppins(
                                    fontSize: MediaQuery.of(context).size.height * 0.018,
                                  ),
                                  controller: _walletAddress,
                                  decoration: InputDecoration(
                                    hintText: walletAddress,
                                    fillColor: Colors.grey,
                                    focusColor: Colors.grey,
                                    hintStyle: GoogleFonts.poppins(fontWeight: FontWeight.normal, color: Colors.grey, fontSize: MediaQuery.of(context).size.width * 0.036),
                                    suffixIcon: SizedBox(
                                      width: MediaQuery.of(context).size.width * 0.2,
                                      height: MediaQuery.of(context).size.height * 0.065,
                                      child: ElevatedButton(
                                        style: ButtonStyle(
                                          backgroundColor: MaterialStateProperty.all(
                                            Color(0xFFF6F6FF),
                                          ),
                                          shape: MaterialStateProperty.all(
                                            RoundedRectangleBorder(
                                                borderRadius: BorderRadius.only(topRight: Radius.circular(MediaQuery.of(context).size.height * 0.05), bottomRight: Radius.circular(MediaQuery.of(context).size.height * 0.05)),
                                                side: BorderSide(
                                                  color: Color(0xFFD6D6E8),
                                                )),
                                          ),
                                          elevation: MaterialStateProperty.resolveWith<double>(
                                            (Set<MaterialState> states) {
                                              if (states.contains(MaterialState.disabled)) {
                                                return 0;
                                              }
                                              return 0; // Defer to the widget's default.
                                            },
                                          ),
                                        ),
                                        onPressed: () async {
                                          if (Platform.isAndroid) {
                                            var permission = Platform.isAndroid ? Permission.storage : Permission.photos;
                                            var status_storage = await Permission.storage.status;

                                            var status = await permission.request();
                                            if (status == PermissionStatus.granted) {
                                              final result = await Permission.camera.request();
                                              if (result == PermissionStatus.granted) {
                                                FocusScope.of(context).requestFocus(new FocusNode());
                                                Future.delayed(Duration(milliseconds: 200), () async {
                                                  setState(() async {
                                                    try {
                                                      String? qrResult = await MajaScan.startScan(title: "QRcode scanner", titleColor: Colors.amberAccent[700], qRCornerColor: Colors.orange, qRScannerColor: Colors.orange);
                                                      setState(() {
                                                        _walletAddress.text = qrResult ?? 'null string';
                                                      });
                                                    } on PlatformException catch (ex) {
                                                      if (ex.code == MajaScan.CameraAccessDenied) {
                                                        setState(() {
                                                          _walletAddress.text = "Camera permission was denied";
                                                        });
                                                      } else {
                                                        setState(() {
                                                          _walletAddress.text = "Unknown Error $ex";
                                                        });
                                                      }
                                                    } on FormatException {
                                                      setState(() {
                                                        _walletAddress.text = "You pressed the back button before scanning anything";
                                                      });
                                                    } catch (ex) {
                                                      setState(() {
                                                        _walletAddress.text = "Unknown Error $ex";
                                                      });
                                                    }
                                                  });
                                                });
                                              } else {
                                                showDialog(
                                                    context: context,
                                                    builder: (BuildContext context) => CupertinoAlertDialog(
                                                          title: Text('Camera Permission'),
                                                          content: Text('This app needs camera access to  scan QR code '),
                                                          actions: <Widget>[
                                                            CupertinoDialogAction(
                                                              child: Text('Deny'),
                                                              onPressed: () => Navigator.of(context).pop(),
                                                            ),
                                                            CupertinoDialogAction(
                                                              child: Text('Settings'),
                                                              onPressed: () => openAppSettings(),
                                                            ),
                                                          ],
                                                        ));
                                              }
                                            }
                                          } else {
                                            FocusScope.of(context).requestFocus(new FocusNode());
                                            Future.delayed(Duration(milliseconds: 200), () async {
                                              try {
                                                String? qrResult = await MajaScan.startScan(title: "QRcode scanner", titleColor: Colors.amberAccent[700], qRCornerColor: Colors.orange, qRScannerColor: Colors.orange);
                                                setState(() {
                                                  _walletAddress.text = qrResult ?? 'null string';
                                                });
                                              } on PlatformException catch (ex) {
                                                if (ex.code == MajaScan.CameraAccessDenied) {
                                                  setState(() {
                                                    _walletAddress.text = "Camera permission was denied";
                                                  });
                                                } else {
                                                  setState(() {
                                                    _walletAddress.text = "Unknown Error $ex";
                                                  });
                                                }
                                              } on FormatException {
                                                setState(() {
                                                  _walletAddress.text = "You pressed the back button before scanning anything";
                                                });
                                              } catch (ex) {
                                                setState(() {
                                                  _walletAddress.text = "Unknown Error $ex";
                                                });
                                              }
                                            });
                                          }
                                        },
                                        child: Image.asset(
                                          "images/bi_qr-code-scan.png",
                                          height: MediaQuery.of(context).size.height * 0.035,
                                        ),
                                      ),
                                    ),
                                    errorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red)),
                                    contentPadding: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width * 0.04, vertical: MediaQuery.of(context).size.height * 0.017),
                                    enabledBorder: OutlineInputBorder(
                                        borderRadius: new BorderRadius.only(
                                            topLeft: Radius.circular(MediaQuery.of(context).size.height * 0.05),
                                            bottomLeft: Radius.circular(MediaQuery.of(context).size.height * 0.05),
                                            topRight: Radius.circular(MediaQuery.of(context).size.height * 0.05),
                                            bottomRight: Radius.circular(MediaQuery.of(context).size.height * 0.05)),
                                        borderSide: BorderSide(
                                          color: Color(0xFFD6D6E8),
                                        )),
                                    focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Color(0xff4048FF))),
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: MediaQuery.of(context).size.height * .015,
                            ),
                            ElevatedButton(
                              child: Padding(
                                padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.04, right: MediaQuery.of(context).size.width * 0.04, top: MediaQuery.of(context).size.height * 0.02, bottom: MediaQuery.of(context).size.height * 0.02),
                                child: Text(
                                  "Confirm",
                                  style: GoogleFonts.poppins(fontWeight: FontWeight.w600, fontSize: MediaQuery.of(context).size.height * 0.024, color: Colors.white),
                                ),
                              ),
                              onPressed: () {
                                setState(() {
                                  removecontactData(index);
                                  addContactData(_nameaddbook.text, _walletAddress.text);
                                  load();
                                  Navigator.of(context).pop();
                                  _walletAddress.text = '';
                                  _nameaddbook.text = '';
                                });
                              },
                              style: ButtonStyle(
                                backgroundColor: MaterialStateProperty.all(
                                  const Color(0xFF4048FF),
                                ),
                                shape: MaterialStateProperty.all(
                                  RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.08),
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: MediaQuery.of(context).size.height * .015,
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ));
          });
        });
  }

  displayDeleteContactDialog(
    int index,
    String nameaddbook,
    String walletAddress,
  ) {
    showDialog(
        barrierDismissible: false,
        context: MyApp.materialKey.currentContext ?? OneContext() as BuildContext,
        builder: (context) {
          return StatefulBuilder(builder: (BuildContext context, setState) {
            return Dialog(
                backgroundColor: Colors.transparent,
                insetPadding: EdgeInsets.zero,
                child: Container(
                  height: MediaQuery.of(context).size.height * .4,
                  width: MediaQuery.of(context).size.height * .4,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(40.0),
                  ),
                  child: Stack(
                    children: [
                      SingleChildScrollView(
                        child: Column(
                          children: [
                            Align(
                              child: Padding(
                                padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.02, left: MediaQuery.of(context).size.width * 0.65),
                                child: IconButton(
                                    onPressed: () async {
                                      Navigator.of(context).pop();
                                    },
                                    icon: Icon(
                                      Icons.close,
                                      color: Color(0xFF75758F),
                                      size: MediaQuery.of(context).size.height * 0.04,
                                    )),
                              ),
                            ),
                            SizedBox(
                              height: MediaQuery.of(context).size.height * .015,
                            ),
                            Text(
                              "Are you sure you want to",
                              textAlign: TextAlign.center,
                              style: GoogleFonts.poppins(
                                color: Color(0xFF1F2337),
                                fontSize: 18,
                                fontWeight: FontWeight.w600,
                                fontStyle: FontStyle.normal,
                              ),
                            ),
                            Text(
                              "delete this contact",
                              textAlign: TextAlign.center,
                              style: GoogleFonts.poppins(
                                color: Color(0xFF1F2337),
                                fontSize: 18,
                                fontWeight: FontWeight.w600,
                                fontStyle: FontStyle.normal,
                              ),
                            ),
                            SizedBox(
                              height: 12,
                            ),
                            ElevatedButton(
                              child: Padding(
                                padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.06, right: MediaQuery.of(context).size.width * 0.06, top: MediaQuery.of(context).size.height * 0.02, bottom: MediaQuery.of(context).size.height * 0.02),
                                child: Text(
                                  "Delete",
                                  style: GoogleFonts.poppins(fontWeight: FontWeight.w700, fontSize: MediaQuery.of(context).size.height * 0.024, color: Colors.white),
                                ),
                              ),
                              onPressed: () async {
                                setState(() {
                                  removecontactData(index);
                                  load();
                                  Navigator.of(context).pop();
                                  _walletAddress1.text = '';
                                  _nameaddbook1.text = '';
                                });
                              },
                              style: ButtonStyle(
                                backgroundColor: MaterialStateProperty.all(
                                  const Color(0xFF4048FF),
                                ),
                                shape: MaterialStateProperty.all(
                                  RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.08),
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 15,
                            ),
                            Container(
                              decoration: BoxDecoration(
                                boxShadow: <BoxShadow>[
                                  BoxShadow(
                                    color: Colors.black.withOpacity(0.2),
                                    blurRadius: 100 / 5,
                                    offset: Offset(0, 100 / 5),
                                  ),
                                ],
                              ),
                              child: ElevatedButton(
                                child: Padding(
                                  padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.03, right: MediaQuery.of(context).size.width * 0.06, top: MediaQuery.of(context).size.height * 0.02, bottom: MediaQuery.of(context).size.height * 0.02),
                                  child: Text(
                                    "  Cancel",
                                    textAlign: TextAlign.center,
                                    style: GoogleFonts.poppins(
                                      fontWeight: FontWeight.w700,
                                      fontSize: MediaQuery.of(context).size.height * 0.024,
                                      color: const Color(0xFF4048FF),
                                    ),
                                  ),
                                ),
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                                style: ButtonStyle(
                                  backgroundColor: MaterialStateProperty.all(
                                    Colors.white,
                                  ),
                                  shape: MaterialStateProperty.all(
                                    RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.08),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 30,
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ));
          });
        });
  }

  void _showPopupMenu(
    context,
    Offset offset,
    int index,
    String name,
    String adress,
  ) async {
    double left = offset.dx;
    double top = offset.dy;
    PopupMenuButton(
      onSelected: (value) {},
      itemBuilder: (context) {
        return [];
      },
    );
    await showMenu(
      context: context,
      position: RelativeRect.fromLTRB(left, top, 0.0, 0.0),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20.0),
      ),
      items: [
        PopupMenuItem(
          value: 0,
          child: Padding(
            padding: const EdgeInsets.only(left: 20, right: 0),
            child: Text(
              "Edit",
              style: TextStyle(color: Colors.black),
            ),
          ),
          onTap: () => displayEditContactDialog(index, name, adress),
        ),
        PopupMenuItem(
          value: 1,
          child: Center(
            child: Text(
              "Delete",
              style: TextStyle(color: Colors.black),
            ),
          ),
          onTap: () => displayDeleteContactDialog(
            index,
            name,
            adress,
          ),
        ),
      ],
      elevation: 8.0,
    );
  }

  String formatAdress(String? address) {
    String result = address ?? "";
    if (result.length >= 8) {
      return result.substring(0, 4) + '...........................' + result.substring(result.length - 3, result.length);
    }
    return result;
  }

  Widget buildListitem(_AZItem item, int index, BuildContext context) {
    final tag = item.getSuspensionTag();
    final off = !item.isShowSuspension;
    return Column(
      children: <Widget>[
        Offstage(
          offstage: off,
          child: buildHead(tag),
        ),
        Row(
          children: [
            Expanded(
              child: Column(
                children: [
                  Align(
                    alignment: Alignment.topLeft,
                    child: Container(
                        margin: EdgeInsets.only(right: 16, left: 20),
                        child: Text(
                          item.title.name ?? "",
                          style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500, color: Color(0XFF212529)),
                        )),
                  ),
                  Align(
                    alignment: Alignment.topLeft,
                    child: Container(
                        margin: EdgeInsets.only(right: 16, left: 20),
                        child: Text(
                          formatAdress(item.title.address) ?? "",
                          style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500, color: Color(0XFF75758F)),
                        )),
                  ),
                ],
              ),
            ),
            Align(
              alignment: Alignment.topRight,
              child: InkWell(
                  onTapDown: (TapDownDetails details) {
                    _showPopupMenu(context, details.globalPosition, item.title.id ?? 0, item.title.name ?? "", item.title.address ?? "");
                  },
                  child: Container(width: MediaQuery.of(context).size.width * 0.09, height: MediaQuery.of(context).size.height * 0.03, color: Colors.transparent, child: Image.asset('images/clickb.png'))),
            ),
            SizedBox(
              width: 25,
            )
          ],
        ),
      ],
    );
  }

  Widget buildHead(String tag) => Container(
        height: 30,
        margin: EdgeInsets.only(right: 16),
        padding: EdgeInsets.only(left: 10),
        color: Color(0xffCDCFFF).withOpacity(0.15),
        alignment: Alignment.topLeft,
        child: Text('${tag.toUpperCase()}', style: GoogleFonts.poppins(fontWeight: FontWeight.normal, color: Color(0xff4048FF), fontSize: 20)),
      );
  Offset tapPosition = Offset.zero;
}
