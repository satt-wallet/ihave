import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:math' as math;
import 'dart:typed_data';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:flutter_svg/svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:ihave/controllers/LoginController.dart';
import 'package:ihave/controllers/walletController.dart';
import 'package:ihave/model/crypto.dart';
import 'package:ihave/service/apiService.dart';
import 'package:ihave/ui/WelcomeScreen.dart';
import 'package:ihave/ui/kyc.dart';
import 'package:ihave/ui/staticElements/headerNavigation.dart';
import 'package:ihave/util/utils.dart';
import 'package:ihave/widgets/alertDialogWidget.dart';
import 'package:ihave/widgets/customAppBar.dart';
import 'package:ihave/widgets/customBottomBar.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:webview_flutter/webview_flutter.dart';

class BuyPage extends StatefulWidget {
  const BuyPage({Key? key}) : super(key: key);

  @override
  _BuyPageState createState() => _BuyPageState();
}

class _BuyPageState extends State<BuyPage> {
  bool _userInputOnChanged = false;
  bool _clickedFiatContainer = false;
  String _selectedFiatElement = "USD";
  bool _clickedValueFiat = false;

  bool _clickedCryptoSecondBEP20Container = false;
  bool _selectedCryptoSecondBEP20AddedToken = false;
  String _selectedCryptoSecondBEP20PicUrl = "";
  String _selectedCryptoSecondBEP20Name = "";

  double requestAmount = 0;
  bool _isLoadingNetwork = false;
  String _selectedFiat = "USD";
  String _selectedCurrency = "USD \$";
  List<Crypto> _cryptoList = <Crypto>[];
  List<Crypto> _cryptoListBEP20 = <Crypto>[];
  List<Crypto> _cryptoListERC20 = <Crypto>[];
  List<Crypto> _secondCryptoListBEP20 = <Crypto>[];
  List<Crypto> _secondCryptoListERC20 = <Crypto>[];
  List<Crypto> _secondCryptoListFiatBEP20 = <Crypto>[];
  List<Crypto> _secondCryptoListFiatERC20 = <Crypto>[];
  List<String> networkList = ["ERC20", "BEP20", "BTC"];
  APIService apiService = LoginController.apiService;
  String _selectedCrypto = "";
  bool _isVisible = false;
  bool _isProfileVisible = false;
  bool _isWalletVisible = false;
  bool _isNotificationVisible = false;
  String fiatName = "";
  String fiatSymbol = "";
  final _formKey = GlobalKey<FormState>();
  TextEditingController _cryptoBalance = TextEditingController();
  TextEditingController _dollarBalance = TextEditingController();

  TextEditingController _crypto = TextEditingController();
  TextEditingController _cryptoBEP20 = TextEditingController();
  TextEditingController _cryptoERC20 = TextEditingController();

  FocusNode cryptoFocusNode = FocusNode();
  FocusNode dollarFocusNode = FocusNode();
  FocusNode _cryptoFocusNode = FocusNode();
  bool _cryptoField = false;
  bool _dollarField = false;
  bool _cryptoPattern = false;
  bool _dollarPattern = false;
  String cryptoBalance = "";
  String dollarBalance = "";
  bool _firstStepBuy = true;
  bool _secondStepBuy = false;
  String _errorMessage = "";
  String _tokenSymbol = "";
  bool _errorTransactionExist = false;
  String _userId = "";
  String _quoteId = "";
  String _walletId = "";
  String _baseAmount = "";
  String _currencySelected = "";
  String _paymentId = "";
  bool _formSimplex = false;
  String _simplexForm = "";
  bool _isLoadingBuy = false;
  bool _isLoadingSimplex = false;
  bool _dropDownOpened = false;
  bool _isSelected = false;

  bool _clickedContainerPaymentMethod = false;
  String _selectedPaymentMethod = "fiat";
  bool _clickedCryptoBep20List = false;
  bool _clickedCryptoErc20List = false;
  String _selectedCryptoBEP20 = "";
  String _selectedCryptoERC20 = "";
  String _secondSelectedCryptoBEP20 = "";
  String _secondSelectedCryptoERC20 = "";
  String _secondSelectedCryptoFiatBep20 = "";
  String _secondSelectedCryptoFiatErc20 = "";
  String cryptoName1 = "";
  bool _clickedSecondContainerPaymentMethod = false;
  String _selectedSecondPaymentMethod = "bep20";
  double approximationValueBep20 = 0;
  double approximationValueErc20 = 0;
  String simplexAmount = "";
  String simplexCurrency = "";

  bool buttonPayCreditCard = false;
  final Completer<WebViewController> _controller = Completer<WebViewController>();
  final List<String> fiatList = [
    'PHP ₱',
    'GEL ლ',
    'QAR ﷼',
    'MDL lei',
    'COP \$',
    'ARS \$',
    'NAD \$',
    'AZN AZN',
    'CRC ₡',
    'CLP \$',
    'UYU \$U',
    'CNY ¥',
    'XAF FCFA',
    'ANG ƒ',
    'CRC ₡',
    'UZS лв',
    'AED د.إ',
    'IDR Rp',
    'DOP RD\$',
    'KZT ₸',
    'VND ₫',
    'MXN \$',
    'RON lei',
    'MAD MAD',
    'BRL R\$',
    'BGN лв',
    'TWD NT\$',
    'SGD S\$',
    'NGN ₦',
    'MYR RM',
    'HKD \$',
    'UAH ₴',
    'INR ₹',
    'ILS ₪',
    'HUF Ft',
    'ZAR R',
    'TRY ₺',
    'SEK kr',
    'PLN zł',
    'NZD \$',
    'NOK kr',
    'DKK kr',
    'CZK Kč',
    'CHF CHF',
    'KRW ₩',
    'USD \$',
    'JPY ¥',
    'EUR €'
  ];
  ThemeData scrolltheme = ThemeData(
      scrollbarTheme: ScrollbarThemeData(
    isAlwaysShown: true,
    thickness: MaterialStateProperty.all(3),
    thumbColor: MaterialStateProperty.all(Color(0xFF75758F)),
    radius: const Radius.circular(30),
  ));
  var seen = Set<String>();
  List<String> uniqueFiatList = [];
  String? firebaseToken = " ";

  List<Crypto> _secondCryptoListFiatPOLYGON = [];
  bool _clickedCryptoPOLYGONFiatContainer = false;
  bool _selectedCryptoPOLYGONFiatAddedToken = false;
  String _selectedCryptoPOLYGONFiatPicUrl = "";
  String _selectedCryptoPOLYGONFiatName = "";
  String _selectedCryptoPOLYGONFiatSymbol = "";
  String _selectedCryptoPOLYGON = "";

  List<Crypto> _secondCryptoListFiatBTTC = [];
  bool _clickedCryptoBTTCFiatContainer = false;
  bool _selectedCryptoBTTCFiatAddedToken = false;
  String _selectedCryptoBTTCFiatPicUrl = "";
  String _selectedCryptoBTTCFiatName = "";
  String _selectedCryptoBTTCFiatSymbol = "";
  String _selectedCryptoBTTC = "";

  List<Crypto> _secondCryptoListFiatTRX = [];
  bool _clickedCryptoTRXFiatContainer = false;
  bool _selectedCryptoTRXFiatAddedToken = false;
  String _selectedCryptoTRXFiatPicUrl = "";
  String _selectedCryptoTRXFiatName = "";
  String _selectedCryptoTRXFiatSymbol = "";
  String _selectedCryptoTRX = "";

  bool _clickedCryptoBEP20FiatContainer = false;
  bool _selectedCryptoBEP20FiatAddedToken = false;
  String _selectedCryptoBEP20FiatPicUrl = "";
  String _selectedCryptoBEP20FiatName = "";
  String _selectedCryptoBEP20FiatSymbol = "";

  bool _clickedCryptoERC20FiatContainer = false;
  bool _selectedCryptoERC20FiatAddedToken = false;
  String _selectedCryptoERC20FiatPicUrl = "";
  String _selectedCryptoERC20FiatName = "";
  String _selectedCryptoERC20FiatSymbol = "";

  bool _clickedCryptoBEP20FirstContainer = false;
  bool _selectedCryptoBEP20FirstAddedToken = false;
  String _selectedCryptoBEP20FirstPicUrl = "";
  String _selectedCryptoBEP20FirstName = "";
  String _selectedCryptoBEP20FirstSymbol = "";

  bool _clickedCryptoBEP20SecondContainer = false;
  bool _selectedCryptoBEP20SecondAddedToken = false;
  String _selectedCryptoBEP20SecondPicUrl = "";
  String _selectedCryptoBEP20SecondName = "";
  String _selectedCryptoBEP20SecondSymbol = "";

  bool _clickedCryptoERC20FirstContainer = false;
  bool _selectedCryptoERC20FirstAddedToken = false;
  String _selectedCryptoERC20FirstPicUrl = "";
  String _selectedCryptoERC20FirstName = "";
  String _selectedCryptoERC20FirstSymbol = "";

  bool _clickedCryptoERC20SecondContainer = false;
  bool _selectedCryptoERC20SecondAddedToken = false;
  String _selectedCryptoERC20SecondPicUrl = "";
  String _selectedCryptoERC20SecondName = "";
  String _selectedCryptoERC20SecondSymbol = "";

  bool _clickedCryptoBTCSecondContainer = false;
  bool _selectedCryptoBTCSecondAddedToken = false;
  String _selectedCryptoBTCSecondPicUrl = "";
  String _selectedCryptoBTCSecondName = "";
  String _selectedCryptoBTCSecondSymbol = "";
  TextEditingController _WalletPassword = TextEditingController();
  bool _obscureText = true;
  bool _loading = false;

  Future<List<Crypto>> fetchCrypto() async {
    if (Provider.of<WalletController>(context, listen: false).cryptos.length < 1) {
      var cryptoList = await apiService.cryptos1(Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "", version);
      Provider.of<WalletController>(context, listen: false).cryptos = cryptoList;
    }
    return Provider.of<WalletController>(context, listen: false).cryptos;
  }

  void _launchSwapURL(url, String outputCurrency, String inputCurrency) async {
    var sawpurl = url + "?outputCurrency=$outputCurrency&inputCurrency=$inputCurrency&chain=mainnet";
    if (!await launch(sawpurl)) throw 'Could not launch $url';
  }

  createTronWalletUI(BuildContext context) {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return StatefulBuilder(builder: (context, setState) {
            return Scaffold(
              resizeToAvoidBottomInset: true,
              body: Dialog(
                backgroundColor: Colors.transparent,
                insetPadding: EdgeInsets.zero,
                child: SingleChildScrollView(
                  child: Container(
                    height: MediaQuery.of(context).size.height,
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      color: Colors.white.withOpacity(0.9),
                    ),
                    child: Column(
                      children: <Widget>[
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Spacer(),
                            Spacer(),
                            Padding(
                              padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.01, top: MediaQuery.of(context).size.width * 0.06),
                              child: IconButton(
                                alignment: Alignment.topLeft,
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                                splashColor: Colors.transparent,
                                highlightColor: Colors.transparent,
                                icon: Icon(
                                  Icons.close,
                                  size: MediaQuery.of(context).size.width * 0.05,
                                  color: Colors.black.withOpacity(0.5),
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: MediaQuery.of(context).size.height * 0.035,
                        ),
                        Image.asset(
                          "images/tron-wallet.png",
                        ),
                        Text(
                          "Create your Tron wallet",
                          style: GoogleFonts.poppins(fontWeight: FontWeight.w700, fontSize: MediaQuery.of(context).size.height * 0.025, color: Colors.black),
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.075, right: MediaQuery.of(context).size.width * 0.075, top: MediaQuery.of(context).size.height * 0.01),
                          child: Text(
                            "You are about to delete this token. You now have the possibility to create your Tron ​​wallet.You Need to just enter your transactional password to create it ",
                            textAlign: TextAlign.center,
                            style: GoogleFonts.poppins(fontWeight: FontWeight.w400, fontSize: MediaQuery.of(context).size.height * 0.0175, color: const Color(0XFF162746), letterSpacing: 1.1),
                          ),
                        ),
                        Align(
                          alignment: Alignment.topLeft,
                          child: Padding(
                            padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.1, top: MediaQuery.of(context).size.height * 0.035),
                            child: Text(
                              "TRANSACTION PASSWORD",
                              style: GoogleFonts.poppins(fontWeight: FontWeight.w600, color: const Color(0XFF75758F)),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: MediaQuery.of(context).size.height * 0.01,
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width * 0.8,
                          child: TextFormField(
                            style: TextStyle(fontSize: MediaQuery.of(context).size.width * 0.045),
                            textAlignVertical: TextAlignVertical.center,
                            textAlign: TextAlign.left,
                            controller: _WalletPassword,
                            keyboardType: TextInputType.visiblePassword,
                            obscureText: _obscureText,
                            onChanged: (value) {
                              TextSelection previousSelection = _WalletPassword.selection;
                              _WalletPassword.text = value;
                              _WalletPassword.selection = previousSelection;

                              TextSelection.fromPosition(TextPosition(offset: _WalletPassword.text.length));
                            },
                            validator: (value) {
                              if (value!.isEmpty) {
                                return 'Field required';
                              }
                              return null;
                            },
                            decoration: InputDecoration(
                                fillColor: Colors.white,
                                focusColor: Colors.white,
                                hoverColor: Colors.white,
                                filled: true,
                                isDense: true,
                                prefixIcon: Padding(
                                  padding: EdgeInsets.all(MediaQuery.of(context).size.width * 0.017),
                                  child: SvgPicture.asset(
                                    "images/keyIcon.svg",
                                    color: Colors.orange,
                                    height: MediaQuery.of(context).size.width * 0.06,
                                    width: MediaQuery.of(context).size.width * 0.06,
                                  ),
                                ),
                                suffixIcon: Padding(
                                  padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.025),
                                  child: IconButton(
                                      onPressed: () {
                                        setState(() {
                                          _obscureText = !_obscureText;
                                        });
                                      },
                                      icon: _obscureText
                                          ? SvgPicture.asset(
                                              "images/visibility-icon-off.svg",
                                              height: MediaQuery.of(context).size.height * 0.02,
                                            )
                                          : SvgPicture.asset(
                                              "images/visibility-icon-on.svg",
                                              height: MediaQuery.of(context).size.height * 0.02,
                                            )),
                                ),
                                contentPadding: EdgeInsets.symmetric(vertical: 5, horizontal: 5),
                                enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.white)),
                                focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.white)),
                                errorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red)),
                                focusedErrorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red))),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.02),
                          child: SizedBox(
                            width: MediaQuery.of(context).size.width * 0.6,
                            child: ElevatedButton(
                              child: Padding(
                                padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.015, bottom: MediaQuery.of(context).size.height * 0.015),
                                child: _loading
                                    ? CircularProgressIndicator(
                                        color: Colors.white,
                                      )
                                    : Text(
                                        "Create Wallet",
                                        style: GoogleFonts.poppins(fontWeight: FontWeight.w700, fontSize: MediaQuery.of(context).size.height * 0.017),
                                      ),
                              ),
                              onPressed: !_loading
                                  ? () async {
                                      setState(() {
                                        _loading = true;
                                      });
                                      var result = await LoginController.apiService.createTronWallet(Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "", _WalletPassword.text);
                                      switch (result) {
                                        case "success":
                                          Navigator.of(context).pop();

                                          Fluttertoast.showToast(msg: "Wallet tron created successfully", backgroundColor: Color.fromRGBO(0, 151, 117, 1), toastLength: Toast.LENGTH_LONG);
                                          break;
                                        case "exist":
                                          Navigator.of(context).pop();

                                          Fluttertoast.showToast(msg: "You have a tron wallet", backgroundColor: Color(0xFFF52079), toastLength: Toast.LENGTH_LONG);
                                          break;
                                        case "wrong password":
                                          setState(() {
                                            _WalletPassword.text = "";
                                            _loading = false;
                                          });
                                          Fluttertoast.showToast(msg: "Wrong password", backgroundColor: Color(0xFFF52079), toastLength: Toast.LENGTH_LONG);
                                          break;
                                        case "error":
                                          Navigator.of(context).pop();

                                          Fluttertoast.showToast(msg: "Something went wrong please try again", backgroundColor: Color(0xFFF52079), toastLength: Toast.LENGTH_LONG);
                                          break;
                                      }
                                    }
                                  : null,
                              style: ButtonStyle(
                                backgroundColor: MaterialStateProperty.all(
                                  Color(0xFF4048FF),
                                ),
                                shape: MaterialStateProperty.all(
                                  RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.08),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.012),
                          child: SizedBox(
                            width: MediaQuery.of(context).size.width * 0.6,
                            child: ElevatedButton(
                              child: Padding(
                                padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.015, bottom: MediaQuery.of(context).size.height * 0.015),
                                child: Text(
                                  "Skip for now",
                                  style: GoogleFonts.poppins(fontWeight: FontWeight.w700, fontSize: MediaQuery.of(context).size.height * 0.017, color: const Color(0xFF4048FF)),
                                ),
                              ),
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                              style: ButtonStyle(
                                backgroundColor: MaterialStateProperty.all(
                                  Colors.white,
                                ),
                                shape: MaterialStateProperty.all(
                                  RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.08),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            );
          });
        });
  }

  _sortList() {
    fiatList.sort((a, b) => a.toLowerCase().compareTo(b.toLowerCase()));
  }

  @override
  void initState() {
    super.initState();
    setState(() {
      uniqueFiatList = fiatList.where((element) => seen.add(element)).toList();
      _sortList();
    });
    cryptoFocusNode.addListener(() {
      _cryptoBalance.text = formatCryptoBalance(double.parse(_cryptoBalance.text.replaceAll(new RegExp("[^\\d.]"), "")), "");
    });
    _cryptoFocusNode.addListener(() {
      _cryptoBalance.text = formatCryptoBalance(double.parse(_cryptoBalance.text.replaceAll(new RegExp("[^\\d.]"), "")), "");
    });
    dollarFocusNode.addListener(() {
      _dollarBalance.text = formatDollarBalance(double.parse(_dollarBalance.text.replaceAll(new RegExp("[^\\d.]"), "")));
    });
    setState(() {
      _cryptoBalance.text = "50";
    });

    fetchCrypto().then((value) {
      setState(() {
        _cryptoList.clear();
        _cryptoList.addAll(value.where((c) => c.symbol == "SATTBEP20" || c.symbol == "SATT" || c.symbol == "ETH" || c.symbol == "BTC" || c.symbol == "DAI" || c.symbol == "BNB" || c.symbol == "BUSD").toList());

        _cryptoListBEP20.clear();
        _cryptoListBEP20.addAll(value.where((e) => e.symbol == "BNB" || e.symbol == "BUSD" || e.symbol == "CAKE").toList());
        setFirstFieldBEP20(_cryptoListBEP20[0]);
        _secondCryptoListFiatPOLYGON.clear();
        _secondCryptoListFiatPOLYGON.addAll(value.where((e) => e.network == "POLYGON").toList());

        setCryptoInfoPoly(_secondCryptoListFiatPOLYGON[0]);
        _selectedCryptoPOLYGON = _secondCryptoListFiatPOLYGON[0].symbol.toString();

        _secondCryptoListFiatBTTC.clear();
        _secondCryptoListFiatBTTC.addAll(value.where((e) => e.network == "BTTC").toList());
        setCryptoInfoBTTC(_secondCryptoListFiatBTTC[0]);
        _selectedCryptoBTTC = _secondCryptoListFiatBTTC[0].symbol.toString();

        _secondCryptoListFiatTRX.clear();
        _secondCryptoListFiatTRX.addAll(value.where((e) => e.network == "TRON").toList());
        setCryptoInfoTRX(_secondCryptoListFiatTRX[0]);
        _selectedCryptoTRX = _secondCryptoListFiatTRX[0].symbol.toString();

        _cryptoListERC20.clear();
        _cryptoListERC20.addAll(value.where((c) => c.symbol == "WSATT" || c.symbol == "ETH" || c.symbol == "DAI" || c.symbol == "OMG" || c.symbol == "ZRX" || c.symbol == "MKR" || c.symbol == "USDT").toList());
        setFirstFieldERC20(_cryptoListERC20[0]);
        _secondCryptoListBEP20.clear();
        _secondCryptoListBEP20.addAll(value.where((c) => c.symbol == "SATTBEP20" || c.symbol == "BNB" || c.symbol == "BUSD" || c.symbol == "CAKE").toList());

        _secondCryptoListERC20.clear();
        _secondCryptoListERC20.addAll(value.where((c) => c.symbol == "SATT" || c.symbol == "WSATT" || c.symbol == "ETH" || c.symbol == "DAI" || c.symbol == "OMG" || c.symbol == "ZRX" || c.symbol == "MKR" || c.symbol == "USDT").toList());
        _selectedCryptoERC20SecondAddedToken = _secondCryptoListERC20[0].addedToken ?? false;
        _selectedCryptoERC20SecondPicUrl = _secondCryptoListERC20[0].picUrl.toString();
        _selectedCryptoERC20SecondName = _secondCryptoListERC20[0].name.toString();
        _selectedCryptoERC20SecondSymbol = _secondCryptoListERC20[0].symbol.toString();

        _secondCryptoListFiatBEP20.clear();
        _secondCryptoListFiatBEP20.addAll(value.where((c) => c.symbol == "SATTBEP20" || c.symbol == "BUSD" || c.symbol == "BNB").toList());
        _selectedCryptoBEP20FiatAddedToken = _secondCryptoListFiatBEP20[0].addedToken ?? false;
        _selectedCryptoBEP20FiatPicUrl = _secondCryptoListFiatBEP20[0].picUrl.toString();
        _selectedCryptoBEP20FiatName = _secondCryptoListFiatBEP20[0].name.toString();
        _selectedCryptoBEP20FiatSymbol = _secondCryptoListFiatBEP20[0].symbol.toString();

        _secondCryptoListFiatERC20.clear();
        _secondCryptoListFiatERC20.addAll(value.where((c) => c.symbol == "SATT" || c.symbol == "ETH" || c.symbol == "DAI").toList());

        _selectedCrypto = _cryptoList[0].symbol.toString();
        _selectedCryptoBEP20 = _cryptoListBEP20[0].symbol.toString();
        _selectedCryptoERC20 = _cryptoListERC20[0].symbol.toString();
        _secondSelectedCryptoBEP20 = _secondCryptoListBEP20[0].symbol.toString();
        _secondSelectedCryptoERC20 = _secondCryptoListERC20[0].symbol.toString();
        _secondSelectedCryptoFiatBep20 = _secondCryptoListFiatBEP20[0].symbol.toString();
        _secondSelectedCryptoFiatErc20 = _secondCryptoListFiatERC20[0].symbol.toString();

        _selectedCryptoBEP20SecondAddedToken = _secondCryptoListBEP20[0].addedToken ?? false;
        _selectedCryptoBEP20SecondPicUrl = _secondCryptoListBEP20[0].picUrl.toString();
        _selectedCryptoBEP20SecondName = _secondCryptoListBEP20[0].name.toString();
        _selectedCryptoBEP20SecondSymbol = _secondCryptoListBEP20[0].symbol.toString();

        _selectedCryptoERC20FiatAddedToken = _secondCryptoListFiatERC20[0].addedToken ?? false;
        _selectedCryptoERC20FiatPicUrl = _secondCryptoListFiatERC20[0].picUrl.toString();
        _selectedCryptoERC20FiatName = _secondCryptoListFiatERC20[0].name.toString();
        _selectedCryptoERC20FiatSymbol = _secondCryptoListFiatERC20[0].symbol.toString();
        if (Provider.of<LoginController>(context, listen: false).tokenSymbol == "BTC") {
          setState(() {
            _selectedSecondPaymentMethod = "btc";
          });
        } else if (Provider.of<LoginController>(context, listen: false).tokenNetwork == "") {
          setState(() {
            _selectedPaymentMethod = "fiat";
            _selectedSecondPaymentMethod = "bep20";
          });
        } else if (Provider.of<LoginController>(context, listen: false).tokenNetwork == "POLYGON") {
          setState(() {
            _selectedPaymentMethod = "fiat";
            _selectedSecondPaymentMethod = "polygon";
          });
        } else if (Provider.of<LoginController>(context, listen: false).tokenNetwork == "BTTC") {
          setState(() {
            _selectedPaymentMethod = "fiat";
            _selectedSecondPaymentMethod = "bttc";
          });
        } else if (Provider.of<LoginController>(context, listen: false).tokenNetwork == "TRON") {
          setState(() {
            _selectedPaymentMethod = "fiat";
            _selectedSecondPaymentMethod = "tron";
          });
        } else {
          setState(() {
            _selectedPaymentMethod = "fiat";
            _selectedSecondPaymentMethod = Provider.of<LoginController>(context, listen: false).tokenNetwork == "ERC20" ? "erc20" : "bep20";
          });
          if (_selectedSecondPaymentMethod == "bep20") {
            if (Provider.of<LoginController>(context, listen: false).tokenSymbol == "BNB") {
              Crypto cryptoBEP20 = _secondCryptoListFiatBEP20.firstWhere((element) => element.symbol == "BNB");
              setState(() {
                setCryptoInfoBep20(cryptoBEP20);
              });
            } else if (Provider.of<LoginController>(context, listen: false).tokenSymbol == "BUSD") {
              Crypto cryptoBEP20 = _secondCryptoListFiatBEP20.firstWhere((element) => element.symbol == "BUSD");

              setState(() {
                setCryptoInfoBep20(cryptoBEP20);
              });
            } else if (Provider.of<LoginController>(context, listen: false).tokenSymbol == "SATTBEP20") {
              Crypto cryptoBEP20 = _secondCryptoListFiatBEP20.firstWhere((element) => element.symbol == "SATTBEP20");

              setState(() {
                setCryptoInfoBep20(cryptoBEP20);
              });
            }
          } else if (_selectedSecondPaymentMethod == "erc20") {
            if (Provider.of<LoginController>(context, listen: false).tokenSymbol == "ETH") {
              Crypto cryptoERC20 = _secondCryptoListFiatERC20.firstWhere((e) => e.symbol == "ETH");
              setState(() {
                setCryptoInfoErc20(cryptoERC20);
              });
            } else if (Provider.of<LoginController>(context, listen: false).tokenSymbol == "DAI") {
              Crypto cryptoERC20 = _secondCryptoListFiatERC20.firstWhere((e) => e.symbol == "DAI");
              setState(() {
                setCryptoInfoErc20(cryptoERC20);
              });
            } else if (Provider.of<LoginController>(context, listen: false).tokenSymbol == "SATT") {
              Crypto cryptoERC20 = _secondCryptoListFiatERC20.firstWhere((e) => e.symbol == "SATT");
              setState(() {
                setCryptoInfoErc20(cryptoERC20);
              });
            }
          }
        }
        calculatePriceBEP20();
        calculatePriceERC20();
        handleGetQuotes(handleSelectedCrypto());
        _isProfileVisible = !_isProfileVisible;
        _isWalletVisible = !_isWalletVisible;
        _isNotificationVisible = !_isNotificationVisible;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    setState(() {
      _isVisible = Provider.of<WalletController>(context, listen: true).isSideMenuVisible;
    });

    if (_cryptoList.isNotEmpty && _cryptoListBEP20.isNotEmpty && _cryptoListERC20.isNotEmpty && _secondCryptoListBEP20.isNotEmpty && _secondCryptoListERC20.isNotEmpty) {
      setState(() {
        _isVisible = Provider.of<WalletController>(context, listen: true).isSideMenuVisible;
        _isProfileVisible = Provider.of<WalletController>(context, listen: true).isProfileVisible;
        _isWalletVisible = Provider.of<WalletController>(context, listen: true).isWalletVisible;
        _isNotificationVisible = Provider.of<WalletController>(context, listen: true).isNotificationVisible;
      });
      return WillPopScope(
          onWillPop: () {
            if (MediaQuery.of(context).viewInsets.bottom.toString() == "0.0") {
              gotoPageGlobal(context, WelcomeScreen());
              return Future.value(false);
            } else {
              setState(() {
                _clickedValueFiat = false;
              });
              FocusScope.of(context).requestFocus(FocusNode());
              return Future.value(false);
            }
          },
          child: SingleChildScrollView(
            child: Container(
              child: GestureDetector(
                onTap: () {
                  FocusScope.of(context).requestFocus(new FocusNode());
                  setState(() {
                    _dropDownOpened = false;
                    setFieldSelection(_cryptoBalance.text);
                    _clickedValueFiat = false;
                    FocusScope.of(context).requestFocus(FocusNode());
                  });
                },
                child: _firstStepBuy
                    ? Stack(
                        children: <Widget>[
                          ConstrainedBox(
                            constraints: BoxConstraints(maxHeight: MediaQuery.of(context).viewInsets.bottom != 0 ? MediaQuery.of(context).size.height : MediaQuery.of(context).size.height * 0.85),
                            child: Column(
                              mainAxisSize: MainAxisSize.max,
                              children: <Widget>[
                                Container(
                                  height: MediaQuery.of(context).size.height * 0.14,
                                  child: Row(
                                    crossAxisAlignment: CrossAxisAlignment.end,
                                    children: <Widget>[
                                      Padding(
                                        padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.05, 0, 0, MediaQuery.of(context).size.height * 0.032),
                                        child: Text(
                                          "Buy",
                                          style: GoogleFonts.poppins(
                                            fontSize: MediaQuery.of(context).size.height * 0.035,
                                            fontWeight: FontWeight.w700,
                                            color: Colors.white,
                                          ),
                                        ),
                                      ),
                                      Spacer(),
                                      Padding(
                                        padding: EdgeInsets.fromLTRB(0, 0, MediaQuery.of(context).size.width * 0.037, MediaQuery.of(context).size.height * 0.021),
                                        child: InkWell(
                                          onTap: () {
                                            setState(() {
                                              Provider.of<WalletController>(context, listen: false).buyCryptoCurrency = false;
                                              Provider.of<WalletController>(context, listen: false).selectedIndex = 2;
                                            });
                                            gotoPageGlobal(context, WelcomeScreen());
                                          },
                                          child: Text(
                                            "< Back",
                                            style: GoogleFonts.poppins(color: Colors.white, letterSpacing: 1.2, fontWeight: FontWeight.w600, fontSize: MediaQuery.of(context).size.height * 0.022),
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                Expanded(
                                  child: Container(
                                    width: MediaQuery.of(context).size.width,
                                    decoration: BoxDecoration(
                                        color: Colors.white,
                                        borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(MediaQuery.of(context).size.width * 0.08),
                                          topRight: Radius.circular(MediaQuery.of(context).size.width * 0.08),
                                        )),
                                    child: Form(
                                      key: _formKey,
                                      child: Column(
                                        children: <Widget>[
                                          Stack(
                                            children: [
                                              Padding(
                                                padding: EdgeInsets.only(
                                                  top: MediaQuery.of(context).size.height * 0.02,
                                                ),
                                                child: Stack(
                                                  children: [
                                                    /** Container of user inputs for prices  */
                                                    Padding(
                                                      padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.085),
                                                      child: Align(
                                                        alignment: Alignment.topCenter,
                                                        child: TextFormField(
                                                          readOnly: _clickedValueFiat,
                                                          inputFormatters: <TextInputFormatter>[
                                                            DecimalTextInputFormatter(decimalRange: 8),
                                                            FilteringTextInputFormatter.allow(
                                                              new RegExp(r'^[0-9]+\.?\d{0,8}'),
                                                            ),
                                                            CustomMaxValueInputFormatter(maxInputValue: 999999999.99999999),
                                                          ],
                                                          textAlign: TextAlign.center,
                                                          style: GoogleFonts.poppins(
                                                            fontWeight: FontWeight.bold,
                                                            fontSize: MediaQuery.of(context).size.height * 0.045,
                                                            color: _errorTransactionExist ? Color(0XFFF52079) : (_clickedValueFiat ? Color(0xFF75758F) : Colors.black),
                                                          ),
                                                          controller: _cryptoBalance,
                                                          keyboardType: Platform.isIOS ? TextInputType.datetime : TextInputType.number,
                                                          decoration: InputDecoration(
                                                            border: InputBorder.none,
                                                            hintText: '0',
                                                            hintStyle: GoogleFonts.poppins(color: _clickedValueFiat ? Color(0xFF75758F) : Colors.black, fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.045),
                                                            errorStyle: GoogleFonts.poppins(
                                                              fontWeight: FontWeight.w700,
                                                              fontSize: 16,
                                                            ),
                                                            alignLabelWithHint: true,
                                                          ),
                                                          onTap: () {
                                                            setState(() {
                                                              setFieldSelection(_cryptoBalance.text);
                                                              _clickedValueFiat = false;
                                                            });
                                                          },
                                                          onChanged: (value) async {
                                                            if (value.length > 0) {
                                                              await onChangedInput(value);
                                                            } else {
                                                              setState(() {
                                                                approximationValueErc20 = 0;
                                                                approximationValueBep20 = 0;
                                                                _dollarBalance.text = "0";
                                                                simplexAmount = "0";
                                                              });
                                                            }
                                                          },
                                                        ),
                                                      ),
                                                    ),
                                                    Padding(
                                                      padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.17),
                                                      child: Center(
                                                          child: _errorTransactionExist
                                                              ? Text(_errorMessage, textAlign: TextAlign.center, style: GoogleFonts.poppins(color: Color(0XFFF52079), letterSpacing: 0.2, fontWeight: FontWeight.w500, fontSize: MediaQuery.of(context).size.height * 0.014))
                                                              : null),
                                                    ),
                                                    Padding(
                                                      padding: EdgeInsets.only(top: _errorTransactionExist ? MediaQuery.of(context).size.height * 0.23 : MediaQuery.of(context).size.height * 0.17),
                                                      child: Center(
                                                        child: _selectedPaymentMethod == "fiat"
                                                            ? (_selectedSecondPaymentMethod == "bep20"
                                                                ? Text(
                                                                    _secondSelectedCryptoFiatBep20 == "SATTBEP20"
                                                                        ? "1 SATT = ${formatCryptoBalance(_cryptoList.firstWhere((e) => e.symbol == _secondSelectedCryptoFiatBep20).price ?? 0, "")} USD"
                                                                        : "1 ${_secondSelectedCryptoFiatBep20} = ${formatDollarBalance(_cryptoList.firstWhere((e) => e.symbol == _secondSelectedCryptoFiatBep20).price ?? 0)} USD",
                                                                    style: GoogleFonts.poppins(color: Color(0XFFADADC8), fontSize: MediaQuery.of(context).size.height * 0.02, fontStyle: FontStyle.normal, fontWeight: FontWeight.w400, letterSpacing: 0.9),
                                                                  )
                                                                : (_selectedSecondPaymentMethod == "erc20"
                                                                    ? Text(
                                                                        "1 ${_secondSelectedCryptoFiatErc20} = ${formatCryptoBalance(_cryptoList.firstWhere((e) => e.symbol == _secondSelectedCryptoFiatErc20).price ?? 0, "")} USD",
                                                                        style: GoogleFonts.poppins(color: Color(0XFFADADC8), fontSize: MediaQuery.of(context).size.height * 0.02, fontStyle: FontStyle.normal, fontWeight: FontWeight.w400, letterSpacing: 0.9),
                                                                      )
                                                                    : (_selectedSecondPaymentMethod == "polygon"
                                                                        ? Text(
                                                                            "1 ${_selectedCryptoPOLYGONFiatSymbol} = ${formatCryptoBalance(_secondCryptoListFiatPOLYGON.firstWhere((e) => e.symbol == _selectedCryptoPOLYGONFiatSymbol).price ?? 0, "")} USD",
                                                                            style: GoogleFonts.poppins(color: Color(0XFFADADC8), fontSize: MediaQuery.of(context).size.height * 0.02, fontStyle: FontStyle.normal, fontWeight: FontWeight.w400, letterSpacing: 0.9),
                                                                          )
                                                                        : (_selectedSecondPaymentMethod == "bttc"
                                                                            ? Text(
                                                                                "1 ${_selectedCryptoBTTCFiatSymbol} = ${(_secondCryptoListFiatBTTC.firstWhere((e) => e.symbol == _selectedCryptoBTTCFiatSymbol).price ?? 0).toStringAsFixed(8)} USD",
                                                                                style: GoogleFonts.poppins(color: Color(0XFFADADC8), fontSize: MediaQuery.of(context).size.height * 0.02, fontStyle: FontStyle.normal, fontWeight: FontWeight.w400, letterSpacing: 0.9),
                                                                              )
                                                                            : (_selectedSecondPaymentMethod == "tron"
                                                                                ? Text(
                                                                                    "1 ${_selectedCryptoTRXFiatSymbol} = ${formatCryptoBalance(_secondCryptoListFiatTRX.firstWhere((e) => e.symbol == _selectedCryptoTRXFiatSymbol).price ?? 0, "")} USD",
                                                                                    style: GoogleFonts.poppins(color: Color(0XFFADADC8), fontSize: MediaQuery.of(context).size.height * 0.02, fontStyle: FontStyle.normal, fontWeight: FontWeight.w400, letterSpacing: 0.9),
                                                                                  )
                                                                                : Text(
                                                                                    "1 BTC = ${formatCryptoBalance(_cryptoList.firstWhere((e) => e.symbol == "BTC").price ?? 0, "")} USD",
                                                                                    style: GoogleFonts.poppins(color: Color(0XFFADADC8), fontSize: MediaQuery.of(context).size.height * 0.02, fontStyle: FontStyle.normal, fontWeight: FontWeight.w400, letterSpacing: 0.9),
                                                                                  ))))))
                                                            : (_selectedPaymentMethod == "bep20"
                                                                ? Text(
                                                                    "1 ${_selectedCryptoBEP20} = ${formatCryptoBalance((_cryptoListBEP20.firstWhere((e) => e.symbol == _selectedCryptoBEP20).price ?? 0) / (_secondCryptoListBEP20.firstWhere((e) => e.symbol == _secondSelectedCryptoBEP20).price ?? 0), "")} ${_secondSelectedCryptoBEP20 == "SATTBEP20" ? "SATT" : _secondSelectedCryptoBEP20}",
                                                                    style: GoogleFonts.poppins(color: Color(0XFFADADC8), fontSize: MediaQuery.of(context).size.height * 0.02, fontStyle: FontStyle.normal, fontWeight: FontWeight.w400, letterSpacing: 0.9),
                                                                  )
                                                                : Text(
                                                                    "1 ${_selectedCryptoERC20} = ${formatCryptoBalance((_cryptoListERC20.firstWhere((e) => e.symbol == _selectedCryptoERC20).price ?? 0) / (_secondCryptoListERC20.firstWhere((e) => e.symbol == _secondSelectedCryptoERC20).price ?? 0), "")} ${_secondSelectedCryptoERC20}",
                                                                    style: GoogleFonts.poppins(color: Color(0XFFADADC8), fontSize: MediaQuery.of(context).size.height * 0.02, fontStyle: FontStyle.normal, fontWeight: FontWeight.w400, letterSpacing: 0.9),
                                                                  )),
                                                      ),
                                                    ),
                                                    if (!_clickedValueFiat)
                                                      Padding(
                                                        padding: EdgeInsets.only(top: _errorTransactionExist ? MediaQuery.of(context).size.height * 0.295 : MediaQuery.of(context).size.height * 0.245),
                                                        child: Center(
                                                          child: _selectedPaymentMethod == "bep20"
                                                              ? InkWell(
                                                                  onTap: () {
                                                                    setState(() {
                                                                      _clickedValueFiat = true;
                                                                      _crypto.text = approximationValueBep20 > 0 ? formatCryptoBalance(approximationValueBep20, "") : "0";
                                                                      _dropDownOpened = false;
                                                                      setFieldSelection(_cryptoBalance.text);
                                                                    });
                                                                  },
                                                                  child: Text(
                                                                    "≈ ${approximationValueBep20 != 0 ? formatCryptoBalance(approximationValueBep20, "") : "0"} ${_secondSelectedCryptoBEP20 == "SATTBEP20" ? "SATT" : _secondSelectedCryptoBEP20}\n",
                                                                    style: GoogleFonts.poppins(
                                                                      color: Color(0xFF75758F),
                                                                      fontSize: MediaQuery.of(context).size.height * 0.0375,
                                                                      fontWeight: FontWeight.bold,
                                                                    ),
                                                                  ),
                                                                )
                                                              : (_selectedPaymentMethod == "erc20"
                                                                  ? InkWell(
                                                                      onTap: () {
                                                                        setState(() {
                                                                          _clickedValueFiat = true;
                                                                          _crypto.text = approximationValueErc20 > 0 ? formatCryptoBalance(approximationValueErc20, "") : "0";
                                                                          _dropDownOpened = false;
                                                                          setFieldSelection(_cryptoBalance.text);
                                                                        });
                                                                      },
                                                                      child: Text(
                                                                        "≈ ${approximationValueErc20 > 0 ? formatCryptoBalance(approximationValueErc20, "") : "0"} ${_secondSelectedCryptoERC20}\n",
                                                                        style: GoogleFonts.poppins(
                                                                          color: Color(0xFF75758F),
                                                                          fontSize: MediaQuery.of(context).size.height * 0.0375,
                                                                          fontWeight: FontWeight.bold,
                                                                        ),
                                                                      ),
                                                                    )
                                                                  : InkWell(
                                                                      onTap: () {
                                                                        FocusScope.of(context).requestFocus(FocusNode());
                                                                        setState(() {
                                                                          _clickedValueFiat = true;
                                                                          _crypto.text = simplexAmount;
                                                                          _dropDownOpened = false;
                                                                          setFieldSelection(_cryptoBalance.text);
                                                                        });
                                                                      },
                                                                      child: Text(
                                                                        _errorTransactionExist
                                                                            ? "≈ 0 ${_selectedSecondPaymentMethod == "bep20" ? (_secondSelectedCryptoFiatBep20 == "SATTBEP20" ? "SATT" : _secondSelectedCryptoFiatBep20) : (_selectedSecondPaymentMethod == "erc20" ? _secondSelectedCryptoFiatErc20 : (_selectedSecondPaymentMethod == "polygon" ? _selectedCryptoPOLYGONFiatSymbol : (_selectedSecondPaymentMethod == "bttc" ? _selectedCryptoBTTCFiatSymbol : "BTC")))}"
                                                                            : "≈ ${simplexAmount != "" ? formatCryptoBalance(double.tryParse(simplexAmount) ?? 0.00, "") : "0"} ${simplexCurrency == "SATT-SC" || simplexCurrency == "SATT-ERC20" ? "SATT" : simplexCurrency}\n",
                                                                        style: GoogleFonts.poppins(
                                                                          color: Color(0xFF75758F),
                                                                          fontSize: MediaQuery.of(context).size.height * 0.0375,
                                                                          fontWeight: FontWeight.bold,
                                                                        ),
                                                                      ),
                                                                    )),
                                                        ),
                                                      ),
                                                    if (_clickedValueFiat)
                                                      Padding(
                                                        padding: EdgeInsets.only(top: _errorTransactionExist ? MediaQuery.of(context).size.height * 0.295 : MediaQuery.of(context).size.height * 0.245),
                                                        child: Center(
                                                          child: TextFormField(
                                                            onTap: () {},
                                                            autofocus: true,
                                                            inputFormatters: <TextInputFormatter>[
                                                              DecimalTextInputFormatter(decimalRange: 8),
                                                              FilteringTextInputFormatter.allow(
                                                                new RegExp(r'^[0-9]+\.?\d{0,8}'),
                                                              ),
                                                              CustomMaxValueInputFormatter(maxInputValue: 999999999.99999999),
                                                            ],
                                                            controller: _crypto,
                                                            focusNode: _cryptoFocusNode,
                                                            decoration: InputDecoration(
                                                              border: InputBorder.none,
                                                              hintText: '0',
                                                              counterText: '',
                                                              hintStyle: GoogleFonts.poppins(color: Colors.black, fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.045),
                                                              errorStyle: GoogleFonts.poppins(
                                                                fontWeight: FontWeight.w700,
                                                                fontSize: 16,
                                                              ),
                                                              alignLabelWithHint: true,
                                                            ),
                                                            style: GoogleFonts.poppins(fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.045),
                                                            textAlign: TextAlign.center,
                                                            keyboardType: Platform.isIOS ? TextInputType.datetime : TextInputType.number,
                                                            onChanged: (value) async {
                                                              value = value.replaceAll(new RegExp("[^\\d.]"), "");
                                                              try {
                                                                if (value.length == 0 || double.parse(value) <= 0) {
                                                                  _cryptoBalance.text = "";
                                                                } else {
                                                                  value = value.replaceAll(new RegExp("[^\\d.]"), "");
                                                                  _crypto.text = value;
                                                                  _crypto.selection = TextSelection.collapsed(offset: _crypto.text.length);
                                                                  if (_selectedPaymentMethod != "fiat") {
                                                                    switch (_selectedPaymentMethod) {
                                                                      case "bep20":
                                                                        if (value.length > 0) {
                                                                          calculatePriceBEP20();
                                                                        } else {
                                                                          setState(() {
                                                                            approximationValueBep20 = 0;
                                                                            approximationValueErc20 = 0;
                                                                          });
                                                                        }

                                                                        break;
                                                                      case "erc20":
                                                                        if (value.length > 0) {
                                                                          calculatePriceERC20();
                                                                        } else {
                                                                          setState(() {
                                                                            approximationValueErc20 = 0;
                                                                            approximationValueBep20 = 0;
                                                                          });
                                                                        }
                                                                        break;
                                                                    }
                                                                  } else {
                                                                    _cryptoBalance.text = setDollarBalance(
                                                                            _selectedSecondPaymentMethod == "bep20"
                                                                                ? _secondSelectedCryptoFiatBep20
                                                                                : (_selectedSecondPaymentMethod == "erc20"
                                                                                    ? _secondSelectedCryptoFiatErc20
                                                                                    : (_selectedSecondPaymentMethod == "polygon" ? _selectedCryptoPOLYGONFiatSymbol : (_selectedSecondPaymentMethod == "tron" ? _selectedCryptoTRXFiatSymbol : "BTC"))),
                                                                            value)
                                                                        .replaceAll(',', '')
                                                                        .replaceAll('\$', '');
                                                                    await handleGetQuotes(handleSelectedCrypto());
                                                                  }
                                                                }
                                                              } catch (e) {
                                                                setState(() {});
                                                              }
                                                            },
                                                          ),
                                                        ),
                                                      ),

                                                    /** Container PAYMENT Method   */
                                                    Padding(
                                                      padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.0025, left: MediaQuery.of(context).size.width * 0.035),
                                                      child: Align(
                                                        alignment: Alignment.bottomLeft,
                                                        child: InkWell(
                                                          onTap: () {
                                                            setState(() {
                                                              _clickedContainerPaymentMethod = !_clickedContainerPaymentMethod;
                                                              _clickedValueFiat = false;
                                                              FocusScope.of(context).requestFocus(FocusNode());
                                                            });
                                                          },
                                                          child: Container(
                                                            width: MediaQuery.of(context).size.width * 0.45,
                                                            height: _clickedContainerPaymentMethod ? MediaQuery.of(context).size.height * 0.15 : MediaQuery.of(context).size.height * 0.065,
                                                            decoration: BoxDecoration(
                                                              border: Border.all(color: Color(0xFFD6D6E8)),
                                                              borderRadius: BorderRadius.circular(30),
                                                              color: Colors.white,
                                                            ),
                                                            child: Column(
                                                              children: <Widget>[
                                                                Padding(
                                                                  padding: EdgeInsets.only(top: MediaQuery.of(context).size.height < 1000 ? MediaQuery.of(context).size.height * 0.01 : MediaQuery.of(context).size.height * 0.005),
                                                                  child: _selectedPaymentMethod == "fiat"
                                                                      ? Row(
                                                                          children: <Widget>[
                                                                            Padding(
                                                                              padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025, bottom: MediaQuery.of(context).size.height * 0.002),
                                                                              child: SvgPicture.asset(
                                                                                "images/bank-icon-black.svg",
                                                                                height: MediaQuery.of(context).size.height * 0.028,
                                                                                width: MediaQuery.of(context).size.width * 0.028,
                                                                              ),
                                                                            ),
                                                                            Padding(
                                                                              padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.005, right: MediaQuery.of(context).size.width * 0.035),
                                                                              child: Text(
                                                                                "FIAT",
                                                                                style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.018),
                                                                              ),
                                                                            ),
                                                                            Spacer(),
                                                                            Padding(
                                                                              padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.05, top: MediaQuery.of(context).size.height * 0.007),
                                                                              child: SizedBox(
                                                                                width: 10,
                                                                                child: Transform.rotate(
                                                                                    angle: -math.pi / 2,
                                                                                    child: Icon(
                                                                                      Icons.arrow_back_ios,
                                                                                      color: Colors.grey,
                                                                                      size: MediaQuery.of(context).size.width * 0.06,
                                                                                    )),
                                                                              ),
                                                                            ),
                                                                          ],
                                                                        )
                                                                      : (_selectedPaymentMethod == "erc20"
                                                                          ? Row(
                                                                              children: <Widget>[
                                                                                Padding(
                                                                                  padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025, top: MediaQuery.of(context).size.height * 0.005),
                                                                                  child: SvgPicture.asset(
                                                                                    "images/network-erc.svg",
                                                                                    height: MediaQuery.of(context).size.height * 0.028,
                                                                                    width: MediaQuery.of(context).size.width * 0.028,
                                                                                  ),
                                                                                ),
                                                                                Padding(
                                                                                  padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.005, right: MediaQuery.of(context).size.width * 0.035),
                                                                                  child: Text(
                                                                                    "ERC20",
                                                                                    style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.018),
                                                                                  ),
                                                                                ),
                                                                                Spacer(),
                                                                                Padding(
                                                                                  padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.05, top: MediaQuery.of(context).size.height * 0.007),
                                                                                  child: SizedBox(
                                                                                    width: 10,
                                                                                    child: Transform.rotate(
                                                                                        angle: -math.pi / 2,
                                                                                        child: Icon(
                                                                                          Icons.arrow_back_ios,
                                                                                          color: Colors.grey,
                                                                                          size: MediaQuery.of(context).size.width * 0.06,
                                                                                        )),
                                                                                  ),
                                                                                ),
                                                                              ],
                                                                            )
                                                                          : Row(
                                                                              children: <Widget>[
                                                                                Padding(
                                                                                  padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025, top: MediaQuery.of(context).size.height * 0.005),
                                                                                  child: SvgPicture.asset(
                                                                                    "images/network-binance.svg",
                                                                                    height: MediaQuery.of(context).size.height * 0.028,
                                                                                    width: MediaQuery.of(context).size.width * 0.028,
                                                                                  ),
                                                                                ),
                                                                                Padding(
                                                                                  padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.005, right: MediaQuery.of(context).size.width * 0.035),
                                                                                  child: Text(
                                                                                    "BEP20",
                                                                                    style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.018),
                                                                                  ),
                                                                                ),
                                                                                Spacer(),
                                                                                Padding(
                                                                                  padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.05, top: MediaQuery.of(context).size.height * 0.007),
                                                                                  child: SizedBox(
                                                                                    width: 10,
                                                                                    child: Transform.rotate(
                                                                                        angle: -math.pi / 2,
                                                                                        child: Icon(
                                                                                          Icons.arrow_back_ios,
                                                                                          color: Colors.grey,
                                                                                          size: MediaQuery.of(context).size.width * 0.06,
                                                                                        )),
                                                                                  ),
                                                                                ),
                                                                              ],
                                                                            )),
                                                                ),
                                                                Visibility(
                                                                    visible: _clickedContainerPaymentMethod,
                                                                    child: _selectedPaymentMethod == "fiat"
                                                                        ? Padding(
                                                                            padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.012),
                                                                            child: Column(
                                                                              children: <Widget>[
                                                                                Padding(
                                                                                  padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.0085),
                                                                                  child: InkWell(
                                                                                    onTap: () {
                                                                                      setState(() {
                                                                                        _selectedPaymentMethod = "bep20";
                                                                                        _selectedSecondPaymentMethod = "bep20";
                                                                                        _errorMessage = "";
                                                                                        _errorTransactionExist = false;
                                                                                        buttonPayCreditCard = false;
                                                                                        _selectedCrypto = _cryptoList[0].symbol.toString();
                                                                                        _selectedCryptoERC20 = _cryptoListERC20[0].symbol.toString();
                                                                                        _secondSelectedCryptoERC20 = _secondCryptoListERC20[0].symbol.toString();
                                                                                        _secondSelectedCryptoFiatErc20 = _secondCryptoListFiatERC20[0].symbol.toString();
                                                                                        _secondSelectedCryptoFiatBep20 = _secondCryptoListFiatBEP20[0].symbol.toString();
                                                                                        _selectedCryptoBEP20SecondAddedToken = _secondCryptoListBEP20[0].addedToken ?? false;
                                                                                        _selectedCryptoBEP20SecondPicUrl = _secondCryptoListBEP20[0].picUrl.toString();
                                                                                        _selectedCryptoBEP20SecondName = _secondCryptoListBEP20[0].name.toString();
                                                                                        _selectedCryptoBEP20SecondSymbol = _secondCryptoListBEP20[0].symbol.toString();
                                                                                        _secondSelectedCryptoBEP20 = _secondCryptoListBEP20[0].symbol.toString();
                                                                                        _selectedCryptoBEP20 = _cryptoListBEP20[0].symbol.toString();
                                                                                        setFieldSelection("50");
                                                                                        calculatePriceBEP20();
                                                                                        calculatePriceERC20();
                                                                                      });
                                                                                    },
                                                                                    child: Row(
                                                                                      children: <Widget>[
                                                                                        Padding(
                                                                                          padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025),
                                                                                          child: SvgPicture.asset(
                                                                                            "images/network-binance.svg",
                                                                                            height: MediaQuery.of(context).size.height * 0.028,
                                                                                            width: MediaQuery.of(context).size.width * 0.028,
                                                                                          ),
                                                                                        ),
                                                                                        Text(
                                                                                          "BEP20",
                                                                                          style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.w600, fontSize: MediaQuery.of(context).size.height * 0.017),
                                                                                        ),
                                                                                      ],
                                                                                    ),
                                                                                  ),
                                                                                ),
                                                                                InkWell(
                                                                                  onTap: () {
                                                                                    setState(() {
                                                                                      setNetworkValues("erc20", "erc20");
                                                                                      setFieldSelection("50");
                                                                                      calculatePriceBEP20();
                                                                                      calculatePriceERC20();
                                                                                    });
                                                                                  },
                                                                                  child: Row(
                                                                                    children: <Widget>[
                                                                                      Padding(
                                                                                        padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025),
                                                                                        child: SvgPicture.asset(
                                                                                          "images/network-erc.svg",
                                                                                          height: MediaQuery.of(context).size.height * 0.028,
                                                                                          width: MediaQuery.of(context).size.width * 0.028,
                                                                                        ),
                                                                                      ),
                                                                                      Text(
                                                                                        "ERC20",
                                                                                        style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.w600, fontSize: MediaQuery.of(context).size.height * 0.017),
                                                                                      ),
                                                                                    ],
                                                                                  ),
                                                                                ),
                                                                              ],
                                                                            ),
                                                                          )
                                                                        : (_selectedPaymentMethod == "bep20"
                                                                            ? Padding(
                                                                                padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.012),
                                                                                child: Column(
                                                                                  children: <Widget>[
                                                                                    Padding(
                                                                                      padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.0085),
                                                                                      child: InkWell(
                                                                                        onTap: () async {
                                                                                          setState(() {
                                                                                            setNetworkValues("fiat", "bep20");
                                                                                            setFieldSelection("50");
                                                                                            calculatePriceBEP20();
                                                                                            calculatePriceERC20();
                                                                                          });
                                                                                          await handleGetQuotes(handleSelectedCrypto());
                                                                                        },
                                                                                        child: Row(
                                                                                          children: <Widget>[
                                                                                            Padding(
                                                                                              padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025),
                                                                                              child: SvgPicture.asset(
                                                                                                "images/bank-icon-black.svg",
                                                                                                height: MediaQuery.of(context).size.height * 0.028,
                                                                                                width: MediaQuery.of(context).size.width * 0.028,
                                                                                              ),
                                                                                            ),
                                                                                            Text(
                                                                                              "FIAT",
                                                                                              style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.w600, fontSize: MediaQuery.of(context).size.height * 0.017),
                                                                                            ),
                                                                                          ],
                                                                                        ),
                                                                                      ),
                                                                                    ),
                                                                                    InkWell(
                                                                                      onTap: () {
                                                                                        setState(() {
                                                                                          setNetworkValues("erc20", "erc20");
                                                                                          setFieldSelection("50");
                                                                                          calculatePriceBEP20();
                                                                                          calculatePriceERC20();
                                                                                        });
                                                                                      },
                                                                                      child: Row(
                                                                                        children: <Widget>[
                                                                                          Padding(
                                                                                            padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025),
                                                                                            child: SvgPicture.asset(
                                                                                              "images/network-erc.svg",
                                                                                              height: MediaQuery.of(context).size.height * 0.028,
                                                                                              width: MediaQuery.of(context).size.width * 0.028,
                                                                                            ),
                                                                                          ),
                                                                                          Text(
                                                                                            "ERC20",
                                                                                            style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.w600, fontSize: MediaQuery.of(context).size.height * 0.017),
                                                                                          ),
                                                                                        ],
                                                                                      ),
                                                                                    ),
                                                                                  ],
                                                                                ),
                                                                              )
                                                                            : Padding(
                                                                                padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.012),
                                                                                child: Column(
                                                                                  children: <Widget>[
                                                                                    Padding(
                                                                                      padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.0085),
                                                                                      child: InkWell(
                                                                                        onTap: () async {
                                                                                          setState(() {
                                                                                            setNetworkValues("fiat", "bep20");
                                                                                            setFieldSelection("50");
                                                                                            calculatePriceBEP20();
                                                                                            calculatePriceERC20();
                                                                                          });
                                                                                          await handleGetQuotes(handleSelectedCrypto());
                                                                                        },
                                                                                        child: Row(
                                                                                          children: <Widget>[
                                                                                            Padding(
                                                                                              padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025),
                                                                                              child: SvgPicture.asset(
                                                                                                "images/bank-icon-black.svg",
                                                                                                height: MediaQuery.of(context).size.height * 0.028,
                                                                                                width: MediaQuery.of(context).size.width * 0.028,
                                                                                              ),
                                                                                            ),
                                                                                            Text(
                                                                                              "FIAT",
                                                                                              style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.w600, fontSize: MediaQuery.of(context).size.height * 0.017),
                                                                                            ),
                                                                                          ],
                                                                                        ),
                                                                                      ),
                                                                                    ),
                                                                                    InkWell(
                                                                                      onTap: () {
                                                                                        setState(() {
                                                                                          setNetworkValues("bep20", "bep20");
                                                                                          setFieldSelection("50");
                                                                                          calculatePriceBEP20();
                                                                                          calculatePriceERC20();
                                                                                        });
                                                                                      },
                                                                                      child: Row(
                                                                                        children: <Widget>[
                                                                                          Padding(
                                                                                            padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025),
                                                                                            child: SvgPicture.asset(
                                                                                              "images/network-binance.svg",
                                                                                              height: MediaQuery.of(context).size.height * 0.028,
                                                                                              width: MediaQuery.of(context).size.width * 0.028,
                                                                                            ),
                                                                                          ),
                                                                                          Text(
                                                                                            "BEP20",
                                                                                            style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.w600, fontSize: MediaQuery.of(context).size.height * 0.017),
                                                                                          ),
                                                                                        ],
                                                                                      ),
                                                                                    ),
                                                                                  ],
                                                                                ),
                                                                              ))),
                                                              ],
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    ),

                                                    /** First Line of buy    FIAT / BEP20 / ERC20 -- CRYPTO LIST / FIAT LIST*/
                                                    Positioned(
                                                      left: MediaQuery.of(context).size.width * 0.4,
                                                      child: Padding(
                                                        padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.11, top: MediaQuery.of(context).size.height * 0.0025),
                                                        child: _selectedPaymentMethod == "fiat"
                                                            ? InkWell(
                                                                onTap: () {
                                                                  setState(() {
                                                                    _clickedFiatContainer = !_clickedFiatContainer;
                                                                    _clickedValueFiat = false;
                                                                    FocusScope.of(context).requestFocus(FocusNode());
                                                                  });
                                                                },
                                                                child: Container(
                                                                  height: _clickedFiatContainer ? MediaQuery.of(context).size.height * 0.25 : MediaQuery.of(context).size.height * 0.065,
                                                                  width: MediaQuery.of(context).size.width * 0.45,
                                                                  decoration: BoxDecoration(
                                                                    border: Border.all(color: Color(0xFFD6D6E8)),
                                                                    borderRadius: BorderRadius.circular(30),
                                                                    color: Colors.white,
                                                                  ),
                                                                  child: _clickedFiatContainer
                                                                      ? Padding(
                                                                          padding: EdgeInsets.only(
                                                                            bottom: MediaQuery.of(context).size.height * 0.015,
                                                                            right: MediaQuery.of(context).size.height * 0.015,
                                                                            top: MediaQuery.of(context).size.height * 0.015,
                                                                          ),
                                                                          child: Theme(
                                                                            data: scrolltheme,
                                                                            child: Scrollbar(
                                                                              isAlwaysShown: true,
                                                                              child: ListView.builder(
                                                                                scrollDirection: Axis.vertical,
                                                                                shrinkWrap: true,
                                                                                itemCount: fiatList.length,
                                                                                itemBuilder: (context, index) {
                                                                                  return Padding(
                                                                                    padding: EdgeInsets.only(
                                                                                      top: MediaQuery.of(context).size.height * 0.015,
                                                                                      left: MediaQuery.of(context).size.width * 0.015,
                                                                                    ),
                                                                                    child: InkWell(
                                                                                      onTap: () async {
                                                                                        setState(() {
                                                                                          _selectedCurrency = fiatList[index];
                                                                                          _clickedFiatContainer = false;
                                                                                          _selectedFiat = fiatList[index].toString().substring(0, fiatList[index].toString().indexOf(' '));
                                                                                          setFieldSelection("50");
                                                                                        });
                                                                                        await handleGetQuotes(handleSelectedCrypto());
                                                                                      },
                                                                                      child: Padding(
                                                                                        padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.015),
                                                                                        child: Row(
                                                                                          children: <Widget>[
                                                                                            Padding(
                                                                                              padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.02),
                                                                                              child: CircleAvatar(
                                                                                                backgroundColor: Colors.black,
                                                                                                child: CircleAvatar(
                                                                                                  backgroundColor: Colors.white,
                                                                                                  child: Text(
                                                                                                    fiatList[index].substring(fiatList[index].indexOf(' '), fiatList[index].length),
                                                                                                    style: GoogleFonts.poppins(color: Colors.black, fontSize: MediaQuery.of(context).size.height * 0.017, fontWeight: FontWeight.bold),
                                                                                                  ),
                                                                                                ),
                                                                                              ),
                                                                                            ),
                                                                                            Padding(
                                                                                              padding: EdgeInsets.only(),
                                                                                              child: Text(
                                                                                                fiatList[index].substring(0, fiatList[index].indexOf(' ')),
                                                                                                style: GoogleFonts.poppins(color: Color(0xFF1F2337), fontSize: MediaQuery.of(context).size.height * 0.02, fontWeight: FontWeight.w600),
                                                                                              ),
                                                                                            ),
                                                                                          ],
                                                                                        ),
                                                                                      ),
                                                                                    ),
                                                                                  );
                                                                                },
                                                                              ),
                                                                            ),
                                                                          ),
                                                                        )
                                                                      : Padding(
                                                                          padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.015),
                                                                          child: Row(
                                                                            children: <Widget>[
                                                                              Padding(
                                                                                padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.02),
                                                                                child: CircleAvatar(
                                                                                  backgroundColor: Colors.black,
                                                                                  child: CircleAvatar(
                                                                                    backgroundColor: Colors.white,
                                                                                    child: Text(
                                                                                      _selectedCurrency.substring(_selectedCurrency.indexOf(' '), _selectedCurrency.length),
                                                                                      style: GoogleFonts.poppins(color: Colors.black, fontSize: MediaQuery.of(context).size.height * 0.017, fontWeight: FontWeight.bold),
                                                                                    ),
                                                                                  ),
                                                                                ),
                                                                              ),
                                                                              Padding(
                                                                                padding: EdgeInsets.only(),
                                                                                child: Text(
                                                                                  _selectedCurrency.substring(0, _selectedCurrency.indexOf(' ')),
                                                                                  style: GoogleFonts.poppins(color: Colors.black, fontSize: MediaQuery.of(context).size.height * 0.02, fontWeight: FontWeight.w600),
                                                                                ),
                                                                              ),
                                                                            ],
                                                                          ),
                                                                        ),
                                                                ),
                                                              )
                                                            : (_selectedPaymentMethod == "bep20"
                                                                ? InkWell(
                                                                    onTap: () {
                                                                      setState(() {
                                                                        _clickedCryptoBEP20FirstContainer = !_clickedCryptoBEP20FirstContainer;
                                                                        _clickedValueFiat = false;
                                                                        FocusScope.of(context).requestFocus(FocusNode());
                                                                      });
                                                                    },
                                                                    child: Container(
                                                                      height: _clickedCryptoBEP20FirstContainer ? MediaQuery.of(context).size.height * 0.15 : MediaQuery.of(context).size.height * 0.065,
                                                                      width: MediaQuery.of(context).size.width * 0.45,
                                                                      decoration: BoxDecoration(
                                                                        border: Border.all(color: Color(0xFFD6D6E8)),
                                                                        borderRadius: BorderRadius.circular(30),
                                                                        color: Colors.white,
                                                                      ),
                                                                      child: _clickedCryptoBEP20FirstContainer
                                                                          ? Padding(
                                                                              padding: EdgeInsets.only(
                                                                                bottom: MediaQuery.of(context).size.height * 0.015,
                                                                                right: MediaQuery.of(context).size.height * 0.015,
                                                                                top: MediaQuery.of(context).size.height * 0.015,
                                                                              ),
                                                                              child: Theme(
                                                                                data: scrolltheme,
                                                                                child: Scrollbar(
                                                                                  isAlwaysShown: true,
                                                                                  child: ListView.builder(
                                                                                    scrollDirection: Axis.vertical,
                                                                                    shrinkWrap: true,
                                                                                    itemCount: _cryptoListBEP20.length,
                                                                                    itemBuilder: (context, index) {
                                                                                      return Padding(
                                                                                        padding: EdgeInsets.only(
                                                                                          top: MediaQuery.of(context).size.height * 0.015,
                                                                                          left: MediaQuery.of(context).size.width * 0.015,
                                                                                        ),
                                                                                        child: InkWell(
                                                                                          onTap: () {
                                                                                            setState(() {
                                                                                              setFirstFieldBEP20(_cryptoListBEP20[index]);
                                                                                              _selectedCryptoBEP20 = _cryptoListBEP20[index].symbol.toString();
                                                                                              setFieldSelection(_cryptoBalance.text);
                                                                                              if (_secondSelectedCryptoBEP20 == _selectedCryptoBEP20) {
                                                                                                switch (_selectedCryptoBEP20) {
                                                                                                  case "SATTBEP20":
                                                                                                    _secondSelectedCryptoBEP20 = "BNB";

                                                                                                    break;
                                                                                                  case "CAKE":
                                                                                                    _secondSelectedCryptoBEP20 = "SATTBEP20";
                                                                                                    fetchCrypto().then((value) {
                                                                                                      _cryptoListBEP20.clear();
                                                                                                      _cryptoListBEP20.addAll(value.where((e) => e.symbol == "BNB" || e.symbol == "BUSD" || e.symbol == "CAKE").toList());
                                                                                                    });

                                                                                                    _selectedCryptoBEP20SecondAddedToken = _secondCryptoListBEP20[0].addedToken ?? false;
                                                                                                    _selectedCryptoBEP20SecondPicUrl = _secondCryptoListBEP20[0].picUrl.toString();
                                                                                                    _selectedCryptoBEP20SecondName = _secondCryptoListBEP20[0].name.toString();
                                                                                                    _selectedCryptoBEP20SecondSymbol = _secondCryptoListBEP20[0].symbol.toString();

                                                                                                    break;
                                                                                                  case "BNB":
                                                                                                    _secondSelectedCryptoBEP20 = "SATTBEP20";
                                                                                                    Crypto crypto = _secondCryptoListBEP20.firstWhere((element) => element.symbol == "SATTBEP20");
                                                                                                    _selectedCryptoBEP20SecondAddedToken = crypto.addedToken ?? false;
                                                                                                    _selectedCryptoBEP20SecondPicUrl = crypto.picUrl.toString();
                                                                                                    _selectedCryptoBEP20SecondName = crypto.name.toString();
                                                                                                    _selectedCryptoBEP20SecondSymbol = crypto.symbol.toString();
                                                                                                    break;
                                                                                                  case "BUSD":
                                                                                                    _secondSelectedCryptoBEP20 = "SATTBEP20";
                                                                                                    break;
                                                                                                }
                                                                                              }
                                                                                              _cryptoBalance.text = "50";
                                                                                              calculatePriceBEP20();
                                                                                            });
                                                                                          },
                                                                                          child: Row(
                                                                                            children: [
                                                                                              _cryptoListBEP20[index].addedToken == true
                                                                                                  ? (_cryptoListBEP20[index].picUrl != "false" && _cryptoListBEP20[index].picUrl != "null"
                                                                                                      ? Padding(
                                                                                                          padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.018),
                                                                                                          child: Image.network(
                                                                                                            _cryptoListBEP20[index].picUrl ?? "",
                                                                                                            width: MediaQuery.of(context).size.width * 0.055,
                                                                                                            height: MediaQuery.of(context).size.width * 0.055,
                                                                                                          ),
                                                                                                        )
                                                                                                      : Padding(
                                                                                                          padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.018),
                                                                                                          child: SvgPicture.asset(
                                                                                                            'images/indispo.svg',
                                                                                                            width: MediaQuery.of(context).size.width * 0.055,
                                                                                                            height: MediaQuery.of(context).size.width * 0.055,
                                                                                                          ),
                                                                                                        ))
                                                                                                  : Padding(
                                                                                                      padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.018),
                                                                                                      child: SvgPicture.asset(
                                                                                                        'images/' + _cryptoListBEP20[index].name.toString().toLowerCase().replaceAll(' ', '') + '.svg',
                                                                                                        alignment: Alignment.center,
                                                                                                        width: MediaQuery.of(context).size.width * 0.055,
                                                                                                        height: MediaQuery.of(context).size.width * 0.055,
                                                                                                      ),
                                                                                                    ),
                                                                                              SizedBox(
                                                                                                width: MediaQuery.of(context).size.width * 0.02,
                                                                                              ),
                                                                                              new Text(
                                                                                                _cryptoListBEP20[index].symbol.toString().toUpperCase() == "SATTBEP20" ? "SATT" : _cryptoListBEP20[index].symbol.toString().toUpperCase(),
                                                                                                style: GoogleFonts.poppins(color: Colors.black, fontWeight: FontWeight.w600, fontSize: MediaQuery.of(context).size.height * 0.0165),
                                                                                              ),
                                                                                            ],
                                                                                          ),
                                                                                        ),
                                                                                      );
                                                                                    },
                                                                                  ),
                                                                                ),
                                                                              ),
                                                                            )
                                                                          : Padding(
                                                                              padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.015),
                                                                              child: Row(
                                                                                children: [
                                                                                  _selectedCryptoBEP20FirstAddedToken == true
                                                                                      ? (_selectedCryptoBEP20FirstPicUrl != "false" && _selectedCryptoBEP20FirstPicUrl != "null"
                                                                                          ? Padding(
                                                                                              padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.018),
                                                                                              child: Image.network(
                                                                                                _selectedCryptoBEP20FirstPicUrl,
                                                                                                width: MediaQuery.of(context).size.width * 0.055,
                                                                                                height: MediaQuery.of(context).size.width * 0.055,
                                                                                              ),
                                                                                            )
                                                                                          : Padding(
                                                                                              padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.018),
                                                                                              child: SvgPicture.asset(
                                                                                                'images/indispo.svg',
                                                                                                width: MediaQuery.of(context).size.width * 0.055,
                                                                                                height: MediaQuery.of(context).size.width * 0.055,
                                                                                              ),
                                                                                            ))
                                                                                      : Padding(
                                                                                          padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.018),
                                                                                          child: SvgPicture.asset(
                                                                                            'images/' + _selectedCryptoBEP20FirstName.toLowerCase().replaceAll(' ', '') + '.svg',
                                                                                            alignment: Alignment.center,
                                                                                            width: MediaQuery.of(context).size.width * 0.055,
                                                                                            height: MediaQuery.of(context).size.width * 0.055,
                                                                                          ),
                                                                                        ),
                                                                                  SizedBox(
                                                                                    width: MediaQuery.of(context).size.width * 0.02,
                                                                                  ),
                                                                                  new Text(
                                                                                    _selectedCryptoBEP20FirstSymbol == "SATTBEP20" ? "SATT" : _selectedCryptoBEP20FirstSymbol,
                                                                                    style: GoogleFonts.poppins(color: Colors.black, fontWeight: FontWeight.w600, fontSize: MediaQuery.of(context).size.height * 0.0165),
                                                                                  ),
                                                                                  const Spacer(),
                                                                                  Padding(
                                                                                    padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.05),
                                                                                    child: SizedBox(
                                                                                      width: 10,
                                                                                      child: Transform.rotate(
                                                                                          angle: -math.pi / 2,
                                                                                          child: Icon(
                                                                                            Icons.arrow_back_ios,
                                                                                            color: Colors.grey,
                                                                                            size: MediaQuery.of(context).size.width * 0.06,
                                                                                          )),
                                                                                    ),
                                                                                  ),
                                                                                ],
                                                                              ),
                                                                            ),
                                                                    ),
                                                                  )
                                                                : InkWell(
                                                                    onTap: () {
                                                                      setState(() {
                                                                        _clickedCryptoERC20FirstContainer = !_clickedCryptoERC20FirstContainer;
                                                                        _clickedValueFiat = false;
                                                                        FocusScope.of(context).requestFocus(FocusNode());
                                                                      });
                                                                    },
                                                                    child: Container(
                                                                      height: _clickedCryptoERC20FirstContainer ? MediaQuery.of(context).size.height * 0.15 : MediaQuery.of(context).size.height * 0.065,
                                                                      width: MediaQuery.of(context).size.width * 0.45,
                                                                      decoration: BoxDecoration(
                                                                        border: Border.all(color: Color(0xFFD6D6E8)),
                                                                        borderRadius: BorderRadius.circular(30),
                                                                        color: Colors.white,
                                                                      ),
                                                                      child: _clickedCryptoERC20FirstContainer
                                                                          ? Padding(
                                                                              padding: EdgeInsets.only(
                                                                                bottom: MediaQuery.of(context).size.height * 0.015,
                                                                                right: MediaQuery.of(context).size.height * 0.015,
                                                                                top: MediaQuery.of(context).size.height * 0.015,
                                                                              ),
                                                                              child: Theme(
                                                                                data: scrolltheme,
                                                                                child: Scrollbar(
                                                                                  isAlwaysShown: true,
                                                                                  child: ListView.builder(
                                                                                    scrollDirection: Axis.vertical,
                                                                                    shrinkWrap: true,
                                                                                    itemCount: _cryptoListERC20.length,
                                                                                    itemBuilder: (context, index) {
                                                                                      return Padding(
                                                                                        padding: EdgeInsets.only(
                                                                                          top: MediaQuery.of(context).size.height * 0.015,
                                                                                          left: MediaQuery.of(context).size.width * 0.015,
                                                                                        ),
                                                                                        child: InkWell(
                                                                                          onTap: () {
                                                                                            setState(() {
                                                                                              setFirstFieldERC20(_cryptoListERC20[index]);
                                                                                              _selectedCryptoERC20 = _cryptoListERC20[index].symbol.toString();
                                                                                              setFieldSelection(_cryptoBalance.text);
                                                                                              if (_secondSelectedCryptoERC20 == _selectedCryptoERC20) {
                                                                                                if (_selectedCryptoERC20 == "WSATT") {
                                                                                                  _secondSelectedCryptoERC20 = "ETH";

                                                                                                  Crypto crypto = _secondCryptoListERC20.firstWhere((element) => element.symbol == "ETH");
                                                                                                  setFirstFieldERC20(crypto);
                                                                                                } else {
                                                                                                  _secondSelectedCryptoERC20 = "WSATT";
                                                                                                  Crypto crypto = _secondCryptoListERC20.firstWhere((element) => element.symbol == "WSATT");
                                                                                                  setFirstFieldERC20(crypto);
                                                                                                }
                                                                                              }
                                                                                              _cryptoBalance.text = "50";
                                                                                              calculatePriceERC20();
                                                                                            });
                                                                                          },
                                                                                          child: Row(
                                                                                            children: [
                                                                                              _cryptoListERC20[index].addedToken == true
                                                                                                  ? (_cryptoListERC20[index].picUrl != "false" && _cryptoListERC20[index].picUrl != "null"
                                                                                                      ? Padding(
                                                                                                          padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.018),
                                                                                                          child: Image.network(
                                                                                                            _cryptoListERC20[index].picUrl ?? "",
                                                                                                            width: MediaQuery.of(context).size.width * 0.055,
                                                                                                            height: MediaQuery.of(context).size.width * 0.055,
                                                                                                          ),
                                                                                                        )
                                                                                                      : Padding(
                                                                                                          padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.018),
                                                                                                          child: SvgPicture.asset(
                                                                                                            'images/indispo.svg',
                                                                                                            width: MediaQuery.of(context).size.width * 0.055,
                                                                                                            height: MediaQuery.of(context).size.width * 0.055,
                                                                                                          ),
                                                                                                        ))
                                                                                                  : Padding(
                                                                                                      padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.018),
                                                                                                      child: SvgPicture.asset(
                                                                                                        'images/' + _cryptoListERC20[index].name.toString().toLowerCase().replaceAll(' ', '') + '.svg',
                                                                                                        alignment: Alignment.center,
                                                                                                        width: MediaQuery.of(context).size.width * 0.055,
                                                                                                        height: MediaQuery.of(context).size.width * 0.055,
                                                                                                      ),
                                                                                                    ),
                                                                                              SizedBox(
                                                                                                width: MediaQuery.of(context).size.width * 0.02,
                                                                                              ),
                                                                                              new Text(
                                                                                                _cryptoListERC20[index].symbol.toString().toUpperCase() == "SATTBEP20" ? "SATT" : _cryptoListERC20[index].symbol.toString().toUpperCase(),
                                                                                                style: GoogleFonts.poppins(color: Colors.black, fontWeight: FontWeight.w600, fontSize: MediaQuery.of(context).size.height * 0.0165),
                                                                                              ),
                                                                                            ],
                                                                                          ),
                                                                                        ),
                                                                                      );
                                                                                    },
                                                                                  ),
                                                                                ),
                                                                              ),
                                                                            )
                                                                          : Padding(
                                                                              padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.015),
                                                                              child: Row(
                                                                                children: [
                                                                                  _selectedCryptoERC20FirstAddedToken == true
                                                                                      ? (_selectedCryptoERC20FirstPicUrl != "false" && _selectedCryptoERC20FirstPicUrl != "null"
                                                                                          ? Padding(
                                                                                              padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.018),
                                                                                              child: Image.network(
                                                                                                _selectedCryptoERC20FirstPicUrl,
                                                                                                width: MediaQuery.of(context).size.width * 0.055,
                                                                                                height: MediaQuery.of(context).size.width * 0.055,
                                                                                              ),
                                                                                            )
                                                                                          : Padding(
                                                                                              padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.018),
                                                                                              child: SvgPicture.asset(
                                                                                                'images/indispo.svg',
                                                                                                width: MediaQuery.of(context).size.width * 0.055,
                                                                                                height: MediaQuery.of(context).size.width * 0.055,
                                                                                              ),
                                                                                            ))
                                                                                      : Padding(
                                                                                          padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.018),
                                                                                          child: SvgPicture.asset(
                                                                                            'images/' + _selectedCryptoERC20FirstName.toLowerCase().replaceAll(' ', '') + '.svg',
                                                                                            alignment: Alignment.center,
                                                                                            width: MediaQuery.of(context).size.width * 0.055,
                                                                                            height: MediaQuery.of(context).size.width * 0.055,
                                                                                          ),
                                                                                        ),
                                                                                  SizedBox(
                                                                                    width: MediaQuery.of(context).size.width * 0.02,
                                                                                  ),
                                                                                  new Text(
                                                                                    _selectedCryptoERC20FirstSymbol == "SATTBEP20" ? "SATT" : _selectedCryptoERC20FirstSymbol,
                                                                                    style: GoogleFonts.poppins(color: Colors.black, fontWeight: FontWeight.w600, fontSize: MediaQuery.of(context).size.height * 0.0165),
                                                                                  ),
                                                                                  const Spacer(),
                                                                                  Padding(
                                                                                    padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.05),
                                                                                    child: SizedBox(
                                                                                      width: 10,
                                                                                      child: Transform.rotate(
                                                                                          angle: -math.pi / 2,
                                                                                          child: Icon(
                                                                                            Icons.arrow_back_ios,
                                                                                            color: Colors.grey,
                                                                                            size: MediaQuery.of(context).size.width * 0.06,
                                                                                          )),
                                                                                    ),
                                                                                  ),
                                                                                ],
                                                                              ),
                                                                            ),
                                                                    ),
                                                                  )),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              /** ARROW FOR DROPDOWN BUTTON FIRST LINE BUY*/
                                              Positioned(
                                                  left: MediaQuery.of(context).size.width * 0.88,
                                                  top: MediaQuery.of(context).size.height > 1100 ? MediaQuery.of(context).size.height * 0.0185 : MediaQuery.of(context).size.height * 0.04,
                                                  child: _selectedPaymentMethod == "fiat"
                                                      ? (!_clickedFiatContainer
                                                          ? SizedBox(
                                                              width: 10,
                                                              child: Transform.rotate(
                                                                  angle: -math.pi / 2,
                                                                  child: Icon(
                                                                    Icons.arrow_back_ios,
                                                                    color: Colors.grey,
                                                                    size: MediaQuery.of(context).size.width * 0.06,
                                                                  )),
                                                            )
                                                          : SizedBox())
                                                      : SizedBox()),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),

                          /** PAYMENT METHOD : Pay with credit card / Panckaeswap if network BEP20 / Uniswap if network ERC20 */
                          Positioned(
                            top: _errorTransactionExist ? MediaQuery.of(context).size.height * 0.65 : MediaQuery.of(context).size.height * 0.59,
                            left: MediaQuery.of(context).size.width * 0.075,
                            child: _selectedPaymentMethod == "bep20"
                                ? SizedBox(
                                    height: MediaQuery.of(context).size.height * 0.058,
                                    width: MediaQuery.of(context).size.width * 0.83,
                                    child: ElevatedButton(
                                      style: ButtonStyle(
                                        backgroundColor: MaterialStateProperty.all(
                                          Colors.white,
                                        ),
                                        shape: MaterialStateProperty.all(
                                          RoundedRectangleBorder(
                                            borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.08),
                                          ),
                                        ),
                                      ),
                                      onPressed: () {
                                        var inputCrypto = _cryptoListBEP20.firstWhere((e) => e.symbol?.toLowerCase() == _selectedCryptoBEP20FirstSymbol.toLowerCase()).contract ?? "BNB";
                                        var outPutCrypto = _secondCryptoListBEP20.firstWhere((e) => e.symbol?.toLowerCase() == _secondSelectedCryptoBEP20.toLowerCase()).contract ?? "BNB";
                                        _launchSwapURL("https://pancakeswap.finance/swap", outPutCrypto.toString(), inputCrypto.toString());
                                      },
                                      child: Row(
                                        children: [
                                          SvgPicture.asset("images/pancakeswap-icon.svg"),
                                          Spacer(),
                                          Text(
                                            "Swap with pancakeswap",
                                            style: GoogleFonts.poppins(color: Color(0XFF4048FF), fontWeight: FontWeight.w600, fontSize: MediaQuery.of(context).size.height * 0.02, fontStyle: FontStyle.normal, letterSpacing: 0.7),
                                          ),
                                          Spacer(),
                                        ],
                                      ),
                                    ),
                                  )
                                : (_selectedPaymentMethod == "erc20"
                                    ? SizedBox(
                                        height: MediaQuery.of(context).size.height * 0.058,
                                        width: MediaQuery.of(context).size.width * 0.83,
                                        child: ElevatedButton(
                                          style: ButtonStyle(
                                            backgroundColor: MaterialStateProperty.all(
                                              Colors.white,
                                            ),
                                            shape: MaterialStateProperty.all(
                                              RoundedRectangleBorder(
                                                borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.08),
                                              ),
                                            ),
                                          ),
                                          onPressed: () {
                                            var inputCrypto = _cryptoListERC20.firstWhere((e) => e.symbol?.toLowerCase() == _selectedCryptoERC20FirstSymbol.toLowerCase()).contract ?? "ETH";
                                            var outPutCrypto = _secondCryptoListERC20.firstWhere((e) => e.symbol?.toLowerCase() == _secondSelectedCryptoERC20.toLowerCase()).contract ?? "ETH";
                                            _launchSwapURL("https://app.uniswap.org/#/swap", outPutCrypto, inputCrypto);
                                          },
                                          child: Row(
                                            children: [
                                              SvgPicture.asset(
                                                "images/Uniswap.svg",
                                                height: MediaQuery.of(context).size.height * 0.05,
                                                width: MediaQuery.of(context).size.width * 0.05,
                                                color: Color(0xFFFF007A),
                                              ),
                                              Spacer(),
                                              Text(
                                                "Swap with uniswap",
                                                style: GoogleFonts.poppins(color: Color(0XFF4048FF), fontWeight: FontWeight.w600, fontSize: MediaQuery.of(context).size.height * 0.02, fontStyle: FontStyle.normal, letterSpacing: 0.7),
                                              ),
                                              Spacer()
                                            ],
                                          ),
                                        ),
                                      )
                                    : SizedBox(
                                        height: MediaQuery.of(context).size.height * 0.058,
                                        width: MediaQuery.of(context).size.width * 0.83,
                                        child: ElevatedButton(
                                          style: ButtonStyle(
                                            backgroundColor: MaterialStateProperty.all(
                                              Colors.white,
                                            ),
                                            shape: MaterialStateProperty.all(
                                              RoundedRectangleBorder(
                                                borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.08),
                                              ),
                                            ),
                                          ),
                                          onPressed: () async {
                                            if (buttonPayCreditCard) {
                                              var result = await LoginController.apiService.getPaymentId(
                                                  Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "",
                                                  _selectedSecondPaymentMethod == "btc" ? LoginController.apiService.btcWalletAddress.toString() : (_selectedSecondPaymentMethod == "tron" ? LoginController.apiService.tronAddress.toString() : LoginController.apiService.walletAddress.toString()),
                                                  simplexCurrency,
                                                  _quoteId);
                                              if (result["message"] == "success") {
                                                _paymentId = result["data"]["payment_id"];
                                                setState(() {
                                                  _secondStepBuy = true;
                                                  _firstStepBuy = false;
                                                });
                                              } else {
                                                displayDialog(context, "Error", "Something went wrong please try again!");
                                                setState(() {
                                                  _secondStepBuy = false;
                                                  _firstStepBuy = true;
                                                });
                                              }
                                            }
                                          },
                                          child: Row(
                                            children: [
                                              SvgPicture.asset(
                                                "images/pay-creditcard-icon.svg",
                                                height: MediaQuery.of(context).size.height * 0.05,
                                                width: MediaQuery.of(context).size.width * 0.05,
                                              ),
                                              Padding(
                                                padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.075),
                                                child: Text(
                                                  "Pay by Credit Card",
                                                  style: GoogleFonts.poppins(color: Color(0XFF4048FF), fontWeight: FontWeight.w600, fontSize: MediaQuery.of(context).size.height * 0.02, fontStyle: FontStyle.normal, letterSpacing: 0.7),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      )),
                          ),

                          /** Second LINE of BUY : Container of network ( BEP20 / ERC20 ) */
                          Positioned(
                            top: _errorTransactionExist ? MediaQuery.of(context).size.height * 0.57 : MediaQuery.of(context).size.height * 0.51,
                            child: Padding(
                              padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.035),
                              child: Align(
                                alignment: Alignment.bottomLeft,
                                child: InkWell(
                                  onTap: () {
                                    setState(() {
                                      _clickedSecondContainerPaymentMethod = !_clickedSecondContainerPaymentMethod;
                                      _clickedValueFiat = false;
                                      FocusScope.of(context).requestFocus(FocusNode());
                                    });
                                  },
                                  child: Container(
                                    width: MediaQuery.of(context).size.width * 0.45,
                                    height: _clickedSecondContainerPaymentMethod ? (_selectedPaymentMethod == "fiat" ? MediaQuery.of(context).size.height * 0.3 : MediaQuery.of(context).size.height * 0.15) : MediaQuery.of(context).size.height * 0.065,
                                    decoration: BoxDecoration(color: Colors.white, border: Border.all(color: Color(0xFFD6D6E8)), borderRadius: BorderRadius.circular(30)),
                                    child: Column(
                                      children: <Widget>[
                                        Padding(
                                            padding: EdgeInsets.only(top: MediaQuery.of(context).size.height < 1000 ? MediaQuery.of(context).size.height * 0.015 : (_selectedSecondPaymentMethod == "btc" ? MediaQuery.of(context).size.height * 0.005 : MediaQuery.of(context).size.height * 0.0095)),
                                            child: _selectedSecondPaymentMethod == "erc20"
                                                ? Row(
                                                    children: <Widget>[
                                                      Padding(
                                                        padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025, bottom: MediaQuery.of(context).size.height * 0.002),
                                                        child: SvgPicture.asset(
                                                          "images/network-erc.svg",
                                                          height: MediaQuery.of(context).size.height * 0.028,
                                                          width: MediaQuery.of(context).size.width * 0.028,
                                                        ),
                                                      ),
                                                      Padding(
                                                        padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.035),
                                                        child: Text(
                                                          "ERC20",
                                                          style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.018),
                                                        ),
                                                      ),
                                                      Spacer(),
                                                      Padding(
                                                        padding: EdgeInsets.only(
                                                          right: MediaQuery.of(context).size.width * 0.05,
                                                        ),
                                                        child: SizedBox(
                                                          width: 10,
                                                          child: Transform.rotate(
                                                              angle: -math.pi / 2,
                                                              child: Icon(
                                                                Icons.arrow_back_ios,
                                                                color: Colors.grey,
                                                                size: MediaQuery.of(context).size.width * 0.06,
                                                              )),
                                                        ),
                                                      ),
                                                    ],
                                                  )
                                                : (_selectedSecondPaymentMethod == "btc"
                                                    ? Row(
                                                        children: <Widget>[
                                                          Padding(
                                                            padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.041, right: MediaQuery.of(context).size.width * 0.025),
                                                            child: SvgPicture.asset(
                                                              "images/btc-icon.svg",
                                                              height: MediaQuery.of(context).size.height * 0.025,
                                                              width: MediaQuery.of(context).size.width * 0.025,
                                                            ),
                                                          ),
                                                          Padding(
                                                            padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.035, left: MediaQuery.of(context).size.width * 0.0075),
                                                            child: Text(
                                                              "BTC",
                                                              style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.018),
                                                            ),
                                                          ),
                                                          Spacer(),
                                                          Padding(
                                                            padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.055, top: MediaQuery.of(context).size.height * 0.007),
                                                            child: SizedBox(
                                                              width: 10,
                                                              child: Transform.rotate(
                                                                  angle: -math.pi / 2,
                                                                  child: Icon(
                                                                    Icons.arrow_back_ios,
                                                                    color: Colors.grey,
                                                                    size: MediaQuery.of(context).size.width * 0.06,
                                                                  )),
                                                            ),
                                                          ),
                                                        ],
                                                      )
                                                    : (_selectedSecondPaymentMethod == "polygon"
                                                        ? Row(
                                                            children: <Widget>[
                                                              Padding(
                                                                padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025, bottom: MediaQuery.of(context).size.height * 0.0022),
                                                                child: SvgPicture.asset(
                                                                  "images/polygon-network.svg",
                                                                  height: MediaQuery.of(context).size.height * 0.028,
                                                                  width: MediaQuery.of(context).size.width * 0.028,
                                                                ),
                                                              ),
                                                              Padding(
                                                                padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.002, right: MediaQuery.of(context).size.width * 0.035),
                                                                child: Text(
                                                                  "POLYGON",
                                                                  style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.015),
                                                                ),
                                                              ),
                                                              Spacer(),
                                                              Padding(
                                                                padding: EdgeInsets.only(
                                                                  right: MediaQuery.of(context).size.width * 0.05,
                                                                ),
                                                                child: SizedBox(
                                                                  width: 10,
                                                                  child: Transform.rotate(
                                                                      angle: -math.pi / 2,
                                                                      child: Icon(
                                                                        Icons.arrow_back_ios,
                                                                        color: Colors.grey,
                                                                        size: MediaQuery.of(context).size.width * 0.06,
                                                                      )),
                                                                ),
                                                              ),
                                                            ],
                                                          )
                                                        : (_selectedSecondPaymentMethod == "bttc"
                                                            ? Row(
                                                                children: <Widget>[
                                                                  Padding(
                                                                    padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025, bottom: MediaQuery.of(context).size.height * 0.0022),
                                                                    child: SvgPicture.asset(
                                                                      "images/btt-network.svg",
                                                                      height: MediaQuery.of(context).size.height * 0.028,
                                                                      width: MediaQuery.of(context).size.width * 0.028,
                                                                    ),
                                                                  ),
                                                                  Padding(
                                                                    padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.002, right: MediaQuery.of(context).size.width * 0.035),
                                                                    child: Text(
                                                                      "BTT",
                                                                      style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.018),
                                                                    ),
                                                                  ),
                                                                  Spacer(),
                                                                  Padding(
                                                                    padding: EdgeInsets.only(
                                                                      right: MediaQuery.of(context).size.width * 0.05,
                                                                    ),
                                                                    child: SizedBox(
                                                                      width: 10,
                                                                      child: Transform.rotate(
                                                                          angle: -math.pi / 2,
                                                                          child: Icon(
                                                                            Icons.arrow_back_ios,
                                                                            color: Colors.grey,
                                                                            size: MediaQuery.of(context).size.width * 0.06,
                                                                          )),
                                                                    ),
                                                                  ),
                                                                ],
                                                              )
                                                            : (_selectedSecondPaymentMethod == "tron"
                                                                ? Row(
                                                                    children: <Widget>[
                                                                      Padding(
                                                                        padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025, bottom: MediaQuery.of(context).size.height * 0.0022),
                                                                        child: SvgPicture.asset(
                                                                          "images/wallet-tron.svg",
                                                                          height: MediaQuery.of(context).size.height * 0.028,
                                                                          width: MediaQuery.of(context).size.width * 0.028,
                                                                          color: Colors.black,
                                                                        ),
                                                                      ),
                                                                      Padding(
                                                                        padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.002, right: MediaQuery.of(context).size.width * 0.035),
                                                                        child: Text(
                                                                          "TRON",
                                                                          style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.018),
                                                                        ),
                                                                      ),
                                                                      Spacer(),
                                                                      Padding(
                                                                        padding: EdgeInsets.only(
                                                                          right: MediaQuery.of(context).size.width * 0.05,
                                                                        ),
                                                                        child: SizedBox(
                                                                          width: 10,
                                                                          child: Transform.rotate(
                                                                              angle: -math.pi / 2,
                                                                              child: Icon(
                                                                                Icons.arrow_back_ios,
                                                                                color: Colors.grey,
                                                                                size: MediaQuery.of(context).size.width * 0.06,
                                                                              )),
                                                                        ),
                                                                      ),
                                                                    ],
                                                                  )
                                                                : Row(
                                                                    children: <Widget>[
                                                                      Padding(
                                                                        padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025, bottom: MediaQuery.of(context).size.height * 0.0022),
                                                                        child: SvgPicture.asset(
                                                                          "images/network-binance.svg",
                                                                          height: MediaQuery.of(context).size.height * 0.028,
                                                                          width: MediaQuery.of(context).size.width * 0.028,
                                                                        ),
                                                                      ),
                                                                      Padding(
                                                                        padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.002, right: MediaQuery.of(context).size.width * 0.035),
                                                                        child: Text(
                                                                          "BEP20",
                                                                          style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.018),
                                                                        ),
                                                                      ),
                                                                      Spacer(),
                                                                      Padding(
                                                                        padding: EdgeInsets.only(
                                                                          right: MediaQuery.of(context).size.width * 0.05,
                                                                        ),
                                                                        child: SizedBox(
                                                                          width: 10,
                                                                          child: Transform.rotate(
                                                                              angle: -math.pi / 2,
                                                                              child: Icon(
                                                                                Icons.arrow_back_ios,
                                                                                color: Colors.grey,
                                                                                size: MediaQuery.of(context).size.width * 0.06,
                                                                              )),
                                                                        ),
                                                                      ),
                                                                    ],
                                                                  )))))),
                                        Visibility(
                                            visible: _clickedSecondContainerPaymentMethod,
                                            child: _selectedSecondPaymentMethod == "bep20"
                                                ? Padding(
                                                    padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.012),
                                                    child: Column(
                                                      children: <Widget>[
                                                        InkWell(
                                                          onTap: () async {
                                                            setState(() {
                                                              _selectedSecondPaymentMethod = "erc20";
                                                              _errorMessage = "";
                                                              _errorTransactionExist = false;
                                                              buttonPayCreditCard = false;
                                                              _selectedPaymentMethod = _selectedPaymentMethod == "fiat" ? "fiat" : "erc20";
                                                              setFieldSelection("50");
                                                              calculatePriceBEP20();
                                                              calculatePriceERC20();
                                                            });
                                                            if (_selectedPaymentMethod == "fiat") {
                                                              await handleGetQuotes(handleSelectedCrypto());
                                                            }
                                                          },
                                                          child: Row(
                                                            children: <Widget>[
                                                              Padding(
                                                                padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025),
                                                                child: SvgPicture.asset(
                                                                  "images/network-erc.svg",
                                                                  height: MediaQuery.of(context).size.height * 0.028,
                                                                  width: MediaQuery.of(context).size.width * 0.028,
                                                                ),
                                                              ),
                                                              Text(
                                                                "ERC20",
                                                                style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.w600, fontSize: MediaQuery.of(context).size.height * 0.017),
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                        if (_selectedPaymentMethod == "fiat")
                                                          InkWell(
                                                            onTap: () async {
                                                              setState(() {
                                                                _selectedSecondPaymentMethod = "btc";
                                                                _errorMessage = "";
                                                                _errorTransactionExist = false;
                                                                buttonPayCreditCard = false;
                                                                setFieldSelection("50");
                                                                calculatePriceBEP20();
                                                                calculatePriceERC20();
                                                              });
                                                              if (_selectedPaymentMethod == "fiat") {
                                                                await handleGetQuotes("BTC");
                                                              }
                                                            },
                                                            child: Padding(
                                                              padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.01),
                                                              child: Row(
                                                                children: <Widget>[
                                                                  Padding(
                                                                    padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.041, right: MediaQuery.of(context).size.width * 0.025),
                                                                    child: SvgPicture.asset(
                                                                      "images/btc-icon.svg",
                                                                      height: MediaQuery.of(context).size.height * 0.025,
                                                                      width: MediaQuery.of(context).size.width * 0.025,
                                                                    ),
                                                                  ),
                                                                  Padding(
                                                                    padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.035, left: MediaQuery.of(context).size.width * 0.0075),
                                                                    child: Text(
                                                                      "BTC",
                                                                      style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.018),
                                                                    ),
                                                                  ),
                                                                  Spacer(),
                                                                ],
                                                              ),
                                                            ),
                                                          ),

                                                        /**  ------    POLYGON     -------   */
                                                        if (_selectedPaymentMethod == "fiat")
                                                          InkWell(
                                                            onTap: () async {
                                                              setState(() {
                                                                _selectedSecondPaymentMethod = "polygon";
                                                                _errorMessage = "";
                                                                _errorTransactionExist = false;
                                                                buttonPayCreditCard = false;
                                                                setCryptoInfoPoly(_secondCryptoListFiatPOLYGON[0]);
                                                                setFieldSelection("50");
                                                              });
                                                              if (_selectedPaymentMethod == "fiat") {
                                                                await handleGetQuotes(_selectedCryptoPOLYGONFiatSymbol);
                                                              }
                                                            },
                                                            child: Padding(
                                                              padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.01),
                                                              child: Row(
                                                                children: <Widget>[
                                                                  Padding(
                                                                    padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.035, right: MediaQuery.of(context).size.width * 0.025),
                                                                    child: SvgPicture.asset(
                                                                      "images/polygon-network.svg",
                                                                      height: MediaQuery.of(context).size.height * 0.025,
                                                                      width: MediaQuery.of(context).size.width * 0.015,
                                                                    ),
                                                                  ),
                                                                  Padding(
                                                                    padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.035, left: MediaQuery.of(context).size.width * 0.0),
                                                                    child: Text(
                                                                      "POLYGON",
                                                                      style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.018),
                                                                    ),
                                                                  ),
                                                                  Spacer(),
                                                                ],
                                                              ),
                                                            ),
                                                          ),

                                                        /**       BTTC      */
                                                        if (_selectedPaymentMethod == "fiat")
                                                          InkWell(
                                                            onTap: () async {
                                                              setState(() {
                                                                _selectedSecondPaymentMethod = "bttc";
                                                                _errorMessage = "";
                                                                _errorTransactionExist = false;
                                                                buttonPayCreditCard = false;
                                                                setCryptoInfoBTTC(_secondCryptoListFiatBTTC[0]);
                                                                setFieldSelection("50");
                                                              });
                                                              if (_selectedPaymentMethod == "fiat") {
                                                                await handleGetQuotes(_selectedCryptoBTTCFiatSymbol);
                                                              }
                                                            },
                                                            child: Padding(
                                                              padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.01),
                                                              child: Row(
                                                                children: <Widget>[
                                                                  Padding(
                                                                    padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.035, right: MediaQuery.of(context).size.width * 0.025),
                                                                    child: SvgPicture.asset(
                                                                      "images/btt-network.svg",
                                                                      height: MediaQuery.of(context).size.height * 0.025,
                                                                      width: MediaQuery.of(context).size.width * 0.015,
                                                                    ),
                                                                  ),
                                                                  Padding(
                                                                    padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.035, left: MediaQuery.of(context).size.width * 0.005),
                                                                    child: Text(
                                                                      "BTT",
                                                                      style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.018),
                                                                    ),
                                                                  ),
                                                                  Spacer(),
                                                                ],
                                                              ),
                                                            ),
                                                          ),

                                                        /** TRON */
                                                        if (_selectedPaymentMethod == "fiat")
                                                          InkWell(
                                                            onTap: () async {
                                                              if (LoginController.apiService.tronAddress != null && LoginController.apiService.tronAddress != "") {
                                                                setState(() {
                                                                  _selectedSecondPaymentMethod = "tron";
                                                                  _errorMessage = "";
                                                                  _errorTransactionExist = false;
                                                                  buttonPayCreditCard = false;
                                                                  setCryptoInfoTRX(_secondCryptoListFiatTRX[0]);
                                                                  setFieldSelection("50");
                                                                });
                                                                if (_selectedPaymentMethod == "fiat") {
                                                                  await handleGetQuotes(_selectedCryptoTRXFiatSymbol);
                                                                }
                                                              } else {
                                                                createTronWalletUI(context);
                                                              }
                                                            },
                                                            child: Padding(
                                                              padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.01),
                                                              child: Row(
                                                                children: <Widget>[
                                                                  Padding(
                                                                    padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.035, right: MediaQuery.of(context).size.width * 0.025),
                                                                    child: SvgPicture.asset(
                                                                      "images/wallet-tron.svg",
                                                                      height: MediaQuery.of(context).size.height * 0.025,
                                                                      width: MediaQuery.of(context).size.width * 0.015,
                                                                      color: Colors.black,
                                                                    ),
                                                                  ),
                                                                  Padding(
                                                                    padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.035, left: MediaQuery.of(context).size.width * 0.0),
                                                                    child: Text(
                                                                      "TRON",
                                                                      style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.018),
                                                                    ),
                                                                  ),
                                                                  Spacer(),
                                                                ],
                                                              ),
                                                            ),
                                                          ),
                                                      ],
                                                    ),
                                                  )
                                                : (_selectedSecondPaymentMethod == "erc20"
                                                    ? Padding(
                                                        padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.012),
                                                        child: Column(
                                                          children: <Widget>[
                                                            Padding(
                                                              padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.0085),
                                                              child: InkWell(
                                                                onTap: () async {
                                                                  setState(() {
                                                                    _selectedSecondPaymentMethod = "bep20";
                                                                    _errorMessage = "";
                                                                    _errorTransactionExist = false;
                                                                    buttonPayCreditCard = false;
                                                                    _selectedPaymentMethod = _selectedPaymentMethod == "fiat" ? "fiat" : "bep20";
                                                                    setFieldSelection("50");
                                                                    calculatePriceBEP20();
                                                                    calculatePriceERC20();
                                                                  });
                                                                  if (_selectedPaymentMethod == "fiat") {
                                                                    await handleGetQuotes(handleSelectedCrypto());
                                                                  }
                                                                },
                                                                child: Row(
                                                                  children: <Widget>[
                                                                    Padding(
                                                                      padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025),
                                                                      child: SvgPicture.asset(
                                                                        "images/network-binance.svg",
                                                                        height: MediaQuery.of(context).size.height * 0.028,
                                                                        width: MediaQuery.of(context).size.width * 0.028,
                                                                      ),
                                                                    ),
                                                                    Text(
                                                                      "BEP20",
                                                                      style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.w600, fontSize: MediaQuery.of(context).size.height * 0.017),
                                                                    ),
                                                                  ],
                                                                ),
                                                              ),
                                                            ),
                                                            if (_selectedPaymentMethod == "fiat")
                                                              InkWell(
                                                                onTap: () async {
                                                                  setState(() {
                                                                    _selectedSecondPaymentMethod = "btc";
                                                                    _errorMessage = "";
                                                                    _errorTransactionExist = false;
                                                                    buttonPayCreditCard = false;
                                                                    setFieldSelection("50");
                                                                    calculatePriceBEP20();
                                                                    calculatePriceERC20();
                                                                  });
                                                                  if (_selectedPaymentMethod == "fiat") {
                                                                    await handleGetQuotes("BTC");
                                                                  }
                                                                },
                                                                child: Padding(
                                                                  padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.01),
                                                                  child: Row(
                                                                    children: <Widget>[
                                                                      Padding(
                                                                        padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.041, right: MediaQuery.of(context).size.width * 0.025),
                                                                        child: SvgPicture.asset(
                                                                          "images/btc-icon.svg",
                                                                          height: MediaQuery.of(context).size.height * 0.025,
                                                                          width: MediaQuery.of(context).size.width * 0.025,
                                                                        ),
                                                                      ),
                                                                      Padding(
                                                                        padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.035, left: MediaQuery.of(context).size.width * 0.0075),
                                                                        child: Text(
                                                                          "BTC",
                                                                          style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.018),
                                                                        ),
                                                                      ),
                                                                      Spacer(),
                                                                    ],
                                                                  ),
                                                                ),
                                                              ),

                                                            /**  ------    POLYGON     -------   */
                                                            if (_selectedPaymentMethod == "fiat")
                                                              InkWell(
                                                                onTap: () async {
                                                                  setState(() {
                                                                    _selectedSecondPaymentMethod = "polygon";
                                                                    _errorMessage = "";
                                                                    _errorTransactionExist = false;
                                                                    buttonPayCreditCard = false;
                                                                    setCryptoInfoPoly(_secondCryptoListFiatPOLYGON[0]);
                                                                    setFieldSelection("50");
                                                                  });
                                                                  if (_selectedPaymentMethod == "fiat") {
                                                                    await handleGetQuotes(_selectedCryptoPOLYGONFiatSymbol);
                                                                  }
                                                                },
                                                                child: Padding(
                                                                  padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.01),
                                                                  child: Row(
                                                                    children: <Widget>[
                                                                      Padding(
                                                                        padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.035, right: MediaQuery.of(context).size.width * 0.025),
                                                                        child: SvgPicture.asset(
                                                                          "images/polygon-network.svg",
                                                                          height: MediaQuery.of(context).size.height * 0.025,
                                                                          width: MediaQuery.of(context).size.width * 0.025,
                                                                        ),
                                                                      ),
                                                                      Padding(
                                                                        padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.035, left: MediaQuery.of(context).size.width * 0.00),
                                                                        child: Text(
                                                                          "POLYGON",
                                                                          style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.018),
                                                                        ),
                                                                      ),
                                                                      Spacer(),
                                                                    ],
                                                                  ),
                                                                ),
                                                              ),

                                                            /**       BTTC      */
                                                            if (_selectedPaymentMethod == "fiat")
                                                              InkWell(
                                                                onTap: () async {
                                                                  setState(() {
                                                                    _selectedSecondPaymentMethod = "bttc";
                                                                    _errorMessage = "";
                                                                    _errorTransactionExist = false;
                                                                    buttonPayCreditCard = false;
                                                                    setCryptoInfoBTTC(_secondCryptoListFiatBTTC[0]);
                                                                    setFieldSelection("50");
                                                                  });
                                                                  if (_selectedPaymentMethod == "fiat") {
                                                                    await handleGetQuotes(_selectedCryptoBTTCFiatSymbol);
                                                                  }
                                                                },
                                                                child: Padding(
                                                                  padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.01),
                                                                  child: Row(
                                                                    children: <Widget>[
                                                                      Padding(
                                                                        padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.035, right: MediaQuery.of(context).size.width * 0.025),
                                                                        child: SvgPicture.asset(
                                                                          "images/btt-network.svg",
                                                                          height: MediaQuery.of(context).size.height * 0.025,
                                                                          width: MediaQuery.of(context).size.width * 0.015,
                                                                        ),
                                                                      ),
                                                                      Padding(
                                                                        padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.035, left: MediaQuery.of(context).size.width * 0.005),
                                                                        child: Text(
                                                                          "BTT",
                                                                          style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.018),
                                                                        ),
                                                                      ),
                                                                      Spacer(),
                                                                    ],
                                                                  ),
                                                                ),
                                                              ),
                                                            if (_selectedPaymentMethod == "fiat")
                                                              InkWell(
                                                                onTap: () async {
                                                                  if (LoginController.apiService.tronAddress != null && LoginController.apiService.tronAddress != "") {
                                                                    setState(() {
                                                                      _selectedSecondPaymentMethod = "tron";
                                                                      _errorMessage = "";
                                                                      _errorTransactionExist = false;
                                                                      buttonPayCreditCard = false;
                                                                      setCryptoInfoTRX(_secondCryptoListFiatTRX[0]);
                                                                      setFieldSelection("50");
                                                                    });
                                                                    if (_selectedPaymentMethod == "fiat") {
                                                                      await handleGetQuotes(_selectedCryptoTRXFiatSymbol);
                                                                    }
                                                                  } else {
                                                                    createTronWalletUI(context);
                                                                  }
                                                                },
                                                                child: Padding(
                                                                  padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.01),
                                                                  child: Row(
                                                                    children: <Widget>[
                                                                      Padding(
                                                                        padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.035, right: MediaQuery.of(context).size.width * 0.025),
                                                                        child: SvgPicture.asset(
                                                                          "images/wallet-tron.svg",
                                                                          height: MediaQuery.of(context).size.height * 0.025,
                                                                          width: MediaQuery.of(context).size.width * 0.015,
                                                                          color: Colors.black,
                                                                        ),
                                                                      ),
                                                                      Padding(
                                                                        padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.035, left: MediaQuery.of(context).size.width * 0.0),
                                                                        child: Text(
                                                                          "TRON",
                                                                          style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.018),
                                                                        ),
                                                                      ),
                                                                      Spacer(),
                                                                    ],
                                                                  ),
                                                                ),
                                                              ),
                                                          ],
                                                        ),
                                                      )
                                                    : (_selectedSecondPaymentMethod == "polygon"
                                                        ? Padding(
                                                            padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.012),
                                                            child: Column(
                                                              children: <Widget>[
                                                                Padding(
                                                                  padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.0085),
                                                                  child: InkWell(
                                                                    onTap: () async {
                                                                      setState(() {
                                                                        _selectedSecondPaymentMethod = "bep20";
                                                                        _errorMessage = "";
                                                                        _errorTransactionExist = false;
                                                                        buttonPayCreditCard = false;
                                                                        _selectedPaymentMethod = _selectedPaymentMethod == "fiat" ? "fiat" : "bep20";
                                                                        setFieldSelection("50");
                                                                        calculatePriceBEP20();
                                                                        calculatePriceERC20();
                                                                      });
                                                                      if (_selectedPaymentMethod == "fiat") {
                                                                        await handleGetQuotes(handleSelectedCrypto());
                                                                      }
                                                                    },
                                                                    child: Row(
                                                                      children: <Widget>[
                                                                        Padding(
                                                                          padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025),
                                                                          child: SvgPicture.asset(
                                                                            "images/network-binance.svg",
                                                                            height: MediaQuery.of(context).size.height * 0.028,
                                                                            width: MediaQuery.of(context).size.width * 0.028,
                                                                          ),
                                                                        ),
                                                                        Text(
                                                                          "BEP20",
                                                                          style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.w600, fontSize: MediaQuery.of(context).size.height * 0.017),
                                                                        ),
                                                                      ],
                                                                    ),
                                                                  ),
                                                                ),
                                                                InkWell(
                                                                  onTap: () async {
                                                                    setState(() {
                                                                      _selectedSecondPaymentMethod = "erc20";
                                                                      _errorMessage = "";
                                                                      _errorTransactionExist = false;
                                                                      buttonPayCreditCard = false;
                                                                      _selectedPaymentMethod = _selectedPaymentMethod == "fiat" ? "fiat" : "erc20";
                                                                      setFieldSelection("50");
                                                                      calculatePriceBEP20();
                                                                      calculatePriceERC20();
                                                                    });
                                                                    if (_selectedPaymentMethod == "fiat") {
                                                                      await handleGetQuotes(handleSelectedCrypto());
                                                                    }
                                                                  },
                                                                  child: Row(
                                                                    children: <Widget>[
                                                                      Padding(
                                                                        padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025),
                                                                        child: SvgPicture.asset(
                                                                          "images/network-erc.svg",
                                                                          height: MediaQuery.of(context).size.height * 0.028,
                                                                          width: MediaQuery.of(context).size.width * 0.028,
                                                                        ),
                                                                      ),
                                                                      Text(
                                                                        "ERC20",
                                                                        style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.w600, fontSize: MediaQuery.of(context).size.height * 0.017),
                                                                      ),
                                                                    ],
                                                                  ),
                                                                ),
                                                                if (_selectedPaymentMethod == "fiat")
                                                                  InkWell(
                                                                    onTap: () async {
                                                                      setState(() {
                                                                        _selectedSecondPaymentMethod = "btc";
                                                                        _errorMessage = "";
                                                                        _errorTransactionExist = false;
                                                                        buttonPayCreditCard = false;
                                                                        setFieldSelection("50");
                                                                        calculatePriceBEP20();
                                                                        calculatePriceERC20();
                                                                      });
                                                                      if (_selectedPaymentMethod == "fiat") {
                                                                        await handleGetQuotes("BTC");
                                                                      }
                                                                    },
                                                                    child: Padding(
                                                                      padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.01),
                                                                      child: Row(
                                                                        children: <Widget>[
                                                                          Padding(
                                                                            padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.041, right: MediaQuery.of(context).size.width * 0.025),
                                                                            child: SvgPicture.asset(
                                                                              "images/btc-icon.svg",
                                                                              height: MediaQuery.of(context).size.height * 0.025,
                                                                              width: MediaQuery.of(context).size.width * 0.025,
                                                                            ),
                                                                          ),
                                                                          Padding(
                                                                            padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.035, left: MediaQuery.of(context).size.width * 0.0075),
                                                                            child: Text(
                                                                              "BTC",
                                                                              style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.018),
                                                                            ),
                                                                          ),
                                                                          Spacer(),
                                                                        ],
                                                                      ),
                                                                    ),
                                                                  ),
                                                                /**       BTTC      */
                                                                if (_selectedPaymentMethod == "fiat")
                                                                  InkWell(
                                                                    onTap: () async {
                                                                      setState(() {
                                                                        _selectedSecondPaymentMethod = "bttc";
                                                                        _errorMessage = "";
                                                                        _errorTransactionExist = false;
                                                                        buttonPayCreditCard = false;
                                                                        setCryptoInfoBTTC(_secondCryptoListFiatBTTC[0]);
                                                                        setFieldSelection("50");
                                                                      });
                                                                      if (_selectedPaymentMethod == "fiat") {
                                                                        await handleGetQuotes(_selectedCryptoBTTCFiatSymbol);
                                                                      }
                                                                    },
                                                                    child: Padding(
                                                                      padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.01),
                                                                      child: Row(
                                                                        children: <Widget>[
                                                                          Padding(
                                                                            padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.035, right: MediaQuery.of(context).size.width * 0.025),
                                                                            child: SvgPicture.asset(
                                                                              "images/btt-network.svg",
                                                                              height: MediaQuery.of(context).size.height * 0.025,
                                                                              width: MediaQuery.of(context).size.width * 0.015,
                                                                            ),
                                                                          ),
                                                                          Padding(
                                                                            padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.035, left: MediaQuery.of(context).size.width * 0.005),
                                                                            child: Text(
                                                                              "BTT",
                                                                              style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.018),
                                                                            ),
                                                                          ),
                                                                          Spacer(),
                                                                        ],
                                                                      ),
                                                                    ),
                                                                  ),
                                                                if (_selectedPaymentMethod == "fiat")
                                                                  InkWell(
                                                                    onTap: () async {
                                                                      if (LoginController.apiService.tronAddress != null && LoginController.apiService.tronAddress != "") {
                                                                        setState(() {
                                                                          _selectedSecondPaymentMethod = "tron";
                                                                          _errorMessage = "";
                                                                          _errorTransactionExist = false;
                                                                          buttonPayCreditCard = false;
                                                                          setCryptoInfoTRX(_secondCryptoListFiatTRX[0]);
                                                                          setFieldSelection("50");
                                                                        });
                                                                        if (_selectedPaymentMethod == "fiat") {
                                                                          await handleGetQuotes(_selectedCryptoTRXFiatSymbol);
                                                                        }
                                                                      } else {
                                                                        createTronWalletUI(context);
                                                                      }
                                                                    },
                                                                    child: Padding(
                                                                      padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.01),
                                                                      child: Row(
                                                                        children: <Widget>[
                                                                          Padding(
                                                                            padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.035, right: MediaQuery.of(context).size.width * 0.025),
                                                                            child: SvgPicture.asset(
                                                                              "images/wallet-tron.svg",
                                                                              height: MediaQuery.of(context).size.height * 0.025,
                                                                              width: MediaQuery.of(context).size.width * 0.015,
                                                                              color: Colors.black,
                                                                            ),
                                                                          ),
                                                                          Padding(
                                                                            padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.035, left: MediaQuery.of(context).size.width * 0.0),
                                                                            child: Text(
                                                                              "TRON",
                                                                              style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.018),
                                                                            ),
                                                                          ),
                                                                          Spacer(),
                                                                        ],
                                                                      ),
                                                                    ),
                                                                  ),
                                                              ],
                                                            ),
                                                          )
                                                        : (_selectedSecondPaymentMethod == "bttc"
                                                            ? Padding(
                                                                padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.012),
                                                                child: Column(
                                                                  children: <Widget>[
                                                                    Padding(
                                                                      padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.0085),
                                                                      child: InkWell(
                                                                        onTap: () async {
                                                                          setState(() {
                                                                            _selectedSecondPaymentMethod = "bep20";
                                                                            _errorMessage = "";
                                                                            _errorTransactionExist = false;
                                                                            buttonPayCreditCard = false;
                                                                            _selectedPaymentMethod = _selectedPaymentMethod == "fiat" ? "fiat" : "bep20";
                                                                            setFieldSelection("50");
                                                                            calculatePriceBEP20();
                                                                            calculatePriceERC20();
                                                                          });
                                                                          if (_selectedPaymentMethod == "fiat") {
                                                                            await handleGetQuotes(handleSelectedCrypto());
                                                                          }
                                                                        },
                                                                        child: Row(
                                                                          children: <Widget>[
                                                                            Padding(
                                                                              padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025),
                                                                              child: SvgPicture.asset(
                                                                                "images/network-binance.svg",
                                                                                height: MediaQuery.of(context).size.height * 0.028,
                                                                                width: MediaQuery.of(context).size.width * 0.028,
                                                                              ),
                                                                            ),
                                                                            Text(
                                                                              "BEP20",
                                                                              style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.w600, fontSize: MediaQuery.of(context).size.height * 0.017),
                                                                            ),
                                                                          ],
                                                                        ),
                                                                      ),
                                                                    ),
                                                                    InkWell(
                                                                      onTap: () async {
                                                                        setState(() {
                                                                          _selectedSecondPaymentMethod = "erc20";
                                                                          _errorMessage = "";
                                                                          _errorTransactionExist = false;
                                                                          buttonPayCreditCard = false;
                                                                          _selectedPaymentMethod = _selectedPaymentMethod == "fiat" ? "fiat" : "erc20";
                                                                          setFieldSelection("50");
                                                                          calculatePriceBEP20();
                                                                          calculatePriceERC20();
                                                                        });
                                                                        if (_selectedPaymentMethod == "fiat") {
                                                                          await handleGetQuotes(handleSelectedCrypto());
                                                                        }
                                                                      },
                                                                      child: Row(
                                                                        children: <Widget>[
                                                                          Padding(
                                                                            padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025),
                                                                            child: SvgPicture.asset(
                                                                              "images/network-erc.svg",
                                                                              height: MediaQuery.of(context).size.height * 0.028,
                                                                              width: MediaQuery.of(context).size.width * 0.028,
                                                                            ),
                                                                          ),
                                                                          Text(
                                                                            "ERC20",
                                                                            style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.w600, fontSize: MediaQuery.of(context).size.height * 0.017),
                                                                          ),
                                                                        ],
                                                                      ),
                                                                    ),
                                                                    /**  ------    POLYGON     -------   */
                                                                    if (_selectedPaymentMethod == "fiat")
                                                                      InkWell(
                                                                        onTap: () async {
                                                                          setState(() {
                                                                            _selectedSecondPaymentMethod = "polygon";
                                                                            _errorMessage = "";
                                                                            _errorTransactionExist = false;
                                                                            buttonPayCreditCard = false;
                                                                            setCryptoInfoPoly(_secondCryptoListFiatPOLYGON[0]);
                                                                            setFieldSelection("50");
                                                                          });
                                                                          if (_selectedPaymentMethod == "fiat") {
                                                                            await handleGetQuotes(_selectedCryptoPOLYGONFiatSymbol);
                                                                          }
                                                                        },
                                                                        child: Padding(
                                                                          padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.01),
                                                                          child: Row(
                                                                            children: <Widget>[
                                                                              Padding(
                                                                                padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.035, right: MediaQuery.of(context).size.width * 0.025),
                                                                                child: SvgPicture.asset(
                                                                                  "images/polygon-network.svg",
                                                                                  height: MediaQuery.of(context).size.height * 0.025,
                                                                                  width: MediaQuery.of(context).size.width * 0.025,
                                                                                ),
                                                                              ),
                                                                              Padding(
                                                                                padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.035, left: MediaQuery.of(context).size.width * 0.00),
                                                                                child: Text(
                                                                                  "POLYGON",
                                                                                  style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.018),
                                                                                ),
                                                                              ),
                                                                              Spacer(),
                                                                            ],
                                                                          ),
                                                                        ),
                                                                      ),

                                                                    /** BTC */
                                                                    if (_selectedPaymentMethod == "fiat")
                                                                      InkWell(
                                                                        onTap: () async {
                                                                          setState(() {
                                                                            _selectedSecondPaymentMethod = "btc";
                                                                            _errorMessage = "";
                                                                            _errorTransactionExist = false;
                                                                            buttonPayCreditCard = false;
                                                                            setFieldSelection("50");
                                                                            calculatePriceBEP20();
                                                                            calculatePriceERC20();
                                                                          });
                                                                          if (_selectedPaymentMethod == "fiat") {
                                                                            await handleGetQuotes("BTC");
                                                                          }
                                                                        },
                                                                        child: Padding(
                                                                          padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.01),
                                                                          child: Row(
                                                                            children: <Widget>[
                                                                              Padding(
                                                                                padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.041, right: MediaQuery.of(context).size.width * 0.025),
                                                                                child: SvgPicture.asset(
                                                                                  "images/btc-icon.svg",
                                                                                  height: MediaQuery.of(context).size.height * 0.025,
                                                                                  width: MediaQuery.of(context).size.width * 0.025,
                                                                                ),
                                                                              ),
                                                                              Padding(
                                                                                padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.035, left: MediaQuery.of(context).size.width * 0.0075),
                                                                                child: Text(
                                                                                  "BTC",
                                                                                  style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.018),
                                                                                ),
                                                                              ),
                                                                              Spacer(),
                                                                            ],
                                                                          ),
                                                                        ),
                                                                      ),
                                                                    if (_selectedPaymentMethod == "fiat")
                                                                      InkWell(
                                                                        onTap: () async {
                                                                          if (LoginController.apiService.tronAddress != null && LoginController.apiService.tronAddress != "") {
                                                                            setState(() {
                                                                              _selectedSecondPaymentMethod = "tron";
                                                                              _errorMessage = "";
                                                                              _errorTransactionExist = false;
                                                                              buttonPayCreditCard = false;
                                                                              setCryptoInfoTRX(_secondCryptoListFiatTRX[0]);
                                                                              setFieldSelection("50");
                                                                            });
                                                                            if (_selectedPaymentMethod == "fiat") {
                                                                              await handleGetQuotes(_selectedCryptoTRXFiatSymbol);
                                                                            }
                                                                          } else {
                                                                            createTronWalletUI(context);
                                                                          }
                                                                        },
                                                                        child: Padding(
                                                                          padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.01),
                                                                          child: Row(
                                                                            children: <Widget>[
                                                                              Padding(
                                                                                padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.035, right: MediaQuery.of(context).size.width * 0.025),
                                                                                child: SvgPicture.asset(
                                                                                  "images/wallet-tron.svg",
                                                                                  height: MediaQuery.of(context).size.height * 0.025,
                                                                                  width: MediaQuery.of(context).size.width * 0.015,
                                                                                  color: Colors.black,
                                                                                ),
                                                                              ),
                                                                              Padding(
                                                                                padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.035, left: MediaQuery.of(context).size.width * 0.0),
                                                                                child: Text(
                                                                                  "TRON",
                                                                                  style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.018),
                                                                                ),
                                                                              ),
                                                                              Spacer(),
                                                                            ],
                                                                          ),
                                                                        ),
                                                                      ),
                                                                  ],
                                                                ),
                                                              )
                                                            : (_selectedSecondPaymentMethod == "tron"
                                                                ? Padding(
                                                                    padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.012),
                                                                    child: Column(
                                                                      children: <Widget>[
                                                                        Padding(
                                                                          padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.0085),
                                                                          child: InkWell(
                                                                            onTap: () async {
                                                                              setState(() {
                                                                                _selectedSecondPaymentMethod = "bep20";
                                                                                _errorMessage = "";
                                                                                _errorTransactionExist = false;
                                                                                buttonPayCreditCard = false;
                                                                                _selectedPaymentMethod = _selectedPaymentMethod == "fiat" ? "fiat" : "bep20";
                                                                                setFieldSelection("50");
                                                                                calculatePriceBEP20();
                                                                                calculatePriceERC20();
                                                                              });
                                                                              if (_selectedPaymentMethod == "fiat") {
                                                                                await handleGetQuotes(handleSelectedCrypto());
                                                                              }
                                                                            },
                                                                            child: Row(
                                                                              children: <Widget>[
                                                                                Padding(
                                                                                  padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025),
                                                                                  child: SvgPicture.asset(
                                                                                    "images/network-binance.svg",
                                                                                    height: MediaQuery.of(context).size.height * 0.028,
                                                                                    width: MediaQuery.of(context).size.width * 0.028,
                                                                                  ),
                                                                                ),
                                                                                Text(
                                                                                  "BEP20",
                                                                                  style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.w600, fontSize: MediaQuery.of(context).size.height * 0.017),
                                                                                ),
                                                                              ],
                                                                            ),
                                                                          ),
                                                                        ),
                                                                        InkWell(
                                                                          onTap: () async {
                                                                            setState(() {
                                                                              _selectedSecondPaymentMethod = "erc20";
                                                                              _errorMessage = "";
                                                                              _errorTransactionExist = false;
                                                                              buttonPayCreditCard = false;
                                                                              _selectedPaymentMethod = _selectedPaymentMethod == "fiat" ? "fiat" : "erc20";
                                                                              setFieldSelection("50");
                                                                              calculatePriceBEP20();
                                                                              calculatePriceERC20();
                                                                            });
                                                                            if (_selectedPaymentMethod == "fiat") {
                                                                              await handleGetQuotes(handleSelectedCrypto());
                                                                            }
                                                                          },
                                                                          child: Row(
                                                                            children: <Widget>[
                                                                              Padding(
                                                                                padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025),
                                                                                child: SvgPicture.asset(
                                                                                  "images/network-erc.svg",
                                                                                  height: MediaQuery.of(context).size.height * 0.028,
                                                                                  width: MediaQuery.of(context).size.width * 0.028,
                                                                                ),
                                                                              ),
                                                                              Text(
                                                                                "ERC20",
                                                                                style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.w600, fontSize: MediaQuery.of(context).size.height * 0.017),
                                                                              ),
                                                                            ],
                                                                          ),
                                                                        ),

                                                                        /**  ------    POLYGON     -------   */
                                                                        if (_selectedPaymentMethod == "fiat")
                                                                          InkWell(
                                                                            onTap: () async {
                                                                              setState(() {
                                                                                _selectedSecondPaymentMethod = "polygon";
                                                                                _errorMessage = "";
                                                                                _errorTransactionExist = false;
                                                                                buttonPayCreditCard = false;
                                                                                setCryptoInfoPoly(_secondCryptoListFiatPOLYGON[0]);
                                                                                setFieldSelection("50");
                                                                              });
                                                                              if (_selectedPaymentMethod == "fiat") {
                                                                                await handleGetQuotes(_selectedCryptoPOLYGONFiatSymbol);
                                                                              }
                                                                            },
                                                                            child: Padding(
                                                                              padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.01),
                                                                              child: Row(
                                                                                children: <Widget>[
                                                                                  Padding(
                                                                                    padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.035, right: MediaQuery.of(context).size.width * 0.025),
                                                                                    child: SvgPicture.asset(
                                                                                      "images/polygon-network.svg",
                                                                                      height: MediaQuery.of(context).size.height * 0.025,
                                                                                      width: MediaQuery.of(context).size.width * 0.025,
                                                                                    ),
                                                                                  ),
                                                                                  Padding(
                                                                                    padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.035, left: MediaQuery.of(context).size.width * 0.00),
                                                                                    child: Text(
                                                                                      "POLYGON",
                                                                                      style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.018),
                                                                                    ),
                                                                                  ),
                                                                                  Spacer(),
                                                                                ],
                                                                              ),
                                                                            ),
                                                                          ),

                                                                        /** ------- BTT ------- */
                                                                        if (_selectedPaymentMethod == "fiat")
                                                                          InkWell(
                                                                            onTap: () async {
                                                                              setState(() {
                                                                                _selectedSecondPaymentMethod = "bttc";
                                                                                _errorMessage = "";
                                                                                _errorTransactionExist = false;
                                                                                buttonPayCreditCard = false;
                                                                                setCryptoInfoBTTC(_secondCryptoListFiatBTTC[0]);
                                                                                setFieldSelection("50");
                                                                              });
                                                                              if (_selectedPaymentMethod == "fiat") {
                                                                                await handleGetQuotes(_selectedCryptoBTTCFiatSymbol);
                                                                              }
                                                                            },
                                                                            child: Padding(
                                                                              padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.01),
                                                                              child: Row(
                                                                                children: <Widget>[
                                                                                  Padding(
                                                                                    padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.035, right: MediaQuery.of(context).size.width * 0.025),
                                                                                    child: SvgPicture.asset(
                                                                                      "images/btt-network.svg",
                                                                                      height: MediaQuery.of(context).size.height * 0.025,
                                                                                      width: MediaQuery.of(context).size.width * 0.015,
                                                                                    ),
                                                                                  ),
                                                                                  Padding(
                                                                                    padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.035, left: MediaQuery.of(context).size.width * 0.005),
                                                                                    child: Text(
                                                                                      "BTT",
                                                                                      style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.018),
                                                                                    ),
                                                                                  ),
                                                                                  Spacer(),
                                                                                ],
                                                                              ),
                                                                            ),
                                                                          ),
                                                                        if (_selectedPaymentMethod == "fiat")
                                                                          InkWell(
                                                                            onTap: () async {
                                                                              setState(() {
                                                                                _selectedSecondPaymentMethod = "btc";
                                                                                _errorMessage = "";
                                                                                _errorTransactionExist = false;
                                                                                buttonPayCreditCard = false;
                                                                                setFieldSelection("50");
                                                                                calculatePriceBEP20();
                                                                                calculatePriceERC20();
                                                                              });
                                                                              if (_selectedPaymentMethod == "fiat") {
                                                                                await handleGetQuotes("BTC");
                                                                              }
                                                                            },
                                                                            child: Padding(
                                                                              padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.01),
                                                                              child: Row(
                                                                                children: <Widget>[
                                                                                  Padding(
                                                                                    padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.041, right: MediaQuery.of(context).size.width * 0.025),
                                                                                    child: SvgPicture.asset(
                                                                                      "images/btc-icon.svg",
                                                                                      height: MediaQuery.of(context).size.height * 0.025,
                                                                                      width: MediaQuery.of(context).size.width * 0.025,
                                                                                    ),
                                                                                  ),
                                                                                  Padding(
                                                                                    padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.035, left: MediaQuery.of(context).size.width * 0.0075),
                                                                                    child: Text(
                                                                                      "BTC",
                                                                                      style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.018),
                                                                                    ),
                                                                                  ),
                                                                                  Spacer(),
                                                                                ],
                                                                              ),
                                                                            ),
                                                                          ),
                                                                      ],
                                                                    ),
                                                                  )
                                                                : Padding(
                                                                    padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.012),
                                                                    child: Column(
                                                                      children: <Widget>[
                                                                        Padding(
                                                                          padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.0085),
                                                                          child: InkWell(
                                                                            onTap: () async {
                                                                              setState(() {
                                                                                _selectedSecondPaymentMethod = "bep20";
                                                                                _errorMessage = "";
                                                                                _errorTransactionExist = false;
                                                                                buttonPayCreditCard = false;
                                                                                _selectedPaymentMethod = _selectedPaymentMethod == "fiat" ? "fiat" : "bep20";
                                                                                setFieldSelection("50");
                                                                                calculatePriceBEP20();
                                                                                calculatePriceERC20();
                                                                              });
                                                                              if (_selectedPaymentMethod == "fiat") {
                                                                                await handleGetQuotes(handleSelectedCrypto());
                                                                              }
                                                                            },
                                                                            child: Row(
                                                                              children: <Widget>[
                                                                                Padding(
                                                                                  padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025),
                                                                                  child: SvgPicture.asset(
                                                                                    "images/network-binance.svg",
                                                                                    height: MediaQuery.of(context).size.height * 0.028,
                                                                                    width: MediaQuery.of(context).size.width * 0.028,
                                                                                  ),
                                                                                ),
                                                                                Text(
                                                                                  "BEP20",
                                                                                  style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.w600, fontSize: MediaQuery.of(context).size.height * 0.017),
                                                                                ),
                                                                              ],
                                                                            ),
                                                                          ),
                                                                        ),
                                                                        InkWell(
                                                                          onTap: () async {
                                                                            setState(() {
                                                                              _selectedSecondPaymentMethod = "erc20";
                                                                              _errorMessage = "";
                                                                              _errorTransactionExist = false;
                                                                              buttonPayCreditCard = false;
                                                                              _selectedPaymentMethod = _selectedPaymentMethod == "fiat" ? "fiat" : "erc20";
                                                                              setFieldSelection("50");
                                                                              calculatePriceBEP20();
                                                                              calculatePriceERC20();
                                                                            });
                                                                            if (_selectedPaymentMethod == "fiat") {
                                                                              await handleGetQuotes(handleSelectedCrypto());
                                                                            }
                                                                          },
                                                                          child: Row(
                                                                            children: <Widget>[
                                                                              Padding(
                                                                                padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025),
                                                                                child: SvgPicture.asset(
                                                                                  "images/network-erc.svg",
                                                                                  height: MediaQuery.of(context).size.height * 0.028,
                                                                                  width: MediaQuery.of(context).size.width * 0.028,
                                                                                ),
                                                                              ),
                                                                              Text(
                                                                                "ERC20",
                                                                                style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.w600, fontSize: MediaQuery.of(context).size.height * 0.017),
                                                                              ),
                                                                            ],
                                                                          ),
                                                                        ),
                                                                        /**  ------    POLYGON     -------   */
                                                                        if (_selectedPaymentMethod == "fiat")
                                                                          InkWell(
                                                                            onTap: () async {
                                                                              setState(() {
                                                                                _selectedSecondPaymentMethod = "polygon";
                                                                                _errorMessage = "";
                                                                                _errorTransactionExist = false;
                                                                                buttonPayCreditCard = false;
                                                                                setCryptoInfoPoly(_secondCryptoListFiatPOLYGON[0]);
                                                                                setFieldSelection("50");
                                                                              });
                                                                              if (_selectedPaymentMethod == "fiat") {
                                                                                await handleGetQuotes(_selectedCryptoPOLYGONFiatSymbol);
                                                                              }
                                                                            },
                                                                            child: Padding(
                                                                              padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.01),
                                                                              child: Row(
                                                                                children: <Widget>[
                                                                                  Padding(
                                                                                    padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.035, right: MediaQuery.of(context).size.width * 0.025),
                                                                                    child: SvgPicture.asset(
                                                                                      "images/polygon-network.svg",
                                                                                      height: MediaQuery.of(context).size.height * 0.025,
                                                                                      width: MediaQuery.of(context).size.width * 0.025,
                                                                                    ),
                                                                                  ),
                                                                                  Padding(
                                                                                    padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.035, left: MediaQuery.of(context).size.width * 0.00),
                                                                                    child: Text(
                                                                                      "POLYGON",
                                                                                      style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.018),
                                                                                    ),
                                                                                  ),
                                                                                  Spacer(),
                                                                                ],
                                                                              ),
                                                                            ),
                                                                          ),
                                                                        /** ------- BTT ------- */
                                                                        if (_selectedPaymentMethod == "fiat")
                                                                          InkWell(
                                                                            onTap: () async {
                                                                              setState(() {
                                                                                _selectedSecondPaymentMethod = "bttc";
                                                                                _errorMessage = "";
                                                                                _errorTransactionExist = false;
                                                                                buttonPayCreditCard = false;
                                                                                setCryptoInfoBTTC(_secondCryptoListFiatBTTC[0]);
                                                                                setFieldSelection("50");
                                                                              });
                                                                              if (_selectedPaymentMethod == "fiat") {
                                                                                await handleGetQuotes(_selectedCryptoBTTCFiatSymbol);
                                                                              }
                                                                            },
                                                                            child: Padding(
                                                                              padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.01),
                                                                              child: Row(
                                                                                children: <Widget>[
                                                                                  Padding(
                                                                                    padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.035, right: MediaQuery.of(context).size.width * 0.025),
                                                                                    child: SvgPicture.asset(
                                                                                      "images/btt-network.svg",
                                                                                      height: MediaQuery.of(context).size.height * 0.025,
                                                                                      width: MediaQuery.of(context).size.width * 0.015,
                                                                                    ),
                                                                                  ),
                                                                                  Padding(
                                                                                    padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.035, left: MediaQuery.of(context).size.width * 0.005),
                                                                                    child: Text(
                                                                                      "BTT",
                                                                                      style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.018),
                                                                                    ),
                                                                                  ),
                                                                                  Spacer(),
                                                                                ],
                                                                              ),
                                                                            ),
                                                                          ),
                                                                        if (_selectedPaymentMethod == "fiat")
                                                                          InkWell(
                                                                            onTap: () async {
                                                                              if (LoginController.apiService.tronAddress != null && LoginController.apiService.tronAddress != "") {
                                                                                setState(() {
                                                                                  _selectedSecondPaymentMethod = "tron";
                                                                                  _errorMessage = "";
                                                                                  _errorTransactionExist = false;
                                                                                  buttonPayCreditCard = false;
                                                                                  setCryptoInfoTRX(_secondCryptoListFiatTRX[0]);
                                                                                  setFieldSelection("50");
                                                                                });
                                                                                if (_selectedPaymentMethod == "fiat") {
                                                                                  await handleGetQuotes(_selectedCryptoTRXFiatSymbol);
                                                                                }
                                                                              } else {
                                                                                createTronWalletUI(context);
                                                                              }
                                                                            },
                                                                            child: Padding(
                                                                              padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.01),
                                                                              child: Row(
                                                                                children: <Widget>[
                                                                                  Padding(
                                                                                    padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.035, right: MediaQuery.of(context).size.width * 0.025),
                                                                                    child: SvgPicture.asset(
                                                                                      "images/wallet-tron.svg",
                                                                                      height: MediaQuery.of(context).size.height * 0.025,
                                                                                      width: MediaQuery.of(context).size.width * 0.015,
                                                                                      color: Colors.black,
                                                                                    ),
                                                                                  ),
                                                                                  Padding(
                                                                                    padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.035, left: MediaQuery.of(context).size.width * 0.0),
                                                                                    child: Text(
                                                                                      "TRON",
                                                                                      style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.018),
                                                                                    ),
                                                                                  ),
                                                                                  Spacer(),
                                                                                ],
                                                                              ),
                                                                            ),
                                                                          ),
                                                                      ],
                                                                    ),
                                                                  )))))),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),

                          /** Second Line of buy -- CRYPTO LIST for BEP20 / ERC20 */
                          Positioned(
                              top: _errorTransactionExist ? MediaQuery.of(context).size.height * 0.57 : MediaQuery.of(context).size.height * 0.51,
                              left: MediaQuery.of(context).size.width * 0.425,
                              child: Padding(
                                  padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.075),
                                  child: _selectedPaymentMethod == "bep20"
                                      ? InkWell(
                                          onTap: () {
                                            setState(() {
                                              _clickedCryptoBEP20SecondContainer = !_clickedCryptoBEP20SecondContainer;
                                              _clickedValueFiat = false;
                                              FocusScope.of(context).requestFocus(FocusNode());
                                            });
                                          },
                                          child: Container(
                                            height: _clickedCryptoBEP20SecondContainer ? MediaQuery.of(context).size.height * 0.15 : MediaQuery.of(context).size.height * 0.065,
                                            width: MediaQuery.of(context).size.width * 0.45,
                                            decoration: BoxDecoration(
                                              border: Border.all(color: Color(0xFFD6D6E8)),
                                              borderRadius: BorderRadius.circular(30),
                                              color: Colors.white,
                                            ),
                                            child: _clickedCryptoBEP20SecondContainer
                                                ? Padding(
                                                    padding: EdgeInsets.only(
                                                      bottom: MediaQuery.of(context).size.height * 0.015,
                                                      right: MediaQuery.of(context).size.height * 0.015,
                                                      top: MediaQuery.of(context).size.height * 0.015,
                                                    ),
                                                    child: Theme(
                                                      data: scrolltheme,
                                                      child: ListView.builder(
                                                        scrollDirection: Axis.vertical,
                                                        shrinkWrap: true,
                                                        itemCount: _secondCryptoListBEP20.length,
                                                        itemBuilder: (context, index) {
                                                          return Padding(
                                                            padding: EdgeInsets.only(
                                                              top: MediaQuery.of(context).size.height * 0.015,
                                                              left: MediaQuery.of(context).size.width * 0.015,
                                                            ),
                                                            child: InkWell(
                                                              onTap: () {
                                                                setState(() {
                                                                  _selectedCryptoBEP20SecondAddedToken = _secondCryptoListBEP20[index].addedToken ?? false;
                                                                  _selectedCryptoBEP20SecondPicUrl = _secondCryptoListBEP20[index].picUrl.toString();
                                                                  _selectedCryptoBEP20SecondName = _secondCryptoListBEP20[index].name.toString();
                                                                  _selectedCryptoBEP20SecondSymbol = _secondCryptoListBEP20[index].symbol.toString();
                                                                  _secondSelectedCryptoBEP20 = _secondCryptoListBEP20[index].symbol.toString();

                                                                  setFieldSelection(_cryptoBalance.text);
                                                                  if (_selectedCryptoBEP20 == _secondSelectedCryptoBEP20) {
                                                                    switch (_secondSelectedCryptoBEP20) {
                                                                      case "CAKE":
                                                                        _selectedCryptoBEP20 = "SATTBEP20";
                                                                        cryptoName1 = "SATT";
                                                                        fetchCrypto().then((value) {
                                                                          _cryptoListBEP20.clear();
                                                                          _cryptoListBEP20.addAll(value.where((e) => e.symbol == "SATTBEP20" || e.symbol == "BNB" || e.symbol == "BUSD").toList());
                                                                          setFirstFieldBEP20(_cryptoListBEP20[0]);
                                                                        });
                                                                        break;
                                                                      case "BNB":
                                                                        _selectedCryptoBEP20 = "SATTBEP20";
                                                                        fetchCrypto().then((value) {
                                                                          _cryptoListBEP20.clear();
                                                                          _cryptoListBEP20.addAll(value.where((e) => e.symbol == "SATTBEP20" || e.symbol == "BUSD" || e.symbol == "CAKE").toList());
                                                                          setFirstFieldBEP20(_cryptoListBEP20[0]);
                                                                        });
                                                                        break;
                                                                      case "BUSD":
                                                                        _selectedCryptoBEP20 = "SATTBEP20";

                                                                        fetchCrypto().then((value) {
                                                                          _cryptoListBEP20.clear();
                                                                          _cryptoListBEP20.addAll(value.where((e) => e.symbol == "SATTBEP20" || e.symbol == "BNB" || e.symbol == "CAKE").toList());
                                                                          setFirstFieldBEP20(_cryptoListBEP20[0]);
                                                                        });
                                                                        break;
                                                                      case "SATTBEP20":
                                                                        _selectedCryptoBEP20 = "BNB";

                                                                        fetchCrypto().then((value) {
                                                                          _cryptoListBEP20.clear();
                                                                          _cryptoListBEP20.addAll(value.where((e) => e.symbol == "BNB" || e.symbol == "CAKE" || e.symbol == "BUSD").toList());
                                                                          setFirstFieldBEP20(_cryptoListBEP20[0]);
                                                                        });
                                                                        break;
                                                                    }
                                                                  }
                                                                  _cryptoBalance.text = "50";
                                                                  calculatePriceBEP20();
                                                                });
                                                              },
                                                              child: Row(
                                                                children: [
                                                                  _secondCryptoListBEP20[index].addedToken == true
                                                                      ? (_secondCryptoListBEP20[index].picUrl != "false" && _secondCryptoListBEP20[index].picUrl != "null"
                                                                          ? Padding(
                                                                              padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.018),
                                                                              child: Image.network(
                                                                                _secondCryptoListBEP20[index].picUrl ?? "",
                                                                                width: MediaQuery.of(context).size.width * 0.055,
                                                                                height: MediaQuery.of(context).size.width * 0.055,
                                                                              ),
                                                                            )
                                                                          : Padding(
                                                                              padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.018),
                                                                              child: SvgPicture.asset(
                                                                                'images/indispo.svg',
                                                                                width: MediaQuery.of(context).size.width * 0.055,
                                                                                height: MediaQuery.of(context).size.width * 0.055,
                                                                              ),
                                                                            ))
                                                                      : Padding(
                                                                          padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.018),
                                                                          child: SvgPicture.asset(
                                                                            'images/' + _secondCryptoListBEP20[index].name.toString().toLowerCase().replaceAll(' ', '') + '.svg',
                                                                            alignment: Alignment.center,
                                                                            width: MediaQuery.of(context).size.width * 0.055,
                                                                            height: MediaQuery.of(context).size.width * 0.055,
                                                                          ),
                                                                        ),
                                                                  SizedBox(
                                                                    width: MediaQuery.of(context).size.width * 0.02,
                                                                  ),
                                                                  new Text(
                                                                    _secondCryptoListBEP20[index].symbol.toString().toUpperCase() == "SATTBEP20" ? "SATT" : _secondCryptoListBEP20[index].symbol.toString().toUpperCase(),
                                                                    style: GoogleFonts.poppins(color: Colors.black, fontWeight: FontWeight.w600, fontSize: MediaQuery.of(context).size.height * 0.0165),
                                                                  ),
                                                                ],
                                                              ),
                                                            ),
                                                          );
                                                        },
                                                      ),
                                                    ),
                                                  )
                                                : Padding(
                                                    padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.015),
                                                    child: Row(
                                                      children: [
                                                        _selectedCryptoBEP20SecondAddedToken == true
                                                            ? (_selectedCryptoBEP20SecondPicUrl != "false" && _selectedCryptoBEP20SecondPicUrl != "null"
                                                                ? Padding(
                                                                    padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.018),
                                                                    child: Image.network(
                                                                      _selectedCryptoBEP20SecondPicUrl,
                                                                      width: MediaQuery.of(context).size.width * 0.055,
                                                                      height: MediaQuery.of(context).size.width * 0.055,
                                                                    ),
                                                                  )
                                                                : Padding(
                                                                    padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.018),
                                                                    child: SvgPicture.asset(
                                                                      'images/indispo.svg',
                                                                      width: MediaQuery.of(context).size.width * 0.055,
                                                                      height: MediaQuery.of(context).size.width * 0.055,
                                                                    ),
                                                                  ))
                                                            : Padding(
                                                                padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.018),
                                                                child: SvgPicture.asset(
                                                                  'images/' + _selectedCryptoBEP20SecondName.toLowerCase().replaceAll(' ', '') + '.svg',
                                                                  alignment: Alignment.center,
                                                                  width: MediaQuery.of(context).size.width * 0.055,
                                                                  height: MediaQuery.of(context).size.width * 0.055,
                                                                ),
                                                              ),
                                                        SizedBox(
                                                          width: MediaQuery.of(context).size.width * 0.02,
                                                        ),
                                                        new Text(
                                                          _selectedCryptoBEP20SecondSymbol == "SATTBEP20" ? "SATT" : _selectedCryptoBEP20SecondSymbol,
                                                          style: GoogleFonts.poppins(color: Colors.black, fontWeight: FontWeight.w600, fontSize: MediaQuery.of(context).size.height * 0.0165),
                                                        ),
                                                        const Spacer(),
                                                        Padding(
                                                          padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.05),
                                                          child: SizedBox(
                                                            width: 10,
                                                            child: Transform.rotate(
                                                                angle: -math.pi / 2,
                                                                child: Icon(
                                                                  Icons.arrow_back_ios,
                                                                  color: Colors.grey,
                                                                  size: MediaQuery.of(context).size.width * 0.06,
                                                                )),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                          ),
                                        )
                                      : (_selectedPaymentMethod == "erc20"
                                          ? InkWell(
                                              onTap: () {
                                                setState(() {
                                                  _clickedCryptoERC20SecondContainer = !_clickedCryptoERC20SecondContainer;
                                                  _clickedValueFiat = false;
                                                  FocusScope.of(context).requestFocus(FocusNode());
                                                });
                                              },
                                              child: Container(
                                                height: _clickedCryptoERC20SecondContainer ? MediaQuery.of(context).size.height * 0.15 : MediaQuery.of(context).size.height * 0.065,
                                                width: MediaQuery.of(context).size.width * 0.45,
                                                decoration: BoxDecoration(
                                                  border: Border.all(color: Color(0xFFD6D6E8)),
                                                  borderRadius: BorderRadius.circular(30),
                                                  color: Colors.white,
                                                ),
                                                child: _clickedCryptoERC20SecondContainer
                                                    ? Padding(
                                                        padding: EdgeInsets.only(
                                                          bottom: MediaQuery.of(context).size.height * 0.015,
                                                          right: MediaQuery.of(context).size.height * 0.015,
                                                          top: MediaQuery.of(context).size.height * 0.015,
                                                        ),
                                                        child: Theme(
                                                          data: scrolltheme,
                                                          child: Scrollbar(
                                                            child: ListView.builder(
                                                              scrollDirection: Axis.vertical,
                                                              shrinkWrap: true,
                                                              itemCount: _secondCryptoListERC20.length,
                                                              itemBuilder: (context, index) {
                                                                return Padding(
                                                                  padding: EdgeInsets.only(
                                                                    top: MediaQuery.of(context).size.height * 0.015,
                                                                    left: MediaQuery.of(context).size.width * 0.015,
                                                                  ),
                                                                  child: InkWell(
                                                                    onTap: () {
                                                                      setState(() {
                                                                        _clickedCryptoERC20SecondContainer = false;

                                                                        _selectedCryptoERC20SecondAddedToken = _secondCryptoListERC20[index].addedToken ?? false;
                                                                        _selectedCryptoERC20SecondPicUrl = _secondCryptoListERC20[index].picUrl.toString();
                                                                        _selectedCryptoERC20SecondName = _secondCryptoListERC20[index].name.toString();
                                                                        _selectedCryptoERC20SecondSymbol = _secondCryptoListERC20[index].symbol.toString();
                                                                        _secondSelectedCryptoERC20 = _secondCryptoListERC20[index].symbol.toString();
                                                                        setFieldSelection(_cryptoBalance.text);
                                                                        if (_selectedCryptoERC20 == _secondSelectedCryptoERC20) {
                                                                          if (_secondSelectedCryptoERC20 == "WSATT") {
                                                                            _selectedCryptoERC20 = "ETH";
                                                                            Crypto crypto = _cryptoListERC20.firstWhere((element) => element.symbol == "ETH");
                                                                            setFirstFieldERC20(crypto);
                                                                          } else {
                                                                            _selectedCryptoERC20 = "WSATT";
                                                                            Crypto crypto = _cryptoListERC20.firstWhere((element) => element.symbol == "WSATT");
                                                                            setFirstFieldERC20(crypto);
                                                                          }
                                                                        }
                                                                        _cryptoBalance.text = "50";
                                                                        calculatePriceERC20();
                                                                      });
                                                                    },
                                                                    child: Row(
                                                                      children: [
                                                                        _secondCryptoListERC20[index].addedToken == true
                                                                            ? (_secondCryptoListERC20[index].picUrl != "false" && _secondCryptoListERC20[index].picUrl != "null"
                                                                                ? Padding(
                                                                                    padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.018),
                                                                                    child: Image.network(
                                                                                      _secondCryptoListERC20[index].picUrl ?? "",
                                                                                      width: MediaQuery.of(context).size.width * 0.055,
                                                                                      height: MediaQuery.of(context).size.width * 0.055,
                                                                                    ),
                                                                                  )
                                                                                : Padding(
                                                                                    padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.018),
                                                                                    child: SvgPicture.asset(
                                                                                      'images/indispo.svg',
                                                                                      width: MediaQuery.of(context).size.width * 0.055,
                                                                                      height: MediaQuery.of(context).size.width * 0.055,
                                                                                    ),
                                                                                  ))
                                                                            : Padding(
                                                                                padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.018),
                                                                                child: SvgPicture.asset(
                                                                                  'images/' + _secondCryptoListERC20[index].name.toString().toLowerCase().replaceAll(' ', '') + '.svg',
                                                                                  alignment: Alignment.center,
                                                                                  width: MediaQuery.of(context).size.width * 0.055,
                                                                                  height: MediaQuery.of(context).size.width * 0.055,
                                                                                ),
                                                                              ),
                                                                        SizedBox(
                                                                          width: MediaQuery.of(context).size.width * 0.02,
                                                                        ),
                                                                        new Text(
                                                                          _secondCryptoListERC20[index].symbol.toString().toUpperCase() == "SATTBEP20" ? "SATT" : _secondCryptoListERC20[index].symbol.toString().toUpperCase(),
                                                                          style: GoogleFonts.poppins(color: Colors.black, fontWeight: FontWeight.w600, fontSize: MediaQuery.of(context).size.height * 0.0165),
                                                                        ),
                                                                      ],
                                                                    ),
                                                                  ),
                                                                );
                                                              },
                                                            ),
                                                          ),
                                                        ),
                                                      )
                                                    : Padding(
                                                        padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.015),
                                                        child: Row(
                                                          children: [
                                                            _selectedCryptoERC20SecondAddedToken == true
                                                                ? (_selectedCryptoERC20SecondPicUrl != "false" && _selectedCryptoERC20SecondPicUrl != "null"
                                                                    ? Padding(
                                                                        padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.018),
                                                                        child: Image.network(
                                                                          _selectedCryptoERC20SecondPicUrl,
                                                                          width: MediaQuery.of(context).size.width * 0.055,
                                                                          height: MediaQuery.of(context).size.width * 0.055,
                                                                        ),
                                                                      )
                                                                    : Padding(
                                                                        padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.018),
                                                                        child: SvgPicture.asset(
                                                                          'images/indispo.svg',
                                                                          width: MediaQuery.of(context).size.width * 0.055,
                                                                          height: MediaQuery.of(context).size.width * 0.055,
                                                                        ),
                                                                      ))
                                                                : Padding(
                                                                    padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.018),
                                                                    child: SvgPicture.asset(
                                                                      'images/' + _selectedCryptoERC20SecondName.toLowerCase().replaceAll(' ', '') + '.svg',
                                                                      alignment: Alignment.center,
                                                                      width: MediaQuery.of(context).size.width * 0.055,
                                                                      height: MediaQuery.of(context).size.width * 0.055,
                                                                    ),
                                                                  ),
                                                            SizedBox(
                                                              width: MediaQuery.of(context).size.width * 0.02,
                                                            ),
                                                            new Text(
                                                              _selectedCryptoERC20SecondSymbol == "SATTBEP20" ? "SATT" : _selectedCryptoERC20SecondSymbol,
                                                              style: GoogleFonts.poppins(color: Colors.black, fontWeight: FontWeight.w600, fontSize: MediaQuery.of(context).size.height * 0.0165),
                                                            ),
                                                            const Spacer(),
                                                            Padding(
                                                              padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.05),
                                                              child: SizedBox(
                                                                width: 10,
                                                                child: Transform.rotate(
                                                                    angle: -math.pi / 2,
                                                                    child: Icon(
                                                                      Icons.arrow_back_ios,
                                                                      color: Colors.grey,
                                                                      size: MediaQuery.of(context).size.width * 0.06,
                                                                    )),
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                              ),
                                            )
                                          : (_selectedSecondPaymentMethod == "bep20"
                                              ?

                                              /**           FIAT METHOD && NETWORK SELECTED BEP20 ----- CRYPTO LIST      */
                                              InkWell(
                                                  onTap: () {
                                                    setState(() {
                                                      _clickedCryptoBEP20FiatContainer = !_clickedCryptoBEP20FiatContainer;
                                                      _clickedValueFiat = false;
                                                      FocusScope.of(context).requestFocus(FocusNode());
                                                    });
                                                  },
                                                  child: Container(
                                                    height: _clickedCryptoBEP20FiatContainer ? MediaQuery.of(context).size.height * 0.15 : MediaQuery.of(context).size.height * 0.065,
                                                    width: MediaQuery.of(context).size.width * 0.45,
                                                    decoration: BoxDecoration(
                                                      border: Border.all(color: Color(0xFFD6D6E8)),
                                                      borderRadius: BorderRadius.circular(30),
                                                      color: Colors.white,
                                                    ),
                                                    child: _clickedCryptoBEP20FiatContainer
                                                        ? Padding(
                                                            padding: EdgeInsets.only(
                                                              bottom: MediaQuery.of(context).size.height * 0.015,
                                                              right: MediaQuery.of(context).size.height * 0.015,
                                                              top: MediaQuery.of(context).size.height * 0.015,
                                                            ),
                                                            child: Theme(
                                                              data: scrolltheme,
                                                              child: Scrollbar(
                                                                child: ListView.builder(
                                                                  scrollDirection: Axis.vertical,
                                                                  shrinkWrap: true,
                                                                  itemCount: _secondCryptoListFiatBEP20.length,
                                                                  itemBuilder: (context, index) {
                                                                    return Padding(
                                                                      padding: EdgeInsets.only(
                                                                        top: MediaQuery.of(context).size.height * 0.015,
                                                                        left: MediaQuery.of(context).size.width * 0.015,
                                                                      ),
                                                                      child: InkWell(
                                                                        onTap: () async {
                                                                          setState(() {
                                                                            setCryptoInfoBep20(_secondCryptoListFiatBEP20[index]);
                                                                            setFieldSelection("50");
                                                                          });
                                                                          await handleGetQuotes(handleSelectedCrypto());
                                                                        },
                                                                        child: Row(
                                                                          children: [
                                                                            _secondCryptoListFiatBEP20[index].addedToken == true
                                                                                ? (_secondCryptoListFiatBEP20[index].picUrl != "false" && _secondCryptoListFiatBEP20[index].picUrl != "null"
                                                                                    ? Padding(
                                                                                        padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.018),
                                                                                        child: Image.network(
                                                                                          _secondCryptoListFiatBEP20[index].picUrl ?? "",
                                                                                          width: MediaQuery.of(context).size.width * 0.055,
                                                                                          height: MediaQuery.of(context).size.width * 0.055,
                                                                                        ),
                                                                                      )
                                                                                    : Padding(
                                                                                        padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.018),
                                                                                        child: SvgPicture.asset(
                                                                                          'images/indispo.svg',
                                                                                          width: MediaQuery.of(context).size.width * 0.055,
                                                                                          height: MediaQuery.of(context).size.width * 0.055,
                                                                                        ),
                                                                                      ))
                                                                                : Padding(
                                                                                    padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.018),
                                                                                    child: SvgPicture.asset(
                                                                                      'images/' + _secondCryptoListFiatBEP20[index].name.toString().toLowerCase().replaceAll(' ', '') + '.svg',
                                                                                      alignment: Alignment.center,
                                                                                      width: MediaQuery.of(context).size.width * 0.055,
                                                                                      height: MediaQuery.of(context).size.width * 0.055,
                                                                                    ),
                                                                                  ),
                                                                            SizedBox(
                                                                              width: MediaQuery.of(context).size.width * 0.02,
                                                                            ),
                                                                            new Text(
                                                                              _secondCryptoListFiatBEP20[index].symbol.toString().toUpperCase() == "SATTBEP20" ? "SATT" : _secondCryptoListFiatBEP20[index].symbol.toString().toUpperCase(),
                                                                              style: GoogleFonts.poppins(color: Colors.black, fontWeight: FontWeight.w600, fontSize: MediaQuery.of(context).size.height * 0.0165),
                                                                            ),
                                                                          ],
                                                                        ),
                                                                      ),
                                                                    );
                                                                  },
                                                                ),
                                                              ),
                                                            ),
                                                          )
                                                        : Padding(
                                                            padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.015),
                                                            child: Row(
                                                              children: [
                                                                _selectedCryptoBEP20FiatAddedToken == true
                                                                    ? (_selectedCryptoBEP20FiatPicUrl != "false" && _selectedCryptoBEP20FiatPicUrl != "null"
                                                                        ? Padding(
                                                                            padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.018),
                                                                            child: Image.network(
                                                                              _selectedCryptoBEP20FiatPicUrl,
                                                                              width: MediaQuery.of(context).size.width * 0.055,
                                                                              height: MediaQuery.of(context).size.width * 0.055,
                                                                            ),
                                                                          )
                                                                        : Padding(
                                                                            padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.018),
                                                                            child: SvgPicture.asset(
                                                                              'images/indispo.svg',
                                                                              width: MediaQuery.of(context).size.width * 0.055,
                                                                              height: MediaQuery.of(context).size.width * 0.055,
                                                                            ),
                                                                          ))
                                                                    : Padding(
                                                                        padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.018),
                                                                        child: SvgPicture.asset(
                                                                          'images/' + _selectedCryptoBEP20FiatName.toLowerCase().replaceAll(' ', '') + '.svg',
                                                                          alignment: Alignment.center,
                                                                          width: MediaQuery.of(context).size.width * 0.055,
                                                                          height: MediaQuery.of(context).size.width * 0.055,
                                                                        ),
                                                                      ),
                                                                SizedBox(
                                                                  width: MediaQuery.of(context).size.width * 0.02,
                                                                ),
                                                                new Text(
                                                                  _selectedCryptoBEP20FiatSymbol == "SATTBEP20" ? "SATT" : _selectedCryptoBEP20FiatSymbol,
                                                                  style: GoogleFonts.poppins(color: Colors.black, fontWeight: FontWeight.w600, fontSize: MediaQuery.of(context).size.height * 0.0165),
                                                                ),
                                                                const Spacer(),
                                                                Padding(
                                                                  padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.05),
                                                                  child: SizedBox(
                                                                    width: 10,
                                                                    child: Transform.rotate(
                                                                        angle: -math.pi / 2,
                                                                        child: Icon(
                                                                          Icons.arrow_back_ios,
                                                                          color: Colors.grey,
                                                                          size: MediaQuery.of(context).size.width * 0.06,
                                                                        )),
                                                                  ),
                                                                ),
                                                              ],
                                                            ),
                                                          ),
                                                  ),
                                                )
                                              : (_selectedSecondPaymentMethod == "erc20"
                                                  ? InkWell(
                                                      onTap: () {
                                                        setState(() {
                                                          _clickedCryptoERC20FiatContainer = !_clickedCryptoERC20FiatContainer;
                                                          _clickedValueFiat = false;
                                                          FocusScope.of(context).requestFocus(FocusNode());
                                                        });
                                                      },
                                                      child: Container(
                                                        height: _clickedCryptoERC20FiatContainer ? MediaQuery.of(context).size.height * 0.15 : MediaQuery.of(context).size.height * 0.065,
                                                        width: MediaQuery.of(context).size.width * 0.45,
                                                        decoration: BoxDecoration(
                                                          border: Border.all(color: Color(0xFFD6D6E8)),
                                                          borderRadius: BorderRadius.circular(30),
                                                          color: Colors.white,
                                                        ),
                                                        child: _clickedCryptoERC20FiatContainer
                                                            ? Padding(
                                                                padding: EdgeInsets.only(
                                                                  bottom: MediaQuery.of(context).size.height * 0.015,
                                                                  right: MediaQuery.of(context).size.height * 0.015,
                                                                  top: MediaQuery.of(context).size.height * 0.015,
                                                                ),
                                                                child: Theme(
                                                                  data: scrolltheme,
                                                                  child: Scrollbar(
                                                                    child: ListView.builder(
                                                                      scrollDirection: Axis.vertical,
                                                                      shrinkWrap: true,
                                                                      itemCount: _secondCryptoListFiatERC20.length,
                                                                      itemBuilder: (context, index) {
                                                                        return Padding(
                                                                          padding: EdgeInsets.only(
                                                                            top: MediaQuery.of(context).size.height * 0.015,
                                                                            left: MediaQuery.of(context).size.width * 0.015,
                                                                          ),
                                                                          child: InkWell(
                                                                            onTap: () async {
                                                                              setState(() {
                                                                                setCryptoInfoErc20(_secondCryptoListFiatERC20[index]);
                                                                                setFieldSelection("50");
                                                                              });
                                                                              await handleGetQuotes(handleSelectedCrypto());
                                                                            },
                                                                            child: Row(
                                                                              children: [
                                                                                _secondCryptoListFiatERC20[index].addedToken == true
                                                                                    ? (_secondCryptoListFiatERC20[index].picUrl != "false" && _secondCryptoListFiatERC20[index].picUrl != "null"
                                                                                        ? Padding(
                                                                                            padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.018),
                                                                                            child: Image.network(
                                                                                              _secondCryptoListFiatERC20[index].picUrl ?? "",
                                                                                              width: MediaQuery.of(context).size.width * 0.055,
                                                                                              height: MediaQuery.of(context).size.width * 0.055,
                                                                                            ),
                                                                                          )
                                                                                        : Padding(
                                                                                            padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.018),
                                                                                            child: SvgPicture.asset(
                                                                                              'images/indispo.svg',
                                                                                              width: MediaQuery.of(context).size.width * 0.055,
                                                                                              height: MediaQuery.of(context).size.width * 0.055,
                                                                                            ),
                                                                                          ))
                                                                                    : Padding(
                                                                                        padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.018),
                                                                                        child: SvgPicture.asset(
                                                                                          'images/' + _secondCryptoListFiatERC20[index].name.toString().toLowerCase().replaceAll(' ', '') + '.svg',
                                                                                          alignment: Alignment.center,
                                                                                          width: MediaQuery.of(context).size.width * 0.055,
                                                                                          height: MediaQuery.of(context).size.width * 0.055,
                                                                                        ),
                                                                                      ),
                                                                                SizedBox(
                                                                                  width: MediaQuery.of(context).size.width * 0.02,
                                                                                ),
                                                                                new Text(
                                                                                  _secondCryptoListFiatERC20[index].symbol.toString().toUpperCase() == "SATTBEP20" ? "SATT" : _secondCryptoListFiatERC20[index].symbol.toString().toUpperCase(),
                                                                                  style: GoogleFonts.poppins(color: Colors.black, fontWeight: FontWeight.w600, fontSize: MediaQuery.of(context).size.height * 0.0165),
                                                                                ),
                                                                              ],
                                                                            ),
                                                                          ),
                                                                        );
                                                                      },
                                                                    ),
                                                                  ),
                                                                ),
                                                              )
                                                            : Padding(
                                                                padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.015),
                                                                child: Row(
                                                                  children: [
                                                                    _selectedCryptoERC20FiatAddedToken == true
                                                                        ? (_selectedCryptoERC20FiatPicUrl != "false" && _selectedCryptoERC20FiatPicUrl != "null"
                                                                            ? Padding(
                                                                                padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.018),
                                                                                child: Image.network(
                                                                                  _selectedCryptoERC20FiatPicUrl,
                                                                                  width: MediaQuery.of(context).size.width * 0.055,
                                                                                  height: MediaQuery.of(context).size.width * 0.055,
                                                                                ),
                                                                              )
                                                                            : Padding(
                                                                                padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.018),
                                                                                child: SvgPicture.asset(
                                                                                  'images/indispo.svg',
                                                                                  width: MediaQuery.of(context).size.width * 0.055,
                                                                                  height: MediaQuery.of(context).size.width * 0.055,
                                                                                ),
                                                                              ))
                                                                        : Padding(
                                                                            padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.018),
                                                                            child: SvgPicture.asset(
                                                                              'images/' + _selectedCryptoERC20FiatName.toLowerCase().replaceAll(' ', '') + '.svg',
                                                                              alignment: Alignment.center,
                                                                              width: MediaQuery.of(context).size.width * 0.055,
                                                                              height: MediaQuery.of(context).size.width * 0.055,
                                                                            ),
                                                                          ),
                                                                    SizedBox(
                                                                      width: MediaQuery.of(context).size.width * 0.02,
                                                                    ),
                                                                    new Text(
                                                                      _selectedCryptoERC20FiatSymbol == "SATTBEP20" ? "SATT" : _selectedCryptoERC20FiatSymbol,
                                                                      style: GoogleFonts.poppins(color: Colors.black, fontWeight: FontWeight.w600, fontSize: MediaQuery.of(context).size.height * 0.0165),
                                                                    ),
                                                                    const Spacer(),
                                                                    Padding(
                                                                      padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.05),
                                                                      child: SizedBox(
                                                                        width: 10,
                                                                        child: Transform.rotate(
                                                                            angle: -math.pi / 2,
                                                                            child: Icon(
                                                                              Icons.arrow_back_ios,
                                                                              color: Colors.grey,
                                                                              size: MediaQuery.of(context).size.width * 0.06,
                                                                            )),
                                                                      ),
                                                                    ),
                                                                  ],
                                                                ),
                                                              ),
                                                      ),
                                                    )
                                                  : (_selectedSecondPaymentMethod == "polygon"
                                                      ?
                                                      /** POLYGON LIST */
                                                      InkWell(
                                                          onTap: () {
                                                            setState(() {
                                                              _clickedCryptoERC20FiatContainer = !_clickedCryptoERC20FiatContainer;
                                                              _clickedValueFiat = false;
                                                              FocusScope.of(context).requestFocus(FocusNode());
                                                            });
                                                          },
                                                          child: Container(
                                                            height: _clickedCryptoERC20FiatContainer ? MediaQuery.of(context).size.height * 0.15 : MediaQuery.of(context).size.height * 0.065,
                                                            width: MediaQuery.of(context).size.width * 0.45,
                                                            decoration: BoxDecoration(
                                                              border: Border.all(color: Color(0xFFD6D6E8)),
                                                              borderRadius: BorderRadius.circular(30),
                                                              color: Colors.white,
                                                            ),
                                                            child: _clickedCryptoERC20FiatContainer
                                                                ? Padding(
                                                                    padding: EdgeInsets.only(
                                                                      bottom: MediaQuery.of(context).size.height * 0.015,
                                                                      right: MediaQuery.of(context).size.height * 0.015,
                                                                      top: MediaQuery.of(context).size.height * 0.015,
                                                                    ),
                                                                    child: Theme(
                                                                      data: scrolltheme,
                                                                      child: Scrollbar(
                                                                        child: ListView.builder(
                                                                          scrollDirection: Axis.vertical,
                                                                          shrinkWrap: true,
                                                                          itemCount: _secondCryptoListFiatPOLYGON.length,
                                                                          itemBuilder: (context, index) {
                                                                            return Padding(
                                                                              padding: EdgeInsets.only(
                                                                                top: MediaQuery.of(context).size.height * 0.015,
                                                                                left: MediaQuery.of(context).size.width * 0.015,
                                                                              ),
                                                                              child: InkWell(
                                                                                onTap: () async {
                                                                                  setState(() {
                                                                                    _clickedCryptoERC20FiatContainer = false;
                                                                                    setCryptoInfoPoly(_secondCryptoListFiatPOLYGON[index]);
                                                                                    setFieldSelection("50");
                                                                                  });
                                                                                  await handleGetQuotes(handleSelectedCrypto());
                                                                                },
                                                                                child: Row(
                                                                                  children: [
                                                                                    _secondCryptoListFiatPOLYGON[index].addedToken == true
                                                                                        ? (_secondCryptoListFiatPOLYGON[index].picUrl != "false" && _secondCryptoListFiatPOLYGON[index].picUrl != "null"
                                                                                            ? Padding(
                                                                                                padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.018),
                                                                                                child: Image.network(
                                                                                                  _secondCryptoListFiatPOLYGON[index].picUrl ?? "",
                                                                                                  width: MediaQuery.of(context).size.width * 0.055,
                                                                                                  height: MediaQuery.of(context).size.width * 0.055,
                                                                                                ),
                                                                                              )
                                                                                            : Padding(
                                                                                                padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.018),
                                                                                                child: SvgPicture.asset(
                                                                                                  'images/indispo.svg',
                                                                                                  width: MediaQuery.of(context).size.width * 0.055,
                                                                                                  height: MediaQuery.of(context).size.width * 0.055,
                                                                                                ),
                                                                                              ))
                                                                                        : Padding(
                                                                                            padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.018),
                                                                                            child: SvgPicture.asset(
                                                                                              'images/' + _secondCryptoListFiatPOLYGON[index].name.toString().toLowerCase().replaceAll(' ', '') + '.svg',
                                                                                              alignment: Alignment.center,
                                                                                              width: MediaQuery.of(context).size.width * 0.055,
                                                                                              height: MediaQuery.of(context).size.width * 0.055,
                                                                                            ),
                                                                                          ),
                                                                                    SizedBox(
                                                                                      width: MediaQuery.of(context).size.width * 0.02,
                                                                                    ),
                                                                                    new Text(
                                                                                      _secondCryptoListFiatPOLYGON[index].symbol.toString().toUpperCase(),
                                                                                      style: GoogleFonts.poppins(color: Colors.black, fontWeight: FontWeight.w600, fontSize: MediaQuery.of(context).size.height * 0.0165),
                                                                                    ),
                                                                                  ],
                                                                                ),
                                                                              ),
                                                                            );
                                                                          },
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  )
                                                                : Padding(
                                                                    padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.015),
                                                                    child: Row(
                                                                      children: [
                                                                        _selectedCryptoPOLYGONFiatAddedToken == true
                                                                            ? (_selectedCryptoPOLYGONFiatPicUrl != "false" && _selectedCryptoPOLYGONFiatPicUrl != "null"
                                                                                ? Padding(
                                                                                    padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.018),
                                                                                    child: Image.network(
                                                                                      _selectedCryptoPOLYGONFiatPicUrl,
                                                                                      width: MediaQuery.of(context).size.width * 0.055,
                                                                                      height: MediaQuery.of(context).size.width * 0.055,
                                                                                    ),
                                                                                  )
                                                                                : Padding(
                                                                                    padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.018),
                                                                                    child: SvgPicture.asset(
                                                                                      'images/indispo.svg',
                                                                                      width: MediaQuery.of(context).size.width * 0.055,
                                                                                      height: MediaQuery.of(context).size.width * 0.055,
                                                                                    ),
                                                                                  ))
                                                                            : Padding(
                                                                                padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.018),
                                                                                child: SvgPicture.asset(
                                                                                  'images/' + _selectedCryptoPOLYGONFiatName.toLowerCase().replaceAll(' ', '') + '.svg',
                                                                                  alignment: Alignment.center,
                                                                                  width: MediaQuery.of(context).size.width * 0.055,
                                                                                  height: MediaQuery.of(context).size.width * 0.055,
                                                                                ),
                                                                              ),
                                                                        SizedBox(
                                                                          width: MediaQuery.of(context).size.width * 0.02,
                                                                        ),
                                                                        new Text(
                                                                          _selectedCryptoPOLYGONFiatSymbol,
                                                                          style: GoogleFonts.poppins(color: Colors.black, fontWeight: FontWeight.w600, fontSize: MediaQuery.of(context).size.height * 0.0165),
                                                                        ),
                                                                        const Spacer(),
                                                                        Padding(
                                                                          padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.05),
                                                                          child: SizedBox(
                                                                            width: 10,
                                                                            child: Transform.rotate(
                                                                                angle: -math.pi / 2,
                                                                                child: Icon(
                                                                                  Icons.arrow_back_ios,
                                                                                  color: Colors.grey,
                                                                                  size: MediaQuery.of(context).size.width * 0.06,
                                                                                )),
                                                                          ),
                                                                        ),
                                                                      ],
                                                                    ),
                                                                  ),
                                                          ),
                                                        )
                                                      : (_selectedSecondPaymentMethod == "bttc"
                                                          ? InkWell(
                                                              onTap: () {
                                                                setState(() {
                                                                  _clickedCryptoERC20FiatContainer = !_clickedCryptoERC20FiatContainer;
                                                                  _clickedValueFiat = false;
                                                                  FocusScope.of(context).requestFocus(FocusNode());
                                                                });
                                                              },
                                                              child: Container(
                                                                height: _clickedCryptoERC20FiatContainer ? MediaQuery.of(context).size.height * 0.15 : MediaQuery.of(context).size.height * 0.065,
                                                                width: MediaQuery.of(context).size.width * 0.45,
                                                                decoration: BoxDecoration(
                                                                  border: Border.all(color: Color(0xFFD6D6E8)),
                                                                  borderRadius: BorderRadius.circular(30),
                                                                  color: Colors.white,
                                                                ),
                                                                child: _clickedCryptoERC20FiatContainer
                                                                    ? Padding(
                                                                        padding: EdgeInsets.only(
                                                                          bottom: MediaQuery.of(context).size.height * 0.015,
                                                                          right: MediaQuery.of(context).size.height * 0.015,
                                                                          top: MediaQuery.of(context).size.height * 0.015,
                                                                        ),
                                                                        child: Theme(
                                                                          data: scrolltheme,
                                                                          child: Scrollbar(
                                                                            child: ListView.builder(
                                                                              scrollDirection: Axis.vertical,
                                                                              shrinkWrap: true,
                                                                              itemCount: _secondCryptoListFiatPOLYGON.length,
                                                                              itemBuilder: (context, index) {
                                                                                return Padding(
                                                                                  padding: EdgeInsets.only(
                                                                                    top: MediaQuery.of(context).size.height * 0.015,
                                                                                    left: MediaQuery.of(context).size.width * 0.015,
                                                                                  ),
                                                                                  child: InkWell(
                                                                                    onTap: () async {
                                                                                      setState(() {
                                                                                        _clickedCryptoERC20FiatContainer = false;
                                                                                        setCryptoInfoBTTC(_secondCryptoListFiatBTTC[index]);
                                                                                        setFieldSelection("50");
                                                                                      });
                                                                                      await handleGetQuotes(handleSelectedCrypto());
                                                                                    },
                                                                                    child: Row(
                                                                                      children: [
                                                                                        _secondCryptoListFiatBTTC[index].addedToken == true
                                                                                            ? (_secondCryptoListFiatBTTC[index].picUrl != "false" && _secondCryptoListFiatBTTC[index].picUrl != "null"
                                                                                                ? Padding(
                                                                                                    padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.018),
                                                                                                    child: Image.network(
                                                                                                      _secondCryptoListFiatBTTC[index].picUrl ?? "",
                                                                                                      width: MediaQuery.of(context).size.width * 0.055,
                                                                                                      height: MediaQuery.of(context).size.width * 0.055,
                                                                                                    ),
                                                                                                  )
                                                                                                : Padding(
                                                                                                    padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.018),
                                                                                                    child: SvgPicture.asset(
                                                                                                      'images/indispo.svg',
                                                                                                      width: MediaQuery.of(context).size.width * 0.055,
                                                                                                      height: MediaQuery.of(context).size.width * 0.055,
                                                                                                    ),
                                                                                                  ))
                                                                                            : Padding(
                                                                                                padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.018),
                                                                                                child: SvgPicture.asset(
                                                                                                  'images/' + _secondCryptoListFiatBTTC[index].name.toString().toLowerCase().replaceAll(' ', '') + '.svg',
                                                                                                  alignment: Alignment.center,
                                                                                                  width: MediaQuery.of(context).size.width * 0.055,
                                                                                                  height: MediaQuery.of(context).size.width * 0.055,
                                                                                                ),
                                                                                              ),
                                                                                        SizedBox(
                                                                                          width: MediaQuery.of(context).size.width * 0.02,
                                                                                        ),
                                                                                        new Text(
                                                                                          _secondCryptoListFiatBTTC[index].symbol.toString().toUpperCase(),
                                                                                          style: GoogleFonts.poppins(color: Colors.black, fontWeight: FontWeight.w600, fontSize: MediaQuery.of(context).size.height * 0.0165),
                                                                                        ),
                                                                                      ],
                                                                                    ),
                                                                                  ),
                                                                                );
                                                                              },
                                                                            ),
                                                                          ),
                                                                        ),
                                                                      )
                                                                    : Padding(
                                                                        padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.015),
                                                                        child: Row(
                                                                          children: [
                                                                            _selectedCryptoBTTCFiatAddedToken == true
                                                                                ? (_selectedCryptoBTTCFiatPicUrl != "false" && _selectedCryptoBTTCFiatPicUrl != "null"
                                                                                    ? Padding(
                                                                                        padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.018),
                                                                                        child: Image.network(
                                                                                          _selectedCryptoBTTCFiatPicUrl,
                                                                                          width: MediaQuery.of(context).size.width * 0.055,
                                                                                          height: MediaQuery.of(context).size.width * 0.055,
                                                                                        ),
                                                                                      )
                                                                                    : Padding(
                                                                                        padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.018),
                                                                                        child: SvgPicture.asset(
                                                                                          'images/indispo.svg',
                                                                                          width: MediaQuery.of(context).size.width * 0.055,
                                                                                          height: MediaQuery.of(context).size.width * 0.055,
                                                                                        ),
                                                                                      ))
                                                                                : Padding(
                                                                                    padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.018),
                                                                                    child: SvgPicture.asset(
                                                                                      'images/' + _selectedCryptoBTTCFiatName.toLowerCase().replaceAll(' ', '') + '.svg',
                                                                                      alignment: Alignment.center,
                                                                                      width: MediaQuery.of(context).size.width * 0.055,
                                                                                      height: MediaQuery.of(context).size.width * 0.055,
                                                                                    ),
                                                                                  ),
                                                                            SizedBox(
                                                                              width: MediaQuery.of(context).size.width * 0.02,
                                                                            ),
                                                                            new Text(
                                                                              _selectedCryptoBTTCFiatSymbol,
                                                                              style: GoogleFonts.poppins(color: Colors.black, fontWeight: FontWeight.w600, fontSize: MediaQuery.of(context).size.height * 0.0165),
                                                                            ),
                                                                            const Spacer(),
                                                                            Padding(
                                                                              padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.05),
                                                                              child: SizedBox(
                                                                                width: 10,
                                                                                child: Transform.rotate(
                                                                                    angle: -math.pi / 2,
                                                                                    child: Icon(
                                                                                      Icons.arrow_back_ios,
                                                                                      color: Colors.grey,
                                                                                      size: MediaQuery.of(context).size.width * 0.06,
                                                                                    )),
                                                                              ),
                                                                            ),
                                                                          ],
                                                                        ),
                                                                      ),
                                                              ),
                                                            )
                                                          : (_selectedSecondPaymentMethod == "tron"
                                                              ? InkWell(
                                                                  onTap: () {
                                                                    setState(() {
                                                                      _clickedCryptoERC20FiatContainer = !_clickedCryptoERC20FiatContainer;
                                                                      _clickedValueFiat = false;
                                                                      FocusScope.of(context).requestFocus(FocusNode());
                                                                    });
                                                                  },
                                                                  child: Container(
                                                                    height: _clickedCryptoERC20FiatContainer ? MediaQuery.of(context).size.height * 0.15 : MediaQuery.of(context).size.height * 0.065,
                                                                    width: MediaQuery.of(context).size.width * 0.45,
                                                                    decoration: BoxDecoration(
                                                                      border: Border.all(color: Color(0xFFD6D6E8)),
                                                                      borderRadius: BorderRadius.circular(30),
                                                                      color: Colors.white,
                                                                    ),
                                                                    child: _clickedCryptoERC20FiatContainer
                                                                        ? Padding(
                                                                            padding: EdgeInsets.only(
                                                                              bottom: MediaQuery.of(context).size.height * 0.015,
                                                                              right: MediaQuery.of(context).size.height * 0.015,
                                                                              top: MediaQuery.of(context).size.height * 0.015,
                                                                            ),
                                                                            child: Theme(
                                                                              data: scrolltheme,
                                                                              child: Scrollbar(
                                                                                child: ListView.builder(
                                                                                  scrollDirection: Axis.vertical,
                                                                                  shrinkWrap: true,
                                                                                  itemCount: _secondCryptoListFiatTRX.length,
                                                                                  itemBuilder: (context, index) {
                                                                                    return Padding(
                                                                                      padding: EdgeInsets.only(
                                                                                        top: MediaQuery.of(context).size.height * 0.015,
                                                                                        left: MediaQuery.of(context).size.width * 0.015,
                                                                                      ),
                                                                                      child: InkWell(
                                                                                        onTap: () async {
                                                                                          setState(() {
                                                                                            setCryptoInfoTRX(_secondCryptoListFiatTRX[index]);
                                                                                            setFieldSelection("50");
                                                                                          });
                                                                                          await handleGetQuotes(handleSelectedCrypto());
                                                                                        },
                                                                                        child: Row(
                                                                                          children: [
                                                                                            _secondCryptoListFiatTRX[index].addedToken == true
                                                                                                ? (_secondCryptoListFiatTRX[index].picUrl != "false" && _secondCryptoListFiatTRX[index].picUrl != "null"
                                                                                                    ? Padding(
                                                                                                        padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.018),
                                                                                                        child: Image.network(
                                                                                                          _secondCryptoListFiatTRX[index].picUrl ?? "",
                                                                                                          width: MediaQuery.of(context).size.width * 0.055,
                                                                                                          height: MediaQuery.of(context).size.width * 0.055,
                                                                                                        ),
                                                                                                      )
                                                                                                    : Padding(
                                                                                                        padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.018),
                                                                                                        child: SvgPicture.asset(
                                                                                                          'images/indispo.svg',
                                                                                                          width: MediaQuery.of(context).size.width * 0.055,
                                                                                                          height: MediaQuery.of(context).size.width * 0.055,
                                                                                                        ),
                                                                                                      ))
                                                                                                : Padding(
                                                                                                    padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.018),
                                                                                                    child: SvgPicture.asset(
                                                                                                      'images/' + _secondCryptoListFiatTRX[index].name.toString().toLowerCase().replaceAll(' ', '') + '.svg',
                                                                                                      alignment: Alignment.center,
                                                                                                      width: MediaQuery.of(context).size.width * 0.055,
                                                                                                      height: MediaQuery.of(context).size.width * 0.055,
                                                                                                    ),
                                                                                                  ),
                                                                                            SizedBox(
                                                                                              width: MediaQuery.of(context).size.width * 0.02,
                                                                                            ),
                                                                                            new Text(
                                                                                              _secondCryptoListFiatTRX[index].symbol.toString().toUpperCase(),
                                                                                              style: GoogleFonts.poppins(color: Colors.black, fontWeight: FontWeight.w600, fontSize: MediaQuery.of(context).size.height * 0.0165),
                                                                                            ),
                                                                                          ],
                                                                                        ),
                                                                                      ),
                                                                                    );
                                                                                  },
                                                                                ),
                                                                              ),
                                                                            ),
                                                                          )
                                                                        : Padding(
                                                                            padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.015),
                                                                            child: Row(
                                                                              children: [
                                                                                _selectedCryptoTRXFiatAddedToken == true
                                                                                    ? (_selectedCryptoTRXFiatPicUrl != "false" && _selectedCryptoTRXFiatPicUrl != "null"
                                                                                        ? Padding(
                                                                                            padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.018),
                                                                                            child: Image.network(
                                                                                              _selectedCryptoTRXFiatPicUrl,
                                                                                              width: MediaQuery.of(context).size.width * 0.055,
                                                                                              height: MediaQuery.of(context).size.width * 0.055,
                                                                                            ),
                                                                                          )
                                                                                        : Padding(
                                                                                            padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.018),
                                                                                            child: SvgPicture.asset(
                                                                                              'images/indispo.svg',
                                                                                              width: MediaQuery.of(context).size.width * 0.055,
                                                                                              height: MediaQuery.of(context).size.width * 0.055,
                                                                                            ),
                                                                                          ))
                                                                                    : Padding(
                                                                                        padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.018),
                                                                                        child: SvgPicture.asset(
                                                                                          'images/' + _selectedCryptoTRXFiatName.toLowerCase().replaceAll(' ', '') + '.svg',
                                                                                          alignment: Alignment.center,
                                                                                          width: MediaQuery.of(context).size.width * 0.055,
                                                                                          height: MediaQuery.of(context).size.width * 0.055,
                                                                                        ),
                                                                                      ),
                                                                                SizedBox(
                                                                                  width: MediaQuery.of(context).size.width * 0.02,
                                                                                ),
                                                                                new Text(
                                                                                  _selectedCryptoTRXFiatSymbol,
                                                                                  style: GoogleFonts.poppins(color: Colors.black, fontWeight: FontWeight.w600, fontSize: MediaQuery.of(context).size.height * 0.0165),
                                                                                ),
                                                                                const Spacer(),
                                                                                Padding(
                                                                                  padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.05),
                                                                                  child: SizedBox(
                                                                                    width: 10,
                                                                                    child: Transform.rotate(
                                                                                        angle: -math.pi / 2,
                                                                                        child: Icon(
                                                                                          Icons.arrow_back_ios,
                                                                                          color: Colors.grey,
                                                                                          size: MediaQuery.of(context).size.width * 0.06,
                                                                                        )),
                                                                                  ),
                                                                                ),
                                                                              ],
                                                                            ),
                                                                          ),
                                                                  ),
                                                                )
                                                              : InkWell(
                                                                  onTap: () {
                                                                    setState(() {
                                                                      _clickedCryptoERC20FiatContainer = !_clickedCryptoERC20FiatContainer;
                                                                      _clickedValueFiat = false;
                                                                      FocusScope.of(context).requestFocus(FocusNode());
                                                                    });
                                                                  },
                                                                  child: Container(
                                                                    height: MediaQuery.of(context).size.height * 0.065,
                                                                    width: MediaQuery.of(context).size.width * 0.45,
                                                                    decoration: BoxDecoration(
                                                                      border: Border.all(color: Color(0xFFD6D6E8)),
                                                                      borderRadius: BorderRadius.circular(30),
                                                                      color: Colors.white,
                                                                    ),
                                                                    child: Padding(
                                                                      padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.015),
                                                                      child: Row(
                                                                        children: [
                                                                          Padding(
                                                                            padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.018),
                                                                            child: SvgPicture.asset(
                                                                              'images/bitcoin.svg',
                                                                              alignment: Alignment.center,
                                                                              width: MediaQuery.of(context).size.width * 0.055,
                                                                              height: MediaQuery.of(context).size.width * 0.055,
                                                                            ),
                                                                          ),
                                                                          SizedBox(
                                                                            width: MediaQuery.of(context).size.width * 0.02,
                                                                          ),
                                                                          new Text(
                                                                            "BTC",
                                                                            style: GoogleFonts.poppins(color: Colors.black, fontWeight: FontWeight.w600, fontSize: MediaQuery.of(context).size.height * 0.0165),
                                                                          ),
                                                                          const Spacer(),
                                                                          Padding(
                                                                            padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.05),
                                                                            child: SizedBox(
                                                                              width: 10,
                                                                              child: Transform.rotate(
                                                                                  angle: -math.pi / 2,
                                                                                  child: Icon(
                                                                                    Icons.arrow_back_ios,
                                                                                    color: Colors.grey,
                                                                                    size: MediaQuery.of(context).size.width * 0.06,
                                                                                  )),
                                                                            ),
                                                                          ),
                                                                        ],
                                                                      ),
                                                                    ),
                                                                  ),
                                                                ))))))))),
                          HeaderNavigation(_isVisible, _isWalletVisible, _isNotificationVisible),
                        ],
                      )
                    : ((_secondStepBuy && !_firstStepBuy)
                        ? Stack(
                            children: <Widget>[
                              ConstrainedBox(
                                constraints: BoxConstraints(maxHeight: MediaQuery.of(context).viewInsets.bottom != 0 ? MediaQuery.of(context).size.height : MediaQuery.of(context).size.height * 0.85),
                                child: Column(
                                  children: <Widget>[
                                    Container(
                                      height: MediaQuery.of(context).size.height * 0.14,
                                      child: Row(
                                        crossAxisAlignment: CrossAxisAlignment.end,
                                        children: <Widget>[
                                          Padding(
                                            padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.05, 0, 0, MediaQuery.of(context).size.height * 0.032),
                                            child: Text(
                                              "Buy",
                                              style: GoogleFonts.poppins(
                                                fontSize: MediaQuery.of(context).size.height * 0.035,
                                                fontWeight: FontWeight.w700,
                                                color: Colors.white,
                                              ),
                                            ),
                                          ),
                                          Spacer(),
                                          Padding(
                                            padding: EdgeInsets.fromLTRB(0, 0, MediaQuery.of(context).size.width * 0.037, MediaQuery.of(context).size.height * 0.021),
                                            child: InkWell(
                                              onTap: () {
                                                setState(() {
                                                  _firstStepBuy = true;
                                                  _secondStepBuy = false;
                                                });
                                              },
                                              child: Text(
                                                "< Back",
                                                style: GoogleFonts.poppins(color: Colors.white, letterSpacing: 1.2, fontWeight: FontWeight.w600, fontSize: MediaQuery.of(context).size.height * 0.022),
                                              ),
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                    Expanded(
                                      child: Container(
                                        width: MediaQuery.of(context).size.width,
                                        decoration: BoxDecoration(
                                            color: Colors.white,
                                            borderRadius: BorderRadius.only(
                                              topLeft: Radius.circular(MediaQuery.of(context).size.width * 0.065),
                                              topRight: Radius.circular(MediaQuery.of(context).size.width * 0.065),
                                            )),
                                        child: Column(
                                          children: <Widget>[
                                            Center(
                                              child: Padding(
                                                padding: EdgeInsets.fromLTRB(0, MediaQuery.of(context).size.height * 0.03, 0, 0),
                                                child: new SvgPicture.asset(
                                                  'images/simplex-success.svg',
                                                  height: MediaQuery.of(context).size.height * 0.11,
                                                  width: MediaQuery.of(context).size.height * 0.11,
                                                ),
                                              ),
                                            ),
                                            Padding(
                                              padding: EdgeInsets.fromLTRB(0, MediaQuery.of(context).size.height * 0.02, 0, 0),
                                              child: Text(
                                                "You are about to buy",
                                                style: GoogleFonts.poppins(color: Color(0xFF1F2337), fontWeight: FontWeight.w700, fontSize: MediaQuery.of(context).size.height * 0.023, letterSpacing: 1),
                                              ),
                                            ),
                                            Center(
                                              child: Padding(
                                                padding: EdgeInsets.fromLTRB(0, MediaQuery.of(context).size.height * 0.012, 0, 0),
                                                child: Text(
                                                  "${_cryptoBalance.text} ${_selectedFiat}",
                                                  style: GoogleFonts.poppins(
                                                    color: Color(0xFF1F2337),
                                                    fontWeight: FontWeight.w700,
                                                    fontSize: MediaQuery.of(context).size.height * 0.042,
                                                  ),
                                                ),
                                              ),
                                            ),
                                            Text(
                                              "≈ ${formatCryptoBalance(double.parse(simplexAmount), "")} ${simplexCurrency == "SATT-SC" || simplexCurrency == "SATT-ERC20" ? "SaTT" : simplexCurrency}",
                                              style: GoogleFonts.poppins(color: Color(0xFF75758F), fontWeight: FontWeight.w700, fontSize: MediaQuery.of(context).size.height * 0.033),
                                            ),
                                            Padding(
                                              padding: EdgeInsets.fromLTRB(0, MediaQuery.of(context).size.height * 0.13, 0, 0),
                                              child: !_isLoadingSimplex
                                                  ? SizedBox(
                                                      height: MediaQuery.of(context).size.height * 0.06,
                                                      width: MediaQuery.of(context).size.width * 0.8,
                                                      child: ElevatedButton(
                                                          style: ButtonStyle(
                                                            backgroundColor: MaterialStateProperty.all(Color(0xFFFFA318)),
                                                            shape: MaterialStateProperty.all(
                                                              RoundedRectangleBorder(
                                                                borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.08),
                                                              ),
                                                            ),
                                                          ),
                                                          onPressed: () async {
                                                            setState(() {
                                                              _isLoadingSimplex = true;
                                                            });
                                                            var userLegalResult = await LoginController.apiService.getUserLegal(Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "");
                                                            if (userLegalResult != "error" || userLegalResult != "Invalid Access Token") {
                                                              List legal = userLegalResult;
                                                              if (legal.length == 2 && legal[0]["validate"] == true && legal[0]["validate"] == true) {
                                                                var result = "test";
                                                                if (result != null && result != "") {
                                                                  _simplexForm = result;
                                                                  setState(() {
                                                                    _simplexForm = result;
                                                                    _firstStepBuy = false;
                                                                    _secondStepBuy = false;
                                                                    _formSimplex = true;
                                                                    _isLoadingSimplex = false;
                                                                  });
                                                                } else {
                                                                  setState(() {
                                                                    _isLoadingSimplex = false;
                                                                  });
                                                                  Fluttertoast.showToast(msg: "Something went wrong. Please try again.");
                                                                }
                                                              } else {
                                                                showDialog(
                                                                    context: context,
                                                                    barrierDismissible: false,
                                                                    builder: (BuildContext context) {
                                                                      return StatefulBuilder(builder: (context, setState) {
                                                                        return Dialog(
                                                                          backgroundColor: Colors.transparent,
                                                                          insetPadding: EdgeInsets.zero,
                                                                          child: Container(
                                                                            height: MediaQuery.of(context).size.height,
                                                                            width: MediaQuery.of(context).size.width,
                                                                            decoration: BoxDecoration(
                                                                              color: Colors.white.withOpacity(0.8),
                                                                            ),
                                                                            child: Column(
                                                                              children: <Widget>[
                                                                                Row(
                                                                                  crossAxisAlignment: CrossAxisAlignment.center,
                                                                                  mainAxisAlignment: MainAxisAlignment.start,
                                                                                  children: [
                                                                                    Spacer(),
                                                                                    Spacer(),
                                                                                    Padding(
                                                                                      padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.01, top: MediaQuery.of(context).size.width * 0.06),
                                                                                      child: IconButton(
                                                                                        alignment: Alignment.topLeft,
                                                                                        onPressed: () {
                                                                                          Navigator.of(context).pop();
                                                                                        },
                                                                                        splashColor: Colors.transparent,
                                                                                        highlightColor: Colors.transparent,
                                                                                        icon: Icon(
                                                                                          Icons.close,
                                                                                          size: MediaQuery.of(context).size.width * 0.05,
                                                                                          color: Colors.black.withOpacity(0.5),
                                                                                        ),
                                                                                      ),
                                                                                    ),
                                                                                  ],
                                                                                ),
                                                                                Padding(
                                                                                  padding: EdgeInsets.only(
                                                                                    top: MediaQuery.of(context).size.height * 0.2,
                                                                                  ),
                                                                                  child: SvgPicture.asset("images/KYC.svg"),
                                                                                ),
                                                                                Padding(
                                                                                  padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.03, bottom: MediaQuery.of(context).size.height * 0.023),
                                                                                  child: Text(
                                                                                    "Please verify your account",
                                                                                    style: GoogleFonts.poppins(
                                                                                      fontWeight: FontWeight.w700,
                                                                                      fontStyle: FontStyle.normal,
                                                                                      color: Color(0XFF1F2337),
                                                                                      fontSize: MediaQuery.of(context).size.height * 0.025,
                                                                                    ),
                                                                                  ),
                                                                                ),
                                                                                Text(
                                                                                  "To perform this transaction",
                                                                                  style: GoogleFonts.poppins(color: Color(0XFF47526A), fontSize: MediaQuery.of(context).size.height * 0.017, fontWeight: FontWeight.w500),
                                                                                ),
                                                                                Text(
                                                                                  "Proceed to KYC page",
                                                                                  style: GoogleFonts.poppins(color: Color(0XFF47526A), fontSize: MediaQuery.of(context).size.height * 0.017, fontWeight: FontWeight.w500),
                                                                                ),
                                                                                Padding(
                                                                                  padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.012),
                                                                                  child: SizedBox(
                                                                                    width: MediaQuery.of(context).size.width * 0.4,
                                                                                    child: ElevatedButton(
                                                                                      child: Text(
                                                                                        "KYC",
                                                                                        style: GoogleFonts.poppins(fontWeight: FontWeight.w900, fontSize: MediaQuery.of(context).size.height * 0.017),
                                                                                      ),
                                                                                      onPressed: () {
                                                                                        gotoPageGlobal(context, KycPage());
                                                                                      },
                                                                                      style: ButtonStyle(
                                                                                        backgroundColor: MaterialStateProperty.all(
                                                                                          Color(0xFF4048FF),
                                                                                        ),
                                                                                        shape: MaterialStateProperty.all(
                                                                                          RoundedRectangleBorder(
                                                                                            borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.08),
                                                                                          ),
                                                                                        ),
                                                                                      ),
                                                                                    ),
                                                                                  ),
                                                                                ),
                                                                              ],
                                                                            ),
                                                                          ),
                                                                        );
                                                                      });
                                                                    });
                                                                setState(() {
                                                                  _isLoadingSimplex = false;
                                                                });
                                                              }
                                                            } else {
                                                              displayDialog(context, "Error", "Something went wrong, please try again");
                                                            }
                                                          },
                                                          child: Center(
                                                            child: Text(
                                                              "Continue with Simplex",
                                                              style: GoogleFonts.poppins(
                                                                fontSize: MediaQuery.of(context).size.height * 0.02,
                                                                fontWeight: FontWeight.w600,
                                                              ),
                                                            ),
                                                          )),
                                                    )
                                                  : Container(
                                                      height: MediaQuery.of(context).size.height * 0.06,
                                                      width: MediaQuery.of(context).size.width * 0.8,
                                                      decoration: BoxDecoration(
                                                        borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.05),
                                                        color: Color(0xFFF6F6FF),
                                                      ),
                                                      child: Center(
                                                        child: SizedBox(
                                                          height: MediaQuery.of(context).size.width * 0.032,
                                                          width: MediaQuery.of(context).size.width * 0.032,
                                                          child: CircularProgressIndicator(
                                                            color: Color(0xFFADADC8),
                                                          ),
                                                        ),
                                                      )),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              HeaderNavigation(_isVisible, _isWalletVisible, _isNotificationVisible),
                            ],
                          )
                        : ((_formSimplex && !_firstStepBuy && !_secondStepBuy)
                            ? Stack(
                                children: [
                                  ConstrainedBox(
                                    constraints: BoxConstraints(maxHeight: MediaQuery.of(context).viewInsets.bottom != 0 ? MediaQuery.of(context).size.height : MediaQuery.of(context).size.height * 0.85),
                                    child: InAppWebView(
                                      initialUrlRequest: URLRequest(
                                          url: Uri.https("sandbox.test-simplexcc.com", "/payments/new"),
                                          method: 'POST',
                                          body: Uint8List.fromList(utf8.encode("version=1&partner=satt&payment_flow_type=wallet&return_url_success=https://testnet.satt.atayen.us/&return_url_fail=https://dev.satt.atayen.us/&payment_id=${_paymentId}")),
                                          headers: {'Content-Type': 'application/x-www-form-urlencoded'}),
                                      onWebViewCreated: (controller) {},
                                    ),
                                  ),
                                  HeaderNavigation(_isVisible, _isWalletVisible, _isNotificationVisible),
                                ],
                              )
                            : null)),
              ),
            ),
          ));
    } else {
      return Center(
          child: CircularProgressIndicator(
        color: Colors.white,
      ));
    }
  }

  setDollarBalance(String selectedCrypto, String value) {
    var price = _cryptoList.firstWhere((e) => e.symbol == selectedCrypto).price ?? 0;
    value = value == "" ? "0" : value;
    var amount = (price * double.parse(value));
    return formatDollarBalance(amount);
  }

  setCryptoBalance(String selectedCrypto, String value) {
    var price = _cryptoList.firstWhere((e) => e.symbol == selectedCrypto).price ?? 0;
    value = value == "" ? "0" : value;
    var amount = double.parse(value) / price;
    return formatCryptoBalance(amount, "");
  }

  Future onChangedInput(String value) async {
    try {
      setState(() {
        _userInputOnChanged = true;
      });
      value = value.replaceAll(new RegExp("[^\\d.]"), "");
      setState(() {
        _cryptoBalance.text = value;
      });

      _cryptoBalance.selection = TextSelection.fromPosition(TextPosition(offset: _cryptoBalance.text.length));
      if (_selectedPaymentMethod != "fiat") {
        switch (_selectedPaymentMethod) {
          case "bep20":
            if (value.length > 0) {
              calculatePriceBEP20();
            } else {
              setState(() {
                approximationValueBep20 = 0;
                approximationValueErc20 = 0;
                _dollarBalance.text = "0";
                simplexAmount = "0";
              });
            }

            break;
          case "erc20":
            if (value.length > 0) {
              calculatePriceERC20();
            } else {
              setState(() {
                approximationValueErc20 = 0;
                approximationValueBep20 = 0;
                _dollarBalance.text = "0";
                simplexAmount = "0";
              });
            }
            break;
        }
      } else {
        setState(() {
          buttonPayCreditCard = false;
        });
        if (value.length > 0) {
          await handleGetQuotes(handleSelectedCrypto());
        } else {
          setState(() {
            approximationValueErc20 = 0;
            approximationValueBep20 = 0;
            _dollarBalance.text = "0";
            simplexAmount = "0";
          });
        }
      }
    } on FormatException {}
  }

  calculatePriceERC20() {
    var totalPrice = double.parse(_cryptoBalance.text) * (_cryptoListERC20.firstWhere((e) => e.symbol == _selectedCryptoERC20).price ?? 0);
    setState(() {
      approximationValueErc20 = totalPrice / (_secondCryptoListERC20.firstWhere((e) => e.symbol == _secondSelectedCryptoERC20).price ?? 0);
    });
  }

  calculatePriceBEP20() {
    var totalPrice = double.parse(_cryptoBalance.text) * (_cryptoListBEP20.firstWhere((e) => e.symbol == _selectedCryptoBEP20).price ?? 0);
    setState(() {
      approximationValueBep20 = totalPrice / (_secondCryptoListBEP20.firstWhere((e) => e.symbol == _secondSelectedCryptoBEP20).price ?? 0);
    });
  }

  void setFieldSelection(String balance) {
    setState(() {
      _clickedContainerPaymentMethod = false;
      _clickedSecondContainerPaymentMethod = false;
      _clickedCryptoBEP20FiatContainer = false;
      _clickedCryptoERC20FiatContainer = false;
      _clickedCryptoBEP20FirstContainer = false;
      _clickedCryptoBEP20SecondContainer = false;
      _clickedCryptoERC20FirstContainer = false;
      _clickedFiatContainer = false;
      _cryptoBalance.text = balance;
    });
  }

  void setCryptoInfoTRX(Crypto crypto) {
    _selectedCryptoTRXFiatAddedToken = crypto.addedToken ?? false;
    _selectedCryptoTRXFiatPicUrl = crypto.picUrl.toString();
    _selectedCryptoTRXFiatName = crypto.name.toString();
    _selectedCryptoTRXFiatSymbol = crypto.symbol.toString();
  }

  void setCryptoInfoBTTC(Crypto crypto) {
    _selectedCryptoBTTCFiatAddedToken = crypto.addedToken ?? false;
    _selectedCryptoBTTCFiatPicUrl = crypto.picUrl.toString();
    _selectedCryptoBTTCFiatName = crypto.name.toString();
    _selectedCryptoBTTCFiatSymbol = crypto.symbol.toString();
  }

  void setCryptoInfoPoly(Crypto crypto) {
    _selectedCryptoPOLYGONFiatAddedToken = crypto.addedToken ?? false;
    _selectedCryptoPOLYGONFiatPicUrl = crypto.picUrl.toString();
    _selectedCryptoPOLYGONFiatName = crypto.name.toString();
    _selectedCryptoPOLYGONFiatSymbol = crypto.symbol.toString();
  }

  void setCryptoInfoErc20(Crypto crypto) {
    _secondSelectedCryptoFiatErc20 = crypto.symbol.toString();
    _selectedCryptoERC20FiatAddedToken = crypto.addedToken ?? false;
    _selectedCryptoERC20FiatPicUrl = crypto.picUrl.toString();
    _selectedCryptoERC20FiatName = crypto.name.toString();
    _selectedCryptoERC20FiatSymbol = crypto.symbol.toString();
  }

  void setFirstFieldERC20(Crypto crypto) {
    _selectedCryptoERC20FirstAddedToken = crypto.addedToken ?? false;
    _selectedCryptoERC20FirstPicUrl = crypto.picUrl.toString();
    _selectedCryptoERC20FirstName = crypto.name.toString();
    _selectedCryptoERC20FirstSymbol = crypto.symbol.toString();
  }

  void setFirstFieldBEP20(Crypto crypto) {
    _selectedCryptoBEP20FirstAddedToken = crypto.addedToken ?? false;
    _selectedCryptoBEP20FirstPicUrl = crypto.picUrl.toString();
    _selectedCryptoBEP20FirstName = crypto.name.toString();
    _selectedCryptoBEP20FirstSymbol = crypto.symbol.toString();
  }

  void setCryptoInfoBep20(Crypto crypto) {
    _secondSelectedCryptoFiatBep20 = crypto.symbol.toString();
    _selectedCryptoBEP20FiatAddedToken = crypto.addedToken ?? false;
    _selectedCryptoBEP20FiatPicUrl = crypto.picUrl.toString();
    _selectedCryptoBEP20FiatName = crypto.name.toString();
    _selectedCryptoBEP20FiatSymbol = crypto.symbol.toString();
  }

  handleSelectedCrypto() {
    return _selectedSecondPaymentMethod == "bep20"
        ? (_secondSelectedCryptoFiatBep20 == "SATTBEP20" ? "SATT-SC" : _secondSelectedCryptoFiatBep20)
        : (_selectedSecondPaymentMethod == "btc"
            ? "BTC"
            : ((_selectedSecondPaymentMethod == "polygon"
                ? _selectedCryptoPOLYGONFiatSymbol
                : (_selectedSecondPaymentMethod == "bttc" ? _selectedCryptoBTTCFiatSymbol : (_selectedSecondPaymentMethod == "tron" ? _selectedCryptoTRXFiatSymbol : (_secondSelectedCryptoFiatErc20 == "SATT" ? "SATT-ERC20" : _secondSelectedCryptoFiatErc20))))));
  }

  setNetworkValues(String paymentMethod, String secondPaymentMethod) {
    _selectedPaymentMethod = paymentMethod;
    _selectedSecondPaymentMethod = secondPaymentMethod;
    _errorMessage = "";
    _errorTransactionExist = false;
    buttonPayCreditCard = false;
    _selectedCrypto = _cryptoList[0].symbol.toString();
    _selectedCryptoBEP20 = _cryptoListBEP20[0].symbol.toString();
    _selectedCryptoERC20 = _cryptoListERC20[0].symbol.toString();
    _secondSelectedCryptoBEP20 = _secondCryptoListBEP20[0].symbol.toString();
    _secondSelectedCryptoERC20 = _secondCryptoListERC20[0].symbol.toString();
    _secondSelectedCryptoFiatBep20 = _secondCryptoListFiatBEP20[0].symbol.toString();
    _secondSelectedCryptoFiatErc20 = _secondCryptoListFiatERC20[0].symbol.toString();
  }

  handleGetQuotes(String fiatSymbol) async {
    var result = await LoginController.apiService.getQuotes(Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "", fiatSymbol, _cryptoBalance.text, _selectedFiat, _selectedFiat);

    if (result["error"] != null) {
      setState(() {
        _errorTransactionExist = true;
        _errorMessage = result["error"];
        buttonPayCreditCard = false;
      });
    } else {
      _userId = result["data"]["user_id"];

      _quoteId = result["data"]["quote_id"];

      _baseAmount = result["data"]["fiat_money"]["base_amount"].toString();

      _currencySelected = result["data"]["fiat_money"]["currency"].toString();

      _walletId = result["data"]["wallet_id"];

      setState(() {
        _errorTransactionExist = false;
        _errorMessage = "";
        simplexAmount = result["data"]["digital_money"]["amount"].toString();
        simplexCurrency = result["data"]["digital_money"]["currency"];
        buttonPayCreditCard = true;
      });
    }
  }
}

class CustomMaxValueInputFormatter extends TextInputFormatter {
  final double? maxInputValue;

  CustomMaxValueInputFormatter({this.maxInputValue});

  @override
  TextEditingValue formatEditUpdate(TextEditingValue oldValue, TextEditingValue newValue) {
    final TextSelection newSelection = newValue.selection;
    String truncated = newValue.text;
    if (maxInputValue != null) {
      try {
        final double value = double.parse(newValue.text);
        if (value == null) {
          return TextEditingValue(
            text: truncated,
            selection: newSelection,
          );
        }
        if (value > double.parse(maxInputValue.toString())) {
          truncated = oldValue.text.toString();
        }
      } catch (e) {
        print(e);
      }
    }
    return TextEditingValue(
      text: truncated,
      selection: newSelection,
    );
  }
}

class DecimalTextInputFormatter extends TextInputFormatter {
  DecimalTextInputFormatter({this.decimalRange}) : assert(decimalRange == null || decimalRange > 0);

  final int? decimalRange;

  @override
  TextEditingValue formatEditUpdate(
    TextEditingValue oldValue,
    TextEditingValue newValue,
  ) {
    TextSelection newSelection = newValue.selection;
    String truncated = newValue.text;

    if (decimalRange != null) {
      String value = newValue.text;

      if (value.contains(".") && value.substring(value.indexOf(".") + 1).length > decimalRange!) {
        truncated = oldValue.text;
        newSelection = oldValue.selection;
      } else if (value == ".") {
        truncated = "0.";

        newSelection = newValue.selection.copyWith(
          baseOffset: math.min(truncated.length, truncated.length + 1),
          extentOffset: math.min(truncated.length, truncated.length + 1),
        );
      }
      return TextEditingValue(
        text: truncated,
        selection: newSelection,
        composing: TextRange.empty,
      );
    }
    return newValue;
  }
}