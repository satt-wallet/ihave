import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter_svg/svg.dart';

import 'package:google_fonts/google_fonts.dart';
import 'package:ihave/controllers/LoginController.dart';
import 'package:ihave/ui/signin.dart';
import 'package:ihave/util/utils.dart';
import 'package:provider/provider.dart';

import 'creatwallet.dart';

class TransactionPassword extends StatefulWidget {
  TransactionPassword({Key? key}) : super(key: key);

  @override
  _TransactionPasswordState createState() => _TransactionPasswordState();
}

class _TransactionPasswordState extends State<TransactionPassword> {
  bool _checkedValue1 = false;
  bool _checkedValue2 = false;
  bool _checkedValue3 = false;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async {
          //do nothing
          return false;
        },
        child: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
                colors: [
                  const Color(0xFFF52079),
                  const Color(0xFF1F2337),
                ],
                begin: const FractionalOffset(0.0, -0.1),
                end: const FractionalOffset(0.0, 0.3),
                stops: [0.0, 1.0],
                tileMode: TileMode.clamp),
          ),
          child: Scaffold(
              backgroundColor: Colors.transparent,
              resizeToAvoidBottomInset: false,
              appBar: null,
              body: SafeArea(
                child: SingleChildScrollView(
                  child: Container(
                    height: MediaQuery.of(context).size.height,
                    child: Column(
                      children: [
                        SizedBox(
                          height: MediaQuery.of(context).size.height * 0.02,
                        ),
                        Stack(
                          children: [
                            Align(
                              alignment: Alignment.topCenter,
                              child: SvgPicture.asset(
                                'images/logo-auth.svg',
                                alignment: Alignment.center,
                                width: MediaQuery.of(context).size.height * 0.035,
                                height: MediaQuery.of(context).size.height * 0.035,
                              ),
                            ),
                            Align(
                              alignment: Alignment.topRight,
                              child: Padding(
                                padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.02, top: MediaQuery.of(context).size.height * 0.0075),
                                child: InkWell(
                                  child: SvgPicture.asset(
                                    'images/new-logout.svg',
                                  ),
                                  onTap: () {
                                    Provider.of<LoginController>(context, listen: false).logout();
                                    Navigator.pushReplacement(context, MaterialPageRoute(builder: (_) => Signin()));
                                  },
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: MediaQuery.of(context).size.height * 0.04,
                        ),
                        Padding(
                          padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.05, left: MediaQuery.of(context).size.width * 0.05),
                          child: Text(
                            "The need to know about your transaction password",
                            textAlign: TextAlign.center,
                            style: GoogleFonts.poppins(fontWeight: FontWeight.w700, fontStyle: FontStyle.normal, height: 1, fontSize: MediaQuery.of(context).size.height * 0.035, color: Colors.white),
                          ),
                        ),
                        SizedBox(
                          height: MediaQuery.of(context).size.height * 0.031,
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.05, right: MediaQuery.of(context).size.width * 0.05),
                          child: Text(
                            "Let’s talk about your transaction password.\nThis is required for each & every transaction, wether sending or receiving.",
                            textAlign: TextAlign.center,
                            style: GoogleFonts.poppins(color: Colors.white, fontWeight: FontWeight.w400, letterSpacing: 0.3, fontSize: MediaQuery.of(context).size.height * 0.017, height: 1.25),
                          ),
                        ),
                        SizedBox(
                          height: MediaQuery.of(context).size.height * 0.031,
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.075, right: MediaQuery.of(context).size.width * 0.075),
                          child: Text(
                            "Of note, please write down your transaction password in a secure location where you won’t lose it.",
                            textAlign: TextAlign.center,
                            style: GoogleFonts.poppins(color: Colors.white, fontWeight: FontWeight.w400, letterSpacing: 0.3, fontSize: MediaQuery.of(context).size.height * 0.017, height: 1.25),
                          ),
                        ),
                        SizedBox(
                          height: MediaQuery.of(context).size.height * 0.041,
                        ),
                        Text(
                          "THIS PASSWORD CANNOT BE RESET !",
                          style: GoogleFonts.poppins(
                            decoration: TextDecoration.underline,
                            fontWeight: FontWeight.w700,
                            fontStyle: FontStyle.normal,
                            fontSize: MediaQuery.of(context).size.height * 0.02,
                            color: Colors.white,
                          ),
                        ),
                        SizedBox(
                          height: MediaQuery.of(context).size.height * 0.03,
                        ),
                        Theme(
                          data: ThemeData(
                            checkboxTheme: CheckboxThemeData(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(6),
                              ),
                            ),
                            unselectedWidgetColor: Colors.white,
                          ),
                          child: CheckboxListTile(
                            checkColor: Color(0xFF00CC9E),
                            activeColor: Colors.white,

                            side: BorderSide(color: Colors.white, width: 1.3),
                            shape: CircleBorder(),
                            title: Text(
                              "I affirm that I will write down my password in a safe place.",
                              style: GoogleFonts.poppins(
                                fontWeight: FontWeight.w400,
                                fontStyle: FontStyle.normal,
                                fontSize: MediaQuery.of(context).size.height * 0.018,
                                color: Colors.white,
                              ),
                            ),
                            value: _checkedValue1,
                            onChanged: (value) {
                              setState(() {
                                _checkedValue1 = value!;
                              });
                            },
                            controlAffinity: ListTileControlAffinity.leading, //  <-- leading Checkbox
                          ),
                        ),
                        Theme(
                          data: ThemeData(
                            checkboxTheme: CheckboxThemeData(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(6),
                              ),
                            ),
                            unselectedWidgetColor: Colors.white,
                          ),
                          child: CheckboxListTile(
                            checkColor: Color(0xFF00CC9E),
                            activeColor: Colors.white,
                            side: BorderSide(color: Colors.white, width: 1.3),
                            shape: CircleBorder(),
                            title: Text(
                              "I understand that tech support cannot help me recover a lost password.",
                              style: GoogleFonts.poppins(
                                fontWeight: FontWeight.w400,
                                fontStyle: FontStyle.normal,
                                fontSize: MediaQuery.of(context).size.height * 0.018,
                                color: Colors.white,
                              ),
                            ),
                            value: _checkedValue2,
                            onChanged: (value) {
                              setState(() {
                                _checkedValue2 = value!;
                              });
                            },
                            controlAffinity: ListTileControlAffinity.leading, //  <-- leading Checkbox
                          ),
                        ),
                        Theme(
                          data: ThemeData(
                            checkboxTheme: CheckboxThemeData(
                              shape: RoundedRectangleBorder(
                                side: BorderSide(color: const Color(0xFF00CC9E), width: 3),
                                borderRadius: BorderRadius.circular(6),
                              ),
                            ),
                            unselectedWidgetColor: Colors.white,
                          ),
                          child: CheckboxListTile(
                            checkColor: Color(0xFF00CC9E),
                            activeColor: Colors.white,
                            side: BorderSide(color: _checkedValue3 ? const Color(0xFF00CC9E) : Colors.white, width: 1.3),
                            shape: CircleBorder(),
                            title: Text(
                              "I have read, understood & agree to the TCG and Privacy policy.",
                              style: GoogleFonts.poppins(
                                fontWeight: FontWeight.w400,
                                fontStyle: FontStyle.normal,
                                fontSize: MediaQuery.of(context).size.height * 0.018,
                                color: Colors.white,
                              ),
                            ),
                            value: _checkedValue3,
                            onChanged: (value) {
                              setState(() {
                                _checkedValue3 = value!;
                              });
                            },
                            controlAffinity: ListTileControlAffinity.leading, //  <-- leading Checkbox
                          ),
                        ),
                        SizedBox(
                          height: MediaQuery.of(context).size.height * 0.04,
                        ),
                        Container(
                          height: MediaQuery.of(context).size.height * 0.07,
                          width: MediaQuery.of(context).size.width * 0.68,
                          child: ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                primary: _checkedValue1 == true && _checkedValue2 == true && _checkedValue3 == true ? Colors.greenAccent : Color(0XFFF6F6FF),
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.07),
                                ),
                              ),
                              onPressed: () {
                                setState(() {
                                  if (_checkedValue1 == true && _checkedValue2 == true && _checkedValue3 == true) {
                                    Navigator.pushReplacement(context, MaterialPageRoute(builder: (_) => CreateWallet()));
                                  }
                                });
                              },
                              child: Text(
                                "I understand",
                                style: GoogleFonts.poppins(
                                  color: (_checkedValue1 == true && _checkedValue2 == true && _checkedValue3 == true) ? Colors.white : Color(0XFFADADC8),
                                  fontWeight: FontWeight.w600,
                                  fontSize: MediaQuery.of(context).size.height * 0.022,
                                  fontStyle: FontStyle.normal,
                                ),
                              )),
                        ),
                      ],
                    ),
                  ),
                ),
              )),
        ));
  }
}
