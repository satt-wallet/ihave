import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:google_fonts/google_fonts.dart';
import 'package:ihave/controllers/LoginController.dart';
import 'package:ihave/controllers/walletController.dart';
import 'package:ihave/model/crypto.dart';
import 'package:ihave/ui/exportKeyBtcEth.dart';

import 'package:ihave/ui/help.dart';
import 'package:ihave/ui/kyc.dart';
import 'package:ihave/ui/security.dart';
import 'package:ihave/ui/signin.dart';
import 'package:ihave/ui/splachscreen.dart';
import 'package:ihave/ui/walletConnect.dart';
import 'package:ihave/util/utils.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'Profilepage.dart';
import 'biometrics.dart';
import 'change_password.dart';
import 'desactivate_account.dart';

class MyProfile extends StatefulWidget {
  bool isVisible = false;

  MyProfile(bool isVisible) {
    this.isVisible = isVisible;
  }

  @override
  State<MyProfile> createState() => _MyProfileState();
}

class _MyProfileState extends State<MyProfile> {
  List<Crypto> listCrypto = [];
  bool isClick = false;
  Uint8List? userPicture;
  bool _isLoadingUserPicture = false;

  bool _userPictureExist = false;
  bool _userPictureSocialExist = false;
  String userPictureSocial = "";

  Future getUser() async {
    var user = await Provider.of<LoginController>(context, listen: false).getUserDetails(Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "");

    return user;
  }

  getUserPicture() async {
    setState(() {
      _isLoadingUserPicture = true;
    });

    if (Provider.of<LoginController>(context, listen: false).userPicture == null) {
      var pic = await LoginController.apiService.getUserPicture(Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "");
      Provider.of<LoginController>(context, listen: false).userPicture = pic != "error" ? pic : null;
      var picture = Provider.of<LoginController>(context, listen: false).userPicture;
      if (picture.toString().length > 112 && picture != null) {
        setState(() {
          userPicture = picture;
          userPictureSocial = "";
          _userPictureSocialExist = false;
          _userPictureExist = true;
        });
      } else {
        getUser().then((value) {
          if (value.picLink != null && value.picLink != "" && value.picLink != "AppleIDAuthorizationScopes.fullName") {
            setState(() {
              userPictureSocial = value.picLink;
              _userPictureExist = false;
              _userPictureSocialExist = true;
            });
          } else {
            setState(() {
              _userPictureExist = false;
              _userPictureSocialExist = false;
            });
          }
        });
        setState(() {
          _isLoadingUserPicture = false;
        });
      }
    } else {
      var picture = Provider.of<LoginController>(context, listen: false).userPicture;
      if (picture.toString().length > 112 && picture != null) {
        setState(() {
          userPicture = picture;
          userPictureSocial = "";
          _userPictureSocialExist = false;
          _userPictureExist = true;
        });
      } else {
        getUser().then((value) {
          if (value.picLink != null && value.picLink != "") {
            setState(() {
              userPictureSocial = value.picLink;
              _userPictureExist = false;
              _userPictureSocialExist = true;
            });
          } else {
            setState(() {
              _userPictureExist = false;
              _userPictureSocialExist = false;
            });
          }
        });
      }
    }
    setState(() {
      _isLoadingUserPicture = false;
    });
  }

  @override
  void initState() {
    super.initState();
    getUserPicture();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            color: Colors.white,
          ),
          child: Column(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.03),
                child: Align(
                  alignment: Alignment.topRight,
                  child: TextButton(
                    onPressed: () {
                      Provider.of<WalletController>(context, listen: false).toggleProfileVisibility();
                    },
                    child: Text(
                      "< Back",
                      style: GoogleFonts.poppins(color: const Color(0XFF373737), fontWeight: FontWeight.w600, fontStyle: FontStyle.normal, fontSize: MediaQuery.of(context).size.height * 0.018),
                    ),
                  ),
                ),
              ),
              Row(
                children: [
                  SizedBox(
                    child: (_isLoadingUserPicture)
                        ? CircleAvatar(
                            radius: MediaQuery.of(context).size.height * 0.05,
                            backgroundColor: Colors.transparent,
                            child: SizedBox(
                              height: MediaQuery.of(context).size.height * 0.02,
                              width: MediaQuery.of(context).size.height * 0.02,
                              child: CircularProgressIndicator(
                                color: const Color(0XFF4048FF),
                              ),
                            ),
                          )
                        : CircleAvatar(
                            backgroundColor: Colors.transparent,
                            radius: MediaQuery.of(context).size.height * 0.05,
                            backgroundImage: _userPictureExist ? MemoryImage(userPicture!) : (_userPictureSocialExist ? NetworkImage(userPictureSocial) : AssetImage("images/avatar-welcome.png") as ImageProvider)),
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.025),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        /** First Name */
                        Text(
                          "${Provider.of<LoginController>(context, listen: false).userDetails!.firstName.toString() == "" ? "-" : Provider.of<LoginController>(context, listen: false).userDetails!.firstName.toString()} ${Provider.of<LoginController>(context, listen: false).userDetails!.lastName.toString() == "" ? "-" : Provider.of<LoginController>(context, listen: false).userDetails!.lastName.toString()}",
                          style: GoogleFonts.poppins(fontWeight: FontWeight.w600, fontStyle: FontStyle.normal, fontSize: MediaQuery.of(context).size.height * 0.018),
                        ),
                        Text(
                          Provider.of<LoginController>(context, listen: false).userDetails!.email.toString(),
                          style: GoogleFonts.poppins(fontWeight: FontWeight.w500, fontStyle: FontStyle.normal, fontSize: MediaQuery.of(context).size.height * 0.012),
                        ),
                      ],
                    ),
                  ),
                  const Spacer(),
                  Padding(
                    padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.05),
                    child: GestureDetector(
                      onTap: () {
                        gotoPageGlobal(context, ProfilePage());
                        Provider.of<WalletController>(context, listen: false).toggleProfileVisibility();
                      },
                      child: SvgPicture.asset("images/edit-icon.svg"),
                    ),
                  )
                ],
              ),

              /** Divider */
              Padding(
                padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.075, right: MediaQuery.of(context).size.width * 0.075),
                child: Divider(),
              ),

              /** KYC BLOC */
              GestureDetector(
                onTap: () {
                  Provider.of<WalletController>(context, listen: false).walletConnectScreen = true;
                  gotoPageGlobal(context, KycPage());
                  Provider.of<WalletController>(context, listen: false).toggleProfileVisibility();
                },
                child: Padding(
                  padding: EdgeInsets.only(
                    left: MediaQuery.of(context).size.width * 0.07,
                    top: MediaQuery.of(context).size.height * 0.006,
                    bottom: MediaQuery.of(context).size.height * 0.006,
                  ),
                  child: Row(
                    children: <Widget>[
                      SvgPicture.asset("images/kyc-icon.svg"),
                      Padding(
                        padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.05),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              "KYC",
                              style: GoogleFonts.poppins(fontWeight: FontWeight.w600, fontStyle: FontStyle.normal, fontSize: MediaQuery.of(context).size.height * 0.015),
                            ),
                            Text(
                              "Verify your identity",
                              style: GoogleFonts.poppins(fontWeight: FontWeight.w400, fontStyle: FontStyle.normal, fontSize: MediaQuery.of(context).size.height * 0.014),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),

              /** Divider */
              Padding(
                padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.075, right: MediaQuery.of(context).size.width * 0.075),
                child: Divider(),
              ),
            ],
          )),
    );
  }
}
