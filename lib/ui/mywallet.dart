import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:ihave/controllers/LoginController.dart';
import 'package:ihave/service/apiService.dart';
import 'package:ihave/ui/WelcomeScreen.dart';
import 'package:ihave/ui/settings.dart';
import 'package:ihave/util/utils.dart';
import 'package:provider/provider.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:carousel_slider/carousel_slider.dart';
import '../controllers/walletController.dart';
import '../widgets/customAppBar.dart';
import '../widgets/customBottomBar.dart';

class MyWallet extends StatefulWidget {
  const MyWallet({Key? key}) : super(key: key);

  @override
  _MyWalletState createState() => _MyWalletState();
}

class _MyWalletState extends State<MyWallet> {
  APIService apiService = LoginController.apiService;
  String walletAddress = "";
  String walletAddress1 = "";
  bool isBTCQrButtonClicked = false;
  bool isCryptoQrButtonClicked = false;
  String address = "";
  String addressBtc = "";
  String addressTron = "";
  String walletAddressBTC = "";
  String walletAddressTron = "";
  String address1 = "";
  String addressBtc1 = "";
  String addressTron1 = "";
  String walletAddressBTC1 = "";
  String walletAddressTron1 = "";
  Uint8List? userPicture;
  Uint8List? userPictureTake;
  Uint8List? picture;
  bool _userPictureExist = false;
  bool _userPictureSocialExist = false;
  String userPictureSocial = "";
  bool _isLoadingUserPicture = true;
  List<Map<String, String>> addrList = [];
  bool isSingleWallet = false;
  String? text = "new";
  bool verif = true;

  Future getAddress() async {
    if ((apiService.walletAddress == null || apiService.walletAddress == "") && (apiService.tronAddress == null || apiService.tronAddress == "")) {
      await apiService.getWalletAddress(Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "", "v1");
      await apiService.getWalletAddress(Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "", "v2");
    }
    return apiService.walletAddress;
  }

  Future getAddress1() async {
    if ((apiService.walletAddressV2 == null || apiService.walletAddressV2 == "") && (apiService.tronAddressV2 == null || apiService.tronAddressV2 == "")) {
      await apiService.getWalletAddress(Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "", "v2");
    }
    return apiService.walletAddressV2;
  }

  createAddressesList(String addr1, String addr2, String addr3) {
    addrList.add({"address": walletAddress, "formatted": address});
    addrList.add({"address": walletAddressBTC, "formatted": addressBtc});
    addrList.add({"address": addressTron, "formatted": walletAddressTron});
  }

  createNewAddressesList(String addr1, String addr2, String addr3) {
    addrList.add({"address": walletAddress1, "formatted": address1});
    addrList.add({"address": walletAddressBTC1, "formatted": addressBtc1});
    addrList.add({"address": addressTron1, "formatted": walletAddressTron1});
  }

  @override
  void initState() {
    getAddress().then((value) {
      setState(() {
        walletAddress = apiService.walletAddress ?? "";
        walletAddressBTC = apiService.btcWalletAddress ?? "";
        addressTron = apiService.tronAddress ?? "";
        walletAddressTron = addressTron.length > 0 ? addressTron.substring(0, 3) + '...' + addressTron.substring(addressTron.length - 6, addressTron.length) : "";
        var adr = walletAddress.toString();
        address = 'Ox' + '...' + adr.substring(35);
        var adrBtc = walletAddressBTC.toString();
        addressBtc = (walletAddressBTC.toString().length > 0) ? walletAddressBTC[0] + walletAddressBTC[1] + '...' + walletAddressBTC.substring(27) : "";
      });
    });
    getAddress1().then((value) {
      setState(() {
        walletAddress1 = apiService.walletAddressV2 ?? "";
        walletAddressBTC1 = apiService.btcWalletAddressV2 ?? "";
        addressTron1 = apiService.tronAddressV2 ?? "";
        walletAddressTron1 = addressTron1.length > 0 ? addressTron1.substring(0, 5) + '...' + addressTron1.substring(addressTron1.length - 6, addressTron1.length) : "";
        var adr1 = walletAddress1.toString();
        address1 = 'Ox' + '...' + adr1.substring(35);
        addressBtc1 = (walletAddressBTC1.toString().length > 0) ? walletAddressBTC1[0] + walletAddressBTC1[1] + '...' + walletAddressBTC1.substring(27) : "";
      });
      isSingleWallet = (walletAddress1 == walletAddress || walletAddress1 == "" || walletAddress1 == null);
    });

    Provider.of<LoginController>(context, listen: false).tokenSymbol == "";
    Provider.of<LoginController>(context, listen: false).tokenNetwork == "";
    super.initState();
  }

  @override
  void dispose() {
    userPicture = null;
    super.dispose();
  }

  _launchURLBnb() async {
    var url = 'https://bscscan.com/address/' + walletAddress;
    if (await canLaunch(url)) {
      await launch(url, forceSafariVC: true, forceWebView: false, enableJavaScript: true);
    } else {
      throw 'Could not launch $url';
    }
  }

  _launchURLEth() async {
    var url = 'https://etherscan.io/address/' + walletAddress;
    if (await canLaunch(url)) {
      await launch(url, forceSafariVC: true, forceWebView: false, enableJavaScript: true);
    } else {
      throw 'Could not launch $url';
    }
  }

  _launchURLBTT() async {
    var url = 'https://bttcscan.com/address/' + walletAddress;
    if (await canLaunch(url)) {
      await launch(url, forceSafariVC: true, forceWebView: false, enableJavaScript: true);
    } else {
      throw 'Could not launch $url';
    }
  }

  _launchURLPolygon() async {
    var url = 'https://polygonscan.com/address/' + walletAddress;
    if (await canLaunch(url)) {
      await launch(url, forceSafariVC: true, forceWebView: false, enableJavaScript: true);
    } else {
      throw 'Could not launch $url';
    }
  }

  _launchURLBtc() async {
    var url = 'https://www.blockchain.com/btc/address/' + walletAddressBTC;
    if (await canLaunch(url)) {
      await launch(url, forceSafariVC: true, forceWebView: false, enableJavaScript: true);
    } else {
      throw 'Could not launch $url';
    }
  }

  _launchURLTrx() async {
    var url = 'https://tronscan.org/?_ga=2.242461954.186286839.1669296002-1323534981.1669296002#/address/' + addressTron;
    if (await canLaunch(url)) {
      await launch(url, forceSafariVC: true, forceWebView: false, enableJavaScript: true);
    } else {
      throw 'Could not launch $url';
    }
  }

  //new version
  _launchURLBnb1() async {
    var url = 'https://bscscan.com/address/' + walletAddress1;
    if (await canLaunch(url)) {
      await launch(url, forceSafariVC: true, forceWebView: false, enableJavaScript: true);
    } else {
      throw 'Could not launch $url';
    }
  }

  _launchURLEth1() async {
    var url = 'https://etherscan.io/address/' + walletAddress1;
    if (await canLaunch(url)) {
      await launch(url, forceSafariVC: true, forceWebView: false, enableJavaScript: true);
    } else {
      throw 'Could not launch $url';
    }
  }

  _launchURLBTT1() async {
    var url = 'https://bttcscan.com/address/' + walletAddress1;
    if (await canLaunch(url)) {
      await launch(url, forceSafariVC: true, forceWebView: false, enableJavaScript: true);
    } else {
      throw 'Could not launch $url';
    }
  }

  _launchURLPolygon1() async {
    var url = 'https://polygonscan.com/address/' + walletAddress1;
    if (await canLaunch(url)) {
      await launch(url, forceSafariVC: true, forceWebView: false, enableJavaScript: true);
    } else {
      throw 'Could not launch $url';
    }
  }

  _launchURLBtc1() async {
    var url = 'https://www.blockchain.com/btc/address/' + walletAddressBTC1;
    if (await canLaunch(url)) {
      await launch(url, forceSafariVC: true, forceWebView: false, enableJavaScript: true);
    } else {
      throw 'Could not launch $url';
    }
  }

  _launchURLTrx1() async {
    var url = 'https://tronscan.org/?_ga=2.242461954.186286839.1669296002-1323534981.1669296002#/address/' + addressTron1;
    if (await canLaunch(url)) {
      await launch(url, forceSafariVC: true, forceWebView: false, enableJavaScript: true);
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        gotoPageGlobal(context, WelcomeScreen());
        return Future.value(true);
      },
      child: Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(
          colors: [Color(0xFF4048FF), Color(0xFF1F2337), Color(0xFF1F2337), Color(0xFF1F2337), Color(0xFF1F2337)],
          begin: const FractionalOffset(0.0, 0.0),
          end: const FractionalOffset(0.0, 1.0),
        )),
        child: Scaffold(
          resizeToAvoidBottomInset: false,
          backgroundColor: Colors.transparent,
          appBar: CustomAppBar(),
          bottomNavigationBar: CustomBottomBar(),
          body: Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            alignment: Alignment.topLeft,
            padding: EdgeInsets.fromLTRB(12, 30, 10, 0),
            child: Column(
              children: [
                Text(
                  'wallet ID',
                  style: GoogleFonts.poppins(color: Colors.white, fontWeight: FontWeight.w700, fontSize: MediaQuery.of(context).size.height * 0.035),
                ),
                Visibility(
                  visible: !isSingleWallet,
                  child: InkWell(
                    onTap: () async {
                      setState(() {
                        verif = !verif;
                        verif ? version = "v1" : version = "v2";
                        verif ? text = "new" : text = "old";
                      });
                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Image.asset(
                          "images/refresh.png",
                          width: MediaQuery.of(context).size.width * .032,
                          height: MediaQuery.of(context).size.height * .02,
                        ),
                        SizedBox(width: MediaQuery.of(context).size.width * .02),
                        Text("Go to"),
                        Text(" " + text!),
                        Text(" Wallet"),
                      ],
                    ),
                  ),
                ),
                /**
                 * Old Version
                 */
                version == "v1"
                    ? CarouselSlider(
                        options: CarouselOptions(height: MediaQuery.of(context).size.height * .6),
                        items: [
                          ListTile(
                            title: Column(children: [
                              Padding(
                                padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.02),
                                child: Text(
                                  'ERC / BSC / Polygon / BTTC',
                                  style: GoogleFonts.poppins(color: Colors.white, fontWeight: FontWeight.w600, fontSize: MediaQuery.of(context).size.height * 0.02),
                                ),
                              ),
                              Center(
                                  child: Padding(
                                padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.015),
                                child: QrImage(
                                  data: walletAddress,
                                  size: 200,
                                  backgroundColor: Colors.white,
                                ),
                              )),
                              Padding(
                                padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.015),
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(30),
                                  ),
                                  height: 40,
                                  width: 180,
                                  child: Row(
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.only(left: 20),
                                        child: Text(
                                          address.toString(),
                                          textAlign: TextAlign.center,
                                          style: GoogleFonts.poppins(color: Color(0xFF4048FF), decoration: TextDecoration.underline, decorationThickness: 2, fontWeight: FontWeight.w600, fontSize: MediaQuery.of(context).size.height * 0.023),
                                        ),
                                      ),
                                      Spacer(),
                                      IconButton(
                                        onPressed: () {
                                          Clipboard.setData(ClipboardData(text: walletAddress));
                                          Fluttertoast.showToast(msg: "Copied to clipboard", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.BOTTOM, timeInSecForIosWeb: 1);
                                        },
                                        icon: Icon(
                                          Icons.content_copy_rounded,
                                          color: Color(0xFF4048FF),
                                          size: MediaQuery.of(context).size.height * 0.025,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              SizedBox(height: 20),
                              Row(
                                children: [
                                  ElevatedButton(
                                    onPressed: () {
                                      _launchURLBnb();
                                    },
                                    child: Row(
                                      children: [
                                        Transform.scale(
                                          scale: 1,
                                          child: CircleAvatar(
                                            backgroundColor: Color(0xff4048FF),
                                            child: SvgPicture.asset(
                                              "images/bep.svg",
                                              color: Colors.white,
                                              height: 27,
                                              width: 27,
                                            ),
                                          ),
                                        ),
                                        Spacer(),
                                        SvgPicture.asset(
                                          "images/urlLaunch.svg",
                                          color: Color.fromRGBO(64, 72, 255, 1),
                                          height: 20,
                                          width: 20,
                                        ),
                                      ],
                                    ),
                                    style: ElevatedButton.styleFrom(
                                      shadowColor: Colors.white,
                                      primary: Colors.white,
                                      fixedSize: Size(100, 50),
                                      side: BorderSide(color: Colors.white),
                                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(80.0)),
                                    ),
                                  ),
                                  SizedBox(width: 20),
                                  ElevatedButton(
                                    onPressed: () {
                                      _launchURLPolygon();
                                    },
                                    child: Row(
                                      children: [
                                        Transform.scale(
                                          scale: 1,
                                          child: CircleAvatar(
                                            backgroundColor: Color(0xff8247E5),
                                            child: SvgPicture.asset(
                                              "images/polygon-network.svg",
                                              color: Colors.white,
                                              height: 27,
                                              width: 27,
                                            ),
                                          ),
                                        ),
                                        Spacer(),
                                        SvgPicture.asset(
                                          "images/urlLaunch.svg",
                                          color: Color.fromRGBO(64, 72, 255, 1),
                                          height: 20,
                                          width: 20,
                                        ),
                                      ],
                                    ),
                                    style: ElevatedButton.styleFrom(
                                      shadowColor: Colors.white,
                                      primary: Colors.white,
                                      fixedSize: Size(100, 50),
                                      side: BorderSide(color: Colors.white),
                                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(80.0)),
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 20),
                              Row(
                                children: [
                                  ElevatedButton(
                                    onPressed: () {
                                      _launchURLEth();
                                    },
                                    child: Row(
                                      children: [
                                        Transform.scale(
                                          scale: 1,
                                          child: SvgPicture.asset(
                                            "images/ETH.svg",
                                            height: 35,
                                            width: 35,
                                          ),
                                        ),
                                        Spacer(),
                                        SvgPicture.asset(
                                          "images/urlLaunch.svg",
                                          color: Color.fromRGBO(64, 72, 255, 1),
                                          height: 20,
                                          width: 20,
                                        ),
                                      ],
                                    ),
                                    style: ElevatedButton.styleFrom(
                                      shadowColor: Colors.white,
                                      primary: Colors.white,
                                      fixedSize: Size(100, 50),
                                      side: BorderSide(color: Colors.white),
                                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(80.0)),
                                    ),
                                  ),
                                  SizedBox(width: 20),
                                  ElevatedButton(
                                    onPressed: () {
                                      _launchURLBTT();
                                    },
                                    child: Row(
                                      children: [
                                        Transform.scale(
                                          scale: 1,
                                          child: CircleAvatar(
                                            backgroundColor: Color(0xff000000),
                                            child: SvgPicture.asset(
                                              "images/bittorrent.svg",
                                              color: Colors.white,
                                              height: 40,
                                              width: 40,
                                            ),
                                          ),
                                        ),
                                        Spacer(),
                                        SvgPicture.asset(
                                          "images/urlLaunch.svg",
                                          color: Color.fromRGBO(64, 72, 255, 1),
                                          height: 20,
                                          width: 20,
                                        ),
                                      ],
                                    ),
                                    style: ElevatedButton.styleFrom(
                                      shadowColor: Colors.white,
                                      primary: Colors.white,
                                      fixedSize: Size(100, 50),
                                      side: BorderSide(color: Colors.white),
                                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(80.0)),
                                    ),
                                  ),
                                ],
                              ),
                            ]),
                          ),
                          ListTile(
                            title: Column(children: [
                              Padding(
                                padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.02),
                                child: Text(
                                  'BTC',
                                  style: GoogleFonts.poppins(color: Colors.white, fontWeight: FontWeight.w600, fontSize: MediaQuery.of(context).size.height * 0.02),
                                ),
                              ),
                              Center(
                                  child: Padding(
                                padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.015),
                                child: QrImage(
                                  data: walletAddressBTC,
                                  size: 200,
                                  backgroundColor: Colors.white,
                                ),
                              )),
                              Padding(
                                padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.015),
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(30),
                                  ),
                                  height: 40,
                                  width: 180,
                                  child: Row(
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.only(left: 20),
                                        child: Text(
                                          addressBtc.toString(),
                                          textAlign: TextAlign.center,
                                          style: GoogleFonts.poppins(color: Color(0xFF4048FF), decoration: TextDecoration.underline, decorationThickness: 2, fontWeight: FontWeight.w600, fontSize: MediaQuery.of(context).size.height * 0.023),
                                        ),
                                      ),
                                      Spacer(),
                                      IconButton(
                                        onPressed: () {
                                          Clipboard.setData(ClipboardData(text: walletAddressBTC));
                                          Fluttertoast.showToast(msg: "Copied to clipboard", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.BOTTOM, timeInSecForIosWeb: 1);
                                        },
                                        icon: Icon(
                                          Icons.content_copy_rounded,
                                          color: Color(0xFF4048FF),
                                          size: MediaQuery.of(context).size.height * 0.025,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              SizedBox(height: 20),
                              ElevatedButton(
                                onPressed: () {
                                  _launchURLBtc();
                                },
                                child: Row(
                                  children: [
                                    Transform.scale(
                                      scale: 1,
                                      child: CircleAvatar(
                                        backgroundColor: Color(0xffF7931A),
                                        child: SvgPicture.asset(
                                          "images/btc-icon.svg",
                                          color: Colors.white,
                                          height: 27,
                                          width: 27,
                                        ),
                                      ),
                                    ),
                                    Spacer(),
                                    SvgPicture.asset(
                                      "images/urlLaunch.svg",
                                      color: Color.fromRGBO(64, 72, 255, 1),
                                      height: 20,
                                      width: 20,
                                    ),
                                  ],
                                ),
                                style: ElevatedButton.styleFrom(
                                  shadowColor: Colors.white,
                                  primary: Colors.white,
                                  fixedSize: Size(100, 50),
                                  side: BorderSide(color: Colors.white),
                                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(80.0)),
                                ),
                              ),
                            ]),
                          ),
                          ListTile(
                            title: Column(children: [
                              Padding(
                                padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.02),
                                child: Text(
                                  'TRON',
                                  style: GoogleFonts.poppins(color: Colors.white, fontWeight: FontWeight.w600, fontSize: MediaQuery.of(context).size.height * 0.02),
                                ),
                              ),
                              Center(
                                  child: Padding(
                                padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.015),
                                child: QrImage(
                                  data: addressTron,
                                  size: 200,
                                  backgroundColor: Colors.white,
                                ),
                              )),
                              Padding(
                                padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.015),
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(30),
                                  ),
                                  height: 40,
                                  width: 180,
                                  child: Row(
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.only(left: 20),
                                        child: Text(
                                          walletAddressTron.toString(),
                                          textAlign: TextAlign.center,
                                          style: GoogleFonts.poppins(color: Color(0xFF4048FF), decoration: TextDecoration.underline, decorationThickness: 2, fontWeight: FontWeight.w600, fontSize: MediaQuery.of(context).size.height * 0.023),
                                        ),
                                      ),
                                      Spacer(),
                                      IconButton(
                                        onPressed: () {
                                          Clipboard.setData(ClipboardData(text: addressTron));
                                          Fluttertoast.showToast(msg: "Copied to clipboard", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.BOTTOM, timeInSecForIosWeb: 1);
                                        },
                                        icon: Icon(
                                          Icons.content_copy_rounded,
                                          color: Color(0xFF4048FF),
                                          size: MediaQuery.of(context).size.height * 0.025,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              SizedBox(height: 20),
                              ElevatedButton(
                                onPressed: () {
                                  _launchURLTrx();
                                },
                                child: Row(
                                  children: [
                                    Transform.scale(
                                      scale: 1,
                                      child: SvgPicture.asset(
                                        "images/tron.svg",
                                        height: 35,
                                        width: 35,
                                      ),
                                    ),
                                    Spacer(),
                                    SvgPicture.asset(
                                      "images/urlLaunch.svg",
                                      color: Color.fromRGBO(64, 72, 255, 1),
                                      height: 20,
                                      width: 20,
                                    ),
                                  ],
                                ),
                                style: ElevatedButton.styleFrom(
                                  shadowColor: Colors.white,
                                  primary: Colors.white,
                                  fixedSize: Size(100, 50),
                                  side: BorderSide(color: Colors.white),
                                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(80.0)),
                                ),
                              ),
                            ]),
                          ),
                        ],
                      )
                    :
                    /**
                 * New Version
                 */
                    CarouselSlider(
                        options: CarouselOptions(height: MediaQuery.of(context).size.height * .6),
                        items: [
                          ListTile(
                            title: Column(children: [
                              Padding(
                                padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.02),
                                child: Text(
                                  'ERC / BSC / Polygon / BTTC',
                                  style: GoogleFonts.poppins(color: Colors.white, fontWeight: FontWeight.w600, fontSize: MediaQuery.of(context).size.height * 0.02),
                                ),
                              ),
                              Center(
                                  child: Padding(
                                padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.015),
                                child: QrImage(
                                  data: walletAddress1,
                                  size: 200,
                                  backgroundColor: Colors.white,
                                ),
                              )),
                              Padding(
                                padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.015),
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(30),
                                  ),
                                  height: 40,
                                  width: 180,
                                  child: Row(
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.only(left: 20),
                                        child: Text(
                                          address1.toString(),
                                          textAlign: TextAlign.center,
                                          style: GoogleFonts.poppins(color: Color(0xFF4048FF), decoration: TextDecoration.underline, decorationThickness: 2, fontWeight: FontWeight.w600, fontSize: MediaQuery.of(context).size.height * 0.023),
                                        ),
                                      ),
                                      Spacer(),
                                      IconButton(
                                        onPressed: () {
                                          Clipboard.setData(ClipboardData(text: walletAddress1));
                                          Fluttertoast.showToast(msg: "Copied to clipboard", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.BOTTOM, timeInSecForIosWeb: 1);
                                        },
                                        icon: Icon(
                                          Icons.content_copy_rounded,
                                          color: Color(0xFF4048FF),
                                          size: MediaQuery.of(context).size.height * 0.025,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              SizedBox(height: 20),
                              Row(
                                children: [
                                  ElevatedButton(
                                    onPressed: () {
                                      _launchURLBnb1();
                                    },
                                    child: Row(
                                      children: [
                                        Transform.scale(
                                          scale: 1,
                                          child: CircleAvatar(
                                            backgroundColor: Color(0xff4048FF),
                                            child: SvgPicture.asset(
                                              "images/bep.svg",
                                              color: Colors.white,
                                              height: 27,
                                              width: 27,
                                            ),
                                          ),
                                        ),
                                        Spacer(),
                                        SvgPicture.asset(
                                          "images/urlLaunch.svg",
                                          color: Color.fromRGBO(64, 72, 255, 1),
                                          height: 20,
                                          width: 20,
                                        ),
                                      ],
                                    ),
                                    style: ElevatedButton.styleFrom(
                                      shadowColor: Colors.white,
                                      primary: Colors.white,
                                      fixedSize: Size(100, 50),
                                      side: BorderSide(color: Colors.white),
                                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(80.0)),
                                    ),
                                  ),
                                  SizedBox(width: 20),
                                  ElevatedButton(
                                    onPressed: () {
                                      _launchURLPolygon1();
                                    },
                                    child: Row(
                                      children: [
                                        Transform.scale(
                                          scale: 1,
                                          child: CircleAvatar(
                                            backgroundColor: Color(0xff8247E5),
                                            child: SvgPicture.asset(
                                              "images/polygon-network.svg",
                                              color: Colors.white,
                                              height: 27,
                                              width: 27,
                                            ),
                                          ),
                                        ),
                                        Spacer(),
                                        SvgPicture.asset(
                                          "images/urlLaunch.svg",
                                          color: Color.fromRGBO(64, 72, 255, 1),
                                          height: 20,
                                          width: 20,
                                        ),
                                      ],
                                    ),
                                    style: ElevatedButton.styleFrom(
                                      shadowColor: Colors.white,
                                      primary: Colors.white,
                                      fixedSize: Size(100, 50),
                                      side: BorderSide(color: Colors.white),
                                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(80.0)),
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 20),
                              Row(
                                children: [
                                  ElevatedButton(
                                    onPressed: () {
                                      _launchURLEth1();
                                    },
                                    child: Row(
                                      children: [
                                        Transform.scale(
                                          scale: 1,
                                          child: SvgPicture.asset(
                                            "images/ETH.svg",
                                            height: 35,
                                            width: 35,
                                          ),
                                        ),
                                        Spacer(),
                                        SvgPicture.asset(
                                          "images/urlLaunch.svg",
                                          color: Color.fromRGBO(64, 72, 255, 1),
                                          height: 20,
                                          width: 20,
                                        ),
                                      ],
                                    ),
                                    style: ElevatedButton.styleFrom(
                                      shadowColor: Colors.white,
                                      primary: Colors.white,
                                      fixedSize: Size(100, 50),
                                      side: BorderSide(color: Colors.white),
                                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(80.0)),
                                    ),
                                  ),
                                  SizedBox(width: 20),
                                  ElevatedButton(
                                    onPressed: () {
                                      _launchURLBTT1();
                                    },
                                    child: Row(
                                      children: [
                                        Transform.scale(
                                          scale: 1,
                                          child: CircleAvatar(
                                            backgroundColor: Color(0xff000000),
                                            child: SvgPicture.asset(
                                              "images/bittorrent.svg",
                                              color: Colors.white,
                                              height: 40,
                                              width: 40,
                                            ),
                                          ),
                                        ),
                                        Spacer(),
                                        SvgPicture.asset(
                                          "images/urlLaunch.svg",
                                          color: Color.fromRGBO(64, 72, 255, 1),
                                          height: 20,
                                          width: 20,
                                        ),
                                      ],
                                    ),
                                    style: ElevatedButton.styleFrom(
                                      shadowColor: Colors.white,
                                      primary: Colors.white,
                                      fixedSize: Size(100, 50),
                                      side: BorderSide(color: Colors.white),
                                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(80.0)),
                                    ),
                                  ),
                                ],
                              ),
                            ]),
                          ),
                          ListTile(
                            title: Column(children: [
                              Padding(
                                padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.02),
                                child: Text(
                                  'BTC',
                                  style: GoogleFonts.poppins(color: Colors.white, fontWeight: FontWeight.w600, fontSize: MediaQuery.of(context).size.height * 0.02),
                                ),
                              ),
                              Center(
                                  child: Padding(
                                padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.015),
                                child: QrImage(
                                  data: walletAddressBTC1,
                                  size: 200,
                                  backgroundColor: Colors.white,
                                ),
                              )),
                              Padding(
                                padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.015),
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(30),
                                  ),
                                  height: 40,
                                  width: 180,
                                  child: Row(
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.only(left: 20),
                                        child: Text(
                                          addressBtc1.toString(),
                                          textAlign: TextAlign.center,
                                          style: GoogleFonts.poppins(color: Color(0xFF4048FF), decoration: TextDecoration.underline, decorationThickness: 2, fontWeight: FontWeight.w600, fontSize: MediaQuery.of(context).size.height * 0.023),
                                        ),
                                      ),
                                      Spacer(),
                                      IconButton(
                                        onPressed: () {
                                          Clipboard.setData(ClipboardData(text: walletAddressBTC1));
                                          Fluttertoast.showToast(msg: "Copied to clipboard", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.BOTTOM, timeInSecForIosWeb: 1);
                                        },
                                        icon: Icon(
                                          Icons.content_copy_rounded,
                                          color: Color(0xFF4048FF),
                                          size: MediaQuery.of(context).size.height * 0.025,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              SizedBox(height: 20),
                              ElevatedButton(
                                onPressed: () {
                                  _launchURLBtc1();
                                },
                                child: Row(
                                  children: [
                                    Transform.scale(
                                      scale: 1,
                                      child: CircleAvatar(
                                        backgroundColor: Color(0xffF7931A),
                                        child: SvgPicture.asset(
                                          "images/btc-icon.svg",
                                          color: Colors.white,
                                          height: 27,
                                          width: 27,
                                        ),
                                      ),
                                    ),
                                    Spacer(),
                                    SvgPicture.asset(
                                      "images/urlLaunch.svg",
                                      color: Color.fromRGBO(64, 72, 255, 1),
                                      height: 20,
                                      width: 20,
                                    ),
                                  ],
                                ),
                                style: ElevatedButton.styleFrom(
                                  shadowColor: Colors.white,
                                  primary: Colors.white,
                                  fixedSize: Size(100, 50),
                                  side: BorderSide(color: Colors.white),
                                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(80.0)),
                                ),
                              ),
                            ]),
                          ),
                          ListTile(
                            title: Column(children: [
                              Padding(
                                padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.02),
                                child: Text(
                                  'TRON',
                                  style: GoogleFonts.poppins(color: Colors.white, fontWeight: FontWeight.w600, fontSize: MediaQuery.of(context).size.height * 0.02),
                                ),
                              ),
                              Center(
                                  child: Padding(
                                padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.015),
                                child: QrImage(
                                  data: addressTron1,
                                  size: 200,
                                  backgroundColor: Colors.white,
                                ),
                              )),
                              Padding(
                                padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.015),
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(30),
                                  ),
                                  height: 40,
                                  width: 180,
                                  child: Row(
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.only(left: 20),
                                        child: Text(
                                          walletAddressTron1.toString(),
                                          textAlign: TextAlign.center,
                                          style: GoogleFonts.poppins(color: Color(0xFF4048FF), decoration: TextDecoration.underline, decorationThickness: 2, fontWeight: FontWeight.w600, fontSize: MediaQuery.of(context).size.height * 0.023),
                                        ),
                                      ),
                                      Spacer(),
                                      IconButton(
                                        onPressed: () {
                                          Clipboard.setData(ClipboardData(text: addressTron1));
                                          Fluttertoast.showToast(msg: "Copied to clipboard", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.BOTTOM, timeInSecForIosWeb: 1);
                                        },
                                        icon: Icon(
                                          Icons.content_copy_rounded,
                                          color: Color(0xFF4048FF),
                                          size: MediaQuery.of(context).size.height * 0.025,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              SizedBox(height: 20),
                              ElevatedButton(
                                onPressed: () {
                                  _launchURLTrx1();
                                },
                                child: Row(
                                  children: [
                                    Transform.scale(
                                      scale: 1,
                                      child: SvgPicture.asset(
                                        "images/tron.svg",
                                        height: 35,
                                        width: 35,
                                      ),
                                    ),
                                    Spacer(),
                                    SvgPicture.asset(
                                      "images/urlLaunch.svg",
                                      color: Color.fromRGBO(64, 72, 255, 1),
                                      height: 20,
                                      width: 20,
                                    ),
                                  ],
                                ),
                                style: ElevatedButton.styleFrom(
                                  shadowColor: Colors.white,
                                  primary: Colors.white,
                                  fixedSize: Size(100, 50),
                                  side: BorderSide(color: Colors.white),
                                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(80.0)),
                                ),
                              ),
                            ]),
                          ),
                        ],
                      ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
