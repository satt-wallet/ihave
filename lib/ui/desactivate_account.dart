import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:ihave/ui/confirmAccountDesactivation.dart';
import 'package:ihave/ui/signin.dart';
import 'package:ihave/ui/staticElements/headerNavigation.dart';
import 'package:provider/provider.dart';

import '../controllers/LoginController.dart';
import '../controllers/walletController.dart';
import '../service/apiService.dart';
import '../util/utils.dart';
import '../widgets/customAppBar.dart';
import '../widgets/customBottomBar.dart';
import 'WelcomeScreen.dart';

class DesactivateAccount extends StatefulWidget {
  const DesactivateAccount({Key? key}) : super(key: key);

  @override
  _DesactivateAccountState createState() => _DesactivateAccountState();
}

class _DesactivateAccountState extends State<DesactivateAccount> {
  bool _isVisible = false;
  bool _isProfileVisible = false;
  bool _isWalletVisible = false;
  bool _isNotificationVisible = false;
  bool _checkedValue1 = false;
  bool _isActivatedButton = false;
  bool isDeleted = false;
  bool wrongpassword = false;
  bool isChecked = false;
  String password = "";
  bool passwordEdited = false;
  bool isconfirmed = false;
  bool _obscureText = true;
  bool _alertActivatedButton = false;
  bool _showDialogIsOpened = false;
  bool confirmPasswordEdited = false;
  bool _obscureText1 = true;
  final List<String> reasonList = ['I created multiple accounts', 'Problems with the platform', 'I want my informations deleted', 'I have a privacy concern', 'The services did not meet my\nexpectations', 'Something else ...'];

  TextEditingController _reasonController = new TextEditingController();
  String? firebaseToken = " ";
  APIService apiService = LoginController.apiService;
  bool _selectedElement = false;

  stateInitalisation(BuildContext context) async {
    setState(() {
      _selectedElement = true;
    });
  }

  @override
  void initState() {
    password = Provider.of<LoginController>(context, listen: false).password;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    setState(() {
      _isVisible = Provider.of<WalletController>(context, listen: true).isSideMenuVisible;
      _isProfileVisible = Provider.of<WalletController>(context, listen: true).isProfileVisible;
      _isWalletVisible = Provider.of<WalletController>(context, listen: true).isWalletVisible;
      _isNotificationVisible = Provider.of<WalletController>(context, listen: true).isNotificationVisible;
    });
    return Scaffold(
      appBar: CustomAppBar(),
      bottomNavigationBar: CustomBottomBar(),
      body: WillPopScope(
        onWillPop: () {
          return Future.value(true);
        },
        child: Stack(
          children: [
            SingleChildScrollView(
              child: Column(
                children: [
                  Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Container(
                        alignment: Alignment.center,
                        child: Column(
                          children: [
                            Padding(
                              padding: EdgeInsets.all(MediaQuery.of(context).size.width * 0.02),
                              child: Align(
                                alignment: Alignment.topLeft,
                                child: Text(
                                  "Account\ndesactivation",
                                  style: GoogleFonts.poppins(
                                    color: Color(0xFF1F2337),
                                    fontSize: MediaQuery.of(context).size.width * 0.07,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.027, top: MediaQuery.of(context).size.width * 0.03),
                              child: Column(
                                children: [
                                  Align(
                                    alignment: Alignment.topLeft,
                                    child: Text("We’re sorry to see you go !", style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.037, fontWeight: FontWeight.w700, color: Color(0xFF1F2337))),
                                  ),
                                  Align(
                                    alignment: Alignment.topLeft,
                                    child: Text("At the same time, we want to address\nthe reason why you might be leaving.\nWould you mind explaining why you’re\nchoosing to go ? ",
                                        style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.037, fontWeight: FontWeight.w500, color: Color(0xFF1F2337))),
                                  ),
                                ],
                              ),
                            ),
                            Padding(
                                padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.065),
                                child: Container(
                                  width: MediaQuery.of(context).size.width * 0.9,
                                  height: MediaQuery.of(context).size.height * 0.07,
                                  child: TextFormField(
                                    onTap: () {
                                      setState(() {
                                        _showDialogIsOpened = true;
                                      });
                                      showDialog(
                                          barrierLabel: "dialog",
                                          barrierDismissible: true,
                                          context: context,
                                          useRootNavigator: false,
                                          builder: (context) {
                                            return StatefulBuilder(builder: (BuildContext context, setState) {
                                              return Center(
                                                child: Container(
                                                  height: MediaQuery.of(context).size.height * 0.5,
                                                  width: MediaQuery.of(context).size.width * 0.9,
                                                  decoration: BoxDecoration(
                                                      boxShadow: [
                                                        BoxShadow(
                                                          color: Colors.grey.withOpacity(0.2),
                                                          spreadRadius: 5,
                                                          blurRadius: 7,
                                                          offset: Offset(0, 3), // changes position of shadow
                                                        ),
                                                      ],
                                                      color: Colors.white,
                                                      border: Border.all(
                                                        color: Colors.white,
                                                      ),
                                                      borderRadius: BorderRadius.all(
                                                        Radius.circular(30),
                                                      )),
                                                  child: Padding(
                                                    padding: const EdgeInsets.all(8.0),
                                                    child: ListView.builder(
                                                      itemBuilder: (BuildContext context, int index) {
                                                        return Center(
                                                          child: Container(
                                                            child: Padding(
                                                              padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.height * 0.02, MediaQuery.of(context).size.width * 0.01, MediaQuery.of(context).size.height * 0.01, MediaQuery.of(context).size.width * 0.01),
                                                              child: Card(
                                                                color: Colors.white,
                                                                shadowColor: Colors.white,
                                                                elevation: 0,
                                                                child: Row(
                                                                  children: [
                                                                    Text(reasonList[index], style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.035, color: Colors.black54)),
                                                                    Spacer(),
                                                                    Radio(
                                                                      value: reasonList[index],
                                                                      groupValue: _reasonController.text,
                                                                      activeColor: Color(0xFF4048FF),
                                                                      onChanged: (val) {
                                                                        setState(() {
                                                                          _reasonController.text = val.toString();
                                                                          _reasonController.text = _reasonController.text.replaceAll("\n", " ");
                                                                          Provider.of<LoginController>(context, listen: false).deactivationReason = _reasonController.text;
                                                                          Navigator.of(context).pop();
                                                                        });
                                                                        return stateInitalisation(context);
                                                                      },
                                                                    ),
                                                                  ],
                                                                ),
                                                              ),
                                                            ),
                                                          ),
                                                        );
                                                      },
                                                      itemCount: reasonList.length,
                                                    ),
                                                  ),
                                                ),
                                              );
                                            });
                                          });
                                    },
                                    controller: _reasonController,
                                    readOnly: true,
                                    onChanged: (value) {
                                      setState(() {
                                        _reasonController.text = value;
                                      });
                                    },
                                    decoration: new InputDecoration(
                                        enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.grey.withOpacity(0.3))),
                                        focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.grey)),
                                        errorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red)),
                                        contentPadding: EdgeInsets.all(10),
                                        suffixIcon: Padding(
                                          padding: const EdgeInsetsDirectional.only(end: 12.0),
                                          child: Icon(
                                            Icons.keyboard_arrow_down,
                                            size: 30,
                                            color: Color(0xFF212529),
                                          ), // myIcon is a 48px-wide widget.
                                        )),
                                  ),
                                )),
                            Row(
                              children: [
                                Padding(
                                  padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.03, 0, 0, 0),
                                  child: Align(
                                    alignment: Alignment.topLeft,
                                    child: Container(
                                      decoration: BoxDecoration(border: Border.all(color: Color(0xFF00CC9E), width: 2), borderRadius: BorderRadius.all(Radius.circular(5))),
                                      width: 22,
                                      height: 22,
                                      child: Theme(
                                        data: ThemeData(
                                            checkboxTheme: CheckboxThemeData(
                                              shape: RoundedRectangleBorder(
                                                borderRadius: BorderRadius.circular(5),
                                              ),
                                            ),
                                            unselectedWidgetColor: Colors.transparent),
                                        child: Checkbox(
                                          checkColor: Color(0xFF00CC9E),
                                          activeColor: Colors.transparent,
                                          value: _checkedValue1,
                                          onChanged: (value) {
                                            setState(() {
                                              _checkedValue1 = value!;
                                            });
                                          },
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.033, left: MediaQuery.of(context).size.height * 0.025),
                                  child: InkWell(
                                    child: Text(
                                      "I would like to permanently delete\nmy account on ihave.io and all\nmy personal information.",
                                      style: GoogleFonts.poppins(
                                        color: Colors.black,
                                        fontWeight: FontWeight.w400,
                                        fontSize: MediaQuery.of(context).size.width * 0.037,
                                      ),
                                    ),
                                    onTap: () {
                                      setState(() {
                                        _checkedValue1 = !_checkedValue1;
                                      });
                                    },
                                  ),
                                ),
                              ],
                            ),
                            Padding(
                              padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.height * 0.001, MediaQuery.of(context).size.width * 0.27, MediaQuery.of(context).size.height * 0.001, MediaQuery.of(context).size.width * 0.09),
                              child: Container(
                                height: MediaQuery.of(context).size.height * 0.06,
                                width: MediaQuery.of(context).size.width * 0.8,
                                child: ElevatedButton(
                                  style: ButtonStyle(
                                      backgroundColor: MaterialStateProperty.all(
                                        (_checkedValue1 && _selectedElement) ? Color(0xffF52079) : Color(0xFFF6F6FF),
                                      ),
                                      shape: MaterialStateProperty.all(
                                        RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.09),
                                        ),
                                      ),
                                      side: _checkedValue1 && _reasonController.text != "" ? MaterialStateProperty.all(BorderSide(color: Colors.transparent)) : MaterialStateProperty.all(BorderSide(color: Color(0xFFD6D6E8)))),
                                  onPressed: (_checkedValue1 && _reasonController.text != "")
                                      ? () async {
                                          gotoPageGlobal(context, ConfirmAccountDesactivation());
                                        }
                                      : null,
                                  child: Padding(
                                    padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.height * 0.01, MediaQuery.of(context).size.width * 0.01, MediaQuery.of(context).size.height * 0.01, MediaQuery.of(context).size.width * 0.01),
                                    child: Text(
                                      "Continue",
                                      style: GoogleFonts.poppins(
                                        color: _checkedValue1 && _reasonController.text != "" ? Colors.white : Color(0xffADADC8),
                                        fontWeight: FontWeight.w600,
                                        fontSize: 16,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      ))
                ],
              ),
            ),
            HeaderNavigation(_isVisible, _isWalletVisible, _isNotificationVisible)
          ],
        ),
      ),
    );
  }
}