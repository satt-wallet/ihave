import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:math';
import 'package:flutter/material.dart' hide Action;
import 'package:cloud_firestore/cloud_firestore.dart' hide Transaction;
import 'package:eth_sig_util/eth_sig_util.dart';
import 'package:eth_sig_util/util/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:flutter_svg/svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;
import 'package:ihave/controllers/LoginController.dart';
import 'package:ihave/service/walletConnectBackGroundService.dart';
import 'package:ihave/ui/WelcomeScreen.dart';
import 'package:ihave/ui/staticElements/headerNavigation.dart';
import 'package:ihave/widgets/alertDialogWidget.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:wallet_connect/wallet_connect.dart';
import 'package:web3dart/web3dart.dart';
import '../controllers/walletController.dart';
import '../model/crypto.dart';
import '../util/qrCodeScan.dart';
import '../util/utils.dart';
import '../widgets/customAppBar.dart';
import '../widgets/customBottomBar.dart';
import 'package:flutter_toggle_tab/flutter_toggle_tab.dart';

class WalletConnect extends StatefulWidget {
  WalletConnect({Key? key}) : super(key: key);

  @override
  _WalletConnectState createState() => _WalletConnectState();
}

const maticRpcUri = 'https://eth.llamarpc.com/';
const maticRpcUriEth = 'https://eth.llamarpc.com/';

class _WalletConnectState extends State<WalletConnect> {
  late WCClient _wcClient;
  late SharedPreferences _prefs;
  late InAppWebViewController _webViewController;
  late TextEditingController _textEditingController;
  String walletAddress = "";
  EthPrivateKey? privateKey;
  late double balance;
  late Wallet wallet;
  bool connected = false;
  WCSessionStore? _sessionStore;
  bool _isVisible = false;
  bool _isProfileVisible = false;
  bool _isWalletVisible = false;
  bool _isNotificationVisible = false;
  int _localChainId = 56;
  late StreamSubscription subscription;
  bool _hasNetwork = true;
  int _selectedIndex = 0;
  bool _obscureText = true;
  bool _obscureText2 = true;
  bool _approveClicked = false;
  bool _dialogTransactionPasswordOpened = false;
  late WCBGService walletConnectService;
  final TextEditingController _transactionPassword = new TextEditingController();
  bool passwrong = false;
  FocusNode _transactionPasswordFocus = new FocusNode();
  final _web3client = Web3Client(
    maticRpcUri,
    http.Client(),
  );
  final _web3clientEth = Web3Client(
    maticRpcUriEth,
    http.Client(),
  );

  @override
  void initState() {
    setState(() {
      connected = Provider.of<WalletController>(context, listen: false).walletConnectConnected = connected;
    });
    _initialize();
    super.initState();
  }

  _initialize() async {
    _wcClient = WCClient(
      onSessionRequest: _onSessionRequest,
      onFailure: _onSessionError,
      onDisconnect: _onSessionClosed,
      onEthSign: _onSign,
      onEthSignTransaction: _onSignTransaction,
      onEthSendTransaction: _onSendTransaction,
      onCustomRequest: (_, __) {},
      onConnect: _onConnect,
    );
    walletAddress = LoginController.apiService.walletAddress ?? "";
    _prefs = await SharedPreferences.getInstance();
    _textEditingController = TextEditingController();
    connected = false;
    if (_prefs.getString('session') != null && Provider.of<WalletController>(context, listen: false).privateKey != null) {
      _connectToPreviousSession();
      privateKey = Provider.of<WalletController>(context, listen: false).privateKey;
    }
    subscription = InternetConnectionChecker().onStatusChange.listen((status) {
      final hasInternet = status == InternetConnectionStatus.connected;
      setState(() {
        _hasNetwork = hasInternet;
      });
    });
  }

  @override
  void dispose() {
    subscription.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    setState(() {
      LoginController.walletConnectService.userToken = Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "";
      walletConnectService = LoginController.walletConnectService;
      connected = Provider.of<WalletController>(context, listen: false).walletConnectConnected;
      _isVisible = Provider.of<WalletController>(context, listen: true).isSideMenuVisible;
      _isProfileVisible = Provider.of<WalletController>(context, listen: true).isProfileVisible;
      _isWalletVisible = Provider.of<WalletController>(context, listen: true).isWalletVisible;
      _isNotificationVisible = Provider.of<WalletController>(context, listen: true).isNotificationVisible;
    });
    return _hasNetwork
        ? WillPopScope(
            onWillPop: () {
              return Future.value(true);
            },
            child: Scaffold(
                resizeToAvoidBottomInset: false,
                appBar: CustomAppBar(),
                bottomNavigationBar: CustomBottomBar(),
                body: SingleChildScrollView(
                  child: Stack(
                    children: [
                      Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(top: 50),
                            child: Text(
                              "WalletConnect",
                              style: GoogleFonts.poppins(
                                color: Color(0xFF1F2337),
                                fontSize: MediaQuery.of(context).size.height * 0.03,
                                fontWeight: FontWeight.w700,
                                fontStyle: FontStyle.normal,
                              ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.045),
                            child: Image.asset(connected ? "images/scanwallet.png" : "images/walletconnect.png"),
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.05, left: MediaQuery.of(context).size.width * 0.015, right: MediaQuery.of(context).size.width * 0.015),
                            child: Text(
                              connected ? "Congrats ! \nYou are now connected to " : "WalletConnect is the Web3 standard to connect blockchain wallets to dapps.",
                              textAlign: TextAlign.center,
                              style: GoogleFonts.poppins(
                                color: Color(0xFF1F2337),
                                fontSize: MediaQuery.of(context).size.height * 0.0175,
                                fontWeight: FontWeight.w500,
                                fontStyle: FontStyle.normal,
                              ),
                            ),
                          ),
                          Visibility(
                            visible: !connected,
                            child: Padding(
                              padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.025, left: MediaQuery.of(context).size.width * 0.075, right: MediaQuery.of(context).size.width * 0.075),
                              child: Text(
                                "Select your Network BEP20 or ERC20 and tap on \"scan code\".",
                                textAlign: TextAlign.center,
                                style: GoogleFonts.poppins(
                                  color: Color(0xFF1F2337),
                                  fontSize: MediaQuery.of(context).size.height * 0.0175,
                                  fontWeight: FontWeight.w500,
                                  fontStyle: FontStyle.normal,
                                ),
                              ),
                            ),
                          ),
                          Container(
                            child: !connected
                                ? Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.all(12.0),
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          crossAxisAlignment: CrossAxisAlignment.center,
                                          children: [
                                            Padding(
                                              padding: const EdgeInsets.all(8.0),
                                              child: FlutterToggleTab(
                                                width: MediaQuery.of(context).size.width * 0.15,
                                                height: MediaQuery.of(context).size.height * 0.06,
                                                borderRadius: 30,
                                                selectedIndex: _selectedIndex,
                                                selectedBackgroundColors: [Color(0xFFF6F6FF)],
                                                unSelectedBackgroundColors: [Color(0xFFFFFFFF)],
                                                selectedTextStyle: TextStyle(color: Colors.black, fontSize: MediaQuery.of(context).size.height * 0.0175, fontWeight: FontWeight.w700),
                                                unSelectedTextStyle: TextStyle(color: Colors.black, fontSize: MediaQuery.of(context).size.height * 0.0175, fontWeight: FontWeight.w400),
                                                labels: ["BEP20", "ERC20"],
                                                selectedLabelIndex: (index) {
                                                  setState(() {
                                                    _selectedIndex = index;
                                                    _localChainId = _selectedIndex == 0 ? 56 : 1;
                                                    walletConnectService.localChainId = _localChainId;
                                                  });
                                                },
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(top: 25),
                                        child: ElevatedButton(
                                            onPressed: () async {
                                              if (walletAddress.isNotEmpty) {
                                                var result;
                                                if (Platform.isAndroid) {
                                                  var status = await Permission.photos.status;

                                                  result = await Permission.camera.request();
                                                  if (result == PermissionStatus.granted) {
                                                    Navigator.push(
                                                      context,
                                                      MaterialPageRoute(builder: (_) => QrCodeScan()),
                                                    ).then((value) {
                                                      if (value != null) {
                                                        final session = WCSession.from(value);
                                                        final peerMeta = WCPeerMeta(
                                                          name: "iHave",
                                                          url: "https://iHave.io",
                                                          description: "iHave Wallet",
                                                          icons: ["https://gblobscdn.gitbook.com/spaces%2F-LJJeCjcLrr53DcT1Ml7%2Favatar.png"],
                                                        );
                                                        _wcClient.connectNewSession(session: session, peerMeta: peerMeta);
                                                      }
                                                    });
                                                  } else {
                                                    showDialog(
                                                        context: context,
                                                        barrierDismissible: false,
                                                        builder: (BuildContext context) => CupertinoAlertDialog(
                                                              title: Text('Camera Permission'),
                                                              content: Text('This app needs camera access to  scan QR code '),
                                                              actions: <Widget>[
                                                                CupertinoDialogAction(
                                                                  child: Text('Deny'),
                                                                  onPressed: () => Navigator.of(context).pop(),
                                                                ),
                                                                CupertinoDialogAction(
                                                                  child: Text('Settings'),
                                                                  onPressed: () => openAppSettings(),
                                                                ),
                                                              ],
                                                            ));
                                                  }
                                                } else {
                                                  Navigator.push(
                                                    context,
                                                    MaterialPageRoute(builder: (_) => QrCodeScan()),
                                                  ).then((value) {
                                                    if (value != null) {
                                                      final session = WCSession.from(value);
                                                      final peerMeta = WCPeerMeta(
                                                        name: "iHave",
                                                        url: "https://iHave.io",
                                                        description: "iHave Wallet",
                                                        icons: ["https://gblobscdn.gitbook.com/spaces%2F-LJJeCjcLrr53DcT1Ml7%2Favatar.png"],
                                                      );
                                                      _wcClient.connectNewSession(session: session, peerMeta: peerMeta);
                                                    }
                                                  });
                                                }
                                              } else {
                                                displayDialog(context, "Error", "Please check your wallet Address.");
                                              }
                                            },
                                            style: ElevatedButton.styleFrom(
                                                minimumSize: Size(MediaQuery.of(context).size.width * 0.47, MediaQuery.of(context).size.height * 0.06),
                                                side: BorderSide(color: Color(0xFF4048FF)),
                                                primary: Color(0xFF4048FF),
                                                shape: RoundedRectangleBorder(
                                                  borderRadius: BorderRadius.circular(30.0),
                                                )),
                                            child: Text(
                                              "Scan Code",
                                              style: GoogleFonts.poppins(fontWeight: FontWeight.w600, fontSize: 19),
                                            )),
                                      ),
                                    ],
                                  )
                                : Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: [
                                      Center(
                                        child: Column(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          crossAxisAlignment: CrossAxisAlignment.center,
                                          children: [
                                            Padding(
                                              padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.08),
                                              child: Image.network(_sessionStore?.remotePeerMeta.icons[0] ?? ""),
                                            ),
                                            Padding(
                                              padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.02),
                                              child: Text(
                                                "${_sessionStore?.remotePeerMeta.url}",
                                                textAlign: TextAlign.center,
                                                style: GoogleFonts.poppins(
                                                  color: Color(0xFF4048FF),
                                                  fontSize: 13,
                                                  fontWeight: FontWeight.w700,
                                                ),
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.02, bottom: MediaQuery.of(context).size.height * 0.02),
                                        child: ElevatedButton(
                                          onPressed: () async {
                                            await _wcClient.killSession();
                                            await _wcClient.onDisconnect;
                                            setState(() {
                                              connected = false;
                                              Provider.of<WalletController>(context, listen: false).walletConnectConnected = connected;
                                            });
                                          },
                                          style: ElevatedButton.styleFrom(
                                              minimumSize: Size(MediaQuery.of(context).size.width * 0.27, MediaQuery.of(context).size.height * 0.05),
                                              side: BorderSide(color: Colors.white),
                                              primary: Colors.white,
                                              shape: RoundedRectangleBorder(
                                                borderRadius: BorderRadius.circular(30.0),
                                              )),
                                          child: Text(
                                            "Disconnect",
                                            style: GoogleFonts.poppins(color: Color(0xFF4048FF), fontWeight: FontWeight.w600, fontSize: 13),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                          ),
                        ],
                      ),
                      HeaderNavigation(_isVisible, _isWalletVisible, _isNotificationVisible),
                    ],
                  ),
                )),
          )
        : Stack(
            children: [
              Container(
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                color: Colors.white,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Center(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          "No Internet Connection , please check your network",
                          textAlign: TextAlign.center,
                          style: GoogleFonts.poppins(color: Colors.black, fontSize: 14),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: ElevatedButton(
                        onPressed: () {},
                        child: Text("Retry"),
                      ),
                    ),
                  ],
                ),
              )
            ],
          );
  }

  _connectToPreviousSession() async {
    final _sessionSaved = _prefs.getString('session');
    _sessionStore = _sessionSaved != null ? WCSessionStore.fromJson(jsonDecode(_sessionSaved)) : null;
    if (_sessionStore != null) {
      _wcClient.connectFromSessionStore(_sessionStore!);
      setState(() {
        connected = true;
        Provider.of<WalletController>(context, listen: false).walletConnectConnected = connected;
      });
      if (_localChainId == 56) {
        var balance = await _web3client.getBalance(EthereumAddress.fromHex(walletAddress));
      } else {
        var balance = await _web3clientEth.getBalance(EthereumAddress.fromHex(walletAddress));
      }
    } else {
      //do nothing
    }
  }

  _onConnect() {}

  _onSessionRequest(int id, WCPeerMeta peerMeta) {
    setState(() {
      _approveClicked = false;
      _transactionPassword.text = "";
    });

    showDialog(
        useRootNavigator: false,
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          _dialogTransactionPasswordOpened = true;
          passwrong = false;
          _transactionPassword.text = "";

          return WillPopScope(
            onWillPop: () {
              _wcClient.rejectSession();
              setState(() {
                _transactionPassword.text = "";
                passwrong = false;
              });
              return Future.value(true);
            },
            child: StatefulBuilder(builder: (context, setState) {
              return Container(
                child: AlertDialog(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(30)),
                  ),
                  content: Stack(children: [
                    SingleChildScrollView(
                      child: Container(
                        child: Column(
                          children: [
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Spacer(),
                                Spacer(),
                              ],
                            ),
                            if (peerMeta.icons.isNotEmpty)
                              Container(
                                padding: EdgeInsets.only(bottom: 15.0, top: 8.0),
                                child: Image.network(
                                  peerMeta.icons.first,
                                  width: 60,
                                  height: 60,
                                ),
                              ),
                            Padding(
                              padding: EdgeInsets.only(top: 8.0, left: 15.0, right: 15.0, bottom: 8.0),
                              child: Text(
                                peerMeta.name,
                                style: GoogleFonts.poppins(
                                  fontSize: 17,
                                  fontWeight: FontWeight.w700,
                                ),
                              ),
                            ),
                            if (peerMeta.description.isNotEmpty)
                              Padding(
                                padding: const EdgeInsets.only(bottom: 8.0),
                                child: Text(
                                  peerMeta.description,
                                  style: GoogleFonts.poppins(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                            if (peerMeta.url.isNotEmpty)
                              Padding(
                                padding: EdgeInsets.only(left: 0.0, top: 8.0, right: MediaQuery.of(context).size.width * 0.3),
                                child: Column(
                                  children: [
                                    Text(
                                      'Connection to',
                                      style: GoogleFonts.poppins(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w500,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            Padding(
                              padding: EdgeInsets.only(left: 0.0, top: 1.0, right: MediaQuery.of(context).size.width * 0.13),
                              child: Text(
                                '${peerMeta.url}',
                                style: GoogleFonts.poppins(
                                  color: Color(0xFF4048FF),
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                ),
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.02, bottom: MediaQuery.of(context).size.height * 0.01),
                              child: Container(
                                width: MediaQuery.of(context).size.width * 0.85,
                                child: TextFormField(
                                  style: TextStyle(fontSize: MediaQuery.of(context).size.width * 0.045),
                                  textAlignVertical: TextAlignVertical.center,
                                  textAlign: TextAlign.left,
                                  keyboardType: TextInputType.visiblePassword,
                                  obscureText: _obscureText2,
                                  controller: _transactionPassword,
                                  onChanged: (val) {
                                    TextSelection previousSelection = _transactionPassword.selection;
                                    _transactionPassword.text = val;

                                    _transactionPassword.selection = previousSelection;

                                    setState(() {
                                      passwrong = false;
                                    });
                                  },
                                  decoration: InputDecoration(
                                      hintText: "Transaction Password",
                                      hintStyle: GoogleFonts.poppins(color: Color(0xFF75758F), fontWeight: FontWeight.w600, fontStyle: FontStyle.normal, fontSize: MediaQuery.of(context).size.height * 0.0136),
                                      fillColor: Colors.white,
                                      focusColor: Colors.white,
                                      hoverColor: Colors.grey,
                                      filled: true,
                                      isDense: true,
                                      prefixIcon: Padding(
                                        padding: EdgeInsets.all(MediaQuery.of(context).size.width * 0.017),
                                        child: SvgPicture.asset(
                                          "images/keyIcon.svg",
                                          color: Colors.orange,
                                          height: MediaQuery.of(context).size.width * 0.06,
                                          width: MediaQuery.of(context).size.width * 0.06,
                                        ),
                                      ),
                                      suffixIcon: Padding(
                                        padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.02),
                                        child: GestureDetector(
                                          onTap: () {
                                            setState(() {
                                              _obscureText2 = !_obscureText2;
                                            });
                                          },
                                          child: Icon(
                                            _obscureText2 ? Icons.visibility_off : Icons.visibility,
                                            size: MediaQuery.of(context).size.width * 0.037,
                                          ),
                                        ),
                                      ),
                                      contentPadding: EdgeInsets.symmetric(vertical: 5, horizontal: 5),
                                      enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.grey.shade400)),
                                      focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.grey)),
                                      errorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red)),
                                      focusedErrorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red))),
                                ),
                              ),
                            ),
                            Center(
                              child: passwrong
                                  ? Text(
                                      "Wrong Password",
                                      style: GoogleFonts.poppins(
                                        color: Color(0xFFd63384),
                                        fontWeight: FontWeight.w600,
                                        fontSize: MediaQuery.of(context).size.height * 0.015,
                                        fontStyle: FontStyle.normal,
                                      ),
                                    )
                                  : null,
                            ),
                            Row(
                              children: [
                                Expanded(
                                  child: TextButton(
                                    style: ButtonStyle(
                                      backgroundColor: MaterialStateProperty.all(
                                        Color(0XFFF52079),
                                      ),
                                      shape: MaterialStateProperty.all(
                                        RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.08),
                                        ),
                                      ),
                                    ),
                                    onPressed: () {
                                      _wcClient.rejectSession();
                                      setState(() {
                                        _transactionPassword.text = "";
                                        passwrong = false;
                                        _dialogTransactionPasswordOpened = false;
                                      });
                                      Navigator.of(context).pop();
                                    },
                                    child: Text(
                                      'Reject',
                                      style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16.0, color: Colors.white),
                                    ),
                                  ),
                                ),
                                const SizedBox(width: 16.0),
                                Expanded(
                                  child: TextButton(
                                    style: ButtonStyle(
                                      backgroundColor: MaterialStateProperty.all(
                                        Color(0XFF4048FF),
                                      ),
                                      shape: MaterialStateProperty.all(
                                        RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.08),
                                        ),
                                      ),
                                    ),
                                    onPressed: () async {
                                      setState(() {
                                        _approveClicked = true;
                                      });
                                      _sessionStore = _wcClient.sessionStore;
                                      await _prefs.setString('session', jsonEncode(_wcClient.sessionStore.toJson()));
                                      var result = await generateKey();
                                      if (result == "wrong password") {
                                        setState(() {
                                          Fluttertoast.showToast(msg: "Wrong password");
                                          passwrong = true;
                                          connected = false;
                                        });
                                      } else {
                                        Wallet wallet = Wallet.fromJson(result, _transactionPassword.text);
                                        this.wallet = wallet;
                                        await _wcClient.approveSession(
                                          accounts: [walletAddress],
                                          chainId: _localChainId,
                                        );
                                        Navigator.of(context, rootNavigator: false).pop();
                                        return handleStateConnection();
                                      }
                                    },
                                    child: Text(
                                      'Approve',
                                      style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16.0, color: Colors.white),
                                    ),
                                  ),
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
                  ]),
                ),
              );
            }),
          );
        });
  }

  _onSessionError(dynamic message) {
    setState(() {
      connected = false;
      Provider.of<WalletController>(context, listen: false).walletConnectConnected = connected;
    });
  }

  _onSessionClosed(int? code, String? reason) {
    _prefs.remove('session');
    if (mounted) {
      Provider.of<WalletController>(context, listen: false).walletConnectConnected = false;
      setState(() {
        connected = false;
      });
    }
  }

  handleStateConnection() {
    setState(() {
      connected = true;
      passwrong = false;
      Provider.of<WalletController>(context, listen: false).walletConnectConnected = connected;
      privateKey = wallet.privateKey;
      walletConnectService.privateKey = privateKey;
      Provider.of<WalletController>(context, listen: false).privateKey = privateKey;
    });
  }

  _onSignTransaction(
    int id,
    WCEthereumTransaction ethereumTransaction,
  ) {
    var cryptoPrice = 0.0;
    var cryptos = (json.decode(_prefs.getString("user-list") ?? "") as List).map((i) => Crypto.fromJson(i)).toList();
    if (_localChainId == 56) {
      cryptoPrice = cryptos.firstWhere((e) => e.symbol?.toLowerCase() == "bnb").price ?? 0.0;
    } else {
      cryptoPrice = cryptos.firstWhere((e) => e.symbol?.toLowerCase() == "eth").price ?? 0.0;
    }
    walletConnectService.onSignTransaction(id, ethereumTransaction, _wcClient, cryptoPrice);
  }

  _onSendTransaction(
    int id,
    WCEthereumTransaction ethereumTransaction,
  ) {
    var cryptoPrice = 0.0;
    var cryptos = (json.decode(_prefs.getString("user-list") ?? "") as List).map((i) => Crypto.fromJson(i)).toList();
    if (_localChainId == 56) {
      cryptoPrice = cryptos.firstWhere((e) => e.symbol?.toLowerCase() == "bnb").price ?? 0.0;
    } else {
      cryptoPrice = cryptos.firstWhere((e) => e.symbol?.toLowerCase() == "eth").price ?? 0.0;
    }
    walletConnectService.onSendTransaction(id, ethereumTransaction, _wcClient, cryptoPrice);
  }

  _onTransaction({
    required int id,
    required WCEthereumTransaction ethereumTransaction,
    required String title,
    required VoidCallback onConfirm,
    required VoidCallback onReject,
  }) async {
    ContractFunction? contractFunction;
    BigInt gasInGwei = BigInt.parse(ethereumTransaction.gasPrice ?? '0');
    double gasPrice = calculateGas(gasInGwei);

    try {
      final abiUrl = handleAbiUrl(ethereumTransaction);
      final res = await http.get(Uri.parse(abiUrl));
      final Map<String, dynamic> resMap = jsonDecode(res.body);
      final abi = ContractAbi.fromJson(resMap['result'], '');
      final contract = DeployedContract(abi, EthereumAddress.fromHex(ethereumTransaction.to ?? ""));
      final dataBytes = hexToBytes(ethereumTransaction.data ?? "");
      final funcBytes = dataBytes.take(4).toList();
      final maibiFunctions = contract.functions.where((element) => listEquals<int>(element.selector, funcBytes));
      if (maibiFunctions.isNotEmpty) {
        contractFunction = maibiFunctions.first;
      }
      if (gasPrice == BigInt.zero) {
        gasInGwei = _localChainId == 56 ? await _web3client.estimateGas() : await _web3clientEth.estimateGas();
        gasPrice = calculateGas(gasInGwei);
      }
    } catch (e, trace) {}
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return WillPopScope(
            onWillPop: () => Future.value(false),
            child: StatefulBuilder(builder: (context, setState) {
              return AlertDialog(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(30)),
                ),
                content: SingleChildScrollView(
                  child: Column(
                    children: [
                      Container(
                        child: _wcClient.remotePeerMeta!.icons.isNotEmpty
                            ? Container(
                                height: 100.0,
                                width: 100.0,
                                padding: EdgeInsets.only(bottom: 8.0, top: 8.0),
                                child: Image.network(
                                  _wcClient.remotePeerMeta!.icons.first,
                                  width: 80,
                                  height: 80,
                                ),
                              )
                            : null,
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 8.0, left: 15.0, right: 15.0, bottom: 8.0),
                        child: Text(
                          _wcClient.remotePeerMeta!.name,
                          style: GoogleFonts.poppins(
                            fontSize: 17,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                      Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.only(bottom: 8.0, top: 8.0),
                        child: Text(
                          title,
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 18.0,
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 8.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Receipient',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 16.0,
                              ),
                            ),
                            const SizedBox(height: 8.0),
                            Text(
                              '${ethereumTransaction.to}',
                              style: TextStyle(fontSize: 16.0),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 8.0),
                        child: Column(
                          children: [
                            Row(
                              children: [
                                Expanded(
                                  flex: 2,
                                  child: Text(
                                    'Transaction Fee',
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16.0,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            Row(
                              children: [
                                Expanded(
                                  child: Text(
                                    _localChainId == 56 ? '${EthConversions.weiToEthUnTrimmed(gasInGwei * BigInt.parse(ethereumTransaction.gas ?? '0'), 18)} BNB' : '${EthConversions.weiToEthUnTrimmed(gasInGwei * BigInt.parse(ethereumTransaction.gas ?? '0'), 18)} ETH',
                                    style: TextStyle(fontSize: 16.0),
                                  ),
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 0.0),
                        child: Column(
                          children: [],
                        ),
                      ),
                      if (contractFunction != null)
                        Padding(
                          padding: EdgeInsets.only(bottom: 4.0, right: MediaQuery.of(context).size.width * 0.2),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                'Function',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 16.0,
                                ),
                              ),
                              const SizedBox(height: 2.0),
                              Text(
                                '${contractFunction.name}',
                                style: TextStyle(fontSize: 16.0),
                              ),
                            ],
                          ),
                        ),
                      Theme(
                        data: Theme.of(context).copyWith(dividerColor: Colors.transparent),
                        child: Padding(
                          padding: EdgeInsets.only(bottom: 4.0),
                          child: ExpansionTile(
                            tilePadding: EdgeInsets.zero,
                            title: Text(
                              'Data',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 16.0,
                              ),
                            ),
                            children: [
                              Text(
                                '${ethereumTransaction.data}',
                                style: TextStyle(fontSize: 16.0),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.05),
                        child: Row(
                          children: [
                            Expanded(
                              child: TextButton(
                                style: ButtonStyle(
                                  backgroundColor: MaterialStateProperty.all(
                                    Color(0XFFF52079),
                                  ),
                                  shape: MaterialStateProperty.all(
                                    RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.08),
                                    ),
                                  ),
                                ),
                                onPressed: onReject,
                                child: Text(
                                  'REJECT',
                                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16.0, color: Colors.white),
                                ),
                              ),
                            ),
                            const SizedBox(width: 16.0),
                            Expanded(
                              child: TextButton(
                                style: ButtonStyle(
                                  backgroundColor: MaterialStateProperty.all(
                                    Color(0XFF4048FF),
                                  ),
                                  shape: MaterialStateProperty.all(
                                    RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.08),
                                    ),
                                  ),
                                ),
                                onPressed: onConfirm,
                                child: Text(
                                  'CONFIRM',
                                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16.0, color: Colors.white),
                                ),
                              ),
                            ),
                            const SizedBox(width: 20.0),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              );
            }),
          );
        });
  }

  _onSign(
    int id,
    WCEthereumSignMessage ethereumSignMessage,
  ) {
    final decoded = (ethereumSignMessage.type == WCSignType.TYPED_MESSAGE) ? ethereumSignMessage.data! : ascii.decode(hexToBytes(ethereumSignMessage.data!));
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (_) {
        return WillPopScope(
          onWillPop: () => Future.value(false),
          child: SimpleDialog(
            title: Column(
              children: [
                if (_wcClient.remotePeerMeta!.icons.isNotEmpty)
                  Container(
                    height: 100.0,
                    width: 100.0,
                    padding: const EdgeInsets.only(bottom: 8.0),
                    child: Image.network(_wcClient.remotePeerMeta!.icons.first),
                  ),
                Text(
                  _wcClient.remotePeerMeta!.name,
                  style: TextStyle(
                    fontWeight: FontWeight.normal,
                    fontSize: 20.0,
                  ),
                ),
              ],
            ),
            contentPadding: const EdgeInsets.fromLTRB(16.0, 12.0, 16.0, 16.0),
            children: [
              Container(
                alignment: Alignment.center,
                padding: const EdgeInsets.only(bottom: 8.0),
                child: Text(
                  'Sign Message',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 18.0,
                  ),
                ),
              ),
              Theme(
                data: Theme.of(context).copyWith(dividerColor: Colors.transparent),
                child: Padding(
                  padding: const EdgeInsets.only(bottom: 8.0),
                  child: ExpansionTile(
                    tilePadding: EdgeInsets.zero,
                    title: Text(
                      'Message',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 16.0,
                      ),
                    ),
                    children: [
                      Text(
                        decoded,
                        style: TextStyle(fontSize: 16.0),
                      ),
                    ],
                  ),
                ),
              ),
              Row(
                children: [
                  Expanded(
                    child: TextButton(
                      style: TextButton.styleFrom(
                        primary: Colors.white,
                        backgroundColor: Theme.of(context).colorScheme.secondary,
                      ),
                      onPressed: () async {
                        String signedDataHex;
                        if (ethereumSignMessage.type == WCSignType.TYPED_MESSAGE) {
                          signedDataHex = EthSigUtil.signTypedData(
                            privateKey: privateKey?.privateKeyInt?.toString() ?? "",
                            jsonData: ethereumSignMessage.data!,
                            version: TypedDataVersion.V4,
                          );
                        } else {
                          final creds = privateKey ?? (await generateKey()).privateKey;
                          final encodedMessage = hexToBytes(ethereumSignMessage.data!);
                          final signedData = await creds.signPersonalMessage(encodedMessage);
                          signedDataHex = bytesToHex(signedData, include0x: true);
                        }
                        if (signedDataHex != "") {
                          _wcClient.approveRequest<String>(
                            id: id,
                            result: signedDataHex,
                          );
                          Navigator.of(context, rootNavigator: true).pop();
                        }
                      },
                      child: Text('SIGN'),
                    ),
                  ),
                  const SizedBox(width: 16.0),
                  Expanded(
                    child: TextButton(
                      style: TextButton.styleFrom(
                        primary: Colors.white,
                        backgroundColor: Theme.of(context).colorScheme.secondary,
                      ),
                      onPressed: () {
                        _wcClient.rejectRequest(id: id);
                        Navigator.pop(context);
                      },
                      child: Text('REJECT'),
                    ),
                  ),
                ],
              ),
            ],
          ),
        );
      },
    );
  }

  Transaction _wcEthTxToWeb3Tx(WCEthereumTransaction ethereumTransaction) {
    return Transaction(
      from: EthereumAddress.fromHex(ethereumTransaction.from),
      to: EthereumAddress.fromHex(ethereumTransaction.to ?? ""),
      maxGas: ethereumTransaction.gasLimit != null ? int.tryParse(ethereumTransaction.gasLimit!) : null,
      gasPrice: ethereumTransaction.gasPrice != null ? EtherAmount.inWei(BigInt.parse(ethereumTransaction.gasPrice!)) : null,
      value: EtherAmount.inWei(BigInt.parse(ethereumTransaction.value ?? '0')),
      data: hexToBytes(ethereumTransaction.data ?? ""),
      nonce: ethereumTransaction.nonce != null ? int.tryParse(ethereumTransaction.nonce!) : null,
    );
  }

  handleAbiUrl(WCEthereumTransaction ethereumTransaction) {
    if (_localChainId == 56) {
      return 'https://api.bscscan.com/api?module=contract&action=getabi&address=${ethereumTransaction.to}&apikey=1KK5VK8HGMSJ86WVDBXH2MGIT8CXKQN6QP';
    } else {
      return 'https://api.bscscan.com/api?module=contract&action=getabi&address=${ethereumTransaction.to}&apikey=8S1PRHAZAMHPU4YTC4HU5YT2MT21B1BQ3J';
    }
  }

  double calculateGas(BigInt gasInGwei) {
    var gas = 0.0;
    var cryptoPrice = 0.0;
    if (_localChainId == 56) {
      cryptoPrice = Provider.of<WalletController>(context, listen: false).cryptos.firstWhere((e) => e.symbol?.toLowerCase() == "bnb").price ?? 0.0;
    } else {
      cryptoPrice = Provider.of<WalletController>(context, listen: false).cryptos.firstWhere((e) => e.symbol?.toLowerCase() == "eth").price ?? 0.0;
    }
    gas = ((gasInGwei * BigInt.parse("60000")) / BigInt.parse("1000000000000000000")) * cryptoPrice;
    return gas;
  }

  Future generateKey() async {
    var result = await LoginController.apiService.getBlockchainKey(Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "", _transactionPassword.text);
    return result;
  }
}
