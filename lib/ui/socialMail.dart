import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:ihave/controllers/LoginController.dart';
import 'package:ihave/controllers/walletController.dart';
import 'package:ihave/ui/WelcomeScreen.dart';
import 'package:ihave/ui/creatwallet.dart';
import 'package:ihave/ui/signin.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:ihave/ui/transactionpassword.dart';
import 'package:provider/provider.dart';

import '../util/utils.dart';
import 'activationMail.dart';

class SocialMail extends StatefulWidget {
  SocialMail({Key? key}) : super(key: key);

  @override
  _SocialMailState createState() => _SocialMailState();
}

class _SocialMailState extends State<SocialMail> {
  final FocusNode _pinPutFocusNode = FocusNode();
  final TextEditingController _pinPutController = TextEditingController();
  bool isLoggedIn = false;
  final _formKey = GlobalKey<FormState>();
  String newPassword = "";
  String newPasswordConfirm = "";
  bool isExist = false;
  bool isConfirmedPassword = false;
  bool isResend = false;
  bool isChanged = false;
  bool _firstNameEdited = false;
  bool _nameEdited = false;
  bool _mailPattern = false;
  bool _mailEdited = false;
  bool isFtEmpty = false;
  bool isLtEmpty = false;
  bool isMailEmpty = false;
  TextEditingController firstNameController = new TextEditingController();
  TextEditingController lastNameController = new TextEditingController();
  TextEditingController mailController = new TextEditingController();

  @override
  void initState() {
    setState(() {
      firstNameController.text = Provider.of<LoginController>(context, listen: false).userDetails?.firstName ?? "";
      lastNameController.text = Provider.of<LoginController>(context, listen: false).userDetails?.lastName ?? "";
      mailController.text = Provider.of<LoginController>(context, listen: false).userDetails?.email ?? "";
      _firstNameEdited = (firstNameController.text.length > 0);
      _nameEdited = (lastNameController.text.length > 0);
      _mailEdited = (mailController.text.length > 0);
      _mailPattern = (RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(mailController.text));
    });
  }

  @override
  Widget build(BuildContext context) {
    final isKeyboard = MediaQuery.of(context).viewInsets.bottom != 0;
    return WillPopScope(
        onWillPop: () async {
          return false;
        },
        child: Scaffold(
            resizeToAvoidBottomInset: false,
            appBar: null,
            body: GestureDetector(
                onTap: () {
                  FocusScope.of(context).requestFocus(new FocusNode());
                },
                child: Stack(
                  children: [
                    Container(
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                            colors: [
                              const Color(0xFF4048FF),
                              const Color(0xFF1F2337),
                            ],
                            begin: const FractionalOffset(0.0, -0.1),
                            end: const FractionalOffset(0.0, 0.3),
                            stops: [0.0, 1.0],
                            tileMode: TileMode.clamp),
                      ),
                      child: SingleChildScrollView(
                        child: Column(
                          children: [
                            Container(
                              alignment: Alignment.topLeft,
                              padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.037, MediaQuery.of(context).size.width * 0.1, 0, 0),
                              child: Row(
                                children: [
                                  Padding(
                                    padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.28, MediaQuery.of(context).size.width * 0.05, MediaQuery.of(context).size.width * 0.1, 0),
                                    child: SvgPicture.asset(
                                      'images/logo-auth.svg',
                                      alignment: Alignment.center,
                                      width: MediaQuery.of(context).size.height * 0.04,
                                      height: MediaQuery.of(context).size.height * 0.04,
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(
                                      top: MediaQuery.of(context).size.width * 0.07,
                                      right: MediaQuery.of(context).size.width * 0.01,
                                    ),
                                    child: InkWell(
                                      child: SvgPicture.asset(
                                        'images/new-logout.svg',
                                        alignment: Alignment.center,
                                        width: MediaQuery.of(context).size.width * 0.2,
                                        height: MediaQuery.of(context).size.height * 0.022,
                                      ),
                                      onTap: () {
                                        Provider.of<LoginController>(context, listen: false).logout();
                                        gotoPageGlobal(context, Signin());
                                      },
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Form(
                              key: _formKey,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  SizedBox(
                                    height: MediaQuery.of(context).size.width * 0.1,
                                  ),
                                  Text(
                                    "Update your\n account",
                                    textAlign: TextAlign.center,
                                    style: GoogleFonts.poppins(fontWeight: FontWeight.w600, fontStyle: FontStyle.normal, fontSize: MediaQuery.of(context).size.width * 0.09, color: Colors.white),
                                  ),
                                  Text(
                                    "Check and modify your personal \ninformation if necessary.",
                                    textAlign: TextAlign.center,
                                    style: GoogleFonts.poppins(
                                      fontSize: MediaQuery.of(context).size.width * 0.04,
                                      color: Colors.white,
                                      fontStyle: FontStyle.normal,
                                      fontWeight: FontWeight.w500,
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(top: MediaQuery.of(context).size.width * 0.06, left: MediaQuery.of(context).size.width * 0.12, bottom: MediaQuery.of(context).size.width * 0.007),
                                    child: Container(
                                      alignment: Alignment.centerLeft,
                                      child: Text(
                                        "FIRST NAME",
                                        style: GoogleFonts.poppins(
                                          color: Colors.white,
                                          fontSize: MediaQuery.of(context).size.width * 0.033,
                                          fontWeight: FontWeight.w600,
                                          fontStyle: FontStyle.normal,
                                        ),
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(),
                                    child: Container(
                                      width: MediaQuery.of(context).size.width * 0.8,
                                      child: TextFormField(
                                        controller: firstNameController,
                                        onChanged: (value) {
                                          firstNameController.text = value;
                                          if (value.length == 0) {
                                            setState(() {
                                              _firstNameEdited = false;
                                              isFtEmpty = true;
                                            });
                                          } else {
                                            setState(() {
                                              _firstNameEdited = true;
                                              isFtEmpty = false;
                                            });
                                          }
                                          firstNameController.selection = TextSelection.fromPosition(TextPosition(offset: firstNameController.text.length));
                                        },
                                        validator: (value) {
                                          if (value!.isEmpty) {
                                            setState(() {
                                              _firstNameEdited = false;
                                            });
                                          }
                                        },
                                        decoration: InputDecoration(
                                            suffixIcon: _firstNameEdited
                                                ? Padding(
                                                    padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.06),
                                                    child: Icon(
                                                      Icons.check,
                                                      size: MediaQuery.of(context).size.width * 0.05,
                                                      color: Color(0xFF00CC9E),
                                                    ),
                                                  )
                                                : (isFtEmpty
                                                    ? Padding(
                                                        padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.01),
                                                        child: Icon(
                                                          Icons.close,
                                                          size: MediaQuery.of(context).size.width * 0.05,
                                                          color: Color(0xFFF52079),
                                                        ),
                                                      )
                                                    : null),
                                            fillColor: isFtEmpty ? (_firstNameEdited ? Colors.white : Colors.white.withGreen(990)) : (Colors.white),
                                            focusColor: Colors.white,
                                            hoverColor: _firstNameEdited ? Colors.green : (isFtEmpty ? Colors.red : (Colors.white)),
                                            filled: true,
                                            isDense: true,
                                            contentPadding: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width * 0.05, vertical: MediaQuery.of(context).size.width * 0.035),
                                            enabledBorder: OutlineInputBorder(
                                                borderRadius: BorderRadius.circular(60),
                                                borderSide: BorderSide(
                                                  color: _firstNameEdited ? Colors.green : (isFtEmpty ? Colors.red : Colors.white),
                                                )),
                                            focusedBorder: OutlineInputBorder(
                                                borderRadius: BorderRadius.circular(60),
                                                borderSide: BorderSide(
                                                  color: _firstNameEdited ? Colors.green : (isFtEmpty ? Colors.red : Colors.white),
                                                )),
                                            errorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red)),
                                            focusedErrorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red))),
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(top: MediaQuery.of(context).size.width * 0.01, left: MediaQuery.of(context).size.width * 0.16, bottom: MediaQuery.of(context).size.width * 0.001),
                                    child: Container(
                                      alignment: Alignment.centerLeft,
                                      child: isFtEmpty
                                          ? Text(
                                              "Filed required",
                                              style: GoogleFonts.poppins(
                                                color: Color(0xFFF52079),
                                                fontSize: MediaQuery.of(context).size.width * 0.025,
                                                fontWeight: FontWeight.w600,
                                                fontStyle: FontStyle.normal,
                                              ),
                                            )
                                          : null,
                                    ),
                                  ),
                                  Container(
                                    alignment: Alignment.centerLeft,
                                    padding: EdgeInsets.only(top: MediaQuery.of(context).size.width * 0.045, left: MediaQuery.of(context).size.width * 0.12, bottom: MediaQuery.of(context).size.width * 0.007),
                                    child: Text(
                                      "NAME",
                                      style: GoogleFonts.poppins(
                                        color: Colors.white,
                                        fontSize: MediaQuery.of(context).size.width * 0.033,
                                        fontWeight: FontWeight.w600,
                                        fontStyle: FontStyle.normal,
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(),
                                    child: Container(
                                      width: MediaQuery.of(context).size.width * 0.8,
                                      child: TextFormField(
                                        controller: lastNameController,
                                        onChanged: (value) {
                                          lastNameController.text = value;
                                          if (value.length == 0) {
                                            setState(() {
                                              _nameEdited = false;
                                              isLtEmpty = true;
                                            });
                                          } else {
                                            setState(() {
                                              _nameEdited = true;
                                              isLtEmpty = false;
                                            });
                                          }
                                          lastNameController.selection = TextSelection.fromPosition(TextPosition(offset: lastNameController.text.length));
                                        },
                                        validator: (value) {
                                          if (value!.isEmpty) {
                                            setState(() {
                                              _nameEdited = false;
                                            });
                                          }
                                        },
                                        decoration: InputDecoration(
                                            suffixIcon: _nameEdited
                                                ? Padding(
                                                    padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.06),
                                                    child: Icon(
                                                      Icons.check,
                                                      size: MediaQuery.of(context).size.width * 0.05,
                                                      color: Color(0xFF00CC9E),
                                                    ),
                                                  )
                                                : (isLtEmpty
                                                    ? Padding(
                                                        padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.01),
                                                        child: Icon(
                                                          Icons.close,
                                                          size: MediaQuery.of(context).size.width * 0.05,
                                                          color: Color(0xFFF52079),
                                                        ),
                                                      )
                                                    : null),
                                            fillColor: isLtEmpty ? (_nameEdited ? Colors.white : Colors.white.withGreen(990)) : (Colors.white),
                                            focusColor: Colors.white,
                                            hoverColor: _nameEdited ? Colors.green : (isLtEmpty ? Colors.red : (Colors.white)),
                                            filled: true,
                                            isDense: true,
                                            contentPadding: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width * 0.05, vertical: MediaQuery.of(context).size.width * 0.035),
                                            enabledBorder: OutlineInputBorder(
                                                borderRadius: BorderRadius.circular(60),
                                                borderSide: BorderSide(
                                                  color: _nameEdited ? Colors.green : (isLtEmpty ? Color(0xFFF52079) : Colors.white),
                                                )),
                                            focusedBorder: OutlineInputBorder(
                                                borderRadius: BorderRadius.circular(60),
                                                borderSide: BorderSide(
                                                  color: _nameEdited ? Colors.green : (isLtEmpty ? Color(0xFFF52079) : Colors.white),
                                                )),
                                            errorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Color(0xFFF52079))),
                                            focusedErrorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Color(0xFFF52079)))),
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(top: MediaQuery.of(context).size.width * 0.01, left: MediaQuery.of(context).size.width * 0.16, bottom: MediaQuery.of(context).size.width * 0.001),
                                    child: Container(
                                      alignment: Alignment.centerLeft,
                                      child: isLtEmpty
                                          ? Text(
                                              "Filed required",
                                              style: GoogleFonts.poppins(
                                                color: Color(0xFFF52079),
                                                fontSize: MediaQuery.of(context).size.width * 0.025,
                                                fontWeight: FontWeight.w600,
                                                fontStyle: FontStyle.normal,
                                              ),
                                            )
                                          : null,
                                    ),
                                  ),
                                  Container(
                                    alignment: Alignment.centerLeft,
                                    padding: EdgeInsets.only(
                                      left: MediaQuery.of(context).size.width * 0.12,
                                      bottom: MediaQuery.of(context).size.width * 0.007,
                                      top: MediaQuery.of(context).size.width * 0.045,
                                    ),
                                    child: Text(
                                      "MAIL",
                                      style: GoogleFonts.poppins(
                                        color: Colors.white,
                                        fontSize: MediaQuery.of(context).size.width * 0.033,
                                        fontWeight: FontWeight.w600,
                                        fontStyle: FontStyle.normal,
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.11, right: MediaQuery.of(context).size.width * 0.11),
                                    child: Container(
                                      width: MediaQuery.of(context).size.width * 0.8,
                                      child: TextFormField(
                                        controller: mailController,
                                        onChanged: (value) {
                                          setState(() {
                                            isExist = false;
                                          });
                                          mailController.text = value;
                                          if (mailController.text.length == 0) {
                                            setState(() {
                                              isMailEmpty = true;
                                              _mailEdited = false;
                                            });
                                          } else {
                                            setState(() {
                                              _mailEdited = true;
                                              isMailEmpty = false;
                                            });
                                            if (mailController.text.length > 0) {
                                              if (RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(mailController.text)) {
                                                setState(() {
                                                  _mailPattern = true;
                                                });
                                              } else {
                                                _mailPattern = false;
                                              }
                                            }
                                          }
                                          mailController.selection = TextSelection.fromPosition(TextPosition(offset: mailController.text.length));
                                        },
                                        validator: (value) {
                                          if (value!.isEmpty) {
                                            setState(() {
                                              _mailEdited = false;
                                            });
                                          }
                                        },
                                        decoration: InputDecoration(
                                            suffixIcon: _mailEdited
                                                ? Padding(
                                                    padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.06),
                                                    child: _mailPattern
                                                        ? Icon(
                                                            Icons.check,
                                                            size: MediaQuery.of(context).size.width * 0.05,
                                                            color: Color(0xFF00CC9E),
                                                          )
                                                        : Icon(
                                                            Icons.close,
                                                            size: MediaQuery.of(context).size.width * 0.05,
                                                            color: Color(0xFFF52079),
                                                          ),
                                                  )
                                                : null,
                                            fillColor: _mailPattern ? Colors.white : (_mailEdited ? Colors.white.withGreen(990) : Colors.white),
                                            focusColor: Colors.white,
                                            hoverColor: _mailPattern ? Colors.green : (_mailEdited ? Colors.red : Colors.white),
                                            filled: true,
                                            isDense: true,
                                            contentPadding: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width * 0.05, vertical: MediaQuery.of(context).size.width * 0.035),
                                            enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: _mailPattern ? Colors.green : (_mailEdited ? Color(0xFFF52079) : Colors.white))),
                                            focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: _mailPattern ? Colors.green : (_mailEdited ? Color(0xFFF52079) : Colors.white))),
                                            errorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Color(0xFFF52079))),
                                            focusedErrorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Color(0xFFF52079)))),
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(top: MediaQuery.of(context).size.width * 0.01, left: MediaQuery.of(context).size.width * 0.16, bottom: MediaQuery.of(context).size.width * 0.001),
                                    child: Container(
                                      alignment: Alignment.centerLeft,
                                      child: isMailEmpty
                                          ? Text(
                                              "Filed required",
                                              style: GoogleFonts.poppins(
                                                color: Color(0xFFF52079),
                                                fontSize: MediaQuery.of(context).size.width * 0.025,
                                                fontWeight: FontWeight.w600,
                                                fontStyle: FontStyle.normal,
                                              ),
                                            )
                                          : null,
                                    ),
                                  ),
                                  !_mailPattern
                                      ? _mailEdited
                                          ? Padding(
                                              padding: EdgeInsets.only(top: MediaQuery.of(context).size.width * 0.01, left: MediaQuery.of(context).size.width * 0.16, bottom: MediaQuery.of(context).size.width * 0.001),
                                              child: Container(
                                                alignment: Alignment.centerLeft,
                                                child: Text(
                                                  "Field Email Address must gave a valid form",
                                                  style: GoogleFonts.poppins(
                                                    color: Color(0xFFF52079),
                                                    fontSize: MediaQuery.of(context).size.width * 0.03,
                                                    fontWeight: FontWeight.w300,
                                                    fontStyle: FontStyle.normal,
                                                  ),
                                                ),
                                              ),
                                            )
                                          : Padding(padding: EdgeInsets.all(0.0))
                                      : Padding(padding: EdgeInsets.all(0.0)),
                                  isExist
                                      ? Padding(
                                          padding: const EdgeInsets.only(top: 9.0),
                                          child: Container(
                                              width: MediaQuery.of(context).size.width * 0.8,
                                              height: MediaQuery.of(context).size.height * 0.06,
                                              decoration: BoxDecoration(
                                                color: Colors.pink.withOpacity(0.2),
                                                borderRadius: BorderRadius.all(Radius.circular(30)),
                                              ),
                                              child: Padding(
                                                padding: const EdgeInsets.all(2.0),
                                                child: Row(
                                                  children: [
                                                    Padding(
                                                      padding: const EdgeInsets.all(5.0),
                                                      child: SvgPicture.asset(
                                                        "images/cross-pink.svg",
                                                        height: 24,
                                                        width: 24,
                                                      ),
                                                    ),
                                                    RichText(
                                                      text: TextSpan(children: [
                                                        new TextSpan(
                                                            text: "This email is already linked to an existing\naccount. ",
                                                            style: GoogleFonts.poppins(
                                                              fontWeight: FontWeight.w600,
                                                              color: Colors.pink,
                                                              fontSize: 11,
                                                            )),
                                                        new TextSpan(
                                                          text: "Login here",
                                                          style: GoogleFonts.poppins(
                                                            fontWeight: FontWeight.w600,
                                                            color: Colors.pink,
                                                            fontSize: 11,
                                                          ),
                                                          recognizer: new TapGestureRecognizer()..onTap = () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => Signin())),
                                                        ),
                                                      ]),
                                                    ),
                                                  ],
                                                ),
                                              )),
                                        )
                                      : Padding(
                                          padding: EdgeInsets.all(0.0),
                                        ),
                                  Padding(
                                    padding: EdgeInsets.only(top: MediaQuery.of(context).size.width * 0.1),
                                    child: (_mailPattern && _nameEdited && _firstNameEdited)
                                        ? Container(
                                            height: MediaQuery.of(context).size.height * 0.065,
                                            width: MediaQuery.of(context).size.width * 0.7,
                                            child: ElevatedButton(
                                              style: ButtonStyle(
                                                backgroundColor: MaterialStateProperty.all(
                                                  Color(0xFF00cc9e),
                                                ),
                                                shape: MaterialStateProperty.all(
                                                  RoundedRectangleBorder(
                                                    borderRadius: BorderRadius.circular(30),
                                                  ),
                                                ),
                                              ),
                                              child: Text(
                                                "Confirm",
                                                style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.035, color: Colors.white, fontWeight: FontWeight.w600, fontStyle: FontStyle.normal),
                                              ),
                                              onPressed: () async {
                                                if (_formKey.currentState!.validate()) {
                                                  var res = await submitForm();
                                                  if (res == "success") {
                                                    if (LoginController.apiService.isEnabled) {
                                                      if (LoginController.apiService.walletAddress != null && LoginController.apiService.walletAddress != "") {
                                                        gotoPageGlobal(context, WelcomeScreen());
                                                      } else {
                                                        gotoPageGlobal(context, CreateWallet());
                                                      }
                                                    } else {
                                                      gotoPageGlobal(context, ActivationMail());
                                                    }
                                                  } else {
                                                    setState(() {
                                                      isExist = true;
                                                    });
                                                  }
                                                }
                                              },
                                            ),
                                          )
                                        : Container(
                                            height: MediaQuery.of(context).size.height * 0.065,
                                            width: MediaQuery.of(context).size.width * 0.7,
                                            decoration: BoxDecoration(
                                              borderRadius: BorderRadius.circular(30),
                                              color: Color(0xFFF6F6FF),
                                            ),
                                            child: Center(
                                              child: Text(
                                                "Confirm",
                                                style: GoogleFonts.poppins(
                                                  color: Color(0xFFADADC8),
                                                  fontWeight: FontWeight.w600,
                                                  fontSize: MediaQuery.of(context).size.width * 0.037,
                                                ),
                                              ),
                                            ),
                                          ),
                                  ),
                                  SizedBox(
                                    height: isKeyboard ? MediaQuery.of(context).size.height * 0.4 : MediaQuery.of(context).size.height * 0.3,
                                    width: MediaQuery.of(context).size.height * 0.6,
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ))));
  }

  submitForm() async {
    Provider.of<LoginController>(context, listen: false).userDetails?.firstName = firstNameController.text;
    Provider.of<LoginController>(context, listen: false).userDetails?.lastName = lastNameController.text;
    Provider.of<LoginController>(context, listen: false).userDetails?.email = mailController.text;
    var response = await LoginController.apiService.updateLastStep(firstNameController.text, lastNameController.text, mailController.text, Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "");
    if (response != null) {
      if (response["message"] != "email already exists") {
        await LoginController.apiService.resendCode(mailController.text, Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "");
        return "success";
      }
    }
    return "";
  }
}
