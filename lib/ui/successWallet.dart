import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:ihave/controllers/LoginController.dart';
import 'package:ihave/ui/Profilepage.dart';
import 'package:ihave/ui/WelcomeScreen.dart';
import 'package:provider/provider.dart';

import '../controllers/walletController.dart';
import '../util/utils.dart';
import 'dashboard.dart';
import 'signin.dart';

class SuccessWallet extends StatefulWidget {
  const SuccessWallet({Key? key}) : super(key: key);

  @override
  _SuccessWalletState createState() => _SuccessWalletState();
}

class _SuccessWalletState extends State<SuccessWallet> {
  String userBalance = "";

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        child: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
                colors: [
                  const Color(0xFF03C7A3),
                  const Color(0xFF00147A),
                ],
                begin: const FractionalOffset(0.0, 0.0),
                end: const FractionalOffset(0.0, 0.4),
                stops: [0.0, 1.0],
                tileMode: TileMode.clamp),
          ),
          child: Scaffold(
            backgroundColor: Colors.transparent,
            resizeToAvoidBottomInset: false,
            appBar: null,
            bottomNavigationBar: null,
            body: SingleChildScrollView(
              child: SafeArea(
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: MediaQuery.of(context).size.height * 0.02,
                    ),
                    SvgPicture.asset(
                      'images/logo-auth.svg',
                      alignment: Alignment.center,
                      width: MediaQuery.of(context).size.height * 0.035,
                      height: MediaQuery.of(context).size.height * 0.035,
                    ),
                    SizedBox(
                      height: MediaQuery.of(context).size.height * 0.045,
                    ),
                    Text(
                      "Say ‘Hello’ to your\nnew wallet !",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(fontWeight: FontWeight.w700, fontSize: MediaQuery.of(context).size.height * 0.04, color: Colors.white, height: 1.1),
                    ),
                    SizedBox(
                      height: MediaQuery.of(context).size.height * 0.05,
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.05, right: MediaQuery.of(context).size.width * 0.05),
                      child: Text(
                        "You’re ready to use your wallet, make the most out of the SaTT Platform, and discover a new wave of social transactions. Just be sure to complete your profile to 100% before jumping into full swing.Of particular importance is your KYC an identity verification, that enables you to conduct transactions. ",
                        style: GoogleFonts.poppins(fontWeight: FontWeight.w400, fontSize: MediaQuery.of(context).size.width * 0.04, height: 1.2, fontStyle: FontStyle.normal, letterSpacing: 0.8, color: Colors.white),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    SizedBox(
                      height: MediaQuery.of(context).size.height * 0.06,
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.05, right: MediaQuery.of(context).size.width * 0.05),
                      child: RichText(
                        textAlign: TextAlign.center,
                        text: TextSpan(style: GoogleFonts.poppins(fontWeight: FontWeight.w400, fontSize: MediaQuery.of(context).size.width * 0.04, height: 1.2, fontStyle: FontStyle.normal, letterSpacing: 0.8, color: Colors.white), children: <TextSpan>[
                          new TextSpan(text: "Ready to "),
                          new TextSpan(
                            text: "streamline your crypto journey ? ",
                            style: GoogleFonts.poppins(fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.width * 0.04, height: 1.2, fontStyle: FontStyle.normal, letterSpacing: 0.8, color: Colors.white),
                          ),
                          new TextSpan(text: "Get started now ! By completing your profile, you're in a better position to optimize your security while maximizing your exposure.")
                        ]),
                      ),
                    ),
                    SizedBox(
                      height: MediaQuery.of(context).size.height * 0.1,
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width * 0.7,
                      height: MediaQuery.of(context).size.width * 0.12,
                      child: ElevatedButton(
                        onPressed: () async {
                          gotoPageGlobal(context, ProfilePage());
                        },
                        style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all(
                            Color(0xFF4048FF),
                          ),
                          shape: MaterialStateProperty.all(
                            RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.07),
                            ),
                          ),
                        ),
                        child: Text(
                          "Fill out my profile",
                          style: GoogleFonts.poppins(
                            fontSize: MediaQuery.of(context).size.width * 0.04,
                            fontWeight: FontWeight.w600,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                    Center(
                      child: Padding(
                        padding: EdgeInsets.fromLTRB(0, MediaQuery.of(context).size.width * 0.027, 0, MediaQuery.of(context).size.width * 0.06),
                        child: Container(
                          width: MediaQuery.of(context).size.width * 0.7,
                          height: MediaQuery.of(context).size.width * 0.12,
                          child: ElevatedButton(
                            onPressed: () async {
                              Provider.of<WalletController>(context, listen: false).selectedIndex = 2;
                              gotoPageGlobal(context, WelcomeScreen());
                            },
                            style: ButtonStyle(
                              backgroundColor: MaterialStateProperty.all(
                                Colors.white,
                              ),
                              shape: MaterialStateProperty.all(
                                RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.07),
                                ),
                              ),
                            ),
                            child: Text(
                              "Go to my wallet",
                              style: GoogleFonts.poppins(
                                fontSize: MediaQuery.of(context).size.width * 0.04,
                                fontWeight: FontWeight.w600,
                                color: Color(0xFF4048FF),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
        onWillPop: () async {
          return false;
        });
  }
}
