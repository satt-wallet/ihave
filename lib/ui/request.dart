import 'dart:convert';
import 'dart:math';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:ihave/controllers/LoginController.dart';
import 'package:ihave/controllers/walletController.dart';
import 'package:ihave/model/crypto.dart';
import 'package:ihave/service/apiService.dart';
import 'package:ihave/ui/WelcomeScreen.dart';
import 'package:ihave/ui/staticElements/headerNavigation.dart';
import 'package:ihave/util/utils.dart';
import 'package:pattern_formatter/pattern_formatter.dart';
import 'package:provider/provider.dart';
import 'package:share_plus/share_plus.dart';
import 'dart:io';
import 'dart:math' as math;

import 'package:qr_flutter/qr_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Request extends StatefulWidget {
  Request({Key? key}) : super(key: key);

  @override
  _RequestState createState() => _RequestState();
}

class _RequestState extends State<Request> {
  TextEditingController _cryptoBalance = TextEditingController();
  TextEditingController _dollarBalance = TextEditingController();
  TextEditingController _emailController = TextEditingController();
  TextEditingController _requestMessage = TextEditingController();
  FocusNode cryptoFocusNode = FocusNode();
  FocusNode dollarFocusNode = FocusNode();
  List<String> networkList = ["ERC20", "BEP20", "BTC"];
  bool _isVisible = false;
  bool _isProfileVisible = false;
  bool _isWalletVisible = false;
  bool _isNotificationVisible = false;
  String cryptoBalance = "";
  String dollarBalance = "";
  String walletAddress = "";
  String requestMessage = "";
  String _selectedCrypto = "";
  String senderAddress = "";
  String _smartContract = "";
  String _selectedNetwork = "ERC20";
  bool _textFieldClicked = false;
  bool _firstStepRequestFlow = true;
  bool _secondStepRequestFlow = false;
  bool _successRequest = false;
  bool _errorRequest = false;
  bool _isActivatedButtonRequest = true;
  bool _isLoadingRequest = false;
  var dropdownValue = 1;
  bool _isLoadingNetwork = false;
  bool _cryptoField = false;
  bool _dollarField = false;
  bool _emailField = false;
  bool _cryptoPattern = false;
  bool _dollarPattern = false;
  bool _emailValidForm = false;
  String _cryptoBalanceControl = "";
  List<Crypto> _cryptoList = <Crypto>[];
  APIService apiService = LoginController.apiService;
  int maxLength = 100;
  String valueselc = "";

  /**
   *  New variables for new design
   * */
  bool _clickedContainerPaymentMethod = false;
  String _selectedPaymentMethod = "erc20";
  bool _clickedCryptoContainer = false;
  bool _selectedCryptoAddedToken = false;
  String _selectedCryptoPicUrl = "";
  String _selectedCryptoName = "";
  String _selectedCryptoSymbol = "";

  final _formKeyCryptoDetails = GlobalKey<FormState>();
  final _formKeyMessage = GlobalKey<FormState>();
  String? firebaseToken = " ";
  bool _sameMail = false;
  bool _hasTronWallet = false;
  bool _obscureText = true;
  TextEditingController _WalletPassword = TextEditingController();

  bool _loading = false;

  checkTronWallet() async {
    var result = await LoginController.apiService.walletTronCheck(Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "");
    if (result != null || result != "error") {
      setState(() {
        _hasTronWallet = false;
      });
    } else {
      setState(() {
        _hasTronWallet = true;
      });
    }
  }

  createTronWalletUI(BuildContext context) {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return StatefulBuilder(builder: (context, setState) {
            return Scaffold(
              resizeToAvoidBottomInset: true,
              body: Dialog(
                backgroundColor: Colors.transparent,
                insetPadding: EdgeInsets.zero,
                child: SingleChildScrollView(
                  child: Container(
                    height: MediaQuery.of(context).size.height,
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      color: Colors.white.withOpacity(0.9),
                    ),
                    child: Column(
                      children: <Widget>[
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Spacer(),
                            Spacer(),
                            Padding(
                              padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.01, top: MediaQuery.of(context).size.width * 0.06),
                              child: IconButton(
                                alignment: Alignment.topLeft,
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                                splashColor: Colors.transparent,
                                highlightColor: Colors.transparent,
                                icon: Icon(
                                  Icons.close,
                                  size: MediaQuery.of(context).size.width * 0.05,
                                  color: Colors.black.withOpacity(0.5),
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: MediaQuery.of(context).size.height * 0.035,
                        ),
                        Image.asset(
                          "images/tron-wallet.png",
                        ),
                        Text(
                          "Create your Tron wallet",
                          style: GoogleFonts.poppins(fontWeight: FontWeight.w700, fontSize: MediaQuery.of(context).size.height * 0.025, color: Colors.black),
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.075, right: MediaQuery.of(context).size.width * 0.075, top: MediaQuery.of(context).size.height * 0.01),
                          child: Text(
                            "You are about to delete this token. You now have the possibility to create your Tron ​​wallet.You Need to just enter your transactional password to create it ",
                            textAlign: TextAlign.center,
                            style: GoogleFonts.poppins(fontWeight: FontWeight.w400, fontSize: MediaQuery.of(context).size.height * 0.0175, color: const Color(0XFF162746), letterSpacing: 1.1),
                          ),
                        ),
                        Align(
                          alignment: Alignment.topLeft,
                          child: Padding(
                            padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.1, top: MediaQuery.of(context).size.height * 0.035),
                            child: Text(
                              "TRANSACTION PASSWORD",
                              style: GoogleFonts.poppins(fontWeight: FontWeight.w600, color: const Color(0XFF75758F)),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: MediaQuery.of(context).size.height * 0.01,
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width * 0.8,
                          child: TextFormField(
                            style: TextStyle(fontSize: MediaQuery.of(context).size.width * 0.045),
                            textAlignVertical: TextAlignVertical.center,
                            textAlign: TextAlign.left,
                            controller: _WalletPassword,
                            keyboardType: TextInputType.visiblePassword,
                            obscureText: _obscureText,
                            onChanged: (value) {
                              TextSelection previousSelection = _WalletPassword.selection;
                              _WalletPassword.text = value;
                              _WalletPassword.selection = previousSelection;

                              TextSelection.fromPosition(TextPosition(offset: _WalletPassword.text.length));
                            },
                            validator: (value) {
                              if (value!.isEmpty) {
                                return 'Field required';
                              }
                              return null;
                            },
                            decoration: InputDecoration(
                                fillColor: Colors.white,
                                focusColor: Colors.white,
                                hoverColor: Colors.white,
                                filled: true,
                                isDense: true,
                                prefixIcon: Padding(
                                  padding: EdgeInsets.all(MediaQuery.of(context).size.width * 0.017),
                                  child: SvgPicture.asset(
                                    "images/keyIcon.svg",
                                    color: Colors.orange,
                                    height: MediaQuery.of(context).size.width * 0.06,
                                    width: MediaQuery.of(context).size.width * 0.06,
                                  ),
                                ),
                                suffixIcon: Padding(
                                  padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.025),
                                  child: IconButton(
                                      onPressed: () {
                                        setState(() {
                                          _obscureText = !_obscureText;
                                        });
                                      },
                                      icon: _obscureText
                                          ? SvgPicture.asset(
                                              "images/visibility-icon-off.svg",
                                              height: MediaQuery.of(context).size.height * 0.02,
                                            )
                                          : SvgPicture.asset(
                                              "images/visibility-icon-on.svg",
                                              height: MediaQuery.of(context).size.height * 0.02,
                                            )),
                                ),
                                contentPadding: EdgeInsets.symmetric(vertical: 5, horizontal: 5),
                                enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.white)),
                                focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.white)),
                                errorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red)),
                                focusedErrorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red))),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.02),
                          child: SizedBox(
                            width: MediaQuery.of(context).size.width * 0.6,
                            child: ElevatedButton(
                              child: Padding(
                                padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.015, bottom: MediaQuery.of(context).size.height * 0.015),
                                child: _loading
                                    ? CircularProgressIndicator(
                                        color: Colors.white,
                                      )
                                    : Text(
                                        "Create Wallet",
                                        style: GoogleFonts.poppins(fontWeight: FontWeight.w700, fontSize: MediaQuery.of(context).size.height * 0.017),
                                      ),
                              ),
                              onPressed: !_loading
                                  ? () async {
                                      setState(() {
                                        _loading = true;
                                      });
                                      var result = await LoginController.apiService.createTronWallet(Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "", _WalletPassword.text);
                                      switch (result) {
                                        case "success":
                                          Navigator.of(context).pop();
                                          Fluttertoast.showToast(msg: "Wallet tron created successfully", backgroundColor: Color.fromRGBO(0, 151, 117, 1), toastLength: Toast.LENGTH_LONG);
                                          break;
                                        case "exist":
                                          Navigator.of(context).pop();
                                          Fluttertoast.showToast(msg: "You have a tron wallet", backgroundColor: Color(0xFFF52079), toastLength: Toast.LENGTH_LONG);
                                          break;
                                        case "wrong password":
                                          setState(() {
                                            _WalletPassword.text = "";
                                            _loading = false;
                                          });
                                          Fluttertoast.showToast(msg: "Wrong password", backgroundColor: Color(0xFFF52079), toastLength: Toast.LENGTH_LONG);
                                          break;
                                        case "error":
                                          Navigator.of(context).pop();
                                          Fluttertoast.showToast(msg: "Something went wrong please try again", backgroundColor: Color(0xFFF52079), toastLength: Toast.LENGTH_LONG);
                                          break;
                                      }
                                    }
                                  : null,
                              style: ButtonStyle(
                                backgroundColor: MaterialStateProperty.all(
                                  Color(0xFF4048FF),
                                ),
                                shape: MaterialStateProperty.all(
                                  RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.08),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.012),
                          child: SizedBox(
                            width: MediaQuery.of(context).size.width * 0.6,
                            child: ElevatedButton(
                              child: Padding(
                                padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.015, bottom: MediaQuery.of(context).size.height * 0.015),
                                child: Text(
                                  "Skip for now",
                                  style: GoogleFonts.poppins(fontWeight: FontWeight.w700, fontSize: MediaQuery.of(context).size.height * 0.017, color: const Color(0xFF4048FF)),
                                ),
                              ),
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                              style: ButtonStyle(
                                backgroundColor: MaterialStateProperty.all(
                                  Colors.white,
                                ),
                                shape: MaterialStateProperty.all(
                                  RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.08),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            );
          });
        });
  }

  ThemeData scrolltheme = ThemeData(
      scrollbarTheme: ScrollbarThemeData(
    isAlwaysShown: true,
    thickness: MaterialStateProperty.all(3),
    thumbColor: MaterialStateProperty.all(Color(0xFF75758F)),
    radius: const Radius.circular(30),
  ));

  Future<List<Crypto>> fetchCrypto() async {
    if (Provider.of<WalletController>(context, listen: false).cryptos.length < 1) {
      var cryptoList = await apiService.cryptos1(Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "", version);
      Provider.of<WalletController>(context, listen: false).cryptos = cryptoList;
    }
    return Provider.of<WalletController>(context, listen: false).cryptos;
  }

  @override
  void initState() {
    super.initState();
    checkTronWallet();
    cryptoFocusNode.addListener(() {
      _cryptoBalance.text = formatCryptoBalance(double.parse(_cryptoBalance.text.replaceAll(new RegExp("[^\\d.]"), "")), "");
    });
    dollarFocusNode.addListener(() {
      _dollarBalance.text = formatDollarBalance(double.parse(_dollarBalance.text.replaceAll(new RegExp("[^\\d.]"), "")));
    });
    _firstStepRequestFlow = true;
    _secondStepRequestFlow = false;
    _successRequest = false;
    _errorRequest = false;

    if (Provider.of<LoginController>(context, listen: false).tokenNetwork == "") {
      fetchCrypto().then((value) {
        setState(() {
          _cryptoList.clear();
          _cryptoList.addAll(value.where((c) => c.network == _selectedNetwork).toList());
          _clickedCryptoContainer = false;
          setCryptoSelection(_cryptoList[0]);
          _isProfileVisible = !_isProfileVisible;
          _isWalletVisible = !_isWalletVisible;
          _isNotificationVisible = !_isNotificationVisible;
        });
      });
    } else {
      _selectedNetwork = Provider.of<LoginController>(context, listen: false).tokenNetwork;
      _selectedPaymentMethod = _selectedNetwork == "BTC" ? "fiat" : (_selectedNetwork == "BTTC" ? "btt" : _selectedNetwork.toLowerCase());
      fetchCrypto().then((value) {
        setState(() {
          _cryptoList.clear();
          _cryptoList.addAll(value.where((c) => c.network == _selectedNetwork).toList());
          Crypto crypto = _cryptoList.firstWhere((element) => element.symbol == Provider.of<LoginController>(context, listen: false).tokenSymbol);

          setCryptoSelection(crypto);
          _isProfileVisible = !_isProfileVisible;
          _isWalletVisible = !_isWalletVisible;
          _isNotificationVisible = !_isNotificationVisible;
        });
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    setState(() {
      _isVisible = Provider.of<WalletController>(context, listen: true).isSideMenuVisible;
      Provider.of<LoginController>(context, listen: false).errorRequestCrypto = false;
    });
    if (_cryptoList.isNotEmpty) {
      setState(() {
        _isVisible = Provider.of<WalletController>(context, listen: true).isSideMenuVisible;
        _isProfileVisible = Provider.of<WalletController>(context, listen: true).isProfileVisible;
        _isWalletVisible = Provider.of<WalletController>(context, listen: true).isWalletVisible;
        _isNotificationVisible = Provider.of<WalletController>(context, listen: true).isNotificationVisible;
      });
      return WillPopScope(
        onWillPop: () {
          gotoPageGlobal(context, WelcomeScreen());
          return Future.value(false);
        },
        child: Container(
          child: GestureDetector(
              onTap: () {
                FocusScope.of(context).requestFocus(new FocusNode());
              },
              child: _firstStepRequestFlow
                  ? SizedBox(
                      child: MediaQuery.of(context).viewInsets.bottom != 0
                          ? SingleChildScrollView(
                              child: Container(
                                height: MediaQuery.of(context).size.height * 1.1,
                                child: Stack(
                                  children: [
                                    firstStepWidget(),
                                  ],
                                ),
                              ),
                            )
                          : Stack(
                              children: [
                                firstStepWidget(),
                              ],
                            ),
                    )
                  : (_successRequest
                      ? (_requestMessage.text.toString().length > 0
                          ? SingleChildScrollView(
                              child: Container(
                                height: MediaQuery.of(context).size.height,
                                child: Stack(
                                  children: [secondStepWidget(), HeaderNavigation(_isVisible, _isWalletVisible, _isNotificationVisible)],
                                ),
                              ),
                            )
                          : Stack(
                              children: [secondStepWidget(), HeaderNavigation(_isVisible, _isWalletVisible, _isNotificationVisible)],
                            ))
                      : null)),
        ),
      );
    } else {
      return Center(
          child: CircularProgressIndicator(
        color: Colors.white,
      ));
    }
  }

  firstStepWidget() {
    return Column(
      children: <Widget>[
        Container(
          height: MediaQuery.of(context).size.height * 0.14,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.05, 0, 0, MediaQuery.of(context).size.height * 0.032),
                child: Text(
                  "Request",
                  style: GoogleFonts.poppins(
                    fontSize: MediaQuery.of(context).size.height * 0.035,
                    fontWeight: FontWeight.w700,
                    color: Colors.white,
                  ),
                ),
              ),
              Spacer(),
              Padding(
                padding: EdgeInsets.fromLTRB(0, 0, MediaQuery.of(context).size.width * 0.037, MediaQuery.of(context).size.height * 0.021),
                child: InkWell(
                  onTap: () {
                    Provider.of<WalletController>(context, listen: false).selectedIndex = 2;
                    gotoPageGlobal(context, WelcomeScreen());
                  },
                  child: Text(
                    "< Back",
                    style: GoogleFonts.poppins(color: Colors.white, letterSpacing: 1.2, fontWeight: FontWeight.w600, fontSize: MediaQuery.of(context).size.height * 0.022),
                  ),
                ),
              )
            ],
          ),
        ),
        Expanded(
          child: Container(
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(MediaQuery.of(context).size.width * 0.08),
                  topRight: Radius.circular(MediaQuery.of(context).size.width * 0.08),
                )),
            child: Form(
              key: _formKeyCryptoDetails,
              child: Column(
                children: <Widget>[
                  Stack(
                    children: [
                      /**
                       * USER input CRYPTO BALANCE
                       *    */
                      Padding(
                        padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.075),
                        child: Center(
                          child: Padding(
                            padding: EdgeInsets.fromLTRB(0, MediaQuery.of(context).size.height * 0.03, 0, 0),
                            child: Align(
                              alignment: Alignment.center,
                              child: TextFormField(
                                onTap: () {
                                  setState(() {
                                    _clickedContainerPaymentMethod = false;
                                    _clickedCryptoContainer = false;
                                  });
                                },
                                inputFormatters: <TextInputFormatter>[
                                  DecimalTextInputFormatter(decimalRange: 8),
                                  FilteringTextInputFormatter.allow(
                                    new RegExp(r'^[0-9]+\.?\d{0,8}'),
                                  ),
                                  CustomMaxValueInputFormatter(maxInputValue: 999999999.99999999),
                                ],
                                controller: _cryptoBalance,
                                focusNode: cryptoFocusNode,
                                decoration: InputDecoration(
                                  border: InputBorder.none,
                                  hintText: '0',
                                  counterText: '',
                                  hintStyle: GoogleFonts.poppins(color: Colors.black, fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.045),
                                  errorStyle: GoogleFonts.poppins(
                                    fontWeight: FontWeight.w700,
                                    fontSize: 16,
                                  ),
                                  alignLabelWithHint: true,
                                ),
                                style: GoogleFonts.poppins(fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.045),
                                textAlign: TextAlign.center,
                                keyboardType: Platform.isIOS ? TextInputType.datetime : TextInputType.number,
                                onChanged: (value) {
                                  value = value.replaceAll(new RegExp("[^\\d.]"), "");

                                  try {
                                    if (value.length == 0 || double.parse(value) <= 0) {
                                      _dollarBalance.text = "";
                                      setState(() {
                                        _cryptoField = false;
                                        _dollarField = false;
                                        _cryptoPattern = false;
                                        _dollarPattern = false;
                                      });
                                    } else {
                                      value = value.replaceAll(new RegExp("[^\\d.]"), "");
                                      _cryptoBalance.text = value;
                                      cryptoBalance = _cryptoBalance.text;
                                      setState(() {
                                        _cryptoField = true;
                                        _dollarField = true;
                                        _cryptoPattern = true;
                                        _dollarPattern = true;
                                      });
                                      _dollarBalance.text = setDollarBalance(_selectedCrypto, value);
                                      dollarBalance = _dollarBalance.text;
                                      _cryptoBalance.selection = TextSelection.fromPosition(TextPosition(offset: _cryptoBalance.text.length));
                                    }
                                  } catch (e) {
                                    setState(() {
                                      _cryptoPattern = false;
                                      _dollarPattern = false;
                                      _cryptoField = false;
                                      _dollarField = false;
                                    });
                                  }
                                  valueselc = value;
                                },
                              ),
                            ),
                          ),
                        ),
                      ),

                      /***
                       * User BALANCE
                       * */
                      Padding(
                        padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.19),
                        child: Center(
                          child: Text(
                            "1 ${_selectedCrypto.toLowerCase().contains("satt") ? "SATT" : _selectedCrypto} ≈ \$${formatDollarBalance(_cryptoList.firstWhere((e) => e.symbol == _selectedCrypto).price ?? 0).toString().replaceAll("\$", "")} USD",
                            style: GoogleFonts.poppins(color: Color(0XFFADADC8), fontSize: MediaQuery.of(context).size.height * 0.02, fontStyle: FontStyle.normal, fontWeight: FontWeight.w400, letterSpacing: 0.9),
                          ),
                        ),
                      ),

                      /**
                       * User input DOLLAR BALANCE
                       * */
                      Padding(
                        padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.19),
                        child: Center(
                          child: Padding(
                            padding: EdgeInsets.fromLTRB(0, MediaQuery.of(context).size.height * 0.06, 0, 0),
                            child: TextFormField(
                              onTap: () {
                                setState(() {
                                  _clickedContainerPaymentMethod = false;
                                  _clickedCryptoContainer = false;
                                });
                              },
                              inputFormatters: <TextInputFormatter>[
                                DecimalTextInputFormatter(decimalRange: 8),
                                FilteringTextInputFormatter.allow(
                                  new RegExp(r'^[0-9]+\.?\d{0,8}'),
                                ),
                                CustomMaxValueInputFormatter(maxInputValue: 999999999.99999999),
                              ],
                              controller: _dollarBalance,
                              focusNode: dollarFocusNode,
                              decoration: InputDecoration(
                                  border: InputBorder.none,
                                  hintText: '\$0',
                                  hintStyle: GoogleFonts.poppins(
                                    color: Color(0xFF75758F),
                                    fontSize: MediaQuery.of(context).size.height * 0.0375,
                                    fontWeight: FontWeight.bold,
                                  )),
                              style: GoogleFonts.poppins(
                                color: Color(0xFFADADC8),
                                fontSize: MediaQuery.of(context).size.height * 0.0375,
                                fontWeight: FontWeight.bold,
                              ),
                              keyboardType: Platform.isIOS ? TextInputType.datetime : TextInputType.number,
                              textAlign: TextAlign.center,
                              onChanged: (value) {
                                try {
                                  if (value.length == 0 || double.parse(value) <= 0) {
                                    _cryptoBalance.text = "";
                                    setState(() {
                                      _dollarField = false;
                                      _cryptoField = false;
                                      _dollarPattern = false;
                                      _cryptoPattern = false;
                                    });
                                  } else {
                                    value = value.replaceAll(new RegExp("[^\\d.]"), "");
                                    _dollarBalance.text = value;
                                    dollarBalance = _dollarBalance.text;
                                    _cryptoBalance.text = setCryptoBalance(_selectedCrypto, value);
                                    setState(() {
                                      _dollarField = true;
                                      _cryptoField = true;
                                      _dollarPattern = true;
                                      _cryptoPattern = true;
                                    });
                                    _cryptoBalance.text = setCryptoBalance(_selectedCrypto, value);
                                    cryptoBalance = _cryptoBalance.text;
                                    _dollarBalance.selection = TextSelection.fromPosition(TextPosition(offset: _dollarBalance.text.length));
                                  }
                                } catch (e) {
                                  setState(() {
                                    _dollarPattern = false;
                                    _cryptoPattern = false;
                                    _cryptoField = false;
                                    _dollarField = false;
                                  });
                                }
                              },
                            ),
                          ),
                        ),
                      ),

                      /**
                       *
                       * Facultative message
                       *
                       * */
                      Align(
                        alignment: Alignment.bottomLeft,
                        child: Padding(
                          padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.12, MediaQuery.of(context).size.height * 0.367, 0, 0),
                          child: Text(
                            "FACULTATIVE MESSAGE",
                            style: GoogleFonts.poppins(fontWeight: FontWeight.w600, fontSize: MediaQuery.of(context).size.height * 0.016, height: 1.1, letterSpacing: 0.95, color: Color(0xFF75758F)),
                            textAlign: TextAlign.left,
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.395, left: MediaQuery.of(context).size.width * 0.11),
                        child: Container(
                          width: MediaQuery.of(context).size.width * 0.8,
                          child: TextFormField(
                            maxLines: 3,
                            onTap: () {
                              setState(() {
                                _clickedContainerPaymentMethod = false;
                                _clickedCryptoContainer = false;
                              });
                            },
                            controller: _requestMessage,
                            obscureText: false,
                            style: GoogleFonts.poppins(
                              fontWeight: FontWeight.normal,
                              fontSize: MediaQuery.of(context).size.width * 0.035,
                            ),
                            decoration: InputDecoration(
                              contentPadding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height * 0.015, horizontal: MediaQuery.of(context).size.width * 0.04),
                              hintText: "Send a message to your contact",
                              hintStyle: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.height * 0.014, letterSpacing: 0.8, fontWeight: FontWeight.w400, color: const Color(0XFFADADC8)),
                              enabledBorder: OutlineInputBorder(borderRadius: new BorderRadius.circular(15.0), borderSide: BorderSide(color: const Color(0XFFD6D6E8))),
                              focusedBorder: new OutlineInputBorder(
                                borderRadius: new BorderRadius.circular(15.0),
                                borderSide: BorderSide(color: const Color(0XFFD6D6E8)),
                              ),
                            ),
                          ),
                        ),
                      ),

                      /**
                       *  Button CONFIRM
                       * */
                      Center(
                        child: Padding(
                          padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.52),
                          child: (_cryptoField && _dollarField && _dollarPattern && _cryptoPattern && _dollarBalance.text.length > 0 && _cryptoBalance.text.length > 0)
                              ? SizedBox(
                                  height: MediaQuery.of(context).size.height * 0.058,
                                  width: MediaQuery.of(context).size.width * 0.8,
                                  child: ElevatedButton(
                                    style: ButtonStyle(
                                      backgroundColor: MaterialStateProperty.all(
                                        Color(0xFF4048FF),
                                      ),
                                      shape: MaterialStateProperty.all(
                                        RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.08),
                                        ),
                                      ),
                                    ),
                                    onPressed: () {
                                      if (_requestMessage.text.length > 0) {
                                        Share.share(
                                            'Request amount :\n${cryptoBalance} ${_selectedCrypto == "SATTBEP20" ? "SATT" : _selectedCrypto}\n\nNetwork : ${_selectedPaymentMethod == "fiat" ? "BTC" : _selectedPaymentMethod.toUpperCase()}\nWallet Address :\n${_selectedPaymentMethod == "fiat" ? LoginController.apiService.btcWalletAddress : (_selectedPaymentMethod == "tron" ? LoginController.apiService.tronAddress : LoginController.apiService.walletAddress)}\n\nMessage: ${_requestMessage.text}\n\nhttps://iHave.io');
                                      } else {
                                        Share.share(
                                            'Request amount :\n${cryptoBalance} ${_selectedCrypto == "SATTBEP20" ? "SATT" : _selectedCrypto}\n\nNetwork : ${_selectedPaymentMethod == "fiat" ? "BTC" : _selectedPaymentMethod.toUpperCase()}\nWallet Address :\n${_selectedPaymentMethod == "fiat" ? LoginController.apiService.btcWalletAddress : (_selectedPaymentMethod == "tron" ? LoginController.apiService.tronAddress : LoginController.apiService.walletAddress)}\n\nhttps://iHave.io');
                                      }
                                      setState(() {
                                        _firstStepRequestFlow = false;
                                        _secondStepRequestFlow = false;
                                        _successRequest = true;
                                      });
                                    },
                                    child: Text(
                                      "Request",
                                      style: GoogleFonts.poppins(
                                        fontSize: MediaQuery.of(context).size.height * 0.02,
                                        fontWeight: FontWeight.w600,
                                      ),
                                    ),
                                  ),
                                )
                              : Container(
                                  height: MediaQuery.of(context).size.height * 0.058,
                                  width: MediaQuery.of(context).size.width * 0.8,
                                  decoration: BoxDecoration(borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.08), color: Color(0xFFF6F6FF), border: Border.all(color: Color(0xFFD6D6E8), width: 1)),
                                  child: Center(
                                    child: Text(
                                      "Request",
                                      style: GoogleFonts.poppins(
                                        fontSize: MediaQuery.of(context).size.height * 0.02,
                                        fontWeight: FontWeight.w600,
                                        color: Color(0xFFADADC8),
                                      ),
                                    ),
                                  ),
                                ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.025, left: MediaQuery.of(context).size.width * 0.0375),
                        child: Stack(
                          children: <Widget>[
                            /**
                             *
                             * NETWORK SELECT SECTION      */

                            Padding(
                              padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.475),
                              child: Align(
                                alignment: Alignment.bottomLeft,
                                child: InkWell(
                                  onTap: () {
                                    if (_selectedNetwork != "BTC" || _selectedPaymentMethod != "polygon") {
                                      setState(() {
                                        _clickedCryptoContainer = !_clickedCryptoContainer;
                                      });
                                    }
                                  },
                                  child: Container(
                                    height: _clickedCryptoContainer ? MediaQuery.of(context).size.height * 0.15 : MediaQuery.of(context).size.height * 0.06,
                                    width: MediaQuery.of(context).size.width * 0.45,
                                    decoration: BoxDecoration(
                                      border: Border.all(color: Color(0xFFD6D6E8)),
                                      borderRadius: BorderRadius.circular(30),
                                      color: Colors.white,
                                    ),
                                    child: _clickedCryptoContainer
                                        ? Padding(
                                            padding: EdgeInsets.only(
                                              bottom: MediaQuery.of(context).size.height * 0.015,
                                              right: MediaQuery.of(context).size.height * 0.015,
                                              top: MediaQuery.of(context).size.height * 0.01,
                                            ),
                                            child: Theme(
                                                data: scrolltheme,
                                                child: Scrollbar(
                                                  child: ListView.builder(
                                                    scrollDirection: Axis.vertical,
                                                    shrinkWrap: true,
                                                    itemCount: _cryptoList.length,
                                                    itemBuilder: (context, index) {
                                                      String cryptoName = "";
                                                      if (_cryptoList[index].name.toString().length > 5) {
                                                        cryptoName = _cryptoList[index].name.toString().substring(0, 5).toUpperCase() + "...";
                                                      } else {
                                                        cryptoName = _cryptoList[index].name.toString().toUpperCase();
                                                      }
                                                      return Padding(
                                                        padding: EdgeInsets.only(
                                                          top: MediaQuery.of(context).size.height * 0.015,
                                                          left: MediaQuery.of(context).size.width * 0.015,
                                                        ),
                                                        child: InkWell(
                                                          onTap: () {
                                                            setState(() {
                                                              setFieldSelection(_cryptoList[index], _selectedPaymentMethod, _selectedNetwork, false);
                                                            });
                                                          },
                                                          child: Row(
                                                            children: [
                                                              _cryptoList[index].addedToken == true
                                                                  ? (_cryptoList[index].picUrl != "false" && _cryptoList[index].picUrl != "null"
                                                                      ? Padding(
                                                                          padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.018),
                                                                          child: Image.network(
                                                                            _cryptoList[index].picUrl ?? "",
                                                                            width: MediaQuery.of(context).size.width * 0.055,
                                                                            height: MediaQuery.of(context).size.width * 0.055,
                                                                          ),
                                                                        )
                                                                      : Padding(
                                                                          padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.018),
                                                                          child: SvgPicture.asset(
                                                                            'images/indispo.svg',
                                                                            width: MediaQuery.of(context).size.width * 0.055,
                                                                            height: MediaQuery.of(context).size.width * 0.055,
                                                                          ),
                                                                        ))
                                                                  : Padding(
                                                                      padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.018),
                                                                      child: SvgPicture.asset(
                                                                        'images/' + _cryptoList[index].name.toString().toLowerCase().replaceAll(' ', '') + '.svg',
                                                                        alignment: Alignment.center,
                                                                        width: MediaQuery.of(context).size.width * 0.055,
                                                                        height: MediaQuery.of(context).size.width * 0.055,
                                                                      ),
                                                                    ),
                                                              SizedBox(
                                                                width: MediaQuery.of(context).size.width * 0.02,
                                                              ),
                                                              new Text(
                                                                _cryptoList[index].symbol.toString().toUpperCase() == "SATTBEP20" ? "SATT" : _cryptoList[index].symbol.toString().toUpperCase(),
                                                                style: GoogleFonts.poppins(color: Colors.black, fontWeight: FontWeight.w600, fontSize: MediaQuery.of(context).size.height * 0.0165),
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                      );
                                                    },
                                                  ),
                                                )))
                                        : Padding(
                                            padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.015),
                                            child: Row(
                                              children: [
                                                _selectedCryptoAddedToken == true
                                                    ? (_selectedCryptoPicUrl != "false" && _selectedCryptoPicUrl != "null"
                                                        ? Padding(
                                                            padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.018),
                                                            child: Image.network(
                                                              _selectedCryptoPicUrl,
                                                              width: MediaQuery.of(context).size.width * 0.055,
                                                              height: MediaQuery.of(context).size.width * 0.055,
                                                            ),
                                                          )
                                                        : Padding(
                                                            padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.018),
                                                            child: SvgPicture.asset(
                                                              'images/indispo.svg',
                                                              width: MediaQuery.of(context).size.width * 0.055,
                                                              height: MediaQuery.of(context).size.width * 0.055,
                                                            ),
                                                          ))
                                                    : Padding(
                                                        padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.018),
                                                        child: SvgPicture.asset(
                                                          'images/' + _selectedCryptoName.toLowerCase().replaceAll(' ', '') + '.svg',
                                                          alignment: Alignment.center,
                                                          width: MediaQuery.of(context).size.width * 0.055,
                                                          height: MediaQuery.of(context).size.width * 0.055,
                                                        ),
                                                      ),
                                                SizedBox(
                                                  width: MediaQuery.of(context).size.width * 0.02,
                                                ),
                                                new Text(
                                                  _selectedCryptoSymbol == "SATTBEP20" ? "SATT" : _selectedCryptoSymbol,
                                                  style: GoogleFonts.poppins(color: Colors.black, fontWeight: FontWeight.w600, fontSize: MediaQuery.of(context).size.height * 0.0165),
                                                ),
                                                const Spacer(),
                                                Padding(
                                                  padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.05),
                                                  child: SizedBox(
                                                    width: 10,
                                                    child: Transform.rotate(
                                                        angle: -pi / 2,
                                                        child: Icon(
                                                          Icons.arrow_back_ios,
                                                          color: Colors.grey,
                                                          size: MediaQuery.of(context).size.width * 0.06,
                                                        )),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                  ),
                                ),
                              ),
                            ),
                            /**
                             *  Crypto List SECTION
                             * */
                            InkWell(
                              onTap: () {
                                setState(() {
                                  _clickedContainerPaymentMethod = !_clickedContainerPaymentMethod;
                                });
                              },
                              child: Container(
                                width: MediaQuery.of(context).size.width * 0.45,
                                height: _clickedContainerPaymentMethod ? MediaQuery.of(context).size.height * 0.3 : MediaQuery.of(context).size.height * 0.06,
                                decoration: BoxDecoration(
                                  border: Border.all(color: Color(0xFFD6D6E8)),
                                  borderRadius: BorderRadius.circular(30),
                                  color: Colors.white,
                                ),
                                child: Column(
                                  children: <Widget>[
                                    Padding(
                                      padding: EdgeInsets.only(top: MediaQuery.of(context).size.height < 1000 ? MediaQuery.of(context).size.height * 0.01 : MediaQuery.of(context).size.height * 0.005),
                                      child: _selectedPaymentMethod == "fiat"
                                          ? Row(
                                              children: <Widget>[
                                                Padding(
                                                  padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025),
                                                  child: SvgPicture.asset(
                                                    "images/btc-icon.svg",
                                                    height: MediaQuery.of(context).size.height * 0.025,
                                                    width: MediaQuery.of(context).size.width * 0.025,
                                                  ),
                                                ),
                                                Padding(
                                                  padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.035, left: MediaQuery.of(context).size.width * 0.012),
                                                  child: Text(
                                                    "BTC",
                                                    style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.018),
                                                  ),
                                                ),
                                                Spacer(),
                                                Padding(
                                                  padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.055, top: MediaQuery.of(context).size.height * 0.0035),
                                                  child: SizedBox(
                                                    width: 10,
                                                    child: Transform.rotate(
                                                        angle: -pi / 2,
                                                        child: Icon(
                                                          Icons.arrow_back_ios,
                                                          color: Colors.grey,
                                                          size: MediaQuery.of(context).size.width * 0.06,
                                                        )),
                                                  ),
                                                ),
                                              ],
                                            )
                                          : (_selectedPaymentMethod == "erc20"
                                              ? Row(
                                                  children: <Widget>[
                                                    Padding(
                                                      padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025),
                                                      child: SvgPicture.asset(
                                                        'images/testtest.svg',
                                                        height: MediaQuery.of(context).size.width * 0.08,
                                                        width: MediaQuery.of(context).size.width * 0.08,
                                                        color: Colors.black,
                                                      ),
                                                    ),
                                                    Padding(
                                                      padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.035, left: MediaQuery.of(context).size.width * 0.0075),
                                                      child: Text(
                                                        "ERC20",
                                                        style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.018),
                                                      ),
                                                    ),
                                                    Spacer(),
                                                    Padding(
                                                      padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.05, top: MediaQuery.of(context).size.height * 0.0035),
                                                      child: SizedBox(
                                                        width: 10,
                                                        child: Transform.rotate(
                                                            angle: -pi / 2,
                                                            child: Icon(
                                                              Icons.arrow_back_ios,
                                                              color: Colors.grey,
                                                              size: MediaQuery.of(context).size.width * 0.06,
                                                            )),
                                                      ),
                                                    ),
                                                  ],
                                                )
                                              : (_selectedPaymentMethod == "polygon"
                                                  ? Row(
                                                      children: <Widget>[
                                                        Padding(
                                                          padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025),
                                                          child: SvgPicture.asset(
                                                            'images/polygon-network.svg',
                                                            height: MediaQuery.of(context).size.width * 0.05,
                                                            width: MediaQuery.of(context).size.width * 0.05,
                                                            color: Colors.black,
                                                          ),
                                                        ),
                                                        Padding(
                                                          padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.035),
                                                          child: Text(
                                                            "POLYGON",
                                                            style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.015),
                                                          ),
                                                        ),
                                                        Spacer(),
                                                        Padding(
                                                          padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.05, top: MediaQuery.of(context).size.height * 0.0035),
                                                          child: SizedBox(
                                                            width: 10,
                                                            child: Transform.rotate(
                                                                angle: -pi / 2,
                                                                child: Icon(
                                                                  Icons.arrow_back_ios,
                                                                  color: Colors.grey,
                                                                  size: MediaQuery.of(context).size.width * 0.06,
                                                                )),
                                                          ),
                                                        ),
                                                      ],
                                                    )
                                                  : (_selectedPaymentMethod == "btt"
                                                      ? Row(
                                                          children: <Widget>[
                                                            Padding(
                                                              padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025),
                                                              child: SvgPicture.asset(
                                                                "images/btt-network.svg",
                                                                height: MediaQuery.of(context).size.height * 0.025,
                                                                width: MediaQuery.of(context).size.width * 0.025,
                                                              ),
                                                            ),
                                                            Padding(
                                                              padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.035, left: MediaQuery.of(context).size.width * 0.012),
                                                              child: Text(
                                                                "BTTC",
                                                                style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.018),
                                                              ),
                                                            ),
                                                            Spacer(),
                                                            Padding(
                                                              padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.055, top: MediaQuery.of(context).size.height * 0.0035),
                                                              child: SizedBox(
                                                                width: 10,
                                                                child: Transform.rotate(
                                                                    angle: -pi / 2,
                                                                    child: Icon(
                                                                      Icons.arrow_back_ios,
                                                                      color: Colors.grey,
                                                                      size: MediaQuery.of(context).size.width * 0.06,
                                                                    )),
                                                              ),
                                                            ),
                                                          ],
                                                        )
                                                      : (_selectedPaymentMethod == "tron"
                                                          ? Row(
                                                              children: <Widget>[
                                                                Padding(
                                                                  padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025),
                                                                  child: SvgPicture.asset(
                                                                    'images/wallet-tron.svg',
                                                                    height: MediaQuery.of(context).size.width * 0.05,
                                                                    width: MediaQuery.of(context).size.width * 0.05,
                                                                    color: Colors.black,
                                                                  ),
                                                                ),
                                                                Padding(
                                                                  padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.035, left: MediaQuery.of(context).size.width * 0.005),
                                                                  child: Text(
                                                                    "TRON",
                                                                    style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.018),
                                                                  ),
                                                                ),
                                                                Spacer(),
                                                                Padding(
                                                                  padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.05, top: MediaQuery.of(context).size.height * 0.0035),
                                                                  child: SizedBox(
                                                                    width: 10,
                                                                    child: Transform.rotate(
                                                                        angle: -pi / 2,
                                                                        child: Icon(
                                                                          Icons.arrow_back_ios,
                                                                          color: Colors.grey,
                                                                          size: MediaQuery.of(context).size.width * 0.06,
                                                                        )),
                                                                  ),
                                                                ),
                                                              ],
                                                            )
                                                          : Row(
                                                              children: <Widget>[
                                                                Padding(
                                                                  padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025),
                                                                  child: SvgPicture.asset(
                                                                    'images/bep.svg',
                                                                    height: MediaQuery.of(context).size.width * 0.05,
                                                                    width: MediaQuery.of(context).size.width * 0.05,
                                                                    color: Colors.black,
                                                                  ),
                                                                ),
                                                                Padding(
                                                                  padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.035, left: MediaQuery.of(context).size.width * 0.005),
                                                                  child: Text(
                                                                    "BEP20",
                                                                    style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.018),
                                                                  ),
                                                                ),
                                                                Spacer(),
                                                                Padding(
                                                                  padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.05, top: MediaQuery.of(context).size.height * 0.0035),
                                                                  child: SizedBox(
                                                                    width: 10,
                                                                    child: Transform.rotate(
                                                                        angle: -pi / 2,
                                                                        child: Icon(
                                                                          Icons.arrow_back_ios,
                                                                          color: Colors.grey,
                                                                          size: MediaQuery.of(context).size.width * 0.06,
                                                                        )),
                                                                  ),
                                                                ),
                                                              ],
                                                            ))))),
                                    ),
                                    Visibility(
                                        visible: _clickedContainerPaymentMethod,
                                        child: _selectedPaymentMethod == "fiat"
                                            ? Padding(
                                                padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.012),
                                                child: Column(
                                                  children: <Widget>[
                                                    Padding(
                                                      padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.0085),
                                                      child: InkWell(
                                                        onTap: () {
                                                          fetchCrypto().then((value) {
                                                            setState(() {
                                                              _selectedNetwork = "BEP20";
                                                              _cryptoList.clear();
                                                              _cryptoList.addAll(value.where((c) => c.network == _selectedNetwork).toList());
                                                              setFieldSelection(_cryptoList[0], "bep20", "BEP20", false);
                                                            });
                                                          });
                                                        },
                                                        child: Row(
                                                          children: <Widget>[
                                                            Padding(
                                                              padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025),
                                                              child: SvgPicture.asset(
                                                                'images/bep.svg',
                                                                height: MediaQuery.of(context).size.width * 0.05,
                                                                width: MediaQuery.of(context).size.width * 0.05,
                                                                color: Colors.black,
                                                              ),
                                                            ),
                                                            SizedBox(width: MediaQuery.of(context).size.width * 0.005),
                                                            Text(
                                                              "BEP20",
                                                              style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.017),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                    ),
                                                    InkWell(
                                                      onTap: () {
                                                        fetchCrypto().then((value) {
                                                          setState(() {
                                                            _selectedNetwork = "ERC20";
                                                            _cryptoList.clear();
                                                            _cryptoList.addAll(value.where((c) => c.network == _selectedNetwork).toList());
                                                            setFieldSelection(_cryptoList[0], "erc20", "ERC20", false);
                                                          });
                                                        });
                                                      },
                                                      child: Row(
                                                        children: <Widget>[
                                                          Padding(
                                                            padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.037, right: MediaQuery.of(context).size.width * 0.025),
                                                            child: SvgPicture.asset(
                                                              'images/testtest.svg',
                                                              height: MediaQuery.of(context).size.width * 0.08,
                                                              width: MediaQuery.of(context).size.width * 0.08,
                                                              color: Colors.black,
                                                            ),
                                                          ),
                                                          Padding(
                                                            padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.0075),
                                                            child: Text(
                                                              "ERC20",
                                                              style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.017),
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                    Padding(
                                                      padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.0085, top: MediaQuery.of(context).size.height * 0.005),
                                                      child: InkWell(
                                                        onTap: () {
                                                          fetchCrypto().then((value) {
                                                            setState(() {
                                                              _selectedNetwork = "POLYGON";
                                                              _cryptoList.clear();
                                                              _cryptoList.addAll(value.where((c) => c.network == _selectedNetwork).toList());
                                                              setFieldSelection(_cryptoList[0], "polygon", "POLYGON", false);
                                                            });
                                                          });
                                                        },
                                                        child: Row(
                                                          children: <Widget>[
                                                            Padding(
                                                              padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025),
                                                              child: SvgPicture.asset(
                                                                'images/polygon-network.svg',
                                                                height: MediaQuery.of(context).size.width * 0.05,
                                                                width: MediaQuery.of(context).size.width * 0.05,
                                                                color: Colors.black,
                                                              ),
                                                            ),
                                                            Text(
                                                              "POLYGON",
                                                              style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.017),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                    ),
                                                    Padding(
                                                      padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.0085, top: MediaQuery.of(context).size.height * 0.005),
                                                      child: InkWell(
                                                        onTap: () {
                                                          fetchCrypto().then((value) {
                                                            setState(() {
                                                              _selectedNetwork = "BTTC";
                                                              _cryptoList.clear();
                                                              _cryptoList.addAll(value.where((c) => c.network == _selectedNetwork).toList());
                                                              setFieldSelection(_cryptoList[0], "btt", "BTTC", false);
                                                            });
                                                          });
                                                        },
                                                        child: Row(
                                                          children: <Widget>[
                                                            Padding(
                                                              padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025),
                                                              child: SvgPicture.asset(
                                                                'images/btt-network.svg',
                                                                height: MediaQuery.of(context).size.width * 0.05,
                                                                width: MediaQuery.of(context).size.width * 0.05,
                                                              ),
                                                            ),
                                                            Text(
                                                              "BTTC",
                                                              style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.017),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                    ),
                                                    Padding(
                                                      padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.0085, top: MediaQuery.of(context).size.height * 0.005),
                                                      child: InkWell(
                                                        onTap: () {
                                                          if (LoginController.apiService.tronAddress != null && LoginController.apiService.tronAddress != "") {
                                                            fetchCrypto().then((value) {
                                                              setState(() {
                                                                _selectedNetwork = "TRON";
                                                                _cryptoList.clear();
                                                                _cryptoList.addAll(value.where((c) => c.network == _selectedNetwork).toList());
                                                                setFieldSelection(_cryptoList[0], "tron", "TRON", false);
                                                              });
                                                            });
                                                          } else {
                                                            createTronWalletUI(context);
                                                          }
                                                        },
                                                        child: Row(
                                                          children: <Widget>[
                                                            Padding(
                                                              padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025),
                                                              child: SvgPicture.asset(
                                                                'images/wallet-tron.svg',
                                                                height: MediaQuery.of(context).size.width * 0.05,
                                                                width: MediaQuery.of(context).size.width * 0.05,
                                                                color: Colors.black,
                                                              ),
                                                            ),
                                                            Text(
                                                              "TRON",
                                                              style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.017),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              )
                                            : (_selectedPaymentMethod == "bep20"
                                                ? Padding(
                                                    padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.012),
                                                    child: Column(
                                                      children: <Widget>[
                                                        Padding(
                                                          padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.0085),
                                                          child: InkWell(
                                                            onTap: () async {
                                                              fetchCrypto().then((value) {
                                                                setState(() {
                                                                  _selectedNetwork = "BTC";
                                                                  _cryptoList.clear();
                                                                  _cryptoList.addAll(value.where((c) => c.network == _selectedNetwork).toList());
                                                                  setFieldSelection(_cryptoList[0], "fiat", "BTC", false);
                                                                });
                                                              });
                                                            },
                                                            child: Row(
                                                              children: <Widget>[
                                                                Padding(
                                                                  padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025, bottom: MediaQuery.of(context).size.height * 0.002),
                                                                  child: SvgPicture.asset(
                                                                    "images/btc-icon.svg",
                                                                    height: MediaQuery.of(context).size.height * 0.025,
                                                                    width: MediaQuery.of(context).size.width * 0.025,
                                                                  ),
                                                                ),
                                                                Padding(
                                                                  padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.035, left: MediaQuery.of(context).size.width * 0.012),
                                                                  child: Text(
                                                                    "BTC",
                                                                    style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.018),
                                                                  ),
                                                                ),
                                                              ],
                                                            ),
                                                          ),
                                                        ),
                                                        InkWell(
                                                          onTap: () {
                                                            fetchCrypto().then((value) {
                                                              setState(() {
                                                                _selectedNetwork = "ERC20";
                                                                _cryptoList.clear();
                                                                _cryptoList.addAll(value.where((c) => c.network == _selectedNetwork).toList());
                                                                setFieldSelection(_cryptoList[0], "erc20", "ERC20", false);
                                                              });
                                                            });
                                                          },
                                                          child: Row(
                                                            children: <Widget>[
                                                              Padding(
                                                                padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.037, right: MediaQuery.of(context).size.width * 0.025),
                                                                child: SvgPicture.asset(
                                                                  'images/testtest.svg',
                                                                  height: MediaQuery.of(context).size.width * 0.08,
                                                                  width: MediaQuery.of(context).size.width * 0.08,
                                                                  color: Colors.black,
                                                                ),
                                                              ),
                                                              Padding(
                                                                padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.0075),
                                                                child: Text(
                                                                  "ERC20",
                                                                  style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.017),
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                        Padding(
                                                          padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.0085, top: MediaQuery.of(context).size.height * 0.0085),
                                                          child: InkWell(
                                                            onTap: () {
                                                              fetchCrypto().then((value) {
                                                                setState(() {
                                                                  _selectedNetwork = "POLYGON";
                                                                  _cryptoList.clear();
                                                                  _cryptoList.addAll(value.where((c) => c.network == _selectedNetwork).toList());
                                                                  setFieldSelection(_cryptoList[0], "polygon", "POLYGON", false);
                                                                });
                                                              });
                                                            },
                                                            child: Row(
                                                              children: <Widget>[
                                                                Padding(
                                                                  padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025),
                                                                  child: SvgPicture.asset(
                                                                    'images/polygon-network.svg',
                                                                    height: MediaQuery.of(context).size.width * 0.05,
                                                                    width: MediaQuery.of(context).size.width * 0.05,
                                                                    color: Colors.black,
                                                                  ),
                                                                ),
                                                                Text(
                                                                  "POLYGON",
                                                                  style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.017),
                                                                ),
                                                              ],
                                                            ),
                                                          ),
                                                        ),
                                                        Padding(
                                                          padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.0085, top: MediaQuery.of(context).size.height * 0.005),
                                                          child: InkWell(
                                                            onTap: () {
                                                              fetchCrypto().then((value) {
                                                                setState(() {
                                                                  _clickedCryptoContainer = false;
                                                                  _selectedPaymentMethod = "btt";
                                                                  _selectedNetwork = "BTTC";
                                                                  _cryptoList.clear();
                                                                  _cryptoList.addAll(value.where((c) => c.network == _selectedNetwork).toList());
                                                                  setFieldSelection(_cryptoList[0], "btt", "BTTC", false);
                                                                });
                                                              });
                                                            },
                                                            child: Row(
                                                              children: <Widget>[
                                                                Padding(
                                                                  padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025),
                                                                  child: SvgPicture.asset(
                                                                    'images/btt-network.svg',
                                                                    height: MediaQuery.of(context).size.width * 0.05,
                                                                    width: MediaQuery.of(context).size.width * 0.05,
                                                                  ),
                                                                ),
                                                                Text(
                                                                  "BTTC",
                                                                  style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.017),
                                                                ),
                                                              ],
                                                            ),
                                                          ),
                                                        ),
                                                        Padding(
                                                          padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.0085, top: MediaQuery.of(context).size.height * 0.005),
                                                          child: InkWell(
                                                            onTap: () {
                                                              if (LoginController.apiService.tronAddress != null && LoginController.apiService.tronAddress != "") {
                                                                fetchCrypto().then((value) {
                                                                  setState(() {
                                                                    _selectedNetwork = "TRON";
                                                                    _cryptoList.clear();
                                                                    _cryptoList.addAll(value.where((c) => c.network == _selectedNetwork).toList());
                                                                    setFieldSelection(_cryptoList[0], "tron", "TRON", false);
                                                                  });
                                                                });
                                                              } else {
                                                                createTronWalletUI(context);
                                                              }
                                                            },
                                                            child: Row(
                                                              children: <Widget>[
                                                                Padding(
                                                                  padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025),
                                                                  child: SvgPicture.asset(
                                                                    'images/wallet-tron.svg',
                                                                    height: MediaQuery.of(context).size.width * 0.05,
                                                                    width: MediaQuery.of(context).size.width * 0.05,
                                                                    color: Colors.black,
                                                                  ),
                                                                ),
                                                                Text(
                                                                  "TRON",
                                                                  style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.017),
                                                                ),
                                                              ],
                                                            ),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  )
                                                : (_selectedPaymentMethod == "polygon"
                                                    ? Padding(
                                                        padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.012, left: MediaQuery.of(context).size.width * 0.0085),
                                                        child: Column(
                                                          children: <Widget>[
                                                            Padding(
                                                              padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.0085),
                                                              child: InkWell(
                                                                onTap: () async {
                                                                  fetchCrypto().then((value) {
                                                                    setState(() {
                                                                      _selectedNetwork = "BTC";
                                                                      _cryptoList.clear();
                                                                      _cryptoList.addAll(value.where((c) => c.network == _selectedNetwork).toList());
                                                                      setFieldSelection(_cryptoList[0], "fiat", "BTC", false);
                                                                    });
                                                                  });
                                                                },
                                                                child: Row(
                                                                  children: <Widget>[
                                                                    Padding(
                                                                      padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025, bottom: MediaQuery.of(context).size.height * 0.002),
                                                                      child: SvgPicture.asset(
                                                                        "images/btc-icon.svg",
                                                                        height: MediaQuery.of(context).size.height * 0.025,
                                                                        width: MediaQuery.of(context).size.width * 0.025,
                                                                      ),
                                                                    ),
                                                                    Padding(
                                                                      padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.035, left: MediaQuery.of(context).size.width * 0.0075),
                                                                      child: Text(
                                                                        "BTC",
                                                                        style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.018),
                                                                      ),
                                                                    ),
                                                                  ],
                                                                ),
                                                              ),
                                                            ),
                                                            InkWell(
                                                              onTap: () {
                                                                fetchCrypto().then((value) {
                                                                  setState(() {
                                                                    _selectedNetwork = "BEP20";
                                                                    _cryptoList.clear();
                                                                    _cryptoList.addAll(value.where((c) => c.network == _selectedNetwork).toList());
                                                                    setFieldSelection(_cryptoList[0], "bep20", "BEP20", false);
                                                                  });
                                                                });
                                                              },
                                                              child: Row(
                                                                children: <Widget>[
                                                                  Padding(
                                                                    padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025),
                                                                    child: SvgPicture.asset(
                                                                      'images/bep.svg',
                                                                      height: MediaQuery.of(context).size.width * 0.05,
                                                                      width: MediaQuery.of(context).size.width * 0.05,
                                                                      color: Colors.black,
                                                                    ),
                                                                  ),
                                                                  Text(
                                                                    "BEP20",
                                                                    style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.017),
                                                                  ),
                                                                ],
                                                              ),
                                                            ),
                                                            InkWell(
                                                              onTap: () {
                                                                fetchCrypto().then((value) {
                                                                  setState(() {
                                                                    _selectedNetwork = "ERC20";
                                                                    _cryptoList.clear();
                                                                    _cryptoList.addAll(value.where((c) => c.network == _selectedNetwork).toList());
                                                                    setFieldSelection(_cryptoList[0], "erc20", "ERC20", false);
                                                                  });
                                                                });
                                                              },
                                                              child: Row(
                                                                children: <Widget>[
                                                                  Padding(
                                                                    padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.037, right: MediaQuery.of(context).size.width * 0.025),
                                                                    child: SvgPicture.asset(
                                                                      'images/testtest.svg',
                                                                      height: MediaQuery.of(context).size.width * 0.08,
                                                                      width: MediaQuery.of(context).size.width * 0.08,
                                                                      color: Colors.black,
                                                                    ),
                                                                  ),
                                                                  Padding(
                                                                    padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.0035),
                                                                    child: Text(
                                                                      "ERC20",
                                                                      style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.017),
                                                                    ),
                                                                  ),
                                                                ],
                                                              ),
                                                            ),
                                                            Padding(
                                                              padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.0085, top: MediaQuery.of(context).size.height * 0.005),
                                                              child: InkWell(
                                                                onTap: () {
                                                                  fetchCrypto().then((value) {
                                                                    setState(() {
                                                                      _selectedNetwork = "BTTC";
                                                                      _cryptoList.clear();
                                                                      _cryptoList.addAll(value.where((c) => c.network == _selectedNetwork).toList());
                                                                      setFieldSelection(_cryptoList[0], "btt", "BTTC", false);
                                                                    });
                                                                  });
                                                                },
                                                                child: Row(
                                                                  children: <Widget>[
                                                                    Padding(
                                                                      padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025),
                                                                      child: SvgPicture.asset(
                                                                        'images/btt-network.svg',
                                                                        height: MediaQuery.of(context).size.width * 0.05,
                                                                        width: MediaQuery.of(context).size.width * 0.05,
                                                                      ),
                                                                    ),
                                                                    Text(
                                                                      "BTTC",
                                                                      style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.017),
                                                                    ),
                                                                  ],
                                                                ),
                                                              ),
                                                            ),
                                                            Padding(
                                                              padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.0085, top: MediaQuery.of(context).size.height * 0.005),
                                                              child: InkWell(
                                                                onTap: () {
                                                                  if (LoginController.apiService.tronAddress != null && LoginController.apiService.tronAddress != "") {
                                                                    fetchCrypto().then((value) {
                                                                      setState(() {
                                                                        _selectedNetwork = "TRON";
                                                                        _cryptoList.clear();
                                                                        _cryptoList.addAll(value.where((c) => c.network == _selectedNetwork).toList());
                                                                        setFieldSelection(_cryptoList[0], "tron", "TRON", false);
                                                                      });
                                                                    });
                                                                  } else {
                                                                    createTronWalletUI(context);
                                                                  }
                                                                },
                                                                child: Row(
                                                                  children: <Widget>[
                                                                    Padding(
                                                                      padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025),
                                                                      child: SvgPicture.asset(
                                                                        'images/wallet-tron.svg',
                                                                        height: MediaQuery.of(context).size.width * 0.05,
                                                                        width: MediaQuery.of(context).size.width * 0.05,
                                                                        color: Colors.black,
                                                                      ),
                                                                    ),
                                                                    Text(
                                                                      "TRON",
                                                                      style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.017),
                                                                    ),
                                                                  ],
                                                                ),
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      )
                                                    : (_selectedPaymentMethod == "btt"
                                                        ? Padding(
                                                            padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.012),
                                                            child: Column(
                                                              children: <Widget>[
                                                                Padding(
                                                                  padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.0085),
                                                                  child: InkWell(
                                                                    onTap: () async {
                                                                      fetchCrypto().then((value) {
                                                                        setState(() {
                                                                          _selectedNetwork = "BTC";
                                                                          _cryptoList.clear();
                                                                          _cryptoList.addAll(value.where((c) => c.network == _selectedNetwork).toList());
                                                                          setFieldSelection(_cryptoList[0], "fiat", "BTC", false);
                                                                        });
                                                                      });
                                                                    },
                                                                    child: Row(
                                                                      children: <Widget>[
                                                                        Padding(
                                                                          padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025, bottom: MediaQuery.of(context).size.height * 0.002),
                                                                          child: SvgPicture.asset(
                                                                            "images/btc-icon.svg",
                                                                            height: MediaQuery.of(context).size.height * 0.025,
                                                                            width: MediaQuery.of(context).size.width * 0.025,
                                                                          ),
                                                                        ),
                                                                        Padding(
                                                                          padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.035, left: MediaQuery.of(context).size.width * 0.01),
                                                                          child: Text(
                                                                            "BTC",
                                                                            style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.018),
                                                                          ),
                                                                        ),
                                                                      ],
                                                                    ),
                                                                  ),
                                                                ),
                                                                InkWell(
                                                                  onTap: () {
                                                                    fetchCrypto().then((value) {
                                                                      setState(() {
                                                                        _selectedNetwork = "BEP20";
                                                                        _cryptoList.clear();
                                                                        _cryptoList.addAll(value.where((c) => c.network == _selectedNetwork).toList());
                                                                        setFieldSelection(_cryptoList[0], "bep20", "BEP20", false);
                                                                      });
                                                                    });
                                                                  },
                                                                  child: Row(
                                                                    children: <Widget>[
                                                                      Padding(
                                                                        padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025),
                                                                        child: SvgPicture.asset(
                                                                          'images/bep.svg',
                                                                          height: MediaQuery.of(context).size.width * 0.05,
                                                                          width: MediaQuery.of(context).size.width * 0.05,
                                                                          color: Colors.black,
                                                                        ),
                                                                      ),
                                                                      SizedBox(width: MediaQuery.of(context).size.width * 0.0035),
                                                                      Text(
                                                                        "BEP20",
                                                                        style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.017),
                                                                      ),
                                                                    ],
                                                                  ),
                                                                ),
                                                                InkWell(
                                                                  onTap: () {
                                                                    fetchCrypto().then((value) {
                                                                      setState(() {
                                                                        _selectedNetwork = "ERC20";
                                                                        _cryptoList.clear();
                                                                        _cryptoList.addAll(value.where((c) => c.network == _selectedNetwork).toList());
                                                                        setFieldSelection(_cryptoList[0], "erc20", "ERC20", false);
                                                                      });
                                                                    });
                                                                  },
                                                                  child: Row(
                                                                    children: <Widget>[
                                                                      Padding(
                                                                        padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.037, right: MediaQuery.of(context).size.width * 0.025),
                                                                        child: SvgPicture.asset(
                                                                          'images/testtest.svg',
                                                                          height: MediaQuery.of(context).size.width * 0.08,
                                                                          width: MediaQuery.of(context).size.width * 0.08,
                                                                          color: Colors.black,
                                                                        ),
                                                                      ),
                                                                      Padding(
                                                                        padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.0035),
                                                                        child: Text(
                                                                          "ERC20",
                                                                          style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.017),
                                                                        ),
                                                                      ),
                                                                    ],
                                                                  ),
                                                                ),
                                                                Padding(
                                                                  padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.0085, top: MediaQuery.of(context).size.height * 0.0075),
                                                                  child: InkWell(
                                                                    onTap: () {
                                                                      fetchCrypto().then((value) {
                                                                        setState(() {
                                                                          _selectedNetwork = "POLYGON";
                                                                          _cryptoList.clear();
                                                                          _cryptoList.addAll(value.where((c) => c.network == _selectedNetwork).toList());
                                                                          setFieldSelection(_cryptoList[0], "polygon", "POLYGON", false);
                                                                        });
                                                                      });
                                                                    },
                                                                    child: Row(
                                                                      children: <Widget>[
                                                                        Padding(
                                                                          padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025),
                                                                          child: SvgPicture.asset(
                                                                            'images/polygon-network.svg',
                                                                            height: MediaQuery.of(context).size.width * 0.05,
                                                                            width: MediaQuery.of(context).size.width * 0.05,
                                                                            color: Colors.black,
                                                                          ),
                                                                        ),
                                                                        Text(
                                                                          "POLYGON",
                                                                          style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.017),
                                                                        ),
                                                                      ],
                                                                    ),
                                                                  ),
                                                                ),
                                                                Padding(
                                                                  padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.0085, top: MediaQuery.of(context).size.height * 0.005),
                                                                  child: InkWell(
                                                                    onTap: () {
                                                                      if (LoginController.apiService.tronAddress != null && LoginController.apiService.tronAddress != "") {
                                                                        fetchCrypto().then((value) {
                                                                          setState(() {
                                                                            _selectedNetwork = "TRON";
                                                                            _cryptoList.clear();
                                                                            _cryptoList.addAll(value.where((c) => c.network == _selectedNetwork).toList());
                                                                            setFieldSelection(_cryptoList[0], "tron", "TRON", false);
                                                                          });
                                                                        });
                                                                      } else {
                                                                        createTronWalletUI(context);
                                                                      }
                                                                    },
                                                                    child: Row(
                                                                      children: <Widget>[
                                                                        Padding(
                                                                          padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025),
                                                                          child: SvgPicture.asset(
                                                                            'images/wallet-tron.svg',
                                                                            height: MediaQuery.of(context).size.width * 0.05,
                                                                            width: MediaQuery.of(context).size.width * 0.05,
                                                                            color: Colors.black,
                                                                          ),
                                                                        ),
                                                                        Text(
                                                                          "TRON",
                                                                          style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.017),
                                                                        ),
                                                                      ],
                                                                    ),
                                                                  ),
                                                                ),
                                                              ],
                                                            ),
                                                          )
                                                        : (_selectedPaymentMethod == "tron"
                                                            ? Padding(
                                                                padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.012),
                                                                child: Column(
                                                                  children: <Widget>[
                                                                    Padding(
                                                                      padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.0085),
                                                                      child: InkWell(
                                                                        onTap: () async {
                                                                          setState(() {
                                                                            fetchCrypto().then((value) {
                                                                              setState(() {
                                                                                _selectedNetwork = "BTC";
                                                                                _cryptoList.clear();
                                                                                _cryptoList.addAll(value.where((c) => c.network == _selectedNetwork).toList());
                                                                                setFieldSelection(_cryptoList[0], "fiat", "BTC", false);
                                                                              });
                                                                            });
                                                                          });
                                                                        },
                                                                        child: Row(
                                                                          children: <Widget>[
                                                                            Padding(
                                                                              padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025, bottom: MediaQuery.of(context).size.height * 0.002),
                                                                              child: SvgPicture.asset(
                                                                                "images/btc-icon.svg",
                                                                                height: MediaQuery.of(context).size.height * 0.025,
                                                                                width: MediaQuery.of(context).size.width * 0.025,
                                                                              ),
                                                                            ),
                                                                            Padding(
                                                                              padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.035, left: MediaQuery.of(context).size.width * 0.01),
                                                                              child: Text(
                                                                                "BTC",
                                                                                style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.018),
                                                                              ),
                                                                            ),
                                                                          ],
                                                                        ),
                                                                      ),
                                                                    ),
                                                                    InkWell(
                                                                      onTap: () {
                                                                        setState(() {
                                                                          setState(() {
                                                                            fetchCrypto().then((value) {
                                                                              setState(() {
                                                                                _selectedNetwork = "BEP20";
                                                                                _cryptoList.clear();
                                                                                _cryptoList.addAll(value.where((c) => c.network == _selectedNetwork).toList());
                                                                                setFieldSelection(_cryptoList[0], "bep20", "BEP20", false);
                                                                              });
                                                                            });
                                                                          });
                                                                        });
                                                                      },
                                                                      child: Row(
                                                                        children: <Widget>[
                                                                          Padding(
                                                                            padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025),
                                                                            child: SvgPicture.asset(
                                                                              'images/bep.svg',
                                                                              height: MediaQuery.of(context).size.width * 0.05,
                                                                              width: MediaQuery.of(context).size.width * 0.05,
                                                                              color: Colors.black,
                                                                            ),
                                                                          ),
                                                                          SizedBox(width: MediaQuery.of(context).size.width * 0.0035),
                                                                          Text(
                                                                            "BEP20",
                                                                            style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.017),
                                                                          ),
                                                                        ],
                                                                      ),
                                                                    ),
                                                                    Padding(
                                                                      padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.0085, top: MediaQuery.of(context).size.height * 0.0075),
                                                                      child: InkWell(
                                                                        onTap: () {
                                                                          fetchCrypto().then((value) {
                                                                            setState(() {
                                                                              _selectedNetwork = "POLYGON";
                                                                              _cryptoList.clear();
                                                                              _cryptoList.addAll(value.where((c) => c.network == _selectedNetwork).toList());
                                                                              setFieldSelection(_cryptoList[0], "polygon", "POLYGON", false);
                                                                            });
                                                                          });
                                                                        },
                                                                        child: Row(
                                                                          children: <Widget>[
                                                                            Padding(
                                                                              padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025),
                                                                              child: SvgPicture.asset(
                                                                                'images/polygon-network.svg',
                                                                                height: MediaQuery.of(context).size.width * 0.05,
                                                                                width: MediaQuery.of(context).size.width * 0.05,
                                                                                color: Colors.black,
                                                                              ),
                                                                            ),
                                                                            Text(
                                                                              "POLYGON",
                                                                              style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.017),
                                                                            ),
                                                                          ],
                                                                        ),
                                                                      ),
                                                                    ),
                                                                    Padding(
                                                                      padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.0085, top: MediaQuery.of(context).size.height * 0.005),
                                                                      child: InkWell(
                                                                        onTap: () {
                                                                          fetchCrypto().then((value) {
                                                                            setState(() {
                                                                              _selectedNetwork = "BTTC";
                                                                              _cryptoList.clear();
                                                                              _cryptoList.addAll(value.where((c) => c.network == _selectedNetwork).toList());
                                                                              setFieldSelection(_cryptoList[0], "btt", "BTTC", false);
                                                                            });
                                                                          });
                                                                        },
                                                                        child: Row(
                                                                          children: <Widget>[
                                                                            Padding(
                                                                              padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025),
                                                                              child: SvgPicture.asset(
                                                                                'images/btt-network.svg',
                                                                                height: MediaQuery.of(context).size.width * 0.05,
                                                                                width: MediaQuery.of(context).size.width * 0.05,
                                                                              ),
                                                                            ),
                                                                            Text(
                                                                              "BTTC",
                                                                              style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.017),
                                                                            ),
                                                                          ],
                                                                        ),
                                                                      ),
                                                                    ),
                                                                    InkWell(
                                                                      onTap: () {
                                                                        fetchCrypto().then((value) {
                                                                          setState(() {
                                                                            _cryptoList.clear();
                                                                            _cryptoList.addAll(value.where((c) => c.network == _selectedNetwork).toList());
                                                                            setFieldSelection(_cryptoList[0], "erc20", "ERC20", false);
                                                                          });
                                                                        });
                                                                      },
                                                                      child: Row(
                                                                        children: <Widget>[
                                                                          Padding(
                                                                            padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.037, right: MediaQuery.of(context).size.width * 0.025),
                                                                            child: SvgPicture.asset(
                                                                              'images/testtest.svg',
                                                                              height: MediaQuery.of(context).size.width * 0.08,
                                                                              width: MediaQuery.of(context).size.width * 0.08,
                                                                              color: Colors.black,
                                                                            ),
                                                                          ),
                                                                          Padding(
                                                                            padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.0035),
                                                                            child: Text(
                                                                              "ERC20",
                                                                              style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.017),
                                                                            ),
                                                                          ),
                                                                        ],
                                                                      ),
                                                                    ),
                                                                  ],
                                                                ),
                                                              )
                                                            : Padding(
                                                                padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.012),
                                                                child: Column(
                                                                  children: <Widget>[
                                                                    Padding(
                                                                      padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.0085),
                                                                      child: InkWell(
                                                                        onTap: () async {
                                                                          fetchCrypto().then((value) {
                                                                            setState(() {
                                                                              _cryptoList.clear();
                                                                              _cryptoList.addAll(value.where((c) => c.network == _selectedNetwork).toList());
                                                                              setFieldSelection(_cryptoList[0], "fiat", "BTC", false);
                                                                            });
                                                                          });
                                                                        },
                                                                        child: Row(
                                                                          children: <Widget>[
                                                                            Padding(
                                                                              padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025, bottom: MediaQuery.of(context).size.height * 0.002),
                                                                              child: SvgPicture.asset(
                                                                                "images/btc-icon.svg",
                                                                                height: MediaQuery.of(context).size.height * 0.025,
                                                                                width: MediaQuery.of(context).size.width * 0.025,
                                                                              ),
                                                                            ),
                                                                            Padding(
                                                                              padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.035, left: MediaQuery.of(context).size.width * 0.01),
                                                                              child: Text(
                                                                                "BTC",
                                                                                style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.018),
                                                                              ),
                                                                            ),
                                                                          ],
                                                                        ),
                                                                      ),
                                                                    ),
                                                                    InkWell(
                                                                      onTap: () {
                                                                        fetchCrypto().then((value) {
                                                                          setState(() {
                                                                            _cryptoList.clear();
                                                                            _cryptoList.addAll(value.where((c) => c.network == _selectedNetwork).toList());
                                                                            setFieldSelection(_cryptoList[0], "bep20", "BEP20", false);
                                                                          });
                                                                        });
                                                                      },
                                                                      child: Row(
                                                                        children: <Widget>[
                                                                          Padding(
                                                                            padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025),
                                                                            child: SvgPicture.asset(
                                                                              'images/bep.svg',
                                                                              height: MediaQuery.of(context).size.width * 0.05,
                                                                              width: MediaQuery.of(context).size.width * 0.05,
                                                                              color: Colors.black,
                                                                            ),
                                                                          ),
                                                                          SizedBox(width: MediaQuery.of(context).size.width * 0.0035),
                                                                          Text(
                                                                            "BEP20",
                                                                            style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.017),
                                                                          ),
                                                                        ],
                                                                      ),
                                                                    ),
                                                                    Padding(
                                                                      padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.0085, top: MediaQuery.of(context).size.height * 0.0075),
                                                                      child: InkWell(
                                                                        onTap: () {
                                                                          fetchCrypto().then((value) {
                                                                            setState(() {
                                                                              _cryptoList.clear();
                                                                              _cryptoList.addAll(value.where((c) => c.network == _selectedNetwork).toList());
                                                                              setFieldSelection(_cryptoList[0], "polygon", "POLYGON", false);
                                                                            });
                                                                          });
                                                                        },
                                                                        child: Row(
                                                                          children: <Widget>[
                                                                            Padding(
                                                                              padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025),
                                                                              child: SvgPicture.asset(
                                                                                'images/polygon-network.svg',
                                                                                height: MediaQuery.of(context).size.width * 0.05,
                                                                                width: MediaQuery.of(context).size.width * 0.05,
                                                                                color: Colors.black,
                                                                              ),
                                                                            ),
                                                                            Text(
                                                                              "POLYGON",
                                                                              style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.017),
                                                                            ),
                                                                          ],
                                                                        ),
                                                                      ),
                                                                    ),
                                                                    Padding(
                                                                      padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.0085, top: MediaQuery.of(context).size.height * 0.005),
                                                                      child: InkWell(
                                                                        onTap: () {
                                                                          fetchCrypto().then((value) {
                                                                            setState(() {
                                                                              _cryptoList.clear();
                                                                              _cryptoList.addAll(value.where((c) => c.network == _selectedNetwork).toList());
                                                                              setFieldSelection(_cryptoList[0], "btt", "BTTC", false);
                                                                            });
                                                                          });
                                                                        },
                                                                        child: Row(
                                                                          children: <Widget>[
                                                                            Padding(
                                                                              padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025),
                                                                              child: SvgPicture.asset(
                                                                                'images/btt-network.svg',
                                                                                height: MediaQuery.of(context).size.width * 0.05,
                                                                                width: MediaQuery.of(context).size.width * 0.05,
                                                                              ),
                                                                            ),
                                                                            Text(
                                                                              "BTTC",
                                                                              style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.017),
                                                                            ),
                                                                          ],
                                                                        ),
                                                                      ),
                                                                    ),
                                                                    Padding(
                                                                      padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.0085, top: MediaQuery.of(context).size.height * 0.005),
                                                                      child: InkWell(
                                                                        onTap: () {
                                                                          if (LoginController.apiService.tronAddress != null && LoginController.apiService.tronAddress != "") {
                                                                            fetchCrypto().then((value) {
                                                                              setState(() {
                                                                                _cryptoList.clear();
                                                                                _cryptoList.addAll(value.where((c) => c.network == _selectedNetwork).toList());
                                                                                setFieldSelection(_cryptoList[0], "tron", "TRON", false);
                                                                              });
                                                                            });
                                                                          } else {
                                                                            createTronWalletUI(context);
                                                                          }
                                                                        },
                                                                        child: Row(
                                                                          children: <Widget>[
                                                                            Padding(
                                                                              padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033, right: MediaQuery.of(context).size.width * 0.025),
                                                                              child: SvgPicture.asset(
                                                                                'images/wallet-tron.svg',
                                                                                height: MediaQuery.of(context).size.width * 0.05,
                                                                                width: MediaQuery.of(context).size.width * 0.05,
                                                                                color: Colors.black,
                                                                              ),
                                                                            ),
                                                                            Text(
                                                                              "TRON",
                                                                              style: GoogleFonts.poppins(color: Color(0XFF00041b), fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.017),
                                                                            ),
                                                                          ],
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ],
                                                                ),
                                                              )))))),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }

  secondStepWidget() {
    Column(
      children: <Widget>[
        Container(
          height: MediaQuery.of(context).size.height * 0.14,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.05, 0, 0, MediaQuery.of(context).size.height * 0.032),
                child: Text(
                  "Request",
                  style: GoogleFonts.poppins(
                    fontSize: MediaQuery.of(context).size.height * 0.035,
                    fontWeight: FontWeight.w700,
                    color: Colors.white,
                  ),
                ),
              ),
              Spacer(),
              Padding(
                padding: EdgeInsets.fromLTRB(0, 0, MediaQuery.of(context).size.width * 0.037, MediaQuery.of(context).size.height * 0.021),
                child: InkWell(
                  onTap: () {
                    Provider.of<WalletController>(context, listen: false).selectedIndex = 2;
                    Navigator.pushReplacementNamed(context, '/welcomescreen');
                  },
                  child: Text(
                    "< Back",
                    style: GoogleFonts.poppins(color: Colors.white, letterSpacing: 1.2, fontWeight: FontWeight.w600, fontSize: MediaQuery.of(context).size.height * 0.022),
                  ),
                ),
              )
            ],
          ),
        ),
        Expanded(
          child: Container(
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(MediaQuery.of(context).size.width * 0.08),
                topRight: Radius.circular(MediaQuery.of(context).size.width * 0.08),
              ),
            ),
            child: Column(
              children: <Widget>[
                SizedBox(height: MediaQuery.of(context).size.height * 0.04),
                QrImage(
                    data: _selectedNetwork == "BTC"
                        ? (LoginController.apiService.btcWalletAddress.toString() == "" ? LoginController.apiService.walletAddress.toString() : LoginController.apiService.btcWalletAddress.toString())
                        : (_selectedNetwork == "TRON" ? LoginController.apiService.tronAddress.toString() : LoginController.apiService.walletAddress.toString()),
                    size: MediaQuery.of(context).size.height * 0.15),
                SizedBox(height: MediaQuery.of(context).size.height * 0.02),
                Text(
                  "Request For:\n${_cryptoBalance.text} ${_selectedCrypto.toLowerCase().contains("satt") ? "SATT" : _selectedCrypto}",
                  textAlign: TextAlign.center,
                  style: GoogleFonts.poppins(color: Color(0xFF1F2337), letterSpacing: 0.9, fontWeight: FontWeight.w700, height: 1.3, fontSize: MediaQuery.of(context).size.height * 0.022),
                ),
                SizedBox(height: MediaQuery.of(context).size.height * 0.03),
                if (_requestMessage.text.toString().length > 0)
                  Container(
                      width: MediaQuery.of(context).size.width * 0.8,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(30),
                        color: Color(0xFFF6F6FF),
                      ),
                      child: Center(
                          child: Column(
                        children: [
                          SizedBox(height: MediaQuery.of(context).size.height * 0.02),
                          Text("MESSAGE:", style: GoogleFonts.poppins(fontWeight: FontWeight.w600, color: const Color(0XFF323754), fontSize: MediaQuery.of(context).size.height * 0.016)),
                          SizedBox(height: MediaQuery.of(context).size.height * 0.01),
                          Padding(
                            padding: EdgeInsets.only(
                              left: MediaQuery.of(context).size.width * 0.03,
                              right: MediaQuery.of(context).size.width * 0.03,
                            ),
                            child: Text(_requestMessage.text.toString(),
                                style: GoogleFonts.poppins(
                                  fontWeight: FontWeight.w400,
                                  fontStyle: FontStyle.normal,
                                  color: const Color(0XFF323754),
                                  fontSize: MediaQuery.of(context).size.height * 0.017,
                                )),
                          ),
                          SizedBox(height: MediaQuery.of(context).size.height * 0.01),
                        ],
                      ))),
                SizedBox(height: MediaQuery.of(context).size.height * 0.06),
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.06,
                  width: MediaQuery.of(context).size.width * 0.8,
                  child: ElevatedButton(
                    style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all(
                        Color(0xFF00CC9E),
                      ),
                      shape: MaterialStateProperty.all(
                        RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.07),
                        ),
                      ),
                    ),
                    onPressed: () {
                      setState(() {
                        _emailField = false;
                        _cryptoField = false;
                        _dollarField = false;
                        _cryptoPattern = false;
                        _dollarPattern = false;
                        _isLoadingRequest = false;
                        _isActivatedButtonRequest = true;
                        _secondStepRequestFlow = false;
                        _firstStepRequestFlow = true;
                        _successRequest = false;
                        _cryptoBalance.text = "";
                        _dollarBalance.text = "";
                        _emailController.text = "";
                        _selectedNetwork = "BEP20";
                        dropdownValue = 1;
                        _requestMessage.text = "";
                        fetchCrypto().then((value) {
                          setState(() {
                            _cryptoList.clear();
                            _cryptoList.addAll(value.where((c) => c.network == _selectedNetwork).toList());
                            _selectedCrypto = _cryptoList[0].symbol.toString();
                          });
                        });
                      });
                    },
                    child: Row(
                      children: <Widget>[
                        Spacer(),
                        Text(
                          "Make a new request",
                          style: GoogleFonts.poppins(
                            color: Colors.white,
                            fontSize: MediaQuery.of(context).size.height * 0.019,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        Spacer(),
                      ],
                    ),
                  ),
                ),
                SizedBox(height: MediaQuery.of(context).size.height * 0.012),
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.06,
                  width: MediaQuery.of(context).size.width * 0.8,
                  child: ElevatedButton(
                    style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all(
                        Colors.white,
                      ),
                      shape: MaterialStateProperty.all(
                        RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.07),
                        ),
                      ),
                    ),
                    onPressed: () {
                      Provider.of<WalletController>(context, listen: false).selectedIndex = 2;
                      gotoPageGlobal(context, WelcomeScreen());
                    },
                    child: Row(
                      children: <Widget>[
                        Spacer(),
                        Text(
                          "Back to Wallet",
                          style: GoogleFonts.poppins(
                            color: const Color(0XFF4048FF),
                            fontSize: MediaQuery.of(context).size.height * 0.019,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        Spacer(),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        )
      ],
    );
  }

  Future save(String currency, String amountCrypto, String mail, String message, String wallet) async {
    var userDetails = Provider.of<LoginController>(context, listen: false).userDetails;
    if (userDetails?.email != mail) {
      var receiveRequestResult = await apiService.sendReceiveRequest(userDetails?.userToken ?? "", currency, message, amountCrypto, mail.replaceAll(" ", ""), wallet, userDetails?.email ?? "");
      if (receiveRequestResult) {
        return receiveRequestResult;
      } else
        return false;
    } else {
      return false;
    }
  }

  myWallet(String token) async {
    var wallet = await apiService.getWalletAddress(token, version);
    var walletResponse = json.decode(wallet);
    var walletAddress = walletResponse["address"];
    return walletAddress;
  }

  setDollarBalance(String selectedCrypto, String value) {
    var price = _cryptoList.firstWhere((e) => e.symbol == selectedCrypto).price ?? 0;
    value = value == "" ? "0" : value;
    var amount = (price * double.parse(value));
    return formatDollarBalance(amount);
  }

  setCryptoBalance(String selectedCrypto, String value) {
    var price = _cryptoList.firstWhere((e) => e.symbol == selectedCrypto).price ?? 0;
    value = value == "" ? "0" : value;
    var amount = 0.0;
    if (price > 0) {
      amount = double.parse(value) / price;
    }
    return formatCryptoBalance(amount, "");
  }

  setFieldSelection(Crypto crypto, String paymentMethod, String selectedNetwork, bool loadingNetwork) {
    _selectedCrypto = crypto.symbol.toString();
    _clickedCryptoContainer = false;
    _clickedContainerPaymentMethod = false;
    setCryptoSelection(crypto);
    _cryptoBalance.text = "";
    _dollarBalance.text = "";
    _selectedPaymentMethod = paymentMethod;
    _selectedNetwork = selectedNetwork;
    _isLoadingNetwork = loadingNetwork;
  }

  setCryptoSelection(Crypto crypto) {
    _selectedCrypto = crypto.symbol.toString();
    _selectedCryptoAddedToken = crypto.addedToken ?? false;
    _selectedCryptoPicUrl = crypto.picUrl.toString();
    _selectedCryptoName = crypto.name.toString();
    _selectedCryptoSymbol = crypto.symbol.toString();
  }
}

class DecimalTextInputFormatter extends TextInputFormatter {
  DecimalTextInputFormatter({this.decimalRange}) : assert(decimalRange == null || decimalRange > 0);

  final int? decimalRange;

  @override
  TextEditingValue formatEditUpdate(
    TextEditingValue oldValue, // unused.
    TextEditingValue newValue,
  ) {
    TextSelection newSelection = newValue.selection;
    String truncated = newValue.text;

    if (decimalRange != null) {
      String value = newValue.text;

      if (value.contains(".") && value.substring(value.indexOf(".") + 1).length > decimalRange!) {
        truncated = oldValue.text;
        newSelection = oldValue.selection;
      } else if (value == ".") {
        truncated = "0.";

        newSelection = newValue.selection.copyWith(
          baseOffset: math.min(truncated.length, truncated.length + 1),
          extentOffset: math.min(truncated.length, truncated.length + 1),
        );
      }

      return TextEditingValue(
        text: truncated,
        selection: newSelection,
        composing: TextRange.empty,
      );
    }
    return newValue;
  }
}

class CustomMaxValueInputFormatter extends TextInputFormatter {
  final double? maxInputValue;

  CustomMaxValueInputFormatter({this.maxInputValue});

  @override
  TextEditingValue formatEditUpdate(TextEditingValue oldValue, TextEditingValue newValue) {
    final TextSelection newSelection = newValue.selection;
    String truncated = newValue.text;
    if (maxInputValue != null) {
      try {
        final double value = double.parse(newValue.text);
        if (value == null) {
          return TextEditingValue(
            text: truncated,
            selection: newSelection,
          );
        }
        if (value > double.parse(maxInputValue.toString())) {
          truncated = oldValue.text.toString();
        }
      } catch (e) {
        print(e);
      }
    }
    return TextEditingValue(
      text: truncated,
      selection: newSelection,
    );
  }
}