import 'dart:async';
import 'dart:convert';
import 'dart:core';
import 'dart:io';
import 'dart:typed_data';

import 'package:country_picker/country_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hive/hive.dart';
import 'package:ihave/controllers/LoginController.dart';
import 'package:ihave/controllers/walletController.dart';
import 'package:ihave/model/userDetails.dart';
import 'package:ihave/service/apiService.dart';
import 'package:ihave/ui/WelcomeScreen.dart';
import 'package:ihave/ui/mywallet.dart';
import 'package:ihave/ui/notificationPage.dart';
import 'package:ihave/ui/settings.dart';
import 'package:ihave/ui/staticElements/headerNavigation.dart';
import 'package:ihave/util/utils.dart';
import 'package:ihave/widgets/customAppBar.dart';
import 'package:ihave/widgets/customBottomBar.dart';
import 'package:image_picker/image_picker.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:pinput/pin_put/pin_put.dart';
import 'package:provider/provider.dart';
import 'package:step_progress_indicator/step_progress_indicator.dart';
import 'package:intl/intl.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:custom_image_crop/custom_image_crop.dart';
import 'package:permission_handler/permission_handler.dart';

import '../util/hiveUtils.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({Key? key}) : super(key: key);

  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  late CustomImageCropController controller;
  final FocusNode _pinPutFocusNode = FocusNode();
  var firstnameController = TextEditingController();
  var lastnameController = TextEditingController();
  var genderController = TextEditingController();
  var birthdayController = TextEditingController();
  var localeController = TextEditingController();
  var emailController = TextEditingController();
  var addressController = TextEditingController();
  var codeController = TextEditingController();
  var cityController = TextEditingController();
  var countryController = TextEditingController();
  var phoneController = TextEditingController();
  var _newEmailController = TextEditingController();
  var _passwordController = TextEditingController();
  var _pinPutController = TextEditingController();
  var biometrics = TextEditingController();
  APIService apiService = LoginController.apiService;
  bool _isVisible = false;
  bool _isProfileVisible = false;
  bool _isWalletVisible = false;
  bool _isNotificationVisible = false;
  bool _isClickedImage = false;
  bool _isNewPictureUpdate = false;
  bool _changeMail = false;
  bool _changeMailButtonActive = true;
  bool _changeMailButtonColor = false;
  bool _obscureText = true;
  bool pinputEditor = false;
  final _formKeyProfile = GlobalKey<FormState>();
  final _formKeyProfileEmail = GlobalKey<FormState>();
  File? _pickedImage;
  File? _newPickedImage;
  File? _finallyPickedImage;
  bool _userPictureSocialExist = false;
  String userPictureSocial = "";
  bool _isLoadingNewPicture = false;
  bool _isActivatedButtonPicture = true;
  bool _validatorForms = true;
  String _errorMessageUpdateEmail = "";
  String _errorMessageConfirmCodeEmail = "";
  bool _errorUpdateProfile = false;
  bool _successUpdateProfile = false;
  bool _emailChangedWithSuccess = false;
  bool _noBirthdayDate = true;
  bool _isFormVisible = false;
  int _profileCompletedAt = 0;
  int _stepSelected = 0;
  Uint8List? userPicture;
  bool _userPictureExist = false;
  DateTime? selectedDate;

  DateTime? birthdayDate;
  String _userAvatarPicture = "";
  Country? _selectedCountry;
  bool _userAvatarWelcome = false;
  bool _isLoadingPicture = true;
  bool _birthdayDataExist = false;
  String yearBirthday = "";
  String monthBirthday = "";
  String dayBirthday = "";
  String newYear = "";
  String newMonth = "";
  String newDay = "";
  bool _genderClicked = false;
  String _genderSelected = "";
  String _firstGenderToSelect = "";
  String _secondGenderToSelect = "";
  bool _manSelected = false;
  bool _womanSelected = false;
  bool _notSpecified = false;
  late Box box;
  late Map<dynamic, dynamic> boxMap;
  List<String> loginList = [];
  bool _successUpdatePicture = false;
  Uint8List? userPictureTake;
  Uint8List? picture;
  bool _showDialogOpened = false;
  bool _isLoadingUserPicture = true;
  var myFormat = DateFormat('yyyy-MM-dd');
  late StreamSubscription subscription;
  bool _hasNetwork = true;
  bool _mailFieldForm = false;

  bool ischecked = false;

  bool _isOversized = false;

  Future<void> _selectedDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(context: context, initialDate: birthdayController.text != "dd/mm/yyyy" ? DateTime(int.parse(yearBirthday), int.parse(monthBirthday), int.parse(dayBirthday)) : DateTime.now(), firstDate: DateTime(1920, 8), lastDate: DateTime(2101)).then((data) {
      if (data?.day != null && data?.month != null && data?.year != null) {
        setState(() {
          var month = data?.month != null && data!.month < 10 ? "0${data.month}" : "${data?.month}";
          birthdayController.text = "${data?.day}/$month/${data?.year}";
          yearBirthday = data?.year.toString() ?? "";
          monthBirthday = month;
          dayBirthday = data?.day.toString() ?? "";
        });
      }
    });
  }

  fillLoginList() {
    boxMap = box.toMap();
    loginList.clear();
    boxMap.forEach((key, value) {
      loginList.add(value["email"]);
    });
  }

  _pickImageCamera() async {
    setState(() {
      _isOversized = false;
    });
    final picker = ImagePicker();
    final pickedImage = await picker.getImage(source: ImageSource.camera);

    if (pickedImage != null) {
      final pickedImageFile = File(pickedImage.path);
      var sizeImage = (pickedImageFile.readAsBytesSync().lengthInBytes / 1024) / 1024;
      if (sizeImage > 5) {
        setState(() {
          _isOversized = true;
        });
      } else {
        setState(() {
          _pickedImage = pickedImageFile;
        });
      }
      return _pickedImage;
    }
  }

  _pickImageGallery() async {
    setState(() {
      _isOversized = false;
    });
    final picker = ImagePicker();
    final pickedImage = await picker.getImage(source: ImageSource.gallery);
    final pickedImageFile = File(pickedImage!.path);
    var sizeImage = (pickedImageFile.readAsBytesSync().lengthInBytes / 1024) / 1024;
    if (sizeImage > 5) {
      setState(() {
        _isOversized = true;
      });
    } else {
      setState(() {
        _pickedImage = pickedImageFile;
      });
    }
    return _pickedImage;
  }

  void _remove() {
    setState(() {
      _pickedImage = null;
    });
  }

  Future getUser() async {
    var user = await Provider.of<LoginController>(context, listen: false).getUserDetails(Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "");
    return user;
  }

  percentageCalculation(String email, String firstName, String lastName, String address, String city, String zipCode, String country, String phone, String gender, String birthday) {
    double countData = 1;
    if (email != "") {
      countData++;
    }
    if (firstName != "") {
      countData++;
    }
    if (lastName != "") {
      countData++;
    }
    if (address != "") {
      countData++;
    }
    if (city != "") {
      countData++;
    }
    if (zipCode != "") {
      countData++;
    }
    if (country != "") {
      countData++;
    }
    if (phone != null) {
      countData++;
    }
    if (gender != "") {
      countData++;
    }
    if (birthday != "") {
      countData++;
    }
    return countData;
  }

  getUserPicture() {
    var picture = Provider.of<LoginController>(context, listen: false).userPicture;
    if (picture.toString().length > 112) {
      setState(() {
        userPicture = picture;
        _userPictureExist = true;
        userPictureSocial = "";
        _userPictureSocialExist = false;
        _userAvatarWelcome = false;
      });
    } else {
      getUser().then((value) {
        if (value.picLink != "" && value.picLink != null && value.picLink != "AppleIDAuthorizationScopes.fullName") {
          setState(() {
            _userPictureExist = false;
            _userPictureSocialExist = true;
            userPictureSocial = value.picLink;
            _userAvatarWelcome = false;
          });
        } else {
          setState(() {
            _userPictureExist = false;
            _userPictureSocialExist = false;
            userPictureSocial = "";
            _userAvatarWelcome = true;
          });
        }
      });
    }
    setState(() {
      _isLoadingPicture = false;
    });
  }

  stepCalculation(int profileCompleted) {
    int stepColor = 0;
    if (profileCompleted < 25) {
      stepColor = 0;
    }
    if (profileCompleted >= 25 && profileCompleted <= 50) {
      stepColor = 1;
    }
    if (profileCompleted > 50 && profileCompleted <= 75) {
      stepColor = 2;
    }
    if (profileCompleted > 75 && profileCompleted < 100) {
      stepColor = 3;
    }
    if (profileCompleted == 100) {
      stepColor = 4;
    }
    return stepColor;
  }

  Future updateUser() async {
    var newuserDetails = newUser();
    var newUserUpdated = await Provider.of<LoginController>(context, listen: false).putUserDetails(Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "", newuserDetails);
    return newUserUpdated;
  }

  void initState() {
    super.initState();
    createOpenBox().then((value) {
      setState(() {
        box = value;
        fillLoginList();
      });
    });
    controller = CustomImageCropController();
    subscription = InternetConnectionChecker().onStatusChange.listen((status) {
      final hasInternet = status == InternetConnectionStatus.connected;
      setState(() {
        _hasNetwork = hasInternet;
      });
    });
    setState(() {
      _isLoadingPicture = true;
      ischecked = LoginController.apiService.isSwitched;
    });
    getUserPicture();
    getUser().then((value) {
      setState(() {
        ischecked = value.hasbiometrics;
      });
      var countResult =
          percentageCalculation(value.email.toString(), value.firstName.toString(), value.lastName.toString(), value.address.toString(), value.city.toString(), value.zipCode.toString(), value.country.toString(), value.phone.toString(), value.gender.toString(), value.birthday.toString());
      if (value.birthday.toString().isNotEmpty && value.birthday.toString() != "null-null-null") {
        setState(() {
          String birthday = value.birthday.toString().replaceFirst('-', '/');
          yearBirthday = birthday.substring(0, 4);
          monthBirthday = birthday.substring(birthday.indexOf('/') + 1, birthday.indexOf('-'));
          if (birthday.length == 10 || birthday.length == 9 || birthday.length == 8) {
            dayBirthday = birthday.substring(birthday.indexOf('-') + 1);
          } else {
            dayBirthday = birthday.substring(birthday.indexOf('-') + 1, birthday.indexOf('T'));
          }

          _birthdayDataExist = true;
        });
      } else {
        setState(() {
          _birthdayDataExist = false;
        });
      }
      setState(() {
        _genderSelected = value.gender.toString() == "Not Specified" ? "Not Specified" : (value.gender.toString() == "Man" ? "Man" : "Woman");
        _firstGenderToSelect = _genderSelected == "Not Specified" ? "Man" : (_genderSelected == "Man" ? "Not Specified" : (_genderSelected == "Woman" ? "Man" : ""));
        _secondGenderToSelect = _genderSelected == "Not Specified" ? "Woman" : (_genderSelected == "Man" ? "Woman" : (_genderSelected == "Woman" ? "Not Specified" : ""));
        _profileCompletedAt = ((countResult / 11) * 100).toInt();
        _stepSelected = stepCalculation(_profileCompletedAt);
        emailController.text = value.email.toString();
        firstnameController.text = value.firstName.toString();
        lastnameController.text = value.lastName.toString();
        addressController.text = value.address.toString();
        cityController.text = value.city.toString();
        codeController.text = value.zipCode.toString();
        countryController.text = (value.country.toString().isNotEmpty) ? value.country.toString() : "Select country";
        phoneController.text = value.phone != null && value.phone != "null" ? value.phone.toString() : "";
        genderController.text = value.gender.toString();
        selectedDate = birthdayDate;
        birthdayController.text = (_birthdayDataExist) ? "${dayBirthday}/${monthBirthday}/${yearBirthday}" : "dd/mm/yyyy";
        localeController.text = value.locale.toString();
        biometrics.text = value.hasbiometrics.toString();
      });
    });
    Provider.of<LoginController>(context, listen: false).tokenSymbol == "";
    Provider.of<LoginController>(context, listen: false).tokenNetwork == "";
  }

  UserDetails newUser() {
    if (birthdayController.text != "dd/mm/yyyy") {
      String birthday = birthdayController.text.replaceAll('/', '-');
      String birthdayFormat = birthday.replaceFirst('-', '/');
      String day = birthdayFormat.substring(0, birthdayFormat.indexOf('/'));
      String month = birthdayFormat.substring(birthdayFormat.indexOf('/') + 1, birthdayFormat.indexOf('-'));
      String year = birthdayFormat.substring(birthdayFormat.indexOf('-') + 1);
      return new UserDetails(
        email: emailController.text,
        firstName: firstnameController.text,
        lastName: lastnameController.text,
        address: addressController.text,
        city: cityController.text,
        zipCode: codeController.text,
        country: (countryController.text != "Select country") ? countryController.text : "",
        phone: phoneController.text,
        gender: encodeGender(_genderSelected),
        birthday: "${year}-${month}-${day}",
        locale: localeController.text,
        hasbiometrics: ischecked,
      );
    } else {
      return new UserDetails(
        email: emailController.text,
        firstName: firstnameController.text,
        lastName: lastnameController.text,
        address: addressController.text,
        city: cityController.text,
        zipCode: codeController.text,
        country: (countryController.text != "Select country") ? countryController.text : "",
        phone: phoneController.text,
        gender: encodeGender(_genderSelected),
        locale: localeController.text,
        hasbiometrics: ischecked,
      );
    }
  }

  @override
  void dispose() {
    controller.dispose();
    subscription.cancel();
    setState(() {
      _isLoadingPicture = true;
    });
    super.dispose();
  }

  BoxDecoration get _pinPutDecoration {
    return BoxDecoration(
      color: Colors.white,
      borderRadius: BorderRadius.circular(15.0),
      border: Border.all(
        color: Colors.white,
      ),
    );
  }

  Future<bool> checkAndRequestCameraPermissions() async {
    var cameraPermission = await Permission.camera.status;
    final permissionStatus = await Permission.camera.request();
    if (permissionStatus == PermissionStatus.granted) {
      return Permission.camera == PermissionStatus.granted;
    } else {
      return false;
    }
  }

  @override
  Widget build(BuildContext context) {
    setState(() {
      _isVisible = Provider.of<WalletController>(context, listen: true).isSideMenuVisible;
      _isProfileVisible = Provider.of<WalletController>(context, listen: true).isProfileVisible;
      _isWalletVisible = Provider.of<WalletController>(context, listen: true).isWalletVisible;
      _isNotificationVisible = Provider.of<WalletController>(context, listen: true).isNotificationVisible;
    });
    if (_hasNetwork) {
      if (birthdayController.text != "") {
        if (!_showDialogOpened) {
          return WillPopScope(
              onWillPop: () {
                return Future.value(true);
              },
              child: Scaffold(
                  appBar: AppBar(
                      elevation: 0,
                      leadingWidth: 70,
                      backgroundColor: Colors.transparent,
                      flexibleSpace: Container(
                        decoration: BoxDecoration(
                            gradient: LinearGradient(
                                colors: [
                                  Color(0xFF707070).withOpacity(0.6),
                                  Colors.transparent,
                                ],
                                begin: const FractionalOffset(0.0, 0.0),
                                end: const FractionalOffset(0.0, 1.0),
                                stops: [0.0, 1.0],
                                tileMode: TileMode.clamp)),
                      ),
                      leading: Padding(
                        padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.04, 0, 0, 0),
                        child: InkWell(
                          focusColor: Colors.transparent,
                          hoverColor: Colors.transparent,
                          splashColor: Colors.transparent,
                          highlightColor: Colors.transparent,
                          splashFactory: NoSplash.splashFactory,
                          child: CircleAvatar(
                            backgroundColor: Colors.transparent,
                            radius: 30,
                            child: Padding(
                              padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                              child: Transform.scale(
                                scale: 1,
                                child: SvgPicture.asset('images/logo-white.svg'),
                              ),
                            ),
                          ),
                          onTap: () {},
                        ),
                      ),
                      actions: <Widget>[
                        Container(
                          height: 60,
                          padding: const EdgeInsets.fromLTRB(0, 0, 10, 0),
                          child: InkWell(
                            focusColor: Colors.transparent,
                            hoverColor: Colors.transparent,
                            splashColor: Colors.transparent,
                            highlightColor: Colors.transparent,
                            splashFactory: NoSplash.splashFactory,
                            onTap: () {
                              gotoPageGlobal(context, MyWallet());
                            },
                            child: Container(
                              height: 60,
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  gradient: LinearGradient(
                                      colors: [
                                        Color.fromRGBO(50, 55, 84, 1),
                                        Color.fromRGBO(50, 55, 84, 0),
                                      ],
                                      begin: const FractionalOffset(0.0, 0.0),
                                      end: const FractionalOffset(0.0, 1.0),
                                      stops: [0.0, 1.0],
                                      tileMode: TileMode.clamp)),
                              child: CircleAvatar(
                                radius: MediaQuery.of(context).size.width * 0.1,
                                backgroundColor: Colors.transparent,
                                child: SvgPicture.asset(
                                  "images/wallet-icon.svg",
                                  width: 26,
                                  height: 26,
                                  color: Color(0xFFF6F6FF),
                                ),
                              ),
                            ),
                          ),
                        ),
                        Container(
                          padding: const EdgeInsets.fromLTRB(0, 0, 10, 0),
                          child: InkWell(
                            focusColor: Colors.transparent,
                            hoverColor: Colors.transparent,
                            splashColor: Colors.transparent,
                            highlightColor: Colors.transparent,
                            splashFactory: NoSplash.splashFactory,
                            onTap: () {
                              gotoPageGlobal(context, Setting());
                            },
                            child: _isNewPictureUpdate
                                ? CircleAvatar(
                                    radius: 30,
                                    backgroundColor: Colors.white,
                                    child: CircleAvatar(
                                      radius: 30,
                                      backgroundImage: FileImage(_newPickedImage!),
                                    ),
                                  )
                                : Container(
                                    decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        gradient: LinearGradient(
                                            colors: [
                                              Color.fromRGBO(50, 55, 84, 1),
                                              Color.fromRGBO(50, 55, 84, 0),
                                            ],
                                            begin: const FractionalOffset(0.0, 0.0),
                                            end: const FractionalOffset(0.0, 1.0),
                                            stops: [0.0, 1.0],
                                            tileMode: TileMode.clamp)),
                                    child: CircleAvatar(
                                        backgroundColor: Colors.transparent,
                                        radius: 30,
                                        backgroundImage: _userPictureExist ? MemoryImage(Provider.of<LoginController>(context).userPicture!) : (_userPictureSocialExist ? NetworkImage(userPictureSocial) : AssetImage("images/avatar-welcome.png") as ImageProvider)),
                                  ),
                          ),
                        ),
                        Container(
                          padding: const EdgeInsets.fromLTRB(0, 0, 15, 0),
                          child: InkWell(
                            focusColor: Colors.transparent,
                            hoverColor: Colors.transparent,
                            splashColor: Colors.transparent,
                            highlightColor: Colors.transparent,
                            splashFactory: NoSplash.splashFactory,
                            onTap: () {
                              Provider.of<WalletController>(context, listen: false).toggleWelcomeScreen(false);
                              gotoPageGlobal(context, NotificationPage());
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  gradient: LinearGradient(
                                      colors: [
                                        Color.fromRGBO(50, 55, 84, 1),
                                        Color.fromRGBO(50, 55, 84, 0),
                                      ],
                                      begin: const FractionalOffset(0.0, 0.0),
                                      end: const FractionalOffset(0.0, 1.0),
                                      stops: [0.0, 1.0],
                                      tileMode: TileMode.clamp)),
                              child: CircleAvatar(
                                radius: 30,
                                backgroundColor: Colors.transparent,
                                child: SvgPicture.asset(
                                  'images/notif-icon.svg',
                                  height: 20,
                                  width: 18.96,
                                ),
                              ),
                            ),
                          ),
                        )
                      ]),
                  bottomNavigationBar: CustomBottomBar(),
                  body: Stack(
                    children: [
                      SingleChildScrollView(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: EdgeInsets.only(top: MediaQuery.of(context).size.width * 0.08),
                              child: Container(
                                alignment: Alignment.center,
                                margin: EdgeInsets.only(
                                  left: MediaQuery.of(context).size.width * 0.0008,
                                  right: MediaQuery.of(context).size.width * 0.0008,
                                ),
                                child: Column(mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.start, children: [
                                  Padding(
                                    padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.height * 0.03, MediaQuery.of(context).size.height * 0.03, 0, MediaQuery.of(context).size.height * 0.02),
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          "Profile",
                                          textAlign: TextAlign.left,
                                          style: GoogleFonts.poppins(color: Color(0xFF1F2337), fontSize: MediaQuery.of(context).size.width * 0.067, fontWeight: FontWeight.bold, fontStyle: FontStyle.normal, letterSpacing: 0.8),
                                        ),
                                        Text(
                                          "Completed at ${_profileCompletedAt}%",
                                          style: GoogleFonts.poppins(fontWeight: FontWeight.w600, fontSize: MediaQuery.of(context).size.width * 0.047, color: Color(0xFF00cc9e)),
                                        )
                                      ],
                                    ),
                                  ),
                                  Form(
                                    key: _formKeyProfile,
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Padding(
                                          padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.height * 0.03, MediaQuery.of(context).size.height * 0.025, 0, 0),
                                          child: Text(
                                            "Personal informations",
                                            textAlign: TextAlign.center,
                                            style: GoogleFonts.poppins(color: Colors.black, fontSize: MediaQuery.of(context).size.width * 0.045, fontWeight: FontWeight.w700, letterSpacing: 1),
                                          ),
                                        ),
                                        Center(
                                          child: new Stack(children: [
                                            Container(
                                                margin: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.width * 0.035),
                                                child: _isNewPictureUpdate
                                                    ? CircleAvatar(
                                                        radius: 71,
                                                        backgroundColor: Colors.white,
                                                        child: CircleAvatar(
                                                          radius: 65,
                                                          backgroundImage: FileImage(_newPickedImage!),
                                                        ),
                                                      )
                                                    : ((_userPictureExist)
                                                        ? CircleAvatar(
                                                            radius: 71,
                                                            backgroundColor: Colors.white,
                                                            child: CircleAvatar(
                                                              radius: 65,
                                                              backgroundImage: MemoryImage(userPicture!),
                                                            ),
                                                          )
                                                        : ((_userPictureSocialExist)
                                                            ? CircleAvatar(
                                                                radius: 71,
                                                                backgroundColor: Colors.white,
                                                                child: CircleAvatar(
                                                                  radius: 65,
                                                                  backgroundImage: NetworkImage(userPictureSocial),
                                                                ),
                                                              )
                                                            : CircleAvatar(
                                                                radius: 71,
                                                                backgroundColor: Colors.white,
                                                                child: CircleAvatar(
                                                                  radius: 65,
                                                                  backgroundImage: AssetImage("images/avatar-welcome.png"),
                                                                ),
                                                              )))),
                                            Positioned(
                                              top: 79,
                                              left: 78,
                                              child: SizedBox(
                                                height: MediaQuery.of(context).size.width * 0.185,
                                                width: MediaQuery.of(context).size.width * 0.185,
                                                child: RawMaterialButton(
                                                    elevation: 10,
                                                    child: SvgPicture.asset(
                                                      'images/modif_but.svg',
                                                    ),
                                                    padding: EdgeInsets.all(15.0),
                                                    shape: CircleBorder(),
                                                    onPressed: () {
                                                      setState(() {
                                                        _showDialogOpened = true;
                                                      });
                                                    }),
                                              ),
                                            )
                                          ]),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.12, 0, 0, MediaQuery.of(context).size.height * 0.007),
                                          child: Align(
                                            alignment: Alignment.bottomLeft,
                                            child: Text(
                                              "FIRST NAME",
                                              style: GoogleFonts.poppins(color: Color(0xFF75758f), fontSize: MediaQuery.of(context).size.width * 0.032, fontWeight: FontWeight.w600, letterSpacing: 0.5),
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.085),
                                          child: Align(
                                            alignment: Alignment.bottomLeft,
                                            child: Container(
                                              width: MediaQuery.of(context).size.width * 0.83,
                                              padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                              child: TextFormField(
                                                style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.033, fontStyle: FontStyle.normal, fontWeight: FontWeight.normal),
                                                controller: firstnameController,
                                                onChanged: (value) {},
                                                decoration: InputDecoration(
                                                    fillColor: Colors.white,
                                                    focusColor: Colors.white,
                                                    hoverColor: Colors.white,
                                                    filled: true,
                                                    isDense: true,
                                                    hintText: "",
                                                    contentPadding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height * 0.015, horizontal: MediaQuery.of(context).size.width * 0.033),
                                                    enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.grey.withOpacity(0.5))),
                                                    focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.grey.withOpacity(0.5))),
                                                    errorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red)),
                                                    focusedErrorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red))),
                                              ),
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.12, MediaQuery.of(context).size.height * 0.035, 0, MediaQuery.of(context).size.height * 0.007),
                                          child: Align(
                                            alignment: Alignment.bottomLeft,
                                            child: Text(
                                              "LAST NAME",
                                              style: GoogleFonts.poppins(color: Color(0xFF75758f), fontSize: MediaQuery.of(context).size.width * 0.032, fontWeight: FontWeight.w600, letterSpacing: 0.5),
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.085),
                                          child: Align(
                                            alignment: Alignment.bottomLeft,
                                            child: Container(
                                              width: MediaQuery.of(context).size.width * 0.83,
                                              child: TextFormField(
                                                controller: lastnameController,
                                                onChanged: (value) {},
                                                style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.033, fontStyle: FontStyle.normal, fontWeight: FontWeight.normal),
                                                decoration: InputDecoration(
                                                    fillColor: Colors.white,
                                                    focusColor: Colors.white,
                                                    hoverColor: Colors.white,
                                                    filled: true,
                                                    isDense: true,
                                                    hintText: "",
                                                    contentPadding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height * 0.015, horizontal: MediaQuery.of(context).size.width * 0.033),
                                                    enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.grey.withOpacity(0.5))),
                                                    focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.grey.withOpacity(0.5))),
                                                    errorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red)),
                                                    focusedErrorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red))),
                                              ),
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.12, MediaQuery.of(context).size.height * 0.035, 0, MediaQuery.of(context).size.height * 0.007),
                                          child: Align(
                                            alignment: Alignment.bottomLeft,
                                            child: Text(
                                              "GENDER",
                                              style: GoogleFonts.poppins(color: Color(0xFF75758f), fontSize: MediaQuery.of(context).size.width * 0.032, fontWeight: FontWeight.w600, letterSpacing: 0.5),
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.085),
                                          child: Align(
                                            alignment: Alignment.bottomLeft,
                                            child: Container(
                                              width: MediaQuery.of(context).size.width * 0.83,
                                              decoration: BoxDecoration(border: Border.all(color: Colors.grey.withOpacity(0.5)), borderRadius: _genderClicked ? BorderRadius.circular(30) : BorderRadius.circular(60)),
                                              child: InkWell(
                                                onTap: () {
                                                  setState(() {
                                                    _genderClicked = !_genderClicked;
                                                  });
                                                },
                                                child: Column(
                                                  children: <Widget>[
                                                    Padding(
                                                      padding: EdgeInsets.only(
                                                        top: MediaQuery.of(context).size.width * 0.009,
                                                        bottom: MediaQuery.of(context).size.width * 0.009,
                                                      ),
                                                      child: Row(
                                                        children: <Widget>[
                                                          Padding(
                                                            padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.03, top: MediaQuery.of(context).size.width * 0.02, bottom: MediaQuery.of(context).size.width * 0.02),
                                                            child: Text(
                                                              _genderSelected,
                                                              style: GoogleFonts.poppins(color: Colors.black, fontSize: MediaQuery.of(context).size.width * 0.033, fontStyle: FontStyle.normal, fontWeight: FontWeight.normal),
                                                            ),
                                                          ),
                                                          Spacer(),
                                                          Padding(
                                                            padding: EdgeInsets.only(
                                                              right: MediaQuery.of(context).size.width * 0.03,
                                                            ),
                                                            child: Icon(
                                                              Icons.arrow_drop_down,
                                                              color: Colors.grey,
                                                              size: MediaQuery.of(context).size.width * 0.07,
                                                            ),
                                                          )
                                                        ],
                                                      ),
                                                    ),
                                                    Padding(
                                                        padding: EdgeInsets.only(),
                                                        child: _genderClicked
                                                            ? Column(
                                                                children: [
                                                                  Padding(
                                                                    padding: EdgeInsets.only(top: MediaQuery.of(context).size.width * 0.02, left: MediaQuery.of(context).size.width * 0.03, bottom: MediaQuery.of(context).size.width * 0.02),
                                                                    child: Align(
                                                                      alignment: Alignment.bottomLeft,
                                                                      child: InkWell(
                                                                        onTap: () {
                                                                          var tap = _firstGenderToSelect;
                                                                          var selected = _genderSelected;
                                                                          setState(() {
                                                                            _genderSelected = tap;
                                                                            _firstGenderToSelect = selected;
                                                                            _genderClicked = false;
                                                                          });
                                                                        },
                                                                        child: Text(
                                                                          _firstGenderToSelect,
                                                                          style: GoogleFonts.poppins(color: Colors.black, fontSize: MediaQuery.of(context).size.width * 0.033, fontStyle: FontStyle.normal, fontWeight: FontWeight.normal),
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ),
                                                                  Padding(
                                                                    padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.03, bottom: MediaQuery.of(context).size.width * 0.033),
                                                                    child: Align(
                                                                      alignment: Alignment.bottomLeft,
                                                                      child: InkWell(
                                                                        onTap: () {
                                                                          var tap = _secondGenderToSelect;
                                                                          var selected = _genderSelected;
                                                                          setState(() {
                                                                            _secondGenderToSelect = selected;
                                                                            _genderSelected = tap;
                                                                            _genderClicked = false;
                                                                          });
                                                                        },
                                                                        child: Text(
                                                                          _secondGenderToSelect,
                                                                          style: GoogleFonts.poppins(color: Colors.black, fontSize: MediaQuery.of(context).size.width * 0.033, fontStyle: FontStyle.normal, fontWeight: FontWeight.normal),
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ),
                                                                ],
                                                              )
                                                            : null)
                                                  ],
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.12, MediaQuery.of(context).size.height * 0.035, 0, MediaQuery.of(context).size.height * 0.007),
                                          child: Align(
                                            alignment: Alignment.bottomLeft,
                                            child: Text(
                                              "BIRTHDAY",
                                              style: GoogleFonts.poppins(color: Color(0xFF75758f), fontSize: MediaQuery.of(context).size.width * 0.032, fontWeight: FontWeight.w600, letterSpacing: 0.5),
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.085),
                                          child: Align(
                                            alignment: Alignment.bottomLeft,
                                            child: Container(
                                              width: MediaQuery.of(context).size.width * 0.83,
                                              child: InkWell(
                                                onTap: () => _selectedDate(context),
                                                child: IgnorePointer(
                                                  child: TextFormField(
                                                    style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.033, fontStyle: FontStyle.normal, fontWeight: FontWeight.normal),
                                                    controller: birthdayController,
                                                    onChanged: (value) {
                                                      birthdayController.text = _selectedDate(context) as String;
                                                    },
                                                    decoration: InputDecoration(
                                                      enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.grey.withOpacity(0.5))),
                                                      focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.grey.withOpacity(0.5))),
                                                      contentPadding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height * 0.012, horizontal: MediaQuery.of(context).size.width * 0.033),
                                                      border: OutlineInputBorder(
                                                        borderRadius: BorderRadius.circular(30.0),
                                                      ),
                                                      suffixIcon: Padding(
                                                        padding: EdgeInsets.fromLTRB(0, 0, 10, 0),
                                                        child: Icon(Icons.date_range),
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.12, MediaQuery.of(context).size.height * 0.035, 0, MediaQuery.of(context).size.height * 0.007),
                                          child: Align(
                                            alignment: Alignment.bottomLeft,
                                            child: Text(
                                              "CHOICE OF LANGUAGE",
                                              style: GoogleFonts.poppins(color: Color(0xFF75758f), fontSize: MediaQuery.of(context).size.width * 0.033, fontWeight: FontWeight.w600, letterSpacing: 0.5),
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.085),
                                          child: Align(
                                            alignment: Alignment.bottomLeft,
                                            child: Container(
                                              width: MediaQuery.of(context).size.width * 0.83,
                                              child: DropdownButtonFormField(
                                                decoration: InputDecoration(
                                                    fillColor: Colors.white,
                                                    focusColor: Colors.white,
                                                    hoverColor: Colors.white,
                                                    filled: true,
                                                    isDense: true,
                                                    contentPadding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height * 0.012, horizontal: MediaQuery.of(context).size.width * 0.033),
                                                    enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.grey.withOpacity(0.5))),
                                                    focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.grey.withOpacity(0.5))),
                                                    errorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red)),
                                                    focusedErrorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red))),
                                                alignment: Alignment.center,
                                                hint: localeController == 'English'
                                                    ? Text(
                                                        'English',
                                                        style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.035, color: Colors.black.withOpacity(0.5), fontStyle: FontStyle.normal, fontWeight: FontWeight.normal, letterSpacing: 0.4),
                                                      )
                                                    : Text(
                                                        'English',
                                                        style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.035, color: Colors.black.withOpacity(0.5), fontStyle: FontStyle.normal, fontWeight: FontWeight.normal, letterSpacing: 0.4),
                                                      ),
                                                isExpanded: true,
                                                iconSize: 30.0,
                                                style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.033, fontStyle: FontStyle.normal, fontWeight: FontWeight.normal),
                                                items: ['English', 'French', 'Arabic'].map(
                                                  (val) {
                                                    return DropdownMenuItem<String>(
                                                      value: val,
                                                      child: Text(val),
                                                    );
                                                  },
                                                ).toList(),
                                                onChanged: null,
                                              ),
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.fromLTRB(0, MediaQuery.of(context).size.height * 0.042, 0, MediaQuery.of(context).size.height * 0.027),
                                          child: Divider(
                                            thickness: 1,
                                            color: Colors.grey.shade200,
                                            indent: MediaQuery.of(context).size.width * 0.1,
                                            endIndent: MediaQuery.of(context).size.width * 0.1,
                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.085),
                                          child: Text(
                                            "Contact",
                                            style: GoogleFonts.poppins(
                                              color: Color(0xFF1f2337),
                                              fontSize: MediaQuery.of(context).size.width * 0.048,
                                              fontWeight: FontWeight.bold,
                                              fontStyle: FontStyle.normal,
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.12, MediaQuery.of(context).size.height * 0.016, 0, MediaQuery.of(context).size.height * 0.013),
                                          child: Align(
                                            alignment: Alignment.bottomLeft,
                                            child: Text(
                                              "EMAIL",
                                              style: GoogleFonts.poppins(color: Color(0xFF75758f), fontSize: MediaQuery.of(context).size.width * 0.033, fontWeight: FontWeight.w600, letterSpacing: 0.5),
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.085),
                                          child: Align(
                                            alignment: Alignment.bottomLeft,
                                            child: Container(
                                              width: MediaQuery.of(context).size.width * 0.83,
                                              child: TextFormField(
                                                style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.033, color: const Color(0XFF75758F), fontStyle: FontStyle.normal, fontWeight: FontWeight.normal),
                                                controller: emailController,
                                                onChanged: (value) {},
                                                readOnly: true,
                                                decoration: InputDecoration(
                                                    fillColor: const Color(0XFFF6F6FF),
                                                    focusColor: Colors.white,
                                                    hoverColor: Colors.white,
                                                    filled: true,
                                                    isDense: true,
                                                    hintText: "",
                                                    contentPadding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height * 0.015, horizontal: MediaQuery.of(context).size.width * 0.033),
                                                    enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.grey.withOpacity(0.5))),
                                                    focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.grey.withOpacity(0.5))),
                                                    errorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red)),
                                                    focusedErrorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red))),
                                              ),
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.085, MediaQuery.of(context).size.height * 0.023, 0, 0),
                                          child: Align(
                                            alignment: Alignment.bottomLeft,
                                            child: Container(
                                              decoration: BoxDecoration(
                                                gradient: LinearGradient(
                                                  colors: [Color.fromRGBO(245, 32, 121, -0.01), Color.fromRGBO(64, 72, 255, 1)],
                                                  begin: Alignment(-1, 0),
                                                  end: Alignment(1, 0),
                                                  stops: [0.0, 1.0],
                                                ),
                                                borderRadius: BorderRadius.circular(30),
                                              ),
                                              height: MediaQuery.of(context).size.width * 0.105,
                                              width: MediaQuery.of(context).size.width * 0.83,
                                              child: ElevatedButton(
                                                style: ButtonStyle(
                                                  backgroundColor: _changeMailButtonActive
                                                      ? MaterialStateProperty.all(
                                                          Color(0xFFFFFFFF),
                                                        )
                                                      : null,
                                                  shape: MaterialStateProperty.all(
                                                    RoundedRectangleBorder(
                                                      borderRadius: BorderRadius.circular(30),
                                                    ),
                                                  ),
                                                ),
                                                onPressed: _changeMailButtonActive
                                                    ? () {
                                                        setState(() {
                                                          _changeMail = true;
                                                          _changeMailButtonColor = true;
                                                          _changeMailButtonActive = false;
                                                          _errorMessageUpdateEmail = "";
                                                          _errorUpdateProfile = false;
                                                          _validatorForms = true;
                                                        });
                                                      }
                                                    : null,
                                                child: Row(
                                                  children: <Widget>[
                                                    Spacer(),
                                                    Text(
                                                      "Change",
                                                      style: GoogleFonts.poppins(color: Color(0xFF4048FF), fontWeight: FontWeight.w600, fontSize: MediaQuery.of(context).size.width * 0.042),
                                                    ),
                                                    Spacer(),
                                                  ],
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.fromLTRB(0, MediaQuery.of(context).size.height * 0.023, 0, 0),
                                          child: SizedBox(
                                            child: _changeMail
                                                ? Padding(
                                                    padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.09),
                                                    child: Align(
                                                      alignment: Alignment.bottomLeft,
                                                      child: Container(
                                                        width: MediaQuery.of(context).size.width * 0.83,
                                                        decoration: BoxDecoration(color: Color(0XFFF6F6FF), borderRadius: BorderRadius.all(Radius.circular(20))),
                                                        child: Form(
                                                          key: _formKeyProfileEmail,
                                                          child: Column(
                                                            mainAxisAlignment: MainAxisAlignment.start,
                                                            crossAxisAlignment: CrossAxisAlignment.start,
                                                            children: <Widget>[
                                                              Align(
                                                                alignment: Alignment.topRight,
                                                                child: Padding(
                                                                  padding: EdgeInsets.fromLTRB(0, MediaQuery.of(context).size.width * 0.023, MediaQuery.of(context).size.width * 0.013, 0),
                                                                  child: InkWell(
                                                                    onTap: () {
                                                                      setState(() {
                                                                        _changeMail = false;
                                                                        _passwordController.text = "";
                                                                        _newEmailController.text = "";
                                                                        _errorUpdateProfile = false;
                                                                        _validatorForms = true;
                                                                        _errorMessageUpdateEmail = "";
                                                                        _changeMailButtonColor = false;
                                                                        _changeMailButtonActive = true;
                                                                        _obscureText = true;
                                                                      });
                                                                    },
                                                                    child: CircleAvatar(
                                                                      radius: 13,
                                                                      backgroundColor: Color.fromRGBO(173, 173, 200, 1),
                                                                      child: Icon(
                                                                        Icons.close,
                                                                        color: Colors.white,
                                                                        size: MediaQuery.of(context).size.width * 0.042,
                                                                      ),
                                                                    ),
                                                                  ),
                                                                ),
                                                              ),
                                                              Padding(
                                                                padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.042, 0, 0, MediaQuery.of(context).size.height * 0.005),
                                                                child: Text(
                                                                  "NEW EMAIL",
                                                                  style: GoogleFonts.poppins(fontWeight: FontWeight.w700, fontSize: MediaQuery.of(context).size.width * 0.033, color: Color(0xFF75758f)),
                                                                ),
                                                              ),
                                                              Container(
                                                                padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.02, 0, 0, 0),
                                                                width: MediaQuery.of(context).size.width * 0.8,
                                                                child: TextFormField(
                                                                  controller: _newEmailController,
                                                                  onChanged: (value) {
                                                                    TextSelection previousSelection = _newEmailController.selection;
                                                                    _newEmailController.text = value;

                                                                    _newEmailController.selection = previousSelection;
                                                                    if (_newEmailController.text.isNotEmpty) {
                                                                      setState(() {
                                                                        _mailFieldForm = true;
                                                                      });
                                                                    } else {
                                                                      setState(() {
                                                                        _mailFieldForm = false;
                                                                      });
                                                                    }
                                                                    TextSelection.fromPosition(TextPosition(offset: _newEmailController.text.length));
                                                                    if (_newEmailController.text.isNotEmpty) {
                                                                      if (RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(value)) {
                                                                        setState(() {
                                                                          _validatorForms = true;
                                                                        });
                                                                      } else {
                                                                        setState(() {
                                                                          _validatorForms = false;
                                                                        });
                                                                      }
                                                                    }
                                                                  },
                                                                  validator: (value) {
                                                                    if (value!.isEmpty) {
                                                                      return 'Field required ';
                                                                    }
                                                                  },
                                                                  decoration: InputDecoration(
                                                                      fillColor: Colors.white,
                                                                      focusColor: Colors.white,
                                                                      hoverColor: Colors.white,
                                                                      filled: true,
                                                                      isDense: true,
                                                                      hintText: "",
                                                                      contentPadding: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width * 0.043, vertical: MediaQuery.of(context).size.width * 0.03),
                                                                      enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.grey.withOpacity(0.5))),
                                                                      focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.grey.withOpacity(0.5))),
                                                                      errorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red)),
                                                                      focusedErrorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red))),
                                                                ),
                                                              ),
                                                              _mailFieldForm
                                                                  ? Visibility(
                                                                      child: !_validatorForms
                                                                          ? Padding(
                                                                              padding: EdgeInsets.all(MediaQuery.of(context).size.width * 0.015),
                                                                              child: Text("Field Email Address must have a valid form", style: GoogleFonts.poppins(color: Color(0xfff52079), fontSize: MediaQuery.of(context).size.width * 0.031, fontWeight: FontWeight.normal)),
                                                                            )
                                                                          : Padding(padding: EdgeInsets.all(0.0)),
                                                                    )
                                                                  : Padding(padding: EdgeInsets.all(0.0)),
                                                              Column(
                                                                children: [
                                                                  Padding(
                                                                    padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.003, 0, MediaQuery.of(context).size.width * 0.02, 0),
                                                                    child: Text(
                                                                      "Please enter your",
                                                                      style: GoogleFonts.poppins(
                                                                        fontSize: MediaQuery.of(context).size.width * 0.037,
                                                                        letterSpacing: 0.7,
                                                                        fontWeight: FontWeight.w500,
                                                                        color: Color(0xFF323754),
                                                                      ),
                                                                    ),
                                                                  ),
                                                                  Padding(
                                                                    padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.05, 0, 0, 0),
                                                                    child: Text(
                                                                      "password to confirm",
                                                                      style: GoogleFonts.poppins(
                                                                        fontSize: MediaQuery.of(context).size.width * 0.037,
                                                                        letterSpacing: 0.7,
                                                                        fontWeight: FontWeight.w500,
                                                                        color: Color(0xFF323754),
                                                                      ),
                                                                    ),
                                                                  ),
                                                                ],
                                                              ),
                                                              Padding(
                                                                padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.042, MediaQuery.of(context).size.height * 0.03, 0, MediaQuery.of(context).size.height * 0.005),
                                                                child: Text(
                                                                  "PASSWORD",
                                                                  style: GoogleFonts.poppins(fontWeight: FontWeight.w700, fontSize: MediaQuery.of(context).size.width * 0.033, color: Color(0xFF75758f)),
                                                                ),
                                                              ),
                                                              Container(
                                                                padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.02, 0, 0, 0),
                                                                width: MediaQuery.of(context).size.width * 0.8,
                                                                child: TextFormField(
                                                                  controller: _passwordController,
                                                                  keyboardType: TextInputType.visiblePassword,
                                                                  obscureText: _obscureText,
                                                                  onChanged: (value) {},
                                                                  validator: (value) {
                                                                    if (value!.isEmpty) {
                                                                      setState(() {
                                                                        _validatorForms = false;
                                                                      });
                                                                      return 'Field required';
                                                                    }
                                                                    return null;
                                                                  },
                                                                  decoration: InputDecoration(
                                                                      suffixIcon: Padding(
                                                                        padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.017),
                                                                        child: IconButton(
                                                                            onPressed: () {
                                                                              setState(() {
                                                                                _obscureText = !_obscureText;
                                                                              });
                                                                            },
                                                                            icon: _obscureText
                                                                                ? SvgPicture.asset(
                                                                                    "images/visibility-icon-off.svg",
                                                                                    height: MediaQuery.of(context).size.width * 0.025,
                                                                                    width: MediaQuery.of(context).size.width * 0.025,
                                                                                  )
                                                                                : SvgPicture.asset(
                                                                                    "images/visibility-icon-on.svg",
                                                                                    height: MediaQuery.of(context).size.width * 0.025,
                                                                                    width: MediaQuery.of(context).size.width * 0.025,
                                                                                  )),
                                                                      ),
                                                                      fillColor: Colors.white,
                                                                      focusColor: Colors.white,
                                                                      hoverColor: Colors.white,
                                                                      filled: true,
                                                                      isDense: true,
                                                                      hintText: "",
                                                                      contentPadding: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width * 0.043),
                                                                      enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.grey.withOpacity(0.5))),
                                                                      focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.grey.withOpacity(0.5))),
                                                                      errorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red)),
                                                                      focusedErrorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red))),
                                                                ),
                                                              ),
                                                              Padding(
                                                                padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.055, MediaQuery.of(context).size.height * 0.003, 0, 0),
                                                                child: _errorUpdateProfile
                                                                    ? Text(
                                                                        _errorMessageUpdateEmail,
                                                                        style: GoogleFonts.poppins(
                                                                          color: Colors.red,
                                                                        ),
                                                                      )
                                                                    : null,
                                                              ),
                                                              Center(
                                                                child: Padding(
                                                                  padding: EdgeInsets.only(
                                                                    bottom: MediaQuery.of(context).size.width * 0.04,
                                                                  ),
                                                                  child: Container(
                                                                    height: MediaQuery.of(context).size.width * 0.13,
                                                                    padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.height * 0.001, MediaQuery.of(context).size.height * 0.008, MediaQuery.of(context).size.height * 0.005, 0),
                                                                    width: MediaQuery.of(context).size.width * 0.35,
                                                                    child: ElevatedButton(
                                                                      style: ButtonStyle(
                                                                        backgroundColor: MaterialStateProperty.all(
                                                                          Color(0xFF00cc9e),
                                                                        ),
                                                                        shape: MaterialStateProperty.all(
                                                                          RoundedRectangleBorder(
                                                                            borderRadius: BorderRadius.circular(30),
                                                                          ),
                                                                        ),
                                                                      ),
                                                                      child: Row(
                                                                        children: <Widget>[
                                                                          Spacer(),
                                                                          Text(
                                                                            "Validate",
                                                                            style: GoogleFonts.poppins(fontWeight: FontWeight.w600, fontSize: MediaQuery.of(context).size.width * 0.04),
                                                                          ),
                                                                          Spacer(),
                                                                        ],
                                                                      ),
                                                                      onPressed: () async {
                                                                        if (_formKeyProfileEmail.currentState!.validate() && _validatorForms) {
                                                                          setState(() {
                                                                            _validatorForms = true;
                                                                          });
                                                                          var result = await LoginController.apiService.changeEmail(_newEmailController.text, _passwordController.text, Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "");
                                                                          if (result == "success") {
                                                                            setState(() {
                                                                              _successUpdateProfile = true;
                                                                              _changeMail = false;
                                                                              _errorUpdateProfile = false;
                                                                              _errorMessageUpdateEmail = "";
                                                                            });
                                                                          } else if (result == "wrong password") {
                                                                            setState(() {
                                                                              _errorMessageUpdateEmail = "Wrong Password";
                                                                              _errorUpdateProfile = true;
                                                                            });
                                                                            Future.delayed(const Duration(milliseconds: 2500), () {
                                                                              setState(() {
                                                                                _errorUpdateProfile = false;
                                                                                _errorMessageUpdateEmail = "";
                                                                              });
                                                                            });
                                                                          } else if (result == "duplicated email") {
                                                                            setState(() {
                                                                              _errorMessageUpdateEmail = "duplicated email";
                                                                              _errorUpdateProfile = true;
                                                                            });
                                                                          }
                                                                        }
                                                                      },
                                                                    ),
                                                                  ),
                                                                ),
                                                              )
                                                            ],
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  )
                                                : (_successUpdateProfile
                                                    ? Padding(
                                                        padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.085),
                                                        child: Align(
                                                          alignment: Alignment.bottomLeft,
                                                          child: Container(
                                                            width: MediaQuery.of(context).size.width * 0.83,
                                                            decoration: BoxDecoration(color: Color(0XFFF6F6FF), borderRadius: BorderRadius.all(Radius.circular(20))),
                                                            child: Column(
                                                              children: <Widget>[
                                                                Padding(
                                                                  padding: EdgeInsets.fromLTRB(0, MediaQuery.of(context).size.height * 0.015, 0, 0),
                                                                  child: Row(
                                                                    children: [
                                                                      Spacer(),
                                                                      Padding(
                                                                        padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.06, 0, 0, 0),
                                                                        child: Text(
                                                                          "Confirm it is you",
                                                                          style: GoogleFonts.poppins(fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.width * 0.053, color: Color(0xFF4048ff)),
                                                                        ),
                                                                      ),
                                                                      Spacer(),
                                                                      InkWell(
                                                                        onTap: () {
                                                                          setState(() {
                                                                            _successUpdateProfile = false;
                                                                            _changeMail = false;
                                                                            _passwordController.text = "";
                                                                            _newEmailController.text = "";
                                                                            _errorUpdateProfile = false;
                                                                            _validatorForms = true;
                                                                            _errorMessageUpdateEmail = "";
                                                                            _changeMailButtonColor = false;
                                                                            _changeMailButtonActive = true;
                                                                            _emailChangedWithSuccess = false;
                                                                            _errorMessageConfirmCodeEmail = "";
                                                                            _pinPutController.text = "";
                                                                            _obscureText = true;
                                                                            pinputEditor = false;
                                                                          });
                                                                        },
                                                                        child: CircleAvatar(
                                                                          radius: 13,
                                                                          backgroundColor: Color.fromRGBO(173, 173, 200, 1),
                                                                          child: Icon(
                                                                            Icons.close,
                                                                            color: Colors.white,
                                                                            size: 15,
                                                                          ),
                                                                        ),
                                                                      ),
                                                                      Spacer()
                                                                    ],
                                                                  ),
                                                                ),
                                                                Padding(
                                                                  padding: EdgeInsets.only(top: Platform.isIOS ? MediaQuery.of(context).size.width * 0.065 : MediaQuery.of(context).size.width * 0.033),
                                                                  child: Text(
                                                                    "Please enter the verification\ncode sent to your address",
                                                                    textAlign: TextAlign.center,
                                                                    style: GoogleFonts.poppins(
                                                                      color: Colors.black,
                                                                      fontWeight: FontWeight.normal,
                                                                      fontSize: MediaQuery.of(context).size.width * 0.04,
                                                                    ),
                                                                  ),
                                                                ),
                                                                Padding(
                                                                  padding: EdgeInsets.fromLTRB(0, MediaQuery.of(context).size.height * 0.007, 0, 0),
                                                                  child: Center(
                                                                    child: Text(
                                                                      "Please enter it",
                                                                      style: GoogleFonts.poppins(
                                                                        fontWeight: FontWeight.bold,
                                                                        fontSize: MediaQuery.of(context).size.width * 0.042,
                                                                        color: pinputEditor
                                                                            ? (_errorMessageConfirmCodeEmail != "" ? (_errorMessageConfirmCodeEmail == "Incorrect Code" || _errorMessageConfirmCodeEmail == "Expired Code)" ? Color(0xffF52079) : Color(0xff00CC9E)) : Color(0xFF4048ff))
                                                                            : Color(0xFF4048ff),
                                                                      ),
                                                                    ),
                                                                  ),
                                                                ),
                                                                Container(
                                                                  child: Padding(
                                                                    padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.05, MediaQuery.of(context).size.width * 0.01, MediaQuery.of(context).size.width * 0.05, 0),
                                                                    child: PinPut(
                                                                        textStyle: GoogleFonts.poppins(color: Color(0xFF4048ff)),
                                                                        eachFieldConstraints: BoxConstraints(
                                                                          minWidth: MediaQuery.of(context).size.width * 0.09,
                                                                          maxWidth: MediaQuery.of(context).size.width * 0.09,
                                                                          minHeight: MediaQuery.of(context).size.height * 0.03,
                                                                          maxHeight: Platform.isIOS ? MediaQuery.of(context).size.height * 0.046 : MediaQuery.of(context).size.width * 0.09,
                                                                        ),
                                                                        fieldsCount: 6,
                                                                        onSubmit: (String pin) => _showSnackBar(pin, context),
                                                                        focusNode: _pinPutFocusNode,
                                                                        controller: _pinPutController,
                                                                        submittedFieldDecoration: _pinPutDecoration.copyWith(
                                                                          borderRadius: BorderRadius.circular(10),
                                                                          border: Border.all(
                                                                            color: pinputEditor
                                                                                ? (_errorMessageConfirmCodeEmail != "" ? (_errorMessageConfirmCodeEmail == "Incorrect Code" || _errorMessageConfirmCodeEmail == "Expired Code)" ? Color(0xffF52079) : Color(0xff00CC9E)) : Color(0xFF4048ff))
                                                                                : Colors.grey,
                                                                          ),
                                                                        ),
                                                                        onChanged: (content) async {
                                                                          if (_pinPutController.text.length == 6) {
                                                                            setState(() {
                                                                              pinputEditor = true;
                                                                            });
                                                                          }
                                                                        },
                                                                        selectedFieldDecoration: BoxDecoration(
                                                                          color: Colors.white,
                                                                          borderRadius: BorderRadius.circular(15.0),
                                                                          border: Border.all(
                                                                            color: pinputEditor
                                                                                ? (_errorMessageConfirmCodeEmail != "" ? (_errorMessageConfirmCodeEmail == "Incorrect Code" || _errorMessageConfirmCodeEmail == "Expired Code)" ? Color(0xffF52079) : Color(0xff00CC9E)) : Color(0xFF4048ff))
                                                                                : Colors.grey,
                                                                          ),
                                                                        ),
                                                                        followingFieldDecoration: _pinPutDecoration.copyWith(
                                                                          borderRadius: BorderRadius.circular(5.0),
                                                                          border: Border.all(
                                                                            color: pinputEditor
                                                                                ? (_errorMessageConfirmCodeEmail != "" ? (_errorMessageConfirmCodeEmail == "Incorrect Code" || _errorMessageConfirmCodeEmail == "Expired Code)" ? Color(0xffF52079) : Color(0xff00CC9E)) : Color(0xFF4048ff))
                                                                                : Colors.grey,
                                                                          ),
                                                                        )),
                                                                  ),
                                                                ),
                                                                Padding(
                                                                  padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.01, MediaQuery.of(context).size.height * 0.003, 0, 0),
                                                                  child: (_errorMessageConfirmCodeEmail != "")
                                                                      ? Align(
                                                                          alignment: Alignment.center,
                                                                          child: Text(
                                                                            _errorMessageConfirmCodeEmail,
                                                                            style: GoogleFonts.poppins(color: Colors.red),
                                                                          ),
                                                                        )
                                                                      : null,
                                                                ),
                                                                Padding(
                                                                  padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.width * 0.04),
                                                                  child: Center(
                                                                    child: Container(
                                                                      padding: EdgeInsets.fromLTRB(0, MediaQuery.of(context).size.height * 0.01, 0, 0),
                                                                      width: MediaQuery.of(context).size.width * 0.35,
                                                                      height: MediaQuery.of(context).size.width * 0.15,
                                                                      child: ElevatedButton(
                                                                        style: ButtonStyle(
                                                                          backgroundColor: MaterialStateProperty.all(
                                                                            Color(0xFF00cc9e),
                                                                          ),
                                                                          shape: MaterialStateProperty.all(
                                                                            RoundedRectangleBorder(
                                                                              borderRadius: BorderRadius.circular(30),
                                                                            ),
                                                                          ),
                                                                        ),
                                                                        onPressed: () async {
                                                                          String userToken = Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "";
                                                                          var result = await LoginController.apiService.confirmChangeEmail(_pinPutController.text, userToken);
                                                                          if (result == "email changed") {
                                                                            var index = loginList.indexWhere((e) => e.toLowerCase() == emailController.text.toLowerCase());
                                                                            putLoginData(index, _newEmailController.text, _passwordController.text, box);
                                                                            setState(() {
                                                                              emailController.text = _newEmailController.text;
                                                                              _emailChangedWithSuccess = true;
                                                                              _successUpdateProfile = false;
                                                                              _changeMail = false;
                                                                              _passwordController.text = "";
                                                                              _newEmailController.text = "";
                                                                              _errorUpdateProfile = false;
                                                                              _validatorForms = true;
                                                                              _errorMessageUpdateEmail = "";
                                                                              _changeMailButtonColor = false;
                                                                              _changeMailButtonActive = true;
                                                                              _errorMessageConfirmCodeEmail = "";
                                                                            });
                                                                          } else if (result == "code incorrect") {
                                                                            setState(() {
                                                                              _errorMessageConfirmCodeEmail = "Incorrect Code";
                                                                            });
                                                                          } else if (result == "code expired") {
                                                                            setState(() {
                                                                              _errorMessageConfirmCodeEmail = "Expired Code";
                                                                            });
                                                                          } else {
                                                                            _errorMessageConfirmCodeEmail = result.toString();
                                                                          }
                                                                        },
                                                                        child: Row(
                                                                          children: <Widget>[
                                                                            Spacer(),
                                                                            Text(
                                                                              "Validate",
                                                                              style: GoogleFonts.poppins(fontWeight: FontWeight.w500, fontSize: 18),
                                                                            ),
                                                                            Spacer(),
                                                                          ],
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ),
                                                                ),
                                                              ],
                                                            ),
                                                          ),
                                                        ),
                                                      )
                                                    : (_emailChangedWithSuccess
                                                        ? Padding(
                                                            padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.085),
                                                            child: Align(
                                                              alignment: Alignment.bottomLeft,
                                                              child: Container(
                                                                width: MediaQuery.of(context).size.width * 0.83,
                                                                decoration: BoxDecoration(color: Color(0XFFCCF5EC), borderRadius: BorderRadius.all(Radius.circular(30))),
                                                                child: Padding(
                                                                  padding: EdgeInsets.only(top: MediaQuery.of(context).size.width * 0.025, bottom: MediaQuery.of(context).size.width * 0.025),
                                                                  child: Row(
                                                                    children: <Widget>[
                                                                      Padding(
                                                                        padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.03, 0, MediaQuery.of(context).size.width * 0.03, 0),
                                                                        child: SvgPicture.asset("images/sucess-icon-green.svg"),
                                                                      ),
                                                                      Expanded(
                                                                        child: Text(
                                                                          "YOUR E-MAIL HAS BEEN CHANGED SUCCESSFULLY",
                                                                          style: GoogleFonts.poppins(
                                                                            color: Color(0xFF00CC9E),
                                                                            fontSize: MediaQuery.of(context).size.width * 0.032,
                                                                            fontWeight: FontWeight.w600,
                                                                          ),
                                                                        ),
                                                                      ),
                                                                      Padding(
                                                                        padding: EdgeInsets.fromLTRB(0, 0, MediaQuery.of(context).size.width * 0.05, 0),
                                                                        child: InkWell(
                                                                          onTap: () {
                                                                            setState(() {
                                                                              _emailChangedWithSuccess = false;
                                                                              _pinPutController.text = "";
                                                                              _obscureText = true;
                                                                            });
                                                                          },
                                                                          child: Icon(
                                                                            Icons.close,
                                                                            color: Color(0xFF00CC9E),
                                                                          ),
                                                                        ),
                                                                      ),
                                                                    ],
                                                                  ),
                                                                ),
                                                              ),
                                                            ),
                                                          )
                                                        : null)),
                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.12, MediaQuery.of(context).size.height * 0.035, 0, MediaQuery.of(context).size.height * 0.007),
                                          child: Align(
                                            alignment: Alignment.bottomLeft,
                                            child: Text(
                                              "ADDRESS",
                                              style: GoogleFonts.poppins(color: Color(0xFF75758f), fontSize: MediaQuery.of(context).size.width * 0.032, fontWeight: FontWeight.w600, letterSpacing: 0.5),
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.085),
                                          child: Align(
                                            alignment: Alignment.bottomLeft,
                                            child: Container(
                                              width: MediaQuery.of(context).size.width * 0.83,
                                              child: TextFormField(
                                                controller: addressController,
                                                onChanged: (value) {},
                                                style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.033, fontStyle: FontStyle.normal, fontWeight: FontWeight.normal),
                                                decoration: InputDecoration(
                                                    fillColor: Colors.white,
                                                    focusColor: Colors.white,
                                                    hoverColor: Colors.white,
                                                    filled: true,
                                                    isDense: true,
                                                    hintText: "",
                                                    contentPadding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height * 0.015, horizontal: MediaQuery.of(context).size.width * 0.033),
                                                    enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.grey.withOpacity(0.5))),
                                                    focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.grey.withOpacity(0.5))),
                                                    errorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red)),
                                                    focusedErrorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red))),
                                              ),
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.12, MediaQuery.of(context).size.height * 0.035, 0, MediaQuery.of(context).size.height * 0.007),
                                          child: Align(
                                            alignment: Alignment.bottomLeft,
                                            child: Text(
                                              "ZIP CODE",
                                              style: GoogleFonts.poppins(color: Color(0xFF75758f), fontSize: MediaQuery.of(context).size.width * 0.032, fontWeight: FontWeight.w600, letterSpacing: 0.5),
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.085),
                                          child: Align(
                                            alignment: Alignment.bottomLeft,
                                            child: Container(
                                              width: MediaQuery.of(context).size.width * 0.83,
                                              child: TextFormField(
                                                style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.033, fontStyle: FontStyle.normal, fontWeight: FontWeight.normal),
                                                controller: codeController,
                                                onChanged: (value) {},
                                                decoration: InputDecoration(
                                                    fillColor: Colors.white,
                                                    focusColor: Colors.white,
                                                    hoverColor: Colors.white,
                                                    filled: true,
                                                    isDense: true,
                                                    hintText: "",
                                                    contentPadding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height * 0.015, horizontal: MediaQuery.of(context).size.width * 0.033),
                                                    enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.grey.withOpacity(0.5))),
                                                    focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.grey.withOpacity(0.5))),
                                                    errorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red)),
                                                    focusedErrorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red))),
                                              ),
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.12, MediaQuery.of(context).size.height * 0.035, 0, MediaQuery.of(context).size.height * 0.007),
                                          child: Align(
                                            alignment: Alignment.bottomLeft,
                                            child: Text(
                                              "CITY",
                                              style: GoogleFonts.poppins(color: Color(0xFF75758f), fontSize: MediaQuery.of(context).size.width * 0.032, fontWeight: FontWeight.w600, letterSpacing: 0.5),
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.085),
                                          child: Align(
                                            alignment: Alignment.bottomLeft,
                                            child: Container(
                                              width: MediaQuery.of(context).size.width * 0.83,
                                              child: TextFormField(
                                                style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.033, fontStyle: FontStyle.normal, fontWeight: FontWeight.normal),
                                                controller: cityController,
                                                onChanged: (value) {},
                                                decoration: InputDecoration(
                                                    fillColor: Colors.white,
                                                    focusColor: Colors.white,
                                                    hoverColor: Colors.white,
                                                    filled: true,
                                                    isDense: true,
                                                    hintText: "",
                                                    contentPadding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height * 0.015, horizontal: MediaQuery.of(context).size.width * 0.033),
                                                    enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.grey.withOpacity(0.5))),
                                                    focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.grey.withOpacity(0.5))),
                                                    errorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red)),
                                                    focusedErrorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red))),
                                              ),
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.12, MediaQuery.of(context).size.height * 0.035, 0, MediaQuery.of(context).size.height * 0.007),
                                          child: Align(
                                            alignment: Alignment.bottomLeft,
                                            child: Text(
                                              "COUNTRY",
                                              style: GoogleFonts.poppins(color: Color(0xFF75758f), fontSize: MediaQuery.of(context).size.width * 0.032, fontWeight: FontWeight.w600, letterSpacing: 0.5),
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.085),
                                          child: Align(
                                            alignment: Alignment.bottomLeft,
                                            child: Container(
                                              width: MediaQuery.of(context).size.width * 0.83,
                                              child: InkWell(
                                                onTap: () {
                                                  showCountryPicker(
                                                    context: context,
                                                    showPhoneCode: false,
                                                    onSelect: (Country country) {
                                                      if (country != null) {
                                                        setState(() {
                                                          _selectedCountry = country;
                                                          countryController.text = country.name;
                                                        });
                                                      }
                                                    },
                                                    countryListTheme: CountryListThemeData(
                                                        borderRadius: BorderRadius.only(topLeft: Radius.circular(40.0), topRight: Radius.circular(40.0)),
                                                        inputDecoration:
                                                            InputDecoration(labelText: 'Search', hintText: 'Select country', prefixIcon: const Icon(Icons.search), border: OutlineInputBorder(borderSide: BorderSide(color: const Color(0xFF8C98A8)), borderRadius: BorderRadius.circular(30.0)))),
                                                  );
                                                },
                                                child: IgnorePointer(
                                                  child: TextFormField(
                                                    controller: countryController,
                                                    readOnly: true,
                                                    style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.033, fontStyle: FontStyle.normal, fontWeight: FontWeight.normal),
                                                    decoration: InputDecoration(
                                                        fillColor: Colors.white,
                                                        focusColor: Colors.white,
                                                        hoverColor: Colors.white,
                                                        filled: true,
                                                        isDense: true,
                                                        hintText: "",
                                                        contentPadding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height * 0.015, horizontal: MediaQuery.of(context).size.width * 0.033),
                                                        enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.grey.withOpacity(0.5))),
                                                        focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.grey.withOpacity(0.5))),
                                                        errorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red)),
                                                        focusedErrorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red))),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.12, MediaQuery.of(context).size.height * 0.035, 0, MediaQuery.of(context).size.height * 0.007),
                                          child: Align(
                                            alignment: Alignment.bottomLeft,
                                            child: Text(
                                              "PHONE (OPTIONAL)",
                                              style: GoogleFonts.poppins(color: Color(0xFF75758f), fontSize: MediaQuery.of(context).size.width * 0.032, fontWeight: FontWeight.w600, letterSpacing: 0.5),
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.085),
                                          child: Align(
                                            alignment: Alignment.bottomLeft,
                                            child: Container(
                                              width: MediaQuery.of(context).size.width * 0.83,
                                              child: TextFormField(
                                                controller: phoneController,
                                                onChanged: (value) {},
                                                style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.033, fontStyle: FontStyle.normal, fontWeight: FontWeight.normal),
                                                decoration: InputDecoration(
                                                    fillColor: Colors.white,
                                                    focusColor: Colors.white,
                                                    hoverColor: Colors.white,
                                                    filled: true,
                                                    isDense: true,
                                                    hintText: "",
                                                    contentPadding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height * 0.015, horizontal: MediaQuery.of(context).size.width * 0.033),
                                                    enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.grey.withOpacity(0.5))),
                                                    focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.grey.withOpacity(0.5))),
                                                    errorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red)),
                                                    focusedErrorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red))),
                                              ),
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(top: MediaQuery.of(context).size.width * 0.085, left: MediaQuery.of(context).size.width * 0.085, bottom: MediaQuery.of(context).size.width * 0.075),
                                          child: Container(
                                            height: MediaQuery.of(context).size.width * 0.12,
                                            width: MediaQuery.of(context).size.width * 0.83,
                                            child: ElevatedButton(
                                              style: ButtonStyle(
                                                backgroundColor: MaterialStateProperty.all(
                                                  Color(0xFF4048FF),
                                                ),
                                                shape: MaterialStateProperty.all(
                                                  RoundedRectangleBorder(
                                                    borderRadius: BorderRadius.circular(30),
                                                  ),
                                                ),
                                              ),
                                              child: Row(
                                                children: <Widget>[
                                                  Spacer(),
                                                  Text(
                                                    "Save",
                                                    style: GoogleFonts.poppins(fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.width * 0.042, color: Color(0xFFF6F6FF)),
                                                  ),
                                                  Spacer(),
                                                ],
                                              ),
                                              onPressed: () async {
                                                if (_formKeyProfile.currentState!.validate()) {
                                                  await LoginController.apiService.activateBiometricAuth(Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "", ischecked);
                                                  updateUser();
                                                  Provider.of<WalletController>(context, listen: false).selectedIndex = 2;

                                                  gotoPageGlobal(context, WelcomeScreen());
                                                }
                                              },
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  )
                                ]),
                              ),
                            ),
                          ],
                        ),
                      ),
                      HeaderNavigation(_isVisible, _isWalletVisible, _isNotificationVisible)
                    ],
                  )));
        } else {
          return WillPopScope(
            onWillPop: () {
              setState(() {
                _showDialogOpened = false;
              });
              return Future.value(false);
            },
            child: Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                color: Colors.white,
              ),
              child: Column(
                children: [
                  Padding(
                    padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.02),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Spacer(),
                        Spacer(),
                        Padding(
                          padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.01, top: MediaQuery.of(context).size.width * 0.06),
                          child: SizedBox(
                            child: IconButton(
                              alignment: Alignment.topLeft,
                              onPressed: () {
                                _isNewPictureUpdate
                                    ? setState(() {
                                        _pickedImage = _newPickedImage;
                                        _isNewPictureUpdate = true;
                                      })
                                    : null;

                                setState(() {
                                  _showDialogOpened = false;
                                });

                                return stateInitalisation(context);
                              },
                              splashColor: Colors.transparent,
                              highlightColor: Colors.transparent,
                              icon: Icon(
                                Icons.close,
                                size: MediaQuery.of(context).size.width * 0.05,
                                color: Colors.black.withOpacity(0.5),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.fromLTRB(0, MediaQuery.of(context).size.height * 0.1, 0, MediaQuery.of(context).size.width * 0.05),
                    child: Center(
                      child: Text(
                        "Edit my profile image",
                        style: GoogleFonts.poppins(fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.width * 0.04, color: Color(0xFF1f2337), letterSpacing: 0.7),
                      ),
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                        color: Color.fromRGBO(64, 72, 255, 0.2),
                        border: Border.all(
                          color: Color.fromRGBO(64, 72, 255, 0.2),
                        ),
                        borderRadius: BorderRadius.all(Radius.circular(60))),
                    height: 60,
                    width: MediaQuery.of(context).size.width * 0.8,
                    child: Row(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.037, 0, MediaQuery.of(context).size.width * 0.022, 0),
                          child: SvgPicture.asset(
                            "images/exclam.svg",
                            height: MediaQuery.of(context).size.width * 0.085,
                            width: MediaQuery.of(context).size.width * 0.085,
                          ),
                        ),
                        Text(
                          "ACCEPTED FORMATS: PNG, JPG, GIF.\nMAXIMUM FILE SIZE: 5MB.",
                          style: GoogleFonts.poppins(
                            fontWeight: FontWeight.bold,
                            letterSpacing: 0.7,
                            fontSize: MediaQuery.of(context).size.width * 0.027,
                            color: Color(0xFF4048ff),
                          ),
                        ),
                      ],
                    ),
                  ),
                  _isOversized ? Align(alignment: Alignment.center, child: Text("Can not use this image\nImage Size exceeds 5 MB", style: GoogleFonts.poppins(color: Color(0xFFF3247C), fontWeight: FontWeight.bold), textAlign: TextAlign.center)) : SizedBox.shrink(),
                  Padding(
                    padding: EdgeInsets.only(top: MediaQuery.of(context).size.width * 0.022, bottom: MediaQuery.of(context).size.width * 0.035),
                    child: Container(
                      height: MediaQuery.of(context).size.height * 0.3,
                      width: MediaQuery.of(context).size.width * 0.5,
                      child: Center(
                        child: Padding(
                            padding: EdgeInsets.only(top: MediaQuery.of(context).size.width * 0.015, bottom: MediaQuery.of(context).size.width * 0.015),
                            child: _isNewPictureUpdate
                                ? CustomImageCrop(
                                    cropPercentage: 0.9,
                                    cropController: controller,
                                    image: FileImage(_newPickedImage!),
                                  )
                                : (_userPictureExist
                                    ? CircleAvatar(
                                        radius: 71,
                                        backgroundImage: MemoryImage(userPicture!),
                                      )
                                    : (_userPictureSocialExist
                                        ? CircleAvatar(
                                            radius: 71,
                                            backgroundImage: NetworkImage(userPictureSocial),
                                          )
                                        : CircleAvatar(
                                            radius: 71,
                                            backgroundImage: AssetImage("images/avatar-welcome.png"),
                                          )))),
                      ),
                    ),
                  ),
                  Column(
                    children: [
                      SizedBox(
                        width: MediaQuery.of(context).size.width * 0.6,
                        height: 50,
                        child: ElevatedButton(
                          onPressed: () {
                            setState(() {
                              _isClickedImage = !_isClickedImage;
                            });
                          },
                          style: ElevatedButton.styleFrom(
                            primary: Color(0xFF4048ff),
                            shape: StadiumBorder(),
                          ),
                          child: Padding(
                            padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.035),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                SvgPicture.asset("images/upload-icon.svg"),
                                Text(
                                  "  Choose file",
                                  style: GoogleFonts.poppins(
                                    fontWeight: FontWeight.bold,
                                    fontSize: MediaQuery.of(context).size.width * 0.045,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      Container(
                        child: _isClickedImage
                            ? Padding(
                                padding: EdgeInsets.only(top: MediaQuery.of(context).size.width * 0.032, bottom: MediaQuery.of(context).size.width * 0.027),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Padding(
                                      padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.075),
                                      child: InkWell(
                                          onTap: () async {
                                            if (Platform.isAndroid) {
                                              var status = await Permission.camera.status;
                                              if (status == PermissionStatus.granted) {
                                                _newPickedImage = await _pickImageCamera();
                                                if (_newPickedImage != null) {
                                                  setState(() {
                                                    _isClickedImage = !_isClickedImage;
                                                    _pickedImage = _newPickedImage;
                                                    _isNewPictureUpdate = true;
                                                  });
                                                } else {
                                                  setState(() {
                                                    _isNewPictureUpdate = false;
                                                  });
                                                }
                                              } else {
                                                showDialog(
                                                    context: context,
                                                    builder: (BuildContext context) => CupertinoAlertDialog(
                                                          title: Text('Camera Permission'),
                                                          content: Text('This app needs camera access to take pictures for upload user profile photo'),
                                                          actions: <Widget>[
                                                            CupertinoDialogAction(
                                                              child: Text('Deny'),
                                                              onPressed: () => Navigator.of(context).pop(),
                                                            ),
                                                            CupertinoDialogAction(
                                                              child: Text('Settings'),
                                                              onPressed: () => openAppSettings(),
                                                            ),
                                                          ],
                                                        ));
                                              }
                                            } else {
                                              _newPickedImage = await _pickImageCamera();
                                              if (_newPickedImage != null) {
                                                setState(() {
                                                  _isClickedImage = !_isClickedImage;
                                                  _pickedImage = _newPickedImage;
                                                  _isNewPictureUpdate = true;
                                                });
                                              } else {
                                                setState(() {
                                                  _isNewPictureUpdate = false;
                                                });
                                              }
                                            }
                                          },
                                          splashColor: Colors.purpleAccent,
                                          child: Column(children: [
                                            Icon(
                                              Icons.camera,
                                              color: Color(0xFF4048ff),
                                            ),
                                            Text(
                                              'Camera',
                                              style: GoogleFonts.poppins(
                                                fontSize: MediaQuery.of(context).size.width * 0.033,
                                                fontWeight: FontWeight.w500,
                                                color: Color(0xFF4048ff),
                                              ),
                                            ),
                                          ])),
                                    ),
                                    InkWell(
                                        onTap: () async {
                                          _newPickedImage = await _pickImageGallery();
                                          if (_newPickedImage != null) {
                                            setState(() {
                                              _pickedImage = _newPickedImage;
                                              _isNewPictureUpdate = true;
                                              _isClickedImage = !_isClickedImage;
                                            });
                                          } else {
                                            setState(() {
                                              _isNewPictureUpdate = false;
                                            });
                                          }
                                        },
                                        splashColor: Colors.purpleAccent,
                                        child: Column(children: [
                                          Icon(
                                            Icons.image,
                                            color: Color(0xFF4048ff),
                                          ),
                                          Text(
                                            'Gallery',
                                            style: GoogleFonts.poppins(
                                              fontSize: MediaQuery.of(context).size.width * 0.033,
                                              fontWeight: FontWeight.w500,
                                              color: Color(0xFF4048ff),
                                            ),
                                          ),
                                        ])),
                                  ],
                                ),
                              )
                            : null,
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      SizedBox(
                        height: 50,
                        width: MediaQuery.of(context).size.width * 0.6,
                        child: ElevatedButton(
                          onPressed: _isNewPictureUpdate
                              ? () async {
                                  try {
                                    setState(() {
                                      _isLoadingNewPicture = true;
                                      _isActivatedButtonPicture = false;
                                    });
                                    final image = await controller.onCropImage();
                                    if (_isNewPictureUpdate && _newPickedImage != null) {
                                      _newPickedImage!.writeAsBytesSync(image!.bytes);
                                      var updateResult = await LoginController.apiService.updatePicture(_newPickedImage!.path, Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "");
                                      if (updateResult == "success") {
                                        setState(() {
                                          _successUpdatePicture = true;
                                          Provider.of<LoginController>(context, listen: false).userPicture = _newPickedImage!.readAsBytesSync();
                                          _isLoadingNewPicture = false;
                                          _isActivatedButtonPicture = true;
                                          _userPictureSocialExist = false;
                                          userPictureSocial = "";
                                        });

                                        Navigator.pushReplacement(context, MaterialPageRoute(builder: (_) => ProfilePage()));

                                        Fluttertoast.showToast(msg: "you have successfully updated your picture", backgroundColor: _isNewPictureUpdate ? Color(0xFF00cc9e) : (_isLoadingPicture ? Color(0xFFADADC8) : Color(0xFFADADC8)), gravity: ToastGravity.SNACKBAR);
                                      } else if (updateResult == "over-size") {
                                        Fluttertoast.showToast(msg: "Can not use this image\nImage Size exceeds 5 MB", backgroundColor: Colors.red, gravity: ToastGravity.SNACKBAR, toastLength: Toast.LENGTH_LONG, webPosition: "center");
                                        setState(() {
                                          _successUpdatePicture = false;
                                          _isLoadingNewPicture = false;
                                          _isActivatedButtonPicture = true;
                                        });
                                      } else {
                                        Fluttertoast.showToast(
                                          msg: "Error, something went wrong please try again!",
                                          backgroundColor: Colors.red,
                                          gravity: ToastGravity.SNACKBAR,
                                          toastLength: Toast.LENGTH_LONG,
                                        );
                                        setState(() {
                                          _successUpdatePicture = false;
                                          _isLoadingNewPicture = false;
                                          _isActivatedButtonPicture = true;
                                        });
                                      }
                                    } else {
                                      setState(() {
                                        _isLoadingNewPicture = false;
                                        _isActivatedButtonPicture = true;
                                        _isNewPictureUpdate = false;
                                      });
                                      Navigator.of(context).pop();
                                    }
                                  } catch (e) {
                                    Fluttertoast.showToast(
                                      msg: "Error, something went wrong please try again!",
                                      backgroundColor: Colors.red,
                                      gravity: ToastGravity.SNACKBAR,
                                      toastLength: Toast.LENGTH_LONG,
                                    );
                                    setState(() {
                                      _isLoadingNewPicture = false;
                                      _isActivatedButtonPicture = true;
                                      _isNewPictureUpdate = false;
                                    });
                                    Navigator.pushReplacement(context, MaterialPageRoute(builder: (_) => ProfilePage()));
                                  }
                                }
                              : null,
                          style: ElevatedButton.styleFrom(
                            primary: Color(0xFF00cc9e),
                            shape: StadiumBorder(),
                          ),
                          child: !_isLoadingNewPicture
                              ? Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    SvgPicture.asset("images/save-icon.svg"),
                                    Text(
                                      "  Save picture",
                                      style: GoogleFonts.poppins(
                                        fontWeight: FontWeight.bold,
                                        fontSize: MediaQuery.of(context).size.width * 0.045,
                                      ),
                                    ),
                                  ],
                                )
                              : SizedBox(
                                  height: MediaQuery.of(context).size.width * 0.032,
                                  width: MediaQuery.of(context).size.width * 0.032,
                                  child: CircularProgressIndicator(
                                    color: Colors.grey,
                                  ),
                                ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          );
        }
      } else {
        return Container(
          height: MediaQuery.of(context).size.height * 0.5,
          decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(
                color: Colors.white,
              )),
          child: Center(
              child: CircularProgressIndicator(
            color: Colors.blueAccent,
          )),
        );
      }
    } else {
      return Stack(
        children: [
          Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            color: Colors.white,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Center(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      "No Internet Connection , please check your network",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(color: Colors.black, fontSize: 14),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: ElevatedButton(
                    onPressed: () {},
                    child: Text("Retry"),
                  ),
                ),
              ],
            ),
          )
        ],
      );
    }
  }

  void _showSnackBar(String pin, BuildContext context) {
    final snackBar = SnackBar(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(18.0),
      ),
      duration: Duration(seconds: 5),
      content: Container(
          height: 80.0,
          color: Colors.grey,
          child: Center(
            child: Text(
              'Pin Submitted. Value: $pin',
              style: TextStyle(fontSize: 25.0, backgroundColor: Colors.grey),
            ),
          )),
      backgroundColor: Colors.grey,
    );
  }

  stateInitalisation(BuildContext context) {
    setState(() {
      _isNewPictureUpdate = false;
    });
  }

  String encodeGender(String gender) {
    if (gender == "Man") {
      return 'Profil.campaign_list.genre_masculin';
    } else if (gender == "Woman") {
      return 'Profil.campaign_list.genre_feminin';
    }
    return "Profil.non-binaire";
  }
}