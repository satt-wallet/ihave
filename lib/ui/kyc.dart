import 'dart:convert';
import 'dart:io';
import 'dart:isolate';
import 'dart:typed_data';

import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:ihave/controllers/LoginController.dart';
import 'package:ihave/ui/WelcomeScreen.dart';
import 'package:ihave/ui/settings.dart';
import 'package:ihave/ui/staticElements/headerNavigation.dart';
import 'package:ihave/util/utils.dart';
import 'package:ihave/widgets/alertDialogWidget.dart';
import 'package:ihave/widgets/customAppBar.dart';
import 'package:ihave/widgets/customBottomBar.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';

import '../controllers/walletController.dart';

class KycPage extends StatefulWidget {
  const KycPage({Key? key}) : super(key: key);

  @override
  _KycPageState createState() => _KycPageState();
}

class _KycPageState extends State<KycPage> {
  /** Files picture for identity and address proof */
  File? _pickedIdentityImage;
  File? _pickedProofImage;

  /** Variables for prood ID*/
  bool _pictureUpdated = false;
  bool _loadingIdentitySubmit = false;
  bool _pickedIdentityImageSent = false;
  bool _identityVerificationProgress = false;
  bool _noKYC = true;
  bool _proofIdentityVerfied = false;
  bool _imageKYCIdUploaded = false;
  bool _isLoadingKEYETH = false;

  bool _rejectedKYCId = false;
  bool _rejectedKYCAddress = false;

  /** Variables for prood address */
  bool _loadingAddressSubmit = false;
  bool _pictureUpdatedAddressProof = false;
  bool _pickedAddressImageSent = false;
  bool _addressVerificationProgress = false;
  bool _noKYCAddress = true;
  bool _proofAddressVerfied = false;
  bool _isLoadingPage = true;
  bool _imageKYCAddressUploaded = false;

  /** File KYC **/
  Uint8List? _idKycPicture;
  Uint8List? _addressKycPicture;
  bool _kycIdExist = false;
  bool _kycAddressExist = false;

  bool _isVisible = false;
  bool _isProfileVisible = false;
  bool _isWalletVisible = false;
  bool _isNotificationVisible = false;
  bool _isBigFileSizeProofId = false;
  bool _isBigFileSizeProofAddress = false;

  Future secondElement(List legalList) async {
    print("date time now test kyc 2nd element ${DateTime.now()}");
    switch (legalList[1]["type"]) {
      case "proofDomicile":
        var result = await LoginController.apiService.getUserLegalPicture(Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "", legalList[1]["_id"]);
        if (result.runtimeType != String) {
          setState(() {
            _kycAddressExist = true;
            _addressKycPicture = result;
          });
        }
        switch (legalList[1]["validate"]) {
          case true:
            setState(() {
              _imageKYCAddressUploaded = true;
              _noKYCAddress = false;
              _addressVerificationProgress = false;
              _proofAddressVerfied = true;
            });
            break;
          case "reject":
            setState(() {
              _noKYCAddress = true;
              _imageKYCAddressUploaded = false;
              _kycAddressExist = false;
              _pictureUpdatedAddressProof = false;
              _noKYCAddress = true;
              _rejectedKYCAddress = true;
            });
            break;
          case false:
            setState(() {
              _imageKYCAddressUploaded = true;
              _noKYCAddress = false;
              _addressVerificationProgress = true;
              _proofAddressVerfied = false;
            });
            break;
        }
        break;
      case "proofId":
        var result = await LoginController.apiService.getUserLegalPicture(Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "", legalList[1]["_id"]);
        setState(() {
          _kycIdExist = true;
          _idKycPicture = result;
        });
        switch (legalList[1]["validate"]) {
          case true:
            setState(() {
              _imageKYCIdUploaded = true;
              _noKYC = false;
              _identityVerificationProgress = false;
              _proofIdentityVerfied = true;
            });
            break;
          case "reject":
            setState(() {
              _noKYC = true;
              _imageKYCIdUploaded = false;
              _pictureUpdated = false;
              _kycIdExist = false;
              _rejectedKYCId = true;
            });
            break;
          case false:
            setState(() {
              _imageKYCIdUploaded = true;
              _noKYC = false;
              _identityVerificationProgress = true;
              _proofIdentityVerfied = false;
            });
            break;
        }
        break;
    }
    print("date time now test kyc 2nd element ${DateTime.now()}");
    return "success";
  }

  Future firstElement(List legalList) async {
    print("date time now test kyc 1st element ${DateTime.now()}");
    switch (legalList[0]["type"]) {
      case "proofDomicile":
        var result = await LoginController.apiService.getUserLegalPicture(Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "", legalList[0]["_id"]);
        if (result.runtimeType != String) {
          setState(() {
            _kycAddressExist = true;
            _addressKycPicture = result;
          });
        }
        switch (legalList[0]["validate"]) {
          case true:
            setState(() {
              _imageKYCAddressUploaded = true;
              _noKYCAddress = false;
              _addressVerificationProgress = false;
              _proofAddressVerfied = true;
            });
            break;
          case "reject":
            setState(() {
              _noKYCAddress = true;
              _imageKYCAddressUploaded = false;
              _kycAddressExist = false;
              _pictureUpdatedAddressProof = false;
              _noKYCAddress = true;
              _rejectedKYCAddress = true;
            });
            break;
          case false:
            setState(() {
              _imageKYCAddressUploaded = true;
              _noKYCAddress = false;
              _addressVerificationProgress = true;
              _proofAddressVerfied = false;
            });
            break;
        }
        break;
      case "proofId":
        var result = await LoginController.apiService.getUserLegalPicture(Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "", legalList[0]["_id"]);
        setState(() {
          _kycIdExist = true;
          _idKycPicture = result;
        });
        switch (legalList[0]["validate"]) {
          case true:
            setState(() {
              _imageKYCIdUploaded = true;
              _noKYC = false;
              _identityVerificationProgress = false;
              _proofIdentityVerfied = true;
            });
            break;
          case "reject":
            setState(() {
              _noKYC = true;
              _imageKYCIdUploaded = false;
              _pictureUpdated = false;
              _kycIdExist = false;
              _rejectedKYCId = true;
            });
            break;
          case false:
            setState(() {
              _imageKYCIdUploaded = true;
              _noKYC = false;
              _identityVerificationProgress = true;
              _proofIdentityVerfied = false;
            });
            break;
        }
        break;
    }
    return "success";
  }

  _getUserLegal() async {
    List userLegalResult = await LoginController.apiService.getUserLegal(Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "");
    if (userLegalResult.length == 0) {
      setState(() {
        _noKYCAddress = true;
        _noKYC = true;
        _identityVerificationProgress = false;
        _loadingIdentitySubmit = false;
        _pictureUpdated = false;
        _pickedIdentityImageSent = false;
        _loadingAddressSubmit = false;
        _pictureUpdatedAddressProof = false;
        _pickedAddressImageSent = false;
        _addressVerificationProgress = false;
        _imageKYCAddressUploaded = false;
        _imageKYCIdUploaded = false;
      });
    }
    if (userLegalResult.length == 1) {
      var result = await LoginController.apiService.getUserLegalPicture(Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "", userLegalResult[0]["_id"]);
      if (userLegalResult[0]["type"] == "proofDomicile") {
        if (result.runtimeType != String) {
          if (result.runtimeType != String) {
            setState(() {
              _kycAddressExist = true;
              _addressKycPicture = result;
            });
          }
        }
        if (userLegalResult[0]["validate"] == true) {
          setState(() {
            _imageKYCAddressUploaded = true;
            _noKYCAddress = false;
            _addressVerificationProgress = false;
            _proofAddressVerfied = true;
            _rejectedKYCAddress = false;
          });
        } else if (userLegalResult[0]["validate"].toString() == "reject") {
          setState(() {
            _noKYCAddress = true;
            _imageKYCAddressUploaded = false;
            _kycAddressExist = false;
            _pictureUpdatedAddressProof = false;
            _noKYCAddress = true;
            _rejectedKYCAddress = true;
          });
        } else {
          setState(() {
            _imageKYCAddressUploaded = true;
            _noKYCAddress = false;
            _addressVerificationProgress = true;
            _proofAddressVerfied = false;
            _rejectedKYCAddress = false;
          });
        }
      } else {
        setState(() {
          _kycIdExist = true;
          _idKycPicture = result;
        });
        if (userLegalResult[0]["validate"] == true) {
          setState(() {
            _imageKYCIdUploaded = true;
            _noKYC = false;
            _identityVerificationProgress = false;
            _proofIdentityVerfied = true;
          });
        } else if (userLegalResult[0]["validate"] == "reject") {
          setState(() {
            _noKYC = true;
            _imageKYCIdUploaded = false;
            _pictureUpdated = false;
            _kycIdExist = false;
            _rejectedKYCId = true;
          });
        } else {
          setState(() {
            _imageKYCIdUploaded = true;
            _noKYC = false;
            _identityVerificationProgress = true;
            _proofIdentityVerfied = false;
          });
        }
      }
    }

    if (userLegalResult.length == 2) {
      setState(() {
        _isLoadingPage = true;
      });
      print("Test value of is loading $_isLoadingPage");

      await Future.wait([
        firstElement(userLegalResult),
        secondElement(userLegalResult),
      ]).then((value) {}).catchError((error) {
        Provider.of<WalletController>(context, listen: false).selectedIndex = 2;
        gotoPageGlobal(context, WelcomeScreen());
      });
    }
    setState(() {
      _isLoadingPage = false;
    });
    return userLegalResult;
  }

  _pickImageGallery() async {
    final picker = ImagePicker();
    final pickedImage = await picker.getImage(source: ImageSource.gallery);
    if (pickedImage != null) {
      final pickedImageFile = File(pickedImage.path);
      print("Test size file gallery ${(pickedImageFile.readAsBytesSync().lengthInBytes / 1024) / 1024}");
      return pickedImageFile;
    }
    return "error";
  }

  _pickImageCamera() async {
    final picker = ImagePicker();
    final pickedImage = await picker.getImage(source: ImageSource.camera);
    if (pickedImage != null) {
      final pickedImageFile = File(pickedImage.path);
      return pickedImageFile;
    }
    return "error";
  }

  @override
  void initState() {
    super.initState();
    print(Provider.of<LoginController>(context, listen: false).userDetails?.userToken);
    _getUserLegal();

    _isProfileVisible = !_isProfileVisible;
    _isWalletVisible = !_isWalletVisible;
    _isNotificationVisible = !_isNotificationVisible;
    _pictureUpdated = false;
  }

  @override
  Widget build(BuildContext context) {
    if (_isLoadingPage) {
      return Container(
        height: MediaQuery.of(context).size.height * 0.5,
        decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(
              color: Colors.white,
            )),
        child: Center(
            child: CircularProgressIndicator(
          color: Colors.blueAccent,
        )),
      );
    } else {
      setState(() {
        _isVisible = Provider.of<WalletController>(context, listen: true).isSideMenuVisible;
        _isProfileVisible = Provider.of<WalletController>(context, listen: true).isProfileVisible;
        _isWalletVisible = Provider.of<WalletController>(context, listen: true).isWalletVisible;
        _isNotificationVisible = Provider.of<WalletController>(context, listen: true).isNotificationVisible;
      });
      return Scaffold(
        appBar: CustomAppBar(),
        bottomNavigationBar: CustomBottomBar(),
        body: WillPopScope(
          onWillPop: () {
            return Future.value(true);
          },
          child: Container(
            child: Stack(
              children: [
                SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      SizedBox(
                        height: MediaQuery.of(context).size.height * 0.02,
                      ),
                      /** Back to  Welcome Screen button */
                      if (Platform.isIOS)
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            SizedBox(
                              width: MediaQuery.of(context).size.width * 0.03,
                            ),
                            Text(
                              "KYC",
                              style: GoogleFonts.poppins(fontWeight: FontWeight.w700, fontStyle: FontStyle.normal, fontSize: MediaQuery.of(context).size.height * 0.03, color: Color(0xFF00041B)),
                            ),
                            const Spacer(),
                            InkWell(
                              onTap: () {
                                Navigator.of(context).push(new MaterialPageRoute(builder: (context) => Setting()));
                              },
                              child: Text(
                                "< Back",
                                style: GoogleFonts.poppins(fontWeight: FontWeight.w600, fontStyle: FontStyle.normal, fontSize: MediaQuery.of(context).size.height * 0.02, color: Color(0xFF00041B)),
                              ),
                            ),
                            SizedBox(
                              width: MediaQuery.of(context).size.width * 0.05,
                            ),
                          ],
                        ),
                      if (Platform.isAndroid)
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            SizedBox(
                              width: MediaQuery.of(context).size.width * 0.03,
                            ),
                            Text(
                              "KYC",
                              style: GoogleFonts.poppins(fontWeight: FontWeight.w700, fontStyle: FontStyle.normal, fontSize: MediaQuery.of(context).size.height * 0.03, color: Color(0xFF00041B)),
                            ),
                          ],
                        ),
                      SizedBox(
                        height: MediaQuery.of(context).size.height * 0.02,
                      ),
                      /** Description of KYC */
                      Container(
                        decoration: BoxDecoration(
                          color: const Color(0XFFF6F6FF),
                        ),
                        child: Padding(
                          padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.02, bottom: MediaQuery.of(context).size.height * 0.02),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Padding(
                                padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.05, right: MediaQuery.of(context).size.width * 0.05),
                                child: SvgPicture.asset(
                                  "images/exclam.svg",
                                  height: MediaQuery.of(context).size.height * 0.03,
                                  width: MediaQuery.of(context).size.height * 0.03,
                                ),
                              ),
                              Expanded(
                                child: Padding(
                                  padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.06),
                                  child: Text(
                                    "IN ACCORDANCE WITH THE LEGISLATION IN FORCE, THANK YOU FOR PROVIDING THE NECESSARY ELEMENTS TO VALIDATE YOUR ACCOUNT",
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(fontWeight: FontWeight.w600, fontStyle: FontStyle.normal, letterSpacing: 0.7, color: Color(0XFF4048FF), fontSize: MediaQuery.of(context).size.height * 0.0135),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                      SizedBox(
                        height: MediaQuery.of(context).size.height * 0.02,
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.03),
                        child: Align(
                          alignment: Alignment.topLeft,
                          child: Text(
                            "Identity verification",
                            style: GoogleFonts.poppins(fontWeight: FontWeight.w700, fontStyle: FontStyle.normal, fontSize: MediaQuery.of(context).size.height * 0.03, color: Color(0xFF00041B)),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: MediaQuery.of(context).size.height * 0.025,
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.03, right: MediaQuery.of(context).size.width * 0.15),
                        child: Text(
                          "Upload an ID with information corresponding to those entered in the general information part.",
                          style: GoogleFonts.poppins(fontWeight: FontWeight.w400, fontStyle: FontStyle.normal, height: 1.2, fontSize: MediaQuery.of(context).size.height * 0.019, color: Color(0xFF00041B)),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.03),
                        child: Text(
                          "Passport or ID card accepted.",
                          style: GoogleFonts.poppins(fontWeight: FontWeight.bold, fontStyle: FontStyle.normal, height: 1.2, color: Color(0xFF00041B), fontSize: MediaQuery.of(context).size.height * 0.019),
                        ),
                      ),
                      SizedBox(
                        height: MediaQuery.of(context).size.height * 0.03,
                      ),
                      Padding(
                        padding: EdgeInsets.all(0),
                        child: !_pictureUpdated && !_kycIdExist
                            ? Row(
                                children: <Widget>[
                                  const Spacer(),
                                  Container(
                                    decoration: BoxDecoration(
                                      color: const Color(0XFFF6F6FF),
                                      borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.05),
                                    ),
                                    child: Padding(
                                      padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.025, bottom: MediaQuery.of(context).size.height * 0.025, left: MediaQuery.of(context).size.width * 0.05, right: MediaQuery.of(context).size.width * 0.05),
                                      child: Column(
                                        children: <Widget>[
                                          InkWell(
                                            onTap: () async {
                                              var resultPicture = await _pickImageCamera();
                                              if (resultPicture != "error") {
                                                File pictureProofId = resultPicture;
                                                var test = (pictureProofId.readAsBytesSync().lengthInBytes / 1024) / 1024;
                                                print("Test file size $test MB");
                                                if (test > 5) {
                                                  setState(() {
                                                    _isBigFileSizeProofId = true;
                                                  });
                                                } else {
                                                  setState(() {
                                                    _pickedIdentityImage = resultPicture;
                                                    _pictureUpdated = true;
                                                    _rejectedKYCId = false;
                                                    _isBigFileSizeProofId = false;
                                                  });
                                                }
                                                /** Navigator pop --> SUCCESS CASE */

                                              } else {
                                                print("error");
                                                setState(() {
                                                  _pictureUpdated = false;
                                                  _isBigFileSizeProofId = false;
                                                });
                                              }
                                            },
                                            child: Icon(
                                              Icons.camera,
                                              color: const Color(0XFF4048FF),
                                              size: MediaQuery.of(context).size.height * 0.06,
                                            ),
                                          ),
                                          SizedBox(
                                            height: MediaQuery.of(context).size.height * 0.015,
                                          ),
                                          Text(
                                            "Scan your ID",
                                            style: GoogleFonts.poppins(fontWeight: FontWeight.w600, fontStyle: FontStyle.normal, fontSize: MediaQuery.of(context).size.height * 0.018, color: Color(0XFF4048FF)),
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                  const Spacer(),
                                  Container(
                                    decoration: BoxDecoration(
                                      color: const Color(0XFFF6F6FF),
                                      borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.05),
                                    ),
                                    child: Padding(
                                      padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.025, bottom: MediaQuery.of(context).size.height * 0.025, left: MediaQuery.of(context).size.width * 0.05, right: MediaQuery.of(context).size.width * 0.05),
                                      child: Column(
                                        children: <Widget>[
                                          InkWell(
                                            onTap: () async {
                                              var resultPicture = await _pickImageGallery();
                                              print("test result $resultPicture");
                                              if (resultPicture != "error") {
                                                File pictureProofId = resultPicture;
                                                var test = (pictureProofId.readAsBytesSync().lengthInBytes / 1024) / 1024;
                                                print("file size $test");
                                                if (test > 5) {
                                                  setState(() {
                                                    _isBigFileSizeProofId = true;
                                                  });
                                                } else {
                                                  setState(() {
                                                    _pickedIdentityImage = resultPicture;
                                                    _pictureUpdated = true;
                                                    _rejectedKYCId = false;
                                                    _isBigFileSizeProofId = false;
                                                  });
                                                }
                                              } else {
                                                print("error");
                                                setState(() {
                                                  _pictureUpdated = false;
                                                  _isBigFileSizeProofId = false;
                                                });
                                              }
                                            },
                                            child: Icon(
                                              Icons.image,
                                              color: const Color(0XFF4048FF),
                                              size: MediaQuery.of(context).size.height * 0.06,
                                            ),
                                          ),
                                          SizedBox(
                                            height: MediaQuery.of(context).size.height * 0.015,
                                          ),
                                          Text(
                                            "Browse files",
                                            style: GoogleFonts.poppins(fontWeight: FontWeight.w600, fontStyle: FontStyle.normal, fontSize: MediaQuery.of(context).size.height * 0.018, color: const Color(0XFF4048FF)),
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                  const Spacer(),
                                ],
                              )
                            : (_kycIdExist
                                ? Center(child: getKycImage(_idKycPicture!))
                                : Center(
                                    child: Image.file(
                                      _pickedIdentityImage!,
                                      height: MediaQuery.of(context).size.height * 0.35,
                                    ),
                                  )),
                      ),
                      SizedBox(
                        height: MediaQuery.of(context).size.height * 0.024,
                      ),
                      if (_isBigFileSizeProofId)
                        Padding(
                          padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.013),
                          child: Align(
                            alignment: Alignment.topCenter,
                            child: Container(
                              width: MediaQuery.of(context).size.width * 0.9,
                              decoration: BoxDecoration(borderRadius: BorderRadius.circular(30), color: Color(0XFFf52079).withOpacity(0.2)),
                              child: Padding(
                                padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.04, top: MediaQuery.of(context).size.height * 0.01, bottom: MediaQuery.of(context).size.height * 0.01),
                                child: Row(
                                  children: [
                                    SvgPicture.asset(
                                      "images/false-icon.svg",
                                    ),
                                    Padding(
                                      padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.03),
                                      child: Text(
                                        "Non conforming image",
                                        style: GoogleFonts.poppins(fontWeight: FontWeight.w600, fontSize: MediaQuery.of(context).size.height * 0.015, color: Color(0XFFf52079)),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                      if (_rejectedKYCId)
                        Padding(
                          padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.013),
                          child: Align(
                            alignment: Alignment.topCenter,
                            child: Container(
                              width: MediaQuery.of(context).size.width * 0.9,
                              decoration: BoxDecoration(borderRadius: BorderRadius.circular(30), color: Color(0XFFf52079).withOpacity(0.2)),
                              child: Padding(
                                padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.04, top: MediaQuery.of(context).size.height * 0.01, bottom: MediaQuery.of(context).size.height * 0.01),
                                child: Row(
                                  children: [
                                    SvgPicture.asset(
                                      "images/false-icon.svg",
                                    ),
                                    Padding(
                                      padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.05),
                                      child: Text(
                                        "Your file has been rejected",
                                        style: GoogleFonts.poppins(fontWeight: FontWeight.w600, fontSize: MediaQuery.of(context).size.height * 0.015, color: Color(0XFFf52079)),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                      Padding(
                        padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.02),
                        child: Align(
                          alignment: Alignment.topCenter,
                          child: _pictureUpdated && _noKYC
                              ? SizedBox(
                                  height: MediaQuery.of(context).size.height * 0.065,
                                  width: MediaQuery.of(context).size.width * 0.9,
                                  child: ElevatedButton(
                                    onPressed: _pictureUpdated
                                        ? () async {
                                            setState(() {
                                              _loadingIdentitySubmit = true;
                                            });

                                            var result = await LoginController.apiService.addUserLegal(Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "", "proofId", _pickedIdentityImage!);
                                            print("Test kycccccccc $result");
                                            if (result != "error") {
                                              setState(() {
                                                _loadingIdentitySubmit = false;
                                                _identityVerificationProgress = true;
                                                _noKYC = false;
                                              });
                                            } else {
                                              displayDialog(context, "Error", "Something went wrong, please try again!");
                                              setState(() {
                                                _loadingIdentitySubmit = false;
                                                _identityVerificationProgress = false;
                                                _noKYC = true;
                                              });
                                            }
                                          }
                                        : null,
                                    style: ButtonStyle(
                                      backgroundColor: MaterialStateProperty.all(
                                        !_loadingIdentitySubmit ? Color(0xFF4048FF) : Color(0XFFD6D6E8),
                                      ),
                                      shape: MaterialStateProperty.all(
                                        RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.08),
                                        ),
                                      ),
                                    ),
                                    child: !_loadingIdentitySubmit
                                        ? Text(
                                            "Submit",
                                            style: GoogleFonts.poppins(fontWeight: FontWeight.w600, fontStyle: FontStyle.normal, fontSize: MediaQuery.of(context).size.height * 0.021, color: Colors.white, letterSpacing: 0.7),
                                          )
                                        : SizedBox(
                                            height: MediaQuery.of(context).size.height * 0.02,
                                            width: MediaQuery.of(context).size.height * 0.02,
                                            child: CircularProgressIndicator(
                                              color: Colors.white,
                                            )),
                                  ),
                                )
                              : (_identityVerificationProgress
                                  ? Container(
                                      width: MediaQuery.of(context).size.width * 0.9,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(30),
                                        color: Color(0XFFFFFAEA),
                                      ),
                                      child: Padding(
                                        padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.04, top: MediaQuery.of(context).size.height * 0.01, bottom: MediaQuery.of(context).size.height * 0.01),
                                        child: Row(
                                          children: [
                                            SvgPicture.asset(
                                              "images/exclam.svg",
                                              color: Color(0XFFFFA318),
                                            ),
                                            Padding(
                                              padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.02),
                                              child: Text(
                                                "Waiting for validation",
                                                style: GoogleFonts.poppins(fontWeight: FontWeight.w600, color: Color(0XFFFFA318)),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    )
                                  : (_proofIdentityVerfied
                                      ? Row(
                                          children: [
                                            Spacer(),
                                            Container(
                                              width: MediaQuery.of(context).size.width * 0.9,
                                              decoration: BoxDecoration(borderRadius: BorderRadius.circular(30), color: Color(0XFFCCF5EC).withOpacity(0.5)),
                                              child: Padding(
                                                padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.04, top: MediaQuery.of(context).size.height * 0.01, bottom: MediaQuery.of(context).size.height * 0.01),
                                                child: Row(
                                                  children: [
                                                    SvgPicture.asset("images/sucess-icon-green.svg"),
                                                    Padding(
                                                      padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.04),
                                                      child: Text(
                                                        "Identity verified successfully !",
                                                        style: GoogleFonts.poppins(
                                                          color: Color(0XFF00CC9E),
                                                          fontWeight: FontWeight.w600,
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                            Spacer(),
                                          ],
                                        )
                                      : null)),
                        ),
                      ),

                      /** Dive to seprate ID and Address proof */
                      Padding(
                        padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.035, right: MediaQuery.of(context).size.width * 0.1, left: MediaQuery.of(context).size.width * 0.1),
                        child: Divider(
                          height: 5,
                          thickness: 1,
                        ),
                      ),
                      SizedBox(height: MediaQuery.of(context).size.height * 0.015),
                      Padding(
                        padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.03),
                        child: Align(
                          alignment: Alignment.topLeft,
                          child: Text(
                            "Proof of address",
                            style: GoogleFonts.poppins(fontWeight: FontWeight.w700, fontStyle: FontStyle.normal, color: Color(0xFF00041B), fontSize: MediaQuery.of(context).size.height * 0.03),
                          ),
                        ),
                      ),
                      SizedBox(height: MediaQuery.of(context).size.height * 0.015),
                      Padding(
                        padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.03, right: MediaQuery.of(context).size.width * 0.15),
                        child: Text(
                          "Upload a proof of address which is less than 3 months old (utility bills, property tax receipt, valid driver’s licence, bank statement).",
                          style: GoogleFonts.poppins(color: Color(0xFF00041B), fontWeight: FontWeight.w400, fontStyle: FontStyle.normal, height: 1.2, fontSize: MediaQuery.of(context).size.height * 0.019),
                        ),
                      ),
                      SizedBox(
                        height: MediaQuery.of(context).size.height * 0.03,
                      ),
                      Padding(
                        padding: EdgeInsets.all(0),
                        child: !_pictureUpdatedAddressProof && !_kycAddressExist
                            ? Row(
                                children: <Widget>[
                                  const Spacer(),
                                  Container(
                                    decoration: BoxDecoration(
                                      color: const Color(0XFFF6F6FF),
                                      borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.05),
                                    ),
                                    child: Padding(
                                      padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.025, bottom: MediaQuery.of(context).size.height * 0.025, left: MediaQuery.of(context).size.width * 0.05, right: MediaQuery.of(context).size.width * 0.05),
                                      child: Column(
                                        children: <Widget>[
                                          InkWell(
                                            onTap: () async {
                                              var resultPicture = await _pickImageCamera();
                                              print("proff adress $resultPicture");
                                              if (resultPicture != "error") {
                                                File pictureProofId = resultPicture;
                                                var test = (pictureProofId.readAsBytesSync().lengthInBytes / 1024) / 1024;
                                                print("Test file size $test MB");
                                                if (test > 5) {
                                                  setState(() {
                                                    _isBigFileSizeProofAddress = true;
                                                  });
                                                } else {
                                                  setState(() {
                                                    _pickedProofImage = resultPicture;
                                                    _pictureUpdatedAddressProof = true;
                                                    _rejectedKYCAddress = false;
                                                    _isBigFileSizeProofAddress = false;
                                                  });
                                                }
                                              } else {
                                                print("error");
                                                setState(() {
                                                  _pictureUpdatedAddressProof = false;
                                                  _isBigFileSizeProofAddress = false;
                                                });
                                              }
                                            },
                                            child: Icon(
                                              Icons.camera,
                                              color: const Color(0XFF4048FF),
                                              size: MediaQuery.of(context).size.height * 0.06,
                                            ),
                                          ),
                                          SizedBox(
                                            height: MediaQuery.of(context).size.height * 0.015,
                                          ),
                                          Text(
                                            "Scan your ID",
                                            style: GoogleFonts.poppins(fontWeight: FontWeight.w600, fontStyle: FontStyle.normal, fontSize: MediaQuery.of(context).size.height * 0.018, color: const Color(0XFF4048FF)),
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                  const Spacer(),
                                  Container(
                                    decoration: BoxDecoration(
                                      color: const Color(0XFFF6F6FF),
                                      borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.05),
                                    ),
                                    child: Padding(
                                      padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.025, bottom: MediaQuery.of(context).size.height * 0.025, left: MediaQuery.of(context).size.width * 0.05, right: MediaQuery.of(context).size.width * 0.05),
                                      child: Column(
                                        children: <Widget>[
                                          InkWell(
                                            onTap: () async {
                                              var resultPicture = await _pickImageGallery();
                                              if (resultPicture != "error") {
                                                File pictureProofId = resultPicture;
                                                var test = (pictureProofId.readAsBytesSync().lengthInBytes / 1024) / 1024;
                                                print("Test file size $test MB");
                                                if (test > 5) {
                                                  setState(() {
                                                    _isBigFileSizeProofAddress = true;
                                                  });
                                                } else {
                                                  setState(() {
                                                    _pickedProofImage = resultPicture;
                                                    _pictureUpdatedAddressProof = true;
                                                    _rejectedKYCAddress = false;
                                                    _isBigFileSizeProofAddress = false;
                                                  });
                                                }
                                              } else {
                                                print("error");
                                                setState(() {
                                                  _pictureUpdatedAddressProof = false;
                                                  _isBigFileSizeProofAddress = false;
                                                });
                                              }
                                            },
                                            child: Icon(
                                              Icons.image,
                                              color: const Color(0XFF4048FF),
                                              size: MediaQuery.of(context).size.height * 0.06,
                                            ),
                                          ),
                                          SizedBox(
                                            height: MediaQuery.of(context).size.height * 0.015,
                                          ),
                                          Text(
                                            "Browse files",
                                            style: GoogleFonts.poppins(fontWeight: FontWeight.w600, fontStyle: FontStyle.normal, fontSize: MediaQuery.of(context).size.height * 0.018, color: const Color(0XFF4048FF)),
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                  const Spacer(),
                                ],
                              )
                            : (_kycAddressExist
                                ? Center(child: getKycImage(_addressKycPicture!))
                                : Center(
                                    child: Image.file(
                                    _pickedProofImage!,
                                    height: MediaQuery.of(context).size.height * 0.35,
                                  ))),
                      ),
                      if (_isBigFileSizeProofAddress)
                        Padding(
                          padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.013),
                          child: Align(
                            alignment: Alignment.topCenter,
                            child: Container(
                              width: MediaQuery.of(context).size.width * 0.9,
                              decoration: BoxDecoration(borderRadius: BorderRadius.circular(30), color: Color(0XFFf52079).withOpacity(0.2)),
                              child: Padding(
                                padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.04, top: MediaQuery.of(context).size.height * 0.01, bottom: MediaQuery.of(context).size.height * 0.01),
                                child: Row(
                                  children: [
                                    SvgPicture.asset(
                                      "images/false-icon.svg",
                                    ),
                                    Padding(
                                      padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.03),
                                      child: Text(
                                        "Non conforming image",
                                        style: GoogleFonts.poppins(fontWeight: FontWeight.w600, fontSize: MediaQuery.of(context).size.height * 0.015, color: Color(0XFFf52079)),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                      if (_rejectedKYCAddress)
                        Padding(
                          padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.013),
                          child: Align(
                            alignment: Alignment.topCenter,
                            child: Container(
                              width: MediaQuery.of(context).size.width * 0.9,
                              decoration: BoxDecoration(borderRadius: BorderRadius.circular(30), color: Color(0XFFf52079).withOpacity(0.2)),
                              child: Padding(
                                padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.04, top: MediaQuery.of(context).size.height * 0.01, bottom: MediaQuery.of(context).size.height * 0.01),
                                child: Row(
                                  children: [
                                    SvgPicture.asset(
                                      "images/false-icon.svg",
                                    ),
                                    Padding(
                                      padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.05),
                                      child: Text(
                                        "Your file has been rejected",
                                        style: GoogleFonts.poppins(fontWeight: FontWeight.w600, fontSize: MediaQuery.of(context).size.height * 0.015, color: Color(0XFFf52079)),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                      Padding(
                        padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.05, bottom: MediaQuery.of(context).size.height * 0.1),
                        child: _noKYCAddress && _pictureUpdatedAddressProof
                            ? Align(
                                alignment: Alignment.topCenter,
                                child: SizedBox(
                                  height: MediaQuery.of(context).size.height * 0.065,
                                  width: MediaQuery.of(context).size.width * 0.9,
                                  child: ElevatedButton(
                                    onPressed: _pictureUpdatedAddressProof
                                        ? () async {
                                            setState(() {
                                              _loadingAddressSubmit = true;
                                            });
                                            var result = await LoginController.apiService.addUserLegal(Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "", "proofDomicile", _pickedProofImage!);
                                            print("dgqu $result");
                                            if (result != "error") {
                                              setState(() {
                                                _loadingAddressSubmit = false;
                                                _addressVerificationProgress = true;
                                                _noKYCAddress = false;
                                              });
                                            } else {
                                              displayDialog(context, "Error", "Something went wrong, please try again!");
                                              setState(() {
                                                _loadingAddressSubmit = false;
                                                _addressVerificationProgress = false;
                                                _noKYCAddress = true;
                                              });
                                            }
                                          }
                                        : null,
                                    style: ButtonStyle(
                                      backgroundColor: MaterialStateProperty.all(
                                        !_loadingAddressSubmit ? Color(0xFF4048FF) : Color(0XFFD6D6E8),
                                      ),
                                      shape: MaterialStateProperty.all(
                                        RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.08),
                                        ),
                                      ),
                                    ),
                                    child: !_loadingAddressSubmit
                                        ? Text(
                                            "Submit",
                                            style: GoogleFonts.poppins(fontWeight: FontWeight.w600, fontStyle: FontStyle.normal, fontSize: MediaQuery.of(context).size.height * 0.021, color: Colors.white, letterSpacing: 0.7),
                                          )
                                        : SizedBox(
                                            height: MediaQuery.of(context).size.height * 0.02,
                                            width: MediaQuery.of(context).size.height * 0.02,
                                            child: CircularProgressIndicator(
                                              color: Colors.white,
                                            ),
                                          ),
                                  ),
                                ),
                              )
                            : (_addressVerificationProgress
                                ? Padding(
                                    padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.015),
                                    child: Row(
                                      children: [
                                        const Spacer(),
                                        Container(
                                          width: MediaQuery.of(context).size.width * 0.9,
                                          decoration: BoxDecoration(borderRadius: BorderRadius.circular(30), color: Color(0XFFFFFAEA)),
                                          child: Padding(
                                            padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.04, top: MediaQuery.of(context).size.height * 0.01, bottom: MediaQuery.of(context).size.height * 0.01),
                                            child: Row(
                                              children: [
                                                SvgPicture.asset(
                                                  "images/exclam.svg",
                                                  color: Color(0XFFFFA318),
                                                ),
                                                Padding(
                                                  padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.02),
                                                  child: Text(
                                                    "Waiting for validation",
                                                    style: GoogleFonts.poppins(fontWeight: FontWeight.w600, color: Color(0XFFFFA318)),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                        const Spacer(),
                                      ],
                                    ),
                                  )
                                : (_proofAddressVerfied
                                    ? Row(
                                        children: [
                                          Spacer(),
                                          Container(
                                            width: MediaQuery.of(context).size.width * 0.9,
                                            decoration: BoxDecoration(borderRadius: BorderRadius.circular(30), color: Color(0XFFCCF5EC).withOpacity(0.5)),
                                            child: Padding(
                                              padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.04, top: MediaQuery.of(context).size.height * 0.01, bottom: MediaQuery.of(context).size.height * 0.01),
                                              child: Row(
                                                children: [
                                                  SvgPicture.asset("images/sucess-icon-green.svg"),
                                                  Padding(
                                                    padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.04),
                                                    child: Text(
                                                      "Identity verified successfully!",
                                                      style: GoogleFonts.poppins(
                                                        color: Color(0XFF00CC9E),
                                                        fontWeight: FontWeight.w600,
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                          Spacer(),
                                        ],
                                      )
                                    : null)),
                      ),
                    ],
                  ),
                ),
                HeaderNavigation(_isVisible, _isWalletVisible, _isNotificationVisible),
              ],
            ),
          ),
        ),
      );
    }
  }

  getKycImage(Uint8List imageMemory) {
    var image;
    image = Image.memory(
      imageMemory!,
      height: MediaQuery.of(context).size.height * 0.35,
    );
    var path = File.fromRawPath(_idKycPicture!).path.toString().toLowerCase();
    if (path.contains("pdf")) {
      image = Image.asset(
        "images/pdf-ph.png",
        height: MediaQuery.of(context).size.height * 0.35,
      );
    }
    return image;
  }
}