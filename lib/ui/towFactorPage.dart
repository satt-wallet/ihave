import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:ihave/controllers/LoginController.dart';
import 'package:ihave/ui/WelcomeScreen.dart';
import 'package:ihave/util/hiveUtils.dart';
import 'package:pinput/pin_put/pin_put.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../main.dart';
import '../util/utils.dart';

class TowFactorPage extends StatefulWidget {
  const TowFactorPage({Key? key}) : super(key: key);

  @override
  _TowFactorPageState createState() => _TowFactorPageState();
}

class _TowFactorPageState extends State<TowFactorPage> {
  final FocusNode _pinPutFocusNode = FocusNode();
  final TextEditingController _pinPutController = TextEditingController();

  String authKey = "";

  bool isConfirmPasswordEdited = false;

  bool isConfirmedPassword = false;

  bool successCode = false;
  bool errorCode = false;

  BoxDecoration get _pinPutDecoration {
    return BoxDecoration(
      color: Colors.white,
      borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.033),
      border: Border.all(
        color: Colors.white,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async {
          return false;
        },
        child: Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
              colors: [Color(0xffF52079), Color(0xFF1F2337), Color(0xFF1F2337)],
              begin: const FractionalOffset(0.0, 0.0),
              end: const FractionalOffset(0.0, 1.0),
            )),
            child: Scaffold(
                backgroundColor: Colors.transparent,
                resizeToAvoidBottomInset: false,
                appBar: null,
                body: SingleChildScrollView(
                  child: Column(children: [
                    Padding(
                      padding: EdgeInsets.only(
                        top: MediaQuery.of(context).size.height * 0.02,
                      ),
                      child: Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.fromLTRB(
                          MediaQuery.of(context).size.width * 0.02,
                          MediaQuery.of(context).size.height * 0.05,
                          0,
                          MediaQuery.of(context).size.height * 0.01,
                        ),
                        child: SvgPicture.asset(
                          'images/logo-auth.svg',
                          alignment: Alignment.center,
                          width: MediaQuery.of(context).size.height * 0.044,
                          height: MediaQuery.of(context).size.height * 0.044,
                        ),
                      ),
                    ),
                    Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.fromLTRB(20, 50, 20, 0),
                          child: Container(
                            child: Column(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(
                                    "Sign In",
                                    style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.1, fontWeight: FontWeight.w700, color: Colors.white),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(
                                    "The code has 6 digits displayed in your \n authentication application",
                                    textAlign: TextAlign.center,
                                    style: GoogleFonts.poppins(
                                      fontSize: 16,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(35, 50, 35, 10),
                          child: Container(
                              decoration: BoxDecoration(
                                color: Color(0xff323754),
                                border: Border.all(
                                  color: Colors.transparent,
                                ),
                                borderRadius: BorderRadius.all(Radius.circular(20)),
                              ),
                              child: Column(
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Text(
                                      "Authentification code",
                                      style: GoogleFonts.poppins(
                                        fontWeight: FontWeight.w500,
                                        fontSize: MediaQuery.of(context).size.width * 0.045,
                                        color: isConfirmPasswordEdited ? (isConfirmedPassword ? Colors.green : Color.fromRGBO(245, 32, 121, 1)) : Colors.white,
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: PinPut(
                                        controller: _pinPutController,
                                        textStyle: TextStyle(color: isConfirmPasswordEdited ? (isConfirmedPassword ? Colors.green : Color.fromRGBO(245, 32, 121, 1)) : Colors.blue, fontSize: MediaQuery.of(context).size.width * 0.03),
                                        eachFieldHeight: MediaQuery.of(context).size.width * 0.1,
                                        eachFieldWidth: MediaQuery.of(context).size.width * 0.1,
                                        fieldsCount: 6,
                                        focusNode: _pinPutFocusNode,
                                        onChanged: (content) async {
                                          if (_pinPutController.text.length == 6) {
                                            setState(() {
                                              isConfirmPasswordEdited = true;
                                            });
                                            var codeConfirmed = await LoginController.apiService.verifyQrCode(_pinPutController.text, Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "");
                                            isConfirmedPassword = codeConfirmed;
                                            if (isConfirmedPassword) {
                                              SharedPreferences prefs = await SharedPreferences.getInstance();
                                              prefs.setString('access_token', Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "");
                                              setState(() {
                                                successCode = true;
                                                errorCode == false;
                                              });
                                            } else {
                                              setState(() {
                                                successCode = false;
                                                errorCode = true;
                                              });
                                            }
                                          }
                                        },
                                        submittedFieldDecoration: _pinPutDecoration.copyWith(
                                            border: Border.all(
                                              color: (isConfirmPasswordEdited) ? ((isConfirmedPassword) ? Colors.green : Colors.red) : Colors.grey,
                                            ),
                                            borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.042)),
                                        selectedFieldDecoration: _pinPutDecoration.copyWith(
                                          color: Colors.white,
                                          borderRadius: BorderRadius.circular(15.0),
                                          border: Border.all(
                                            color: (isConfirmPasswordEdited) ? ((isConfirmedPassword) ? Colors.green : Color.fromRGBO(245, 32, 121, 1)) : Colors.grey,
                                          ),
                                        ),
                                        followingFieldDecoration: _pinPutDecoration.copyWith(
                                          borderRadius: BorderRadius.circular(5.0),
                                          border: Border.all(
                                            color: (isConfirmPasswordEdited) ? ((isConfirmedPassword) ? Colors.green : Color.fromRGBO(245, 32, 121, 1)) : Colors.grey,
                                          ),
                                        )),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Text(
                                      isConfirmPasswordEdited ? (isConfirmedPassword ? "Code Match" : "Incorrect Code") : "",
                                      style: GoogleFonts.poppins(
                                        fontSize: 14,
                                        fontWeight: FontWeight.bold,
                                        color: isConfirmPasswordEdited ? (isConfirmedPassword ? Colors.green : Color.fromRGBO(245, 32, 121, 1)) : Colors.blue,
                                      ),
                                    ),
                                  ),
                                ],
                              )),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 20.0),
                          child: isConfirmedPassword
                              ? Container(
                                  width: MediaQuery.of(context).size.width * 0.65,
                                  height: MediaQuery.of(context).size.height * 0.06,
                                  child: ElevatedButton(
                                    onPressed: () {
                                      validate();
                                    },
                                    child: Text(
                                      "Next",
                                      textAlign: TextAlign.center,
                                      style: GoogleFonts.poppins(
                                        fontSize: MediaQuery.of(context).size.width * 0.04,
                                        fontWeight: FontWeight.w600,
                                        color: Colors.white,
                                      ),
                                    ),
                                    style: ButtonStyle(
                                      minimumSize: MaterialStateProperty.all(Size(200, 35)),
                                      backgroundColor: MaterialStateProperty.all(
                                        Color(0xFF0d6efd),
                                      ),
                                      shape: MaterialStateProperty.all(
                                        RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(30),
                                        ),
                                      ),
                                    ),
                                  ),
                                )
                              : Container(
                                  width: MediaQuery.of(context).size.width * 0.65,
                                  height: MediaQuery.of(context).size.height * 0.06,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.08),
                                    color: Color(0xFFF6F6FF),
                                  ),
                                  child: Center(
                                    child: Text(
                                      "Next",
                                      style: GoogleFonts.poppins(
                                        fontSize: MediaQuery.of(context).size.width * 0.04,
                                        fontWeight: FontWeight.w600,
                                        color: Color(0xFFADADC8),
                                      ),
                                    ),
                                  ),
                                ),
                        ),
                        SizedBox(
                          height: MediaQuery.of(context).viewInsets.bottom.toString() == "0.0" ? MediaQuery.of(context).size.height * 0.02 : MediaQuery.of(context).size.height * 0.4,
                        )
                      ],
                    )
                  ]),
                ))));
  }

  validate() {
    if (isConfirmedPassword) {
      gotoPageGlobal(context, WelcomeScreen());
    }
  }
}
