import 'dart:math';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:ihave/controllers/LoginController.dart';
import 'package:ihave/controllers/walletController.dart';
import 'package:ihave/model/crypto.dart';
import 'package:ihave/model/notifications.dart';
import 'package:ihave/service/apiService.dart';
import 'package:ihave/util/utils.dart';
import 'package:intl/intl.dart';
import 'package:jiffy/jiffy.dart';

import 'package:provider/provider.dart';
import 'package:collection/collection.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../main.dart';
import '../widgets/customAppBar.dart';
import '../widgets/customBottomBar.dart';
import 'help.dart';
import "package:collection/collection.dart";
import 'package:jiffy/jiffy.dart';

class NotificationPage extends StatefulWidget {
  const NotificationPage({Key? key}) : super(key: key);

  @override
  _NotificationPageState createState() => _NotificationPageState();
}

class _NotificationPageState extends State<NotificationPage> {
  bool _isLoading = true;
  APIService apiService = LoginController.apiService;
  List<Notifications> _notificationList = <Notifications>[];
  List<List<Notifications>> _notificationListGrouped = <List<Notifications>>[];
  late Future _notification = <Notifications>[] as Future;
  List<String> _list = <String>[];
  List<Crypto> _cryptoList = <Crypto>[];
  var decimal;
  int _counter = 0;
  String? firebaseToken = " ";
  late Future _notifs = <Notifications>[] as Future;
  TextEditingController _search = new TextEditingController();

  bool isWelcomeScreen = false;

  bool isLoadingNotifs = false;

  Future<List<Crypto>> fetchCrypto() async {
    if (Provider.of<WalletController>(context, listen: false).cryptos.length < 1) {
      var cryptoList = await apiService.cryptos1(Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "", version);
      Provider.of<WalletController>(context, listen: false).cryptos = cryptoList;
    }
    return Provider.of<WalletController>(context, listen: false).cryptos;
  }

  Future fetchNotification() async {
    setState(() {
      isLoadingNotifs = true;
    });
    List<Notifications> notificationList = <Notifications>[];
    var notifs = await apiService.getUserNotifications(Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "");
    if (notifs != null && notifs != "" && notifs != "No notifications found") {
      notificationList.addAll(notifs.where((e) => e.type == "send_demande_satt_event" || e.type == "transfer_event" || e.type == "receive_transfer_event" || e.type == "demande_satt_event"));
    }
    setState(() {
      isLoadingNotifs = false;
    });
    return notificationList;
  }

  calculAmount(String amount, double price, int decimal) {
    var decimalcalculated = pow(10, decimal);
    var amountDouble = double.tryParse(amount.replaceAll(',', '')) ?? 0;
    var result = amountDouble * price;
    result = result / decimalcalculated;
    return formatDollarBalance(result);
  }

  ConvertDate(Notifications notif) {
    var dateString = notif.created.toString();
    DateTime dateTimeCreatedAt = DateTime.parse(dateString).add(const Duration(hours: 1));
    ;
    return DateFormat('MMMM d  yyyy hh:mm aaa').format(dateTimeCreatedAt);
  }

  void showNotification() {
    setState(() {
      _counter++;
    });
    flutterLocalNotificationsPlugin.show(0, "Testing $_counter", "How you doin ?", NotificationDetails(android: AndroidNotificationDetails(channel.id, channel.name, importance: Importance.high, color: Colors.blue, playSound: true, icon: '@mipmap/ic_launcher')));
  }

  @override
  void initState() {
    super.initState();
    fetchNotification().then((value) {
      setState(() {
        _notificationList.clear();
        if (value.length > 0) {
          _notificationList.addAll(value.where((e) => e.type == "send_demande_satt_event" || e.type == "transfer_event" || e.type == "receive_transfer_event" || e.type == "demande_satt_event"));
        }
      });
    });
    fetchCrypto().then((value) => setState(() {
          _cryptoList.clear();
          _cryptoList.addAll(value);
          _isLoading = !_isLoading;
        }));
    Provider.of<LoginController>(context, listen: false).tokenSymbol == "";
    Provider.of<LoginController>(context, listen: false).tokenNetwork == "";
    _notification = apiService.getUserNotifications(Provider.of<LoginController>(context, listen: false).userDetails!.userToken ?? "");
  }

  void verifyMethode() async {
    fetchNotification().then((value) {
      setState(() {
        _notificationList.clear();

        _notificationList.addAll(value.where((e) => e.type == "send_demande_satt_event" || e.type == "transfer_event" || e.type == "receive_transfer_event" || e.type == "demande_satt_event"));

        _isLoading = !_isLoading;
      });
    });
  }

  calculDifference(Notifications notif) {
    var myFormat = DateFormat('dd-MM-yyyy');
    var timeFormat = DateFormat("hh:mm aaa");
    var dateString = notif.created.toString();
    DateTime dateTimeCreatedAt = DateTime.parse(dateString).add(const Duration(hours: 1));
    DateTime dateTimeNow = DateTime.now();
    var diffDays = dateTimeNow.difference(dateTimeCreatedAt).inDays;
    var DiffHours = dateTimeNow.difference(dateTimeCreatedAt).inHours;
    if (diffDays <= 1) {
      var diff = dateTimeNow.day - dateTimeCreatedAt.day;
      if (diff == 1) {
        return "Yesterday, ${timeFormat.format(dateTimeCreatedAt)}";
      } else if (diff < 1) {
        return "Today, ${timeFormat.format(dateTimeCreatedAt)}";
      } else {
        var test = ConvertDate(notif).toString();
        return ConvertDate(notif);
      }
    } else {
      var test = ConvertDate(notif).toString();
      return ConvertDate(notif);
    }
  }

  formatNotifAmount(num? decimal, String? amount) {
    var power = pow(10, decimal ?? 18).toString();
    var amountFormatted = BigInt.parse(amount ?? "0") / BigInt.parse(power);
    return amountFormatted.toString();
  }

  @override
  Widget build(BuildContext context) {
    isWelcomeScreen = Provider.of<WalletController>(context, listen: true).isWelcomeScreen;
    return Provider.of<LoginController>(context, listen: false).notifs != ""
        ? Scaffold(
            resizeToAvoidBottomInset: false,
            backgroundColor: Colors.transparent,
            appBar: !isWelcomeScreen ? CustomAppBar() : null,
            bottomNavigationBar: !isWelcomeScreen ? CustomBottomBar() : null,
            body: Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              color: Color(0xFFffffff),
              child: FutureBuilder(
                  future: _notification,
                  builder: (context, AsyncSnapshot snapshot) {
                    if (snapshot.hasData && snapshot.connectionState == ConnectionState.done) {
                      Provider.of<LoginController>(context, listen: false).notifs = snapshot.data;
                      return Padding(
                        padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.01, left: MediaQuery.of(context).size.height * 0.01, right: MediaQuery.of(context).size.height * 0.01),
                        child: Container(
                          child: SingleChildScrollView(
                            child: Stack(children: [
                              Column(
                                children: [
                                  Padding(
                                      padding: EdgeInsets.all(2.0),
                                      child: Form(
                                        child: Column(
                                          children: [
                                            Padding(
                                                padding: EdgeInsets.only(left: MediaQuery.of(context).size.height * 0.01),
                                                child: Align(
                                                    alignment: Alignment.topLeft,
                                                    child: Text(
                                                      "Feed",
                                                      style: GoogleFonts.poppins(
                                                        color: Colors.black,
                                                        fontSize: MediaQuery.of(context).size.height * 0.035,
                                                        fontWeight: FontWeight.w700,
                                                      ),
                                                    ))),
                                            Padding(
                                              padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.height * 0.008, MediaQuery.of(context).size.width * 0.034, MediaQuery.of(context).size.height * 0.008, MediaQuery.of(context).size.width * 0.03),
                                              child: TextFormField(
                                                controller: _search,
                                                enableInteractiveSelection: false,
                                                onChanged: (text) {
                                                  TextSelection previousSelection = _search.selection;
                                                  _search.text = text;
                                                  _search.selection = previousSelection;
                                                  setState(() {
                                                    _isLoading = true;
                                                  });
                                                  searchList(text);
                                                  setState(() {
                                                    _isLoading = false;
                                                  });
                                                },
                                                validator: (value) {
                                                  return null;
                                                },
                                                autocorrect: false,
                                                decoration: new InputDecoration(
                                                  hintText: 'Search',
                                                  prefixIcon: Padding(
                                                    padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.025, right: MediaQuery.of(context).size.width * 0.025),
                                                    child: SvgPicture.asset(
                                                      "images/search.svg",
                                                      width: MediaQuery.of(context).size.width * 0.02,
                                                    ),
                                                  ),
                                                  fillColor: Colors.grey,
                                                  focusColor: Colors.grey,
                                                  hintStyle: GoogleFonts.poppins(fontWeight: FontWeight.normal, color: Colors.grey, fontSize: MediaQuery.of(context).size.width * 0.036),
                                                  contentPadding: new EdgeInsets.symmetric(vertical: 0.0, horizontal: MediaQuery.of(context).size.width * 0.03),
                                                  enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.grey.withOpacity(0.3))),
                                                  focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.grey)),
                                                  errorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red)),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      )),
                                  isLoadingNotifs
                                      ? Container(
                                          child: CircularProgressIndicator(color: Colors.blue),
                                        )
                                      : _notificationList.isNotEmpty
                                          ? ListView.builder(
                                              physics: NeverScrollableScrollPhysics(),
                                              shrinkWrap: true,
                                              scrollDirection: Axis.vertical,
                                              itemCount: _notificationList.length,
                                              itemBuilder: (context, index) {
                                                return Column(
                                                  children: [
                                                    Row(
                                                      children: [
                                                        Column(
                                                          children: [
                                                            Column(
                                                              children: [
                                                                Row(
                                                                  children: [
                                                                    Padding(
                                                                        padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.height * 0.006, MediaQuery.of(context).size.height * 0.006, MediaQuery.of(context).size.height * 0.006, 0),
                                                                        child: _notificationList[index].type.toString() == "transfer_event"
                                                                            ? Container(
                                                                                width: 40,
                                                                                height: 40,
                                                                                decoration: BoxDecoration(
                                                                                  color: Colors.white,
                                                                                  shape: BoxShape.circle,
                                                                                  boxShadow: [BoxShadow(blurRadius: 10, color: Colors.white38, spreadRadius: 2)],
                                                                                ),
                                                                                child: CircleAvatar(
                                                                                  backgroundColor: Colors.white,
                                                                                  child: SvgPicture.asset(
                                                                                    "images/send.svg",
                                                                                    color: Color(0XFF4048FF),
                                                                                    height: 20,
                                                                                    width: 20,
                                                                                  ),
                                                                                ),
                                                                              )
                                                                            : Container(
                                                                                width: 40,
                                                                                height: 40,
                                                                                decoration: BoxDecoration(
                                                                                  color: Colors.white,
                                                                                  shape: BoxShape.circle,
                                                                                  boxShadow: [BoxShadow(blurRadius: 10, color: Colors.grey.shade300, spreadRadius: 1)],
                                                                                ),
                                                                                child: CircleAvatar(
                                                                                  backgroundColor: Colors.white,
                                                                                  child: SvgPicture.asset(
                                                                                    "images/recieve.svg",
                                                                                    color: Color(0XFF00CC9E),
                                                                                    height: 20,
                                                                                    width: 20,
                                                                                  ),
                                                                                ),
                                                                              )),
                                                                  ],
                                                                )
                                                              ],
                                                            )
                                                          ],
                                                        ),
                                                        Padding(
                                                          padding: const EdgeInsets.fromLTRB(8, 10, 1, 1),
                                                          child: Column(
                                                            mainAxisAlignment: MainAxisAlignment.start,
                                                            crossAxisAlignment: CrossAxisAlignment.start,
                                                            mainAxisSize: MainAxisSize.min,
                                                            children: [
                                                              Padding(
                                                                padding: EdgeInsets.fromLTRB(0, 0, MediaQuery.of(context).size.height * 0.09, MediaQuery.of(context).size.height * 0.01),
                                                                child: Text(
                                                                  calculDifference(_notificationList[index]).toString(),
                                                                  style: GoogleFonts.poppins(
                                                                    color: Colors.grey,
                                                                    fontSize: 12,
                                                                    fontWeight: FontWeight.normal,
                                                                  ),
                                                                ),
                                                              ),
                                                              Container(
                                                                width: MediaQuery.of(context).size.width * 0.7,
                                                                child: Wrap(
                                                                  direction: Axis.horizontal,
                                                                  children: [
                                                                    _notificationList[index].type.toString() == "send_demande_satt_event"
                                                                        ? Text(
                                                                            "You requested ${_notificationList[index].notification?.amount} "
                                                                                    "${_notificationList[index].notification?.currency}" +
                                                                                " (${calculAmount(_notificationList[index].notification?.amount ?? "", _cryptoList.firstWhereOrNull((e) => e.symbol == _notificationList[index].notification?.currency)?.price ?? 0.0, 0)})"
                                                                                    " from ${_notificationList[index].notification?.name} ",
                                                                            textAlign: TextAlign.left,
                                                                            style: Theme.of(context).textTheme.subtitle1,
                                                                          )
                                                                        : (_notificationList[index].type.toString() == "receive_transfer_event"
                                                                            ? _notificationList[index].notification!.from != ""
                                                                                ? Text(
                                                                                    "You have received " +
                                                                                        formatNotifAmount(_cryptoList.firstWhereOrNull((c) => c.symbol == _notificationList[index].notification?.currency)?.decimal ?? 18, _notificationList[index].notification?.amount.toString()) +
                                                                                        _notificationList[index].notification!.to.toString() +
                                                                                        " " +
                                                                                        _notificationList[index].notification!.currency.toString() +
                                                                                        " (${calculAmount(_notificationList[index].notification?.amount ?? "", _cryptoList.firstWhereOrNull((e) => e.symbol == _notificationList[index].notification?.currency)?.price ?? 0.0, _cryptoList.firstWhereOrNull((e) => e.decimal != null && e.symbol == _notificationList[index].notification?.currency)?.decimal ?? 18)})" +
                                                                                        " from " +
                                                                                        _notificationList[index].notification!.from.toString().substring(0, 23) +
                                                                                        "...",
                                                                                    style: Theme.of(context).textTheme.subtitle1,
                                                                                  )
                                                                                : Text(
                                                                                    "You have received " +
                                                                                        formatNotifAmount(_cryptoList.firstWhereOrNull((c) => c.symbol == _notificationList[index].notification?.currency)?.decimal ?? 18, _notificationList[index].notification?.amount.toString()) +
                                                                                        _notificationList[index].notification!.to.toString() +
                                                                                        " " +
                                                                                        _notificationList[index].notification!.currency.toString() +
                                                                                        " (${calculAmount(_notificationList[index].notification?.amount ?? "", _cryptoList.firstWhereOrNull((e) => e.symbol == _notificationList[index].notification?.currency)?.price ?? 0.0, _cryptoList.firstWhereOrNull((e) => e.decimal != null && e.symbol == _notificationList[index].notification?.currency)?.decimal ?? 18)})" +
                                                                                        " from " +
                                                                                        _notificationList[index].notification!.from.toString() +
                                                                                        "...",
                                                                                    style: Theme.of(context).textTheme.subtitle1)
                                                                            : (_notificationList[index].type.toString() == "transfer_event"
                                                                                ? Text(
                                                                                    "You sent successfully " +
                                                                                        formatNotifAmount(_cryptoList.firstWhereOrNull((c) => c.symbol == _notificationList[index].notification?.currency)?.decimal ?? 18, _notificationList[index].notification?.amount.toString()) +
                                                                                        " " +
                                                                                        _notificationList[index].notification!.currency.toString() +
                                                                                        "(${calculAmount(_notificationList[index].notification?.amount ?? "", _cryptoList.firstWhereOrNull((e) => e.symbol == _notificationList[index].notification?.currency)?.price ?? 0.0, _cryptoList.firstWhereOrNull((e) => e.decimal != null && e.symbol == _notificationList[index].notification?.currency)?.decimal ?? 18)})"
                                                                                            " to" +
                                                                                        " " +
                                                                                        _notificationList[index].notification!.to.toString().substring(0, 24) +
                                                                                        "...",
                                                                                    style: Theme.of(context).textTheme.subtitle1,
                                                                                  )
                                                                                : Text(
                                                                                    "${_notificationList[index].notification?.name ?? ""} ask you "
                                                                                    "${_notificationList[index].notification?.amount ?? ""} ${_notificationList[index].notification?.currency ?? ""}"
                                                                                    "(${calculAmount(_notificationList[index].notification?.amount ?? "", _cryptoList.firstWhereOrNull((e) => e.symbol == _notificationList[index].notification?.currency)?.price ?? 0.0, 0)})",
                                                                                    style: Theme.of(context).textTheme.subtitle1,
                                                                                  ))),
                                                                  ],
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                    Divider(
                                                      thickness: 1,
                                                      color: Colors.grey.shade200,
                                                    )
                                                  ],
                                                );
                                              },
                                            )
                                          : Container(
                                              child: Padding(
                                              padding: EdgeInsets.all(MediaQuery.of(context).size.height * 0.03),
                                              child: Column(children: [
                                                Text(
                                                  "Nothing to see here for the",
                                                  style: GoogleFonts.poppins(
                                                    fontSize: MediaQuery.of(context).size.width * 0.039,
                                                    fontWeight: FontWeight.w600,
                                                    color: Colors.black,
                                                  ),
                                                ),
                                                Text(
                                                  "moment",
                                                  style: GoogleFonts.poppins(
                                                    fontSize: MediaQuery.of(context).size.width * 0.039,
                                                    fontWeight: FontWeight.w600,
                                                    color: Colors.black,
                                                  ),
                                                )
                                              ]),
                                            )),
                                ],
                              ),
                            ]),
                          ),
                        ),
                      );
                    } else if (snapshot.connectionState == ConnectionState.waiting) {
                      return Center(
                        child: SizedBox(
                          height: MediaQuery.of(context).size.width * 0.037,
                          width: MediaQuery.of(context).size.width * 0.037,
                          child: CircularProgressIndicator(
                            color: Colors.blueAccent,
                          ),
                        ),
                      );
                    } else {
                      return AlertDialog(
                        content: Text("Something went wrong, please try again!"),
                      );
                    }
                  }),
            ),
          )
        : Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            color: Color(0xFFffffff),
            child: FutureBuilder(
                future: _notification,
                builder: (context, AsyncSnapshot snapshot) {
                  if (snapshot.hasData && snapshot.connectionState == ConnectionState.done) {
                    Provider.of<LoginController>(context, listen: false).notifs = snapshot.data;
                    return Padding(
                      padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.01, left: MediaQuery.of(context).size.height * 0.01, right: MediaQuery.of(context).size.height * 0.01),
                      child: Container(
                        child: SingleChildScrollView(
                          child: Stack(children: [
                            Column(
                              children: [
                                Padding(
                                    padding: EdgeInsets.all(2.0),
                                    child: Form(
                                      child: Column(
                                        children: [
                                          Padding(
                                              padding: EdgeInsets.only(left: MediaQuery.of(context).size.height * 0.01),
                                              child: Align(
                                                  alignment: Alignment.topLeft,
                                                  child: Text(
                                                    "My feed",
                                                    style: GoogleFonts.poppins(
                                                      color: Colors.black,
                                                      fontSize: MediaQuery.of(context).size.height * 0.035,
                                                      fontWeight: FontWeight.w700,
                                                    ),
                                                  ))),
                                          Padding(
                                            padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.height * 0.008, MediaQuery.of(context).size.width * 0.034, MediaQuery.of(context).size.height * 0.008, MediaQuery.of(context).size.width * 0.03),
                                            child: TextFormField(
                                              autofocus: true,
                                              controller: _search,
                                              enableInteractiveSelection: false,
                                              onChanged: (text) {
                                                TextSelection previousSelection = _search.selection;
                                                _search.text = text;
                                                _search.selection = previousSelection;
                                                setState(() {
                                                  _isLoading = true;
                                                });
                                                searchList(text);
                                                setState(() {
                                                  _isLoading = false;
                                                });
                                              },
                                              validator: (value) {
                                                return null;
                                              },
                                              autocorrect: false,
                                              decoration: new InputDecoration(
                                                hintText: 'Search',
                                                prefixIcon: Padding(
                                                  padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.025, right: MediaQuery.of(context).size.width * 0.025),
                                                  child: SvgPicture.asset(
                                                    "images/search.svg",
                                                    width: MediaQuery.of(context).size.width * 0.02,
                                                  ),
                                                ),
                                                fillColor: Colors.grey,
                                                focusColor: Colors.grey,
                                                hintStyle: GoogleFonts.poppins(fontWeight: FontWeight.normal, color: Colors.grey, fontSize: MediaQuery.of(context).size.width * 0.036),
                                                contentPadding: new EdgeInsets.symmetric(vertical: 0.0, horizontal: MediaQuery.of(context).size.width * 0.03),
                                                enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.grey.withOpacity(0.3))),
                                                focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.grey)),
                                                errorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red)),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    )),
                                _notificationList.isNotEmpty
                                    ? ListView.builder(
                                        physics: NeverScrollableScrollPhysics(),
                                        shrinkWrap: true,
                                        scrollDirection: Axis.vertical,
                                        itemCount: _notificationList.length,
                                        itemBuilder: (context, index) {
                                          return Column(
                                            children: [
                                              Row(
                                                children: [
                                                  Column(
                                                    children: [
                                                      Column(
                                                        children: [
                                                          Row(
                                                            children: [
                                                              Padding(
                                                                  padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.height * 0.006, MediaQuery.of(context).size.height * 0.006, MediaQuery.of(context).size.height * 0.006, 0),
                                                                  child: _notificationList[index].type.toString() == "transfer_event"
                                                                      ? Container(
                                                                          width: 40,
                                                                          height: 40,
                                                                          decoration: BoxDecoration(
                                                                            color: Colors.white,
                                                                            shape: BoxShape.circle,
                                                                            boxShadow: [BoxShadow(blurRadius: 10, color: Colors.white38, spreadRadius: 2)],
                                                                          ),
                                                                          child: CircleAvatar(
                                                                            backgroundColor: Colors.white,
                                                                            child: SvgPicture.asset(
                                                                              "images/send.svg",
                                                                              color: Color(0XFF4048FF),
                                                                              height: 20,
                                                                              width: 20,
                                                                            ),
                                                                          ),
                                                                        )
                                                                      : Container(
                                                                          width: 40,
                                                                          height: 40,
                                                                          decoration: BoxDecoration(
                                                                            color: Colors.white,
                                                                            shape: BoxShape.circle,
                                                                            boxShadow: [BoxShadow(blurRadius: 10, color: Colors.grey.shade300, spreadRadius: 1)],
                                                                          ),
                                                                          child: CircleAvatar(
                                                                            backgroundColor: Colors.white,
                                                                            child: SvgPicture.asset(
                                                                              "images/recieve.svg",
                                                                              color: Color(0XFF00CC9E),
                                                                              height: 20,
                                                                              width: 20,
                                                                            ),
                                                                          ),
                                                                        )),
                                                            ],
                                                          )
                                                        ],
                                                      )
                                                    ],
                                                  ),
                                                  Padding(
                                                    padding: const EdgeInsets.fromLTRB(8, 10, 1, 1),
                                                    child: Column(
                                                      mainAxisAlignment: MainAxisAlignment.start,
                                                      crossAxisAlignment: CrossAxisAlignment.start,
                                                      mainAxisSize: MainAxisSize.min,
                                                      children: [
                                                        Padding(
                                                          padding: EdgeInsets.fromLTRB(0, 0, MediaQuery.of(context).size.height * 0.09, MediaQuery.of(context).size.height * 0.01),
                                                          child: Text(
                                                            calculDifference(_notificationList[index]).toString(),
                                                            style: GoogleFonts.poppins(
                                                              color: Colors.grey,
                                                              fontSize: 12,
                                                              fontWeight: FontWeight.normal,
                                                            ),
                                                          ),
                                                        ),
                                                        Container(
                                                          width: MediaQuery.of(context).size.width * 0.5,
                                                          child: Wrap(
                                                            direction: Axis.horizontal,
                                                            children: [
                                                              _notificationList[index].type.toString() == "send_demande_satt_event"
                                                                  ? Text(
                                                                      "You requested ${_notificationList[index].notification?.amount}"
                                                                              "${_notificationList[index].notification?.currency}" +
                                                                          " (${calculAmount(_notificationList[index].notification?.amount ?? "", _cryptoList.firstWhereOrNull((e) => e.symbol == _notificationList[index].notification?.currency)?.price ?? 0.0, 0)})"
                                                                              " from ${_notificationList[index].notification?.name} ",
                                                                      textAlign: TextAlign.left,
                                                                    )
                                                                  : (_notificationList[index].type.toString() == "receive_transfer_event"
                                                                      ? _notificationList[index].notification!.from != ""
                                                                          ? Text("You have received " +
                                                                              formatNotifAmount(_cryptoList.firstWhereOrNull((c) => c.symbol == _notificationList[index].notification?.currency)?.decimal ?? 18, _notificationList[index].notification?.amount.toString()) +
                                                                              _notificationList[index].notification!.to.toString() +
                                                                              " " +
                                                                              _notificationList[index].notification!.currency.toString() +
                                                                              " (${calculAmount(_notificationList[index].notification?.amount ?? "", _cryptoList.firstWhereOrNull((e) => e.symbol == _notificationList[index].notification?.currency)?.price ?? 0.0, _cryptoList.firstWhereOrNull((e) => e.decimal != null && e.symbol == _notificationList[index].notification?.currency)?.decimal ?? 18)})" +
                                                                              " from " +
                                                                              _notificationList[index].notification!.from.toString().substring(0, 23) +
                                                                              "...")
                                                                          : Text("You have received " +
                                                                              formatNotifAmount(_cryptoList.firstWhereOrNull((c) => c.symbol == _notificationList[index].notification?.currency)?.decimal ?? 18, _notificationList[index].notification?.amount.toString()) +
                                                                              _notificationList[index].notification!.to.toString() +
                                                                              " " +
                                                                              _notificationList[index].notification!.currency.toString() +
                                                                              " (${calculAmount(_notificationList[index].notification?.amount ?? "", _cryptoList.firstWhereOrNull((e) => e.symbol == _notificationList[index].notification?.currency)?.price ?? 0.0, _cryptoList.firstWhereOrNull((e) => e.decimal != null && e.symbol == _notificationList[index].notification?.currency)?.decimal ?? 18)})" +
                                                                              " from " +
                                                                              _notificationList[index].notification!.from.toString() +
                                                                              "...")
                                                                      : (_notificationList[index].type.toString() == "transfer_event"
                                                                          ? Text("You sent successfully " +
                                                                              formatNotifAmount(_cryptoList.firstWhereOrNull((c) => c.symbol == _notificationList[index].notification?.currency)?.decimal ?? 18, _notificationList[index].notification?.amount.toString()) +
                                                                              " " +
                                                                              _notificationList[index].notification!.currency.toString() +
                                                                              "(${calculAmount(_notificationList[index].notification?.amount ?? "", _cryptoList.firstWhereOrNull((e) => e.symbol == _notificationList[index].notification?.currency)?.price ?? 0.0, _cryptoList.firstWhereOrNull((e) => e.decimal != null && e.symbol == _notificationList[index].notification?.currency)?.decimal ?? 18)})"
                                                                                  " to" +
                                                                              " " +
                                                                              _notificationList[index].notification!.to.toString().substring(0, 24) +
                                                                              "...")
                                                                          : Text("${_notificationList[index].notification?.name ?? ""} ask you "
                                                                              "${_notificationList[index].notification?.amount ?? ""} ${_notificationList[index].notification?.currency ?? ""}"
                                                                              "(${calculAmount(_notificationList[index].notification?.amount ?? "", _cryptoList.firstWhereOrNull((e) => e.symbol == _notificationList[index].notification?.currency)?.price ?? 0.0, 0)})"))),
                                                            ],
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              Divider(
                                                thickness: 1,
                                                color: Colors.grey.shade200,
                                              )
                                            ],
                                          );
                                        },
                                      )
                                    : Container(
                                        child: Padding(
                                        padding: EdgeInsets.all(MediaQuery.of(context).size.height * 0.03),
                                        child: Column(children: [
                                          Text(
                                            "Nothing to see here for the",
                                            style: GoogleFonts.poppins(
                                              fontSize: MediaQuery.of(context).size.width * 0.039,
                                              fontWeight: FontWeight.w600,
                                              color: Colors.black,
                                            ),
                                          ),
                                          Text(
                                            "moment",
                                            style: GoogleFonts.poppins(
                                              fontSize: MediaQuery.of(context).size.width * 0.039,
                                              fontWeight: FontWeight.w600,
                                              color: Colors.black,
                                            ),
                                          )
                                        ]),
                                      )),
                              ],
                            ),
                          ]),
                        ),
                      ),
                    );
                  } else if (snapshot.connectionState == ConnectionState.waiting) {
                    return Center(
                      child: SizedBox(
                        height: MediaQuery.of(context).size.width * 0.037,
                        width: MediaQuery.of(context).size.width * 0.037,
                        child: CircularProgressIndicator(
                          color: Colors.blueAccent,
                        ),
                      ),
                    );
                  } else {
                    return AlertDialog(
                      content: Text("Something went wrong, please try again!"),
                    );
                  }
                }),
          );
  }

  searchList(String text) {
    setState(() {
      if (text != null && text != "") {
        if (_notificationList.length < 1) {
          fetchNotification().then((value) {
            setState(() {
              _notificationList.addAll(value.where((e) => e.type == "send_demande_satt_event" || e.type == "transfer_event" || e.type == "receive_transfer_event" || e.type == "demande_satt_event"));
            });
            searchList(text);
          });
        }
        List<Notifications> finalResult = <Notifications>[];
        if ("demande".contains(text) || "aquire".contains(text)) {
          var result = _notificationList.where((e) => e.type == "send_demande_satt_event").toList();
          finalResult.addAll(result);
        }

        if ("send".contains(text)) {
          var result = _notificationList.where((e) => e.type == "transfer_event").toList();
          finalResult.addAll(result);
        }
        if ("receive".contains(text)) {
          var result = _notificationList.where((e) => e.type! == "receive_transfer_event").toList();
          finalResult.addAll(result);
        }
        if ("demande".contains(text)) {
          var result = _notificationList.where((e) => e.type! == "demande_satt_event").toList();
          finalResult.addAll(result);
        }
        var resultCrypto = _notificationList.where((e) => e.notification?.currency?.toLowerCase().contains(text.toLowerCase()) == true).toList();
        finalResult.addAll(resultCrypto);
        var resultFrom = _notificationList.where((e) => (e.notification?.from?.toLowerCase().contains(text.toLowerCase()) == true) || (e.notification?.name?.toLowerCase().contains(text.toLowerCase()) == true)).toList();
        finalResult.addAll(resultFrom);
        var resultTo = _notificationList.where((e) => e.notification?.to?.toLowerCase().contains(text.toLowerCase()) == true).toList();
        finalResult.addAll(resultTo);
        _notificationList.clear();
        _notificationList.addAll(finalResult);
        setState(() {
          _notificationList = _notificationList.toSet().toList();
        });
      } else {
        fetchNotification().then((value) {
          setState(() {
            _notificationList.addAll(value.where((e) => e.type == "send_demande_satt_event" || e.type == "transfer_event" || e.type == "receive_transfer_event" || e.type == "demande_satt_event"));
          });
        });
      }
    });
  }
}
