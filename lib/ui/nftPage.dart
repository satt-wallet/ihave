import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:ihave/controllers/LoginController.dart';
import 'package:ihave/ui/staticElements/headerNavigation.dart';
import 'package:ihave/widgets/nftGrid.dart';
import 'package:provider/provider.dart';

import '../controllers/walletController.dart';
import '../model/nft.dart';
import '../service/apiService.dart';

class NftPage extends StatefulWidget {
  const NftPage({Key? key}) : super(key: key);

  @override
  State<NftPage> createState() => _NftPageState();
}

class _NftPageState extends State<NftPage> {
  List<NFT> _nfts = <NFT>[];

  TextEditingController _search = new TextEditingController();

  bool _isLoading = false;
  bool _isNotificationVisible = false;
  bool _isVisible = false;

  bool _isProfileVisible = false;

  bool _isWalletVisible = false;

  int selectedIndex = -1;
  String firstText = "";
  String middleText = "";
  APIService apiService = LoginController.apiService;
  String finalText = "";
  String walletAddress = "";
  FocusNode _searchFocus = new FocusNode();
  bool hasFocus = false;

  Future getAddress() async {
    if (apiService.walletAddress == null || apiService.walletAddress == "") {
      await apiService.getWalletAddress(Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "", version);
    }
    return apiService.walletAddress;
  }

  @override
  void initState() {
    getAddress().then((value) {
      walletAddress = apiService.walletAddress ?? "";
    });
    _nfts = Provider.of<WalletController>(context, listen: false).nfts;
    _searchFocus.addListener(() {
      hasFocus = !hasFocus;
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    setState(() {
      _isVisible = Provider.of<WalletController>(context, listen: true).isSideMenuVisible;
      _isProfileVisible = Provider.of<WalletController>(context, listen: true).isProfileVisible;
      _isWalletVisible = Provider.of<WalletController>(context, listen: true).isWalletVisible;
      _isNotificationVisible = Provider.of<WalletController>(context, listen: true).isNotificationVisible;
    });
    return Stack(children: [
      Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 20),
              child: Align(
                alignment: Alignment.topLeft,
                child: Text(
                  "NFT",
                  style: GoogleFonts.poppins(fontSize: 32, fontWeight: FontWeight.w700, color: Color(0xFF00041B)),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.height * 0.02, 0, MediaQuery.of(context).size.height * 0.02, 0),
              child: TextFormField(
                controller: _search,
                focusNode: _searchFocus,
                onChanged: (text) {
                  _search.text = text;
                  _search.selection = TextSelection.fromPosition(TextPosition(offset: _search.text.length));
                  setState(() {
                    _isLoading = true;
                  });
                  searchList(text);
                  setState(() {
                    _isLoading = false;
                  });
                },
                validator: (value) {
                  return null;
                },
                autocorrect: false,
                readOnly: _isNotificationVisible,
                decoration: new InputDecoration(
                  hintText: 'SEARCH AN ITEM',
                  fillColor: Colors.grey,
                  focusColor: Colors.grey,
                  hintStyle: GoogleFonts.poppins(fontWeight: FontWeight.normal, color: Colors.grey, fontSize: MediaQuery.of(context).size.width * 0.036),
                  contentPadding: new EdgeInsets.symmetric(vertical: 0.0, horizontal: MediaQuery.of(context).size.width * 0.03),
                  enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.grey.withOpacity(0.3))),
                  focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.grey)),
                  errorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red)),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 17),
              child: Divider(),
            ),
            Container(
              height: MediaQuery.of(context).size.height * 0.63,
              child: Provider.of<WalletController>(context, listen: false).nfts.length <= 0 ? nftGrid() : NFTGrid(nfts: _nfts),
            ),
          ],
        ),
      ),
      ((_search.text.length > 0) && hasFocus)
          ? Positioned(
              top: MediaQuery.of(context).size.height * 0.13,
              left: MediaQuery.of(context).size.width * 0.042,
              child: Align(
                alignment: Alignment.center,
                child: Visibility(
                  child: _nfts.length > 0
                      ? Center(
                          child: Container(
                            width: MediaQuery.of(context).size.width * 0.91,
                            height: MediaQuery.of(context).size.width * 0.165 * _nfts.length,
                            decoration: BoxDecoration(
                              border: Border.all(color: Colors.grey.withOpacity(0.5)),
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(20.0),
                            ),
                            child: Column(
                              children: [
                                Row(
                                  children: [
                                    Expanded(
                                      child: Container(
                                        child: ListView.builder(
                                          shrinkWrap: true,
                                          scrollDirection: Axis.vertical,
                                          itemBuilder: (context, index) {
                                            return Column(
                                              children: [
                                                ListTile(
                                                  title: formatSearchResult(_nfts[index]),
                                                  onTap: () {
                                                    setState(() {
                                                      selectedIndex = index;
                                                      _search.text = _nfts[index].name ?? "";
                                                      searchList(_search.text);
                                                      formatSearchResult(_nfts[index]);
                                                    });
                                                  },
                                                ),
                                              ],
                                            );
                                          },
                                          itemCount: _nfts.length,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        )
                      : SizedBox.shrink(),
                ),
              ),
            )
          : SizedBox.shrink(),
      HeaderNavigation(_isVisible, _isWalletVisible, _isNotificationVisible),
    ]);
  }

  Widget nftGrid() {
    return new FutureBuilder<List<NFT>>(
      future: LoginController.apiService.getOpenSeaAssets(walletAddress),
      builder: (context, snapshot) {
        if (snapshot.hasError) print(snapshot.error);
        if (snapshot.hasData && snapshot.data!.length > 0) {
          _nfts.clear();
          _nfts.addAll(snapshot.data?.toList() ?? <NFT>[]);
          Provider.of<WalletController>(context, listen: false).nfts = _nfts;
          return new NFTGrid(nfts: _nfts);
        } else {
          return new Column(children: [
            Text(
              "Nothing to see here for the",
              style: GoogleFonts.poppins(
                fontSize: MediaQuery.of(context).size.width * 0.039,
                fontWeight: FontWeight.w600,
                color: Colors.black,
              ),
            ),
            Text(
              "moment",
              style: GoogleFonts.poppins(
                fontSize: MediaQuery.of(context).size.width * 0.039,
                fontWeight: FontWeight.w600,
                color: Colors.black,
              ),
            )
          ]);
        }
      },
    );
  }

  RichText formatSearchResult(NFT element) {
    firstText = element?.name.toString().toLowerCase().substring(0, element?.name.toString().toLowerCase().indexOf(_search.text.toLowerCase())) ?? "";
    middleText = _search.text;
    var position = element?.name.toString().toLowerCase().indexOf(_search.text.toLowerCase()) ?? 0;
    finalText = element?.name.toString().substring(position + _search.text.length) ?? "";
    return RichText(
      text: TextSpan(children: [
        new TextSpan(
          text: firstText,
          style: GoogleFonts.poppins(
            color: Color(0xFFADADC8),
            fontSize: 16,
            fontWeight: FontWeight.w600,
          ),
        ),
        new TextSpan(
          text: middleText,
          style: GoogleFonts.poppins(
            color: Color(0xFF323754),
            fontSize: 16,
            fontWeight: FontWeight.w600,
          ),
        ),
        new TextSpan(
          text: finalText,
          style: GoogleFonts.poppins(
            color: Color(0xFFADADC8),
            fontSize: 16,
            fontWeight: FontWeight.w600,
          ),
        ),
      ]),
    );
  }

  searchList(String text) {
    setState(() {
      if (text != null && text != "") {
        _nfts = Provider.of<WalletController>(context, listen: false).nfts;
        var searchListName = _nfts.where((e) => e.name!.toLowerCase().contains(text.toLowerCase())).toList();
        var searchResult = searchListName;
        _nfts = searchResult.toSet().toList();
      } else {
        _nfts = Provider.of<WalletController>(context, listen: false).nfts;
      }
    });
  }
}
