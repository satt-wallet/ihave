import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_svg/svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:ihave/controllers/walletController.dart';
import 'package:ihave/ui/WelcomeScreen.dart';
import 'package:ihave/ui/signin.dart';
import 'package:ihave/ui/staticElements/headerNavigation.dart';
import 'package:ihave/util/utils.dart';
import 'package:provider/provider.dart';

import '../controllers/LoginController.dart';
import '../widgets/customAppBar.dart';
import '../widgets/customBottomBar.dart';
import 'blockchainKey.dart';

class ConfirmAccountDesactivation extends StatefulWidget {
  const ConfirmAccountDesactivation({Key? key}) : super(key: key);

  @override
  State<ConfirmAccountDesactivation> createState() => _ConfirmAccountDesactivationState();
}

class _ConfirmAccountDesactivationState extends State<ConfirmAccountDesactivation> {
  bool _isVisible = false;
  bool _isProfileVisible = false;
  bool _isWalletVisible = false;
  bool _isNotificationVisible = false;
  TextEditingController _passwordController = TextEditingController();
  bool _checkedValue1 = false;
  bool _isActivatedButton = false;
  bool isDeleted = false;
  bool wrongpassword = false;
  bool isChecked = false;
  String password = "";
  final _desactivateKey = GlobalKey<FormState>();
  bool passwordEdited = false;
  bool isconfirmed = false;
  bool _obscureText = true;
  bool _alertActivatedButton = false;

  bool confirmPasswordEdited = false;
  bool _obscureText1 = true;

  @override
  Widget build(BuildContext context) {
    setState(() {
      _isVisible = Provider.of<WalletController>(context, listen: true).isSideMenuVisible;
      _isProfileVisible = Provider.of<WalletController>(context, listen: true).isProfileVisible;
      _isWalletVisible = Provider.of<WalletController>(context, listen: true).isWalletVisible;
      _isNotificationVisible = Provider.of<WalletController>(context, listen: true).isNotificationVisible;
    });
    return Scaffold(
      appBar: CustomAppBar(),
      bottomNavigationBar: CustomBottomBar(),
      body: Stack(
        children: [
          SingleChildScrollView(
            child: Column(
              children: [
                Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Container(
                        alignment: Alignment.center,
                        child: Column(children: [
                          Padding(
                            padding: EdgeInsets.all(MediaQuery.of(context).size.width * 0.02),
                            child: Align(
                              alignment: Alignment.topLeft,
                              child: Text(
                                "Account\ndesactivation",
                                style: GoogleFonts.poppins(
                                  color: Color(0xFF1F2337),
                                  fontSize: MediaQuery.of(context).size.width * 0.07,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.027, top: MediaQuery.of(context).size.width * 0.03),
                            child: Column(
                              children: [
                                Align(
                                  alignment: Alignment.topLeft,
                                  child: Text("Warning !\n", style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.05, fontWeight: FontWeight.w700, color: Color(0xFFF52079))),
                                ),
                                Align(
                                  alignment: Alignment.topLeft,
                                  child: Text("If you decide to desactivate your\naccount, be sure to back up &\ndownload your blockchain key\nfirst.", style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.043, fontWeight: FontWeight.w600, color: Color(0xFFF52079))),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.2),
                            child: ElevatedButton(
                              style: ButtonStyle(
                                minimumSize: MaterialStateProperty.all(Size(MediaQuery.of(context).size.width * 0.8, MediaQuery.of(context).size.height * 0.06)),
                                backgroundColor: MaterialStateProperty.all(Color(0xffF52079)),
                                shape: MaterialStateProperty.all(
                                  RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.09),
                                  ),
                                ),
                              ),
                              onPressed: () {
                                deactivateAccont();
                              },
                              child: Text(
                                "I confirm deactivation",
                                style: GoogleFonts.poppins(
                                  color: Color(0xffFFFFFF),
                                  fontWeight: FontWeight.w600,
                                  fontSize: 16,
                                ),
                              ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.02),
                            child: ElevatedButton(
                              style: ButtonStyle(
                                minimumSize: MaterialStateProperty.all(Size(MediaQuery.of(context).size.width * 0.8, MediaQuery.of(context).size.height * 0.06)),
                                backgroundColor: MaterialStateProperty.all(Color(0xffFFFFFF)),
                                shape: MaterialStateProperty.all(
                                  RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.09),
                                  ),
                                ),
                              ),
                              onPressed: () {
                                gotoPageGlobal(context, BlockchainKey());
                              },
                              child: Text(
                                "Download my key",
                                style: GoogleFonts.poppins(
                                  color: Color(0xff4048FF),
                                  fontWeight: FontWeight.w600,
                                  fontSize: 16,
                                ),
                              ),
                            ),
                          ),
                        ])))
              ],
            ),
          ),
          HeaderNavigation(_isVisible, _isWalletVisible, _isNotificationVisible)
        ],
      ),
    );
  }

  void deactivateAccont() {
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (context) {
          return StatefulBuilder(builder: (BuildContext context, setState) {
            return Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              child: Dialog(
                  backgroundColor: Colors.transparent,
                  insetPadding: EdgeInsets.zero,
                  child: Container(
                    height: MediaQuery.of(context).size.height,
                    decoration: BoxDecoration(
                      color: Colors.white,
                    ),
                    child: Stack(
                      children: [
                        SingleChildScrollView(
                          child: Column(
                            children: [
                              Padding(
                                padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.04, MediaQuery.of(context).size.width * 0.04, MediaQuery.of(context).size.width * 0.02, MediaQuery.of(context).size.width * 0.1),
                                child: Row(
                                  children: [
                                    Text(
                                      "Account desactivation",
                                      style: GoogleFonts.poppins(
                                        fontSize: MediaQuery.of(context).size.width * 0.06,
                                        fontWeight: FontWeight.w600,
                                      ),
                                    ),
                                    Spacer(),
                                    IconButton(
                                        onPressed: () {
                                          Navigator.of(context).pop();
                                          setState(() {
                                            _passwordController.text = "";
                                            passwordEdited = false;
                                            wrongpassword = false;
                                            isconfirmed = false;
                                            _obscureText = true;
                                          });
                                        },
                                        icon: Icon(
                                          Icons.close,
                                          color: Colors.black54,
                                          size: 25,
                                        )),
                                  ],
                                ),
                              ),
                              Text(
                                "Are you sure you want\nto leave us ?",
                                textAlign: TextAlign.center,
                                style: GoogleFonts.poppins(
                                  fontSize: MediaQuery.of(context).size.width * 0.04,
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.03, bottom: MediaQuery.of(context).size.width * 0.01),
                                child: SvgPicture.asset("images/sadMoonboy.svg"),
                              ),
                              Form(
                                key: _desactivateKey,
                                child: Column(
                                  children: [
                                    Text(
                                      "Once you confirm your password, this",
                                      style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.04, fontWeight: FontWeight.w500, color: Colors.black),
                                    ),
                                    Align(
                                      alignment: Alignment.center,
                                      child: Row(
                                        children: [
                                          Spacer(),
                                          Text(
                                            "account will be ",
                                            style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.04, fontWeight: FontWeight.w500, color: Colors.black),
                                          ),
                                          Text(
                                            "definetely deactivated.",
                                            style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.04, fontWeight: FontWeight.w700, color: Colors.black),
                                          ),
                                          Spacer(),
                                        ],
                                      ),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.04, MediaQuery.of(context).size.width * 0.04, MediaQuery.of(context).size.width * 0.04, MediaQuery.of(context).size.width * 0.1),
                                      child: Align(
                                        alignment: Alignment.center,
                                        child: Column(
                                          children: [
                                            Text("We advise you to be sure of your action.\nIn case of question or doubt, we suggest", textAlign: TextAlign.center, style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.04, fontWeight: FontWeight.w500, color: Colors.black)),
                                            Row(
                                              children: [
                                                Spacer(),
                                                Text("you to ", textAlign: TextAlign.center, style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.04, fontWeight: FontWeight.w500, color: Colors.black)),
                                                InkWell(
                                                  child: Text("contact our support team.", textAlign: TextAlign.center, style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.04, fontWeight: FontWeight.w500, color: Color(0xFF4048FF))),
                                                  onTap: () {
                                                    Provider.of<WalletController>(context, listen: false).selectedIndex = 4;
                                                    gotoPageGlobal(context, WelcomeScreen());
                                                  },
                                                ),
                                                Spacer(),
                                              ],
                                            )
                                          ],
                                        ),
                                      ),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.height * 0.04, MediaQuery.of(context).size.width * 0.04, MediaQuery.of(context).size.height * 0.04, MediaQuery.of(context).size.width * 0.04),
                                      child: TextFormField(
                                        controller: _passwordController,
                                        keyboardType: TextInputType.text,
                                        obscureText: _obscureText,
                                        onChanged: (value) {
                                          wrongpassword = false;
                                          passwordEdited = true;
                                          TextSelection previousSelection = _passwordController.selection;
                                          _passwordController.text = value;
                                          _passwordController.selection = previousSelection;
                                          if (_passwordController.text.length > 0) {
                                            setState(() {
                                              passwordEdited = true;
                                            });
                                          } else {
                                            setState(() {
                                              passwordEdited = false;
                                            });
                                          }
                                          if (_passwordController.text == "") {
                                            setState(() {
                                              isconfirmed = false;
                                            });
                                          } else {
                                            setState(() {
                                              isconfirmed = true;
                                            });
                                          }
                                          TextSelection.fromPosition(TextPosition(offset: value.length));
                                        },
                                        validator: (value) {
                                          if (value!.isEmpty) {
                                            return 'Field required';
                                          }
                                          return null;
                                        },
                                        decoration: InputDecoration(
                                            filled: true,
                                            isDense: _obscureText,
                                            border: InputBorder.none,
                                            suffixStyle: TextStyle(color: Colors.grey),
                                            hintStyle: TextStyle(
                                              fontSize: 18,
                                            ),
                                            suffixIcon: Padding(
                                              padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.01, top: MediaQuery.of(context).size.width * 0.01, bottom: MediaQuery.of(context).size.width * 0.01),
                                              child: IconButton(
                                                  onPressed: () {
                                                    setState(() {
                                                      _obscureText = !_obscureText;
                                                    });
                                                  },
                                                  icon: _obscureText
                                                      ? SvgPicture.asset(
                                                          "images/visibility-icon-off.svg",
                                                          height: MediaQuery.of(context).size.height * 0.02,
                                                          width: MediaQuery.of(context).size.width * 0.03,
                                                        )
                                                      : SvgPicture.asset(
                                                          "images/visibility-icon-on.svg",
                                                          height: MediaQuery.of(context).size.height * 0.02,
                                                          width: MediaQuery.of(context).size.width * 0.03,
                                                        )),
                                            ),
                                            prefixIcon: Padding(
                                              padding: const EdgeInsets.all(2.0),
                                              child: SvgPicture.asset(
                                                "images/green-key-password.svg",
                                                height: MediaQuery.of(context).size.height * 0.02,
                                                width: MediaQuery.of(context).size.width * 0.03,
                                              ),
                                            ),
                                            contentPadding: EdgeInsets.symmetric(horizontal: 20),
                                            fillColor: passwordEdited ? (!wrongpassword ? Colors.white : Colors.white.withGreen(990)) : (Colors.white),
                                            focusColor: Colors.white,
                                            hoverColor: passwordEdited ? (!wrongpassword ? Colors.grey : Colors.red) : (Colors.white),
                                            enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: passwordEdited ? (!wrongpassword ? BorderSide(color: Colors.grey) : BorderSide(color: Colors.red)) : (BorderSide(color: Colors.grey))),
                                            focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: passwordEdited ? (!wrongpassword ? BorderSide(color: Colors.grey) : BorderSide(color: Colors.red)) : BorderSide(color: Colors.grey)),
                                            errorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Color(0xffF52079))),
                                            focusedErrorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Color(0xffF52079)))),
                                      ),
                                    ),
                                    Visibility(
                                      visible: _passwordController.text != "",
                                      child: wrongpassword
                                          ? Padding(
                                              padding: EdgeInsets.fromLTRB(2, 0, MediaQuery.of(context).size.width * 0.35, 0),
                                              child: Text(
                                                "Wrong password",
                                                style: GoogleFonts.poppins(
                                                  color: Color(0xffF52079),
                                                  fontSize: 14.0,
                                                  fontWeight: FontWeight.w600,
                                                ),
                                              ),
                                            )
                                          : Padding(padding: EdgeInsets.all(0.0)),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.height * 0.008, MediaQuery.of(context).size.width * 0.1, MediaQuery.of(context).size.height * 0.008, MediaQuery.of(context).size.width * 0.01),
                                      child: Container(
                                        width: MediaQuery.of(context).size.width * 0.8,
                                        height: MediaQuery.of(context).size.height * 0.06,
                                        child: ElevatedButton(
                                            style: ElevatedButton.styleFrom(
                                                minimumSize: Size(260.0, 40.0),
                                                primary: isconfirmed ? Color(0xFFF52079) : Color(0xffD6D6E8),
                                                shape: RoundedRectangleBorder(
                                                  borderRadius: BorderRadius.circular(50.0),
                                                )),
                                            onPressed: isconfirmed
                                                ? () async {
                                                    if (_desactivateKey.currentState!.validate()) {
                                                      var accountDeleted = await LoginController.apiService.RemoveAccount(
                                                        Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "",
                                                        Provider.of<LoginController>(context, listen: false).deactivationReason,
                                                        _passwordController.text,
                                                      );
                                                      setState(() {
                                                        isDeleted = (accountDeleted == "account deleted");
                                                        wrongpassword = (accountDeleted == "wrong password");
                                                      });
                                                    }
                                                    if (isDeleted) {
                                                      Provider.of<LoginController>(context, listen: false).deactivationReason = "";
                                                      Navigator.pushReplacement(context, MaterialPageRoute(builder: (_) => Signin()));
                                                      Provider.of<LoginController>(context, listen: false).logout();
                                                    }
                                                  }
                                                : null,
                                            child: Text(
                                              "Confirm deactivation",
                                              style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.04, fontWeight: FontWeight.w600, color: isconfirmed ? Colors.white : Color(0xffadb5bd)),
                                            )),
                                      ),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.height * 0.008, MediaQuery.of(context).size.width * 0.02, MediaQuery.of(context).size.height * 0.008, MediaQuery.of(context).size.width * 0.08),
                                      child: Container(
                                        width: MediaQuery.of(context).size.width * 0.8,
                                        height: MediaQuery.of(context).size.height * 0.06,
                                        child: ElevatedButton(
                                            style: ElevatedButton.styleFrom(
                                                minimumSize: Size(MediaQuery.of(context).size.width * 0.8, MediaQuery.of(context).size.height * 0.05),
                                                primary: Color(0xff4048FF),
                                                shape: RoundedRectangleBorder(
                                                  borderRadius: BorderRadius.circular(50.0),
                                                )),
                                            onPressed: () {
                                              Navigator.of(context).pop();
                                              setState(() {
                                                _passwordController.text = "";
                                                confirmPasswordEdited = false;
                                                passwordEdited = false;
                                                wrongpassword = false;
                                                isconfirmed = false;
                                                _obscureText1 = true;
                                                _obscureText = true;
                                              });
                                            },
                                            child: Text(
                                              "cancel",
                                              style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.05, fontWeight: FontWeight.w600, color: Colors.white),
                                            )),
                                      ),
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  )),
            );
          });
        });
  }
}
