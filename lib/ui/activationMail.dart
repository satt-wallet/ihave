import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:ihave/controllers/LoginController.dart';
import 'package:ihave/model/userDetails.dart';
import 'package:ihave/ui/WelcomeScreen.dart';
import 'package:ihave/ui/signin.dart';
import 'package:ihave/ui/signup.dart';
import 'package:ihave/ui/dashboard.dart';
import 'package:ihave/model/user.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;
import 'package:google_sign_in/google_sign_in.dart';
import 'package:ihave/ui/transactionpassword.dart';
import 'package:ihave/util/utils.dart';
import 'package:ihave/widgets/alertDialogWidget.dart';
import 'package:pinput/pin_put/pin_put.dart';
import 'package:provider/provider.dart';

class ActivationMail extends StatefulWidget {
  ActivationMail({Key? key}) : super(key: key);

  @override
  _ActivationMailState createState() => _ActivationMailState();
}

class _ActivationMailState extends State<ActivationMail> {
  final FocusNode _pinPutFocusNode = FocusNode();
  bool _isCorrectCode = false;
  String displayText = "Incorrect Code";
  bool _isButtonActive = false;
  bool _isReSent = false;
  bool _isLoading = false;
  String displayText2 = "we have sent a new confirmation email";
  String _isValid = "";
  final TextEditingController _pinPutController = TextEditingController();

  bool isPinPutEdited = false;
  bool isPinPutfocused = false;

  bool _isCodeExpired = false;
  String errorMessage = "";

  verification(String email, String code) async {
    var result = "";
    bool _isVerified = false;
    var confirm = await LoginController.apiService.confirmCode(email, code);
    if (confirm.runtimeType == bool) {
      if (!confirm) {
        result = "Incorrect Code";
        setState(() {
          _isCorrectCode = false;
          _isButtonActive = false;
          _isCodeExpired = false;
        });
      } else {
        setState(() {
          _isCorrectCode = true;
          _isButtonActive = true;
          _isCodeExpired = false;
        });
        if (Provider.of<LoginController>(context, listen: false).userDetails!.userToken.toString().isNotEmpty) {
          result = "code match";
        }
      }
    } else {
      if (confirm == "code expired") {
        setState(() {
          _isCodeExpired = true;
          _isCorrectCode = false;
          _isButtonActive = false;
        });
        result = "Code Expired";
      } else {
        result = "Incorrect Code";
        setState(() {
          _isCodeExpired = false;
          _isCorrectCode = false;
          _isButtonActive = false;
        });
      }
    }
    return result;
  }

  BoxDecoration get _pinPutDecoration {
    return BoxDecoration(
      color: Colors.white,
      borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.033),
      border: Border.all(
        color: Colors.white,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async {
          return false;
        },
        child: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
                colors: [
                  const Color(0xFF4048FF),
                  const Color(0xFF1F2337),
                ],
                begin: const FractionalOffset(0.0, 0.0),
                end: const FractionalOffset(0.0, 0.2),
                stops: [0.0, 1.0],
                tileMode: TileMode.clamp),
          ),
          child: Scaffold(
              resizeToAvoidBottomInset: false,
              appBar: null,
              backgroundColor: Colors.transparent,
              body: GestureDetector(
                onTap: () {
                  FocusScope.of(context).requestFocus(new FocusNode());
                },
                child: SafeArea(
                  child: Container(
                    height: MediaQuery.of(context).size.height,
                    width: MediaQuery.of(context).size.width,
                    child: Stack(
                      children: [
                        SingleChildScrollView(
                          child: Column(
                            children: [
                              SizedBox(
                                height: MediaQuery.of(context).size.height * 0.02,
                              ),
                              Stack(
                                children: [
                                  Align(
                                    alignment: Alignment.topCenter,
                                    child: SvgPicture.asset(
                                      'images/logo-auth.svg',
                                      alignment: Alignment.center,
                                      width: MediaQuery.of(context).size.height * 0.035,
                                      height: MediaQuery.of(context).size.height * 0.035,
                                    ),
                                  ),
                                  Align(
                                    alignment: Alignment.topRight,
                                    child: Padding(
                                      padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.02, top: MediaQuery.of(context).size.height * 0.0075),
                                      child: InkWell(
                                        child: SvgPicture.asset(
                                          'images/new-logout.svg',
                                        ),
                                        onTap: () {
                                          Provider.of<LoginController>(context, listen: false).logout();
                                          gotoPageGlobal(context, Signin());
                                        },
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: MediaQuery.of(context).size.height * 0.05,
                              ),
                              Text(
                                "An activation email\nhas been sent !",
                                textAlign: TextAlign.center,
                                style: GoogleFonts.poppins(fontWeight: FontWeight.w700, fontSize: MediaQuery.of(context).size.height * 0.041, color: Colors.white, fontStyle: FontStyle.normal, height: 1.1),
                              ),
                              SizedBox(
                                height: MediaQuery.of(context).size.height * 0.035,
                              ),
                              Padding(
                                padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.075, right: MediaQuery.of(context).size.width * 0.075),
                                child: Text(
                                  "Please activate your account by copying the code that we sent to you by email. You will be able to log in as soon as you have validated the registration link.",
                                  textAlign: TextAlign.center,
                                  style: GoogleFonts.poppins(
                                    fontWeight: FontWeight.w300,
                                    fontSize: MediaQuery.of(context).size.height * 0.017,
                                    color: Colors.white,
                                    letterSpacing: 0.2,
                                    fontStyle: FontStyle.normal,
                                    height: 1.5,
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: MediaQuery.of(context).size.height * 0.045,
                              ),
                              Container(
                                margin: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.03, right: MediaQuery.of(context).size.width * 0.03),
                                padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.06, right: MediaQuery.of(context).size.width * 0.06, top: MediaQuery.of(context).size.height * 0.01, bottom: MediaQuery.of(context).size.height * 0.013),
                                width: MediaQuery.of(context).size.width * 0.9,
                                decoration: BoxDecoration(
                                  color: const Color(0xFF323754),
                                  borderRadius: BorderRadius.all(Radius.circular(MediaQuery.of(context).size.height * 0.02)),
                                ),
                                child: Column(
                                  children: [
                                    Padding(
                                      padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.0075, bottom: MediaQuery.of(context).size.height * 0.01),
                                      child: Text(
                                        "Authentication code",
                                        style:
                                            GoogleFonts.poppins(color: isPinPutEdited ? (_isCorrectCode ? Color(0xFF00CC9E) : Color(0xFFF52079)) : Color(0xFFFFFFFF), fontSize: MediaQuery.of(context).size.height * 0.022, fontWeight: FontWeight.w600, fontStyle: FontStyle.normal, letterSpacing: 0.7),
                                      ),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.0075, bottom: MediaQuery.of(context).size.height * 0.02),
                                      child: PinPut(
                                          onChanged: (content) async {
                                            setState(() {
                                              errorMessage = "";
                                            });
                                            isPinPutfocused = true;
                                            setState(() {
                                              isPinPutEdited = false;
                                            });
                                            if (_pinPutController.text.length == 6) {
                                              isPinPutEdited = true;
                                              var validation = await verification(Provider.of<LoginController>(context, listen: false).userDetails!.email.toString(), _pinPutController.text);
                                              if (validation == "code match") {
                                                setState(() {
                                                  _isCorrectCode = true;
                                                });
                                              } else {
                                                errorMessage = validation.toString().toLowerCase() == "code expired" ? "Expired Code" : "Error Code";
                                              }
                                            }
                                          },
                                          textStyle: GoogleFonts.poppins(
                                            fontSize: MediaQuery.of(context).size.height * 0.0175,
                                            fontWeight: FontWeight.w700,
                                            color: (isPinPutEdited) ? ((_isCorrectCode) ? Color(0xFF00CC9E) : Color(0xFFF52079)) : Color(0xFF323754),
                                          ),
                                          eachFieldHeight: MediaQuery.of(context).size.height * 0.05,
                                          eachFieldWidth: MediaQuery.of(context).size.width * 0.105,
                                          fieldsCount: 6,
                                          onSubmit: (String pin) => _showSnackBar(pin, context),
                                          focusNode: _pinPutFocusNode,
                                          controller: _pinPutController,
                                          submittedFieldDecoration: _pinPutDecoration.copyWith(
                                              border: Border.all(
                                                color: isPinPutfocused ? ((isPinPutEdited) ? ((_isCorrectCode) ? Color(0xFF00CC9E) : Color(0xFFF52079)) : Color(0xFF4048FF)) : Color(0xFF4048FF),
                                              ),
                                              borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.042)),
                                          selectedFieldDecoration: _pinPutDecoration.copyWith(
                                              color: Colors.white,
                                              borderRadius: BorderRadius.circular(15.0),
                                              border: Border.all(
                                                color: isPinPutfocused ? ((isPinPutEdited) ? ((_isCorrectCode) ? Color(0xFF00CC9E) : Color(0xFFF52079)) : Color(0xFF4048FF)) : Color(0xFF4048FF),
                                              )),
                                          followingFieldDecoration: _pinPutDecoration.copyWith(
                                            borderRadius: BorderRadius.circular(5.0),
                                            border: Border.all(
                                              color: isPinPutfocused ? ((isPinPutEdited) ? ((_isCorrectCode) ? Color(0xFF00CC9E) : Color(0xFFF52079)) : Color(0xFF4048FF)) : Colors.grey,
                                            ),
                                          )),
                                    ),
                                    Text(
                                      isPinPutEdited ? (_isCorrectCode ? "Code Match" : errorMessage) : "This code will only be valid for 5 min",
                                      style: GoogleFonts.poppins(
                                        fontSize: isPinPutEdited ? 14 : 12,
                                        fontWeight: isPinPutEdited ? FontWeight.w700 : FontWeight.w700,
                                        color: isPinPutEdited ? (_isCorrectCode ? Color(0xFF00CC9E) : Color(0xFFF52079)) : const Color(0xFFD6D6E8),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.05),
                                child: Container(
                                  height: MediaQuery.of(context).size.width * 0.14,
                                  width: MediaQuery.of(context).size.width * 0.65,
                                  child: ElevatedButton(
                                      style: ElevatedButton.styleFrom(
                                        primary: _isButtonActive ? Color(0XFF4048FF) : Colors.grey.shade50,
                                        shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.07),
                                        ),
                                      ),
                                      onPressed: () async {
                                        var validation = await verification(Provider.of<LoginController>(context, listen: false).userDetails!.email.toString(), _pinPutController.text);
                                        errorMessage = "";
                                        if (validation == "code match") {
                                          await LoginController.apiService.activateAccount(Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "", 1);
                                          setState(() {
                                            _isCorrectCode = true;
                                          });

                                          if (LoginController.apiService.walletAddress != null && LoginController.apiService.walletAddress != "") {
                                            gotoPageGlobal(context, WelcomeScreen());
                                          } else {
                                            Navigator.pushReplacement(context, MaterialPageRoute(builder: (_) => TransactionPassword()));
                                          }
                                        } else {
                                          setState(() {
                                            _isCorrectCode = false;
                                            errorMessage = validation.toString().toLowerCase() == "code expired" ? "Expired Code" : "Error Code";
                                          });
                                        }
                                      },
                                      child: Text(
                                        "Confirm Email",
                                        style: GoogleFonts.poppins(
                                          color: _isButtonActive ? const Color(0xFFFFFFFF) : const Color(0xFFADADC8),
                                          fontWeight: FontWeight.w600,
                                          fontSize: MediaQuery.of(context).size.height * 0.022,
                                          fontStyle: FontStyle.normal,
                                        ),
                                      )),
                                ),
                              ),
                              SizedBox(height: MediaQuery.of(context).size.height * 0.035),
                              Padding(
                                padding: EdgeInsets.only(
                                  left: MediaQuery.of(context).size.width * 0.1,
                                  right: MediaQuery.of(context).size.width * 0.1,
                                ),
                                child: Text(
                                  "If you did not receive the verification Email, make sure to check your spam folder.",
                                  textAlign: TextAlign.center,
                                  style: GoogleFonts.poppins(fontWeight: FontWeight.w700, fontSize: MediaQuery.of(context).size.height * 0.016, color: Colors.white, height: 1.5, letterSpacing: 0.3),
                                ),
                              ),
                              SizedBox(
                                height: MediaQuery.of(context).size.height * 0.025,
                              ),
                              Container(
                                height: MediaQuery.of(context).size.height * 0.07,
                                width: MediaQuery.of(context).size.width * 0.7,
                                child: ElevatedButton(
                                    style: ElevatedButton.styleFrom(
                                      primary: Colors.white,
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(MediaQuery.of(context).size.height * 0.07),
                                      ),
                                    ),
                                    onPressed: () async {
                                      setState(() {
                                        _isLoading = true;
                                        _isReSent = false;
                                      });
                                      var resendCode = await LoginController.apiService.resendCode(Provider.of<LoginController>(context, listen: false).userDetails!.email.toString().trim(), _pinPutController.text);
                                      if (resendCode == "error") {
                                        setState(() {
                                          _isReSent = false;
                                          _isLoading = false;
                                        });
                                        displayDialog(context, "Error", "Something went wrong, please try again");
                                      } else {
                                        setState(() {
                                          _isReSent = true;
                                          _isLoading = false;
                                        });
                                      }
                                    },
                                    child: !_isLoading
                                        ? Text(
                                            "Re-send code",
                                            style: GoogleFonts.poppins(color: Color(0xFF4048FF), fontWeight: FontWeight.w600, fontSize: MediaQuery.of(context).size.height * 0.022),
                                          )
                                        : SizedBox(
                                            height: MediaQuery.of(context).size.height * 0.018,
                                            width: MediaQuery.of(context).size.height * 0.018,
                                            child: CircularProgressIndicator(
                                              color: Color(0xFF4048FF),
                                            ))),
                              ),
                              Padding(
                                padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.033),
                                child: _isReSent
                                    ? Text(
                                        displayText2,
                                        textAlign: TextAlign.center,
                                        style: GoogleFonts.poppins(fontWeight: FontWeight.w700, fontSize: MediaQuery.of(context).size.height * 0.019, color: Colors.white),
                                      )
                                    : null,
                              ),
                              SizedBox(
                                height: MediaQuery.of(context).viewInsets.bottom.toString() == "0.0" ? MediaQuery.of(context).size.height * 0.02 : MediaQuery.of(context).size.height * 0.35,
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              )),
        ));
  }

  void _showSnackBar(String pin, BuildContext context) {
    final snackBar = SnackBar(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(18.0),
      ),
      duration: Duration(seconds: 5),
      content: Container(
          height: 80.0,
          color: Colors.grey,
          child: Center(
            child: Text(
              'Pin Submitted. Value: $pin',
              style: TextStyle(fontSize: 25.0, backgroundColor: Colors.grey),
            ),
          )),
      backgroundColor: Colors.grey,
    );
  }
}