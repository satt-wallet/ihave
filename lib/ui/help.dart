import 'dart:ui';

import 'package:convex_bottom_bar/convex_bottom_bar.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:ihave/controllers/LoginController.dart';
import 'package:ihave/controllers/walletController.dart';
import 'package:ihave/model/helpModel.dart';
import 'package:ihave/ui/staticElements/headerNavigation.dart';
import 'package:ihave/widgets/customAppBar.dart';
import 'package:ihave/widgets/customBottomBar.dart';
import 'package:one_context/one_context.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

class Help extends StatefulWidget {
  Help({Key? key}) : super(key: key);

  @override
  _HelpState createState() => _HelpState();
}

class _HelpState extends State<Help> with TickerProviderStateMixin {
  bool _isVisible = false;
  bool _isProfileVisible = false;
  bool _isWalletVisible = false;
  bool _isNotificationVisible = false;
  bool _isclicked = false;
  String clickedWallet = "";
  String clickedAccount = "";
  String clickedPrivacy = "";
  String newListClik = "";
  bool _isSearch = false;
  bool searchFormField = false;
  bool _isclickedaccount = false;
  bool _isclickedprivacy = false;
  final dataKey = new GlobalKey();
  final _formkey = GlobalKey<FormState>();
  List<Helps> helpAccount = Helps.helpAccount;
  List<Helps> helpWallet = Helps.helpWallet;
  List<Helps> helpTransaction = Helps.helpTransaction;
  List<Helps> newList = <Helps>[];

  static List<Widget> _widgetOptions = <Widget>[];

  TextEditingController name = new TextEditingController();
  TextEditingController email = new TextEditingController();
  TextEditingController object = new TextEditingController();
  TextEditingController message = new TextEditingController();
  TextEditingController _search = new TextEditingController();
  TabController? _tabController;
  final maxLines = 3;
  bool _isvalidFormEmail = false;
  bool isSend = false;
  bool _validate = false;
  bool _isLoading = true;
  int _selectedIndex = 0;
  ScrollController scrollController = ScrollController();
  bool showbtn = false;
  final scrollDirection = Axis.vertical;
  bool isClick = false;

  _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url, forceSafariVC: true, forceWebView: false, enableJavaScript: true);
    } else {
      throw 'Could not launch $url';
    }
  }

  _launchURLERC20() async {
    const url = 'https://etherscan.io/token/0xdf49c9f599a0a9049d97cff34d0c30e468987389';
    if (await canLaunch(url)) {
      await launch(url, forceSafariVC: true, forceWebView: false, enableJavaScript: true);
    } else {
      throw 'Could not launch $url';
    }
  }

  _launchURLBEP20() async {
    const url = 'https://bscscan.com/token/0x448bee2d93be708b54ee6353a7cc35c4933f1156';
    if (await canLaunch(url)) {
      await launch(url, forceSafariVC: true, forceWebView: false, enableJavaScript: true);
    } else {
      throw 'Could not launch $url';
    }
  }

  _launchURLSATT() async {
    const url = 'https://dapp.satt.io/welcome';
    if (await canLaunch(url)) {
      await launch(url, forceSafariVC: true, forceWebView: false, enableJavaScript: true);
    } else {
      throw 'Could not launch $url';
    }
  }

  _launchURLSIMPLEX() async {
    const url = 'https://www.simplex.com/';
    if (await canLaunch(url)) {
      await launch(url, forceSafariVC: true, forceWebView: false, enableJavaScript: true);
    } else {
      throw 'Could not launch $url';
    }
  }

  _launchURLbittrex() async {
    const url = 'https://global.bittrex.com';
    if (await canLaunch(url)) {
      await launch(url, forceSafariVC: true, forceWebView: false, enableJavaScript: true);
    } else {
      throw 'Could not launch $url';
    }
  }

  _launchURLProbit() async {
    const url = 'https://www.probit.com/app/exchange/SATT-USDT';
    if (await canLaunch(url)) {
      await launch(url, forceSafariVC: true, forceWebView: false, enableJavaScript: true);
    } else {
      throw 'Could not launch $url';
    }
  }

  _launchURLHitbtc() async {
    const url = 'https://hitbtc.com/satt-to-usdt';
    if (await canLaunch(url)) {
      await launch(url, forceSafariVC: true, forceWebView: false, enableJavaScript: true);
    } else {
      throw 'Could not launch $url';
    }
  }

  _launchURLBitcoin() async {
    const url = 'https://exchange.bitcoin.com';
    if (await canLaunch(url)) {
      await launch(url, forceSafariVC: true, forceWebView: false, enableJavaScript: true);
    } else {
      throw 'Could not launch $url';
    }
  }

  _launchURLDigifinex() async {
    const url = 'https://www.digifinex.com/en-ww/trade/USDT/SATT';
    if (await canLaunch(url)) {
      await launch(url, forceSafariVC: true, forceWebView: false, enableJavaScript: true);
    } else {
      throw 'Could not launch $url';
    }
  }

  _launchURLUniswap() async {
    const url = 'https://app.uniswap.org/#/swap?chain=mainnet';
    if (await canLaunch(url)) {
      await launch(url, forceSafariVC: true, forceWebView: false, enableJavaScript: true);
    } else {
      throw 'Could not launch $url';
    }
  }

  _launchURLContract() async {
    const url = 'https://etherscan.io/token/0x70A6395650b47D94A77dE4cFEDF9629f6922e645';
    if (await canLaunch(url)) {
      await launch(url, forceSafariVC: true, forceWebView: false, enableJavaScript: true);
    } else {
      throw 'Could not launch $url';
    }
  }

  _launchURLUniswapTutorial() async {
    const url = 'https://satt-token.com/blog/2020/10/16/getting-on-uniswap-1-how-to-get-wsatt-so-as-to-provide-liquidity-on-uniswap-to-collect-rewards/';
    if (await canLaunch(url)) {
      await launch(url, forceSafariVC: true, forceWebView: false, enableJavaScript: true);
    } else {
      throw 'Could not launch $url';
    }
  }

  _launchURLPancackeSwap() async {
    const url = 'https://pancakeswap.finance/swap#/swap?inputCurrency=BNB&outputCurrency=0x448bee2d93be708b54ee6353a7cc35c4933f1156';
    if (await canLaunch(url)) {
      await launch(url, forceSafariVC: true, forceWebView: false, enableJavaScript: true);
    } else {
      throw 'Could not launch $url';
    }
  }

  _launchURLPancackeSwapContract() async {
    const url = 'https://pancakeswap.finance/swap#/swap?inputCurrency=BNB&outputCurrency=0x448bee2d93be708b54ee6353a7cc35c4933f1156';
    if (await canLaunch(url)) {
      await launch(url, forceSafariVC: true, forceWebView: false, enableJavaScript: true);
    } else {
      throw 'Could not launch $url';
    }
  }

  _launchURLPancackeSwapTutorial() async {
    const url = 'https://satt-token.com/blog/2021/02/23/pancakeswap-bnb-%E2%86%94-satt-bep20-en/';
    if (await canLaunch(url)) {
      await launch(url, forceSafariVC: true, forceWebView: false, enableJavaScript: true);
    } else {
      throw 'Could not launch $url';
    }
  }

  _launchURLBridge() async {
    const url = 'https://satt-token.com/blog/2021/02/17/how-to-use-the-erc20-bep20-bridge/';
    if (await canLaunch(url)) {
      await launch(url, forceSafariVC: true, forceWebView: false, enableJavaScript: true);
    } else {
      throw 'Could not launch $url';
    }
  }

  _launchURLMetamaskReg() async {
    const url = 'https://satt-token.com/blog/2021/01/08/tutorial-register-on-metamask-and-export-your-satt/';
    if (await canLaunch(url)) {
      await launch(url, forceSafariVC: true, forceWebView: false, enableJavaScript: true);
    } else {
      throw 'Could not launch $url';
    }
  }

  _launchURLMetamaskConf() async {
    const url = 'https://satt-token.com/blog/2021/01/08/tutorial-register-on-metamask-and-export-your-satt/';
    if (await canLaunch(url)) {
      await launch(url, forceSafariVC: true, forceWebView: false, enableJavaScript: true);
    } else {
      throw 'Could not launch $url';
    }
  }

  _launchURLTrustWalletReg() async {
    const url = 'https://satt-token.com/blog/2021/02/19/tutorial-download-trust-wallet-and-export-your-satt-erc20-wallet/';
    if (await canLaunch(url)) {
      await launch(url, forceSafariVC: true, forceWebView: false, enableJavaScript: true);
    } else {
      throw 'Could not launch $url';
    }
  }

  _launchURLTrustWalletConf() async {
    const url = 'https://satt-token.com/blog/2021/02/19/tutorial-configure-trust-wallet-for-bsc-and-convert-your-satt/';
    if (await canLaunch(url)) {
      await launch(url, forceSafariVC: true, forceWebView: false, enableJavaScript: true);
    } else {
      throw 'Could not launch $url';
    }
  }

  _launchURtHere() async {
    const url = 'https://satt-token.com/blog/2021/02/17/how-to-use-the-erc20-bep20-bridge/';
    if (await canLaunch(url)) {
      await launch(url, forceSafariVC: true, forceWebView: false, enableJavaScript: true);
    } else {
      throw 'Could not launch $url';
    }
  }

  _launchURtIframe() async {
    const url = 'https://iframe-apps.com/';
    if (await canLaunch(url)) {
      await launch(url, forceSafariVC: true, forceWebView: false, enableJavaScript: true);
    } else {
      throw 'Could not launch $url';
    }
  }

  _launchURtSatt() async {
    const url = 'https://dapp.satt.io/welcome';
    if (await canLaunch(url)) {
      await launch(url, forceSafariVC: true, forceWebView: false, enableJavaScript: true);
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  void initState() {
    super.initState();
    _tabController = new TabController(vsync: this, length: 3);
    Provider.of<LoginController>(context, listen: false).tokenSymbol == "";
    Provider.of<LoginController>(context, listen: false).tokenNetwork == "";
  }

  @override
  void dispose() {
    _tabController?.dispose();
    super.dispose();
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
      Provider.of<WalletController>(context, listen: false).isSideMenuVisible = false;
      Provider.of<WalletController>(context, listen: false).isProfileVisible = false;
      Provider.of<WalletController>(context, listen: false).isWalletVisible = false;
      Provider.of<WalletController>(context, listen: false).isNotificationVisible = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    setState(() {
      _isVisible = Provider.of<WalletController>(context, listen: true).isSideMenuVisible;
      _isProfileVisible = Provider.of<WalletController>(context, listen: true).isProfileVisible;
      _isWalletVisible = Provider.of<WalletController>(context, listen: true).isWalletVisible;
      _isNotificationVisible = Provider.of<WalletController>(context, listen: true).isNotificationVisible;
      _widgetOptions = <Widget>[
        helpWallet.isEmpty
            ? Center(
                child: SizedBox(
                  height: MediaQuery.of(context).size.width * 0.037,
                  width: MediaQuery.of(context).size.width * 0.037,
                  child: CircularProgressIndicator(
                    color: Color(0xFF4048FF),
                  ),
                ),
              )
            : SingleChildScrollView(
                child: Column(
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width,
                      child: ExpansionTile(
                        title: Text(
                          "What exactly is a SaTT ?",
                          style: GoogleFonts.poppins(
                            color: Colors.black,
                            fontWeight: FontWeight.w500,
                            fontSize: MediaQuery.of(context).size.width * 0.037,
                          ),
                        ),
                        children: [
                          ListTile(
                              title: RichText(
                            text: TextSpan(
                                style: GoogleFonts.poppins(
                                  color: Colors.black,
                                  fontWeight: FontWeight.w500,
                                  fontSize: MediaQuery.of(context).size.width * 0.037,
                                ),
                                children: [
                                  new TextSpan(
                                      text: 'SaTT is a digital advertising token '
                                          'that facilitates smoother transactions between content creators and advertisers. '
                                          'Ultimately,SaTT facilitates faster, cheaper, and more secure digital advertising purchases. '
                                          'Of note, SaTT is available on both the Ethereum blockchain'),
                                  new TextSpan(
                                      text: ' ERC20 ', style: GoogleFonts.poppins(color: Colors.blue, fontWeight: FontWeight.w500, fontSize: MediaQuery.of(context).size.width * 0.037, decoration: TextDecoration.underline), recognizer: new TapGestureRecognizer()..onTap = () => _launchURLERC20()),
                                  new TextSpan(text: 'and Binance Smart Chain'),
                                  new TextSpan(
                                      text: ' BEP20.', style: GoogleFonts.poppins(color: Colors.blue, fontWeight: FontWeight.w500, fontSize: MediaQuery.of(context).size.width * 0.037, decoration: TextDecoration.underline), recognizer: new TapGestureRecognizer()..onTap = () => _launchURLBEP20())
                                ]),
                          )),
                        ],
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      child: ExpansionTile(
                        title: Text(
                          "How to buy SaTT??",
                          style: GoogleFonts.poppins(
                            color: Colors.black,
                            fontWeight: FontWeight.w500,
                            fontSize: MediaQuery.of(context).size.width * 0.037,
                          ),
                        ),
                        children: [
                          ListTile(
                              title: RichText(
                            text: TextSpan(
                                style: GoogleFonts.poppins(
                                  color: Colors.black,
                                  fontWeight: FontWeight.w500,
                                  fontSize: MediaQuery.of(context).size.width * 0.037,
                                ),
                                children: [
                                  new TextSpan(text: 'Buy SaTT ERC20 and BEP20 with your credit card or Apple Pay on'),
                                  new TextSpan(
                                      text: ' SaTT.io ',
                                      style: GoogleFonts.poppins(
                                        color: Colors.blue,
                                        fontWeight: FontWeight.w500,
                                        fontSize: MediaQuery.of(context).size.width * 0.037,
                                      ),
                                      recognizer: new TapGestureRecognizer()..onTap = () => _launchURLSATT()),
                                  new TextSpan(text: 'and'),
                                  new TextSpan(
                                      text: ' Simplex.',
                                      style: GoogleFonts.poppins(
                                        color: Colors.blue,
                                        fontWeight: FontWeight.w500,
                                        fontSize: MediaQuery.of(context).size.width * 0.037,
                                      ),
                                      recognizer: new TapGestureRecognizer()..onTap = () => _launchURLSIMPLEX()),
                                  new TextSpan(text: 'Buy SaTT ERC20 on '),
                                  new TextSpan(
                                      text: ' Bittrex',
                                      style: GoogleFonts.poppins(
                                        color: Colors.blue,
                                        fontWeight: FontWeight.w500,
                                        fontSize: MediaQuery.of(context).size.width * 0.037,
                                      ),
                                      recognizer: new TapGestureRecognizer()..onTap = () => _launchURLbittrex()),
                                  new TextSpan(text: ','),
                                  new TextSpan(
                                      text: ' Probit',
                                      style: GoogleFonts.poppins(
                                        color: Colors.blue,
                                        fontWeight: FontWeight.w500,
                                        fontSize: MediaQuery.of(context).size.width * 0.037,
                                      ),
                                      recognizer: new TapGestureRecognizer()..onTap = () => _launchURLProbit()),
                                  new TextSpan(text: ','),
                                  new TextSpan(
                                      text: ' HitBTC',
                                      style: GoogleFonts.poppins(
                                        color: Colors.blue,
                                        fontWeight: FontWeight.w500,
                                        fontSize: MediaQuery.of(context).size.width * 0.037,
                                      ),
                                      recognizer: new TapGestureRecognizer()..onTap = () => _launchURLHitbtc()),
                                  new TextSpan(text: ','),
                                  new TextSpan(
                                      text: ' Bitcoin.com',
                                      style: GoogleFonts.poppins(
                                        color: Colors.blue,
                                        fontWeight: FontWeight.w500,
                                        fontSize: MediaQuery.of(context).size.width * 0.037,
                                      ),
                                      recognizer: new TapGestureRecognizer()..onTap = () => _launchURLBitcoin()),
                                  new TextSpan(text: ','),
                                  new TextSpan(
                                      text: ' Digifinex',
                                      style: GoogleFonts.poppins(
                                        color: Colors.blue,
                                        fontWeight: FontWeight.w500,
                                        fontSize: MediaQuery.of(context).size.width * 0.037,
                                      ),
                                      recognizer: new TapGestureRecognizer()..onTap = () => _launchURLDigifinex()),
                                  new TextSpan(text: '.'),
                                  new TextSpan(text: ' Buy Satt ERC20 (WSATT) on '),
                                  new TextSpan(
                                      text: ' Uniswap',
                                      style: GoogleFonts.poppins(
                                        color: Colors.blue,
                                        fontWeight: FontWeight.w500,
                                        fontSize: MediaQuery.of(context).size.width * 0.037,
                                      ),
                                      recognizer: new TapGestureRecognizer()..onTap = () => _launchURLUniswap()),
                                  new TextSpan(text: ' - '),
                                  new TextSpan(text: 'contract:'),
                                  new TextSpan(
                                      text: '  0x70A6395650b47D94A77dE4cFEDF9629f6922e645 \n',
                                      style: GoogleFonts.poppins(
                                        color: Colors.blue,
                                        fontWeight: FontWeight.w500,
                                        fontSize: MediaQuery.of(context).size.width * 0.037,
                                      ),
                                      recognizer: new TapGestureRecognizer()..onTap = () => _launchURLContract()),
                                  new TextSpan(text: '.'),
                                  new TextSpan(
                                      text: ' Uniswap tutorial ',
                                      style: GoogleFonts.poppins(
                                        color: Colors.blue,
                                        fontWeight: FontWeight.w500,
                                        fontSize: MediaQuery.of(context).size.width * 0.037,
                                      ),
                                      recognizer: new TapGestureRecognizer()..onTap = () => _launchURLUniswapTutorial()),
                                  new TextSpan(text: '.'),
                                  new TextSpan(text: ' Buy Satt BEP20 on '),
                                  new TextSpan(
                                      text: '  Pancakeswap',
                                      style: GoogleFonts.poppins(
                                        color: Colors.blue,
                                        fontWeight: FontWeight.w500,
                                        fontSize: MediaQuery.of(context).size.width * 0.037,
                                      ),
                                      recognizer: new TapGestureRecognizer()..onTap = () => _launchURLPancackeSwapContract()),
                                  new TextSpan(text: ' - '),
                                  new TextSpan(text: 'contract: '),
                                  new TextSpan(
                                      text: ' 0x70A6395650b47D94A77dE4cFEDF9629f6922e645',
                                      style: GoogleFonts.poppins(
                                        color: Colors.blue,
                                        fontWeight: FontWeight.w500,
                                        fontSize: MediaQuery.of(context).size.width * 0.037,
                                      ),
                                      recognizer: new TapGestureRecognizer()..onTap = () => _launchURLPancackeSwapContract()),
                                  new TextSpan(text: '.'),
                                  new TextSpan(
                                      text: ' Pancakeswap tutorial',
                                      style: GoogleFonts.poppins(
                                        color: Colors.blue,
                                        fontWeight: FontWeight.w500,
                                        fontSize: MediaQuery.of(context).size.width * 0.037,
                                      ),
                                      recognizer: new TapGestureRecognizer()..onTap = () => _launchURLPancackeSwapTutorial()),
                                  new TextSpan(text: ' Bridge SATT ERC20 -> SATT BEP20 : \n 0x655371C0622cACc22732E872a68034f38E04d6e5 .  '),
                                  new TextSpan(
                                      text: 'Bridge tutorial',
                                      style: GoogleFonts.poppins(color: Colors.blue, fontWeight: FontWeight.w500, fontSize: MediaQuery.of(context).size.width * 0.037, decoration: TextDecoration.underline),
                                      recognizer: new TapGestureRecognizer()..onTap = () => _launchURLBridge()),
                                ]),
                          )),
                          ListTile(
                            title: RichText(
                              text: TextSpan(
                                style: GoogleFonts.poppins(
                                  color: Colors.black,
                                  fontWeight: FontWeight.w500,
                                  fontSize: MediaQuery.of(context).size.width * 0.037,
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      child: ExpansionTile(
                        title: Text(
                          "We’ve partnered with Multichain, here's how to bridge your SaTT:",
                          style: GoogleFonts.poppins(
                            color: Colors.black,
                            fontWeight: FontWeight.w500,
                            fontSize: MediaQuery.of(context).size.width * 0.037,
                          ),
                        ),
                        children: [
                          ListTile(
                              title: RichText(
                            text: TextSpan(
                                style: GoogleFonts.poppins(
                                  color: Colors.black,
                                  fontWeight: FontWeight.w500,
                                  fontSize: MediaQuery.of(context).size.width * 0.037,
                                ),
                                children: [
                                  new TextSpan(text: '1.Go to '),
                                  new TextSpan(
                                      text: 'https://app.multichain.org/#/router. ',
                                      style: GoogleFonts.poppins(color: Colors.blue, fontWeight: FontWeight.w500, fontSize: MediaQuery.of(context).size.width * 0.037, decoration: TextDecoration.underline),
                                      recognizer: new TapGestureRecognizer()..onTap = () => _launchURL("https://app.multichain.org/#/router")),
                                  new TextSpan(
                                      text: '2. In the top menu bar, select Ethereum Mainnet or BNB Chain Mainnet, depending on '
                                          'the SaTT you have in your wallet.\n'
                                          '3. Connect your wallet, making sure it is configured on the same blockchain as the one '
                                          'previously selected.\n'
                                          '4. Select SaTT from the drop-down list.\n'
                                          '5. Enter the amount you wish to exchange and then confirm the transaction.\n'
                                          '6. You will receive your SaTT on the same address.\n'),
                                ]),
                          )),
                        ],
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      child: ExpansionTile(
                        title: Text(
                          "How do I configure SaTT to the crypto wallets Metamask or Trust Wallet ?",
                          style: GoogleFonts.poppins(
                            color: Colors.black,
                            fontWeight: FontWeight.w500,
                            fontSize: MediaQuery.of(context).size.width * 0.037,
                          ),
                        ),
                        children: [
                          ListTile(
                              title: RichText(
                            text: TextSpan(
                                style: GoogleFonts.poppins(
                                  color: Colors.black,
                                  fontWeight: FontWeight.w500,
                                  fontSize: MediaQuery.of(context).size.width * 0.037,
                                ),
                                children: [
                                  new TextSpan(text: 'Whether you’re trying to configure MetaMask or Trust Wallet,we recommend reviewing the following resources:\n'),
                                  new TextSpan(
                                      text: '  MetaMask Registration\n ',
                                      style: GoogleFonts.poppins(
                                        color: Colors.blue,
                                        fontWeight: FontWeight.w500,
                                        fontSize: MediaQuery.of(context).size.width * 0.037,
                                      ),
                                      recognizer: new TapGestureRecognizer()..onTap = () => _launchURLMetamaskReg()),
                                  new TextSpan(
                                      text: ' MetaMask Configuration\n',
                                      style: GoogleFonts.poppins(
                                        color: Colors.blue,
                                        fontWeight: FontWeight.w500,
                                        fontSize: MediaQuery.of(context).size.width * 0.037,
                                      ),
                                      recognizer: new TapGestureRecognizer()..onTap = () => _launchURLMetamaskConf()),
                                  new TextSpan(
                                      text: '  Trust Wallet Registration\n ',
                                      style: GoogleFonts.poppins(
                                        color: Colors.blue,
                                        fontWeight: FontWeight.w500,
                                        fontSize: MediaQuery.of(context).size.width * 0.037,
                                      ),
                                      recognizer: new TapGestureRecognizer()..onTap = () => _launchURLTrustWalletReg()),
                                  new TextSpan(
                                      text: ' Trust Wallet Configuration\n',
                                      style: GoogleFonts.poppins(
                                        color: Colors.blue,
                                        fontWeight: FontWeight.w500,
                                        fontSize: MediaQuery.of(context).size.width * 0.037,
                                      ),
                                      recognizer: new TapGestureRecognizer()..onTap = () => _launchURLTrustWalletConf()),
                                  new TextSpan(text: 'Likewise, you can learn  how to use the ERC-20 to  BEP-20 bridge '),
                                  new TextSpan(
                                      text: ' Here ',
                                      style: GoogleFonts.poppins(
                                        color: Colors.blue,
                                        fontWeight: FontWeight.w500,
                                        fontSize: MediaQuery.of(context).size.width * 0.037,
                                      ),
                                      recognizer: new TapGestureRecognizer()..onTap = () => _launchURtHere()),
                                ]),
                          )),
                        ],
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      child: ExpansionTile(
                        title: Text(
                          "How can I validate my KYC?",
                          style: GoogleFonts.poppins(
                            color: Colors.black,
                            fontWeight: FontWeight.w500,
                            fontSize: MediaQuery.of(context).size.width * 0.037,
                          ),
                        ),
                        children: [
                          ListTile(
                              title: RichText(
                            text: TextSpan(
                                style: GoogleFonts.poppins(
                                  color: Colors.black,
                                  fontWeight: FontWeight.w500,
                                  fontSize: MediaQuery.of(context).size.width * 0.037,
                                ),
                                children: [
                                  new TextSpan(
                                      text: 'To validate your KYC, start by logging into your SaTT dashboard. Next, select your profile picture.\nOnce selected, click the ‘legal-KYC’ button.\nNow, you can'
                                          'upload your ID card with the same information that you included during the registration process.\nOnce complete, select the ‘submit’ button.\nAfter'
                                          'submission, the verification process can take anywhere from 2 to 5 days, on average.\nIf everything was sent correctly, your KYC will be validated.\nIf, however,'
                                          'there is a missing document, irrelevant file, or invisible inclusion, you will receive amplifying follow-on information.'),
                                ]),
                          )),
                        ],
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      child: ExpansionTile(
                        title: Text(
                          "What can I purchase using SaTT ?",
                          style: GoogleFonts.poppins(
                            color: Colors.black,
                            fontWeight: FontWeight.w500,
                            fontSize: MediaQuery.of(context).size.width * 0.037,
                          ),
                        ),
                        children: [
                          ListTile(
                              title: RichText(
                            text: TextSpan(
                                style: GoogleFonts.poppins(
                                  color: Colors.black,
                                  fontWeight: FontWeight.w500,
                                  fontSize: MediaQuery.of(context).size.width * 0.037,
                                ),
                                children: [
                                  new TextSpan(text: 'For advertisers seeking accelerated growth on Facebook,'),
                                  new TextSpan(
                                      text: ' Iframe Apps ',
                                      style: GoogleFonts.poppins(
                                        color: Colors.blue,
                                        fontWeight: FontWeight.w500,
                                        fontSize: MediaQuery.of(context).size.width * 0.037,
                                      ),
                                      recognizer: new TapGestureRecognizer()..onTap = () => _launchURtIframe()),
                                  new TextSpan(text: 'offers a series of eight SaTT-eligible apps,enabling access to advertising products and services rooted in a network of vetted partners. '),
                                ]),
                          )),
                        ],
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      child: ExpansionTile(
                        title: Text(
                          "How to contact us ?",
                          style: GoogleFonts.poppins(
                            color: Colors.black,
                            fontWeight: FontWeight.w500,
                            fontSize: MediaQuery.of(context).size.width * 0.037,
                          ),
                        ),
                        children: [
                          ListTile(
                              title: RichText(
                            text: TextSpan(
                                style: GoogleFonts.poppins(
                                  color: Colors.black,
                                  fontWeight: FontWeight.w500,
                                  fontSize: MediaQuery.of(context).size.width * 0.037,
                                ),
                                children: [
                                  new TextSpan(text: 'We love to chat with our community, do not hesitate to contact us\n'),
                                  new TextSpan(text: 'Facebook : '),
                                  new TextSpan(
                                      text: 'https://www.facebook.com/SaTT.Token\n',
                                      style: GoogleFonts.poppins(
                                        color: Colors.blue,
                                        fontWeight: FontWeight.w500,
                                        fontSize: MediaQuery.of(context).size.width * 0.037,
                                      ),
                                      recognizer: new TapGestureRecognizer()..onTap = () => _launchURL('https://www.facebook.com/SaTT.Token')),
                                  new TextSpan(text: 'Twitter : '),
                                  new TextSpan(
                                      text: 'https://twitter.com/SaTT_Token\n',
                                      style: GoogleFonts.poppins(
                                        color: Colors.blue,
                                        fontWeight: FontWeight.w500,
                                        fontSize: MediaQuery.of(context).size.width * 0.037,
                                      ),
                                      recognizer: new TapGestureRecognizer()..onTap = () => _launchURL('https://twitter.com/SaTT_Token')),
                                  new TextSpan(text: 'Telegram : '),
                                  new TextSpan(
                                      text: ' https://t.me/satttoken\n',
                                      style: GoogleFonts.poppins(
                                        color: Colors.blue,
                                        fontWeight: FontWeight.w500,
                                        fontSize: MediaQuery.of(context).size.width * 0.037,
                                      ),
                                      recognizer: new TapGestureRecognizer()..onTap = () => _launchURL(' https://t.me/satttoken')),
                                  new TextSpan(text: 'Discord : '),
                                  new TextSpan(
                                      text: 'https://discord.gg/ut4GfUc\n',
                                      style: GoogleFonts.poppins(
                                        color: Colors.blue,
                                        fontWeight: FontWeight.w500,
                                        fontSize: MediaQuery.of(context).size.width * 0.037,
                                      ),
                                      recognizer: new TapGestureRecognizer()..onTap = () => _launchURL('https://discord.gg/ut4GfUc')),
                                  new TextSpan(text: 'Email : '),
                                  new TextSpan(
                                      text: 'contact@satt-token.com\n',
                                      style: GoogleFonts.poppins(
                                        color: Colors.blue,
                                        fontWeight: FontWeight.w500,
                                        fontSize: MediaQuery.of(context).size.width * 0.037,
                                      ),
                                      recognizer: new TapGestureRecognizer()..onTap = () => _launchURL('contact@satt-token.com')),
                                ]),
                          )),
                        ],
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      child: ExpansionTile(
                        title: Text(
                          "How can I retrieve my SaTT password?",
                          style: GoogleFonts.poppins(
                            color: Colors.black,
                            fontWeight: FontWeight.w500,
                            fontSize: MediaQuery.of(context).size.width * 0.037,
                          ),
                        ),
                        children: [
                          ListTile(
                              title: RichText(
                            text: TextSpan(
                                style: GoogleFonts.poppins(
                                  color: Colors.black,
                                  fontWeight: FontWeight.w500,
                                  fontSize: MediaQuery.of(context).size.width * 0.037,
                                ),
                                children: [
                                  new TextSpan(
                                      text: 'Your wallet’s password is retrievable by simply selecting the ‘forgot password’ link.\n'
                                          'From there, follow the instructions to select a new password.\n'
                                          'If you lose your blockchain password, however, there is no method of retrieval.\n'
                                          'This password enables you to send and receive SaTT.\n'
                                          'As a result, the password is inaccessible, even to our SaTT technical support team.\n'
                                          'We highly recommend writing down your blockchain password in a secure location where you won’t lose it.\n'
                                          'In turn, you can avoid a potentially costly situation.'),
                                ]),
                          )),
                        ],
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      child: ExpansionTile(
                        title: Text(
                          "How can I unblock a blocked account?",
                          style: GoogleFonts.poppins(
                            color: Colors.black,
                            fontWeight: FontWeight.w500,
                            fontSize: MediaQuery.of(context).size.width * 0.037,
                          ),
                        ),
                        children: [
                          ListTile(
                              title: RichText(
                            text: TextSpan(
                                style: GoogleFonts.poppins(
                                  color: Colors.black,
                                  fontWeight: FontWeight.w500,
                                  fontSize: MediaQuery.of(context).size.width * 0.037,
                                ),
                                children: [
                                  new TextSpan(
                                      text: 'The SaTT platform includes security protocols to prevent malicious activity.\n'
                                          'For example, after 3 incorrect password entries, an account will be blocked for 30-minutes.\n'
                                          'After a follow-on incorrect attempt, the 30-minute timer will be reset.'),
                                ]),
                          )),
                        ],
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      child: ExpansionTile(
                        title: Text(
                          "Where can I store my SaTT after\nmy purchase?",
                          style: GoogleFonts.poppins(
                            color: Colors.black,
                            fontWeight: FontWeight.w500,
                            fontSize: MediaQuery.of(context).size.width * 0.037,
                          ),
                        ),
                        children: [
                          ListTile(
                              title: RichText(
                            text: TextSpan(
                                style: GoogleFonts.poppins(
                                  color: Colors.black,
                                  fontWeight: FontWeight.w500,
                                  fontSize: MediaQuery.of(context).size.width * 0.037,
                                ),
                                children: [
                                  new TextSpan(text: 'SaTT can be stored in a variety of cryptocurrency wallets,including our native '),
                                  new TextSpan(
                                      text: ' SaTT Wallet,',
                                      style: GoogleFonts.poppins(
                                        color: Colors.blue,
                                        fontWeight: FontWeight.w500,
                                        fontSize: MediaQuery.of(context).size.width * 0.037,
                                      ),
                                      recognizer: new TapGestureRecognizer()..onTap = () => _launchURtSatt()),
                                  new TextSpan(text: ' Ledger wallet, MetaMask wallet,Ethereum wallet, or Binance Smart Chain wallet.  '),
                                ]),
                          )),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
        helpAccount.isEmpty
            ? Center(
                child: SizedBox(
                  height: MediaQuery.of(context).size.width * 0.037,
                  width: MediaQuery.of(context).size.width * 0.037,
                  child: CircularProgressIndicator(
                    color: Color(0xFF4048FF),
                  ),
                ),
              )
            : SingleChildScrollView(
                child: Column(
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width,
                      child: ExpansionTile(
                        title: Text(
                          "How can I log in without Facebook\nConnect if I deleted my Facebook account?",
                          style: GoogleFonts.poppins(
                            color: Colors.black,
                            fontWeight: FontWeight.w500,
                            fontSize: MediaQuery.of(context).size.width * 0.037,
                          ),
                        ),
                        children: [
                          ListTile(
                              title: RichText(
                            text: TextSpan(
                                style: GoogleFonts.poppins(
                                  color: Colors.black,
                                  fontWeight: FontWeight.w500,
                                  fontSize: MediaQuery.of(context).size.width * 0.037,
                                ),
                                children: [
                                  new TextSpan(text: 'For assistance after a Facebook account deletion,please email contact@satt-token.com and have your account information ready, including the email you use to access Facebook. '),
                                ]),
                          )),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
        helpTransaction.isEmpty
            ? Center(
                child: SizedBox(
                  height: MediaQuery.of(context).size.width * 0.037,
                  width: MediaQuery.of(context).size.width * 0.037,
                  child: CircularProgressIndicator(
                    color: Color(0xFF4048FF),
                  ),
                ),
              )
            : SingleChildScrollView(
                child: Column(
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width,
                      child: ExpansionTile(
                        title: Text(
                          "How can I retrieve my SaTT password?",
                          style: GoogleFonts.poppins(
                            color: Colors.black,
                            fontWeight: FontWeight.w500,
                            fontSize: MediaQuery.of(context).size.width * 0.037,
                          ),
                        ),
                        children: [
                          ListTile(
                              title: RichText(
                            text: TextSpan(
                                style: GoogleFonts.poppins(
                                  color: Colors.black,
                                  fontWeight: FontWeight.w500,
                                  fontSize: MediaQuery.of(context).size.width * 0.037,
                                ),
                                children: [
                                  new TextSpan(
                                      text:
                                          'Your wallet’s password is retrievable by simply selecting the ‘forgot password’ link. From there, follow the instructions to select a new password. If you lose your blockchain password,however, there is no method of retrieval.This password enables you to send and receive SaTT.As a result, the password is inaccessible, even to our SaTTtechnical support team. We highly recommend writing down yourblockchain password in a secure location where you won’t lose it.In turn, you can avoid a potentially costly situation.'),
                                ]),
                          )),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
      ];
    });

    return Scaffold(
        resizeToAvoidBottomInset: true,
        appBar: CustomAppBar(),
        bottomNavigationBar: CustomBottomBar(),
        body: Stack(
          children: [
            SingleChildScrollView(
              controller: scrollController,
              physics: ClampingScrollPhysics(),
              child: Container(
                child: Column(children: [
                  Container(
                    height: MediaQuery.of(context).size.height * 0.15,
                    child: Row(
                      children: [
                        Padding(
                          padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.072, MediaQuery.of(context).size.width * 0.04, MediaQuery.of(context).size.width * 0.002, MediaQuery.of(context).size.width * 0.03),
                          child: Text(
                            "Help",
                            style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.height * 0.032, fontWeight: FontWeight.w700, color: Colors.black),
                          ),
                        ),
                        Spacer(),
                        Padding(
                          padding: EdgeInsets.fromLTRB(0, MediaQuery.of(context).size.width * 0.07, MediaQuery.of(context).size.width * 0.05, MediaQuery.of(context).size.width * 0.06),
                          child: Container(
                            padding: EdgeInsets.all(8),
                            decoration: BoxDecoration(border: Border.all(color: Colors.grey.withOpacity(0.5)), borderRadius: BorderRadius.all(Radius.circular(30.0))),
                            child: InkWell(
                              onTap: () {
                                setState(() {
                                  _isSearch = !_isSearch;
                                  searchFormField = false;
                                  _search.text = "";
                                });
                              },
                              child: Icon(
                                Icons.search,
                                color: Colors.black.withOpacity(0.8),
                                size: MediaQuery.of(context).size.height * 0.04,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  if (_isSearch)
                    Padding(
                      padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.height * 0.03, 0, MediaQuery.of(context).size.height * 0.03, MediaQuery.of(context).size.width * 0.01),
                      child: SingleChildScrollView(
                        child: Container(
                          child: Column(
                            children: [
                              TextFormField(
                                controller: _search,
                                onChanged: (text) {
                                  setState(() {
                                    _search.text = text;
                                  });
                                  _search.selection = TextSelection.fromPosition(TextPosition(offset: _search.text.length));
                                  searchList(text);
                                  if (_search.text.length > 0) {
                                    setState(() {
                                      searchFormField = true;
                                    });
                                  } else {
                                    setState(() {
                                      searchFormField = false;
                                    });
                                  }
                                },
                                validator: (value) {
                                  return null;
                                },
                                autocorrect: false,
                                decoration: new InputDecoration(
                                  hintText: '',
                                  prefixIcon: Padding(
                                      padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.025, right: MediaQuery.of(context).size.width * 0.025),
                                      child: Icon(
                                        Icons.search,
                                        size: 35,
                                        color: Colors.black,
                                      )),
                                  fillColor: Colors.grey,
                                  focusColor: Colors.grey,
                                  hintStyle: GoogleFonts.poppins(fontWeight: FontWeight.normal, color: Colors.grey, fontSize: MediaQuery.of(context).size.width * 0.05),
                                  contentPadding: new EdgeInsets.symmetric(vertical: 0.0, horizontal: MediaQuery.of(context).size.width * 0.06),
                                  enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.grey.withOpacity(0.3))),
                                  focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.grey)),
                                  errorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red)),
                                ),
                              ),
                              searchFormField
                                  ? Visibility(
                                      child: newList.isNotEmpty
                                          ? SingleChildScrollView(
                                              child: Container(
                                                child: ListView.builder(
                                                  physics: const NeverScrollableScrollPhysics(),
                                                  shrinkWrap: true,
                                                  scrollDirection: Axis.vertical,
                                                  itemBuilder: (context, index) {
                                                    return Column(
                                                      children: [
                                                        ListTile(
                                                          title: RichText(
                                                            text: TextSpan(children: [
                                                              TextSpan(
                                                                text: newList[index].question.toString().substring(0, newList[index].question.toString().toLowerCase().indexOf(_search.text.toLowerCase())),
                                                                style: GoogleFonts.poppins(
                                                                  color: Colors.grey,
                                                                  fontSize: 16,
                                                                ),
                                                              ),
                                                              TextSpan(
                                                                text: _search.text,
                                                                style: GoogleFonts.poppins(
                                                                  color: Colors.black,
                                                                  fontSize: 16,
                                                                ),
                                                              ),
                                                              TextSpan(
                                                                text: newList[index].question.toString().substring(newList[index].question.toString().toLowerCase().indexOf(_search.text.toLowerCase()) + _search.text.length),
                                                                style: GoogleFonts.poppins(
                                                                  color: Colors.grey,
                                                                  fontSize: 16,
                                                                ),
                                                              )
                                                            ]),
                                                          ),
                                                          onTap: () {
                                                            setState(() {
                                                              isClick = !isClick;
                                                              newListClik = newList[index].question ?? "";
                                                            });
                                                          },
                                                        ),
                                                        if (newListClik == newList[index].question && isClick)
                                                          Column(
                                                            children: [
                                                              Row(
                                                                children: [
                                                                  Expanded(
                                                                    child: Column(
                                                                      children: [
                                                                        Text(
                                                                          newList[index].answer.toString(),
                                                                          textAlign: TextAlign.center,
                                                                          style: GoogleFonts.poppins(fontSize: 16, fontWeight: FontWeight.normal, color: Colors.black),
                                                                        ),
                                                                      ],
                                                                    ),
                                                                  ),
                                                                ],
                                                              ),
                                                            ],
                                                          ),
                                                      ],
                                                    );
                                                  },
                                                  itemCount: newList.length,
                                                ),
                                              ),
                                            )
                                          : SingleChildScrollView(
                                              child: Container(
                                                height: MediaQuery.of(context).size.height,
                                                child: Column(
                                                  children: [
                                                    Padding(
                                                      padding: EdgeInsets.all(MediaQuery.of(context).size.height * 0.04),
                                                      child: SvgPicture.asset("images/no.svg"),
                                                    ),
                                                    Text(
                                                      "Sorry,",
                                                      style: GoogleFonts.poppins(
                                                        fontSize: MediaQuery.of(context).size.width * 0.039,
                                                        fontWeight: FontWeight.w600,
                                                        color: Colors.black,
                                                      ),
                                                    ),
                                                    Text(
                                                      " no result found",
                                                      style: GoogleFonts.poppins(
                                                        fontSize: MediaQuery.of(context).size.width * 0.039,
                                                        fontWeight: FontWeight.w600,
                                                        color: Colors.black,
                                                      ),
                                                    ),
                                                    Padding(
                                                      padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.06, MediaQuery.of(context).size.width * 0.09, MediaQuery.of(context).size.width * 0.06, MediaQuery.of(context).size.width * 0.01),
                                                      child: Container(
                                                        width: MediaQuery.of(context).size.width * 0.65,
                                                        height: MediaQuery.of(context).size.height * 0.06,
                                                        child: ElevatedButton(
                                                          style: ElevatedButton.styleFrom(
                                                              minimumSize: Size(260.0, 40.0),
                                                              side: BorderSide(color: Colors.white),
                                                              primary: Colors.white,
                                                              shape: RoundedRectangleBorder(
                                                                borderRadius: BorderRadius.circular(50.0),
                                                              )),
                                                          child: Text(
                                                            "Back to FAQ",
                                                            style: GoogleFonts.poppins(fontWeight: FontWeight.w600, fontSize: MediaQuery.of(context).size.width * 0.05, color: Color(0xFF4048FF)),
                                                          ),
                                                          onPressed: () {
                                                            setState(() {
                                                              _isSearch = false;
                                                            });
                                                          },
                                                        ),
                                                      ),
                                                    ),
                                                    Padding(
                                                      padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.06, MediaQuery.of(context).size.width * 0.01, MediaQuery.of(context).size.width * 0.06, MediaQuery.of(context).size.width * 0.01),
                                                      child: Container(
                                                        width: MediaQuery.of(context).size.width * 0.65,
                                                        height: MediaQuery.of(context).size.height * 0.06,
                                                        child: ElevatedButton(
                                                            style: ElevatedButton.styleFrom(
                                                                minimumSize: Size(260.0, 40.0),
                                                                side: BorderSide(color: Colors.white),
                                                                primary: Colors.white,
                                                                shape: RoundedRectangleBorder(
                                                                  borderRadius: BorderRadius.circular(50.0),
                                                                )),
                                                            child: Text(
                                                              "Contact us",
                                                              style: GoogleFonts.poppins(fontWeight: FontWeight.w600, fontSize: MediaQuery.of(context).size.width * 0.05, color: Color(0xFF4048FF)),
                                                            ),
                                                            onPressed: () {
                                                              scrollController.animateTo(MediaQuery.of(context).size.height * 0.7,
                                                                  duration: Duration(milliseconds: 500), //duration of scroll
                                                                  curve: Curves.fastOutSlowIn);
                                                              setState(() {
                                                                _isSearch = false;
                                                              });
                                                            }),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ))
                                  : Padding(padding: EdgeInsets.all(0.0)),
                            ],
                          ),
                        ),
                      ),
                    ),
                  if (_isSearch && _search.text == "")
                    Column(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.height * 0.002, MediaQuery.of(context).size.height * 0.02, MediaQuery.of(context).size.height * 0.03, MediaQuery.of(context).size.height * 0.001),
                          child: Text(
                            "Frequently Asked Questions",
                            textAlign: TextAlign.center,
                            style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.05, fontWeight: FontWeight.w700, color: Colors.black),
                          ),
                        ),
                        Padding(
                            padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.height * 0.01, MediaQuery.of(context).size.width * 0.05, MediaQuery.of(context).size.height * 0.01, MediaQuery.of(context).size.width * 0.05),
                            child: Row(
                              children: [
                                Spacer(),
                                InkWell(
                                  borderRadius: BorderRadius.all(Radius.circular(30.0)),
                                  onTap: () {
                                    setState(() {
                                      _isclickedaccount = false;
                                      _isclickedprivacy = false;
                                      _isclicked = false;
                                    });
                                    _onItemTapped(0);
                                  },
                                  child: Container(
                                    padding: EdgeInsets.all(8),
                                    decoration: BoxDecoration(border: Border.all(color: Colors.grey.withOpacity(0.5)), borderRadius: BorderRadius.all(Radius.circular(30.0))),
                                    child: Padding(
                                        padding: EdgeInsets.fromLTRB(15, 7, 15, 7),
                                        child: Text(
                                          "Wallet",
                                          textAlign: TextAlign.center,
                                          style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.04, fontWeight: FontWeight.w500, color: Colors.black),
                                        )),
                                  ),
                                ),
                                Spacer(),
                                InkWell(
                                  borderRadius: BorderRadius.all(Radius.circular(30.0)),
                                  onTap: () {
                                    setState(() {
                                      _isclickedaccount = false;
                                      _isclickedprivacy = false;
                                      _isclicked = false;
                                    });
                                    _onItemTapped(1);
                                  },
                                  child: Container(
                                    padding: EdgeInsets.all(8),
                                    decoration: BoxDecoration(border: Border.all(color: Colors.grey.withOpacity(0.5)), borderRadius: BorderRadius.all(Radius.circular(30.0))),
                                    child: Padding(
                                      padding: const EdgeInsets.all(6.0),
                                      child: Text(
                                        "Account",
                                        textAlign: TextAlign.center,
                                        style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.04, fontWeight: FontWeight.w500, color: Colors.black),
                                      ),
                                    ),
                                  ),
                                ),
                                Spacer(),
                                InkWell(
                                  borderRadius: BorderRadius.all(Radius.circular(30.0)),
                                  onTap: () {
                                    setState(() {
                                      _isclickedaccount = false;
                                      _isclickedprivacy = false;
                                      _isclicked = false;
                                    });
                                    _onItemTapped(2);
                                  },
                                  child: Container(
                                    padding: EdgeInsets.all(8),
                                    decoration: BoxDecoration(border: Border.all(color: Colors.grey.withOpacity(0.5)), borderRadius: BorderRadius.all(Radius.circular(30.0))),
                                    child: Padding(
                                        padding: const EdgeInsets.all(6.0),
                                        child: Text(
                                          "Security",
                                          textAlign: TextAlign.center,
                                          style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.04, fontWeight: FontWeight.w500, color: Colors.black),
                                        )),
                                  ),
                                ),
                                Spacer(),
                              ],
                            )),
                        Padding(
                          padding: EdgeInsets.all(0.0),
                          child: Container(
                            width: MediaQuery.of(context).size.width * 0.9,
                            height: MediaQuery.of(context).size.height * 0.5,
                            child: _widgetOptions.elementAt(_selectedIndex),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.1, top: MediaQuery.of(context).size.height * 0.04, right: MediaQuery.of(context).size.height * 0.08),
                          child: Text(
                            "Unanswered questions ?\nThat’s what we’re here to answer.",
                            textAlign: TextAlign.left,
                            style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.05, fontWeight: FontWeight.w700, color: Colors.black),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.all(MediaQuery.of(context).size.height * 0.01),
                          child: Form(
                            key: _formkey,
                            child: Column(
                              children: [
                                Padding(
                                  padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.13, MediaQuery.of(context).size.width * 0.1, MediaQuery.of(context).size.width * 0.49, MediaQuery.of(context).size.height * 0.005),
                                  child: Align(
                                    alignment: Alignment.topLeft,
                                    child: Text("YOUR NAME ", style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.035, fontWeight: FontWeight.w600, color: Colors.grey)),
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.08, 0, MediaQuery.of(context).size.width * 0.08, MediaQuery.of(context).size.height * 0.02),
                                  child: TextFormField(
                                    controller: name,
                                    style: TextStyle(fontSize: 18, color: Colors.black, fontWeight: FontWeight.w500),
                                    validator: (value) {
                                      if (value!.isEmpty) {
                                        return 'Field required';
                                      }
                                      return null;
                                    },
                                    decoration: InputDecoration(
                                        fillColor: Colors.white,
                                        filled: true,
                                        isDense: true,
                                        contentPadding: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width * 0.037, vertical: MediaQuery.of(context).size.width * 0.033),
                                        enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.grey)),
                                        focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.grey)),
                                        errorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red)),
                                        focusedErrorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red))),
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.13, 0, 0, MediaQuery.of(context).size.height * 0.005),
                                  child: Align(
                                    alignment: Alignment.topLeft,
                                    child: Text("YOUR MAIL ", style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.035, fontWeight: FontWeight.w600, color: Colors.grey)),
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.08, 0, MediaQuery.of(context).size.width * 0.08, MediaQuery.of(context).size.height * 0.02),
                                  child: TextFormField(
                                    controller: email,
                                    style: TextStyle(fontSize: 18, color: Colors.black, fontWeight: FontWeight.w500),
                                    onChanged: (value) {
                                      TextSelection previousSelection = email.selection;
                                      email.text = value;

                                      email.selection = previousSelection;

                                      TextSelection.fromPosition(TextPosition(offset: email.text.length));
                                      if (email.text.length != 0) {
                                        if (value.contains('@')) {
                                          setState(() {
                                            _isvalidFormEmail = true;
                                          });
                                        } else {
                                          setState(() {
                                            _isvalidFormEmail = false;
                                          });
                                        }
                                      }
                                    },
                                    validator: (value) {
                                      if (value!.isEmpty) {
                                        return 'Field required';
                                      } else if (RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(value)) {
                                        return null;
                                      } else {
                                        return 'Enter valid email';
                                      }
                                    },
                                    decoration: InputDecoration(
                                        fillColor: Colors.white,
                                        filled: true,
                                        isDense: true,
                                        contentPadding: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width * 0.037, vertical: MediaQuery.of(context).size.width * 0.033),
                                        enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.grey)),
                                        focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.grey)),
                                        errorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red)),
                                        focusedErrorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red))),
                                  ),
                                ),
                                Visibility(
                                  visible: (email.text.length > 0),
                                  child: Padding(
                                    padding: !_isvalidFormEmail ? EdgeInsets.all(2.0) : EdgeInsets.all(0.0),
                                    child: !_isvalidFormEmail
                                        ? Text(
                                            'Field Email Address must have a valid form',
                                            style: GoogleFonts.poppins(color: Colors.grey, fontSize: 13, fontWeight: FontWeight.normal),
                                          )
                                        : null,
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.13, 0, 0, MediaQuery.of(context).size.height * 0.005),
                                  child: Align(
                                    alignment: Alignment.topLeft,
                                    child: Text("OBJECT ", style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.035, fontWeight: FontWeight.w600, color: Colors.grey)),
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.08, 0, MediaQuery.of(context).size.width * 0.08, MediaQuery.of(context).size.height * 0.02),
                                  child: TextFormField(
                                    controller: object,
                                    validator: (value) {
                                      if (value!.isEmpty) {
                                        return 'Field required';
                                      }
                                    },
                                    decoration: InputDecoration(
                                        fillColor: Colors.white,
                                        filled: true,
                                        isDense: true,
                                        contentPadding: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width * 0.037, vertical: MediaQuery.of(context).size.width * 0.033),
                                        enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.grey)),
                                        focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.grey)),
                                        errorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red)),
                                        focusedErrorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red))),
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.13, 0, 0, MediaQuery.of(context).size.width * 0.02),
                                  child: Align(
                                    alignment: Alignment.topLeft,
                                    child: Text("MESSAGE ", style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.035, fontWeight: FontWeight.w600, color: Colors.grey)),
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.081, 0, MediaQuery.of(context).size.width * 0.081, 0),
                                  child: SizedBox(
                                    child: TextField(
                                      controller: message,
                                      textInputAction: TextInputAction.done,
                                      minLines: 5,
                                      maxLines: 7,
                                      style: TextStyle(fontSize: 18.0, color: Colors.black),
                                      decoration: InputDecoration(
                                        errorText: _validate ? 'Field required' : null,
                                        fillColor: Colors.white,
                                        filled: true,
                                        isDense: true,
                                        contentPadding: EdgeInsets.symmetric(
                                          vertical: MediaQuery.of(context).size.height * 0.03,
                                          horizontal: MediaQuery.of(context).size.height * 0.02,
                                        ),
                                        enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(40), borderSide: BorderSide(color: Colors.grey)),
                                        focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(40), borderSide: BorderSide(color: Colors.grey)),
                                      ),
                                    ),
                                  ),
                                ),
                                Padding(
                                    padding: EdgeInsets.fromLTRB(
                                      MediaQuery.of(context).size.height * 0.003,
                                      MediaQuery.of(context).size.height * 0.025,
                                      MediaQuery.of(context).size.height * 0.001,
                                      0,
                                    ),
                                    child: Container(
                                      height: MediaQuery.of(context).size.height * 0.07,
                                      width: MediaQuery.of(context).size.width * 0.8,
                                      child: ElevatedButton(
                                        style: ButtonStyle(
                                          backgroundColor: MaterialStateProperty.all(
                                            Color(0xFF4048FF),
                                          ),
                                          shape: MaterialStateProperty.all(
                                            RoundedRectangleBorder(
                                              borderRadius: BorderRadius.circular(30),
                                            ),
                                          ),
                                        ),
                                        onPressed: () async {
                                          if (_formkey.currentState!.validate()) {
                                            var sendForm = await LoginController.apiService.sendHelp(Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "", email.text, message.text, name.text, object.text);
                                            isSend = (sendForm == "success");
                                            isSend
                                                ? showDialog(
                                                    context: context,
                                                    builder: (context) {
                                                      return AlertDialog(
                                                          insetPadding: EdgeInsets.zero,
                                                          shape: RoundedRectangleBorder(
                                                            borderRadius: BorderRadius.all(Radius.circular(30)),
                                                          ),
                                                          title: Column(
                                                            children: [
                                                              Padding(
                                                                padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.6, top: MediaQuery.of(context).size.width * 0.012),
                                                                child: IconButton(
                                                                    alignment: Alignment.topLeft,
                                                                    onPressed: () {
                                                                      setState(() {
                                                                        name.text = "";
                                                                        email.text = "";
                                                                        object.text = "";
                                                                        message.text = "";
                                                                      });
                                                                      Navigator.of(context).pop();
                                                                    },
                                                                    icon: Icon(
                                                                      Icons.close,
                                                                      color: Colors.black54,
                                                                      size: 43,
                                                                    )),
                                                              ),
                                                              SvgPicture.asset(
                                                                "images/success_info.svg",
                                                                height: 120,
                                                                width: 120,
                                                              ),
                                                            ],
                                                          ),
                                                          content: Container(
                                                            width: MediaQuery.of(context).size.width * 0.7,
                                                            height: MediaQuery.of(context).size.height * 0.15,
                                                            child: Column(
                                                              children: [
                                                                Text(
                                                                  "Success",
                                                                  textAlign: TextAlign.center,
                                                                  style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.047, fontWeight: FontWeight.w700, color: Colors.black),
                                                                ),
                                                                Text(
                                                                  "Your message has been \n sent successfully",
                                                                  textAlign: TextAlign.center,
                                                                  style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.04, fontWeight: FontWeight.normal, color: Colors.black),
                                                                ),
                                                              ],
                                                            ),
                                                          ));
                                                    })
                                                : Fluttertoast();
                                          }
                                        },
                                        child: Text("Send", style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.04, fontWeight: FontWeight.w700, color: Colors.white)),
                                      ),
                                    ))
                              ],
                            ),
                          ),
                        ),
                        SizedBox(
                          height: MediaQuery.of(context).size.height * 0.1,
                        )
                      ],
                    ),
                  if (!_isSearch)
                    Column(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.height * 0.002, MediaQuery.of(context).size.height * 0.02, MediaQuery.of(context).size.height * 0.03, MediaQuery.of(context).size.height * 0.001),
                          child: Text(
                            "Frequently Asked Questions",
                            textAlign: TextAlign.center,
                            style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.05, fontWeight: FontWeight.w700, color: Colors.black),
                          ),
                        ),
                        Padding(
                            padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.height * 0.01, MediaQuery.of(context).size.width * 0.05, MediaQuery.of(context).size.height * 0.01, MediaQuery.of(context).size.width * 0.05),
                            child: Row(
                              children: [
                                Spacer(),
                                InkWell(
                                  borderRadius: BorderRadius.all(Radius.circular(30.0)),
                                  onTap: () {
                                    setState(() {
                                      _isclickedaccount = false;
                                      _isclickedprivacy = false;
                                      _isclicked = false;
                                    });
                                    _onItemTapped(0);
                                  },
                                  child: Container(
                                    padding: EdgeInsets.all(8),
                                    decoration: BoxDecoration(border: Border.all(color: Colors.grey.withOpacity(0.5)), borderRadius: BorderRadius.all(Radius.circular(30.0))),
                                    child: Padding(
                                        padding: EdgeInsets.fromLTRB(15, 7, 15, 7),
                                        child: Text(
                                          "Wallet",
                                          textAlign: TextAlign.center,
                                          style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.04, fontWeight: FontWeight.w500, color: Colors.black),
                                        )),
                                  ),
                                ),
                                Spacer(),
                                InkWell(
                                  borderRadius: BorderRadius.all(Radius.circular(30.0)),
                                  onTap: () {
                                    setState(() {
                                      _isclickedaccount = false;
                                      _isclickedprivacy = false;
                                      _isclicked = false;
                                    });
                                    _onItemTapped(1);
                                  },
                                  child: Container(
                                    padding: EdgeInsets.all(8),
                                    decoration: BoxDecoration(border: Border.all(color: Colors.grey.withOpacity(0.5)), borderRadius: BorderRadius.all(Radius.circular(30.0))),
                                    child: Padding(
                                      padding: const EdgeInsets.all(6.0),
                                      child: Text(
                                        "Account",
                                        textAlign: TextAlign.center,
                                        style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.04, fontWeight: FontWeight.w500, color: Colors.black),
                                      ),
                                    ),
                                  ),
                                ),
                                Spacer(),
                                InkWell(
                                  borderRadius: BorderRadius.all(Radius.circular(30.0)),
                                  onTap: () {
                                    setState(() {
                                      _isclickedaccount = false;
                                      _isclickedprivacy = false;
                                      _isclicked = false;
                                    });
                                    _onItemTapped(2);
                                  },
                                  child: Container(
                                    padding: EdgeInsets.all(8),
                                    decoration: BoxDecoration(border: Border.all(color: Colors.grey.withOpacity(0.5)), borderRadius: BorderRadius.all(Radius.circular(30.0))),
                                    child: Padding(
                                        padding: const EdgeInsets.all(6.0),
                                        child: Text(
                                          "Security",
                                          textAlign: TextAlign.center,
                                          style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.04, fontWeight: FontWeight.w500, color: Colors.black),
                                        )),
                                  ),
                                ),
                                Spacer(),
                              ],
                            )),
                        Padding(
                          padding: EdgeInsets.all(0.0),
                          child: Container(
                            width: MediaQuery.of(context).size.width * 0.9,
                            height: MediaQuery.of(context).size.height * 0.5,
                            child: _widgetOptions.elementAt(_selectedIndex),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.1, top: MediaQuery.of(context).size.height * 0.04, right: MediaQuery.of(context).size.height * 0.08),
                          child: Text(
                            "Unanswered questions ?\nThat’s what we’re here to answer.",
                            textAlign: TextAlign.left,
                            style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.05, fontWeight: FontWeight.w700, color: Colors.black),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.all(MediaQuery.of(context).size.height * 0.01),
                          child: Form(
                            key: _formkey,
                            child: Column(
                              children: [
                                Padding(
                                  padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.13, MediaQuery.of(context).size.width * 0.1, MediaQuery.of(context).size.width * 0.49, MediaQuery.of(context).size.height * 0.005),
                                  child: Align(
                                    alignment: Alignment.topLeft,
                                    child: Text("YOUR NAME ", style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.035, fontWeight: FontWeight.w600, color: Colors.grey)),
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.08, 0, MediaQuery.of(context).size.width * 0.08, MediaQuery.of(context).size.height * 0.02),
                                  child: TextFormField(
                                    controller: name,
                                    style: TextStyle(fontSize: 18, color: Colors.grey, fontWeight: FontWeight.w500),
                                    validator: (value) {
                                      if (value!.isEmpty) {
                                        return 'Field required';
                                      }
                                      return null;
                                    },
                                    decoration: InputDecoration(
                                        fillColor: Colors.white,
                                        filled: true,
                                        isDense: true,
                                        contentPadding: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width * 0.037, vertical: MediaQuery.of(context).size.width * 0.033),
                                        enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.grey)),
                                        focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.grey)),
                                        errorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red)),
                                        focusedErrorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red))),
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.13, 0, 0, MediaQuery.of(context).size.height * 0.005),
                                  child: Align(
                                    alignment: Alignment.topLeft,
                                    child: Text("YOUR MAIL ", style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.035, fontWeight: FontWeight.w600, color: Colors.grey)),
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.08, 0, MediaQuery.of(context).size.width * 0.08, MediaQuery.of(context).size.height * 0.02),
                                  child: TextFormField(
                                    controller: email,
                                    style: TextStyle(fontSize: 18, color: Colors.grey, fontWeight: FontWeight.w500),
                                    onChanged: (value) {
                                      TextSelection previousSelection = email.selection;
                                      email.text = value;

                                      email.selection = previousSelection;

                                      TextSelection.fromPosition(TextPosition(offset: email.text.length));
                                      if (email.text.length != 0) {
                                        if (value.contains('@')) {
                                          setState(() {
                                            _isvalidFormEmail = true;
                                          });
                                        } else {
                                          setState(() {
                                            _isvalidFormEmail = false;
                                          });
                                        }
                                      }
                                    },
                                    validator: (value) {
                                      if (value!.isEmpty) {
                                        return 'Field required';
                                      } else if (RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(value)) {
                                        return null;
                                      } else {
                                        return 'Enter valid email';
                                      }
                                    },
                                    decoration: InputDecoration(
                                        fillColor: Colors.white,
                                        filled: true,
                                        isDense: true,
                                        contentPadding: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width * 0.037, vertical: MediaQuery.of(context).size.width * 0.033),
                                        enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.grey)),
                                        focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.grey)),
                                        errorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red)),
                                        focusedErrorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red))),
                                  ),
                                ),
                                Visibility(
                                  visible: (email.text.length > 0),
                                  child: Padding(
                                    padding: !_isvalidFormEmail ? EdgeInsets.all(2.0) : EdgeInsets.all(0.0),
                                    child: !_isvalidFormEmail
                                        ? Text(
                                            'Field Email Address must have a valid form',
                                            style: GoogleFonts.poppins(color: Colors.grey, fontSize: 13, fontWeight: FontWeight.normal),
                                          )
                                        : null,
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.13, 0, 0, MediaQuery.of(context).size.height * 0.005),
                                  child: Align(
                                    alignment: Alignment.topLeft,
                                    child: Text("OBJECT ", style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.035, fontWeight: FontWeight.w600, color: Colors.grey)),
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.08, 0, MediaQuery.of(context).size.width * 0.08, MediaQuery.of(context).size.height * 0.02),
                                  child: TextFormField(
                                    controller: object,
                                    validator: (value) {
                                      if (value!.isEmpty) {
                                        return 'Field required';
                                      }
                                    },
                                    decoration: InputDecoration(
                                        fillColor: Colors.white,
                                        filled: true,
                                        isDense: true,
                                        contentPadding: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width * 0.037, vertical: MediaQuery.of(context).size.width * 0.033),
                                        enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.grey)),
                                        focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.grey)),
                                        errorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red)),
                                        focusedErrorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red))),
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.13, 0, 0, MediaQuery.of(context).size.width * 0.02),
                                  child: Align(
                                    alignment: Alignment.topLeft,
                                    child: Text("MESSAGE ", style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.035, fontWeight: FontWeight.w600, color: Colors.grey)),
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.081, 0, MediaQuery.of(context).size.width * 0.081, 0),
                                  child: SizedBox(
                                    child: TextField(
                                      controller: message,
                                      textInputAction: TextInputAction.done,
                                      minLines: 5,
                                      maxLines: 7,
                                      style: TextStyle(fontSize: 18.0, color: Colors.black),
                                      decoration: InputDecoration(
                                        errorText: _validate ? 'Field required' : null,
                                        fillColor: Colors.white,
                                        filled: true,
                                        isDense: true,
                                        contentPadding: EdgeInsets.symmetric(
                                          vertical: MediaQuery.of(context).size.height * 0.03,
                                          horizontal: MediaQuery.of(context).size.height * 0.02,
                                        ),
                                        enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(40), borderSide: BorderSide(color: Colors.grey)),
                                        focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(40), borderSide: BorderSide(color: Colors.grey)),
                                      ),
                                    ),
                                  ),
                                ),
                                Padding(
                                    padding: EdgeInsets.fromLTRB(
                                      MediaQuery.of(context).size.height * 0.003,
                                      MediaQuery.of(context).size.height * 0.025,
                                      MediaQuery.of(context).size.height * 0.001,
                                      0,
                                    ),
                                    child: Container(
                                      height: MediaQuery.of(context).size.height * 0.07,
                                      width: MediaQuery.of(context).size.width * 0.8,
                                      child: ElevatedButton(
                                        style: ButtonStyle(
                                          backgroundColor: MaterialStateProperty.all(
                                            Color(0xFF4048FF),
                                          ),
                                          shape: MaterialStateProperty.all(
                                            RoundedRectangleBorder(
                                              borderRadius: BorderRadius.circular(30),
                                            ),
                                          ),
                                        ),
                                        onPressed: () async {
                                          if (_formkey.currentState!.validate()) {
                                            var sendForm = await LoginController.apiService.sendHelp(Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "", email.text, message.text, name.text, object.text);
                                            isSend = (sendForm == "success");
                                            isSend
                                                ? showDialog(
                                                    context: context,
                                                    builder: (context) {
                                                      return AlertDialog(
                                                          insetPadding: EdgeInsets.zero,
                                                          shape: RoundedRectangleBorder(
                                                            borderRadius: BorderRadius.all(Radius.circular(30)),
                                                          ),
                                                          title: Column(
                                                            children: [
                                                              Padding(
                                                                padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.6, top: MediaQuery.of(context).size.width * 0.012),
                                                                child: IconButton(
                                                                    alignment: Alignment.topLeft,
                                                                    onPressed: () {
                                                                      setState(() {
                                                                        name.text = "";
                                                                        email.text = "";
                                                                        object.text = "";
                                                                        message.text = "";
                                                                      });
                                                                      Navigator.of(context).pop();
                                                                    },
                                                                    icon: Icon(
                                                                      Icons.close,
                                                                      color: Colors.black54,
                                                                      size: 43,
                                                                    )),
                                                              ),
                                                              SvgPicture.asset(
                                                                "images/success_info.svg",
                                                                height: 120,
                                                                width: 120,
                                                              ),
                                                            ],
                                                          ),
                                                          content: Container(
                                                            width: MediaQuery.of(context).size.width * 0.7,
                                                            height: MediaQuery.of(context).size.height * 0.15,
                                                            child: Column(
                                                              children: [
                                                                Text(
                                                                  "Success",
                                                                  textAlign: TextAlign.center,
                                                                  style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.047, fontWeight: FontWeight.w700, color: Colors.black),
                                                                ),
                                                                Text(
                                                                  "Your message has been \n sent successfully",
                                                                  textAlign: TextAlign.center,
                                                                  style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.04, fontWeight: FontWeight.normal, color: Colors.black),
                                                                ),
                                                              ],
                                                            ),
                                                          ));
                                                    })
                                                : Fluttertoast();
                                          }
                                        },
                                        child: Text("Send", style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.04, fontWeight: FontWeight.w700, color: Colors.white)),
                                      ),
                                    ))
                              ],
                            ),
                          ),
                        ),
                        SizedBox(
                          height: MediaQuery.of(context).size.height * 0.1,
                        )
                      ],
                    ),
                ]),
              ),
            ),
            HeaderNavigation(_isVisible, _isWalletVisible, _isNotificationVisible)
          ],
        ));
  }

  searchList(String text) {
    setState(() {
      if (text != null && text != "") {
        List<Helps> list = [helpAccount, helpWallet, helpTransaction].expand((x) => x).toList();
        newList = list.where((e) => e.question!.toLowerCase().contains(text.toLowerCase())).toList();
        newList.toSet().toList();
      }
    });
  }
}

class Style extends StyleHook {
  @override
  double get activeIconSize => OneContext().mediaQuery.size.width * 0.19;

  @override
  double get activeIconMargin => OneContext().mediaQuery.size.width * 0.02;

  @override
  double get iconSize => OneContext().mediaQuery.size.width * 0.12;

  @override
  TextStyle textStyle(Color color, String? s) {
    return TextStyle(fontSize: 0, color: color);
  }
}