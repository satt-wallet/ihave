import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:ihave/controllers/LoginController.dart';
import 'package:ihave/controllers/walletController.dart';
import 'package:ihave/model/contact.dart';
import 'package:ihave/model/crypto.dart';
import 'package:ihave/ui/AdressBook.dart';
import 'package:ihave/ui/Profilepage.dart';
import 'package:ihave/ui/WebViewWidget.dart';
import 'package:ihave/ui/WelcomeScreen.dart';
import 'package:ihave/ui/authMethods.dart';
import 'package:ihave/ui/biometrics.dart';
import 'package:ihave/ui/change_password.dart';
import 'package:ihave/ui/desactivate_account.dart';
import 'package:ihave/ui/exportKeyBtcEth.dart';
import 'package:ihave/ui/help.dart';
import 'package:ihave/ui/kyc.dart';
import 'package:ihave/ui/revoke.dart';
import 'package:ihave/ui/security.dart';
import 'package:ihave/ui/signin.dart';
import 'package:ihave/ui/walletConnect.dart';
import 'package:ihave/util/utils.dart';
import 'package:ihave/widgets/customAppBar.dart';
import 'package:provider/provider.dart';
import 'package:share_plus/share_plus.dart';
import 'package:package_info_plus/package_info_plus.dart';

class Setting extends StatefulWidget {
  const Setting({Key? key}) : super(key: key);

  @override
  _SettingState createState() => _SettingState();
}

class _SettingState extends State<Setting> {
  bool _isLoadingUserPicture = false;
  Uint8List? userPicture;
  List<Crypto> listCrypto = [];
  bool _isClick = false;

  bool _userPictureExist = false;
  bool _userPictureSocialExist = false;
  String userPictureSocial = "";
  String appName = "";
  String packageName = "";
  String version = "";
  String buildNumber = "";

  Future getUser() async {
    var user = await Provider.of<LoginController>(context, listen: false).getUserDetails(Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "");
    return user;
  }

  getUserPicture() async {
    setState(() {
      _isLoadingUserPicture = true;
    });

    if (Provider.of<LoginController>(context, listen: false).userPicture == null) {
      var pic = await LoginController.apiService.getUserPicture(Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "");
      Provider.of<LoginController>(context, listen: false).userPicture = pic != "error" ? pic : null;
      var picture = Provider.of<LoginController>(context, listen: false).userPicture;
      if (picture.toString().length > 112 && picture != null) {
        setState(() {
          userPicture = picture;
          userPictureSocial = "";
          _userPictureSocialExist = false;
          _userPictureExist = true;
        });
      } else {
        getUser().then((value) {
          if (value.picLink != null && value.picLink != "" && value.picLink != "AppleIDAuthorizationScopes.fullName") {
            setState(() {
              userPictureSocial = value.picLink;
              _userPictureExist = false;
              _userPictureSocialExist = true;
            });
          } else {
            setState(() {
              _userPictureExist = false;
              _userPictureSocialExist = false;
            });
          }
        });
        setState(() {
          _isLoadingUserPicture = false;
        });
      }
    } else {
      var picture = Provider.of<LoginController>(context, listen: false).userPicture;
      if (picture.toString().length > 112 && picture != null) {
        setState(() {
          userPicture = picture;
          userPictureSocial = "";
          _userPictureSocialExist = false;
          _userPictureExist = true;
        });
      } else {
        getUser().then((value) {
          if (value.picLink != null && value.picLink != "") {
            setState(() {
              userPictureSocial = value.picLink;
              _userPictureExist = false;
              _userPictureSocialExist = true;
            });
          } else {
            setState(() {
              _userPictureExist = false;
              _userPictureSocialExist = false;
            });
          }
        });
      }
    }
    setState(() {
      _isLoadingUserPicture = false;
    });
  }

  @override
  void initState() {
    super.initState();

    PackageInfo.fromPlatform().then((PackageInfo packageInfo) {
      setState(() {
        appName = packageInfo.appName;
        packageName = packageInfo.packageName;
        version = packageInfo.version;
        buildNumber = packageInfo.buildNumber;
      });
    });
    getUserPicture();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: 0,
          leadingWidth: 70,
          backgroundColor: Colors.transparent,
          flexibleSpace: Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    colors: [
                      Color(0xFF707070).withOpacity(0.6),
                      Colors.transparent,
                    ],
                    begin: const FractionalOffset(0.0, 0.0),
                    end: const FractionalOffset(0.0, 1.0),
                    stops: [0.0, 1.0],
                    tileMode: TileMode.clamp)),
          ),
          leading: Padding(
            padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.04, 0, 0, 0),
            child: InkWell(
              focusColor: Colors.transparent,
              hoverColor: Colors.transparent,
              splashColor: Colors.transparent,
              highlightColor: Colors.transparent,
              splashFactory: NoSplash.splashFactory,
              child: Container(
                height: 60,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                ),
                child: CircleAvatar(
                  backgroundColor: Colors.transparent,
                  radius: 30,
                  child: Padding(
                      padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                      child: Transform.scale(
                        scale: 1,
                        child: SvgPicture.asset(
                          'images/logo-white.svg',
                        ),
                      )),
                ),
              ),
              onTap: () {},
            ),
          ),
        ),
        body: SingleChildScrollView(
          child: Container(
            child: Column(
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.04),
                  child: Align(
                    alignment: Alignment.topRight,
                    child: InkWell(
                      focusColor: Colors.transparent,
                      hoverColor: Colors.transparent,
                      splashColor: Colors.transparent,
                      highlightColor: Colors.transparent,
                      onTap: () {
                        Provider.of<WalletController>(context, listen: false).selectedIndex = 2;
                        Navigator.of(context).push(new MaterialPageRoute(builder: (context) => WelcomeScreen()));
                      },
                      child: Text(
                        "< Back",
                        style: GoogleFonts.poppins(color: const Color(0XFF373737), fontWeight: FontWeight.w600, fontStyle: FontStyle.normal, fontSize: MediaQuery.of(context).size.height * 0.018),
                      ),
                    ),
                  ),
                ),
                Row(
                  children: [
                    Padding(
                      padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.03),
                      child: SizedBox(
                        child: (_isLoadingUserPicture)
                            ? CircleAvatar(
                                radius: MediaQuery.of(context).size.height * 0.05,
                                backgroundColor: Colors.transparent,
                                child: SizedBox(
                                  height: MediaQuery.of(context).size.height * 0.02,
                                  width: MediaQuery.of(context).size.height * 0.02,
                                  child: CircularProgressIndicator(
                                    color: const Color(0XFF4048FF),
                                  ),
                                ),
                              )
                            : CircleAvatar(
                                backgroundColor: Colors.transparent,
                                radius: MediaQuery.of(context).size.height * 0.05,
                                backgroundImage: _userPictureExist ? MemoryImage(userPicture!) : (_userPictureSocialExist ? NetworkImage(userPictureSocial) : AssetImage("images/avatar-welcome.png") as ImageProvider)),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.025),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          /** First Name */
                          Text(
                              "${LoginController.apiService.firstName == "" || LoginController.apiService.firstName.toString() == "null" ? "Your Full Name" : LoginController.apiService.firstName} ${LoginController.apiService.lastName == "" || LoginController.apiService.lastName.toString() == "null" ? "" : LoginController.apiService.lastName}",
                              style: Theme.of(context).textTheme.headline5),
                          Text(Provider.of<LoginController>(context, listen: false).userDetails?.email.toString() ?? "", style: Theme.of(context).textTheme.headline6),
                        ],
                      ),
                    ),
                    const Spacer(),
                    InkWell(
                      focusColor: Colors.transparent,
                      hoverColor: Colors.transparent,
                      splashColor: Colors.transparent,
                      highlightColor: Colors.transparent,
                      onTap: () {
                        Navigator.of(context).push(new MaterialPageRoute(builder: (context) => ProfilePage()));
                      },
                      child: Padding(
                        padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.05),
                        child: SvgPicture.asset("images/edit-icon.svg"),
                      ),
                    )
                  ],
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.015,
                ),
                /** KYC BLOC */
                Padding(
                  padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.075, right: MediaQuery.of(context).size.width * 0.075),
                  child: Divider(),
                ),
                InkWell(
                  focusColor: Colors.transparent,
                  hoverColor: Colors.transparent,
                  splashColor: Colors.transparent,
                  highlightColor: Colors.transparent,
                  onTap: () {
                    Navigator.of(context).push(new MaterialPageRoute(builder: (context) => KycPage()));
                  },
                  child: Padding(
                    padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.0075, bottom: MediaQuery.of(context).size.height * 0.0075),
                    child: Padding(
                      padding: EdgeInsets.only(
                        left: MediaQuery.of(context).size.width * 0.07,
                        top: MediaQuery.of(context).size.height * 0.006,
                        bottom: MediaQuery.of(context).size.height * 0.006,
                      ),
                      child: Row(
                        children: <Widget>[
                          SvgPicture.asset(
                            "images/kyc-icon.svg",
                            width: MediaQuery.of(context).size.width * 0.06,
                          ),
                          SizedBox(
                            width: MediaQuery.of(context).size.width * 0.035,
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[Text("KYC", style: Theme.of(context).textTheme.headline5), Text("Verify your identity", style: Theme.of(context).textTheme.headline6)],
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.075, right: MediaQuery.of(context).size.width * 0.075),
                  child: Divider(),
                ),
                /** Security BLOC */
                InkWell(
                  focusColor: Colors.transparent,
                  hoverColor: Colors.transparent,
                  splashColor: Colors.transparent,
                  highlightColor: Colors.transparent,
                  onTap: () {
                    setState(() {
                      _isClick = !_isClick;
                    });
                  },
                  child: Padding(
                    padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.0075, bottom: MediaQuery.of(context).size.height * 0.0075),
                    child: Padding(
                      padding: EdgeInsets.only(
                        left: MediaQuery.of(context).size.width * 0.07,
                        top: MediaQuery.of(context).size.height * 0.006,
                        bottom: MediaQuery.of(context).size.height * 0.006,
                      ),
                      child: Row(
                        children: <Widget>[
                          SvgPicture.asset(
                            "images/security-icon.svg",
                            width: MediaQuery.of(context).size.width * 0.06,
                          ),
                          SizedBox(
                            width: MediaQuery.of(context).size.width * 0.035,
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[Text("Security", style: Theme.of(context).textTheme.headline5), Text("Passwords, 2FA, Biometric", style: Theme.of(context).textTheme.headline6)],
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                if (_isClick)
                  Column(
                    children: [
                      /** Authentication Method BLOC */
                      InkWell(
                        focusColor: Colors.transparent,
                        hoverColor: Colors.transparent,
                        splashColor: Colors.transparent,
                        highlightColor: Colors.transparent,
                        onTap: () {
                          Navigator.of(context).push(new MaterialPageRoute(builder: (context) => AuthMethods()));
                        },
                        child: Padding(
                          padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.085, top: MediaQuery.of(context).size.height * 0.035),
                          child: Row(
                            children: <Widget>[
                              SvgPicture.asset(
                                "images/play-icon.svg",
                                width: MediaQuery.of(context).size.width * 0.017,
                              ),
                              SizedBox(
                                width: MediaQuery.of(context).size.width * 0.04,
                              ),
                              Text("Authentication method", style: Theme.of(context).textTheme.headline5)
                            ],
                          ),
                        ),
                      ),
                      SizedBox(
                        height: MediaQuery.of(context).size.height * 0.017,
                      ),
                      const Divider(
                        thickness: 1.2,
                      ),
                      /** Change Password BLOC */
                      InkWell(
                        focusColor: Colors.transparent,
                        hoverColor: Colors.transparent,
                        splashColor: Colors.transparent,
                        highlightColor: Colors.transparent,
                        onTap: () {
                          Navigator.of(context).push(new MaterialPageRoute(builder: (context) => ChangePassword()));
                        },
                        child: Padding(
                          padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.085, top: MediaQuery.of(context).size.height * 0.035),
                          child: Row(
                            children: <Widget>[
                              SvgPicture.asset(
                                "images/play-icon.svg",
                                width: MediaQuery.of(context).size.width * 0.017,
                              ),
                              SizedBox(
                                width: MediaQuery.of(context).size.width * 0.04,
                              ),
                              Text("Change your password", style: Theme.of(context).textTheme.headline5)
                            ],
                          ),
                        ),
                      ),
                      SizedBox(
                        height: MediaQuery.of(context).size.height * 0.017,
                      ),
                      const Divider(
                        thickness: 1.2,
                      ),
                      /** 2FA Bloc */
                      InkWell(
                        focusColor: Colors.transparent,
                        hoverColor: Colors.transparent,
                        splashColor: Colors.transparent,
                        highlightColor: Colors.transparent,
                        onTap: () {
                          Navigator.of(context).push(new MaterialPageRoute(builder: (context) => SecurityPage()));
                        },
                        child: Padding(
                          padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.085, top: MediaQuery.of(context).size.height * 0.017),
                          child: Row(
                            children: <Widget>[
                              SvgPicture.asset(
                                "images/play-icon.svg",
                                width: MediaQuery.of(context).size.width * 0.017,
                              ),
                              SizedBox(
                                width: MediaQuery.of(context).size.width * 0.04,
                              ),
                              Text("Double authentication (2FA)", style: Theme.of(context).textTheme.headline5)
                            ],
                          ),
                        ),
                      ),
                      SizedBox(
                        height: MediaQuery.of(context).size.height * 0.017,
                      ),
                      const Divider(
                        thickness: 1.2,
                      ),
                      /** Biometrics BLOC */
                      InkWell(
                        focusColor: Colors.transparent,
                        hoverColor: Colors.transparent,
                        splashColor: Colors.transparent,
                        highlightColor: Colors.transparent,
                        onTap: () {
                          Navigator.of(context).push(new MaterialPageRoute(builder: (context) => Biometrics()));
                        },
                        child: Padding(
                          padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.085, top: MediaQuery.of(context).size.height * 0.017),
                          child: Row(
                            children: <Widget>[
                              SvgPicture.asset(
                                "images/play-icon.svg",
                                width: MediaQuery.of(context).size.width * 0.017,
                              ),
                              SizedBox(
                                width: MediaQuery.of(context).size.width * 0.04,
                              ),
                              Text("Biometric", style: Theme.of(context).textTheme.headline5)
                            ],
                          ),
                        ),
                      ),
                      SizedBox(
                        height: MediaQuery.of(context).size.height * 0.017,
                      ),
                      const Divider(
                        thickness: 1.2,
                      ),
                      /** Account deactivation BLOC*/
                      InkWell(
                        focusColor: Colors.transparent,
                        hoverColor: Colors.transparent,
                        splashColor: Colors.transparent,
                        highlightColor: Colors.transparent,
                        onTap: () {
                          gotoPageGlobal(context, DesactivateAccount());
                        },
                        child: Padding(
                          padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.085, top: MediaQuery.of(context).size.height * 0.017),
                          child: Row(
                            children: <Widget>[
                              SvgPicture.asset(
                                "images/play-icon.svg",
                                width: MediaQuery.of(context).size.width * 0.017,
                              ),
                              SizedBox(
                                width: MediaQuery.of(context).size.width * 0.04,
                              ),
                              Text("Account deactivation", style: Theme.of(context).textTheme.headline5)
                            ],
                          ),
                        ),
                      ),
                      SizedBox(
                        height: MediaQuery.of(context).size.height * 0.017,
                      ),
                      const Divider(
                        thickness: 1.2,
                      )
                    ],
                  ),
                if (!_isClick)
                  Padding(
                    padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.075, right: MediaQuery.of(context).size.width * 0.075),
                    child: Divider(),
                  ),
                /** Adress Book BLOC  */
                InkWell(
                  focusColor: Colors.transparent,
                  hoverColor: Colors.transparent,
                  splashColor: Colors.transparent,
                  highlightColor: Colors.transparent,
                  onTap: () {
                    gotoPageGlobal(context, AdressBook(contacts: <Contact>[]));
                  },
                  child: Padding(
                    padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.0075, bottom: MediaQuery.of(context).size.height * 0.0075),
                    child: Padding(
                      padding: EdgeInsets.only(
                        left: MediaQuery.of(context).size.width * 0.07,
                        top: MediaQuery.of(context).size.height * 0.006,
                        bottom: MediaQuery.of(context).size.height * 0.006,
                      ),
                      child: Row(
                        children: <Widget>[
                          Image.asset(
                            "images/ph_address-book.png",
                            width: MediaQuery.of(context).size.width * 0.06,
                          ),
                          SizedBox(
                            width: MediaQuery.of(context).size.width * 0.035,
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[Text("Address Book", style: Theme.of(context).textTheme.headline5), Text("Add or search contacts", style: Theme.of(context).textTheme.headline6)],
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.075, right: MediaQuery.of(context).size.width * 0.075),
                  child: Divider(),
                ),
                /** Revoke Smartcontract BLOC  */
                InkWell(
                  focusColor: Colors.transparent,
                  hoverColor: Colors.transparent,
                  splashColor: Colors.transparent,
                  highlightColor: Colors.transparent,
                  onTap: () {
                    gotoPageGlobal(context, Revoke());
                  },
                  child: Padding(
                    padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.0075, bottom: MediaQuery.of(context).size.height * 0.0075),
                    child: Padding(
                      padding: EdgeInsets.only(
                        left: MediaQuery.of(context).size.width * 0.07,
                        top: MediaQuery.of(context).size.height * 0.006,
                        bottom: MediaQuery.of(context).size.height * 0.006,
                      ),
                      child: Row(
                        children: <Widget>[
                          Image.asset(
                            "images/uil_file-contract.png",
                            width: MediaQuery.of(context).size.width * 0.06,
                          ),
                          SizedBox(
                            width: MediaQuery.of(context).size.width * 0.035,
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[Text("Smartcontracts", style: Theme.of(context).textTheme.headline5), Text("Follow & revoke your contracts", style: Theme.of(context).textTheme.headline6)],
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.075, right: MediaQuery.of(context).size.width * 0.075),
                  child: Divider(),
                ),

                /** Blockchain KEY BLOC  */
                InkWell(
                  focusColor: Colors.transparent,
                  hoverColor: Colors.transparent,
                  splashColor: Colors.transparent,
                  highlightColor: Colors.transparent,
                  onTap: () {
                    Navigator.of(context).push(new MaterialPageRoute(builder: (context) => ExportKey()));
                  },
                  child: Padding(
                    padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.0075, bottom: MediaQuery.of(context).size.height * 0.0075),
                    child: Padding(
                      padding: EdgeInsets.only(
                        left: MediaQuery.of(context).size.width * 0.07,
                        top: MediaQuery.of(context).size.height * 0.006,
                        bottom: MediaQuery.of(context).size.height * 0.006,
                      ),
                      child: Row(
                        children: <Widget>[
                          SvgPicture.asset(
                            "images/json-icon.svg",
                            width: MediaQuery.of(context).size.width * 0.06,
                          ),
                          SizedBox(
                            width: MediaQuery.of(context).size.width * 0.035,
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[Text("Blockchain key", style: Theme.of(context).textTheme.headline5), Text("Export your .JSON", style: Theme.of(context).textTheme.headline6)],
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.075, right: MediaQuery.of(context).size.width * 0.075),
                  child: Divider(),
                ),
                /** Wallet connect bloc */
                InkWell(
                  focusColor: Colors.transparent,
                  hoverColor: Colors.transparent,
                  splashColor: Colors.transparent,
                  highlightColor: Colors.transparent,
                  onTap: () {
                    Navigator.of(context).push(new MaterialPageRoute(builder: (context) => WalletConnect()));
                  },
                  child: Padding(
                    padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.0075, bottom: MediaQuery.of(context).size.height * 0.0075),
                    child: Padding(
                      padding: EdgeInsets.only(
                        left: MediaQuery.of(context).size.width * 0.07,
                        top: MediaQuery.of(context).size.height * 0.006,
                        bottom: MediaQuery.of(context).size.height * 0.006,
                      ),
                      child: Row(
                        children: <Widget>[
                          SvgPicture.asset(
                            "images/wallet-connect.svg",
                            width: MediaQuery.of(context).size.width * 0.06,
                          ),
                          SizedBox(
                            width: MediaQuery.of(context).size.width * 0.035,
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[Text("Wallet Connect", style: Theme.of(context).textTheme.headline5), Text("Connect to any DAPP ", style: Theme.of(context).textTheme.headline6)],
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.075, right: MediaQuery.of(context).size.width * 0.075),
                  child: Divider(),
                ),
                /** Help BLOC */
                InkWell(
                  focusColor: Colors.transparent,
                  hoverColor: Colors.transparent,
                  splashColor: Colors.transparent,
                  highlightColor: Colors.transparent,
                  onTap: () {
                    Provider.of<WalletController>(context, listen: false).selectedIndex = 4;
                    Navigator.of(context).push(new MaterialPageRoute(builder: (context) => Help()));
                  },
                  child: Padding(
                    padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.0075, bottom: MediaQuery.of(context).size.height * 0.0075),
                    child: Padding(
                      padding: EdgeInsets.only(
                        left: MediaQuery.of(context).size.width * 0.07,
                        top: MediaQuery.of(context).size.height * 0.006,
                        bottom: MediaQuery.of(context).size.height * 0.006,
                      ),
                      child: Row(
                        children: <Widget>[
                          SvgPicture.asset(
                            "images/help-new-icon.svg",
                            width: MediaQuery.of(context).size.width * 0.06,
                          ),
                          SizedBox(
                            width: MediaQuery.of(context).size.width * 0.035,
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[Text("Help", style: Theme.of(context).textTheme.headline5), Text("Frequently Asked Questions", style: Theme.of(context).textTheme.headline6)],
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.075, right: MediaQuery.of(context).size.width * 0.075),
                  child: Divider(),
                ),
                InkWell(
                  focusColor: Colors.transparent,
                  hoverColor: Colors.transparent,
                  splashColor: Colors.transparent,
                  highlightColor: Colors.transparent,
                  onTap: () async {
                    Share.share('Check out this awesome wallet :\nhttps://iHave.io');
                  },
                  child: Padding(
                    padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.0075, bottom: MediaQuery.of(context).size.height * 0.0075),
                    child: Padding(
                      padding: EdgeInsets.only(
                        left: MediaQuery.of(context).size.width * 0.07,
                        top: MediaQuery.of(context).size.height * 0.006,
                        bottom: MediaQuery.of(context).size.height * 0.006,
                      ),
                      child: Row(
                        children: <Widget>[
                          SvgPicture.asset(
                            "images/share-icon.svg",
                            width: MediaQuery.of(context).size.width * 0.06,
                          ),
                          SizedBox(
                            width: MediaQuery.of(context).size.width * 0.035,
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[Text("Share the App", style: Theme.of(context).textTheme.headline5), Text("Invite your friends", style: Theme.of(context).textTheme.headline6)],
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.05,
                ),
                /** Log out bloc */
                InkWell(
                  focusColor: Colors.transparent,
                  hoverColor: Colors.transparent,
                  splashColor: Colors.transparent,
                  highlightColor: Colors.transparent,
                  onTap: () async {
                    Provider.of<LoginController>(context, listen: false).userDetails?.userToken = "";
                    Provider.of<WalletController>(context, listen: false).checkTron = false;
                    Provider.of<WalletController>(context, listen: false).cryptos = [];
                    Provider.of<LoginController>(context, listen: false).userPicture = null;
                    Provider.of<LoginController>(context, listen: false).Balance = "";
                    await Provider.of<LoginController>(context, listen: false).logout();
                    reinitUserDetails();
                    Navigator.of(context).push(new MaterialPageRoute(builder: (context) => Signin()));
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      SvgPicture.asset(
                        "images/log-out-icon.svg",
                      ),
                      SizedBox(
                        width: MediaQuery.of(context).size.width * 0.03,
                      ),
                      Text(
                        "Logout",
                        style: GoogleFonts.poppins(fontWeight: FontWeight.w500, color: const Color(0XFFF52079), fontStyle: FontStyle.normal, fontSize: MediaQuery.of(context).size.height * 0.018),
                      )
                    ],
                  ),
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.05,
                ),
                Text(
                  "$appName v $version",
                  style: Theme.of(context).textTheme.headline4,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}