import 'dart:async';
import 'dart:typed_data';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:ihave/controllers/LoginController.dart';
import 'package:ihave/service/apiService.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:ihave/ui/passPhrase.dart';
import 'package:ihave/ui/signin.dart';
import 'package:ihave/ui/successWallet.dart';
import 'package:ihave/util/utils.dart';
import 'package:ihave/widgets/alertDialogWidget.dart';
import 'package:provider/provider.dart';

class CreateWallet extends StatefulWidget {
  CreateWallet({Key? key}) : super(key: key);

  @override
  _CreateWalletState createState() => _CreateWalletState();
}

class _CreateWalletState extends State<CreateWallet> {
  TextEditingController _WalletPassword = TextEditingController();
  TextEditingController _confirmWalletPassword = TextEditingController();

  bool isLoggedIn = false;

  bool _obscureText = true;
  bool _confirmobscureText = true;
  double _strength = 0;
  RegExp numReg = RegExp(r".*[0-9].*");
  RegExp letterReg = RegExp(r".*[A-Za-z].*");
  APIService apiService = LoginController.apiService;
  bool _isLoadingButton = false;
  String dropDownValue = "En";
  String _displayText = '';
  bool _isActivatedButton = false;
  bool _isSamePassword = false;
  Uint8List? userPicture;
  bool _samePassword = false;
  bool hasLength = false;
  bool hasNumber = false;
  RegExp RegUpperCase = RegExp(r'[A-Z]');
  RegExp? BlacklistingTextInputFormatter;
  RegExp RegLowerCase = RegExp(r'[a-z]');
  RegExp RegNumber = RegExp(r'[0-9]');
  RegExp RegSpecialChar = RegExp('[A-Za-z0-9]');
  bool _passwordFieldForm = false;
  bool _passwordEdited = false;
  bool confirmPasswordEdited = false;
  bool isConfirmed = false;
  String? firebaseToken = " ";

  bool checkSpecialChar(String pass) {
    var specialPass = pass.replaceAll(RegSpecialChar, "");
    return specialPass.length > 0;
  }

  void _checkPassword(String value) {
    if (_WalletPassword.text.length < 8) {
      setState(() {
        _strength = 1 / 4;
        _displayText = 'Low security level';
      });
    } else {
      if (_WalletPassword.text.length < 10) {
        setState(() {
          _strength = 2 / 4;
          _displayText = 'Medium level security';
        });
      } else {
        if (_WalletPassword.text.length >= 10 && _WalletPassword.text.length <= 19 && RegLowerCase.hasMatch(_WalletPassword.text) && RegUpperCase.hasMatch(_WalletPassword.text) && checkSpecialChar(_WalletPassword.text)) {
          setState(() {
            _strength = 1;
            _displayText = 'High security level';
          });
        } else {
          if (_WalletPassword.text.length > 19 && RegLowerCase.hasMatch(_WalletPassword.text) && RegUpperCase.hasMatch(_WalletPassword.text) && checkSpecialChar(_WalletPassword.text)) {
            setState(() {
              _strength = 1;
              _displayText = ' Very high security level';
            });
          }
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async {
          return false;
        },
        child: Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                  colors: [
                    const Color(0xFFF52079),
                    const Color(0xFF1F2337),
                  ],
                  begin: const FractionalOffset(0.0, -0.1),
                  end: const FractionalOffset(0.0, 0.3),
                  stops: [0.0, 1.0],
                  tileMode: TileMode.clamp),
            ),
            child: Scaffold(
                backgroundColor: Colors.transparent,
                resizeToAvoidBottomInset: false,
                appBar: null,
                body: GestureDetector(
                    onTap: () {
                      FocusScope.of(context).requestFocus(new FocusNode());
                    },
                    child: SafeArea(
                      child: Container(
                          height: MediaQuery.of(context).size.height,
                          width: MediaQuery.of(context).size.width,
                          child: SingleChildScrollView(
                              child: Column(children: [
                            SizedBox(
                              height: MediaQuery.of(context).size.height * 0.02,
                            ),
                            Stack(
                              children: [
                                Align(
                                  alignment: Alignment.topCenter,
                                  child: SvgPicture.asset(
                                    'images/logo-auth.svg',
                                    alignment: Alignment.center,
                                    width: MediaQuery.of(context).size.height * 0.035,
                                    height: MediaQuery.of(context).size.height * 0.035,
                                  ),
                                ),
                                Align(
                                  alignment: Alignment.topRight,
                                  child: Padding(
                                    padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.02, top: MediaQuery.of(context).size.height * 0.0075),
                                    child: InkWell(
                                      child: SvgPicture.asset(
                                        'images/new-logout.svg',
                                      ),
                                      onTap: () {
                                        Provider.of<LoginController>(context, listen: false).logout();
                                        Navigator.pushReplacement(context, MaterialPageRoute(builder: (_) => Signin()));
                                      },
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: MediaQuery.of(context).size.height * 0.065,
                            ),
                            Text(
                              "Create your\nTransaction\nPassword",
                              textAlign: TextAlign.center,
                              style: GoogleFonts.poppins(
                                color: Colors.white,
                                fontStyle: FontStyle.normal,
                                fontWeight: FontWeight.w700,
                                height: 1,
                                fontSize: MediaQuery.of(context).size.height * 0.04,
                              ),
                            ),
                            SizedBox(
                              height: MediaQuery.of(context).size.height * 0.03,
                            ),
                            Padding(
                              padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.085, right: MediaQuery.of(context).size.width * 0.085),
                              child: Text(
                                "To complete each transaction,  you will need to enter this password. Write it down carefully, and store it in a secure place !",
                                textAlign: TextAlign.center,
                                style: GoogleFonts.poppins(
                                  color: Colors.white,
                                  fontStyle: FontStyle.normal,
                                  letterSpacing: 1.2,
                                  fontWeight: FontWeight.w500,
                                  fontSize: MediaQuery.of(context).size.height * 0.017,
                                ),
                              ),
                            ),
                            SizedBox(
                              height: MediaQuery.of(context).size.height * 0.02,
                            ),
                            Text(
                              "YOU WILL NOT BE ABLE TO RESET IT !",
                              style: GoogleFonts.poppins(
                                decoration: TextDecoration.underline,
                                fontWeight: FontWeight.w700,
                                fontStyle: FontStyle.normal,
                                fontSize: MediaQuery.of(context).size.height * 0.02,
                                color: Colors.white,
                              ),
                            ),
                            SizedBox(
                              height: MediaQuery.of(context).size.height * 0.04,
                            ),
                            Padding(
                              padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.125, bottom: MediaQuery.of(context).size.height * 0.004),
                              child: Align(
                                alignment: Alignment.topLeft,
                                child: Text(
                                  "CHOOSE YOUR WALLET PASSWORD",
                                  style: GoogleFonts.poppins(color: Colors.white, fontStyle: FontStyle.normal, fontSize: MediaQuery.of(context).size.height * 0.018, fontWeight: FontWeight.w600),
                                ),
                              ),
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width * 0.8,
                              child: TextFormField(
                                style: TextStyle(fontSize: MediaQuery.of(context).size.width * 0.045),
                                textAlignVertical: TextAlignVertical.center,
                                textAlign: TextAlign.left,
                                controller: _WalletPassword,
                                keyboardType: TextInputType.visiblePassword,
                                obscureText: _obscureText,
                                onChanged: (value) {
                                  _passwordEdited = true;
                                  TextSelection previousSelection = _WalletPassword.selection;
                                  _WalletPassword.text = value;
                                  _WalletPassword.selection = previousSelection;

                                  if (_WalletPassword.text.isNotEmpty) {
                                    _checkPassword(value);
                                    if (RegUpperCase.hasMatch(_WalletPassword.text) && RegLowerCase.hasMatch(_WalletPassword.text) && RegNumber.hasMatch(_WalletPassword.text) && checkSpecialChar(_WalletPassword.text) && _WalletPassword.text.length >= 8) {
                                      setState(() {
                                        _passwordFieldForm = true;
                                      });
                                    } else {
                                      setState(() {
                                        _passwordFieldForm = false;
                                      });
                                    }
                                    if (_WalletPassword.text != _confirmWalletPassword.text || !_passwordFieldForm) {
                                      setState(() {
                                        _isActivatedButton = false;
                                        isConfirmed = false;
                                      });
                                    } else {
                                      setState(() {
                                        _isActivatedButton = true;
                                        isConfirmed = true;
                                      });
                                    }
                                  }
                                  TextSelection.fromPosition(TextPosition(offset: _WalletPassword.text.length));
                                },
                                validator: (value) {
                                  if (value!.isEmpty) {
                                    return 'Field required';
                                  }
                                  return null;
                                },
                                decoration: InputDecoration(
                                    fillColor: Colors.white,
                                    focusColor: Colors.white,
                                    hoverColor: Colors.white,
                                    filled: true,
                                    isDense: true,
                                    prefixIcon: Padding(
                                      padding: EdgeInsets.all(MediaQuery.of(context).size.width * 0.017),
                                      child: SvgPicture.asset(
                                        "images/keyIcon.svg",
                                        color: Colors.orange,
                                        height: MediaQuery.of(context).size.width * 0.06,
                                        width: MediaQuery.of(context).size.width * 0.06,
                                      ),
                                    ),
                                    suffixIcon: Padding(
                                      padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.025),
                                      child: IconButton(
                                          onPressed: () {
                                            setState(() {
                                              _obscureText = !_obscureText;
                                            });
                                          },
                                          icon: _obscureText
                                              ? SvgPicture.asset(
                                                  "images/visibility-icon-off.svg",
                                                  height: MediaQuery.of(context).size.height * 0.02,
                                                )
                                              : SvgPicture.asset(
                                                  "images/visibility-icon-on.svg",
                                                  height: MediaQuery.of(context).size.height * 0.02,
                                                )),
                                    ),
                                    contentPadding: EdgeInsets.symmetric(vertical: 5, horizontal: 5),
                                    enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.white)),
                                    focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.white)),
                                    errorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red)),
                                    focusedErrorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red))),
                              ),
                            ),
                            Visibility(
                              visible: (_WalletPassword.text.length > 0),
                              child: !_passwordFieldForm
                                  ? Column(
                                      children: [
                                        Padding(
                                          padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.015, bottom: MediaQuery.of(context).size.height * 0.015, right: MediaQuery.of(context).size.width * 0.125, left: MediaQuery.of(context).size.width * 0.125),
                                          child: Text("Password must contain at least 8 characters uppercase, lowercase, digit and special character",
                                              style: GoogleFonts.poppins(
                                                fontWeight: FontWeight.w700,
                                                color: Color(0xffF52079),
                                                fontSize: 13,
                                              )),
                                        ),
                                      ],
                                    )
                                  : Text(""),
                            ),
                            Padding(
                              padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.125, top: MediaQuery.of(context).size.height * 0.015),
                              child: Align(
                                alignment: Alignment.topLeft,
                                child: Text(
                                  "CONFIRM",
                                  style: GoogleFonts.poppins(color: Colors.white, fontStyle: FontStyle.normal, fontSize: MediaQuery.of(context).size.height * 0.018, fontWeight: FontWeight.w600),
                                ),
                              ),
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width * 0.8,
                              child: TextFormField(
                                style: TextStyle(fontSize: MediaQuery.of(context).size.width * 0.045),
                                controller: _confirmWalletPassword,
                                keyboardType: TextInputType.visiblePassword,
                                obscureText: _confirmobscureText,
                                onChanged: (value) {
                                  confirmPasswordEdited = true;
                                  TextSelection previousSelection = _confirmWalletPassword.selection;
                                  _confirmWalletPassword.text = value;
                                  _confirmWalletPassword.selection = previousSelection;
                                  if (_confirmWalletPassword.text == _WalletPassword.text) {
                                    setState(() {
                                      isConfirmed = true;
                                      _isActivatedButton = true;
                                    });
                                  } else {
                                    setState(() {
                                      isConfirmed = false;
                                      _isActivatedButton = false;
                                    });
                                  }
                                  TextSelection.fromPosition(TextPosition(offset: _confirmWalletPassword.text.length));
                                },
                                validator: (value) {
                                  if (value!.isEmpty) {
                                    return 'Field required';
                                  }
                                },
                                decoration: InputDecoration(
                                    fillColor: Colors.white,
                                    focusColor: Colors.white,
                                    hoverColor: Colors.white,
                                    filled: true,
                                    isDense: true,
                                    border: InputBorder.none,
                                    prefixIcon: Padding(
                                      padding: EdgeInsets.all(MediaQuery.of(context).size.width * 0.017),
                                      child: SvgPicture.asset(
                                        "images/keyIcon.svg",
                                        color: Colors.orange,
                                        height: MediaQuery.of(context).size.width * 0.06,
                                        width: MediaQuery.of(context).size.width * 0.06,
                                      ),
                                    ),
                                    suffixIcon: Padding(
                                      padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.025),
                                      child: IconButton(
                                          onPressed: () {
                                            setState(() {
                                              _confirmobscureText = !_confirmobscureText;
                                            });
                                          },
                                          icon: _confirmobscureText
                                              ? SvgPicture.asset(
                                                  "images/visibility-icon-off.svg",
                                                  height: MediaQuery.of(context).size.height * 0.02,
                                                )
                                              : SvgPicture.asset(
                                                  "images/visibility-icon-on.svg",
                                                  height: MediaQuery.of(context).size.height * 0.02,
                                                )),
                                    ),
                                    contentPadding: EdgeInsets.symmetric(vertical: 5, horizontal: 5),
                                    enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.white)),
                                    focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.white)),
                                    errorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red)),
                                    focusedErrorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red))),
                              ),
                            ),
                            confirmPasswordEdited
                                ? Visibility(
                                    child: !isConfirmed
                                        ? Align(
                                            alignment: Alignment.topLeft,
                                            child: Padding(
                                              padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.015, left: MediaQuery.of(context).size.width * 0.125),
                                              child: Text(
                                                "Password must match !!",
                                                style: GoogleFonts.poppins(color: Color(0xFFfe4164), fontStyle: FontStyle.normal, fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.width * 0.03),
                                              ),
                                            ),
                                          )
                                        : Padding(padding: EdgeInsets.all(0.0)),
                                  )
                                : Padding(padding: EdgeInsets.all(0.0)),
                            _passwordEdited
                                ? Padding(
                                    padding: EdgeInsets.only(top: MediaQuery.of(context).size.width * 0.005, left: MediaQuery.of(context).size.width * 0.15),
                                    child: _isSamePassword
                                        ? Row(
                                            children: [
                                              SvgPicture.asset(
                                                "images/icon-password-message.svg",
                                                color: Color(0xFFfe4164),
                                                height: MediaQuery.of(context).size.width * 0.035,
                                                width: MediaQuery.of(context).size.width * 0.035,
                                              ),
                                              Padding(
                                                padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.033),
                                                child: Text(
                                                  "Don't use the same password as your \naccount password",
                                                  style: GoogleFonts.poppins(color: Color(0xFFfe4164), fontStyle: FontStyle.normal, fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.width * 0.03),
                                                ),
                                              ),
                                            ],
                                          )
                                        : null,
                                  )
                                : Padding(padding: EdgeInsets.all(0.0)),
                            _passwordEdited
                                ? Column(
                                    children: [
                                      SizedBox(
                                        height: MediaQuery.of(context).size.height * 0.025,
                                      ),
                                      Row(
                                        children: [
                                          Spacer(),

                                          /** BOX of Low security , medium security and high security */
                                          SizedBox(
                                            width: _strength == 1 ? MediaQuery.of(context).size.width * 0.7 : (_strength <= 1 / 4 ? MediaQuery.of(context).size.width * 0.1 : MediaQuery.of(context).size.width * 0.4),
                                            child: LinearProgressIndicator(
                                              value: 2,
                                              backgroundColor: Colors.transparent,
                                              color: _strength <= 1 / 4
                                                  ? const Color(0XFFF52079)
                                                  : _strength == 2 / 4
                                                      ? Color(0XFFF9E756)
                                                      : _strength == 1
                                                          ? Color(0XFF00CC9E)
                                                          : Color(0xFF00CC9E),
                                              minHeight: 3,
                                            ),
                                          ),

                                          /** Rest of box */
                                          SizedBox(
                                            width: _strength == 1 ? MediaQuery.of(context).size.width * 0.05 : (_strength <= 1 / 4 ? MediaQuery.of(context).size.width * 0.65 : MediaQuery.of(context).size.width * 0.35),
                                            child: LinearProgressIndicator(
                                              value: 2,
                                              color: _strength == 1 ? Colors.white : const Color(0XFFADADC8),
                                              backgroundColor: Colors.transparent,
                                              minHeight: 3,
                                            ),
                                          ),
                                          Spacer(),
                                        ],
                                      ),
                                      SizedBox(
                                        height: MediaQuery.of(context).size.height * 0.015,
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.01),
                                        child: Container(
                                          height: MediaQuery.of(context).size.height * 0.045,
                                          width: MediaQuery.of(context).size.width * 0.6,
                                          decoration: BoxDecoration(
                                            borderRadius: BorderRadius.circular(30),
                                            color: _strength <= 1 / 4
                                                ? const Color(0XFFF52079)
                                                : _strength == 2 / 4
                                                    ? Color(0XFFF9E756)
                                                    : _strength == 1
                                                        ? Color(0XFF00CC9E)
                                                        : Color(0xFF00CC9E),
                                          ),
                                          child: Row(
                                            children: [
                                              Spacer(),
                                              SvgPicture.asset(
                                                "images/icon-password-message.svg",
                                                height: MediaQuery.of(context).size.height * 0.024,
                                                width: MediaQuery.of(context).size.height * 0.024,
                                                color: _strength <= 1 / 4
                                                    ? Colors.white
                                                    : _strength <= 2 / 4
                                                        ? Colors.black
                                                        : _strength == 3 / 4
                                                            ? Colors.orange
                                                            : _strength == 1
                                                                ? Colors.white
                                                                : Color(0xFF00CC9E),
                                              ),
                                              Padding(
                                                padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.03),
                                                child: Text(_displayText,
                                                    style: GoogleFonts.poppins(
                                                        color: _strength <= 1 / 4
                                                            ? Colors.white
                                                            : _strength <= 2 / 4
                                                                ? Colors.black
                                                                : _strength == 3 / 4
                                                                    ? Colors.orange
                                                                    : _strength == 1
                                                                        ? Colors.white
                                                                        : Color(0xFF00CC9E),
                                                        fontSize: MediaQuery.of(context).size.height * 0.015,
                                                        fontWeight: FontWeight.w700,
                                                        letterSpacing: 0.6)),
                                              ),
                                              Spacer()
                                            ],
                                          ),
                                        ),
                                      ),
                                      SizedBox(
                                        height: MediaQuery.of(context).size.height * 0.01,
                                      )
                                    ],
                                  )
                                : Padding(padding: EdgeInsets.all(0.0)),
                            Padding(
                              padding: EdgeInsets.fromLTRB(0, MediaQuery.of(context).size.width * 0.055, 0, MediaQuery.of(context).viewInsets.bottom.toString() == "0.0" ? MediaQuery.of(context).size.width * 0.07 : MediaQuery.of(context).size.height * 0.4),
                              child: _isActivatedButton
                                  ? Container(
                                      width: MediaQuery.of(context).size.width * 0.65,
                                      height: MediaQuery.of(context).size.width * 0.13,
                                      child: ElevatedButton(
                                          style: ButtonStyle(
                                            backgroundColor: MaterialStateProperty.all(
                                              Color(0xFF00CC9E),
                                            ),
                                            shape: MaterialStateProperty.all(
                                              RoundedRectangleBorder(
                                                borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.07),
                                              ),
                                            ),
                                          ),
                                          onPressed: () async {
                                            setState(() {
                                              _isActivatedButton = false;
                                              _isLoadingButton = true;
                                            });

                                            String userToken = Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "";
                                            var responseWallet = await apiService.createWallet(userToken, _WalletPassword.text, version);
                                            if (responseWallet.toString() != "error" && responseWallet.toString() != "Same Password") {
                                              setState(() {
                                                _isLoadingButton = false;
                                                _isActivatedButton = true;
                                              });
                                              Provider.of<LoginController>(context, listen: false).userPicture = userPicture;
                                              gotoPageGlobal(context, PassPhrase());
                                            } else if (responseWallet.toString() == "Same Password") {
                                              setState(() {
                                                _isLoadingButton = false;
                                                _isActivatedButton = true;
                                              });
                                              _isSamePassword = true;
                                              Timer(Duration(seconds: 3), () {
                                                setState(() {
                                                  _isSamePassword = false;
                                                });
                                              });
                                            } else {
                                              setState(() {
                                                _isLoadingButton = false;
                                                _isActivatedButton = true;
                                              });
                                              displayDialog(context, "Error", "Something went wrong, please try again");
                                            }
                                          },
                                          child: Row(
                                            children: <Widget>[
                                              Spacer(),
                                              Text(
                                                "Continue",
                                                style: GoogleFonts.poppins(
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.w600,
                                                  fontSize: MediaQuery.of(context).size.width * 0.042,
                                                ),
                                              ),
                                              Spacer(),
                                            ],
                                          )))
                                  : Container(
                                      width: MediaQuery.of(context).size.width * 0.65,
                                      height: MediaQuery.of(context).size.width * 0.13,
                                      decoration: BoxDecoration(borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.07), color: Color(0xFFF6F6FF)),
                                      child: Row(
                                        children: <Widget>[
                                          Spacer(),
                                          Padding(
                                            padding: const EdgeInsets.all(0),
                                            child: !_isLoadingButton
                                                ? Text(
                                                    "Continue",
                                                    style: GoogleFonts.poppins(
                                                      color: Color(0xFFADADC8),
                                                      fontWeight: FontWeight.w600,
                                                      fontSize: MediaQuery.of(context).size.width * 0.042,
                                                    ),
                                                  )
                                                : SizedBox(
                                                    height: MediaQuery.of(context).size.width * 0.032,
                                                    width: MediaQuery.of(context).size.width * 0.032,
                                                    child: CircularProgressIndicator(
                                                      color: Color(0xFFADADC8),
                                                    ),
                                                  ),
                                          ),
                                          Spacer(),
                                        ],
                                      ),
                                    ),
                            ),
                          ]))),
                    )))));
  }
}