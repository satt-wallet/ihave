import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:ihave/controllers/LoginController.dart';
import 'package:ihave/controllers/walletController.dart';
import 'package:ihave/model/crypto.dart';
import 'package:ihave/service/apiService.dart';
import 'package:ihave/util/utils.dart';
import 'package:provider/provider.dart';

import 'addToken.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final _formKey = GlobalKey<FormState>();
  bool isFirstInit = true;
  bool _isProfileVisible = false;
  bool _isWalletVisible = false;
  bool _isNotificationVisible = false;
  bool _isSearching = false;
  APIService apiService = LoginController.apiService;
  List<Crypto> _cryptoList = <Crypto>[];
  List<Crypto> _searchList = <Crypto>[];
  TextEditingController _search = new TextEditingController();
  late var timer;
  String variation = "";

  Future<List<Crypto>> fetchCrypto() async {
    if (Provider.of<WalletController>(context, listen: false).cryptos.length < 1) {
      var cryptoList = await apiService.cryptos1(Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "", version);
      Provider.of<WalletController>(context, listen: false).cryptos = cryptoList;
    }
    return Provider.of<WalletController>(context, listen: false).cryptos;
  }

  @override
  void initState() {
    fetchCrypto().then((value) {
      setState(() {
        _cryptoList.clear();
        _cryptoList.addAll(value);
      });
    });
    Provider.of<LoginController>(context, listen: false).tokenSymbol == "";
    Provider.of<LoginController>(context, listen: false).tokenNetwork == "";
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (_cryptoList.isNotEmpty || !isFirstInit) {
      setState(() {
        isFirstInit = false;
      });
      return WillPopScope(
          onWillPop: () async {
            return false;
          },
          child: Container(
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                colors: [
                  Color.fromRGBO(64, 72, 255, 1),
                  Color.fromRGBO(0, 0, 1, 1),
                ],
                begin: const FractionalOffset(0.0, 0.0),
                end: const FractionalOffset(0.0, 1.0),
                stops: [0.0, 1.0],
                tileMode: TileMode.clamp,
              )),
              child: Container(
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.5),
                        spreadRadius: 5,
                        blurRadius: 7,
                        offset: Offset(0, 3),
                      )
                    ],
                    color: Colors.white,
                    border: Border.all(color: Colors.white),
                    borderRadius: BorderRadius.only(topLeft: Radius.circular(20), topRight: Radius.circular(20)),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Stack(children: [
                      SingleChildScrollView(
                        physics: ScrollPhysics(),
                        child: Column(children: [
                          Padding(
                              padding: const EdgeInsets.fromLTRB(30, 25, 30, 5),
                              child: Form(
                                key: _formKey,
                                child: TextFormField(
                                    controller: _search,
                                    readOnly: _isNotificationVisible,
                                    autofocus: false,
                                    decoration: new InputDecoration(
                                        hintText: 'Search',
                                        prefixIcon: Padding(
                                          padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.02, right: MediaQuery.of(context).size.width * 0.02),
                                          child: Icon(
                                            Icons.search,
                                            size: MediaQuery.of(context).size.width * 0.037,
                                          ),
                                        ),
                                        fillColor: Colors.grey,
                                        focusColor: Colors.grey,
                                        hintStyle: GoogleFonts.poppins(fontWeight: FontWeight.bold, color: Colors.grey, fontSize: MediaQuery.of(context).size.width * 0.047),
                                        contentPadding: new EdgeInsets.symmetric(vertical: 0.0, horizontal: MediaQuery.of(context).size.width * 0.04),
                                        enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.black12)),
                                        focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.black12)),
                                        errorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red)),
                                        focusedErrorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red))),
                                    onChanged: (text) {
                                      searchList(text);
                                    }),
                              )),
                          Divider(thickness: 1, color: Colors.grey.withOpacity(0.6)),
                          Container(
                            child: ListView.builder(
                              shrinkWrap: true,
                              primary: false,
                              scrollDirection: Axis.vertical,
                              itemBuilder: (context, index) {
                                return Card(
                                    child: Padding(
                                  padding: const EdgeInsets.all(0.0),
                                  child: Column(children: [
                                    Row(
                                      children: [
                                        _cryptoList[index].addedToken == true
                                            ? (_cryptoList[index].picUrl != "false" && _cryptoList[index].picUrl != "null"
                                                ? Image.network(
                                                    _cryptoList[index].picUrl ?? "",
                                                    width: MediaQuery.of(context).size.width * 0.095,
                                                    height: MediaQuery.of(context).size.width * 0.095,
                                                  )
                                                : SvgPicture.asset(
                                                    'images/indispo.svg',
                                                    width: MediaQuery.of(context).size.width * 0.095,
                                                    height: MediaQuery.of(context).size.width * 0.095,
                                                  ))
                                            : SvgPicture.asset(
                                                'images/' + _cryptoList[index].name.toString().toLowerCase().replaceAll(' ', '') + '.svg',
                                                width: MediaQuery.of(context).size.width * 0.095,
                                                height: MediaQuery.of(context).size.width * 0.095,
                                              ),
                                        Padding(
                                          padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.022),
                                          child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                _cryptoList[index].name ?? _cryptoList[index].undername ?? "<Network Error>",
                                                style: GoogleFonts.poppins(fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.width * 0.037),
                                              ),
                                              Text(
                                                _cryptoList[index].network ?? "<Network Error>",
                                                style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.03),
                                              )
                                            ],
                                          ),
                                        ),
                                        Spacer(),
                                        Column(
                                          crossAxisAlignment: CrossAxisAlignment.end,
                                          mainAxisAlignment: MainAxisAlignment.end,
                                          children: [
                                            Text(formatDollarBalance(_cryptoList[index].price ?? 0), style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.036)),
                                            Text(
                                              (_cryptoList[index].variation?.toStringAsFixed(2) ?? "0.00") + "%",
                                              style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.036, color: ((_cryptoList[index].variation ?? 0.00) >= 0) ? Colors.green : Colors.red),
                                            )
                                          ],
                                        )
                                      ],
                                    )
                                  ]),
                                ));
                              },
                              itemCount: _cryptoList.length,
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.all(MediaQuery.of(context).size.width * 0.02),
                            child: Container(
                              width: MediaQuery.of(context).size.width * 0.65,
                              child: ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                  minimumSize: Size(260.0, 40.0),
                                  side: BorderSide(color: Colors.white),
                                  primary: Colors.white,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(50.0),
                                  ),
                                ),
                                child: Text(
                                  "+ Add Token",
                                  style: GoogleFonts.poppins(fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.width * 0.045, color: Colors.blue),
                                ),
                                onPressed: () {
                                  gotoPageGlobal(context, AddToken());
                                },
                              ),
                            ),
                          ),
                        ]),
                      ),
                    ]),
                  ))));
    } else {
      return Center(
          child: CircularProgressIndicator(
        color: Colors.white,
      ));
    }
  }

  searchList(String text) {
    setState(() {
      if (text != null && text != "") {
        _cryptoList = Provider.of<WalletController>(context, listen: false).cryptos;
        var searchListName = _cryptoList.where((e) => e.name!.toLowerCase().contains(text.toLowerCase())).toList();
        var searchListSymbol = _cryptoList.where((e) => e.symbol?.toLowerCase() == text.toLowerCase()).toList();
        var searchResult = searchListName;
        searchResult.addAll(searchListSymbol);
        ;
        _cryptoList = searchResult.toSet().toList();
      } else {
        _cryptoList = Provider.of<WalletController>(context, listen: false).cryptos;
      }
    });
  }
}