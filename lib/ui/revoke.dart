import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_tab_indicator_styler/flutter_tab_indicator_styler.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:ihave/controllers/LoginController.dart';
import 'package:ihave/controllers/walletController.dart';
import 'package:ihave/main.dart';
import 'package:ihave/service/web3Service.dart';
import 'package:ihave/util/contactsHive.dart';
import 'package:ihave/widgets/customAppBar.dart';
import 'package:ihave/widgets/customBottomBar.dart';
import 'package:one_context/one_context.dart';
import 'package:provider/provider.dart';

import '../model/approvedContracts.dart';

TextEditingController _passwordController = new TextEditingController();
bool _obscureText2 = true;
bool passwrong = false;
BuildContext? contextRevoke;
Web3Service web3Service = new Web3Service("", LoginController.apiService.walletAddress ?? "", contextRevoke ?? MyApp.materialKey.currentContext!);

class Revoke extends StatefulWidget {
  @override
  _RevokePageState createState() => _RevokePageState();
}

class _RevokePageState extends State<Revoke> with TickerProviderStateMixin {
  late TabController tabController;
  List<ApprovedContract> _approvedContracts = [];
  List<ApprovedContract> _approvedContractsETH = [];
  List<ApprovedContract> _approvedContractsBSC = [];
  var revokeList = "ALL";

  bool isContractsLoading = false;

  double progress = 0;

  getContracts() async {
    setState(() {
      isContractsLoading = true;
    });
    var contracts = await web3Service.getApprovedTokens();
    setState(() {
      isContractsLoading = false;
    });
    return contracts;
  }

  @override
  void initState() {
    super.initState();
    tabController = TabController(length: 5, vsync: this, initialIndex: 0);
    getContracts().then((value) {
      setState(() {
        _approvedContracts = value;
        _approvedContractsETH = _approvedContracts.where((element) => element.network!.toLowerCase().contains("erc")).toList();
        _approvedContractsBSC = _approvedContracts.where((element) => element.network!.toLowerCase().contains("bep")).toList();
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    setState(() {
      web3Service.context = context;
      progress = Provider.of<WalletController>(context, listen: true).progress;
    });
    web3Service.userToken = Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "";
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: CustomAppBar(),
      bottomNavigationBar: CustomBottomBar(),
      body: SingleChildScrollView(
        child: Stack(children: [
          Column(
            children: <Widget>[
              Row(
                children: [
                  Padding(
                    padding: EdgeInsets.only(top: height * .03, left: width * 0.045),
                    child: Text(
                      "Smartcontracts",
                      style: GoogleFonts.poppins(
                        color: Color(0xFF1F2337),
                        fontSize: height * 0.04,
                        fontWeight: FontWeight.w700,
                        fontStyle: FontStyle.normal,
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: height * 0.02,
              ),
              Padding(
                padding: EdgeInsets.only(left: width * 0.045),
                child: Text(
                  "Review and revoke your token approvals for any Dapp.",
                  style: GoogleFonts.poppins(
                    color: Color(0xFF1F2337),
                    fontSize: height * 0.02,
                    fontWeight: FontWeight.w300,
                    fontStyle: FontStyle.normal,
                  ),
                ),
              ),
              SizedBox(
                height: height * .02,
              ),
              Stack(
                fit: StackFit.passthrough,
                alignment: Alignment.bottomCenter,
                children: <Widget>[
                  Container(
                    width: width * 0.92,
                    decoration: BoxDecoration(
                      border: Border(
                        bottom: BorderSide(color: Color(0xffADADC8), width: 2.5),
                      ),
                    ),
                  ),
                  TabBar(
                    controller: tabController,
                    labelColor: Color(0xff4048FF),
                    unselectedLabelColor: Color(0xffADADC8),
                    indicatorWeight: 3,
                    onTap: (int index) {
                      setState(() {
                        revokeList = index == 0
                            ? "ALL"
                            : index == 1
                                ? "ERC"
                                : index == 3
                                    ? "BSC"
                                    : "";
                      });
                    },
                    tabs: [
                      Text(
                        'All',
                      ),
                      Text(
                        'ERC',
                      ),
                      Text(
                        'BTTC',
                      ),
                      Text(
                        'BSC',
                      ),
                      Container(
                        height: 55.0,
                        alignment: Alignment.centerRight,
                        child: Text(
                          'TRON',
                        ),
                      ),
                    ],
                    indicator: MaterialIndicator(
                      color: Color(0xff4048FF),
                      horizontalPadding: 14,
                      tabPosition: TabPosition.bottom,
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: height * .02,
              ),
              isContractsLoading
                  ? Container(
                      height: height * 0.5,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Spacer(),
                          CircularProgressIndicator(),
                          Text(
                            "Please hold while we check Blockchain\nLoading your data ${progress.toStringAsFixed(0)}%",
                            style: GoogleFonts.poppins(
                              color: Colors.black,
                            ),
                            textAlign: TextAlign.center,
                          ),
                          Spacer(),
                          Spacer(),
                        ],
                      ),
                    )
                  : contractsList(revokeList),
            ],
          ),
        ]),
      ),
    );
  }

  Widget contractsList(String network) {
    var ls = [];
    setState(() {
      ls = network == "ALL"
          ? _approvedContracts
          : network == "BSC"
              ? _approvedContractsBSC
              : network == "ERC"
                  ? _approvedContractsETH
                  : [];
    });
    return SingleChildScrollView(
      physics: ScrollPhysics(),
      child: Column(
        children: [
          Container(
            color: Colors.white,
            margin: EdgeInsets.all(10),
            padding: EdgeInsets.all(15),
            child: ListView.builder(
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemCount: ls.length,
                itemBuilder: (BuildContext context, int index) {
                  return Column(
                    children: [
                      Row(
                        children: [
                          Text(
                            ls[index].symbol.length > 7 ? ls[index].symbol.substring(0, 7) : ls[index].symbol ?? "",
                            style: TextStyle(
                              color: Color(0xff212529),
                              fontSize: MediaQuery.of(context).size.width * .035,
                            ),
                          ),
                          SizedBox(
                            width: MediaQuery.of(context).size.width * .02,
                          ),
                          Text(
                            ls[index].name.length > 35 ? ls[index].name.substring(0, 35) : ls[index].name ?? "" ?? "",
                            style: TextStyle(
                              color: Color(0xff75758F),
                              fontSize: MediaQuery.of(context).size.width * .035,
                            ),
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          SvgPicture.asset(
                            ls[index].network.toString().toLowerCase().contains("bep") ? "images/bep.svg" : "images/ETH.svg",
                            height: 35,
                            width: 35,
                          ),
                          SizedBox(
                            width: MediaQuery.of(context).size.width * .023,
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width * .44,
                            child: Column(
                              children: [
                                Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    ls[index].network ?? "",
                                    style: TextStyle(
                                      color: Color(0xff212529),
                                      fontSize: MediaQuery.of(context).size.width * .035,
                                    ),
                                  ),
                                ),
                                Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    formatAddress(ls[index].contract ?? ""),
                                    style: TextStyle(
                                      color: Color(0xff75758F),
                                      fontSize: MediaQuery.of(context).size.width * .035,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            width: MediaQuery.of(context).size.width * .22,
                          ),
                          Container(
                            child: GestureDetector(
                                onTap: () {
                                  displayDeleteContactDialog(ls[index]);
                                },
                                child: Image.asset(
                                  "images/Groupcor.png",
                                  height: MediaQuery.of(context).size.height * 0.035,
                                  width: MediaQuery.of(context).size.width * 0.058,
                                )),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: MediaQuery.of(context).size.height * .017,
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width * 0.92,
                        decoration: BoxDecoration(
                          border: Border(
                            bottom: BorderSide(color: Color(0xffADADC8), width: 1.5),
                          ),
                        ),
                      ),
                    ],
                  );
                }),
          ),
        ],
      ),
    );
  }
}

formatAddress(String address) {
  var display = address.substring(0, 7) + '...' + address.substring(35);
  return display;
}

displayDeleteContactDialog(ApprovedContract approved) {
  showDialog(
      barrierDismissible: false,
      context: MyApp.materialKey.currentContext ?? OneContext() as BuildContext,
      builder: (context) {
        return StatefulBuilder(builder: (BuildContext context, setState) {
          return Dialog(
              backgroundColor: Colors.transparent,
              insetPadding: EdgeInsets.zero,
              child: Container(
                height: MediaQuery.of(context).size.height * .37,
                width: MediaQuery.of(context).size.height * .4,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(40.0),
                ),
                child: Stack(
                  children: [
                    SingleChildScrollView(
                      child: Column(
                        children: [
                          Align(
                            child: Padding(
                              padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.02, left: MediaQuery.of(context).size.width * 0.65),
                              child: IconButton(
                                  onPressed: () async {
                                    Navigator.of(context).pop();
                                  },
                                  icon: Icon(
                                    Icons.close,
                                    color: Color(0xFF75758F),
                                    size: MediaQuery.of(context).size.height * 0.04,
                                  )),
                            ),
                          ),
                          SizedBox(
                            height: MediaQuery.of(context).size.height * .01,
                          ),
                          Text(
                            "Are you sure to revoke this",
                            textAlign: TextAlign.center,
                            style: GoogleFonts.poppins(
                              color: Color(0xFF1F2337),
                              fontSize: 18,
                              fontWeight: FontWeight.w600,
                              fontStyle: FontStyle.normal,
                            ),
                          ),
                          Text(
                            "smartcontact",
                            textAlign: TextAlign.center,
                            style: GoogleFonts.poppins(
                              color: Color(0xFF1F2337),
                              fontSize: 18,
                              fontWeight: FontWeight.w600,
                              fontStyle: FontStyle.normal,
                            ),
                          ),
                          SizedBox(
                            height: 12,
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width * 0.75,
                            child: TextFormField(
                              style: TextStyle(fontSize: MediaQuery.of(context).size.width * 0.045),
                              textAlignVertical: TextAlignVertical.center,
                              textAlign: TextAlign.left,
                              keyboardType: TextInputType.visiblePassword,
                              obscureText: _obscureText2,
                              controller: _passwordController,
                              onChanged: (val) {
                                TextSelection previousSelection = _passwordController.selection;
                                _passwordController.text = val;

                                _passwordController.selection = previousSelection;

                                setState(() {
                                  passwrong = false;
                                });
                              },
                              decoration: InputDecoration(
                                  hintText: "TRANSACTION PASSWORD",
                                  hintStyle: GoogleFonts.poppins(color: Color(0xFF75758F), fontWeight: FontWeight.w600, fontStyle: FontStyle.normal, fontSize: MediaQuery.of(context).size.height * 0.016),
                                  fillColor: Colors.white,
                                  focusColor: Colors.white,
                                  hoverColor: Colors.grey,
                                  filled: true,
                                  isDense: true,
                                  prefixIcon: Padding(
                                    padding: EdgeInsets.all(MediaQuery.of(context).size.width * 0.017),
                                    child: SvgPicture.asset(
                                      "images/keyIcon.svg",
                                      color: Colors.orange,
                                      height: MediaQuery.of(context).size.width * 0.06,
                                      width: MediaQuery.of(context).size.width * 0.06,
                                    ),
                                  ),
                                  suffixIcon: Padding(
                                    padding: EdgeInsets.all(MediaQuery.of(context).size.width * 0.03),
                                    child: GestureDetector(
                                      onTap: () {
                                        setState(() {
                                          _obscureText2 = !_obscureText2;
                                        });
                                      },
                                      child: SvgPicture.asset(
                                        _obscureText2 ? "images/visibility-icon-off.svg" : "images/visibility-icon-on.svg",
                                        height: MediaQuery.of(context).size.height * 0.0001,
                                        width: MediaQuery.of(context).size.height * 0.0001,
                                      ),
                                    ),
                                  ),
                                  contentPadding: EdgeInsets.symmetric(vertical: 5, horizontal: 5),
                                  enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Color(0xFFD6D6E8))),
                                  focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Color(0xFFD6D6E8))),
                                  errorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red)),
                                  focusedErrorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(60), borderSide: BorderSide(color: Colors.red))),
                            ),
                          ),
                          SizedBox(
                            height: 12,
                          ),
                          ElevatedButton(
                            child: Padding(
                              padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.06, right: MediaQuery.of(context).size.width * 0.06, top: MediaQuery.of(context).size.height * 0.02, bottom: MediaQuery.of(context).size.height * 0.02),
                              child: Text(
                                "I Revoke",
                                style: GoogleFonts.poppins(fontWeight: FontWeight.w700, fontSize: MediaQuery.of(context).size.height * 0.024, color: Colors.white),
                              ),
                            ),
                            onPressed: () async {
                              var result = await web3Service.revokeAllowance(approved.contract ?? "", approved.network ?? "", _passwordController.text, approved.spender ?? "");
                              if (result == "Wrong Pass") {
                                setState(() {
                                  passwrong = true;
                                });
                              }
                              Navigator.of(context).pop();
                            },
                            style: ButtonStyle(
                              backgroundColor: MaterialStateProperty.all(
                                const Color(0xFF4048FF),
                              ),
                              shape: MaterialStateProperty.all(
                                RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.08),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 15,
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ));
        });
      });
}

String formatAdress(String? address) {
  String result = address ?? "";
  if (result.length != 0) {
    return result.substring(0, 4) + '...........................' + result.substring(result.length - 3, result.length);
  }
  return result;
}
