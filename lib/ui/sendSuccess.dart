import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:ihave/controllers/LoginController.dart';
import 'package:ihave/controllers/walletController.dart';
import 'package:ihave/service/apiService.dart';
import 'package:ihave/ui/WelcomeScreen.dart';
import 'package:ihave/ui/send.dart';
import 'package:ihave/ui/staticElements/headerNavigation.dart';
import 'package:ihave/widgets/customAppBar.dart';
import 'package:provider/provider.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'dashboard.dart';
import 'package:url_launcher/url_launcher.dart';

class SendSuccess extends StatefulWidget {
  SendSuccess({Key? key}) : super(key: key);

  @override
  _SendSuccessState createState() => _SendSuccessState();
}

class _SendSuccessState extends State<SendSuccess> {
  APIService apiService = LoginController.apiService;
  String crypto = "";
  String amount = "";
  String transactionHash = "";
  String linkTransaction = "";
  bool _isVisible = false;
  bool _isProfileVisible = false;
  bool _isWalletVisible = false;
  bool _isNotificationVisible = false;

  @override
  void initState() {
    setState(() {
      crypto = Provider.of<WalletController>(context, listen: false).selectedCrypto.toString();
      amount = Provider.of<WalletController>(context, listen: false).amount.toString();
      transactionHash = Provider.of<WalletController>(context, listen: false).transactionHash.toString();
      linkTransaction = Provider.of<WalletController>(context, listen: false).linkTransaction.toString();
    });
  }

  @override
  Widget build(BuildContext context) {
    setState(() {
      _isVisible = Provider.of<WalletController>(context, listen: true).isSideMenuVisible;
      _isProfileVisible = Provider.of<WalletController>(context, listen: true).isProfileVisible;
      _isWalletVisible = Provider.of<WalletController>(context, listen: true).isWalletVisible;
      _isNotificationVisible = Provider.of<WalletController>(context, listen: true).isNotificationVisible;
    });
    return Scaffold(
      appBar: CustomAppBar(),
      body: Stack(children: [
        Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          color: Color(0xFF20c997),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(20, 80, 20, 20),
                child: SvgPicture.asset(
                  'images/success_info.svg',
                  alignment: Alignment.center,
                  width: 60,
                  height: 60,
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(20, 20, 20, 20),
                child: Text(
                  "Successfully sent!",
                  style: GoogleFonts.poppins(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 18),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(20, 20, 20, 20),
                child: Text(
                  "you sent $amount $crypto",
                  style: GoogleFonts.poppins(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 14),
                ),
              ),
              Container(
                  height: MediaQuery.of(context).size.height * 0.15,
                  width: MediaQuery.of(context).size.width * 0.9,
                  decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.5),
                        spreadRadius: 5,
                        blurRadius: 7,
                        offset: Offset(0, 3), // changes position of shadow
                      ),
                    ],
                    color: Colors.white,
                    border: Border.all(
                      color: Colors.white,
                    ),
                    borderRadius: BorderRadius.all(Radius.circular(20)),
                  ),
                  child: Column(children: [
                    Text(
                      "your transaction hash:",
                      style: GoogleFonts.poppins(fontSize: 16, fontWeight: FontWeight.bold, color: Colors.black54),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: InkWell(
                        onTap: () => {
                          if (linkTransaction.isNotEmpty)
                            {
                              launch(linkTransaction),
                            }
                        },
                        child: Text(
                          transactionHash,
                          style: GoogleFonts.poppins(fontSize: 14, color: Colors.blue, fontWeight: FontWeight.bold, decoration: TextDecoration.underline),
                        ),
                      ),
                    ),
                    Center(
                      child: InkWell(
                        onTap: () {
                          Clipboard.setData(ClipboardData(text: transactionHash));
                          Fluttertoast.showToast(msg: "Copied to clipboard", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.BOTTOM, timeInSecForIosWeb: 1);
                        },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            SvgPicture.asset(
                              'images/recieve.svg',
                              alignment: Alignment.center,
                              width: 15,
                              height: 15,
                              color: Colors.black,
                            ),
                            Padding(
                              padding: const EdgeInsets.all(5.0),
                              child: Text("copy"),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ])),
              Padding(
                padding: const EdgeInsets.fromLTRB(15, 30, 15, 0),
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(5.0),
                      child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                              minimumSize: Size(MediaQuery.of(context).size.width * 0.7, MediaQuery.of(context).size.height * 0.05),
                              side: BorderSide(color: Colors.white),
                              primary: Colors.white,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(18.0),
                              )),
                          onPressed: () {},
                          child: Text(
                            "View transaction history",
                            style: GoogleFonts.poppins(fontSize: 14, color: Colors.blue, fontWeight: FontWeight.bold),
                          )),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(5.0),
                      child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                              minimumSize: Size(MediaQuery.of(context).size.width * 0.7, MediaQuery.of(context).size.height * 0.05),
                              side: BorderSide(color: Colors.white),
                              primary: Colors.white,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(18.0),
                              )),
                          onPressed: () {
                            WidgetsBinding.instance!.addPostFrameCallback((_) {
                              Navigator.pushReplacement(context, MaterialPageRoute(builder: (_) => WelcomeScreen()));
                            });
                          },
                          child: Text(
                            "Return to wallet",
                            style: GoogleFonts.poppins(fontSize: 14, color: Colors.blue, fontWeight: FontWeight.bold),
                          )),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(5.0),
                      child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                              minimumSize: Size(MediaQuery.of(context).size.width * 0.7, MediaQuery.of(context).size.height * 0.05),
                              side: BorderSide(color: Colors.white),
                              primary: Colors.white,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(18.0),
                              )),
                          onPressed: () {
                            WidgetsBinding.instance!.addPostFrameCallback((_) {
                              Navigator.pushReplacement(context, MaterialPageRoute(builder: (_) => Send()));
                            });
                          },
                          child: Text(
                            "Make another transaction",
                            style: GoogleFonts.poppins(fontSize: 14, color: Colors.blue, fontWeight: FontWeight.bold),
                          )),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        HeaderNavigation(_isVisible, _isWalletVisible, _isNotificationVisible)
      ]),
    );
  }
}