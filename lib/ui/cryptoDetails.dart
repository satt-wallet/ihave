import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:ihave/controllers/LoginController.dart';
import 'package:ihave/controllers/walletController.dart';
import 'package:ihave/service/apiService.dart';
import 'package:ihave/ui/WelcomeScreen.dart';
import 'package:ihave/ui/staticElements/headerNavigation.dart';
import 'package:ihave/widgets/customAppBar.dart';
import 'package:ihave/widgets/customBottomBar.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:simple_html_css/simple_html_css.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import 'package:syncfusion_flutter_charts/sparkcharts.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:webview_flutter/webview_flutter.dart';

import '../model/crypto.dart';
import '../util/utils.dart';

class CryptoDetails extends StatefulWidget {
  const CryptoDetails({Key? key}) : super(key: key);

  @override
  _CryptoDetailsState createState() => _CryptoDetailsState();
}

class _CryptoDetailsState extends State<CryptoDetails> {
  APIService apiService = new APIService();
  String duration = "max";
  String coinId = "";
  bool allClicked = true;
  bool _isVisible = false;
  bool _isProfileVisible = false;
  bool _isWalletVisible = false;
  bool _isNotificationVisible = false;
  TooltipBehavior? _tooltip;
  late Future dataFuture;
  late Future dataHistory;
  late CrosshairBehavior _crosshairBehavior;
  late TrackballBehavior _trackballBehavior;
  List<ChartData> _data = [];
  List<Crypto> _cryptoList = [];
  String cryptoSelected = "";
  String cryptoSelectedNetwork = "";
  GlobalKey _refreshKeyCryptoDetails = GlobalKey<RefreshIndicatorState>();
  String? firebaseToken = " ";

  void _launchURL(url) async {
    if (!await launch(url)) throw 'Could not launch $url';
  }

  Future<void> _refresh() async {
    setState(() {
      dataFuture = apiService.getCryptoData(coinId);
      dataHistory = apiService.getCryptoHistory(coinId, duration);
    });
  }

  @override
  void initState() {
    Provider.of<WalletController>(context, listen: false).isSelectCryptoMode = true;
    _refreshKeyCryptoDetails = GlobalKey<RefreshIndicatorState>();
    _cryptoList = Provider.of<WalletController>(context, listen: false).cryptos;
    coinId = Provider.of<LoginController>(context, listen: false).marketCupCrypto;
    _tooltip = TooltipBehavior(enable: true);
    duration = "max";
    allClicked = true;
    _crosshairBehavior = CrosshairBehavior(enable: true);
    dataFuture = apiService.getCryptoData(coinId);
    dataHistory = apiService.getCryptoHistory(coinId, duration);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    setState(() {
      _isVisible = Provider.of<WalletController>(context, listen: true).isSideMenuVisible;
      _isProfileVisible = Provider.of<WalletController>(context, listen: true).isProfileVisible;
      _isWalletVisible = Provider.of<WalletController>(context, listen: true).isWalletVisible;
      _isNotificationVisible = Provider.of<WalletController>(context, listen: true).isNotificationVisible;
    });
    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: Container(
        child: Scaffold(
          resizeToAvoidBottomInset: false,
          body: Container(
            color: Colors.white,
            child: RefreshIndicator(
              onRefresh: _refresh,
              child: Stack(
                children: [
                  SingleChildScrollView(
                    child: Column(
                      children: <Widget>[
                        Container(
                          child: Column(
                            children: <Widget>[
                              Padding(
                                padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.02, right: MediaQuery.of(context).size.width * 0.042),
                                child: Align(
                                  alignment: Alignment.bottomRight,
                                  child: InkWell(
                                    child: Text(
                                      "< Back",
                                      style: GoogleFonts.poppins(color: Colors.black, letterSpacing: 0.2, fontWeight: FontWeight.w600, fontSize: MediaQuery.of(context).size.width * 0.035),
                                    ),
                                    onTap: () {
                                      setState(() {
                                        Provider.of<WalletController>(context, listen: false).selectedIndex = 2;
                                      });
                                      Provider.of<WalletController>(context, listen: false).toggleSelectedCrypto(false);
                                      gotoPageGlobal(context, WelcomeScreen());
                                    },
                                  ),
                                ),
                              ),
                              FutureBuilder(
                                  future: dataFuture,
                                  builder: (context, AsyncSnapshot snapshot) {
                                    if (snapshot.hasData && snapshot.connectionState == ConnectionState.done) {
                                      return Column(
                                        children: [
                                          Padding(
                                            padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.03),
                                            child: Row(
                                              children: [
                                                Image.network(
                                                  snapshot.data["image"]["small"],
                                                  height: MediaQuery.of(context).size.height * 0.07,
                                                  width: MediaQuery.of(context).size.width * 0.15,
                                                ),
                                                Padding(
                                                  padding: const EdgeInsets.only(left: 2.0),
                                                  child: Column(
                                                    children: [
                                                      Text(
                                                        snapshot.data["name"],
                                                        textAlign: TextAlign.left,
                                                        style: GoogleFonts.poppins(fontWeight: FontWeight.w700, color: Color(0xFF1F2337), fontStyle: FontStyle.normal, fontSize: MediaQuery.of(context).size.height * 0.030),
                                                      ),
                                                      Padding(
                                                        padding: const EdgeInsets.only(left: 8.0),
                                                        child: Text(
                                                          snapshot.data["market_data"]["current_price"]["usd"] != null ? "${formatDollarBalance((snapshot.data["market_data"]["current_price"]["usd"]).toDouble()).toString()}" : "No data",
                                                          textAlign: TextAlign.left,
                                                          style: GoogleFonts.poppins(color: Color(0xFF75758F), fontWeight: FontWeight.w700, fontStyle: FontStyle.normal, fontSize: MediaQuery.of(context).size.height * 0.030),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                          )
                                        ],
                                      );
                                    } else {
                                      return Column(
                                        children: [
                                          Text(
                                            "--",
                                            style: GoogleFonts.poppins(color: Colors.white, fontWeight: FontWeight.w700, fontStyle: FontStyle.normal, fontSize: MediaQuery.of(context).size.height * 0.035),
                                          ),
                                          Text(
                                            "\$ --",
                                            style: GoogleFonts.poppins(color: Colors.white, fontWeight: FontWeight.w700, fontStyle: FontStyle.normal, fontSize: MediaQuery.of(context).size.height * 0.035),
                                          )
                                        ],
                                      );
                                    }
                                  }),
                            ],
                          ),
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width,
                          color: Colors.white,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Padding(
                                padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.014, left: MediaQuery.of(context).size.width * 0.04),
                                child: Text(
                                  "Price",
                                  style: GoogleFonts.poppins(fontWeight: FontWeight.w700, fontStyle: FontStyle.normal, fontSize: MediaQuery.of(context).size.height * 0.021),
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.03),
                                child: Row(
                                  children: <Widget>[
                                    Spacer(),
                                    SizedBox(
                                      height: MediaQuery.of(context).size.height * 0.04,
                                      width: MediaQuery.of(context).size.width * 0.17,
                                      child: ElevatedButton(
                                        onPressed: () {
                                          setState(() {
                                            duration = "1";
                                            dataHistory = apiService.getCryptoHistory(coinId, duration);
                                          });
                                        },
                                        style: ButtonStyle(
                                            elevation: MaterialStateProperty.all(0),
                                            backgroundColor: MaterialStateProperty.all(duration == "1" ? Color(0XFF4048FF) : Color(0XFFF6F6FF)),
                                            shape: MaterialStateProperty.all(RoundedRectangleBorder(borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.08)))),
                                        child: Text(
                                          "1D",
                                          style: GoogleFonts.poppins(color: duration == "1" ? Colors.white : Color(0XFF212529), fontWeight: FontWeight.w600, fontStyle: FontStyle.normal, fontSize: MediaQuery.of(context).size.height * 0.02),
                                        ),
                                      ),
                                    ),
                                    Spacer(),
                                    SizedBox(
                                      height: MediaQuery.of(context).size.height * 0.04,
                                      width: MediaQuery.of(context).size.width * 0.17,
                                      child: ElevatedButton(
                                          onPressed: () {
                                            setState(() {
                                              duration = "7";
                                              dataHistory = apiService.getCryptoHistory(coinId, duration);
                                            });
                                          },
                                          style: ButtonStyle(
                                              elevation: MaterialStateProperty.all(0),
                                              backgroundColor: MaterialStateProperty.all(duration == "7" ? Color(0XFF4048FF) : Color(0XFFF6F6FF)),
                                              shape: MaterialStateProperty.all(RoundedRectangleBorder(borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.08)))),
                                          child: Text(
                                            "1W",
                                            style: GoogleFonts.poppins(color: duration == "7" ? Colors.white : Color(0XFF212529), fontWeight: FontWeight.w600, fontStyle: FontStyle.normal, fontSize: MediaQuery.of(context).size.height * 0.02),
                                          )),
                                    ),
                                    Spacer(),
                                    SizedBox(
                                      height: MediaQuery.of(context).size.height * 0.04,
                                      width: MediaQuery.of(context).size.width * 0.17,
                                      child: ElevatedButton(
                                          onPressed: () {
                                            setState(() {
                                              duration = "30";
                                              dataHistory = apiService.getCryptoHistory(coinId, duration);
                                            });
                                          },
                                          style: ButtonStyle(
                                              elevation: MaterialStateProperty.all(0),
                                              backgroundColor: MaterialStateProperty.all(duration == "30" ? Color(0XFF4048FF) : Color(0XFFF6F6FF)),
                                              shape: MaterialStateProperty.all(RoundedRectangleBorder(borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.08)))),
                                          child: Text(
                                            "1M",
                                            style: GoogleFonts.poppins(color: duration == "30" ? Colors.white : Color(0XFF212529), fontWeight: FontWeight.w600, fontStyle: FontStyle.normal, fontSize: MediaQuery.of(context).size.height * 0.02),
                                          )),
                                    ),
                                    Spacer(),
                                    SizedBox(
                                      height: MediaQuery.of(context).size.height * 0.04,
                                      width: MediaQuery.of(context).size.width * 0.17,
                                      child: ElevatedButton(
                                          onPressed: () {
                                            setState(() {
                                              duration = "max";
                                              allClicked = false;
                                              dataHistory = apiService.getCryptoHistory(coinId, duration);
                                            });
                                          },
                                          style: ButtonStyle(
                                              elevation: MaterialStateProperty.all(0),
                                              backgroundColor: MaterialStateProperty.all((duration == "max" && !allClicked) ? Color(0XFF4048FF) : Color(0xFFF6F6FF)),
                                              shape: MaterialStateProperty.all(RoundedRectangleBorder(borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.08)))),
                                          child: Text(
                                            "1Y",
                                            style: GoogleFonts.poppins(color: (duration == "max" && !allClicked) ? Colors.white : Color(0XFF212529), fontWeight: FontWeight.w600, fontStyle: FontStyle.normal, fontSize: MediaQuery.of(context).size.height * 0.02),
                                          )),
                                    ),
                                    Spacer(),
                                    SizedBox(
                                      height: MediaQuery.of(context).size.height * 0.04,
                                      width: MediaQuery.of(context).size.width * 0.17,
                                      child: ElevatedButton(
                                          onPressed: () {
                                            setState(() {
                                              duration = "max";
                                              allClicked = true;
                                              dataHistory = apiService.getCryptoHistory(coinId, duration);
                                            });
                                          },
                                          style: ButtonStyle(
                                              elevation: MaterialStateProperty.all(0),
                                              backgroundColor: MaterialStateProperty.all(duration == "max" && allClicked ? Color(0XFF4048FF) : Color(0xFFF6F6FF)),
                                              shape: MaterialStateProperty.all(RoundedRectangleBorder(borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.08)))),
                                          child: Text(
                                            "All",
                                            style: GoogleFonts.poppins(color: duration == "max" && allClicked ? Colors.white : Color(0XFF212529), fontWeight: FontWeight.w600, fontStyle: FontStyle.normal, fontSize: MediaQuery.of(context).size.height * 0.02),
                                          )),
                                    ),
                                    Spacer(),
                                  ],
                                ),
                              ),
                              FutureBuilder(
                                future: dataHistory,
                                builder: (context, AsyncSnapshot snapshot) {
                                  if (snapshot.hasData && snapshot.connectionState == ConnectionState.done) {
                                    _data.clear();
                                    for (var i = 0; i < snapshot.data["prices"].length; i++) {
                                      var date = DateTime.fromMillisecondsSinceEpoch(snapshot.data["prices"][i][0]);
                                      final DateFormat displayFormatter = DateFormat('yyyy-MM-dd HH:mm:ss.SSS');
                                      final DateFormat serverFormatter = duration == "max" ? DateFormat('dd/MM/yyyy') : DateFormat('dd/MM');
                                      final DateTime displayDate = displayFormatter.parse(date.toString());
                                      final String formatted = serverFormatter.format(displayDate);
                                      _data.add(new ChartData(formatted, snapshot.data["prices"][i][1]));
                                    }

                                    if (snapshot.data["prices"].length == 1) {
                                      return Padding(
                                        padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.075, bottom: MediaQuery.of(context).size.height * 0.075),
                                        child: Center(
                                          child: Text(
                                            "No data",
                                            style: GoogleFonts.poppins(fontWeight: FontWeight.w700, fontSize: MediaQuery.of(context).size.height * 0.025),
                                          ),
                                        ),
                                      );
                                    } else {
                                      return Column(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: <Widget>[
                                          SfCartesianChart(
                                            plotAreaBorderWidth: 0,
                                            trackballBehavior: TrackballBehavior(
                                                enable: true,
                                                tooltipSettings: InteractiveTooltip(
                                                  enable: true,
                                                ),
                                                builder: (context, TrackballDetails trackballDetails) {
                                                  return Container(
                                                    height: 30,
                                                    width: 100,
                                                    decoration: BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(10)), color: Color(0XFF00CC9E)),
                                                    child: Center(
                                                      child: Text(
                                                        "${trackballDetails.point!.y.toString().substring(0, trackballDetails.point!.y.toString().indexOf('.') + 3)}",
                                                        style: GoogleFonts.poppins(fontWeight: FontWeight.w500, fontStyle: FontStyle.normal, fontSize: 10.5, color: Colors.white),
                                                      ),
                                                    ),
                                                  );
                                                }),
                                            zoomPanBehavior: ZoomPanBehavior(enablePinching: true, zoomMode: ZoomMode.x, enableDoubleTapZooming: true, maximumZoomLevel: 0.1),
                                            primaryXAxis: CategoryAxis(
                                              majorGridLines: MajorGridLines(width: 0),
                                              axisLine: AxisLine(width: 0),
                                            ),
                                            primaryYAxis: NumericAxis(
                                              majorGridLines: MajorGridLines(width: 0),
                                              axisLine: AxisLine(width: 0),
                                              isVisible: false,
                                              interactiveTooltip: InteractiveTooltip(enable: true),
                                            ),
                                            crosshairBehavior: CrosshairBehavior(enable: true, activationMode: ActivationMode.singleTap, lineColor: Color(0XFF00CC9E), lineType: CrosshairLineType.horizontal, lineDashArray: <double>[5, 5], lineWidth: 2),
                                            series: <ChartSeries>[
                                              AreaSeries<ChartData, String>(
                                                  dataSource: _data,
                                                  xValueMapper: (ChartData data, _) => data.x,
                                                  yValueMapper: (ChartData data, _) => data.y,
                                                  animationDuration: 1000,
                                                  borderWidth: 3,
                                                  gradient: const LinearGradient(colors: <Color>[Color.fromRGBO(0, 204, 158, 1), Color.fromRGBO(0, 204, 158, 0)], begin: const FractionalOffset(0.0, 0.0), end: const FractionalOffset(0.0, 1.0), stops: [0.0, 1.0], tileMode: TileMode.clamp))
                                            ],
                                          )
                                        ],
                                      );
                                    }
                                  } else {
                                    return Padding(
                                      padding: EdgeInsets.only(
                                        top: MediaQuery.of(context).size.height * 0.08,
                                        bottom: MediaQuery.of(context).size.height * 0.08,
                                      ),
                                      child: Center(
                                        child: SizedBox(
                                          height: 15,
                                          width: 15,
                                          child: CircularProgressIndicator(
                                            color: Colors.blueAccent,
                                          ),
                                        ),
                                      ),
                                    );
                                  }
                                },
                              ),
                              Padding(
                                padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.025),
                                child: FutureBuilder(
                                    future: dataFuture,
                                    builder: (context, AsyncSnapshot snapshot) {
                                      if (snapshot.hasData && snapshot.connectionState == ConnectionState.done) {
                                        return Column(
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: <Widget>[
                                            const Divider(),
                                            Row(
                                              children: <Widget>[
                                                Padding(
                                                  padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.03),
                                                  child: Text(
                                                    "MARKET CAP",
                                                    style: GoogleFonts.poppins(color: Color(0XFF75758F), fontWeight: FontWeight.w600, fontStyle: FontStyle.normal, fontSize: MediaQuery.of(context).size.height * 0.018),
                                                  ),
                                                ),
                                                Spacer(),
                                                Padding(
                                                  padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.04),
                                                  child: Column(
                                                    crossAxisAlignment: CrossAxisAlignment.end,
                                                    children: <Widget>[
                                                      Text(
                                                        snapshot.data["market_data"]["market_cap"]["usd"] != null ? "${formatDollarBalance(snapshot.data["market_data"]["market_cap"]["usd"].toDouble())}" : "No data",
                                                        style: GoogleFonts.poppins(fontWeight: FontWeight.w600, fontStyle: FontStyle.normal, fontSize: MediaQuery.of(context).size.height * 0.019, color: Color(0xFF212529)),
                                                      ),
                                                      Text(
                                                        snapshot.data["market_data"]["market_cap_change_percentage_24h"] == null
                                                            ? "No data"
                                                            : (snapshot.data["market_data"]["market_cap_change_percentage_24h"].toString()[0] == "-"
                                                                ? (snapshot.data["market_data"]["market_cap_change_percentage_24h"].toString().substring(snapshot.data["market_data"]["market_cap_change_percentage_24h"].toString().indexOf('.')).length > 3
                                                                    ? "${snapshot.data["market_data"]["market_cap_change_percentage_24h"].toString().substring(0, snapshot.data["market_data"]["market_cap_change_percentage_24h"].toString().indexOf('.') + 3)}%"
                                                                    : "${snapshot.data["market_data"]["market_cap_change_percentage_24h"].toString()}%")
                                                                : (snapshot.data["market_data"]["market_cap_change_percentage_24h"].toString().substring(snapshot.data["market_data"]["market_cap_change_percentage_24h"].toString().indexOf('.')).length > 3
                                                                    ? "${snapshot.data["market_data"]["market_cap_change_percentage_24h"].toString().substring(0, snapshot.data["market_data"]["market_cap_change_percentage_24h"].toString().indexOf('.') + 3)}%"
                                                                    : "${snapshot.data["market_data"]["market_cap_change_percentage_24h"].toString()}%")),
                                                        style: GoogleFonts.poppins(
                                                          fontWeight: FontWeight.w600,
                                                          fontStyle: FontStyle.normal,
                                                          fontSize: MediaQuery.of(context).size.height * 0.017,
                                                          color: snapshot.data["market_data"]["market_cap_change_percentage_24h"].toString()[0] == "-" ? Colors.red : (snapshot.data["market_data"]["market_cap_change_percentage_24h"] == null ? Colors.black : Color(0XFF00CC9E)),
                                                        ),
                                                      )
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                            const Divider(),
                                            Row(
                                              children: <Widget>[
                                                Padding(
                                                  padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.03),
                                                  child: Text(
                                                    "FULLY DILUTED\nMARKET CAP",
                                                    style: GoogleFonts.poppins(color: Color(0XFF75758F), fontWeight: FontWeight.w600, fontStyle: FontStyle.normal, fontSize: MediaQuery.of(context).size.height * 0.018),
                                                  ),
                                                ),
                                                Spacer(),
                                                Padding(
                                                  padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.04),
                                                  child: Column(
                                                    crossAxisAlignment: CrossAxisAlignment.end,
                                                    children: <Widget>[
                                                      Text(
                                                        snapshot.data["market_data"]["fully_diluted_valuation"]["usd"] == null ? "No data" : "${formatDollarBalance(snapshot.data["market_data"]["fully_diluted_valuation"]["usd"].toDouble())}",
                                                        style: GoogleFonts.poppins(fontWeight: FontWeight.w600, fontStyle: FontStyle.normal, fontSize: MediaQuery.of(context).size.height * 0.019, color: Color(0xFF212529)),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                            const Divider(),
                                            Row(
                                              children: <Widget>[
                                                Padding(
                                                  padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.03),
                                                  child: Text(
                                                    "VOLUME 24H",
                                                    style: GoogleFonts.poppins(color: Color(0XFF75758F), fontWeight: FontWeight.w600, fontStyle: FontStyle.normal, fontSize: MediaQuery.of(context).size.height * 0.018),
                                                  ),
                                                ),
                                                Spacer(),
                                                Padding(
                                                  padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.04),
                                                  child: Column(
                                                    crossAxisAlignment: CrossAxisAlignment.end,
                                                    children: <Widget>[
                                                      Text(
                                                        snapshot.data["market_data"]["total_volume"]["usd"] == null ? "No data" : "${formatDollarBalance(snapshot.data["market_data"]["total_volume"]["usd"].toDouble())}",
                                                        style: GoogleFonts.poppins(fontWeight: FontWeight.w600, fontStyle: FontStyle.normal, fontSize: MediaQuery.of(context).size.height * 0.019, color: Color(0xFF212529)),
                                                      ),
                                                      Text(
                                                        snapshot.data["market_data"]["price_change_percentage_24h"] == null
                                                            ? "No data"
                                                            : (snapshot.data["market_data"]["market_cap_change_percentage_24h"].toString()[0] == "-"
                                                                ? (snapshot.data["market_data"]["market_cap_change_percentage_24h"].toString().substring(snapshot.data["market_data"]["market_cap_change_percentage_24h"].toString().indexOf('.')).length > 3
                                                                    ? "${snapshot.data["market_data"]["market_cap_change_percentage_24h"].toString().substring(0, snapshot.data["market_data"]["market_cap_change_percentage_24h"].toString().indexOf('.') + 3)}%"
                                                                    : "${snapshot.data["market_data"]["market_cap_change_percentage_24h"].toString()}%")
                                                                : (snapshot.data["market_data"]["market_cap_change_percentage_24h"].toString().substring(snapshot.data["market_data"]["market_cap_change_percentage_24h"].toString().indexOf('.')).length > 3
                                                                    ? "${snapshot.data["market_data"]["market_cap_change_percentage_24h"].toString().substring(0, snapshot.data["market_data"]["market_cap_change_percentage_24h"].toString().indexOf('.') + 3)}%"
                                                                    : "${snapshot.data["market_data"]["market_cap_change_percentage_24h"].toString()}%")),
                                                        style: GoogleFonts.poppins(
                                                          fontWeight: FontWeight.w600,
                                                          fontStyle: FontStyle.normal,
                                                          fontSize: MediaQuery.of(context).size.height * 0.017,
                                                          color: snapshot.data["market_data"]["market_cap_change_percentage_24h"].toString()[0] == "-" ? Colors.red : (snapshot.data["market_data"]["market_cap_change_percentage_24h"] == null ? Colors.black : Color(0XFF00CC9E)),
                                                        ),
                                                      )
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                            const Divider(),
                                            Row(
                                              children: <Widget>[
                                                Padding(
                                                  padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.03),
                                                  child: Text(
                                                    "CIRCULATING SUPPLY",
                                                    style: GoogleFonts.poppins(color: Color(0XFF75758F), fontWeight: FontWeight.w600, fontStyle: FontStyle.normal, fontSize: MediaQuery.of(context).size.height * 0.018),
                                                  ),
                                                ),
                                                Spacer(),
                                                Padding(
                                                  padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.04),
                                                  child: Column(
                                                    crossAxisAlignment: CrossAxisAlignment.end,
                                                    children: <Widget>[
                                                      Text(
                                                        snapshot.data["market_data"]["circulating_supply"] == null ? "No data" : "${formatCryptoBalance(snapshot.data["market_data"]["circulating_supply"].toDouble(), "")}",
                                                        style: GoogleFonts.poppins(fontWeight: FontWeight.w600, fontStyle: FontStyle.normal, fontSize: MediaQuery.of(context).size.height * 0.019, color: Color(0xFF212529)),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                            const Divider(),
                                            Padding(
                                              padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.04, top: MediaQuery.of(context).size.height * 0.012, bottom: MediaQuery.of(context).size.height * 0.012),
                                              child: Text(
                                                "Description",
                                                style: GoogleFonts.poppins(fontWeight: FontWeight.w700, fontStyle: FontStyle.normal, fontSize: MediaQuery.of(context).size.height * 0.033),
                                              ),
                                            ),
                                            Padding(
                                              padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.04, right: MediaQuery.of(context).size.width * 0.04, bottom: MediaQuery.of(context).size.height * 0.03),
                                              child: RichText(
                                                  text: HTML.toTextSpan(context, snapshot.data["description"]["en"].toString(), linksCallback: (link) {
                                                _launchURL(link);
                                              })),
                                            )
                                          ],
                                        );
                                      } else {
                                        return Column(
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: <Widget>[
                                            const Divider(),
                                            Row(
                                              children: <Widget>[
                                                Padding(
                                                  padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.03),
                                                  child: Text(
                                                    "MARKET CAP",
                                                    style: GoogleFonts.poppins(color: Color(0XFF75758F), fontWeight: FontWeight.w600, fontStyle: FontStyle.normal, fontSize: MediaQuery.of(context).size.height * 0.018),
                                                  ),
                                                ),
                                                Spacer(),
                                                Padding(
                                                  padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.04),
                                                  child: Column(
                                                    crossAxisAlignment: CrossAxisAlignment.end,
                                                    children: <Widget>[
                                                      Text(
                                                        "\$ --",
                                                        style: GoogleFonts.poppins(fontWeight: FontWeight.w600, fontStyle: FontStyle.normal, fontSize: MediaQuery.of(context).size.height * 0.019, color: Color(0xFF212529)),
                                                      ),
                                                      Text(
                                                        "--%",
                                                        style: GoogleFonts.poppins(fontWeight: FontWeight.w600, fontStyle: FontStyle.normal, fontSize: MediaQuery.of(context).size.height * 0.017, color: Color(0xFF212529)),
                                                      )
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                            const Divider(),
                                            Row(
                                              children: <Widget>[
                                                Padding(
                                                  padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.03),
                                                  child: Text(
                                                    "FULLY DILUTED\nMARKET CAP",
                                                    style: GoogleFonts.poppins(color: Color(0XFF75758F), fontWeight: FontWeight.w600, fontStyle: FontStyle.normal, fontSize: MediaQuery.of(context).size.height * 0.018),
                                                  ),
                                                ),
                                                Spacer(),
                                                Padding(
                                                  padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.04),
                                                  child: Column(
                                                    crossAxisAlignment: CrossAxisAlignment.end,
                                                    children: <Widget>[
                                                      Text(
                                                        "\$ --",
                                                        style: GoogleFonts.poppins(fontWeight: FontWeight.w600, fontStyle: FontStyle.normal, fontSize: MediaQuery.of(context).size.height * 0.019, color: Color(0xFF212529)),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                            const Divider(),
                                            Row(
                                              children: <Widget>[
                                                Padding(
                                                  padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.03),
                                                  child: Text(
                                                    "VOLUME 24H",
                                                    style: GoogleFonts.poppins(color: Color(0XFF75758F), fontWeight: FontWeight.w600, fontStyle: FontStyle.normal, fontSize: MediaQuery.of(context).size.height * 0.018),
                                                  ),
                                                ),
                                                Spacer(),
                                                Padding(
                                                  padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.04),
                                                  child: Column(
                                                    crossAxisAlignment: CrossAxisAlignment.end,
                                                    children: <Widget>[
                                                      Text(
                                                        "\$ --",
                                                        style: GoogleFonts.poppins(fontWeight: FontWeight.w600, fontStyle: FontStyle.normal, fontSize: MediaQuery.of(context).size.height * 0.019, color: Color(0xFF212529)),
                                                      ),
                                                      Text(
                                                        "--%",
                                                        style: GoogleFonts.poppins(fontWeight: FontWeight.w600, fontStyle: FontStyle.normal, fontSize: MediaQuery.of(context).size.height * 0.017, color: Color(0xFF212529)),
                                                      )
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                            const Divider(),
                                            Row(
                                              children: <Widget>[
                                                Padding(
                                                  padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.03),
                                                  child: Text(
                                                    "CIRCULATING SUPPLY",
                                                    style: GoogleFonts.poppins(color: Color(0XFF75758F), fontWeight: FontWeight.w600, fontStyle: FontStyle.normal, fontSize: MediaQuery.of(context).size.height * 0.018),
                                                  ),
                                                ),
                                                Spacer(),
                                                Padding(
                                                  padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.04),
                                                  child: Column(
                                                    crossAxisAlignment: CrossAxisAlignment.end,
                                                    children: <Widget>[
                                                      Text(
                                                        "--",
                                                        style: GoogleFonts.poppins(fontWeight: FontWeight.w600, fontStyle: FontStyle.normal, fontSize: MediaQuery.of(context).size.height * 0.019, color: Color(0xFF212529)),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                            const Divider(),
                                          ],
                                        );
                                      }
                                    }),
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  HeaderNavigation(_isVisible, _isWalletVisible, _isNotificationVisible),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class ChartData {
  ChartData(this.x, this.y);

  final String? x;
  final double? y;
}
