import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:ihave/util/lightTheme.dart';
import 'package:ihave/widgets/customAppBar.dart';
import 'package:ihave/widgets/customBottomBar.dart';
import 'package:provider/provider.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:url_launcher/url_launcher.dart';
import '../controllers/walletController.dart';
import '../model/nft.dart';
import '../util/lightTheme.dart';
import '../util/lightTheme.dart';
import 'WelcomeScreen.dart';
import 'package:model_viewer_plus/model_viewer_plus.dart';

class NftSlider extends StatefulWidget {
  const NftSlider({Key? key}) : super(key: key);

  @override
  State<NftSlider> createState() => _NftSliderState();
}

class _NftSliderState extends State<NftSlider> {
  NFT? _nft;
  List<NFT> _nfts = [];

  @override
  void initState() {
    _nft = Provider.of<WalletController>(context, listen: false).nft;
    _nfts = Provider.of<WalletController>(context, listen: false).nfts;
    super.initState();
  }

  String formatIPFSImages(String image) {
    var finalURL = "";
    if (image.toLowerCase().contains("ipfs")) {
      finalURL = image.replaceAll("ipfs://", "https://ipfs.io/ipfs/");
      return finalURL;
    }
    return image;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        Provider.of<WalletController>(context, listen: false).selectedIndex = 4;
        Navigator.of(context).push(new MaterialPageRoute(builder: (context) => WelcomeScreen()));
        return Future.value(true);
      },
      child: Scaffold(
          resizeToAvoidBottomInset: false,
          appBar: CustomAppBar(),
          bottomNavigationBar: CustomBottomBar(),
          body: SingleChildScrollView(
              child: Stack(children: [
            Column(
              children: [
                Padding(
                  padding: EdgeInsets.only(top: 10, right: MediaQuery.of(context).size.width * 0.04),
                  child: Align(
                    alignment: Alignment.topRight,
                    child: InkWell(
                      focusColor: Colors.transparent,
                      hoverColor: Colors.transparent,
                      splashColor: Colors.transparent,
                      highlightColor: Colors.transparent,
                      onTap: () {
                        Provider.of<WalletController>(context, listen: false).selectedIndex = 4;
                        Navigator.of(context).push(new MaterialPageRoute(builder: (context) => WelcomeScreen()));
                      },
                      child: Text(
                        "< Back",
                        style: GoogleFonts.poppins(color: const Color(0XFF373737), fontWeight: FontWeight.w600, fontStyle: FontStyle.normal, fontSize: MediaQuery.of(context).size.height * 0.018),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.05),
                  child: Container(
                    height: MediaQuery.of(context).size.height * 0.45,
                    width: MediaQuery.of(context).size.width * 0.9,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(30)),
                    ),
                    child: _nft?.animation != null && (_nft?.animation?.toLowerCase().contains(".glb") == true || _nft?.animation?.toLowerCase().contains(".gltf") == true)
                        ? ModelViewer(
                            src: _nft?.animation ?? "",
                            alt: "Nft Glb preview",
                            ar: false,
                            arModes: ["webxr scene-viewer"],
                            autoRotate: false,
                            cameraControls: true,
                          )
                        : (_nft?.image?.toLowerCase().contains("svg") == true
                            ? ClipRRect(
                                borderRadius: BorderRadius.all(Radius.circular(30)),
                                child: new SvgPicture.network(
                                  formatIPFSImages(_nft?.image ?? "").replaceAll("https", "http"),
                                  fit: BoxFit.fill,
                                ),
                              )
                            : ClipRRect(
                                borderRadius: BorderRadius.all(Radius.circular(30)),
                                child: new Image.network(
                                  formatIPFSImages(_nft?.image ?? ""),
                                  fit: BoxFit.fill,
                                ),
                              )),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.04),
                  child: Text(_nft?.name ?? "",
                      style: GoogleFonts.poppins(
                        color: TextColor,
                        fontSize: 24,
                        fontWeight: FontWeight.w700,
                      )),
                ),
                Padding(
                  padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.02),
                  child: Text(_nft?.collection ?? "",
                      style: GoogleFonts.poppins(
                        color: TextColor,
                        fontSize: 10,
                        fontWeight: FontWeight.w600,
                      )),
                ),
              ],
            )
          ]))),
    );
  }

  void launchOpenSeaUrl(NFT? nft) async {
    var url = nft?.permalink ?? "";
    if (await canLaunch(url)) {
      await launch(url, forceSafariVC: true, forceWebView: false, enableJavaScript: true);
    } else {
      throw 'Could not launch $url';
    }
  }
}
