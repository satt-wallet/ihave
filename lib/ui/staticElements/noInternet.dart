import 'package:flutter/material.dart';
import 'dart:ui' as ui;

class NoInternet extends StatefulWidget {
  const NoInternet({Key? key}) : super(key: key);

  @override
  _NoInternetState createState() => _NoInternetState();
}

class _NoInternetState extends State<NoInternet> {
  @override
  Widget build(BuildContext context) {
    return new MediaQuery(
        data: new MediaQueryData.fromWindow(ui.window),
        child: new Directionality(
            textDirection: TextDirection.rtl,
            child: Padding(
              padding: const EdgeInsets.all(100.0),
              child: Container(
                color: Colors.white,
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Text("No Internet Connection", textDirection: TextDirection.ltr),
                    ElevatedButton(
                      onPressed: () async {},
                      child: Text("Retry", textDirection: TextDirection.ltr),
                    )
                  ],
                ),
              ),
            )));
  }
}
