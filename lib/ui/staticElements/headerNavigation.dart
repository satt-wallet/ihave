import 'package:flutter/cupertino.dart';
import 'package:ihave/controllers/walletController.dart';
import 'package:ihave/ui/notificationPage.dart';
import 'package:ihave/widgets/navBar.dart';
import 'package:provider/provider.dart';

import '../myprofile.dart';
import '../mywallet.dart';

class HeaderNavigation extends StatelessWidget {
  bool isVisible = false;

  bool isProfileVisible = false;
  bool isWalletVisible = false;
  bool isNotificationVisible = false;

  HeaderNavigation(bool isVisible, bool isWalletVisible, bool isNotificationVisible) {
    this.isVisible = isVisible;
    this.isWalletVisible = isWalletVisible;
    this.isNotificationVisible = isNotificationVisible;
  }

  Widget build(BuildContext context) {
    return Stack(children: [
      Visibility(visible: isVisible, child: NavBar(Provider.of<WalletController>(context, listen: true).isSideMenuVisible)),
    ]);
  }
}
