import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:ihave/controllers/LoginController.dart';
import 'package:ihave/controllers/walletController.dart';
import 'package:ihave/ui/staticElements/headerNavigation.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

import '../util/lightTheme.dart';
import '../widgets/customAppBar.dart';
import '../widgets/customBottomBar.dart';

class AuthMethods extends StatefulWidget {
  const AuthMethods({Key? key}) : super(key: key);

  @override
  State<AuthMethods> createState() => _AuthMethodsState();
}

class _AuthMethodsState extends State<AuthMethods> {
  bool _isVisible = false;
  bool _isProfileVisible = false;
  bool _isWalletVisible = false;
  bool _isNotificationVisible = false;
  String idOnSn = "";
  String idOnSn2 = "";

  bool isAddFacebook = true;

  bool isAddGoogle = true;

  bool isAlreadyUsed = false;

  bool isChangeSuccess = false;

  bool isDisconnectSuccessful = false;

  @override
  void initState() {
    setState(() {
      idOnSn = LoginController.apiService.idOnSn;
      idOnSn2 = LoginController.apiService.idOnSn2;
      isAddGoogle = (idOnSn2 != null && idOnSn2 != "" && idOnSn2 != "null");
      isAddFacebook = (idOnSn != null && idOnSn != "" && idOnSn != "null");
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    setState(() {
      _isVisible = Provider.of<WalletController>(context, listen: true).isSideMenuVisible;
      _isProfileVisible = Provider.of<WalletController>(context, listen: true).isProfileVisible;
      _isWalletVisible = Provider.of<WalletController>(context, listen: true).isWalletVisible;
      _isNotificationVisible = Provider.of<WalletController>(context, listen: true).isNotificationVisible;
    });
    return Scaffold(
      appBar: CustomAppBar(),
      bottomNavigationBar: CustomBottomBar(),
      body: WillPopScope(
        onWillPop: () {
          return Future.value(true);
        },
        child: Stack(
          children: [
            SingleChildScrollView(
              child: Column(
                children: [
                  Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Container(
                        alignment: Alignment.center,
                        child: Column(
                          children: [
                            Padding(
                              padding: EdgeInsets.all(MediaQuery.of(context).size.width * 0.02),
                              child: Align(
                                alignment: Alignment.topLeft,
                                child: Text(
                                  "Authentication\nmethod",
                                  style: MyTheme.iHaveTheme.textTheme.headline2,
                                ),
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.027, top: MediaQuery.of(context).size.width * 0.03),
                              child: Column(
                                children: [
                                  Align(
                                    alignment: Alignment.topLeft,
                                    child: Text(
                                      "Connect with one of your Social Networks\n",
                                      style: MyTheme.iHaveTheme.textTheme.bodyText1,
                                    ),
                                  ),
                                  Align(
                                    alignment: Alignment.topLeft,
                                    child: Text("You can access your Wallet through your social networks account. Of note, we do respect your privacy and do not use any of your data without your explicit consent :",
                                        style: GoogleFonts.poppins(fontSize: MediaQuery.of(context).size.width * 0.037, fontWeight: FontWeight.w500, color: Color(0xFF1F2337))),
                                  ),
                                  Column(
                                    children: [
                                      Visibility(
                                        child: Padding(
                                          padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.077),
                                          child: Text("Account already linked !", style: GoogleFonts.poppins(color: Colors.red)),
                                        ),
                                        visible: isAlreadyUsed && !isChangeSuccess,
                                      ),
                                      Visibility(
                                        child: Padding(
                                          padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.077),
                                          child: Text("Account linked with success.", style: GoogleFonts.poppins(color: Colors.green)),
                                        ),
                                        visible: isChangeSuccess && !isAlreadyUsed,
                                      ),
                                      Visibility(
                                        child: Padding(
                                          padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.077),
                                          child: Text("Successfully disconnected.", style: GoogleFonts.poppins(color: Colors.green)),
                                        ),
                                        visible: isDisconnectSuccessful && !isChangeSuccess && !isAlreadyUsed,
                                      ),
                                      isAddGoogle
                                          ? Padding(
                                              padding: EdgeInsets.only(top: (isAlreadyUsed || isChangeSuccess || isDisconnectSuccessful) ? MediaQuery.of(context).size.height * 0.011 : MediaQuery.of(context).size.height * 0.077),
                                              child: Container(
                                                  height: MediaQuery.of(context).size.height * 0.058,
                                                  width: MediaQuery.of(context).size.width * 0.65,
                                                  child: ElevatedButton(
                                                    style: ButtonStyle(
                                                      backgroundColor: MaterialStateProperty.all(
                                                        Color(0xFFEEEEEE),
                                                      ),
                                                      shape: MaterialStateProperty.all(
                                                        RoundedRectangleBorder(
                                                          borderRadius: BorderRadius.circular(MediaQuery.of(context).size.height * 0.07),
                                                        ),
                                                      ),
                                                    ),
                                                    onPressed: () async {
                                                      var result = await LoginController.apiService.removeSocial(Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "", "google");
                                                      if (result == "success") {
                                                        setState(() {
                                                          idOnSn = LoginController.apiService.idOnSn;
                                                          idOnSn2 = LoginController.apiService.idOnSn2;
                                                          isAddGoogle = (idOnSn2 != null && idOnSn2 != "" && idOnSn2 != "null");
                                                          isAddFacebook = (idOnSn != null && idOnSn != "" && idOnSn != "null");
                                                          isDisconnectSuccessful = true;
                                                        });
                                                        await Future.delayed(const Duration(seconds: 2), () {
                                                          setState(() {
                                                            isDisconnectSuccessful = false;
                                                          });
                                                        });
                                                      }
                                                    },
                                                    child: Row(children: [
                                                      SvgPicture.asset(
                                                        'images/google_image.svg',
                                                        alignment: Alignment.center,
                                                        width: MediaQuery.of(context).size.width * 0.038,
                                                        height: MediaQuery.of(context).size.width * 0.038,
                                                        color: Color(0xFF8D8D8D),
                                                      ),
                                                      Padding(
                                                        padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.05),
                                                        child: Text(
                                                          "Log out of Google",
                                                          style: GoogleFonts.roboto(color: Color(0xFF757575), fontWeight: FontWeight.w500, fontSize: MediaQuery.of(context).size.width * 0.038, fontStyle: FontStyle.normal),
                                                        ),
                                                      ),
                                                    ]),
                                                  )),
                                            )
                                          : Padding(
                                              padding: EdgeInsets.only(top: (isAlreadyUsed || isChangeSuccess || isDisconnectSuccessful) ? MediaQuery.of(context).size.height * 0.011 : MediaQuery.of(context).size.height * 0.077),
                                              child: Container(
                                                  height: MediaQuery.of(context).size.height * 0.058,
                                                  width: MediaQuery.of(context).size.width * 0.65,
                                                  child: ElevatedButton(
                                                    style: ButtonStyle(
                                                      backgroundColor: MaterialStateProperty.all(
                                                        Colors.white,
                                                      ),
                                                      shape: MaterialStateProperty.all(
                                                        RoundedRectangleBorder(
                                                          borderRadius: BorderRadius.circular(MediaQuery.of(context).size.height * 0.07),
                                                        ),
                                                      ),
                                                    ),
                                                    onPressed: () async {
                                                      var result = await Provider.of<LoginController>(context, listen: false).addGoogle();
                                                      setState(() {
                                                        idOnSn = LoginController.apiService.idOnSn;
                                                        idOnSn2 = LoginController.apiService.idOnSn2;
                                                        isAddGoogle = (idOnSn2 != null && idOnSn2 != "" && idOnSn2 != "null");
                                                        isAddFacebook = (idOnSn != null && idOnSn != "" && idOnSn != "null");
                                                        isAlreadyUsed = result == "error account exists";
                                                        isChangeSuccess = result == "success";
                                                      });
                                                      await Future.delayed(const Duration(seconds: 2), () {
                                                        setState(() {
                                                          isAlreadyUsed = false;
                                                          isChangeSuccess = false;
                                                        });
                                                      });
                                                    },
                                                    child: Row(children: [
                                                      SvgPicture.asset(
                                                        'images/google_image.svg',
                                                        alignment: Alignment.center,
                                                        width: MediaQuery.of(context).size.width * 0.038,
                                                        height: MediaQuery.of(context).size.width * 0.038,
                                                      ),
                                                      Padding(
                                                        padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.05),
                                                        child: Text(
                                                          "Sign in with Google",
                                                          style: GoogleFonts.roboto(color: Color(0xFF757575), fontWeight: FontWeight.w500, fontSize: MediaQuery.of(context).size.width * 0.038, fontStyle: FontStyle.normal),
                                                        ),
                                                      ),
                                                    ]),
                                                  )),
                                            ),
                                      isAddFacebook
                                          ? Padding(
                                              padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.011),
                                              child: Container(
                                                height: MediaQuery.of(context).size.height * 0.058,
                                                width: MediaQuery.of(context).size.width * 0.65,
                                                child: ElevatedButton(
                                                  onPressed: () async {
                                                    var result = await LoginController.apiService.removeSocial(Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "", "facebook");
                                                    if (result == "success") {
                                                      setState(() {
                                                        idOnSn = LoginController.apiService.idOnSn;
                                                        idOnSn2 = LoginController.apiService.idOnSn2;
                                                        isAddGoogle = (idOnSn2 != null && idOnSn2 != "" && idOnSn2 != "null");
                                                        isAddFacebook = (idOnSn != null && idOnSn != "" && idOnSn != "null");
                                                        isDisconnectSuccessful = true;
                                                      });
                                                      await Future.delayed(const Duration(seconds: 2), () {
                                                        setState(() {
                                                          isDisconnectSuccessful = false;
                                                        });
                                                      });
                                                    }
                                                  },
                                                  style: ButtonStyle(
                                                    backgroundColor: MaterialStateProperty.all(
                                                      Color(0xFFEEEEEE),
                                                    ),
                                                    shape: MaterialStateProperty.all(
                                                      RoundedRectangleBorder(
                                                        borderRadius: BorderRadius.circular(MediaQuery.of(context).size.height * 0.07),
                                                      ),
                                                    ),
                                                  ),
                                                  child: Row(children: [
                                                    Padding(
                                                      padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.007, 0, 0, 0),
                                                      child: SvgPicture.asset(
                                                        'images/facebook-icon.svg',
                                                        alignment: Alignment.center,
                                                        color: Color(0xFF8D8D8D),
                                                        width: MediaQuery.of(context).size.width * 0.03,
                                                        height: MediaQuery.of(context).size.width * 0.047,
                                                      ),
                                                    ),
                                                    Padding(
                                                      padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.05, 0, 0, 0),
                                                      child: Text(
                                                        "Log out of Facebook",
                                                        style: GoogleFonts.roboto(color: Color(0xFF757575), fontWeight: FontWeight.w500, fontSize: MediaQuery.of(context).size.width * 0.038, fontStyle: FontStyle.normal),
                                                      ),
                                                    ),
                                                  ]),
                                                ),
                                              ),
                                            )
                                          : Padding(
                                              padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.011),
                                              child: Container(
                                                height: MediaQuery.of(context).size.height * 0.058,
                                                width: MediaQuery.of(context).size.width * 0.65,
                                                child: ElevatedButton(
                                                  onPressed: () async {
                                                    var result = await Provider.of<LoginController>(context, listen: false).addFacebookAccount();
                                                    setState(() {
                                                      idOnSn = LoginController.apiService.idOnSn;
                                                      idOnSn2 = LoginController.apiService.idOnSn2;
                                                      isAddGoogle = (idOnSn2 != null && idOnSn2 != "" && idOnSn2 != "null");
                                                      isAddFacebook = (idOnSn != null && idOnSn != "" && idOnSn != "null");
                                                      isAlreadyUsed = result == "error account exists";
                                                      isChangeSuccess = result == "success";
                                                    });
                                                    await Future.delayed(const Duration(seconds: 2), () {
                                                      setState(() {
                                                        isAlreadyUsed = false;
                                                        isChangeSuccess = false;
                                                      });
                                                    });
                                                  },
                                                  style: ButtonStyle(
                                                    backgroundColor: MaterialStateProperty.all(
                                                      Color(0xFF1967FF),
                                                    ),
                                                    shape: MaterialStateProperty.all(
                                                      RoundedRectangleBorder(
                                                        borderRadius: BorderRadius.circular(MediaQuery.of(context).size.height * 0.07),
                                                      ),
                                                    ),
                                                  ),
                                                  child: Row(children: [
                                                    Padding(
                                                      padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.007, 0, 0, 0),
                                                      child: SvgPicture.asset(
                                                        'images/facebook-icon.svg',
                                                        alignment: Alignment.center,
                                                        color: Colors.white,
                                                        width: MediaQuery.of(context).size.width * 0.03,
                                                        height: MediaQuery.of(context).size.width * 0.047,
                                                      ),
                                                    ),
                                                    Padding(
                                                      padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.05, 0, 0, 0),
                                                      child: Text(
                                                        "Sign in with Facebook",
                                                        style: GoogleFonts.roboto(color: Colors.white, fontWeight: FontWeight.w500, fontSize: MediaQuery.of(context).size.width * 0.038, fontStyle: FontStyle.normal),
                                                      ),
                                                    ),
                                                  ]),
                                                ),
                                              ),
                                            ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ))
                ],
              ),
            ),
            HeaderNavigation(_isVisible, _isWalletVisible, _isNotificationVisible)
          ],
        ),
      ),
    );
  }
}
