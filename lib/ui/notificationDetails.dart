import 'package:flutter/material.dart';

class TabExample extends StatefulWidget {
  @override
  _TabExampleState createState() => _TabExampleState();
}

class _TabExampleState extends State<TabExample> {
  var tabIndex = 0;

  @override
  Widget build(BuildContext context) {
    var childList = [
      Container(
        color: Colors.green,
        child: Center(
          child: ElevatedButton(
              child: Text('to Tab 3'),
              onPressed: () {
                setState(() {
                  tabIndex = 2;
                });
              }),
        ),
      ),
      Container(color: Colors.red),
      Container(color: Colors.yellow),
      Container(color: Colors.cyan),
    ];

    return DefaultTabController(
      length: 4,
      initialIndex: tabIndex,
      child: Scaffold(
        appBar: AppBar(),
        body: childList[tabIndex],
        bottomNavigationBar: TabBar(
          onTap: (index) {
            setState(() {
              tabIndex = index;
            });
          },
          labelColor: Colors.black,
          tabs: <Widget>[
            Tab(text: 'Green'),
            Tab(text: 'Red'),
            Tab(text: 'Yellow'),
            Tab(text: 'Cyan'),
          ],
        ),
      ),
    );
  }
}
