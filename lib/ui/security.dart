import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:ihave/controllers/LoginController.dart';
import 'package:ihave/controllers/walletController.dart';
import 'package:ihave/service/apiService.dart';
import 'package:ihave/ui/staticElements/headerNavigation.dart';
import 'package:ihave/util/utils.dart';
import 'package:ihave/widgets/alertDialogWidget.dart';
import 'package:ihave/widgets/customAppBar.dart';
import 'package:pinput/pin_put/pin_put.dart';
import 'package:provider/provider.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:url_launcher/url_launcher.dart';

import '../widgets/customBottomBar.dart';
import 'WelcomeScreen.dart';

class SecurityPage extends StatefulWidget {
  const SecurityPage({Key? key}) : super(key: key);

  @override
  _SecurityPageState createState() => _SecurityPageState();
}

class _SecurityPageState extends State<SecurityPage> {
  TextEditingController _authKey = TextEditingController();
  bool isSwitched = false;
  final FocusNode _pinPutFocusNode = FocusNode();
  final TextEditingController _pinPutController = TextEditingController();
  bool _isVisible = false;
  bool _isProfileVisible = false;
  bool _isWalletVisible = false;
  bool _isNotificationVisible = false;
  String authKey = "";
  bool isPinPutEdited = false;
  bool isPinPutfocused = false;
  bool _isCorrectCode = false;
  bool isConfirmPasswordEdited = false;
  bool isActivatedButton = false;
  bool isConfirmedPassword = false;
  Uint8List? qrImage;
  bool successCode = false;
  bool errorCode = false;

  final _formKeyGlobal = GlobalKey<FormState>();

  BoxDecoration get _pinPutDecoration {
    return BoxDecoration(
      color: Colors.white,
      borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.033),
      border: Border.all(
        color: Colors.white,
      ),
    );
  }

  _launchURLGplay() async {
    const url = 'https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2&hl=fr&gl=US';
    if (await canLaunch(url)) {
      await launch(url, forceSafariVC: true, forceWebView: true);
    } else {
      throw 'Could not launch $url';
    }
  }

  _launchURLAppStore() async {
    const url = 'https://apps.apple.com/us/app/google-authenticator/id388497605';
    if (await canLaunch(url)) {
      await launch(url, forceSafariVC: true, forceWebView: true);
    } else {
      throw 'Could not launch $url';
    }
  }

  Future fetchSecret() async {
    var secret = await LoginController.apiService.get2FaSecret(Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "");
    return secret;
  }

  Future check2Fa() async {
    LoginController.apiService.checkValidation(Provider.of<LoginController>(context).userDetails?.userToken ?? "");
    return LoginController.apiService.is2FA;
  }

  @override
  void initState() {
    check2Fa().then((value) {
      setState(() {
        isSwitched = LoginController.apiService.is2FA;
      });
    });
    if (!LoginController.apiService.is2FA) {
      fetchSecret().then((value) {
        setState(() {
          authKey = value["secret"];
          _authKey.text = authKey.length > 15 ? authKey.substring(0, 20) + "..." : authKey;
          var towFactorQr = value["qrCode"].toString().substring(value["qrCode"].toString().indexOf(",") + 1, value["qrCode"].toString().length);
          if (towFactorQr.length > 1) {
            qrImage = base64Decode(towFactorQr);
          }
        });
      });
    }
    isSwitched = LoginController.apiService.is2FA;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    setState(() {
      _isVisible = Provider.of<WalletController>(context, listen: true).isSideMenuVisible;
      _isProfileVisible = Provider.of<WalletController>(context, listen: true).isProfileVisible;
      _isWalletVisible = Provider.of<WalletController>(context, listen: true).isWalletVisible;
      _isNotificationVisible = Provider.of<WalletController>(context, listen: true).isNotificationVisible;
    });
    return WillPopScope(
        onWillPop: () {
          return Future.value(true);
        },
        child: Scaffold(
            appBar: CustomAppBar(),
            bottomNavigationBar: CustomBottomBar(),
            body: Stack(children: [
              SingleChildScrollView(
                  child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Container(
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Column(mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.start, children: [
                      Text(
                        "2-Step\nauthentication ",
                        style: GoogleFonts.poppins(
                          color: Color(0xFF1F2337),
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.03),
                        child: Row(
                          children: [
                            Text(
                              "Enable 2-step\nauthentication",
                              style: GoogleFonts.poppins(
                                color: Color(0xFF1F2337),
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            Spacer(),
                            Visibility(
                              visible: !LoginController.apiService.is2FA,
                              child: Column(
                                children: [
                                  SizedBox(
                                    height: MediaQuery.of(context).size.height * 0.015,
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.01),
                                    child: Switch(
                                      value: isSwitched,
                                      onChanged: (value) {
                                        setState(() {
                                          isSwitched = value;
                                        });
                                      },
                                      activeTrackColor: Color(0xFFA0A0FF),
                                      activeColor: Color(0xFF4048FF),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      Text(
                        "Add an extra level of security when logging into your wallet.",
                        style: GoogleFonts.poppins(fontSize: 14, fontWeight: FontWeight.w400, color: Color(0xFF75758F)),
                      ),
                      Visibility(
                        visible: isSwitched,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Visibility(
                                visible: LoginController.apiService.is2FA,
                                child: Row(
                                  children: [
                                    SvgPicture.asset(
                                      "images/true.svg",
                                      height: MediaQuery.of(context).size.width * 0.1,
                                      width: MediaQuery.of(context).size.width * 0.1,
                                    ),
                                    Text(
                                      " The 2-step connection is now \n activated for your account ",
                                      style: GoogleFonts.poppins(
                                        fontSize: 18,
                                        fontWeight: FontWeight.w500,
                                        color: Color(0xFF1F2337),
                                      ),
                                    ),
                                  ],
                                )),
                            Text(
                              "\nConfigure",
                              style: GoogleFonts.poppins(fontSize: 16, color: Color(0xFF1F2337), fontWeight: FontWeight.bold),
                            ),
                            Text(
                              "\nAt each connection, in addition to your password, you will need to use an authentication application that will generate a unique code and temporary. ",
                              style: GoogleFonts.poppins(
                                fontSize: 14,
                                color: Color(0xFF1F2337),
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                            Text(
                              "\nDownload an authenticator app.",
                              style: GoogleFonts.poppins(
                                fontSize: 14,
                                color: Color(0xFF1F2337),
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                            Center(
                              child: Padding(
                                padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.03),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    InkWell(
                                      child: Image.asset(Platform.isIOS ? "images/app-store-dl.png" : "images/googleplay.png"),
                                      onTap: () {
                                        Platform.isIOS ? _launchURLAppStore() : _launchURLGplay();
                                        ;
                                      },
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.015),
                              child: Divider(
                                indent: 0,
                                endIndent: 0,
                                color: Color(0xFFD6D6E8),
                                thickness: 1.5,
                              ),
                            ),
                            !LoginController.apiService.is2FA
                                ? Visibility(
                                    child: Padding(
                                      padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                                      child: Container(
                                          height: MediaQuery.of(context).size.height * 0.52,
                                          child: Column(
                                            mainAxisAlignment: MainAxisAlignment.start,
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                "Step1",
                                                style: GoogleFonts.poppins(fontSize: 16, color: Color(0xFF1F2337), fontWeight: FontWeight.bold),
                                              ),
                                              Text(
                                                "\nScan the QRcode opposite using your authenticator application, or enter the secret key manually.",
                                                style: GoogleFonts.poppins(
                                                  color: Color(0xFF1F2337),
                                                  fontSize: 14,
                                                ),
                                              ),
                                              Padding(
                                                padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.03, bottom: MediaQuery.of(context).size.height * 0.03),
                                                child: Row(
                                                  mainAxisAlignment: MainAxisAlignment.center,
                                                  children: [
                                                    Container(
                                                      width: MediaQuery.of(context).size.width * 0.8,
                                                      child: TextFormField(
                                                        style: GoogleFonts.poppins(
                                                          fontSize: MediaQuery.of(context).size.height * 0.018,
                                                          fontWeight: FontWeight.w600,
                                                          color: Color(0XFF4048ff),
                                                        ),
                                                        controller: _authKey,
                                                        onChanged: (value) {},
                                                        readOnly: true,
                                                        decoration: InputDecoration(
                                                          suffixIcon: SizedBox(
                                                            width: MediaQuery.of(context).size.width * 0.2,
                                                            child: ElevatedButton(
                                                              style: ButtonStyle(
                                                                backgroundColor: MaterialStateProperty.all(
                                                                  Color(0xFFF6F6FF),
                                                                ),
                                                                shape: MaterialStateProperty.all(
                                                                  RoundedRectangleBorder(
                                                                      borderRadius: BorderRadius.only(topRight: Radius.circular(MediaQuery.of(context).size.height * 0.05), bottomRight: Radius.circular(MediaQuery.of(context).size.height * 0.05)), side: BorderSide(color: Color(0xFF4048FF))),
                                                                ),
                                                                elevation: MaterialStateProperty.resolveWith<double>(
                                                                  (Set<MaterialState> states) {
                                                                    if (states.contains(MaterialState.disabled)) {
                                                                      return 0;
                                                                    }
                                                                    return 0; // Defer to the widget's default.
                                                                  },
                                                                ),
                                                              ),
                                                              onPressed: () {
                                                                Clipboard.setData(ClipboardData(text: authKey));
                                                                Fluttertoast.showToast(msg: "Copied to clipboard", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.BOTTOM, timeInSecForIosWeb: 1);
                                                              },
                                                              child: Icon(
                                                                Icons.copy,
                                                                color: Color(0XFF4048ff),
                                                              ),
                                                            ),
                                                          ),
                                                          errorBorder: OutlineInputBorder(
                                                              borderRadius: BorderRadius.circular(60),
                                                              borderSide: BorderSide(
                                                                color: Color(0xFF4048FF),
                                                              )),
                                                          contentPadding: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width * 0.06),
                                                          enabledBorder: OutlineInputBorder(
                                                              borderRadius: new BorderRadius.only(
                                                                  topLeft: Radius.circular(MediaQuery.of(context).size.height * 0.05),
                                                                  bottomLeft: Radius.circular(MediaQuery.of(context).size.height * 0.05),
                                                                  topRight: Radius.circular(MediaQuery.of(context).size.height * 0.05),
                                                                  bottomRight: Radius.circular(MediaQuery.of(context).size.height * 0.05)),
                                                              borderSide: BorderSide(
                                                                color: Color(0xFF4048FF),
                                                              )),
                                                          focusedBorder: new OutlineInputBorder(
                                                            borderRadius: new BorderRadius.only(
                                                                topLeft: Radius.circular(MediaQuery.of(context).size.height * 0.05),
                                                                bottomLeft: Radius.circular(MediaQuery.of(context).size.height * 0.05),
                                                                topRight: Radius.circular(MediaQuery.of(context).size.height * 0.05),
                                                                bottomRight: Radius.circular(MediaQuery.of(context).size.height * 0.05)),
                                                            borderSide: BorderSide(
                                                              color: Color(0xFF4048FF),
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              Center(
                                                  child: Padding(
                                                padding: const EdgeInsets.all(8.0),
                                                child: qrImage != null ? Image.memory(qrImage!) : SizedBox.shrink(),
                                              )),
                                            ],
                                          )),
                                    ),
                                  )
                                : Padding(
                                    padding: EdgeInsets.all(0.0),
                                  ),
                            Text(
                              "Step2",
                              style: GoogleFonts.poppins(color: Color(0xFF1F2337), fontSize: 16, fontWeight: FontWeight.bold),
                            ),
                            Text(
                              "\nEnter the 6-digit code displayed in your authentication application.",
                              style: GoogleFonts.poppins(
                                color: Color(0xFF1F2337),
                                fontSize: 16,
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.015),
                              child: Container(
                                decoration: BoxDecoration(
                                  color: Color(0xFFF6F6FE),
                                  border: Border.all(
                                    color: Colors.transparent,
                                  ),
                                  borderRadius: BorderRadius.all(Radius.circular(20)),
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Center(
                                        child: Padding(
                                          padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height * 0.007),
                                          child: Text(
                                            "Authentication code",
                                            style: GoogleFonts.poppins(
                                              fontSize: 18,
                                              fontWeight: FontWeight.w600,
                                              color: (isPinPutEdited) ? ((_isCorrectCode) ? Color(0xFF00CC9E) : Color(0xFFF52079)) : Color(0xFF4048FF),
                                            ),
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.fromLTRB(15, 5, 15, 5),
                                        child: Form(
                                          key: _formKeyGlobal,
                                          child: PinPut(
                                              controller: _pinPutController,
                                              fieldsCount: 6,
                                              focusNode: _pinPutFocusNode,
                                              onChanged: (content) async {
                                                isPinPutfocused = true;
                                                if (_pinPutController.text.length == 6) {
                                                  isPinPutEdited = true;
                                                  var codeConfirmed = await LoginController.apiService.verifyQrCode(_pinPutController.text, Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "");
                                                  isConfirmPasswordEdited = true;
                                                  isConfirmedPassword = codeConfirmed;
                                                  if (isConfirmedPassword) {
                                                    setState(() {
                                                      _isCorrectCode = true;
                                                      isActivatedButton = true;
                                                      errorCode == false;
                                                    });
                                                  } else {
                                                    setState(() {
                                                      _isCorrectCode = false;
                                                      isActivatedButton = false;
                                                      errorCode = true;
                                                    });
                                                  }
                                                }
                                              },
                                              textStyle: TextStyle(
                                                fontSize: MediaQuery.of(context).size.width * 0.05,
                                                fontWeight: FontWeight.bold,
                                                color: (isPinPutEdited) ? ((_isCorrectCode) ? Color(0xFF00CC9E) : Color(0xFFF52079)) : Color(0xFF4048FF),
                                              ),
                                              eachFieldHeight: MediaQuery.of(context).size.width * 0.1,
                                              eachFieldWidth: MediaQuery.of(context).size.width * 0.1,
                                              onSubmit: (String pin) => _showSnackBar(pin, context),
                                              submittedFieldDecoration: _pinPutDecoration.copyWith(
                                                  border: Border.all(
                                                    color: isPinPutfocused ? ((isPinPutEdited) ? ((_isCorrectCode) ? Color(0xFF00CC9E) : Color(0xFFF52079)) : Color(0xFF4048FF)) : Color(0xFF4048FF),
                                                  ),
                                                  borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width * 0.042)),
                                              selectedFieldDecoration: _pinPutDecoration.copyWith(
                                                  color: Colors.white,
                                                  borderRadius: BorderRadius.circular(15.0),
                                                  border: Border.all(
                                                    color: isPinPutfocused ? ((isPinPutEdited) ? ((_isCorrectCode) ? Color(0xFF00CC9E) : Color(0xFFF52079)) : Color(0xFF4048FF)) : Color(0xFF4048FF),
                                                  )),
                                              followingFieldDecoration: _pinPutDecoration.copyWith(
                                                borderRadius: BorderRadius.circular(5.0),
                                                border: Border.all(
                                                  color: isPinPutfocused ? ((isPinPutEdited) ? ((_isCorrectCode) ? Color(0xFF00CC9E) : Color(0xFFF52079)) : Color(0xFF4048FF)) : Colors.grey,
                                                ),
                                              )),
                                        ),
                                      ),
                                      Center(
                                        child: Padding(
                                          padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height * 0.007),
                                          child: Text(
                                            isPinPutEdited ? (_isCorrectCode ? "Code Match" : "Incorrect Code") : "This code will only be valid for 5 min",
                                            style: GoogleFonts.poppins(
                                              fontSize: 14,
                                              fontWeight: FontWeight.w700,
                                              color: isPinPutEdited ? (_isCorrectCode ? Color(0xFF00CC9E) : Color(0xFFF52079)) : Color(0xFF75758F),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                      Visibility(
                        visible: isSwitched,
                        child: Center(
                          child: Padding(
                            padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height * 0.05),
                            child: ElevatedButton(
                                style: ButtonStyle(
                                  minimumSize: MaterialStateProperty.all(Size(MediaQuery.of(context).size.width * 0.8, MediaQuery.of(context).size.height * 0.065)),
                                  backgroundColor: MaterialStateProperty.all(isActivatedButton ? Color(0xff4048FF) : Color(0xffbcbcbc)),
                                  shape: MaterialStateProperty.all(
                                    RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(30),
                                    ),
                                  ),
                                ),
                                onPressed: isActivatedButton
                                    ? () async {
                                        var result = await save(LoginController.apiService.is2FA);
                                        if (result["message"] != null && result["message"] != "" && result["message"] == "profile updated") {
                                          setState(() {
                                            LoginController.apiService.is2FA = !LoginController.apiService.is2FA;
                                            _pinPutController.text = "";
                                            _isCorrectCode = false;
                                            isPinPutEdited = false;
                                            isSwitched = LoginController.apiService.is2FA;
                                          });
                                          if (LoginController.apiService.is2FA) {
                                            showDialog(
                                                context: context,
                                                builder: (BuildContext context) {
                                                  return StatefulBuilder(builder: (context, setState) {
                                                    return AlertDialog(
                                                        insetPadding: EdgeInsets.zero,
                                                        shape: RoundedRectangleBorder(
                                                          borderRadius: BorderRadius.all(Radius.circular(30)),
                                                        ),
                                                        content: Container(
                                                          width: MediaQuery.of(context).size.width * 0.72,
                                                          height: MediaQuery.of(context).size.height * 0.3,
                                                          child: Column(children: [
                                                            Padding(
                                                              padding: EdgeInsets.only(bottom: 8.0),
                                                              child: Row(
                                                                children: [
                                                                  Spacer(),
                                                                  Image.asset("images/succ.png"),
                                                                  Spacer(),
                                                                  Padding(
                                                                    padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.width * 0.2),
                                                                    child: IconButton(
                                                                        onPressed: () {
                                                                          Navigator.of(context).pop();
                                                                        },
                                                                        icon: Icon(
                                                                          Icons.close,
                                                                          color: Colors.black54,
                                                                          size: 40,
                                                                        )),
                                                                  ),
                                                                ],
                                                              ),
                                                            ),
                                                            Text("Success!", style: GoogleFonts.poppins(color: Colors.black, fontSize: 16.0, fontWeight: FontWeight.bold)),
                                                            Text("2FA activated Successfully", style: GoogleFonts.poppins(color: Colors.black, fontSize: 16.0, fontWeight: FontWeight.normal))
                                                          ]),
                                                        ));
                                                  });
                                                });
                                            Timer(Duration(seconds: 1), () {
                                              Provider.of<WalletController>(context, listen: false).selectedIndex = 2;
                                              Navigator.of(context).pop();
                                              gotoPageGlobal(context, WelcomeScreen());
                                            });
                                          } else {
                                            {
                                              showDialog(
                                                  context: context,
                                                  builder: (BuildContext context) {
                                                    return StatefulBuilder(builder: (context, setState) {
                                                      return AlertDialog(
                                                          insetPadding: EdgeInsets.zero,
                                                          shape: RoundedRectangleBorder(
                                                            borderRadius: BorderRadius.all(Radius.circular(30)),
                                                          ),
                                                          content: Container(
                                                            width: MediaQuery.of(context).size.width * 0.72,
                                                            height: MediaQuery.of(context).size.height * 0.3,
                                                            child: Column(children: [
                                                              Padding(
                                                                padding: EdgeInsets.only(bottom: 8.0),
                                                                child: Row(
                                                                  children: [
                                                                    Spacer(),
                                                                    Image.asset("images/succ.png"),
                                                                    Spacer(),
                                                                    Padding(
                                                                      padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.width * 0.2),
                                                                      child: IconButton(
                                                                          onPressed: () {
                                                                            Navigator.of(context).pop();
                                                                          },
                                                                          icon: Icon(
                                                                            Icons.close,
                                                                            color: Colors.black54,
                                                                            size: 40,
                                                                          )),
                                                                    ),
                                                                  ],
                                                                ),
                                                              ),
                                                              Text("Success", style: GoogleFonts.poppins(color: Colors.black, fontSize: 16.0, fontWeight: FontWeight.bold)),
                                                              Text("2FA deactivated Successfully", style: GoogleFonts.poppins(color: Colors.black, fontSize: 16.0, fontWeight: FontWeight.normal))
                                                            ]),
                                                          ));
                                                    });
                                                  });
                                              Timer(Duration(seconds: 1), () {
                                                Provider.of<WalletController>(context, listen: false).selectedIndex = 2;
                                                Navigator.of(context).pop();
                                                gotoPageGlobal(context, WelcomeScreen());
                                              });
                                            }
                                          }
                                        } else {
                                          {
                                            showDialog(
                                                context: context,
                                                builder: (BuildContext context) {
                                                  return StatefulBuilder(builder: (context, setState) {
                                                    return AlertDialog(
                                                        insetPadding: EdgeInsets.zero,
                                                        shape: RoundedRectangleBorder(
                                                          borderRadius: BorderRadius.all(Radius.circular(30)),
                                                        ),
                                                        content: Container(
                                                          height: 150,
                                                          width: 200,
                                                          child: Column(children: [
                                                            Padding(
                                                              padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.height * 0.22, 0, 0, 0),
                                                              child: IconButton(
                                                                  onPressed: () {
                                                                    Navigator.of(context).pop();
                                                                  },
                                                                  icon: Icon(
                                                                    Icons.close,
                                                                    color: Colors.black,
                                                                    size: 30,
                                                                  )),
                                                            ),
                                                            Text("Error", style: GoogleFonts.poppins(color: Colors.black, fontSize: 16.0, fontWeight: FontWeight.bold)),
                                                            Text("Something went wrong  please try again", style: GoogleFonts.poppins(color: Colors.black, fontSize: 16.0, fontWeight: FontWeight.normal))
                                                          ]),
                                                        ));
                                                  });
                                                });
                                            Timer(Duration(seconds: 1), () {
                                              Provider.of<WalletController>(context, listen: false).selectedIndex = 2;
                                              Navigator.of(context).pop();
                                              gotoPageGlobal(context, WelcomeScreen());
                                            });
                                          }
                                        }
                                      }
                                    : null,
                                child: Text(
                                  LoginController.apiService.is2FA ? "Disable" : "Confirm",
                                  style: GoogleFonts.poppins(
                                    fontSize: 15,
                                    fontWeight: FontWeight.bold,
                                    color: isActivatedButton ? Colors.white : Colors.grey,
                                  ),
                                )),
                          ),
                        ),
                      )
                    ]),
                  ),
                ),
              )),
              HeaderNavigation(_isVisible, _isWalletVisible, _isNotificationVisible)
            ])));
  }

  void _showSnackBar(String pin, BuildContext context) {
    final snackBar = SnackBar(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(18.0),
      ),
      duration: Duration(seconds: 5),
      content: Container(
          height: 80.0,
          color: Colors.grey,
          child: Center(
            child: Text(
              'Pin Submitted. Value: $pin',
              style: TextStyle(fontSize: 25.0, backgroundColor: Colors.grey),
            ),
          )),
      backgroundColor: Colors.grey,
    );
  }

  save(bool isSwitched) async {
    var result = await LoginController.apiService.activate2FA(Provider.of<LoginController>(context, listen: false).userDetails?.userToken ?? "", !LoginController.apiService.is2FA);
    return result;
  }
}
