import 'package:flutter_test/flutter_test.dart';
import 'package:ihave/util/utils.dart';

void main(){
  group("Logic", (){
    // format dollar
      test('amount should be formatted Properly', () {
      double amount = 0;
      var formatted = formatDollarBalance(amount);
      print (formatted);
      expect(formatted, "\$0.00");
  });
      test('amount should be formatted Properly', () {
        double amount = 0.01;
        var formatted = formatDollarBalance(amount);
        print (formatted);
        expect(formatted, "\$0.01");
      });
      test('amount should be formatted Properly', () {
        double amount = 0.0112546879;
        var formatted = formatDollarBalance(amount);
        print (formatted);
        expect(formatted, "\$0.01125468");
      });
      test('amount should be formatted Properly', () {
        double amount = 1;
        var formatted = formatDollarBalance(amount);
        print (formatted);
        expect(formatted, "\$1");
      });
      test('amount should be formatted Properly', () {
        double amount = 1.8152368;
        var formatted = formatDollarBalance(amount);
        print (formatted);
        expect(formatted, "\$1.815236");
      });
      test('amount should be formatted Properly', () {
        double amount = 9.089564231;
        var formatted = formatDollarBalance(amount);
        print (formatted);
        expect(formatted, "\$9.0895");
      });
     test('amount should be formatted Properly', () {
        double amount = 9;
        var formatted = formatDollarBalance(amount);
        print (formatted);
        expect(formatted, "\$9");
      });
      test('amount should be formatted Properly', () {
        double amount = 11;
        var formatted = formatDollarBalance(amount);
        print (formatted);
        expect(formatted, "\$11.00");
      });
      test('amount should be formatted Properly', () {
        double amount = 111;
        var formatted = formatDollarBalance(amount);
        print (formatted);
        expect(formatted, "\$111.00");
      });
      test('amount should be formatted Properly', () {
        double amount = 111.2531532;
        var formatted = formatDollarBalance(amount);
        print (formatted);
        expect(formatted, "\$111.25");
      });
      test('amount should be formatted Properly', () {
        double amount = 1112.531532;
        var formatted = formatDollarBalance(amount);
        print (formatted);
        expect(formatted, "\$1,112.53");
      });
      test('amount should be formatted Properly', () {
        double amount = 11125.31532;
        var formatted = formatDollarBalance(amount);
        print (formatted);
        expect(formatted, "\$11,125");
      });
      test('amount should be formatted Properly', () {
        double amount = 111253153;
        var formatted = formatDollarBalance(amount);
        print (formatted);
        expect(formatted, "\$111,253,153");
      });
      test('amount should be formatted Properly', () {
        double amount = 111253;
        var formatted = formatDollarBalance(amount);
        print (formatted);
        expect(formatted, "\$111,253");
      });

      //format crypto

      test('amount should be formatted Properly', () {
        double amount = 0;
        var formatted = formatCryptoBalance(amount, "");
        print (formatted);
        expect(formatted, "0.00");
      });
      test('amount should be formatted Properly', () {
        double amount = 0.01;
        var formatted = formatCryptoBalance(amount, "");
        print (formatted);
        expect(formatted, "0.01");
      });
      test('amount should be formatted Properly', () {
        double amount = 0.0112546879;
        var formatted = formatCryptoBalance(amount, "");
        print (formatted);
        expect(formatted, "0.01125468");
      });
      test('amount should be formatted Properly', () {
        double amount = 1;
        var formatted = formatCryptoBalance(amount, "");
        print (formatted);
        expect(formatted, "1");
      });
      test('amount should be formatted Properly', () {
        double amount = 1.8152368;
        var formatted = formatCryptoBalance(amount, "");
        print (formatted);
        expect(formatted, "1.815236");
      });
      test('amount should be formatted Properly', () {
        double amount = 9.089564231;
        var formatted = formatCryptoBalance(amount, "");
        print (formatted);
        expect(formatted, "9.0895");
      });
      test('amount should be formatted Properly', () {
        double amount = 9;
        var formatted = formatCryptoBalance(amount, "");
        print (formatted);
        expect(formatted, "9");
      });
      test('amount should be formatted Properly', () {
        double amount = 11;
        var formatted = formatCryptoBalance(amount, "");
        print (formatted);
        expect(formatted, "11.00");
      });
      test('amount should be formatted Properly', () {
        double amount = 111;
        var formatted = formatCryptoBalance(amount, "");
        print (formatted);
        expect(formatted, "111.00");
      });
      test('amount should be formatted Properly', () {
        double amount = 111.2531532;
        var formatted = formatCryptoBalance(amount, "");
        print (formatted);
        expect(formatted, "111.25");
      });
      test('amount should be formatted Properly', () {
        double amount = 1112.531532;
        var formatted = formatCryptoBalance(amount, "");
        print (formatted);
        expect(formatted, "1,112.53");
      });
      test('amount should be formatted Properly', () {
        double amount = 11125.31532;
        var formatted = formatCryptoBalance(amount, "");
        print (formatted);
        expect(formatted, "11,125");
      });
      test('amount should be formatted Properly', () {
        double amount = 111253153;
        var formatted = formatCryptoBalance(amount, "");
        print (formatted);
        expect(formatted, "111,253,153");
      });
      test('amount should be formatted Properly', () {
        double amount = 111253;
        var formatted = formatCryptoBalance(amount, "");
        print (formatted);
        expect(formatted, "111,253");
      });
  });
}